//
//  CompartirViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 07/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "CompartirViewController.h"
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>
#import <Twitter/Twitter.h>
#import "UIViewAmpliado.h"
#import "UIAllControls.h"

@interface CompartirViewController ()

@property (nonatomic) IBOutlet MKGradientButton *btnFacebook;
@property (nonatomic) IBOutlet MKGradientButton *btnTwitter;
@property (nonatomic) IBOutlet MKGradientButton *btnCancelar;

- (IBAction)btnFacebook_click:(id)sender;
- (IBAction)btnTwitter_click:(id)sender;
- (IBAction)btnCancelar_click:(id)sender;


@end

@implementation CompartirViewController

#define GRUPO_COMPARTIR @"CompartirViewController"

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.btnCancelar);
    Traducir(self.btnFacebook);
    Traducir(self.btnTwitter);
    
    [self.btnCancelar setBorderColor:[Locator Default].Color.Blanco];
    [self.btnFacebook setBorderColor:[Locator Default].Color.Blanco];
    [self.btnTwitter setBorderColor:[Locator Default].Color.Blanco];
}

- (void)viewDidUnload
{
    [self setBtnFacebook:nil];
    [self setBtnTwitter:nil];
    [self setBtnCancelar:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (IBAction)btnFacebook_click:(id)sender
{
    [UIImage ImageAssyncFromStringUrl:[self getURlImageQR] Grupo:GRUPO_COMPARTIR Result:^(UIImage *pResult)
     {
         SLComposeViewController *lViewFacebook = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
         
         [lViewFacebook addImage:pResult];
         [lViewFacebook setInitialText:Traducir(@"Captura tu tambien tu codigo Yaesmio!")];
         
         lViewFacebook.completionHandler = ^(SLComposeViewControllerResult result)
         {
             [self btnCancelar_click:nil];
         };
         [self presentViewController:lViewFacebook animated:true completion:nil];
     }];
}

- (IBAction)btnTwitter_click:(id)sender
{
    NSString *lurlQR = [self getURlImageQR];
    
    [UIImage ImageAssyncFromStringUrl:lurlQR Grupo:GRUPO_COMPARTIR Result:^(UIImage *pResult)
     {
         SLComposeViewController *lViewTwitter = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
         [lViewTwitter addImage:pResult];
         [lViewTwitter setInitialText:Traducir(@"Captura tu tambien tu codigo Yaesmio!")];
         lViewTwitter.completionHandler = ^(SLComposeViewControllerResult result)
         {
             [self dismissViewControllerAnimated:true completion:nil];
             [self btnCancelar_click:nil];
         };
         [self presentViewController:lViewTwitter animated:true completion:nil];
     }];
}

- (NSString *)getURlImageQR
{
    return self.producto.ImagenQR.UrlPath;
}

- (IBAction)btnCancelar_click:(id)sender
{
    [self dismissSemiModalViewController:self];
    
    if (self.Respond != nil)
    {
        self.Respond(0);
    }
}

@end
