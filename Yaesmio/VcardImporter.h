//
//  VcardImporter.h
//  AddressBookVcardImport
//
//  Created by Alan Harper on 20/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

@interface VcardImporter : NSObject

@property (assign) ABAddressBookRef addressBook;
@property (assign) ABRecordRef      personRecord;
@property (strong) NSString         *base64image;

- (void) parseFilesVcfWithPath:(NSString*)path;
- (void) parseString:(NSString*)pString;

+(BOOL) isFormatVcard:(NSString*)pString;

@end