//
//  ParametrosConfirmarCompra.m
//  Yaesmio
//
//  Created by mglFreelance on 26/11/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ParametrosConfirmarCompra.h"

@implementation ParametrosConfirmarCompra

- (id)init
{
	if((self = [super init]))
    {
		self.IdCompra      = @"";
        self.IdPaypal      = @"";
	}
	
	return self;
}

- (id)init:(NSString*)pIdCompra Paypal:(NSString*)pIdPaypal
{
	if((self = [self init]))
    {
		self.IdCompra      = pIdCompra;
        self.IdPaypal      = pIdPaypal;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.IdCompra xmlNodeForDoc:node->doc elementName:@"IdentCompra" elementNSPrefix:@"tnsParametros"]);
    
    xmlAddChild(node, [self.IdPaypal xmlNodeForDoc:node->doc elementName:@"IdentPayPal" elementNSPrefix:@"tnsParametros"]);
}



@end
