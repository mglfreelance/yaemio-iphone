//
//  DestinatarioNotificacionCell.h
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DestinatarioNotificacion.h"

@interface DestinatarioNotificacionCell : UITableViewCell

-(void)ConfigurarConDestinatario:(DestinatarioNotificacion*)pDestinatario;

@end
