//
//  Emoticono.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SoapObject.h"

@interface Emoticono : SoapObject

+ (Emoticono *)deserializeNode:(xmlNodePtr)cur;


/* elements */
@property (assign) int      IdEmoticono;
@property (strong) Media    *Imagen;

+(Emoticono*)getEmoticonoArray:(NSArray*)pArray withID:(int)pId;

@end