//
//  ContactsManager.m
//  Yaesmio
//
//  Created by freelance on 21/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ContactsManager.h"
#import "AddressBook.h"

@interface ContactsManager ()

@property (strong) RHAddressBook *AddressBook;
@property (assign) BOOL seMostrosMensajePermisos;

@end

@implementation ContactsManager


- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.AddressBook = nil;
        self.seMostrosMensajePermisos = false;
    }
    
    return self;
}

- (BOOL)seTieneAcceso
{
    return  (self.AddressBook != nil);
}

- (void)ComprobarAccesoAddressBook
{
    if (self.AddressBook == nil)
    {
        self.AddressBook = [self obtenerAccesoAContactosTelefono];
    }
}

- (NSString *)getNombreContactoPorMail:(NSString *)pMail
{
    if (self.seTieneAcceso)
    {
        NSArray *list = [self.AddressBook peopleWithEmail:pMail];
        
        if (list != nil && list.count > 0)
        {
            RHPerson *people = list.firstObject;
            
            if (people != nil)
            {
                return people.firstName;
            }
        }
    }
    
    return pMail;
}

- (UIImage *)getImagenContactoPorMail:(NSString *)pMail
{
    if (self.seTieneAcceso)
    {
        NSArray *list = [self.AddressBook peopleWithEmail:pMail];
        
        if (list != nil && list.count > 0)
        {
            RHPerson *people = list.firstObject;
            
            if (people != nil && people.hasImage)
            {
                return people.originalImage;
            }
        }
    }
    
    return [Locator Default].Imagen.SinContacto;
}

- (NSArray *)getListaContactosConMailOTelefono
{
    NSMutableArray *resul = [NSMutableArray new];
    
    for (RHPerson *item in[self.AddressBook peopleOrderedByUsersPreference])
    {
        if (item.emails.count > 0 || item.phoneNumbers.count > 0)
        {
            [resul addObject:item];
        }
    }
    
    return resul;
}

#pragma mark- Metodos Privados

- (RHAddressBook *)obtenerAccesoAContactosTelefono
{
    RHAuthorizationStatus Status = [RHAddressBook authorizationStatus];
    RHAddressBook *resultado = [[RHAddressBook alloc] init];
    
    if (Status == RHAuthorizationStatusNotDetermined)
    {
        [resultado requestAuthorizationWithCompletion: ^(bool granted, NSError *error)
         {
             if (granted == true)
             {
                 self.AddressBook = resultado;
             }
         }];
        return nil;
    }
    else if (Status == RHAuthorizationStatusAuthorized)
    {
        return resultado;
    }
    else if (self.seMostrosMensajePermisos == false)
    {
        [[Locator Default].Alerta showMessageAcept:Traducir(@"No es posible acceder a su libreta de contactos, por favor, autorize a la aplicación yaesmio desde ajustes->privacidad->contactos")];
        self.seMostrosMensajePermisos = true;
    }
    
    return nil;
}

@end
