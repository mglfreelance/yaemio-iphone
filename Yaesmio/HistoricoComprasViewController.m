//
//  HistoricoComprasViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "HistoricoComprasViewController.h"
#import "UIAllControls.h"
#import "ProductoHistoricoCell.h"

@interface HistoricoComprasViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic) IBOutlet UILabel *labelDescripcionCabecera;
@property (nonatomic) IBOutlet UITableView *TableViewListaCompras;
@property (nonatomic) IBOutlet UILabel *labelLoading;
@property (nonatomic) IBOutlet UIView *ViewLoading;
@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCamposTraducir;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentedTipoHistorico;

- (IBAction)TabTipoHistorico:(id)sender;
- (IBAction)CerrarModalExecute:(id)sender;

@property (nonatomic, assign) BOOL NoExisteMasProductos;
@property (strong, readonly)  NSMutableArray *ListaProductos;
@property (strong)            NSArray *ListaEmoticonos;

@end

@implementation HistoricoComprasViewController

@synthesize ListaProductos = _ListaProductos;

- (void)viewDidLoad
{
    [super viewDidLoad];
    Traducir(self.ListaCamposTraducir);
    self.NoExisteMasProductos = false;
    self.labelDescripcionCabecera.text = @"...";
    
    [self CargaInicial];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
}

- (void)viewUnload
{
    [self setLabelDescripcionCabecera:nil];
    [self setTableViewListaCompras:nil];
    _ListaProductos = nil;
    [self setListaCamposTraducir:nil];
    [self setListaEmoticonos:nil];
}

- (IBAction)CerrarModalExecute:(id)sender
{
    [self CerrarModal];
}

- (NSMutableArray *)ListaProductos
{
    if (_ListaProductos == nil)
    {
        _ListaProductos = [NSMutableArray new];
    }
    
    return _ListaProductos;
}

#pragma mark- Delegado UITableView

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    Historico *lHist = self.ListaProductos[indexPath.row];
    
    if (lHist.Valoracion != 0)
    {
        return 150;
    }
    else
    {
        return 140;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ListaProductos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProductoHistoricoCell *lVistaCelda = [tableView dequeueReusableCellWithIdentifier:@"HistoricoProducto"];
    
    [lVistaCelda Configurar:self Emoticonos:self.ListaEmoticonos Elemento:self.ListaProductos[indexPath.row]];
    
    if (indexPath.row == self.ListaProductos.count - 1)
    {
        [self ObtenerSiguienteListaProductos];
    }
    
    return lVistaCelda;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Historico *myHistorico = self.ListaProductos[indexPath.row];
    
    if (myHistorico.Valoracion == 0)
    {
        [[Locator Default].Vistas MostrarValoracionProductoHistorico:self Historico:myHistorico Emoticonos:self.ListaEmoticonos Resultado:^(int pResult)
         {
             [self.TableViewListaCompras beginUpdates];
             [self.TableViewListaCompras reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
             [self.TableViewListaCompras endUpdates];
         }];
    }
}

#pragma mark- Privado

- (void)CargaInicial
{
    if (self.ListaEmoticonos == nil)
    {
        self.ViewLoading.hidden = false;
        [[Locator Default].ServicioSOAP ObtenerListaEmoticonos:^(SRResultMethod pResult, NSArray *pLista)
         {
             self.ViewLoading.hidden = true;
             
             if (pResult == SRResultMethodYES)
             {
                 self.ListaEmoticonos = pLista;
                 [self ObtenerSiguienteListaProductos];
             }
             else
             {
                 [[Locator Default].Alerta showMessageAcept:Traducir(@"No es posible ahora mismo mostrar su historico, intentelo de nuevo mas tarde.") ResulBlock:^{
                     [self.navigationController popViewControllerAnimated:true];
                 }];
             }
         }];
    }
    else
    {
        [self ObtenerSiguienteListaProductos];
    }
}

- (void)ObtenerSiguienteListaProductos
{
    if (self.NoExisteMasProductos == false && self.ViewLoading.hidden)
    {
        self.labelLoading.text = Traducir(@"Obteniendo histórico compras.");
        self.ViewLoading.hidden = false;
        [[Locator Default].ServicioSOAP ObtenerListaHistoricoComprasConIdUser:[Locator Default].Preferencias.IdUsuario PosInicio:self.ListaProductos.count + 1.0 Cuantos:5 TipoProductosVer:[self getTipoHistoricoVer] Result:^(SRResultMethod pResult, NSString *pMensaje, NSArray *plistaProductos, int NumeroTotalElementos)
         {
             switch (pResult)
             {
                 case SRResultMethodYES:
                     [self.ListaProductos addObjectsFromArray:plistaProductos];
                     self.NoExisteMasProductos = (plistaProductos.count >= NumeroTotalElementos);
                     [self.TableViewListaCompras reloadData];
                     self.labelDescripcionCabecera.text = [self getCadenaCompraRealizada];
                     break;
                     
                 case SRResultMethodNO:
                     if ([pMensaje isEqualToString:@""])
                     {
                         pMensaje = Traducir(@"Todavia no tienes ninguna compra realizada. Empieza captruando un codigo yaesmio!");
                     }
                     self.labelDescripcionCabecera.text = pMensaje;
                     break;
                     
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
             }
             
             self.ViewLoading.hidden = true;
         }];
    }
}

#pragma mark- Metodos Privados

- (NSString *)getCadenaCompraRealizada
{
    int pNumCompras = 0;
    
    Historico *item = [self.ListaProductos lastObject];
    
    if (item != nil)
    {
        pNumCompras = [item.NumElementos intValue];
    }
    
    if (pNumCompras ==0)
    {
         return Traducir(@"Todavía no has realizado ninguna compra en los últimos 6 meses.");
    }
    else if (pNumCompras == 1)
    {
        return [NSString stringWithFormat:Traducir(@"Has realizado %d compra en los últimos 6 meses."), pNumCompras];
    }
    else
    {
        return [NSString stringWithFormat:Traducir(@"Has realizado %d compras en los últimos 6 meses."), pNumCompras];
    }
}

- (SRTipoHistoricoAObtener)getTipoHistoricoVer
{
    switch (self.SegmentedTipoHistorico.selectedSegmentIndex)
    {
        case 0:
            return SRTipoHistoricoAObtenerProductos;
            
        case 1:
            return SRTipoHistoricoAObtenerCupones;
    }
    
    return SRTipoHistoricoAObtenerTodos;
}

- (IBAction)TabTipoHistorico:(id)sender
{
    if (self.ViewLoading.hidden)
    {
        [self.ListaProductos removeAllObjects];
        self.NoExisteMasProductos = false;
        [self ObtenerSiguienteListaProductos];
    }
    else
    {
        if (self.SegmentedTipoHistorico.selectedSegmentIndex == 0)
        {
            self.SegmentedTipoHistorico.selectedSegmentIndex = 1;
        }
        else
        {
            self.SegmentedTipoHistorico.selectedSegmentIndex = 0;
        }
    }
}

@end
