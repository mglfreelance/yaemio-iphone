//
//  PreferencesManager.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 09/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import "PreferencesManager.h"

NSString *const PreferencesKeyNombreUsuario = @"PreferencesManager.NombreUsuario";
NSString *const PreferencesKeyNumeroNotificaciones = @"PreferencesManager.NumeroNotificacionesSinLeer";
NSString *const PreferencesKeyNumeroComprasSinValorar = @"PreferencesManager.NumeroComprasNoValoradas";
NSString *const PreferencesKeyLogadoUsuario = @"PreferencesManager.LogadoUsuario";

@implementation PreferencesManager

- (id)init
{
    if (self = [super init])
    {
        [self Configurar];
    }
    
    return self;
}

- (void)Configurar
{
    if ([self ExisteConfiguracionPrevia] == false)
    {
        _EstaUsuarioLogado                       = false;
        self.GeolocalizacionActiva               = true;
        self.AutoLoginUsuario                    = true;
        self.EscucharSonidoAlRecibirNotificacion = true;
        self.IdPush                              = @"";
        self.NumeroNotificacionesSinLeer         = 0;
        self.IdPaypal                            = @"";
        self.urlPasarelaPagoWeb                  = @"";
        self.NumeroComprasNoValoradas            = 0;
    }
}

- (BOOL)ExisteConfiguracionPrevia
{
    NSUserDefaults *Preferencias = [NSUserDefaults standardUserDefaults];
    
    return ([Preferencias objectForKey:@"GeolocalizacionActiva"] != nil);
}

- (BOOL)ExisteUsuario
{
    NSString *pUsuario = self.IdUsuario;
    
    if (pUsuario == nil)
    {
        return false;
    }
    else
    {
        return ([pUsuario isEqualToString:@""] == false);
    }
}

- (NSString *)IdUsuario
{
    id lValor = [self getPreferenceStringWitKey:@"IdUsuario"];
    
    return lValor;
}

- (void)setIdUsuario:(NSString *)IdUsuario
{
    [self saveStringPreferencesWithKey:@"IdUsuario" Dato:IdUsuario];
}

- (NSString *)MailUsuario
{
    return [self getPreferenceStringWitKey:@"Usuario"].lowercaseString;
}

- (void)setMailUsuario:(NSString *)pUsuario
{
    [self saveStringPreferencesWithKey:@"Usuario" Dato:pUsuario.lowercaseString];
}

- (NSString *)NombreUsuario
{
    return [self getPreferenceStringWitKey:@"NombreUsuario"];
}

- (void)setNombreUsuario:(NSString *)pNombre
{
    [self saveStringPreferencesWithKey:@"NombreUsuario" Dato:pNombre];
    [self EnviarNotificacionLocal:PreferencesKeyNombreUsuario];
}

- (NSString *)Password
{
    return [self getPreferenceStringWitKey:@"Password"];
}

- (void)setPassword:(NSString *)pPassword
{
    [self saveStringPreferencesWithKey:@"Password" Dato:pPassword];
}

- (BOOL)MostrarPantallaQR
{
    return [self getPreferenceBoolWitKey:@"MostrarPantallaQR"];
}

- (void)setMostrarPantallaQR:(BOOL)pbolPrimerInicioApp
{
    [self saveBoolPreferencesWithKey:@"MostrarPantallaQR" Dato:pbolPrimerInicioApp];
}

- (NSInteger)NumeroNotificacionesSinLeer
{
    return [self getPreferenceIntWitKey:@"NumeroNotificacionesSinLeer"];
}

- (void)setNumeroNotificacionesSinLeer:(NSInteger)pNumero
{
    if (pNumero < 0)
    {
        pNumero = 0;
    }
    
    [self saveIntPreferencesWithKey:@"NumeroNotificacionesSinLeer" Dato:(int)pNumero];
    [self EnviarNotificacionLocal:PreferencesKeyNumeroNotificaciones];
}

- (NSInteger)NumeroComprasNoValoradas
{
    return [self getPreferenceIntWitKey:@"NumeroComprasNoValoradas"];
}

- (void)setNumeroComprasNoValoradas:(NSInteger)pNumero
{
    if (pNumero < 0)
    {
        pNumero = 0;
    }
    
    [self saveIntPreferencesWithKey:@"NumeroComprasNoValoradas" Dato:(int)pNumero];
    [self EnviarNotificacionLocal:PreferencesKeyNumeroComprasSinValorar];
}

-(UIImage*)ImagenUsuario
{
    return [self getPreferenceObjectWitKey:@"ImagenUsuario"];
}

-(void)setImagenUsuario:(UIImage *)Imagen
{
    [self saveObjectPreferencesWithKey:@"ImagenUsuario" Dato:Imagen];
}

- (Provincia *)ProvinciaBusqueda
{
    return [self getPreferenceObjectWitKey:@"ProvinciaBusqueda"];
}

- (void)setProvinciaBusqueda:(Provincia *)ProvinciaBusqueda
{
    [self saveObjectPreferencesWithKey:@"ProvinciaBusqueda" Dato:ProvinciaBusqueda];
}

- (Pais *)PaisBusqueda
{
    return [self getPreferenceObjectWitKey:@"PaisBusqueda"];
}

- (void)setPaisBusqueda:(Pais *)PaisBusqueda
{
    [self saveObjectPreferencesWithKey:@"PaisBusqueda" Dato:PaisBusqueda];
}

- (NSArray *)ListaDestacados
{
    return [self getPreferenceObjectWitKey:@"ListaDestacados"];
}

- (void)setListaDestacados:(NSArray *)Lista
{
    [self saveObjectPreferencesWithKey:@"ListaDestacados" Dato:Lista];
}

- (void)GuardarUsuario:(NSString *)pIdUsuario NombreUsuario:(NSString *)pNombre Mail:(NSString *)pMail ConPassword:(NSString *)pPassword
{
    [self setIdUsuario:pIdUsuario];
    [self setMailUsuario:pMail];
    [self setPassword:pPassword];
    [self setNombreUsuario:pNombre];
    self.EstaUsuarioLogado = (pIdUsuario.isNotEmpty && pMail.isNotEmpty && pPassword.isNotEmpty);
    [self EnviarNotificacionLocal:PreferencesKeyLogadoUsuario];
}

- (BOOL)GeolocalizacionActiva
{
    return [self getPreferenceBoolWitKey:@"GeolocalizacionActiva"];
}

- (void)setGeolocalizacionActiva:(BOOL)GeolocalizacionActiva
{
    [self saveBoolPreferencesWithKey:@"GeolocalizacionActiva" Dato:GeolocalizacionActiva];
}

- (NSString *)IdApp
{
    return @"YAE1";
}

- (BOOL)AutoLoginUsuario
{
    return [self getPreferenceBoolWitKey:@"AutoLoginUsuario"];
}

- (void)setAutoLoginUsuario:(BOOL)AutoLoginUsuario
{
    [self saveBoolPreferencesWithKey:@"AutoLoginUsuario" Dato:AutoLoginUsuario];
    
    if (AutoLoginUsuario == false)
    {
        self.CompletadoDatosUsuario = false;
        self.NumeroVecesAMostrarMensajeCompletarUsuario = 0;
        self.Password  = @"";
    }
}

- (BOOL)EsPosibleAutologar
{
    bool lResultado = (self.AutoLoginUsuario && self.ExisteUsuario && self.EstaUsuarioLogado == false);
    
    return lResultado;
}

- (BOOL)CompletadoDatosUsuario
{
    return [self getPreferenceBoolWitKey:@"CompletadoDatosUsuario"];
}

- (void)setCompletadoDatosUsuario:(BOOL)CompletadoDatosUsuario
{
    [self saveBoolPreferencesWithKey:@"CompletadoDatosUsuario" Dato:CompletadoDatosUsuario];
}

- (int)NumeroVecesAMostrarMensajeCompletarUsuario
{
    return [self getPreferenceIntWitKey:@"NumeroVecesAMostrarMensajeCompletarUsuario"];
}

- (void)setNumeroVecesAMostrarMensajeCompletarUsuario:(int)NumeroVecesAMostrarMensajeCompletarUsuario
{
    [self saveIntPreferencesWithKey:@"NumeroVecesAMostrarMensajeCompletarUsuario" Dato:NumeroVecesAMostrarMensajeCompletarUsuario];
}

- (void)setUrlImagenFondoPantallaPrincipal:(NSString *)pUrl
{
    [self saveStringPreferencesWithKey:@"UrlImagenFondoPantallaPrincipal" Dato:pUrl];
}

- (NSString *)UrlImagenFondoPantallaPrincipal
{
    return [self getPreferenceStringWitKey:@"UrlImagenFondoPantallaPrincipal"];
}

- (NSString *)IdPush
{
    id lValor = [self getPreferenceStringWitKey:@"IdPush"];
    
    return lValor;
}

- (void)setIdPush:(NSString *)pId
{
    [self saveStringPreferencesWithKey:@"IdPush" Dato:pId];
}

- (BOOL)EscucharSonidoAlRecibirNotificacion
{
    BOOL lValor = [self getPreferenceBoolWitKey:@"EscucharSonidoAlRecibirNotificacion"];
    
    return lValor;
}

- (void)setEscucharSonidoAlRecibirNotificacion:(BOOL)pEscuchar
{
    [self saveBoolPreferencesWithKey:@"EscucharSonidoAlRecibirNotificacion" Dato:pEscuchar];
}

#pragma  mark- Privado

- (void)saveStringPreferencesWithKey:(NSString *)pkey Dato:(NSString *)pDato
{
    NSUserDefaults *Preferencias = [NSUserDefaults standardUserDefaults];
    
    [Preferencias setObject:pDato forKey:pkey];
    
    [Preferencias synchronize];
}

- (NSString *)getPreferenceStringWitKey:(NSString *)pKey
{
    NSUserDefaults *Preferencias = [NSUserDefaults standardUserDefaults];
    
    return [Preferencias stringForKey:pKey];
}

- (void)saveBoolPreferencesWithKey:(NSString *)pkey Dato:(BOOL)pDato
{
    NSUserDefaults *Preferencias = [NSUserDefaults standardUserDefaults];
    
    [Preferencias setBool:pDato forKey:pkey];
    
    [Preferencias synchronize];
}

- (BOOL)getPreferenceBoolWitKey:(NSString *)pKey
{
    NSUserDefaults *Preferencias = [NSUserDefaults standardUserDefaults];
    
    return [Preferencias boolForKey:pKey];
}

- (void)saveIntPreferencesWithKey:(NSString *)pkey Dato:(int)pDato
{
    NSUserDefaults *Preferencias = [NSUserDefaults standardUserDefaults];
    
    [Preferencias setInteger:pDato forKey:pkey];
    
    [Preferencias synchronize];
}

- (int)getPreferenceIntWitKey:(NSString *)pKey
{
    NSUserDefaults *Preferencias = [NSUserDefaults standardUserDefaults];
    
    return (int)[Preferencias integerForKey:pKey];
}

- (id)getPreferenceObjectWitKey:(NSString *)pKey
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:pKey];
    
    return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

- (void)saveObjectPreferencesWithKey:(NSString *)pkey Dato:(id<NSCoding>)pDato
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:pDato];
    
    NSUserDefaults *Preferencias = [NSUserDefaults standardUserDefaults];
    
    [Preferencias setObject:data forKey:pkey];
    
    [Preferencias synchronize];
}

- (void)EnviarNotificacionLocal:(NSString *)pKey
{
    [[NSNotificationCenter defaultCenter] postNotificationName:pKey object:self];
}

@end
