//
//  ContactoCell.h
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactoGrupo.h"

@interface ContactoCell : UICollectionViewCell

-(void)ConfigurarConContactoGrupo:(ContactoGrupo*)pContacto IndexPath:(NSIndexPath*)pIndex Delegate:(GenericViewCellBlock)pDelegate;

@end
