//
//  Destacado.m
//  Yaesmio
//
//  Created by freelance on 20/08/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ProductoDestacado.h"

@implementation ProductoDestacado

- (id)init
{
    if ((self = [super init]))
    {
        self.AbreviaturaMoneda  = @"";
        self.Precio             = [NSDecimalNumber zero];
        self.Filas              = 1;
        self.ProductoId         = @"";
        self.Columnas           = 1;
        self.Imagen             = [Media createMediaImage];
        self.Nombre             = @"";
        self.NombreEmpresa      = @"";
        self.Descuento          = [NSDecimalNumber zero];
        self.Recomendada        = false;
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    if (!self)
    {
        return nil;
    }
    self.AbreviaturaMoneda  = [aDecoder decodeObjectForKey:@"AbreviaturaMoneda"];
    self.Precio             = [aDecoder decodeObjectForKey:@"Precio"];
    self.Filas              = (int)[aDecoder decodeIntegerForKey:@"Filas"];
    self.ProductoId         = [aDecoder decodeObjectForKey:@"ProductoId"];
    self.Columnas           = (int)[aDecoder decodeIntegerForKey:@"Columnas"];
    self.Imagen             = [aDecoder decodeObjectForKey:@"Imagen"];
    self.Nombre             = [aDecoder decodeObjectForKey:@"Nombre"];
    self.NombreEmpresa      = [aDecoder decodeObjectForKey:@"NombreEmpresa"];
    self.Descuento          = [aDecoder decodeObjectForKey:@"Descuento"];
    self.Recomendada        = [aDecoder decodeBoolForKey:@"Recomendada"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.AbreviaturaMoneda forKey:@"AbreviaturaMoneda"];
    [aCoder encodeObject:self.Precio forKey:@"Precio"];
    [aCoder encodeInteger:self.Filas forKey:@"Filas"];
    [aCoder encodeObject:self.ProductoId forKey:@"ProductoId"];
    [aCoder encodeInteger:self.Columnas forKey:@"Columnas"];
    [aCoder encodeObject:self.Imagen forKey:@"Imagen"];
    [aCoder encodeObject:self.Nombre forKey:@"Nombre"];
    [aCoder encodeObject:self.NombreEmpresa  forKey:@"NombreEmpresa"];
    [aCoder encodeObject:self.Descuento forKey:@"Descuento"];
    [aCoder encodeBool:self.Recomendada forKey:@"Recomendada"];
}

+ (ProductoDestacado *)deserializeNode:(xmlNodePtr)cur
{
    ProductoDestacado *newObject = [ProductoDestacado new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    // NSLog(@"campo: %@",[NSString stringWithUTF8String: pobjCur->name]);
    
    if ([nodeName isEqualToString:@"Moneda"])
    {
        self.AbreviaturaMoneda = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"PrecioActual"])
    {
        self.Precio =  [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"IdProd"])
    {
        self.ProductoId = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Nombre"])
    {
        self.Nombre = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Imagen"])
    {
        self.Imagen.UrlPath = ComprobarImagen([NSString deserializeNode:pobjCur]);
    }
    else if ([nodeName isEqualToString:@"ImagenPequena"])
    {
        self.Imagen.UrlPathSmall = ComprobarImagen([NSString deserializeNode:pobjCur]);
    }
    else if ([nodeName isEqualToString:@"NombreEmpresa"])
    {
        self.NombreEmpresa = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ZZDESCUENTO"])
    {
        self.Descuento = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Recomendada"])
    {
        self.Recomendada = [SOAP_Boolean deserializeNode:pobjCur].boolValue;
    }
}

@end
