//
//  ProductoHistoricoCell.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Historico.h"

@interface ProductoHistoricoCell : UITableViewCell

-(void)Configurar:(UIViewController*)pView Emoticonos:(NSArray*)pLista Elemento:(Historico*)pHistorico;

@end
