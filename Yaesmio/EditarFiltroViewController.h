//
//  EditarFiltroViewController.h
//  Yaesmio
//
//  Created by freelance on 22/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "YaesmioViewControllers.h"
#import "FiltroBusquedaProductos.h"

@interface EditarFiltroViewController : YaesmioViewController

@property (strong) FiltroBusquedaProductos *Filtro;
@property (copy) ResultadoPorDefectoBlock Resultado;

@end
