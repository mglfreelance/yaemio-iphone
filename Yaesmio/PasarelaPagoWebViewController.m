//
//  PasarelaPagoWebViewController.m
//  Yaesmio
//
//  Created by freelance on 09/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "PasarelaPagoWebViewController.h"
#import "UIAllControls.h"

#define URL_PAGO @"%@version=%@&Ds_Merchant_Currency=%@&Ds_Merchant_Amount=%@&Ds_Merchant_MerchantName=%@&Ds_Merchant_Order=%@&Ds_Merchant_ConsumerLanguage=%@&Ds_Merchant_Identifier=%@&Ds_sap=%@"

@interface PasarelaPagoWebViewController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *WebView;

@property (strong) EmpresaCarrito *Carrito;
@property (strong) NSString *Referencia;
@property (copy) ResultadoPasarelaPagoWebBlock Resultado;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *IndicadorBusy;

@property (nonatomic, assign) BOOL isLoading;

@end

@implementation PasarelaPagoWebViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.WebView.scrollView.bounces = false;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:true];
    
    if (self.Carrito != nil)
    {
        self.isLoading = true;
        [self.WebView loadRequest:[[NSURLRequest alloc] initWithURL:[self getUrl]]];
    }
}

- (void)viewUnload
{
    [self setWebView:nil];
    [self setIndicadorBusy:nil];
    [self setCarrito:nil];
    [self setReferencia:nil];
    [self setResultado:nil];
}

- (void)ConfigurarConCarrito:(EmpresaCarrito *)carrito Referencia:(NSString *)referencia Resultado:(ResultadoPasarelaPagoWebBlock)resultado
{
    self.Carrito = carrito;
    self.Referencia = referencia;
    self.Resultado = resultado;
}

- (BOOL)isLoading
{
    return (self.IndicadorBusy.hidden == false);
}

- (void)setIsLoading:(BOOL)isLoading
{
    if (isLoading)
    {
        [self.IndicadorBusy startAnimating];
    }
    else
    {
        [self.IndicadorBusy stopAnimating];
    }
}

#pragma mark- Webview Delegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    self.isLoading = true;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.isLoading = false;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
#if DEBUG
    [[Locator Default].Alerta showMessageAcept:Traducir(@"Ha sido imposibe mostrar la web.") ResulBlock:^
    {
        [self.navigationController popViewControllerAnimated:true];
    }];
#endif
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSLog(@"%@", request);
    
    
    if ([request.URL.absoluteString rangeOfString:@"http://yaesmio.com/redsys/compraKO.php"].length > 0)
    {
        [self CerrarPantalla:nil];
    }
    else if ([request.URL.absoluteString rangeOfString:@"http://yaesmio.com/redsys/compraOK.php"].length > 0)
    {
        NSString *valor =  request.URL[@"Ds_AuthorisationCode"];
        [self CerrarPantalla:valor];
        
        //yaesmio.com/redsys/compraOK.php?s=0000000863&Ds_Date=10/06/2014&Ds_Hour=09:18&Ds_SecurePayment=1&Ds_Amount=23500&Ds_Currency=978&Ds_Order=0000000863&Ds_MerchantCode=014240899&Ds_Terminal=001&Ds_Signature=0DFD5B9431B50E084C1AA13746DB9495C42A3C36&Ds_Response=0000&Ds_TransactionType=0&Ds_MerchantData=&Ds_AuthorisationCode=318927&Ds_ExpiryDate=1712&Ds_Merchant_Identifier=09d53f737a93493b2fea162d9727008f288d7ace&Ds_ConsumerLanguage=1&Ds_Card_Country=724
    }
    
    return true;
}

#pragma  mark- Privado

- (NSURL *)getUrl
{
    NSString *resultado = [[[[[NSString stringWithFormat:URL_PAGO,[Locator Default].Preferencias.urlPasarelaPagoWeb, [self getVersion], [self getCurrency], self.Carrito.PrecioTotal, [self getMerchantName], [self getIdCarrito], [self getLanguage], [self getReference], [Locator Default].Preferencias.IdUsuario] stringByReplacingOccurrencesOfString:@"\n" withString:@""] stringByReplacingOccurrencesOfString:@" " withString:@"%20"] stringByReplacingOccurrencesOfString:@"\n\n" withString:@"\n"]stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    
    NSArray *newLineChars = [NSArray arrayWithObjects:@"\\u000A", @"\\u000B", @"\\u000C", @"\\u000D", @"\\u0085", @" ", nil];
    
    for (NSString *nl in newLineChars)
        resultado = [resultado stringByReplacingOccurrencesOfString:nl withString:@""];
    
    return [NSURL URLWithString:resultado];
}

- (NSString *)getCurrency
{
    if ([self.Carrito.Moneda.uppercaseString isEqualToString:@"EUR"])
    {
        return @"978";
    }
    else
    {
        return @"";
    }
}

- (NSString *)getIdCarrito
{
    NSString *version = [NSString stringWithFormat:@"P%@",self.Carrito.IDCarrito];
    
#ifdef DEBUG
    version = self.Carrito.IDCarrito; // modo sandbox
#endif
    
    return version;}


- (NSString *)getMerchantName
{
    if (self.Carrito.Elementos.count > 0)
    {
        ElementoCarrito *elemento = self.Carrito.Elementos.firstObject;
        
        if (elemento != nil)
        {
            return elemento.Producto.NombreEmpresa;
        }
    }
    
    return @"";
}

- (NSString *)getLanguage
{
    if ([[Locator Default].Idioma.IdiomaAppSegunSistema isEqualToString:@"es"])
    {
        return @"001";
    }
    
    return @"";
}

- (NSString *)getReference
{
    if (self.Referencia.isNotEmpty)
    {
        return self.Referencia;
    }
    else
    {
        return @"REQUIRED";
    }
}

- (NSString *)getVersion
{
    NSString *version = @"1";
    
#ifdef DEBUG
    version = @"0"; // modo sandbox
#endif
    
    return version;
}

- (void)CerrarPantalla:(NSString *)pId
{
    [self dismissViewControllerAnimated:true completion:nil];
    
    if (self.Resultado != nil)
    {
        self.Resultado(pId);
    }
}

@end
