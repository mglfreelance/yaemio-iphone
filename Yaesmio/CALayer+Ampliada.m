//
//  CALayer+Ampliada.m
//  Yaesmio
//
//  Created by freelance on 03/07/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "CALayer+Ampliada.h"

@implementation CALayer (Ampliada)

- (void)setBorderColorFromUIColor:(UIColor *)color
{
    self.borderColor = color.CGColor;
}

-(UIColor*) BorderColorFromUIColor
{
    return [UIColor colorWithCGColor:self.borderColor];
}

@end
