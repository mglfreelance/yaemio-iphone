//
//  ValorarHistoricoViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Historico.h"
#import "YaesmioViewControllers.h"

@interface ValorarHistoricoViewController : YaesmioViewController

@property (copy)   ResultadoPorDefectoBlock Resultado;
@property (strong) Historico                *ProductoHistorico;
@property (strong) NSArray                  *ListaEmoticonos;

@end
