//
//  MKGradientButton+Ampliado.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 16/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "MKGradientButton.h"

@interface MKGradientButton (Ampliado)

-(void) setStyleWithBackgroundColor:(UIColor*)pColor andBorderColor:(UIColor*) pBorderColor andButtonColor:(UIColor*) pButtonColor andBorderRadius:(float) pRadius;

@end
