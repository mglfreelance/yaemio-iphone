//
//  AlertaManager.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 22/10/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlertaManager : NSObject

- (void)Initialize:(UIWindow *)window;

- (void)showErrorConexSOAP;

- (void)showErrorConexSOAP:(void (^)())pblock;

- (void)showMessageAcept:(NSString *)pMensaje;

- (void)showMessageAcept:(NSString *)pMensaje ResulBlock:(void (^)())pblock;

- (void)showMessageAceptWithTitle:(NSString *)pTitle AndMessage:(NSString *)pMensaje;

- (void)showMessageAceptWithTitle:(NSString *)pTitle AndMessage:(NSString *)pMensaje ResulBlock:(void (^)())pblock;

- (void)showMessage:(NSString *)pTitle Message:(NSString *)pMessage cancelButtonTitle:(NSString *)cancelButtonTitle AceptButtonTitle:(NSString *)otherButtonTitles completionBlock:(void (^)(NSUInteger buttonIndex))block;

- (void)showMessage:(NSString *)pMessage cancelButtonTitle:(NSString *)cancelButtonTitle AceptButtonTitle:(NSString *)otherButtonTitles completionBlock:(void (^)(NSUInteger buttonIndex))block;

- (void)showMessageLoginIfNotLogin:(UIViewController *)pView Message:(NSString *)pMessage Result:(ResultadoPorDefectoBlock)pResult;

- (void)showBussy:(NSString *)pMensaje;

- (void)hideBussy;

- (void)showNotificationWithTitle:(NSString *)pTitle AndMessage:(NSString *)pMessage PushResult:(ResultadoBlock)pResult;

@end
