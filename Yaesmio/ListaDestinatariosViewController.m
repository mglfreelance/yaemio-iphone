//
//  ListaDestinatariosViewController.m
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ListaDestinatariosViewController.h"
#import "DestinatarioNotificacionCell.h"

@interface ListaDestinatariosViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *labelTitulo;

@end

@implementation ListaDestinatariosViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.labelTitulo);
    [[Locator Default].Contactos ComprobarAccesoAddressBook];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
}

- (void)viewUnload
{
    [self setLabelTitulo:nil];
}

#pragma mark- Delegado UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ListaDestinatarios.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DestinatarioNotificacionCell *lVistaCelda = [tableView dequeueReusableCellWithIdentifier:@"DestinatarioNotificacionCell"];
    
    [lVistaCelda ConfigurarConDestinatario:self.ListaDestinatarios[indexPath.row]];
    
    return lVistaCelda;
}

#pragma mark- Metodos Privados


@end
