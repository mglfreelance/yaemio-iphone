//
//  TituloInformacionCell.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TituloInformacion.h"

@interface TituloInformacionCell : UITableViewCell

-(void)ConfigurarConPosicion:(int)pPosicion Elemento:(TituloInformacion*)pTitulo;

@end
