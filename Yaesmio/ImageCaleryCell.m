//
//  ImageCaleryCell.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ImageCaleryCell.h"
#import "UIAllControls.h"

@interface ImageCaleryCell () <FGalleryPhotoViewDelegate>
@property (nonatomic) IBOutlet FGalleryPhotoView        *GaleryPhoto;
@property (nonatomic) IBOutlet UIActivityIndicatorView  *ActivityIndicator;
@property (strong)             Media                    *media;

@end

@implementation ImageCaleryCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self inicializar];
    }
    return self;
}

-(void)inicializar
{
    self.GaleryPhoto.imageView.image = nil;
    self.ActivityIndicator.hidden             = false;
    self.ActivityIndicator.hidesWhenStopped   = true;
    [self.ActivityIndicator startAnimating];
    self.GaleryPhoto = [self.GaleryPhoto initWithFrame:self.frame];
    self.GaleryPhoto.backgroundColor = [Locator Default].Color.Clear;
    self.GaleryPhoto.photoDelegate = self;
}

+(NSString*) getReuseIdentifier
{
    return @"Cell_ImageGalery";
}

-(void)ConfigurarImagen:(Media*)pMedia
{
    [self inicializar];
    self.media = pMedia;
    
    [UIImage ImageAssyncFromStringUrl:self.media.UrlPath Grupo:[ImageCaleryCell getReuseIdentifier] Result:^(UIImage *pResult)
     {
         [self.GaleryPhoto resetZoom];
         self.GaleryPhoto.imageView.image = pResult;
         [self.ActivityIndicator stopAnimating];
     }];
    
}


@end
