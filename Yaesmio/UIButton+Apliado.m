//
//  UIButton+Apliado.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 28/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "UIButton+Apliado.h"
#import "UIImage+Ampliado.h"

@implementation UIButton (Apliado)

-(void) cargarFondoUrlImagenAsincrono:(NSURL*)pURl Grupo:(NSString*)pGrupo
{
    UIViewContentMode pActua = self.contentMode;
    self.contentMode = UIViewContentModeCenter;
    //self.BackgroundImageAllStates = [UIImage imageNamed:@"loadin.png"];
    [UIImage ImageAssyncFromUrl:pURl Grupo:pGrupo Result:^(UIImage *pResult)
     {
         self.BackgroundImageAllStates = pResult;
         self.contentMode = pActua;
     }];
}

-(void) cargarFondoStringUrlImagenAsincrono:(NSString*)pUrl Grupo:(NSString*)pGrupo
{
    [self cargarFondoUrlImagenAsincrono:[NSURL URLWithString:pUrl] Grupo:pGrupo];
}


-(NSString*) TextStateNormal
{
    return [self titleForState:UIControlStateNormal];
}

-(void) setTextStateNormal:(NSString *)pTextStateNormal
{
    [self setTitle:pTextStateNormal forState:UIControlStateNormal];
}

-(NSAttributedString*)AttributedTextStateNormal
{
    return [self attributedTitleForState:UIControlStateNormal];
}

-(void)setAttributedTextStateNormal:(NSAttributedString*)pAtributed
{
    [self setAttributedTitle:pAtributed forState:UIControlStateNormal];
}

-(UIImage*)ImageStateNormal
{
    return [self imageForState:UIControlStateNormal];
}

-(void) setImageStateNormal:(UIImage *)pImageStateNormal
{
    [self setImage:pImageStateNormal forState:UIControlStateNormal];
}

-(void) setBackgroundImageAllStates:(UIImage *)pImage
{
    UIEdgeInsets insets = UIEdgeInsetsMake(pImage.size.height/2, pImage.size.width/2, pImage.size.height/2, pImage.size.width/2);
    UIImage * lImage = [pImage resizableImageWithCapInsets:insets];
    UIImage *lImageSelected = [pImage resizableImageWithCapInsets:insets];
    [self setBackgroundImage:lImage forState:UIControlStateNormal];
    // Even doing the following results in the same behaviour
    [self setBackgroundImage:lImageSelected forState:UIControlStateHighlighted];
}

-(UIImage*) BackgroundImageAllStates
{
    return [self imageForState:UIControlStateNormal];
}

@end
