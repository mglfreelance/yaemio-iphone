//
//  CrearDireccionEnvioViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import "MantenimientoDireccionEnvioViewController.h"
#import "UIAllControls.h"

@interface MantenimientoDireccionEnvioViewController ()

@property (nonatomic) IBOutlet UIScrollView *ScrollViewDatos;
@property (nonatomic) IBOutlet MKGradientButton *btnGuardarDireccion;
@property (nonatomic) IBOutlet UITextField *txtDireccion;
@property (nonatomic) IBOutlet UITextField *txtNumero;
@property (nonatomic) IBOutlet UITextField *txtBloqueEscalera;
@property (nonatomic) IBOutlet UITextField *txtPiso;
@property (nonatomic) IBOutlet UITextField *txtPuerta;
@property (nonatomic) IBOutlet UITextField *txtCodigoPostal;
@property (nonatomic) IBOutlet UIButton *btnPais;
@property (nonatomic) IBOutlet UIButton *btnProvincia;
@property (nonatomic) IBOutlet UITextField *txtTelefono;
@property (nonatomic) IBOutlet UITextField *txtPoblacion;
@property (nonatomic) IBOutlet UITextField *txtEscalera;
@property (nonatomic) IBOutlet UITextView *txtComentarios;
@property (nonatomic) IBOutlet UILabel *labelDireccion;
@property (nonatomic) IBOutlet UILabel *labelDescripcionDireccionEnvio;

@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCamposTraduccion;

- (IBAction)btn_Provincia_click:(id)sender;
- (IBAction)btn_Pais_click:(id)sender;
- (IBAction)btnGuardarDireccionEnvio_click:(id)sender;

@property (nonatomic) ScrollManager *manager;
@property (strong)    Pais *PaisSeleccionado;
@property (strong)    Provincia *ProvinciaSeleccionada;

@end

@implementation MantenimientoDireccionEnvioViewController

NSString *mTextoErr = @"";

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaCamposTraduccion);
    
    self.PaisSeleccionado = nil;
    self.ProvinciaSeleccionada  = nil;
    
    self.labelDescripcionDireccionEnvio.text = [self obtenerDescripcionSegunTipo];
    
    [self.btnPais setBackgroundImage:[Locator Default].Imagen.FondoBotonSeleccionBlanco forState:UIControlStateNormal];
    [self.btnProvincia setBackgroundImage:[Locator Default].Imagen.FondoBotonSeleccionBlanco forState:UIControlStateNormal];
    
    self.manager = [ScrollManager NewAndConfigureWithScrollView:self.ScrollViewDatos AndDidEndTextField:self Selector:@selector(btnGuardarDireccionEnvio_click:)];
    [self CargarDatosDireccion];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:self.EsVistaCarrito];
}

- (void)viewUnload
{
    [self setTxtDireccion:nil];
    [self setTxtNumero:nil];
    [self setTxtBloqueEscalera:nil];
    [self setTxtPiso:nil];
    [self setTxtPuerta:nil];
    [self setTxtCodigoPostal:nil];
    [self setBtnPais:nil];
    [self setBtnProvincia:nil];
    [self setTxtPoblacion:nil];
    [self setTxtTelefono:nil];
    [self setManager:nil];
    [self setPaisSeleccionado:nil];
    [self setProvinciaSeleccionada:nil];
    [self setManager:nil];
    [self setTxtEscalera:nil];
    [self setTxtComentarios:nil];
    [self setListaCamposTraduccion:nil];
    [self setLabelDireccion:nil];
    [self setManager:nil];
    [self setPaisSeleccionado:nil];
    [self setProvinciaSeleccionada:nil];
}

#pragma mark- Eventos Privados

- (IBAction)btn_Provincia_click:(id)sender
{
    if (self.PaisSeleccionado == nil)
    {
        [[Locator Default].Alerta showMessageAcept:Traducir(@"Por favor, seleccione un Pais para poder mostrar sus provincias.")];
    }
    else
    {
        [[Locator Default].Vistas MostrarSeleccionProvincia:self Pais:self.PaisSeleccionado ProvinciaSeleccionada:self.ProvinciaSeleccionada MostrarTodas:false MostrarCercaDeMi:false Resultado:^(Provincia *pResult)
         {
             if (pResult != nil)
             {
                 self.ProvinciaSeleccionada = pResult;
                 self.btnProvincia.TextStateNormal = self.ProvinciaSeleccionada.Nombre;
             }
         }];
    }
}

- (IBAction)btn_Pais_click:(id)sender
{
    [[Locator Default].Vistas MostrarSeleccionPais:self Tipo:TipoListaPaisesGenerica PaisSeleccionado:self.PaisSeleccionado Resultado:^(Pais *pResult)
     {
         if (pResult != nil)
         {
             self.PaisSeleccionado = pResult;
             self.btnPais.TextStateNormal = self.PaisSeleccionado.Nombre;
             self.ProvinciaSeleccionada = nil;
             self.btnProvincia.TextStateNormal = @"";
         }
     }];
}

- (IBAction)btnGuardarDireccionEnvio_click:(id)sender
{
    mTextoErr = @"";
    
    if ([self ComprovarCamposDireccion])
    {
        switch (self.TipoDireccion)
        {
            case DireccionEnvioModoAlternativa:
                [self GuardarDireccionAlternativa];
                break;
                
            case DireccionEnvioModoPrincipal:
                [self GuardarDireccionPrincipal];
                break;
                
            case DireccionEnvioModoSoloEnvio:
                [self GuardarDireccionSoloEnvio];
                break;
        }
    }
    else
    {
        [[Locator Default].Alerta showMessageAcept:[Traducir(@"Los datos no son correctos, por favor, revise:\n@") stringByReplacingOccurrencesOfString:@"@" withString:mTextoErr]];
        mTextoErr = @"";
    }
}

#pragma mark- Metodos Privados

- (ElementoPicker *)ObtenerElementoPorID:(NSArray *)pLista ID:(NSString *)pID
{
    ElementoPicker *lResultado = nil;
    
    if ([pID isEqualToString:@""] == false)
    {
        for (ElementoPicker *lProvincia in pLista)
        {
            if ([lProvincia.Codigo isEqualToString:pID])
            {
                lResultado = lProvincia;
                break;
            }
        }
    }
    
    return lResultado;
}

- (BOOL)ComprovarCamposDireccion
{
    if (self.PaisSeleccionado == nil)
    {
        mTextoErr = Traducir(@"No ha seleccionado Pais.");
    }
    else if (self.ProvinciaSeleccionada == nil)
    {
        mTextoErr = Traducir(@"No ha seleccionado Provincia.");
    }
    else if ([self.txtDireccion.text isEqualToString:@""])
    {
        mTextoErr = Traducir(@"No ha escrito direccion.");
    }
    else if ([self.txtCodigoPostal.text isEqualToString:@""])
    {
        mTextoErr = Traducir(@"No ha escrito Codigo Postal.");
    }
    else if ([self.txtPoblacion.text isEqualToString:@""])
    {
        mTextoErr = Traducir(@"No ha escrito Poblacion");
    }
    else if (self.txtComentarios.text.length > 50)
    {
        mTextoErr = [NSString stringWithFormat:Traducir(@"El campo %@ no admite mas de %@ caracteres"), Traducir(@"Comentarios:"), @"50"];
    }
    else if (self.txtNumero.text.length > 10)
    {
        mTextoErr = [NSString stringWithFormat:Traducir(@"El campo %@ no admite mas de %@ caracteres"), Traducir(@"Nº:"), @"10"];
    }
    else if (self.txtCodigoPostal.text.length > 10)
    {
        mTextoErr = [NSString stringWithFormat:Traducir(@"El campo %@ no admite mas de %@ caracteres"), Traducir(@"Codigo Postal:"), @"10"];
    }
    else if (self.txtPuerta.text.length > 10)
    {
        mTextoErr = [NSString stringWithFormat:Traducir(@"El campo %@ no admite mas de %@ caracteres"), Traducir(@"Puerta:"), @"10"];
    }
    else if (self.txtBloqueEscalera.text.length > 10)
    {
        mTextoErr = [NSString stringWithFormat:Traducir(@"El campo %@ no admite mas de %@ caracteres"), Traducir(@"Bloque:"), @"10"];
    }
    else if (self.txtEscalera.text.length > 10)
    {
        mTextoErr = [NSString stringWithFormat:Traducir(@"El campo %@ no admite mas de %@ caracteres"), Traducir(@"Escalera:"), @"10"];
    }
    else if (self.txtPoblacion.text.length > 40)
    {
        mTextoErr = [NSString stringWithFormat:Traducir(@"El campo %@ no admite mas de %@ caracteres"), Traducir(@"Población:"), @"40"];
    }
    else if (self.txtTelefono.text.length > 30)
    {
        mTextoErr = [NSString stringWithFormat:Traducir(@"El campo %@ no admite mas de %@ caracteres"), Traducir(@"Teléfono de contacto:"), @"30"];
    }
    
    return [mTextoErr isEqualToString:@""];
}

- (void)CargarDatosDireccion
{
    self.labelDireccion.text = [self ObtenerNombrePorTipo:self.TipoDireccion];
    
    if (self.Direccion != nil)
    {
        self.txtDireccion.text = self.Direccion.LaDireccion;
        self.txtNumero.text = self.Direccion.Numero;
        self.txtBloqueEscalera.text = self.Direccion.Bloque;
        self.txtPiso.text = self.Direccion.Piso;
        self.txtPuerta.text = self.Direccion.Puerta;
        self.txtCodigoPostal.text = self.Direccion.Cod_postal;
        self.btnPais.TextStateNormal = self.Direccion.Pais;
        self.btnProvincia.TextStateNormal = self.Direccion.Provincia;
        self.txtTelefono.text = self.Direccion.Telefono;
        self.txtPoblacion.text = self.Direccion.Poblacion;
        self.PaisSeleccionado = [Pais newWithId:self.Direccion.PaisID andName:self.Direccion.Pais];
        self.txtEscalera.text = self.Direccion.Escalera;
        self.ProvinciaSeleccionada = [Provincia newWithId:self.Direccion.ProvinciaID andName:self.Direccion.Provincia];
        self.txtComentarios.text = self.Direccion.TextoDescripcion;
        self.btnGuardarDireccion.TextStateNormal = Traducir(@"Guardar direción");
    }
}

- (Direccion *)ObtenerDireccionDeCampos
{
    Direccion *lResultado = [Direccion new];
    
    if ([self ComprovarCamposDireccion])
    {
        lResultado.LaDireccion = self.txtDireccion.text;
        lResultado.Numero = self.txtNumero.text;
        lResultado.Bloque = self.txtBloqueEscalera.text;
        lResultado.Piso = self.txtPiso.text;
        lResultado.Puerta = self.txtPuerta.text;
        lResultado.Cod_postal = self.txtCodigoPostal.text;
        lResultado.Pais = self.btnPais.TextStateNormal;
        lResultado.Provincia = self.btnProvincia.TextStateNormal;
        lResultado.Telefono = self.txtTelefono.text;
        lResultado.Poblacion = self.txtPoblacion.text;
        lResultado.PaisID = self.PaisSeleccionado.Codigo;
        lResultado.ProvinciaID = self.ProvinciaSeleccionada.Codigo;
        lResultado.TextoDescripcion = self.txtComentarios.text;
        lResultado.Escalera = self.txtEscalera.text;
    }
    
    return lResultado;
}

- (void)CerrarPantalla:(Direccion *)lDirecion
{
    [self.navigationController popViewControllerAnimated:true];
    
    if (self.Resultado != nil)
    {
        self.Resultado(lDirecion);
    }
}

- (void)MostrarMensajeResultadoGuardadoDireccion:(Direccion *)lDirecion pResult:(SRResultMethod)pResult Mensaje:(NSString *)pMensaje
{
    switch (pResult)
    {
        case SRResultMethodErrConex:
            [[Locator Default].Alerta showErrorConexSOAP];
            break;
            
        case SRResultMethodNO:
            [[Locator Default].Alerta showMessageAcept:pMensaje];
            break;
            
        case SRResultMethodYES:
            [[Locator Default].Alerta hideBussy];
            [self CerrarPantalla:lDirecion];
            break;
    }
}

- (void)GuardarDireccionPrincipal
{
    Direccion *lDirecion = [self ObtenerDireccionDeCampos];
    
    if (lDirecion != nil)
    {
        [[Locator Default].Alerta showBussy:Traducir(@"Guardando Dirección.")];
        [[Locator Default].ServicioSOAP ModificarDireccionPrincipal:lDirecion Result:^(SRResultMethod pResult, NSString *pMensaje)
         {
             [self MostrarMensajeResultadoGuardadoDireccion:lDirecion pResult:pResult Mensaje:pMensaje];
         }];
    }
    else
    {
        [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible crear la dirección.")];
    }
}

- (void)GuardarDireccionAlternativa
{
    Direccion *lDirecion = [self ObtenerDireccionDeCampos];
    
    if (lDirecion != nil)
    {
        [[Locator Default].Alerta showBussy:Traducir(@"Guardando Dirección.")];
        [[Locator Default].ServicioSOAP ModificarDireccionAlternativa:lDirecion Result:^(SRResultMethod pResult, NSString *pMensaje)
         {
             [self MostrarMensajeResultadoGuardadoDireccion:lDirecion pResult:pResult Mensaje:pMensaje];
         }];
    }
    else
    {
        [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible crear la dirección.")];
    }
}

- (void)GuardarDireccionSoloEnvio
{
    Direccion *lDirecion = [self ObtenerDireccionDeCampos];
    
    if (lDirecion != nil)
    {
        [self CerrarPantalla:lDirecion];
    }
    else
    {
        [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible crear la dirección.")];
    }
}

- (NSString *)ObtenerNombrePorTipo:(DireccionEnvioModo)pTipo
{
    NSString *result = Traducir(@"Direcciones de envío");
    
    switch (pTipo)
    {
        case DireccionEnvioModoAlternativa:
            result = Traducir(@"Dirección de envío alternativa");
            break;
            
        case DireccionEnvioModoPrincipal:
            result = Traducir(@"Dirección de envío por defecto");
            break;
            
        case DireccionEnvioModoSoloEnvio:
            result = Traducir(@"Dirección solo envío");
            break;
    }
    
    return result;
}

- (NSString *)obtenerDescripcionSegunTipo
{
    switch (self.TipoDireccion)
    {
        case DireccionEnvioModoPrincipal:
            return Traducir(@"Añade aquí la dirección de envío que se marcará como predeterminada en tus compras.");
            
        case DireccionEnvioModoAlternativa:
            return Traducir(@"Añade aquí una dirección de envío como alternativa a la dirección por defecto. Podrás elegirla en el momento de la compra o añadir otra específica para ese envío");
            
        case DireccionEnvioModoSoloEnvio:
            return Traducir(@"Añade aquí una dirección solo para este envío.");
    }
}

@end
