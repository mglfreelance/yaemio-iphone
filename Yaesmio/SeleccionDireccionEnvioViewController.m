//
//  SeleccionDireccionEnvioViewController.m
//  Yaesmio
//
//  Created by freelance on 06/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SeleccionDireccionEnvioViewController.h"
#import "UIAllControls.h"

@interface SeleccionDireccionEnvioViewController ()

@property (weak, nonatomic) IBOutlet UITableViewCell *DireccionPorDefectoCell;
@property (weak, nonatomic) IBOutlet UILabel *labelDireccionPorDefecto;
@property (weak, nonatomic) IBOutlet UIButton *botonNuevaDireccionPorDefecto;

@property (weak, nonatomic) IBOutlet UITableViewCell *DireccionAlternativaCell;
@property (weak, nonatomic) IBOutlet UILabel *labelDireccionAlternativa;
@property (weak, nonatomic) IBOutlet UIButton *botonNuevaDireccionAlternativa;

@property (weak, nonatomic) IBOutlet UITableViewCell *DireccionSoloEnvioCell;
@property (weak, nonatomic) IBOutlet UILabel *labelDireccionSoloEnvio;
@property (weak, nonatomic) IBOutlet UIButton *botonNuevaDireccionSoloEnvio;

@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCamposTraducir;


- (IBAction)NuevaDireccionExecute:(id)sender;


@property (strong) Direccion *DireccionPorDefecto;
@property (strong) Direccion *DireccionAlternativa;
@property (strong) Direccion *DireccionSoloEnvio;
@property (strong) Usuario *Usuario;
@property (copy) ResultadoSeleccionDireccionEnvioBlock Resultado;

@end

@implementation SeleccionDireccionEnvioViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self CargarDireccionesEnvio];
}

- (void)viewUnload
{
    [self setUsuario:nil];
    [self setResultado:nil];
    [self setDireccionPorDefectoCell:nil];
    [self setBotonNuevaDireccionPorDefecto:nil];
    [self setLabelDireccionPorDefecto:nil];
    [self setDireccionAlternativaCell:nil];
    [self setLabelDireccionAlternativa:nil];
    [self setBotonNuevaDireccionSoloEnvio:nil];
    [self setDireccionSoloEnvioCell:nil];
    [self setLabelDireccionSoloEnvio:nil];
    [self setBotonNuevaDireccionAlternativa:nil];
    [self setListaCamposTraducir:nil];
}

- (void)configurarConUsuario:(Usuario *)usuario Resultado:(ResultadoSeleccionDireccionEnvioBlock)resultado
{
    self.Usuario = usuario;
    self.Resultado = resultado;
}

#pragma mark- Eventos Internos

- (IBAction)NuevaDireccionExecute:(id)sender
{
    if (self.DireccionPorDefecto == nil)
    {
        [[Locator Default].Vistas MostrarEditarDireccionEnvioPrincipal:self Direccion:nil EsVistaCarrito:true Result: ^(Direccion *pResultado)
         {
             self.DireccionPorDefecto = pResultado;
             self.Usuario.ExisteDireccion1 =(self.DireccionPorDefecto != nil);
             [self MostrarDireccionesEnvio];
         }];
    }
    else if (self.DireccionAlternativa == nil)
    {
        [[Locator Default].Vistas MostrarEditarDireccionEnvioAlternativa:self Direccion:nil EsVistaCarrito:true Result: ^(Direccion *pResultado)
         {
             self.DireccionAlternativa = pResultado;
             self.Usuario.ExisteDireccion2 =(self.DireccionAlternativa != nil);
             [self MostrarDireccionesEnvio];
         }];
    }
    else
    {
        [[Locator Default].Vistas MostrarEditarDireccionEnvioSoloEnvio:self Direccion:self.DireccionSoloEnvio Result: ^(Direccion *pResult)
         {
             self.DireccionSoloEnvio = pResult;
             [self MostrarDireccionesEnvio];
         }];
    }
}

#pragma mark- Delegate UITableView

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Direccion *dire = [self getDireccionWithIndexPath:indexPath];
    
    if (dire != nil && self.Resultado != nil)
    {
        self.Resultado(dire);
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    return ([self getDireccionWithIndexPath:indexPath] != nil);
}

#pragma  mark- Privado

- (void)CargarDireccionesEnvio
{
    self.DireccionAlternativaCell.hidden = true;
    self.DireccionPorDefectoCell.hidden = true;
    self.DireccionSoloEnvioCell.hidden = true;
    
    if (self.Usuario.ExisteDireccion1)
    {
        [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo tus direcciones de envio.")];
        [[Locator Default].ServicioSOAP ObtenerDirecionPrincipalUsuario: ^(SRResultMethod pResult, Direccion *pDireccion)
         {
             switch (pResult)
             {
                 case SRResultMethodErrConex:
                     [self MostrarErrorGrave];
                     break;
                     
                 case SRResultMethodNO:
                     [self MostrarDireccionesEnvio];
                     break;
                     
                 case SRResultMethodYES:
                     self.DireccionPorDefecto = pDireccion;
                     
                     if (pDireccion != nil && self.Usuario.ExisteDireccion2)
                     {
                         [[Locator Default].ServicioSOAP ObtenerDirecionAlternativaUsuario: ^(SRResultMethod pResultAlternativa, Direccion *pAlternativa)
                          {
                              switch (pResultAlternativa)
                              {
                                  case SRResultMethodErrConex:
                                      [self MostrarErrorGrave];
                                      break;
                                      
                                  default:
                                      self.DireccionAlternativa = pAlternativa;
                                      [self MostrarDireccionesEnvio];
                                      break;
                              }
                          }];
                     }
                     else
                     {
                         [self MostrarDireccionesEnvio];
                     }
                     
                     break;
             }
         }];
    }
    else
    {
        [self MostrarDireccionesEnvio];
    }
}

- (void)MostrarErrorGrave
{
    [[Locator Default].Alerta showErrorConexSOAP: ^{
        [self.navigationController popViewControllerAnimated:true];
    }];
}

- (void)MostrarDireccionesEnvio
{
    [[Locator Default].Alerta hideBussy];
    
    self.botonNuevaDireccionSoloEnvio.AttributedTextStateNormal = [[Locator Default].FuenteLetra getTextoSubrayado:Traducir(@"Agregar una dirección nueva")];
    self.botonNuevaDireccionPorDefecto.AttributedTextStateNormal = [[Locator Default].FuenteLetra getTextoSubrayado:Traducir(@"Agregar una dirección nueva")];
    self.botonNuevaDireccionAlternativa.AttributedTextStateNormal = [[Locator Default].FuenteLetra getTextoSubrayado:Traducir(@"Agregar una dirección nueva")];
    
    [self RefrescarDatosDireccion:self.DireccionPorDefecto Label:self.labelDireccionPorDefecto Boton:self.botonNuevaDireccionPorDefecto];
    [self RefrescarDatosDireccion:self.DireccionAlternativa Label:self.labelDireccionAlternativa Boton:self.botonNuevaDireccionAlternativa];
    [self RefrescarDatosDireccion:self.DireccionSoloEnvio Label:self.labelDireccionSoloEnvio Boton:self.botonNuevaDireccionSoloEnvio];
    
    self.DireccionPorDefectoCell.hidden = false;
    self.DireccionAlternativaCell.hidden = (self.DireccionPorDefecto == nil);
    self.DireccionSoloEnvioCell.hidden = (self.DireccionAlternativa == nil);
}

- (void)RefrescarDatosDireccion:(Direccion *)Direccion Label:(UILabel *)Label Boton:(UIButton *)Boton
{
    BOOL tieneDireccion = (Direccion != nil);
    
    Boton.hidden = tieneDireccion;
    Label.hidden = !tieneDireccion;
    Label.text = [self obtenerDescripcionDireccion:Direccion];
}

- (NSString *)obtenerDescripcionDireccion:(Direccion *)pDireccion
{
    NSString *Texto  = @"";
    
    if (pDireccion != nil)
    {
        Texto = [NSString stringWithFormat:@"%@ %@, %@ %@ %@ %@\n%@ %@ %@ %@", pDireccion.LaDireccion, pDireccion.Numero, pDireccion.Bloque, pDireccion.Piso, pDireccion.Escalera, pDireccion.Puerta, pDireccion.Cod_postal, pDireccion.Poblacion, pDireccion.Provincia, pDireccion.Pais];
    }
    
    return Texto;
}

- (Direccion *)getDireccionWithIndexPath:(NSIndexPath *)index
{
    switch (index.row)
    {
        case 0:
            return self.DireccionPorDefecto;
            
        case 1:
            return self.DireccionAlternativa;
            
        case 2:
            return self.DireccionSoloEnvio;
    }
    
    return nil;
}

@end
