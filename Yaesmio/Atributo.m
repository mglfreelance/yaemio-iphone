//
//  Atributo.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 31/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "Atributo.h"

@implementation Atributo

- (id)init
{
	if((self = [super init]))
    {
		self.AtributoId = @"";
		self.NombreAtributo = @"";
		self.TraduccionId = @"";
	}
	
	return self;
}

- (id)init:(NSString*)pAtributoID Nombre:(NSString*)pNombreAtributo ValorID:(NSString*)pTraduccionID
{
	if((self = [self init]))
    {
		self.AtributoId = pAtributoID;
		self.NombreAtributo = pNombreAtributo;
		self.TraduccionId = pTraduccionID;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.AtributoId xmlNodeForDoc:node->doc elementName:@"AtributoId" elementNSPrefix:@"tns3"]);
	
	xmlAddChild(node, [self.NombreAtributo xmlNodeForDoc:node->doc elementName:@"NombreAtributo" elementNSPrefix:@"tns3"]);
	
	xmlAddChild(node, [self.TraduccionId xmlNodeForDoc:node->doc elementName:@"TraduccionId" elementNSPrefix:@"tns3"]);
}

+ (Atributo *)deserializeNode:(xmlNodePtr)cur
{
	Atributo *newObject = [Atributo new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
    
    if ([newObject.NombreAtributo isEqualToString:@""])
    {   // si es "" es nil
        newObject = nil;
    }
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"AtributoId"])
    {
        self.AtributoId = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"NombreAtributo"])
    {
        self.NombreAtributo = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"TraduccionId"])
    {
        self.TraduccionId = [NSString deserializeNode:pobjCur];
    }
}

-(BOOL)comprobarObjeto
{
    return ([self.NombreAtributo isEqualToString:@""] == false);
}

@end
