//
//  CompraViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EmpresaCarrito.h"
#import "YaesmioViewControllers.h"

@interface CompraViewController : YaesmioViewController

-(void)ConfigurarConCarrito:(EmpresaCarrito*)carrito Direccion:(Direccion*)direccion Usuario:(Usuario*)usuario Resultado:(ResultadoPorDefectoBlock)resultado;

@end
