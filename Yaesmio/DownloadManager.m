//
//  NSURL+Ampliado.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "DownloadManager.h"
#import "PreDownloadFile.h"

@interface DownloadManager ()

@property (strong) NSMutableDictionary *ListaPreDownloadFiles;
@property (strong) PreDownloadFile     *ActualFile;
@property (strong) PreDownloadFile     *ActualFile2;

@end

@implementation DownloadManager

- (void)RequestAssync:(NSURL *)pURL Type:(DownloadManagerTypeFile)pType Grupo:(NSString*)pGrupo ResultImage:(getAssyncUrlFileWithUrlBlock)pResultImage
{
    [self RequestUrl:pURL ResultFile:pResultImage ResultData:nil Type:pType Grupo:pGrupo];
}

- (void)RequestAssync:(NSURL *)pURL Type:(DownloadManagerTypeFile)pType Grupo:(NSString*)pGrupo ResultData:(getAssyncDataWithUrlBlock)pResultData
{
    [self RequestUrl:pURL ResultFile:nil ResultData:pResultData Type:pType Grupo:pGrupo];
}

+(void)ClearAssyncFileCacheLastWeek
{
    dispatch_async( dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSString *tempPath = [DownloadManager getPathImagesCache];
        NSError *lerr;
        NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:tempPath error:&lerr];
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSDate *now = [[NSDate new] dateByAddingTimeInterval:60*60*24*-7];
        
        if (dirContents)
        {
            for (int i = 0; i < [dirContents count]; i++)
            {
                NSString *contentsOnly = [NSString stringWithFormat:@"%@/%@", tempPath, [dirContents objectAtIndex:i]];
                if (CACHEAR_IMAGENES == false || [DownloadManager isFileOld:fileManager File:contentsOnly Date:now])
                {
                    [fileManager removeItemAtPath:contentsOnly error:&lerr];
                    NSLog(@"delete %@ file->%@",lerr==nil?@"OK":@"KO", contentsOnly);
                }
            }
        }
        
        dispatch_async( dispatch_get_main_queue(), ^{
            // Add code here to update the UI/send notifications based on the
            // results of the background processing
        });
    });
    
}

-(void)ClearCacheImages
{
    NSString *tempPath = [DownloadManager getPathImagesCache];
    NSError *lerr;
    NSArray *dirContents = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:tempPath error:&lerr];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (dirContents)
    {
        for (int i = 0; i < [dirContents count]; i++)
        {
            NSString *contentsOnly = [NSString stringWithFormat:@"%@/%@", tempPath, [dirContents objectAtIndex:i]];
            [fileManager removeItemAtPath:contentsOnly error:&lerr];
            NSLog(@"delete %@ file->%@",lerr==nil?@"OK":@"KO", contentsOnly);
        }
    }
}

-(id)init
{
    self = [super init];
    
    if (self)
    {
        self.ListaPreDownloadFiles =[NSMutableDictionary new];
        self.ActualFile = nil;
    }
    
    return self;
}

-(void)CancelAllDownloads:(NSString*)pGrupo
{
    @synchronized(self)
    {
        self.CanceledActualDonwload = true;
        [self CancelarDownloadActual:self.ActualFile Grupo:pGrupo];
        self.CanceledActualDonwload = true;
        [self CancelarDownloadActual:self.ActualFile2 Grupo:pGrupo];
        self.CanceledActualDonwload = true;
        for (PreDownloadFile *pFile in self.ListaPreDownloadFiles.allValues)
        {
            self.CanceledActualDonwload = true;
            if (pGrupo == nil || [pFile.Grupo isEqualToString:pGrupo])
            {
                [self.ListaPreDownloadFiles removeObjectForKey:pFile.ID];
            }
        }
    }
    self.CanceledActualDonwload = false;
    [self NextImageFronStack]; // si existen que siga haciendo download
}

#pragma mark - PRIVADO

- (void)CallResultFile:(PreDownloadFile *)pResult
{
    if (pResult.DownloadCorrect ==  false)
    {
        pResult.FilePath = nil;
        pResult.Data = nil;
    }
    [self CallResponse:pResult.FilePath Data:pResult.Data ResultFile:pResult.FileBlockResult ResultData:pResult.DataBlockResult];
}

- (void)RequestUrl:(NSURL *)pURL ResultFile:(getAssyncUrlFileWithUrlBlock)pResultImage ResultData:(getAssyncDataWithUrlBlock) pResultData Type:(DownloadManagerTypeFile)pType Grupo:(NSString*)pGrupo
{
    NSString *filePath=[self getFilePath:pURL Type:pType];
    //NSLog(@"imageURL:%@",filePath);
    
    if ([DownloadManager ExisteImagenCacheada:filePath])
    {
        // ya esta cacheado llamar directamente a los resultBlock
        //NSLog(@"imageCache:%@",filePath);
        NSData *lData = [NSMutableData dataWithData:[[NSFileManager defaultManager] contentsAtPath:filePath]];
        [self CallResponse:filePath Data:lData ResultFile:pResultImage ResultData:pResultData];
    }
    else if (self.ActualFile == nil)
    {
        self.CanceledActualDonwload = false;
        self.ActualFile = [[PreDownloadFile alloc] initWithURL:pURL Type:pType FilePath:filePath Grupo:pGrupo FileBlock:pResultImage DataBlock:pResultData];
        
        [self.ActualFile InitDownloadWithResult:^(PreDownloadFile *pResult)
         {
             [self CallResultFile:pResult];
             self.ActualFile = nil;
         }];
    }
    else if (self.ActualFile2 == nil)
    {
        self.CanceledActualDonwload = false;
        self.ActualFile2 = [[PreDownloadFile alloc] initWithURL:pURL Type:pType FilePath:filePath Grupo:pGrupo FileBlock:pResultImage DataBlock:pResultData];
        
        [self.ActualFile2 InitDownloadWithResult:^(PreDownloadFile *pResult)
         {
             [self CallResultFile:pResult];
             self.ActualFile2 = nil;
         }];
    }
    else
    {
        // ya existe uno download asi que lo pongo en la cola
        [self AgregateStackGetImage:[[PreDownloadFile alloc] initWithURL:pURL Type:pType FilePath:filePath Grupo:pGrupo FileBlock:pResultImage DataBlock:pResultData]];
    }
    
}

+(NSString*) getPathImagesCache
{
    NSString *lstrResultado = [NSTemporaryDirectory() stringByAppendingPathComponent:@"fileURLCache"];
    
    /* check for existence of cache directory */
    if ([self ExisteImagenCacheada:lstrResultado] == false)
    {
        NSError *error= nil;
        /* create a new cache directory */
        [[NSFileManager defaultManager] createDirectoryAtPath:lstrResultado withIntermediateDirectories:NO attributes:nil error:&error];
    }
    
    return lstrResultado;
}

- (void)CallResponse:(NSString*)pUrl Data:(NSData*)pDataImage ResultFile:(getAssyncUrlFileWithUrlBlock)pResultImage ResultData:(getAssyncDataWithUrlBlock) pResultData
{
    if (self.CanceledActualDonwload == false)
    {
        if (pResultImage != nil)
        {
            pResultImage([NSURL fileURLWithPath:pUrl]);
        }
        
        if (pResultData != nil)
        {
            pResultData(pDataImage);
        }
    }
    
    [self NextImageFronStack];
}

+(BOOL) ExisteImagenCacheada:(NSString*)pNombreImagen
{
    return [[NSFileManager defaultManager] fileExistsAtPath:pNombreImagen];
}

-(void)AgregateStackGetImage:(PreDownloadFile*)pFile
{
    @synchronized(self)
    {
        // NSLog(@"%@",pURL.absoluteString);
        if (self.ListaPreDownloadFiles[pFile.ID] == nil)
        {
            [self.ListaPreDownloadFiles setObject:pFile forKey:pFile.ID];
        }
    }
}

-(void) NextImageFronStack
{
    if (self.ListaPreDownloadFiles.count >0)
    {
        @synchronized(self.ListaPreDownloadFiles)
        {
            NSString *lkey = self.ListaPreDownloadFiles.allKeys[0];
            PreDownloadFile *file = self.ListaPreDownloadFiles[lkey];
            [self.ListaPreDownloadFiles removeObjectForKey:lkey];
            
            if (file != nil)
            {
                [self RequestUrl:file.Url ResultFile:file.FileBlockResult ResultData:file.DataBlockResult Type:file.FileType Grupo:file.Grupo];
            }
        }
    }
}

+(BOOL) isFileOld:(NSFileManager*)pFileManager File:(NSString*)pfile Date:(NSDate*)pDate
{
    BOOL result = false;
    NSError *myErr;
    NSDictionary* attrs = [pFileManager attributesOfItemAtPath:pfile error:&myErr];
    
    NSDate *date = (NSDate*)[attrs objectForKey: NSFileCreationDate];
    
    if (date != nil)
    {
        result = ([date compareIgnoreTime:pDate]== NSOrderedDescending);
    }
    
    return result;
}

-(NSString*) getFileNameCacheUrl:(NSURL*)pURL Type:(DownloadManagerTypeFile)pTipe
{
    return [self getFilePath:pURL Type:pTipe];
}

- (NSString *)getFilePath:(NSURL *)pURL Type:(DownloadManagerTypeFile)pType
{
    NSString *dataPath = [DownloadManager getPathImagesCache];
    NSString *fileName = [[pURL path] lastPathComponent];
    if ([fileName hasSuffix:@".ashx"])
    {   // es un servicio no tengo el nombre correcto de fichero
        fileName = [NSString stringWithFormat:@"%@",[pURL.query stringByReplacingOccurrencesOfString:@"=" withString:@""]];
    }
    if ([fileName isEqualToString:@""])
    {  // el nombre de fichero no es correcto
        NSDateFormatter *gmtFormatter = [[NSDateFormatter alloc] init];
        [gmtFormatter setDateFormat:@"yyyyDDDHHmmss"];
        fileName = [gmtFormatter stringFromDate:[NSDate new]];
    }
    
    fileName = [NSString stringWithFormat:@"%@%@",fileName, [self getNameFileForType:pType]];
    
    return [dataPath stringByAppendingPathComponent:fileName];
}

-(NSString*)getNameFileForType:(DownloadManagerTypeFile)pType
{
    NSString * resul;
    
    switch (pType)
    {
        case DownloadManagerTypeFileImage:
            resul = @".png";
            break;
            
        case DownloadManagerTypeFileAudio:
            resul = @".wav";
            break;
            
        case DownloadManagerTypeFileOther:
            resul = @"";
            break;
    }
    
    return resul;
}

-(void)CancelarDownloadActual:(PreDownloadFile*)pFile Grupo:(NSString*)pGrupo
{
    if (pFile != nil && (pGrupo == nil || [pFile.Grupo isEqualToString:pGrupo]))
    {
        pFile.DataBlockResult = nil;
        pFile.FileBlockResult = nil;
    }
}

@end
