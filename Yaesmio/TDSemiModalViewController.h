//
//  TDSemiModalViewController.h
//  TDSemiModal
//
//  Created by Nathan  Reed on 18/10/10.
//  Copyright 2010 Nathan Reed. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YaesmioViewControllers.h"

@interface TDSemiModalViewController : YaesmioViewController

typedef NS_ENUM(NSInteger, TDSemiModalExtensionEnum)
{
    TDSemiModalExtensionEnumButtonFlip       = 1, // por defecto
    TDSemiModalExtensionEnumAparece  = 2, // degradado
};

@property (nonatomic, strong) UIView *coverView;
@property (assign) TDSemiModalExtensionEnum typeAnimation;

@end
