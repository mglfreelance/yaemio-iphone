//
//  EnviarNotificacionViewController.h
//  Yaesmio
//
//  Created by freelance on 21/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "YaesmioViewControllers.h"
#import "ProductoPadre.h"

@interface EnviarNotificacionViewController : YaesmioViewController

@property (strong) ProductoPadre *Producto;

@end
