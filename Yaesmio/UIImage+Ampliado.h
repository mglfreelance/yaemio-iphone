//
//  UIImage+Ampliado.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 25/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Ampliado)

typedef void (^getAssyncImageWithUrlBlock)(UIImage *pResult);

-(UIImage *)scaleToWidth:(CGFloat)width;
-(UIImage *)scaleToMaxWidth:(CGFloat)width AndMaxHeight:(CGFloat) height;

+(void)ImageAssyncFromUrl:(NSURL*)pUrlImage Grupo:(NSString*)pGrupo Result:(getAssyncImageWithUrlBlock)pResult;

+(void)ImageAssyncFromStringUrl:(NSString*)pUrlImage Grupo:(NSString*)pGrupo Result:(getAssyncImageWithUrlBlock)pResult;

@end
