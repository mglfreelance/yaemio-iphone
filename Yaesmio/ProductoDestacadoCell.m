//
//  ProductoDestacadoCell.m
//  Yaesmio
//
//  Created by freelance on 21/08/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ProductoDestacadoCell.h"
#import "UIAllControls.h"

@interface ProductoDestacadoCell ()

@property (weak, nonatomic) IBOutlet UIImageView *ImagenProducto;
@property (weak, nonatomic) IBOutlet UILabel *LabelNombreProducto;
@property (weak, nonatomic) IBOutlet UILabel *LabelNombreEmpresa;
@property (weak, nonatomic) IBOutlet UILabel *labelMoneda;
@property (weak, nonatomic) IBOutlet UILabel *LabelPrecio;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *IndicatorBusy;
@property (weak, nonatomic) IBOutlet UIImageView *ImagenRecomendado;
@property (weak, nonatomic) IBOutlet UIView *viewDescuento;
@property (weak, nonatomic) IBOutlet UILabel *labelDescuento;
@property (weak, nonatomic) IBOutlet UIView *viewImagen;
@property (weak, nonatomic) IBOutlet UIView *VistaProducto;
@property (weak, nonatomic) IBOutlet UILabel *labelTantoPorciento;

@property (strong) NSString *IdImagen;

@end

@implementation ProductoDestacadoCell

- (void)ActualizarConProducto:(ProductoDestacado *)pProducto
{
    [self prepareForReuse];
    
    [self.ImagenProducto cargarUrlImagenAsincrono:pProducto.Imagen.UrlPath Busy:self.IndicatorBusy];
    self.LabelNombreProducto.text = pProducto.Nombre;
    self.LabelNombreEmpresa.text  = pProducto.NombreEmpresa;
    self.labelMoneda.text = pProducto.AbreviaturaMoneda;
    self.LabelPrecio.text = [pProducto.Precio toStringFormaterCurrency];
    self.labelDescuento.text = [pProducto.Descuento toStringDiscount];
    self.labelMoneda.hidden = (pProducto.Precio.integerValue == 0);
    self.LabelPrecio.hidden = (pProducto.Precio.integerValue == 0);
    self.ImagenRecomendado.hidden = !pProducto.Recomendada;
    self.viewDescuento.hidden = (pProducto.Precio.isZeroOrLess);
    
    if(pProducto.Filas>1 && pProducto.Columnas >1)
    {
        self.labelDescuento.font = [[Locator Default].FuenteLetra FuenteNegrita:25.0];
        self.labelTantoPorciento.font = [[Locator Default].FuenteLetra FuenteNormal:20.0];
        [self.viewDescuento mgSetSizeX:UNCHANGED andY:UNCHANGED andWidth:65.0 andHeight:37.0];
    }
    else
    {
        self.labelDescuento.font = [[Locator Default].FuenteLetra FuenteNegrita:16.0];
        self.labelTantoPorciento.font = [[Locator Default].FuenteLetra FuenteNormal:13.0];
        [self.viewDescuento mgSetSizeX:UNCHANGED andY:UNCHANGED andWidth:45.0 andHeight:25.0];
    }
    [self.labelDescuento mgSizeFitsMaxWidth:MAXFLOAT MaxHeight:MAXFLOAT];
    [self.labelTantoPorciento mgSizeFitsMaxWidth:MAXFLOAT MaxHeight:MAXFLOAT];
    
    [self.labelDescuento mgMoveInSuperView:MGPositionInViewToCenterVertical AgregateHorizontal:0.0 AgregateVertical:0.0];
    [self.labelTantoPorciento mgMoveInSuperView:MGPositionInViewToRight AgregateHorizontal:2.0 AgregateVertical:0.0];
    [self.labelDescuento mgMoveToControl:self.labelTantoPorciento Position:MGPositionMoveToBefore Separation:0.7];
    [self.labelTantoPorciento mgMoveToControl:self.labelDescuento Position:MGPositionMoveToBotton Separation:1.0];
    
    [self.viewDescuento mgMoveInSuperView:MGPositionInViewToRight AgregateHorizontal:-6.0 AgregateVertical:0.0];
 }

- (void)prepareForReuse
{
    self.VistaProducto.layer.borderWidth = 1.f;
    self.VistaProducto.layer.borderColor = [UIColor whiteColor].CGColor;
    
    self.ImagenProducto.image = nil;
    self.labelMoneda.text = @"";
    self.LabelNombreEmpresa.text = @"";
    self.LabelNombreProducto.text = @"";
    self.LabelPrecio.text = @"";
    self.ImagenRecomendado.hidden = true;
}


@end
