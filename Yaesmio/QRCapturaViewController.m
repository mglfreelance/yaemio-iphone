//
//  QRCapturaViewController
//  yaesmio
//
//  Created by manuel garcia lopez on 07/05/13.
//  Copyright (c) 2013 manuel garcia lopez. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>
#import "QRCapturaViewController.h"
#import "UIAllControls.h"
#import "ZBarReaderView.h"
#import "ZBarCameraSimulator.h"
#import "ZBarCaptureReader.h"
#import "VcardImporter.h"
#import "UIView+Genie.h"

@interface QRCapturaViewController () < ZBarReaderViewDelegate >

@property (nonatomic) IBOutlet ZBarReaderView *VistaQR;
@property (nonatomic) ZBarCameraSimulator *cameraSim;
@property (nonatomic) IBOutlet UIImageView *ImageFondoCamara;
@property (nonatomic) IBOutlet UILabel *labelAntesImagen;
@property (nonatomic) IBOutlet UIView *VistaAnimada;
@property (nonatomic) IBOutlet UILabel *labelDespuesImagen;
@property (weak, nonatomic) IBOutlet UILabel *labelTitulo;
@property (nonatomic) IBOutlet UIImageView *imgYaesmioAnimado;
@property (nonatomic) IBOutlet UIButton *btnFlash;
@property (nonatomic) IBOutlet UIButton *btnVerQRAnterior;
@property (nonatomic) IBOutlet UIImageView *imgQRcapturado;
@property (nonatomic) IBOutlet UIActivityIndicatorView *controlBusy;

- (IBAction)btnFlash_click:(id)sender;
- (IBAction)btnVerQRacterior_click:(id)sender;
- (IBAction)CerrarModalExecute:(id)sender;

@property (assign) BOOL ActivarLecturaQR;
@property (strong) NSString *QRAnterior;

@end

@implementation QRCapturaViewController

#pragma mark - View Controller Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.labelTitulo);
    self.ActivarLecturaQR = false;
    self.ImageFondoCamara.hidden = true;
    self.VistaAnimada.hidden = true;
    self.VistaQR.readerDelegate = self;
    self.VistaQR.tracksSymbols = false;
    self.imgQRcapturado.hidden = true;
    [self.VistaQR.scanner setSymbology:0 config:ZBAR_CFG_ENABLE to:0];
    [self.VistaQR.scanner setSymbology:ZBAR_QRCODE config:ZBAR_CFG_ENABLE to:1];
    [self.VistaQR mgSetSizeX:UNCHANGED andY:40 andWidth:UNCHANGED andHeight:UNCHANGED];
    [self ComponerTextoImagenTextoAnimado];
    
    self.btnFlash.hidden = ![self.VistaQR.device isTorchModeSupported:AVCaptureTorchModeOn];
    
    if (TARGET_IPHONE_SIMULATOR)
    {
        self.cameraSim = [[ZBarCameraSimulator alloc] initWithViewController:self];
        self.cameraSim.readerView = self.VistaQR;
    }
    
    self.ImageFondoCamara.image =  [self.ImageFondoCamara.image stretchableImageWithLeftCapWidth:0 topCapHeight:0];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(VuelveDeBackground) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    [self.view mgAddGestureRecognizerSwipe:self Action:@selector(SwipeRecognizer:) Direccion:UISwipeGestureRecognizerDirectionLeft];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
    
    //para el primer inicio no muestre captura de QR
    if ([Locator Default].Preferencias.MostrarPantallaQR)
    {
        self.VistaQR.hidden = false;
        self.ImageFondoCamara.hidden = false;
    }
    
    [self iniciarQR];
    
    if ([self.VistaQR.device isTorchModeSupported:AVCaptureTorchModeOn])
    {
        self.VistaQR.device.torchMode = AVCaptureTorchModeOff;
    }
    else
    {
        self.btnFlash.hidden = true;
    }
    
    [self performSelector:@selector(activarCapturaQR) withObject:self afterDelay:2];
    self.btnVerQRAnterior.hidden = (self.QRAnterior == nil || [self.QRAnterior isEqualToString:@""]);
    self.view.userInteractionEnabled = true;
    [self.controlBusy stopAnimating];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [self pararQR];
    self.ActivarLecturaQR = false;
    self.VistaAnimada.hidden = true;
}

- (void)viewUnload
{
    [super viewUnload];
    
    self.cameraSim = nil;
    self.QRAnterior = nil;
}

#pragma mark - Private Methods

- (void)iniciarQR
{
    [self.VistaQR start];
    //    unsigned int versionmayor = 0;
    //    unsigned int versionmenor = 0;
    //    zbar_version(&versionmayor, &versionmenor);
    //    NSLog(@"version %d - %d",versionmayor, versionmenor);// ver 0.11
}

- (void)pararQR
{
    [self.VistaQR stop];
}

- (void)VuelveDeBackground
{
    self.VistaAnimada.hidden = true;
    [self MostrarMensajeAnimado];
}

#pragma mark - Delegate ZBarReaderView

- (void)readerView:(ZBarReaderView *)view didReadSymbols:(ZBarSymbolSet *)syms fromImage:(UIImage *)img
{
    if (self.ActivarLecturaQR)
    {
        // do something useful with results
        for (ZBarSymbol *sym in syms)
        {
            //NSLog(@"%@",  sym.data);
            
            if (sym.type == ZBAR_QRCODE)
            {
                [self ObtenerProductoDeQR:sym.data MostrarAnimacion:true];
                break;
            }
        }
    }
    else
    {
        [self.VistaQR flushCache];
    }
}

#pragma mark- Gestion QR

- (void)ObtenerProductoDeQR:(NSString *)pQR MostrarAnimacion:(BOOL)pMostrarAnimacion
{
    self.view.userInteractionEnabled = false;
    self.QRAnterior = pQR;
    [self OcultarVistaAnimada];
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
    if (pMostrarAnimacion)
    {
        [self MostrarAnimacionCapturaQR];
    }
    else
    {
        [self.controlBusy startAnimating];
    }
    
    [[Locator Default].GPS getLocation:^(GPSManager pResult, double pLatitud, double pLongitud)
     {
         if (pResult == GPSManagerOK)
         {
             [Locator Default].ServicioSOAP.HeaderLatitud = [NSString stringWithFormat:@"%f", pLatitud];
             [Locator Default].ServicioSOAP.HeaderLongitud = [NSString stringWithFormat:@"%f", pLongitud];
         }
         else
         {
             [Locator Default].ServicioSOAP.HeaderLatitud = @"0";
             [Locator Default].ServicioSOAP.HeaderLongitud = @"0";
         }
         
         // comprueba el QR que exista
         [self CargarProducto:pQR Latitud:[Locator Default].ServicioSOAP.HeaderLatitud Longitud:[Locator Default].ServicioSOAP.HeaderLongitud];
     }];
}

- (void)SwipeRecognizer:(UISwipeGestureRecognizer *)sender
{
    [self btnVerQRacterior_click:nil]; // mostrar qr anterior si existe
}

- (void)MostrarMensajeNoCapturado:(NSString *)pMensajeError
{
    if ([pMensajeError isEqualToString:@""])
    {
        pMensajeError = Traducir(@"El codigo que has capturado ya no esta activo. Informate aqui de como encontrar mas codigos yaesmio!");
    }
    
    [[Locator Default].Alerta showMessageAceptWithTitle:Traducir(@"¡Ups!") AndMessage:pMensajeError ResulBlock:^{
        self.ActivarLecturaQR = true;
        [self MostrarMensajeAnimado];
        [self.controlBusy stopAnimating];
    }];
}

- (void)MostrarMensajeErrorSoap:(SRResultObtenerProducto)pResult
{
    [[Locator Default].Alerta showErrorConexSOAP:^{
        self.ActivarLecturaQR = true;
        [self MostrarMensajeAnimado];
    }];
    [self.controlBusy stopAnimating];
}

- (void)CargarProducto:(NSString *)urlProducto Latitud:(NSString *)pLatitud Longitud:(NSString *)pLongitud
{
    [[Locator Default].ServicioSOAP ObtenerProducto:urlProducto Result:^(SRResultObtenerProducto pResult, NSString *pMensajeError, ProductoPadre *pProductoPadre)
     {
         switch (pResult)
         {
             case SRResultObtenerProductoErrConex:
                 [self MostrarMensajeErrorSoap:pResult];
                 break;
                 
             case SRResultObtenerProductoNoExiste:
                 [self MostrarMensajeNoCapturado:pMensajeError];
                 break;
                 
             case SRResultObtenerProductoNoYaesmio:
                 [self MostrarQRNoYaesmio:urlProducto];
                 break;
                 
             case SRResultObtenerProductoYES:
                 [[Locator Default].Alerta hideBussy];
                 // guardar la latitud y longitud que obtubo al escanear QR
                 pProductoPadre.ProductoHijo.Longitud = pLongitud;
                 pProductoPadre.ProductoHijo.Latitud = pLatitud;
                 [[Locator Default].Vistas MostrarProducto:self Producto:pProductoPadre VieneDeFavoritos:false];
                 break;
         }
     }];
}

- (void)activarCapturaQR
{
    self.ActivarLecturaQR = true;
    [self MostrarMensajeAnimado];
}

- (void)MostrarQRNoYaesmio:(NSString *)pQr
{
    [self.controlBusy stopAnimating];
    
    if ([VcardImporter isFormatVcard:pQr])
    {   // vcard
        VcardImporter *card = [[VcardImporter alloc] init];
        [card parseString:pQr];
        [[Locator Default].Vistas MostrarPantallaContacto:self Datos:card];
    }
    else if ([pQr hasPrefix:@"SMSTO:"])
    {   // envio de sms
        NSArray *upperComponents = [pQr componentsSeparatedByString:@":"];
        
        if (upperComponents.count == 3)
        {
            [[Locator Default].Vistas MostrarPantallaSMS:self Telefono:upperComponents[1] Texto:upperComponents[2]];
        }
    }
    else if ([NSURL extractForString:pQr] != nil)
    {
        [[Locator Default].Vistas MostrarWebView:self url:[NSURL extractForString:pQr].absoluteString];
        [[Locator Default].Alerta hideBussy];
    }
    else
    {
        // si no es cualquier otro lo llevo a safari a una busqueda
        [[Locator Default].Vistas MostrarWebSafari:pQr];
        [[Locator Default].Alerta hideBussy];
    }
    
    [[Locator Default].Alerta hideBussy];
    self.ActivarLecturaQR = true;
}

#pragma mark- Animacion texto

- (void)ComponerTextoImagenTextoAnimado
{
    self.labelAntesImagen.text = Traducir(@"Busca los codigos con");
    [self.labelAntesImagen mgSizeFitsMaxWidth:MAXFLOAT MaxHeight:UNCHANGED];
    [self.labelAntesImagen mgSetSizeX:0 andY:0 andWidth:UNCHANGED andHeight:UNCHANGED];
    
    [self.imgYaesmioAnimado mgMoveToControl:self.labelAntesImagen Position:MGPositionMoveToBehind Separation:5.0];
    
    self.labelDespuesImagen.text = Traducir(@"y encontrarás las mejores ofertas");
    [self.labelDespuesImagen mgSizeFitsMaxWidth:MAXFLOAT MaxHeight:UNCHANGED];
    [self.labelDespuesImagen mgMoveToControl:self.imgYaesmioAnimado Position:MGPositionMoveToBehind Separation:5.0];
    
    [self.VistaAnimada mgSetSizeX:0 andY:0 andWidth:self.labelDespuesImagen.frame.origin.x + self.labelDespuesImagen.frame.size.width andHeight:UNCHANGED];
}

- (void)MostrarMensajeAnimado
{
    if (self.VistaAnimada.hidden == true)
    {
        [self.view.layer removeAllAnimations];
        
        self.VistaAnimada.hidden = true;
        [self.VistaAnimada mgSetSizeX:320 andY:UNCHANGED andWidth:UNCHANGED andHeight:UNCHANGED];
        self.VistaAnimada.hidden = false;
        int duracion = 10;
        
        [UIView animateWithDuration:duracion animations:^{
            [self.VistaAnimada mgSetSizeX:self.VistaAnimada.frame.size.width * (-1) andY:UNCHANGED andWidth:UNCHANGED andHeight:UNCHANGED];
        } completion:^(BOOL finished) {
            if (self.ActivarLecturaQR && finished == true)
            {
                self.VistaAnimada.hidden = true;
                [self performSelector:@selector(MostrarMensajeAnimado) withObject:self afterDelay:1];
            }
        }];
    }
}

- (void)OcultarVistaAnimada
{
    self.VistaAnimada.hidden = true;
    self.ActivarLecturaQR = false;
}

- (IBAction)btnFlash_click:(id)sender
{
    if ([self.VistaQR.device isTorchModeSupported:AVCaptureTorchModeOn])
    {
        if (self.VistaQR.device.torchLevel > 0)
        {
            self.VistaQR.device.torchMode = AVCaptureTorchModeOff;
            self.btnFlash.ImageStateNormal = [Locator Default].Imagen.LedApagado;
        }
        else
        {
            self.VistaQR.device.torchMode = AVCaptureTorchModeOn;
            self.btnFlash.ImageStateNormal = [Locator Default].Imagen.LedEncendido;
        }
    }
    else
    {
        self.btnFlash.hidden = true;
    }
}

- (IBAction)btnVerQRacterior_click:(id)sender
{
    if (self.QRAnterior != nil && [self.QRAnterior isEqualToString:@""] == false)
    {
        [self ObtenerProductoDeQR:self.QRAnterior MostrarAnimacion:false];
    }
}

- (IBAction)CerrarModalExecute:(id)sender
{
    [self CerrarModal];
}

- (void)MostrarAnimacionCapturaQR
{
    self.imgQRcapturado.hidden = false;
    CGRect endRect = CGRectMake(self.btnVerQRAnterior.frame.origin.x, self.btnVerQRAnterior.frame.origin.y + 8, 28, 28);
    [self.imgQRcapturado genieInTransitionWithDuration:0.7 destinationRect:endRect destinationEdge:BCRectEdgeTop completion:^
     {
         self.imgQRcapturado.hidden = true;
         self.btnVerQRAnterior.hidden = false;
         [self.controlBusy startAnimating];
     }];
}

@end
