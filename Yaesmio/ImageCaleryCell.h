//
//  ImageCaleryCell.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Media.h"

@interface ImageCaleryCell : UICollectionViewCell

+(NSString*) getReuseIdentifier;

-(void)ConfigurarImagen:(Media*)pMedia;

@end
