//
//  ScrollManager.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ScrollManager : NSObject

-(ScrollManager*) initWithView:(UIView*)pview andScrollView:(UIScrollView*) pScrollView andTextFields:(NSArray*) pListTextfields;

-(ScrollManager*) initWithView:(UIView*)pview andScrollView:(UIScrollView*) pScrollView;

-(ScrollManager*) initWithScrollView:(UIScrollView*) pScrollView;

+(ScrollManager*) NewAndConfigureWithScrollView:(UIScrollView *)pScrollView AndDidEndTextField:(id)pdelegate Selector:(SEL)pSel;

-(void)ConfigureDidEndTextField:(id)delegate Selector:(SEL) pSel;

//quitar scroll
-(void) ClearScroll;

@end
