//
//  Locator.m
//  yaesmio
//
//  Created by manuel garcia lopez on 06/05/13.
//  Copyright (c) 2013 manuel garcia lopez. All rights reserved.
//

#import "Locator.h"
#import "AppDelegate.h"


@implementation Locator

static Locator *sharedMySingleton = nil;

+ (Locator *)Default
{
    @synchronized(self)
    {
        if (sharedMySingleton == nil)
        {
            sharedMySingleton = [Locator new];
        }
    }
    return sharedMySingleton;
}

ColorManager *mColor;
- (ColorManager *)Color
{
    if (mColor == Nil)
    {
        mColor = [[ColorManager alloc] init];
    }
    
    return mColor;
}

PreferencesManager *mPreferences;
- (PreferencesManager *)Preferencias
{
    if (mPreferences == Nil)
    {
        mPreferences = [[PreferencesManager alloc] init];
    }
    
    return mPreferences;
}

SoundManager *mSoundManager;
- (SoundManager *)Sounds
{
    if (mSoundManager == Nil)
    {
        mSoundManager = [[SoundManager alloc] init];
    }
    
    return mSoundManager;
}

CarritoManager *mCarrito;
- (CarritoManager *)Carrito
{
    if (mCarrito == Nil)
    {
        mCarrito = [[CarritoManager alloc] init];
    }
    
    return mCarrito;
}

ProductosFavoritosManager *mProductosFavoritos;
- (ProductosFavoritosManager *)ProductosFavoritos
{
    if (mProductosFavoritos == Nil)
    {
        mProductosFavoritos = [[ProductosFavoritosManager alloc] init];
    }
    
    return mProductosFavoritos;
}

ImagenManager *mImagen;
- (ImagenManager *)Imagen
{
    if (mImagen == Nil)
    {
        mImagen = [[ImagenManager alloc] init];
    }
    
    return mImagen;
}

ServicioSOAPManager *mSOAP;
- (ServicioSOAPManager *)ServicioSOAP
{
    if (mSOAP == Nil)
    {
        mSOAP = [[ServicioSOAPManager alloc] init];
    }
    
    return mSOAP;
}

FontManager *mFuenteLetra;
- (FontManager *)FuenteLetra
{
    if (mFuenteLetra == Nil)
    {
        mFuenteLetra = [[FontManager alloc] init];
    }
    
    return mFuenteLetra;
}

ExpresionesRegularesManager *mExpresionesRegulares;
- (ExpresionesRegularesManager *)ExpresionesRegulares
{
    if (mExpresionesRegulares == Nil)
    {
        mExpresionesRegulares = [[ExpresionesRegularesManager alloc] init];
    }
    
    return mExpresionesRegulares;
}

IdiomaManager *mIdioma;
- (IdiomaManager *)Idioma
{
    if (mIdioma == Nil)
    {
        mIdioma = [[IdiomaManager alloc] init];
    }
    
    return mIdioma;
}

GeoposicionamientoManager *mGPS;
- (GeoposicionamientoManager *)GPS
{
    if (mGPS == Nil)
    {
        mGPS = [[GeoposicionamientoManager alloc] init];
    }
    
    return mGPS;
}

ViewControllersManager *mVistas;
- (ViewControllersManager *)Vistas
{
    if (mVistas == Nil)
    {
        mVistas = [[ViewControllersManager alloc] init];
    }
    
    return mVistas;
}

DownloadManager *mDownload;
- (DownloadManager *)Download
{
    if (mDownload == Nil)
    {
        mDownload = [[DownloadManager alloc] init];
    }
    
    return mDownload;
}

AlertaManager *mAlertaManager;
- (AlertaManager *)Alerta
{
    if (mAlertaManager == Nil)
    {
        mAlertaManager = [[AlertaManager alloc] init];
    }
    
    return mAlertaManager;
}

DeviceManager *mDeviceManager;
- (DeviceManager *)Device
{
    if (mDeviceManager == Nil)
    {
        mDeviceManager = [[DeviceManager alloc] init];
    }
    
    return mDeviceManager;
}

ContactsManager *mContactos;
- (ContactsManager *)Contactos
{
    if (mContactos == Nil)
    {
        mContactos = [[ContactsManager alloc] init];
    }
    
    return mContactos;
}

-(NSString*)VersionApp
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}

- (NSString *)VersionAndBuid
{
    return [NSString stringWithFormat:@"%@ (%@)", [self VersionApp], [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]];
}

@end
