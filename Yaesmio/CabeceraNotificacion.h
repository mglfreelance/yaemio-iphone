//
//  CabeceraNotificacion.h
//  Yaesmio
//
//  Created by freelance on 16/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"

typedef NS_ENUM (NSInteger, SNTipoNotificacion)
{
    SNTipoNotificacionNinguna    = 0,
    SNTipoNotificacionRecibida   = 2,
    SNTipoNotificacionLeida      = 3,
    SNTipoNotificacionEliminada  = 4,
};

@interface CabeceraNotificacion : SoapObject

@property (strong) NSDate *Fecha;

@property (strong) NSString *Cuerpo;
@property (strong) NSString *Empresa;
@property (strong) NSString *IdProducto;
@property (strong) NSString *idNotificacion;
@property (strong) Media *Imagen;
@property (strong) NSString *MailEmisior;
@property (strong) NSString *Medio;
@property (strong) NSString *Moneda;
@property (strong) NSString *NombreProducto;
@property (strong) NSDecimalNumber *Precio;
@property (strong) NSString *TextoEnvio;
@property (assign) SNTipoNotificacion Tipo;
@property (strong) NSDecimalNumber *Descuento;
@property (assign) BOOL Recomendada;

+ (CabeceraNotificacion *)deserializeNode:(xmlNodePtr)cur;

@end
