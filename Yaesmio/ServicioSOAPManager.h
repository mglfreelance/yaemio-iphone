//
//  ServicioSOAPManager.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 21/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Direccion.h"
#import "Pais.h"
#import "Provincia.h"
#import "MembershipDatosUsuario.h"
#import "Usuario.h"
#import "Producto.h"
#import "ProductoPadre.h"
#import "ValoresdeAtributo.h"
#import "EmpresaCarrito.h"
#import "Favorito.h"
#import "Empresa.h"
#import "GrupoArticulo.h"
#import "FiltroBusquedaProductos.h"
#import "LoginUsuarioResponse.h"
#import "GrupoNotificacion.h"

#pragma mark- Servicio SOAP

typedef NS_ENUM (NSInteger, SRResultMethod)
{
    SRResultMethodYES       = 1,            // todo OK
    SRResultMethodNO        = 2,            // devolucion no OK
    SRResultMethodErrConex  = 0,            // err del servicio
};

@interface ServicioSOAPManager : NSObject

@property (nonatomic, strong) NSString *HeaderLatitud;
@property (nonatomic, strong) NSString *HeaderLongitud;


#pragma mark- Metodo Identificacion Usuario

typedef NS_ENUM (NSInteger, SRResultIdentificacion)
{
    SRResultIdentificacionYES       = SRResultMethodYES,        // todo OK
    SRResultIdentificacionNO        = SRResultMethodNO,         // devolucion no OK
    SRResultIdentificacionInactivo  = 3,                        // el usuario no esta activo todavia
    SRResultIdentificacionBloqueado = 4,                        // el usuario esta bloqueado
    SRResultIdentificacionErrConex  = SRResultMethodErrConex,   // err del servicio
};
typedef void (^IdentificacionUsuarioBlock)(SRResultIdentificacion pResult, LoginUsuarioResponse *pResponse);
- (void)IdentificacionUsuarioWithEmail:(NSString *)email AndPassword:(NSString *)password Result:(IdentificacionUsuarioBlock)pBlock;

#pragma mark- Metodo Recuperar Password

typedef void (^RecuperarPasswordConEmailBlock)(SRResultMethod pResult);
- (void)RecuperarPasswordConEmail:(NSString *)email Result:(RecuperarPasswordConEmailBlock)pBlock;

#pragma mark- Metodo Comprovar email

typedef void (^ComprovarExisteEmailBlock)(SRResultMethod pResult);
- (void)ComprovarExisteEmail:(NSString *)email Result:(ComprovarExisteEmailBlock)pBlock;

#pragma mark- Metodo Consultar Euros Yaesmio

typedef void (^ConsultarEurosYaesmioBlock)(SRResultMethod pResult, NSNumber *pEuros);
- (void)ConsultarEurosYaesmioPorEmail:(NSString *)email Result:(ConsultarEurosYaesmioBlock)pBlock;

#pragma mark- Metodo Devolver Lista Pais

typedef NS_ENUM (NSInteger, SRTipoListaPaisAObtener)
{
    SRTipoListaPaisAObtenerNormal  = 1,
    SRTipoListaPaisAObtenerOtra    = 2,
    SRTipoListaPaisAObtenerCupones = 3,
};
typedef void (^DevolverListaPaisBlock)(SRResultMethod pResult, NSArray *pListaPais);
- (void)DevolverListaPais:(SRTipoListaPaisAObtener)Tipo Resultado:(DevolverListaPaisBlock)pBlock;

#pragma mark- Metodo Devolver Lista Provincia

typedef void (^DevolverListaProvinciaBlock)(SRResultMethod pResult, NSArray *pListaProvincia);
- (void)DevolverListaProvinciaPorPais:(NSString *)pIDPais Result:(DevolverListaProvinciaBlock)pBlock;

#pragma mark- Metodo Crear Usuario

typedef NS_ENUM (NSInteger, SRResultCreateUser)
{
    SRResultCreateUserYES       = 0,
    SRResultCreateUserExists    = 1,
    SRResultCreateUserNO        = 2,
    SRResultCreateUserErrConex  = 99,         // err del servicio
};
typedef void (^CrearUsuarioBlock)(SRResultCreateUser pResult, NSString *MensajeSAP);
- (void)CrearUsuario:(MembershipDatosUsuario *)pUsuario Result:(CrearUsuarioBlock)pBlock;

#pragma mark- Metodo Editar Password Usuario

typedef NS_ENUM (NSInteger, SRResultEditPassword)
{
    SRResultEditPasswordYES                  = SRResultMethodYES,
    SRResultEditPasswordOldPasswordIncorrect = SRResultMethodNO,
    SRResultEditPasswordNewPasswordIncorrect = 3,
    SRResultEditPasswordErrConex             = SRResultMethodErrConex,         // err del servicio
};
typedef void (^EditarPasswordBlock)(SRResultEditPassword pResult);
- (void)EditarPassword:(NSString *)pMailUsuario ConPasswordAntigua:(NSString *)pPasswordAntiguo YPasswordNuevo:(NSString *)pPasswordNuevo Result:(EditarPasswordBlock)pBlock;

#pragma mark- Metodo Modificar Usuario

typedef void (^ModificarUsuarioBlock)(SRResultMethod pResult, NSString *MensajeSAP);
- (void)ModificarUsuario:(Usuario *)pUsuario Result:(ModificarUsuarioBlock)pBlock;

#pragma mark- Metodo Obtener Usuario

typedef void (^ObtenerUsuarioBlock)(SRResultMethod pResult, NSString *pmensajeError, Usuario *pUsuario);
- (void)ObtenerUsuario:(NSString *)pIdUsuario Result:(ObtenerUsuarioBlock)pBlock;

#pragma mark- Metodo Obtener Direccion Principal Usuario

typedef void (^ObtenerDirecionPrincipalUsuarioBlock)(SRResultMethod pResult, Direccion *pDireccion);
- (void)ObtenerDirecionPrincipalUsuario:(ObtenerDirecionPrincipalUsuarioBlock)pBlock;

#pragma mark- Metodo Obtener Direccion Alternativa Usuario

typedef void (^ObtenerDirecionAlternativaUsuarioBlock)(SRResultMethod pResult, Direccion *pDireccion);
- (void)ObtenerDirecionAlternativaUsuario:(ObtenerDirecionAlternativaUsuarioBlock)pBlock;

#pragma mark- Metodo Modificar Direccion Principal

typedef void (^ModificarDireccionPrincipalBlock)(SRResultMethod pResult, NSString *pMensaje);
- (void)ModificarDireccionPrincipal:(Direccion *)pDireccion Result:(ModificarDireccionPrincipalBlock)pBlock;

#pragma mark- Metodo Modificar Direccion Alternativa

typedef void (^ModificarDireccionAlternativaBlock)(SRResultMethod pResult, NSString *pMensaje);
- (void)ModificarDireccionAlternativa:(Direccion *)pDireccion Result:(ModificarDireccionAlternativaBlock)pBlock;

#pragma mark- Metodo Obtener Producto

typedef NS_ENUM (NSInteger, SRResultObtenerProducto)
{
    SRResultObtenerProductoYES        = SRResultMethodYES,      // todo OK
    SRResultObtenerProductoNoExiste   = SRResultMethodNO,       // devolucion no OK
    SRResultObtenerProductoNoYaesmio  = 5,                      // no es producto yaesmio
    SRResultObtenerProductoErrConex   = SRResultMethodErrConex, // err del servicio
};
typedef void (^ObtenerProductoBlock)(SRResultObtenerProducto pResult, NSString *pMensajeError, ProductoPadre *pProductoPadre);

- (void)ObtenerProducto:(NSString *)pUrlProducto Result:(ObtenerProductoBlock)pBlock;
- (void)ObtenerProducto:(NSString *)pCodigo Medio:(NSString *)pMedio Canal:(TipoCanalProducto)pTipoCanal Resultado:(ObtenerProductoBlock)pBlock;

typedef void (^ObtenerProductoPorIDBlock)(SRResultObtenerProducto pResult, Producto *pProducto);
- (void)ObtenerProductoHijo:(NSString *)pidProducto Result:(ObtenerProductoPorIDBlock)pBlock;

#pragma mark- Metodo Comprar lista Productos (solo apartar)

typedef void (^ComprarListaProductosBlock)(SRResultMethod pResult,NSString *pMensaje, NSString *pIdCompra);
- (void)IniciarCompraConListaProductos:(NSArray *)plistaProductos Usuario:(Usuario *)pUsuario DireccionEnvio:(Direccion *)pDireccion Result:(ComprarListaProductosBlock)pBlock;

#pragma mark- Metodo Actualizar Compra (ejecutar compra)

typedef void (^FinalizarCompraConIDBlock)(SRResultMethod pResult, NSString *pMensaje);
- (void)FinalizarCompraConID:(NSString *)pIdCompra IDPaypal:(NSString *)pIdPaypal Result:(FinalizarCompraConIDBlock)pBlock;

#pragma mark- Metodo Obtener Lista Favoritos

typedef void (^ObtenerListaFavoritosBlock)(SRResultMethod pResult, NSArray *plistaProductos, int numElementosTotales, NSString *pMensaje);
- (void)ObtenerListaFavoritosConIdUser:(NSString *)pIdUser PosInicio:(int)pIntInicio Cuantos:(int)pintCantidad Result:(ObtenerListaFavoritosBlock)pBlock;

#pragma mark- Metodo Agregar Favorito

typedef void (^AgregarFavoritoBlock)(SRResultMethod pResult, NSString *pMensaje);
- (void)AgregarFavoritoConIdUser:(NSString *)pIdUser IdProducto:(ProductoPadre *)pProducto Result:(AgregarFavoritoBlock)pBlock;

#pragma mark- Metodo Eliminar Favorito

typedef void (^EliminarFavoritoBlock)(SRResultMethod pResult, NSString *pMensaje);
- (void)EliminarFavoritoConIdUser:(NSString *)pIdUser Favorito:(Favorito *)pFavorito Result:(EliminarFavoritoBlock)pBlock;

#pragma mark- Metodo Obtener Historico Compra

typedef NS_ENUM (NSInteger, SRTipoHistoricoAObtener)
{
    SRTipoHistoricoAObtenerTodos     = 0, // todo OK
    SRTipoHistoricoAObtenerProductos = 1, // devolucion no OK
    SRTipoHistoricoAObtenerCupones   = 2, // devolucion no OK
};
typedef void (^ObtenerListaHistoricoComprasBlock)(SRResultMethod pResult, NSString *pMensaje, NSArray *plistaProductos, int NumeroTotalElementos);
- (void)ObtenerListaHistoricoComprasConIdUser:(NSString *)pIdUser PosInicio:(int)pIntInicio Cuantos:(int)pintCantidad TipoProductosVer:(SRTipoHistoricoAObtener)TipoProductosVer Result:(ObtenerListaHistoricoComprasBlock)pBlock;

#pragma mark- Metodo Chequear Compra

typedef void (^ChequearCompraBlock)(SRResultMethod pResult, NSString *pMensaje, NSDecimalNumber *pGastosEnvio, NSString *pMailPaypal);
- (void)ChequearCompra:(EmpresaCarrito *)pCarrito Usuario:(Usuario *)pUsuario DireccionEnvio:(Direccion *)pDireccion Result:(ChequearCompraBlock)pBlock;

#pragma mark- Metodo Cancelar Compra

typedef void (^CancelarCompraBlock)(SRResultMethod pResult);
- (void)CancelarCompra:(NSString *)pIdCompra Result:(CancelarCompraBlock)pBlock;

#pragma mark- Metodo Obtener Textos

typedef void (^ObtenerTextoBlock)(SRResultMethod pResult, NSString *pTexto);
- (void)ObtenerTexto:(NSString *)pID Result:(ObtenerTextoBlock)pBlock;


#pragma mark- Metodo Valorar Producto Comprado Historico
typedef void (^ValorarProductoCompraBlock)(SRResultMethod pResult, NSString *pTexto);
- (void)ValorarProductoCompra:(NSString *)pIdProducto Pedido:(NSString *)pIdPedido Valoracion:(int)pValor TextoValoracion:(NSString *)pTextoValoracion Result:(ValorarProductoCompraBlock)pBlock;


#pragma mark- Metodo Obtener Lista Emoticonos
typedef void (^ObtenerListaEmoticonosBlock)(SRResultMethod pResult, NSArray *pLista);
- (void)ObtenerListaEmoticonos:(ObtenerListaEmoticonosBlock)pBlock;


#pragma mark- Metodo Obtener Lista Titulos Informacion
typedef void (^ObtenerListaTitulosInformacionBlock)(SRResultMethod pResult, NSArray *pLista);
- (void)ObtenerListaTitulosInformacion:(ObtenerListaTitulosInformacionBlock)pBlock;


#pragma mark- Metodo Obtener Lista Sitios Yaesmio
typedef void (^ObtenerListaSitiosYaesmioBlock)(SRResultMethod pResult, NSArray *pLista);
- (void)ObtenerListaSitiosYaesmio:(ObtenerListaSitiosYaesmioBlock)pBlock;


#pragma mark- Metodo Obtener Lista Productos con filtro
typedef void (^ObtenerListaProductosBlock)(SRResultMethod pResult, NSString *MensajeSAP, NSArray *pLista, int NumeroProductosEncontrados);
- (void)ObtenerListaProductos:(FiltroBusquedaProductos *)pFiltro PosicionInicial:(int)pPosicionInicial NumeroElementos:(int)pNumeroElementos Result:(ObtenerListaProductosBlock)pBlock;


#pragma mark- Metodo Devolver Lista Categorias Producto
typedef void (^DevolverListaCategoriasProductoBlock)(SRResultMethod pResult, NSString *MensajeSAP, NSArray *pListaCategoriasProducto);
- (void)DevolverListaCategoriasProducto:(DevolverListaCategoriasProductoBlock)pBlock;


#pragma mark- Metodo Obtener Lista Empresas Busqueda
typedef void (^DevolverListaEmpresaBusquedaBlock)(SRResultMethod pResult, NSString *MensajeSAP, NSArray *pListaEmpresaBusqueda);
- (void)DevolverListaEmpresaBusqueda:(DevolverListaEmpresaBusquedaBlock)pBlock;


#pragma mark- Metodo Obtener Configuracion App
typedef void (^ObtenerConfiguracionAppBlock)(SRResultMethod pResult, NSString *MensajeSAP, NSString *urlImagenFondoApp, NSString *urlSonido, NSArray *ListaParameters);
- (void)ObtenerConfiguracionApp:(ObtenerConfiguracionAppBlock)pBlock;


#pragma mark- Realizar compra Precio 0
typedef void (^RealizarCompraPrecioCeroBlock)(SRResultMethod pResult, NSString *MensajeSAP);
- (void)RealizarCompraPrecioCero:(ProductoPadre *)pProducto Unidades:(NSDecimalNumber *)pUnidades Usuario:(NSString *)pidUsuario Result:(RealizarCompraPrecioCeroBlock)pBlock;


#pragma mark- Activar notificaciones
typedef void (^ActivarNotificacionesYaesmioBlock)(SRResultMethod pResult, NSString *MensajeSAP);
- (void)ActivarNotificacionesYaesmio:(NSString *)pIdPush IdUsuario:(NSString *)pIdUsuario MacDevice:(NSString *)pMacDevice Telefono:(NSString *)pTelefono Result:(ActivarNotificacionesYaesmioBlock)pBlock;


#pragma mark- Lista Notificaciones Recibidas
typedef void (^ObtenerListaNotificacionesRecibidasBlock)(SRResultMethod pResult, NSString *MensajeSAP, NSArray *Lista, int NumeroTotalNotificaciones);
- (void)ObtenerListaNotificacionesRecibidas:(NSString *)pIdUsuario NumeroLineas:(int)pNumeroLineas PosicionInicial:(int)pPosicionInicial Visto:(BOOL)pVisto Result:(ObtenerListaNotificacionesRecibidasBlock)pBlock;


#pragma mark- Marcar notificacion Recibida como leida
typedef NS_ENUM (NSInteger, MarcadoNotificacionRecibida)
{
    MarcadoNotificacionRecibidaLeida       = 1,
    MarcadoNotificacionRecibidaRecibida    = 2,
    MarcadoNotificacionRecibidaEliminada   = 3,
};
typedef void (^MarcarNotificacionRecibidaComoLeidaBlock)(SRResultMethod pResult, NSString *MensajeSAP);
- (void)MarcarNotificacionRecibida:(NSString *)pIdNotificacion Idusuario:(NSString *)pIdUsuario Tipo:(MarcadoNotificacionRecibida)pTipo Result:(MarcarNotificacionRecibidaComoLeidaBlock)pBlock;


#pragma mark- Lista Notificaciones Enviadas
typedef void (^ObtenerListaNotificacionesEnviadasBlock)(SRResultMethod pResult, NSString *MensajeSAP, NSArray *Lista, int NumeroTotalNotificaciones);
- (void)ObtenerListaNotificacionesEnviadas:(NSString *)pIdUsuario NumeroLineas:(int)pNumeroLineas PosicionInicial:(int)pPosicionInicial Visto:(BOOL)pVisto Result:(ObtenerListaNotificacionesEnviadasBlock)pBlock;


#pragma mark- Marcar notificacion Enviada como Eliminada
typedef void (^MarcarNotificacionEnviadaEliminadaBlock)(SRResultMethod pResult, NSString *MensajeSAP);
- (void)MarcarNotificacionEnviadaEliminada:(NSString *)pIdNotificacion Result:(MarcarNotificacionEnviadaEliminadaBlock)pBlock;


#pragma mark- Lista Destinatarios Notificacion Enviada
typedef void (^ObtenerListaDestinatariosNotificacionEnviadaBlock)(SRResultMethod pResult, NSString *MensajeSAP, NSArray *Lista);
- (void)ObtenerListaDestinatariosNotificacionEnviada:(NSString *)pIdNotificacion Result:(ObtenerListaDestinatariosNotificacionEnviadaBlock)pBlock;


#pragma mark- Lista Grupos Notificacion creados por el usuario
typedef void (^ObtenerListaGruposNotificacionlock)(SRResultMethod pResult, NSString *MensajeSAP, NSArray *Lista);
- (void)ObtenerListaGruposNotificacion:(ObtenerListaGruposNotificacionlock)pBlock;


#pragma mark- Eliminar Grupo Notificacion
typedef void (^EliminarGrupoNotificacionBlock)(SRResultMethod pResult, NSString *MensajeSAP);
- (void)EliminarGrupoNotificacion:(NSString*)pidGrupo Result:(EliminarGrupoNotificacionBlock)pBlock;


#pragma mark- Crear Grupo Notificacion
typedef void (^CrearGrupoNotificacionBlock)(SRResultMethod pResult, NSString *MensajeSAP, GrupoNotificacion *pGrupo);
- (void)CrearGrupoNotificacion:(NSString*)pNombreGrupo Contactos:(NSArray*)pListaContactos Result:(CrearGrupoNotificacionBlock)pBlock;


#pragma mark- Modificar Grupo Notificacion
typedef void (^ModificarGrupoNotificacionBlock)(SRResultMethod pResult, NSString *MensajeSAP);
- (void)ModificarGrupoNotificacion:(GrupoNotificacion*)pGrupo Result:(ModificarGrupoNotificacionBlock)pBlock;


#pragma mark- Crear o Eliminar Contacto en Grupo Notificacion
typedef void (^CrearEliminarContactosDelGrupoBlock)(SRResultMethod pResult, NSString *MensajeSAP, NSArray *pListaContactosActuales);
- (void)EliminarContactosDelGrupo:(NSString*)pIdGrupo ListaContactos:(NSArray*)pListaContactos Usuario:(NSString*)pIdUsuario Result:(CrearEliminarContactosDelGrupoBlock)pBlock;
- (void)AgregarContactosAlGrupo:(NSString*)pIdGrupo ListaContactos:(NSArray*)pListaContactos Usuario:(NSString*)pIdUsuario Result:(CrearEliminarContactosDelGrupoBlock)pBlock;


#pragma mark- Comprobar Contactos
typedef void (^ComprobarListaContactosBlock)(SRResultMethod pResult, NSString *MensajeSAP, NSArray *pListaContactos);
- (void)ComprobarListaContactos:(NSArray*)pLista DeUsuario:(NSString*)pIdUsuario Result:(ComprobarListaContactosBlock)pBlock;


#pragma mark- Enviar Notificacion
typedef void (^EnviarNotificacionPushBlock)(SRResultMethod pResult, NSString *MensajeSAP);
- (void)EnviarNotificacionPush:(ProductoPadre*)pProducto DeUsuario:(NSString*)pIdUsuario ListaIdContactos:(NSArray*)pListaIdContactos ListaIdGrupos:(NSArray*)pListaIdGrupos ListaMails:(NSArray*)pListaMails Mensaje:(NSString*)pMensaje Result:(EnviarNotificacionPushBlock)pBlock;


#pragma mark- Desactivar Notificaciones Push
typedef void (^DesactivarNotificacionesPushBlock)(SRResultMethod pResult, NSString *MensajeSAP);
- (void)DesactivarNotificacionesPush:(NSString*)pIdPush DeUsuario:(NSString*)pIdUsuario MacDevice:(NSString*)pMacDevice Telefono:(NSString*)pTelefono Result:(DesactivarNotificacionesPushBlock)pBlock;


#pragma mark- Obtener Provincia de latitud y longitud
typedef void (^ObtenerProvinciaDeLatitudLongitudBlock)(SRResultMethod pResult, NSString *MensajeSAP, Pais *pPais, Provincia *pProvincia);
- (void)ObtenerProvinciaDeLatitud:(double)pLatitud Longitud:(double)pLongitud OCodigoPostal:(NSString*)pCodigoPostal IdPais:(NSString*)pIdPais Result:(ObtenerProvinciaDeLatitudLongitudBlock)pBlock;

#pragma mark- Obtener Numero Notificaciones No Leidas
typedef void (^ObtenerNumeroNotificacionesNoLeidasBlock)(SRResultMethod pResult, NSString *MensajeSAP, NSInteger NumeroNotificacionesNoLeidas);
- (void)ObtenerNumeroNotificacionesNoLeidas:(ObtenerNumeroNotificacionesNoLeidasBlock)pBlock;


#pragma mark- Obtener Lista Tarjeta Pago Usuario
typedef void (^ObtenerListaTarjetasUsuarioBlock)(SRResultMethod pResult, NSString *MensajeSAP, NSArray *ListaTarjetas);
- (void)ObtenerListaTarjetasUsuario:(ObtenerListaTarjetasUsuarioBlock)pBlock;


#pragma mark- Obtener Lista Monedas
typedef void (^ObtenerListaMonedasBlock)(SRResultMethod pResult, NSString *MensajeSAP, NSArray *ListaMonedas);
- (void)ObtenerListaMonedas:(ObtenerListaMonedasBlock)pBlock;

#pragma mark- Obtener Lista Destacados
typedef void (^ObtenerListaDestacadosBlock)(SRResultMethod pResult, NSString *MensajeSAP, NSArray *ListaDestacados, NSInteger NumeroPaginas);
- (void)ObtenerListaDestacados:(NSInteger)Pagina Dispositivo:(NSString*)TipoDispositivo Result:(ObtenerListaDestacadosBlock)pBlock;
@end
