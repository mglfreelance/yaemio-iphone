//
//  ProductoImagenesViewController.m
//  Yaesmio
//
//  Created by freelance on 05/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ProductoImagenesViewController.h"
#import "MediaProductoCell.h"

@interface ProductoImagenesViewController () <UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionImagenes;
@property (weak, nonatomic) IBOutlet UILabel *labelTitulo;


@property (strong) NSArray *ListaImagenes;

@end

@implementation ProductoImagenesViewController

@synthesize ListaImagenes = _ListaImagenes;

- (void)viewUnload
{
    for (MediaProductoCell *Cell in self.collectionImagenes.visibleCells)
    {
        if (Cell != nil)
        {
            [Cell unloadAllControls];
        }
    }
    
    [self setLabelTitulo:nil];
    [self setCollectionImagenes:nil];
    [self setListaImagenes:nil];
    [super viewUnload];
}

- (void)RellenarPantallaConDatosProducto
{
    self.collectionImagenes.backgroundColor = [UIColor clearColor];
    self.collectionImagenes.backgroundView = [[UIView alloc] initWithFrame:CGRectZero];
    self.ListaImagenes = [Media FiltrarArray:self.Producto.ProductoHijo.ListaMedia PorTipo:MediaTypeMIME_image];
    [self.collectionImagenes reloadData];
}

- (Media *)getMedia:(NSIndexPath *)pIndex
{
    return self.ListaImagenes[pIndex.row];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return self.ListaImagenes.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Media *lMedia = [self getMedia:indexPath];
    
    MediaProductoCell *cell = [cv dequeueReusableCellWithReuseIdentifier:[MediaProductoCell getReuseIdentifier:lMedia] forIndexPath:indexPath];
    
    [cell ConfigurarImagen:lMedia];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [[Locator Default].Vistas MostrarGaleriaImagenes:self ListaImagenes:self.ListaImagenes NumImagenAmostrar:(int)indexPath.row UrlImagenEmpresa:self.Producto.LogoEmpresa.UrlPath];
}

@end
