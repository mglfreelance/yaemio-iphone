//
//  NSDate+Ampliado.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 20/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "NSDate+Ampliado.h"

@implementation NSDate (Ampliado)

- (NSString *)format:(NSString *)pFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat:pFormat];
    return [dateFormatter stringFromDate:self];
}

+ (NSDate *)fromString:(NSString *)pstrDate WithFormat:(NSString *)pFormat
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:pFormat];
    NSDate *myDate = [df dateFromString:pstrDate];
    return myDate;
}

- (NSComparisonResult)compareIgnoreTime:(NSDate *)pDate
{
    NSComparisonResult result = NSOrderedSame;
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:self];
    
    NSDateComponents *componentDate = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit fromDate:pDate];
    
    if (components.year < componentDate.year)
    {
        result = NSOrderedDescending;
    }
    else if (components.year > componentDate.year)
    {
        result = NSOrderedAscending;
    }
    else
    {
        if (components.month < componentDate.month)
        {
            result = NSOrderedDescending;
        }
        else if (components.month > componentDate.month)
        {
            result = NSOrderedAscending;
        }
        else
        {
            if (components.day < componentDate.day)
            {
                result = NSOrderedDescending;
            }
            else if (components.day > componentDate.day)
            {
                result = NSOrderedAscending;
            }
            else
            {
                result = NSOrderedSame;
            }
        }
    }
    
    return result;
}

- (NSDate *)dateWithYear:(NSInteger)year Month:(NSInteger)month Day:(NSInteger)day Hour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit fromDate:self];
    
    if (year != UNCHANGEDATE)
    {
        [components setYear:year];
    }
    
    if (month != UNCHANGEDATE)
    {
        [components setMonth:month];
    }
    
    if (day != UNCHANGEDATE)
    {
        [components setDay:day];
    }
    
    if (hour != UNCHANGEDATE)
    {
        [components setHour:hour];
    }
    
    if (minute != UNCHANGEDATE)
    {
        [components setMinute:minute];
    }
    
    if (second != UNCHANGEDATE)
    {
        [components setSecond:second];
    }
    
    NSDate *newDate = [calendar dateFromComponents:components];
    return newDate;
}

- (int)Year
{
    if (self == nil)
    {
        return 0;
    }
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSYearCalendarUnit fromDate:self];
    
    return (int)components.year;
}

- (int)Month
{
    if (self == nil)
    {
        return 0;
    }
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSMonthCalendarUnit fromDate:self];
    
    return (int)components.month;
}

- (int)Day
{
    if (self == nil)
    {
        return 0;
    }
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSDayCalendarUnit fromDate:self];
    
    return (int)components.day;
}

- (int)Hour
{
    if (self == nil)
    {
        return 0;
    }
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSHourCalendarUnit fromDate:self];
    
    return (int)components.hour;
}

- (int)Minute
{
    if (self == nil)
    {
        return 0;
    }
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSMinuteCalendarUnit fromDate:self];
    
    return (int)components.minute;
}

- (int)Second
{
    if (self == nil)
    {
        return 0;
    }
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSSecondCalendarUnit fromDate:self];
    
    return (int)components.second;
}

@end
