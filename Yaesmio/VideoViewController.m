//
//  VideoViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/10/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "VideoViewController.h"
#import "UIAllControls.h"

@interface VideoViewController ()

@property (strong) UIImageView          *imgLogoEmpresa;
@property (assign) UIDeviceOrientation  mCurrentOrientation;
@property (assign) UIDeviceOrientation  mPreviousOrientation;
@property (assign) int mDegree;
@property (assign) int mHeight;
@property (assign) int mWidht;

@end

@implementation VideoViewController

#define GRUPO_VIDEOCONTROLLER @"VideoViewController"

-(id)initWithContentURL:(NSURL *)contentURL LogoEmpresa:(NSString*)pUrlLogoEmpresa Timer:(NSTimeInterval)pTimer Resul:(ResultTimerVideoBlock) pResult
{
    if ((self = [super initWithContentURL:contentURL]) != NULL)
	{
		self.UrlImageEmpresa = pUrlLogoEmpresa;
        self.Timer = pTimer;
        self.Result = pResult;
	}
	
	return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imgLogoEmpresa = [[UIImageView alloc] initWithFrame:CGRectMake(5, 55, 100, 30)];
    [self.view addSubview:self.imgLogoEmpresa];
    [self.imgLogoEmpresa cargarUrlImagenIzquierdaAsincrono:self.UrlImageEmpresa Grupo:GRUPO_VIDEOCONTROLLER];
    
    [[Locator Default].Carrito QuitarBotonCarrito:self];
    
    self.mCurrentOrientation = UIDeviceOrientationPortrait;
    self.mPreviousOrientation = UIDeviceOrientationPortrait;
    self.mHeight = self.view.frame.size.height;
    self.mWidht = self.view.frame.size.width;
    [self.view setCenter:CGPointMake(self.mWidht/2, self.mHeight/2)];
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(deviceOrientationDidChange:) name: UIDeviceOrientationDidChangeNotification object: nil];

    self.moviePlayer.initialPlaybackTime =self.Timer;
}

-(void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
   
    if (self.moviePlayer.playbackState == MPMoviePlaybackStatePlaying)
    {
        [self.moviePlayer stop];
        self.moviePlayer.contentURL = nil;
    }
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
    
    NSTimeInterval time = -1;
    
    if (self.moviePlayer.playableDuration != self.moviePlayer.currentPlaybackTime)
    {
        time = self.moviePlayer.currentPlaybackTime;
    }
    
    if (self.Result != nil)
    {
        self.Result(time);
    }
    self.UrlVideo = nil;
    self.UrlImageEmpresa = nil;
    self.Timer = 0;
    self.Result = nil;
    self.imgLogoEmpresa.image = nil;
    self.imgLogoEmpresa = nil;
    self.mCurrentOrientation = 0;
    self.mPreviousOrientation = 0;
}

- (void)deviceOrientationDidChange:(NSNotification *)notification
{
    //Obtaining the current device orientation
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    //Ignoring specific orientations
    if (orientation == UIDeviceOrientationFaceUp || orientation == UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationUnknown || self.mCurrentOrientation == orientation)
    {
        return;
    }
    
    //Responding only to changes in landscape or portrait
    self.mPreviousOrientation = self.mCurrentOrientation;
    self.mCurrentOrientation = orientation;
    //
    [self performSelector:@selector(orientationChangedMethod) withObject:nil afterDelay:0];
    
}

-(void)orientationChangedMethod
{
    int lDegree = [self getDegrees];
    self.mDegree += lDegree;
    lDegree = (self.mDegree /90) % 2;
    
    [UIView animateWithDuration:0.5 animations:^
     {
         if (lDegree ==0)
             [self.view setBounds:CGRectMake(0, 0, self.mWidht, self.mHeight)];
         else
             [self.view setBounds:CGRectMake(0, 0, self.mHeight, self.mWidht)];
         
         [self.view setTransform:CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(self.mDegree))];
    }];
    
}

-(int) getDegrees
{
    int result = 0;
    
    switch (self.mPreviousOrientation)
    {
        case UIDeviceOrientationPortrait:
            if (self.mCurrentOrientation == UIDeviceOrientationLandscapeLeft)
                result = 90;
            else if (self.mCurrentOrientation == UIDeviceOrientationLandscapeRight)
                result = -90;
            else if ( self.mCurrentOrientation == UIDeviceOrientationPortraitUpsideDown)
                result = -180;
            break;
            
        case UIDeviceOrientationLandscapeLeft:
            if (self.mCurrentOrientation == UIDeviceOrientationPortrait)
                result = -90;
            else if (self.mCurrentOrientation == UIDeviceOrientationLandscapeRight)
                result = -180;
            else if ( self.mCurrentOrientation == UIDeviceOrientationPortraitUpsideDown)
                result = 90;
            break;
            
        case UIDeviceOrientationLandscapeRight:
            if (self.mCurrentOrientation == UIDeviceOrientationLandscapeLeft)
                result = 180;
            else if (self.mCurrentOrientation == UIDeviceOrientationPortrait)
                result = 90;
            else if ( self.mCurrentOrientation == UIDeviceOrientationPortraitUpsideDown)
                result = -90;
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            if (self.mCurrentOrientation == UIDeviceOrientationLandscapeLeft)
                result = -90;
            else if (self.mCurrentOrientation == UIDeviceOrientationLandscapeRight)
                result = 90;
            else if ( self.mCurrentOrientation == UIDeviceOrientationPortrait)
                result = 180;
            break;
            
        default:
            result =0;
            break;
    }
    
    return result;
}



@end
