//
//  PickerValorAtributoViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 05/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "PickerElementosViewController.h"
#import "UIAllControls.h"

@interface PickerElementosViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic) IBOutlet UIBarButtonItem  *controlBotonCancelar;
@property (nonatomic) IBOutlet UIBarButtonItem  *controlBotonAceptar;
@property (nonatomic) IBOutlet UIToolbar        *controlToolBar;
@property (nonatomic) IBOutlet UIPickerView     *ControlPicker;

- (IBAction)btnDone_click:(id)sender;
- (IBAction)btnCancel_click:(id)sender;

@end

@implementation PickerElementosViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.controlBotonAceptar.title = Traducir(@"Aceptar");
    self.controlBotonCancelar.title = Traducir(@"Cancelar");
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        self.controlToolBar.tintColor =[Locator Default].Color.Principal;
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.ElementoSeleccionado != nil)
    {
        int row = [self.ListaElementos obtenerIndiceElemento:self.ElementoSeleccionado] +1;
        [self.ControlPicker selectRow:row inComponent:0 animated:false];
    }
    else if (self.ListaElementos.count >1)
    {
        [self.ControlPicker selectRow:1 inComponent:0 animated:false];
    }
}

- (void)viewDidUnload
{
    [self setControlPicker:nil];
    [self setControlToolBar:nil];
    [self setControlBotonCancelar:nil];
    [self setControlBotonAceptar:nil];
    [super viewDidUnload];
}

- (BOOL) shouldAutorotate
{
    return NO;
}

#pragma  mark- UIPickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.ListaElementos.count +1;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel  *lVista;
    
    if (view != nil && [view isKindOfClass:[UILabel class]])
    {
        lVista = (UILabel*) view;
    }
    else
    {
        CGSize ancho = [pickerView rowSizeForComponent:component];
        lVista = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, ancho.width-10, ancho.height)];
    }
    
    lVista.adjustsFontSizeToFitWidth = true;
    lVista.backgroundColor = [Locator Default].Color.Clear;
    lVista.textAlignment =NSTextAlignmentCenter;
    
    if (row == 0)
    {
        lVista.text = [self.NombrePantalla uppercaseString];
        lVista.textColor = [Locator Default].Color.Azul;
        lVista.font = [Locator Default].FuenteLetra.PickerViewCabecera;
    }
    else
    {
        ElementoPicker *lValor =self.ListaElementos[row -1];
        lVista.text = lValor.Nombre;
        lVista.textColor = [Locator Default].Color.Texto;
        lVista.font = [Locator Default].FuenteLetra.PickerView;
    }
    
    return lVista;
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.controlBotonAceptar.enabled = (row != 0);
}

#pragma mark- Eventos Objetos

- (IBAction)btnDone_click:(id)sender
{
    int row = [self.ControlPicker selectedRowInComponent:0]-1.0;
    if (row >= 0)
    {
        [self dismissSemiModalViewController:self];
        
        ElementoPicker *lResultado = nil;
        
        if (self.ListaElementos.count > row)
        {
            lResultado = self.ListaElementos[row];
        }
        
        if (self.Respond != nil)
            self.Respond(lResultado);
    }
}

- (IBAction)btnCancel_click:(id)sender
{
    [self dismissSemiModalViewController:self];
    
    if (self.Respond != nil)
        self.Respond(nil);
}

@end
