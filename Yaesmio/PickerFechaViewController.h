//
//  PickerFechaViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 26/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAllControls.h"

@interface PickerFechaViewController : TDSemiModalViewController

typedef void (^PickerFechaBlock)(NSDate *pResult);

@property (nonatomic, strong) NSString          *NombrePantalla;
@property (nonatomic, strong) NSDate            *FechaSeleccionada;
@property (nonatomic, strong) NSDate            *FechaMaxima;
@property (copy)              PickerFechaBlock  Respond;

@end
