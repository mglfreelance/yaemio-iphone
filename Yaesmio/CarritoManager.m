//
//  CarritoManager.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 16/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import "CarritoManager.h"
#import "EmpresaCarrito.h"
#import "JSBadgeView.h"

@interface CarritoManager ()

@property (nonatomic) JSBadgeView *mActualBadge;
@property (nonatomic) NSMutableDictionary *mListaEmpresas;
@property (nonatomic) UIViewController *PantallaActual;
@property (nonatomic) UITapGestureRecognizer *TapGesture;

@end

@implementation CarritoManager


- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.mListaEmpresas = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (int)NumeroProductosAgregados
{
    int lNumero = 0;
    
    for (EmpresaCarrito *lDato in self.mListaEmpresas.allValues)
    {
        lNumero += [lDato.Elementos count];
    }
    
    return lNumero;
}

- (void)ActualizarEstado
{
    [self actualizeBadge];
}

- (void)actualizeBadge
{
    if (self.mActualBadge != nil)
    {
        self.mActualBadge.badgeText = [NSString stringWithFormat:@"%d", self.NumeroProductosAgregados];
        
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
        {
            self.mActualBadge.hidden = FALSE;
        }
        else
        {
            self.mActualBadge.hidden = (self.NumeroProductosAgregados == 0);
        }
        
        self.PantallaActual.navigationItem.rightBarButtonItem.imageInsets = [self getImageInsetsButton];
        
        self.mActualBadge.badgePositionAdjustment = CGPointMake(-21, 0);
    }
}

- (void)agregarBotonCarrito:(UIViewController *)pView EsVistaCarrito:(BOOL)pEsCarrito
{
    if (pEsCarrito == false)
    {
        self.PantallaActual = pView;
        
        UIBarButtonItem *Button = [[UIBarButtonItem alloc] initWithImage:[[Locator Default].Imagen ImagenBotonCarrito] style:UIBarButtonItemStyleBordered target:self action:@selector(MostrarPantallaCarrito)];
        Button.imageInsets = [self getImageInsetsButton];
        
        self.PantallaActual.navigationItem.rightBarButtonItem = Button;
        
        if (self.mActualBadge == nil)
        {
            self.mActualBadge = [[JSBadgeView alloc] initWithParentView:self.PantallaActual.navigationController.navigationBar alignment:JSBadgeViewAlignmentCenterRight];
            self.TapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(MostrarPantallaCarrito)];
            [self.mActualBadge addGestureRecognizer:self.TapGesture];
        }
        else
        {
            [self.mActualBadge removeFromSuperview];
            [self.PantallaActual.navigationController.navigationBar addSubview:self.mActualBadge];
        }
        
        self.mActualBadge.badgeTextFont = [self.mActualBadge.badgeTextFont fontWithSize:11];
        self.mActualBadge.badgeTextColor = [Locator Default].Color.Blanco;
        self.mActualBadge.badgeBackgroundColor = [Locator Default].Color.Clear;
        self.mActualBadge.badgeOverlayColor = [Locator Default].Color.Clear;
        self.mActualBadge.badgeTextShadowColor = [Locator Default].Color.Clear;
        
        [self actualizeBadge];
    }
}

- (void)QuitarBotonCarrito:(UIViewController *)pView
{
    self.mActualBadge.badgeText = @"";
    self.mActualBadge.hidden = true;
    self.PantallaActual = pView;
}

- (void)agregarProducto:(ProductoPadre *)pProducto Unidades:(NSDecimalNumber *)pUnidades
{
    if (!pProducto.ProductoHijo.esPublicitario && pProducto.ProductoHijo.Stock.isGreaterZero && pProducto.ProductoHijo.Stock.intValue >= pUnidades.intValue)
    {
        EmpresaCarrito *LDato = [self ObtenerOCrearListaProductosPorEmpresa:pProducto.EmpresaId ExigeDireccionEnvio:!pProducto.SinDireccionEnvio];
        
        [LDato agregarProducto:pProducto Unidades:pUnidades];
        
        [self actualizeBadge];
    }
}

- (int)NumeroEmpresasAgregadas
{
    return (int) self.mListaEmpresas.count;
}

- (NSArray *)EmpresasCarrito
{
    return [[NSArray alloc] initWithArray:self.mListaEmpresas.allValues];
}

- (NSArray *)obtenerListaProductosPorEmpresa:(NSString *)pIdEmpresa ExigeDireccionEnvio:(BOOL)pTieneDireccionEnvio
{
    NSArray *lResultado = [self ObtenerOCrearListaProductosPorEmpresa:pIdEmpresa ExigeDireccionEnvio:pTieneDireccionEnvio].Elementos;
    
    return lResultado;
}

- (void)MostrarPantallaCarrito
{
    [[Locator Default].Vistas MostrarCarritoCompra:self.PantallaActual];
}

- (BOOL)ExisteProductoEnCarrito:(ProductoPadre *)pProducto
{
    BOOL lResultado = false;
    
    EmpresaCarrito *lEmpresa = self.mListaEmpresas[[self ObtenerKeyPorIdEmpresa:pProducto.EmpresaId ExigeDireccionEnvio:!pProducto.SinDireccionEnvio]];
    
    if (lEmpresa != nil)
    {
        lResultado = ([lEmpresa ObtenerElementoProducto:pProducto] != nil);
    }
    
    return lResultado;
}

- (ProductoPadre *)ObtenerProductoDeFavorito:(Favorito *)pFavorito
{
    ProductoPadre *result = nil;
    
    for (EmpresaCarrito *myEmpresa in self.mListaEmpresas.allValues)
    {
        for (ElementoCarrito *Elemento in myEmpresa.Elementos)
        {
            if ([Elemento.Producto.ProductoId isEqualToString:pFavorito.ProductoIdPadre] && [Elemento.Producto.ProductoHijo.ProductoId isEqualToString:pFavorito.ProductoId])
            {
                result = Elemento.Producto;
                break;
            }
        }
        
        if (result != nil)
        {
            break;
        }
    }
    
    return result;
}

- (BOOL)ExisteFavoritoEnCarrito:(Favorito *)pFavorito
{
    return ([self ObtenerProductoDeFavorito:pFavorito] != nil);
}

-(int) obtenerNumeroProductosPorEmpresa:(NSString*)pIdEmpresa ExigeDireccionEnvio:(BOOL)pTieneDireccionEnvio
{
    return (int) [self ObtenerOCrearListaProductosPorEmpresa:pIdEmpresa ExigeDireccionEnvio:pTieneDireccionEnvio].Elementos.count;
}

- (BOOL)LLamadaDelegateActualizarBadge:(BOOL)lResultado
{
    if (lResultado == true)
    {
        [self actualizeBadge];
        
        if (self.delegate != nil)
        {
            [self.delegate CambiadoElementosCarrito];
        }
    }
    
    return lResultado;
}

- (BOOL)sePuedeAgregarUnidadesFavorito:(Favorito *)pFavorito Unidades:(NSDecimalNumber *)pUnidades
{
    BOOL result = true;
    
    ProductoPadre *Producto = [self ObtenerProductoDeFavorito:pFavorito];
    
    if (Producto != nil)
    {
        result = [self sePuedeAgregarUnidadesProducto:Producto Unidades:pUnidades];
    }
    
    return result;
}

- (BOOL)sePuedeAgregarUnidadesProducto:(ProductoPadre *)pProducto Unidades:(NSDecimalNumber *)pUnidades
{
    BOOL result = false;
    
    EmpresaCarrito *lEmpresa = self.mListaEmpresas[[self ObtenerKeyPorIdEmpresa:pProducto.EmpresaId ExigeDireccionEnvio:!pProducto.SinDireccionEnvio]];
    
    if (lEmpresa != nil)
    {
        ElementoCarrito *lElemento = [lEmpresa ObtenerElementoProducto:pProducto];
        
        if (lElemento != nil)
        {
            result = [lElemento sePuedeAgregarUnidadesProducto:pUnidades];
        }
    }
    
    return result;
}

- (BOOL)QuitarElemento:(ElementoCarrito *)pElemento
{
    BOOL lResultado = false;
    
    EmpresaCarrito *lEmpresa = self.mListaEmpresas[[self ObtenerKeyPorIdEmpresa:pElemento.Producto.EmpresaId ExigeDireccionEnvio:!pElemento.Producto.SinDireccionEnvio]];
    
    if (lEmpresa != nil)
    {
        lResultado = [lEmpresa QuitarElmento:pElemento];
    }
    
    if (lEmpresa.existenElementos == false)
    {
        [self QuitarEmpresa:lEmpresa];
    }
    
    return [self LLamadaDelegateActualizarBadge:lResultado];
}

- (bool)ModificarProducto:(ElementoCarrito *)pActual Modificado:(ProductoPadre *)pProductoCarrito Unidades:(NSDecimalNumber *)pUnidades
{
    EmpresaCarrito *lEmpresa = [self ObtenerOCrearListaProductosPorEmpresa:pActual.Producto.EmpresaId ExigeDireccionEnvio:!pProductoCarrito.SinDireccionEnvio];
    
    bool lResultado = [lEmpresa ModificarElemento:pActual Modificado:pProductoCarrito Unidades:pUnidades];
    
    return [self LLamadaDelegateActualizarBadge:lResultado];
}

#pragma mark- Metodos Privados

- (EmpresaCarrito *)ObtenerOCrearListaProductosPorEmpresa:(NSString *)pidEmpresa ExigeDireccionEnvio:(BOOL)pTieneDireccionEnvio
{
    EmpresaCarrito *lResultado;
    
    NSString *key = [self ObtenerKeyPorIdEmpresa:pidEmpresa ExigeDireccionEnvio:pTieneDireccionEnvio];
    lResultado = self.mListaEmpresas[key];
    
    if (lResultado == nil)
    {
        lResultado = [[EmpresaCarrito alloc] init:key];
        [self.mListaEmpresas setObject:lResultado forKey:key];
    }
    
    return lResultado;
}

- (bool)QuitarEmpresa:(EmpresaCarrito *)pEmpresa
{
    bool lResultado = false;
    
    EmpresaCarrito *lEmpresa = self.mListaEmpresas[[self ObtenerKeyPorIdEmpresa:pEmpresa.IdEmpresa ExigeDireccionEnvio:pEmpresa.esNecesariaDireccionEnvio]];
    
    if (lEmpresa != nil)
    {
        [self.mListaEmpresas removeObjectForKey:[self ObtenerKeyPorIdEmpresa:lEmpresa.IdEmpresa ExigeDireccionEnvio:lEmpresa.esNecesariaDireccionEnvio]];
        lResultado = true;
    }
    
    return lResultado;
}

- (UIEdgeInsets)getImageInsetsButton
{
    UIEdgeInsets resul;
    int lNum = self.NumeroProductosAgregados;
    int lValorDerecho   = 0;
    int lValorIzquierdo = 0;
    
    if (lNum > 0 && [Locator Default].Preferencias.EstaUsuarioLogado)
    {
        if (lNum > 99)
        {
            lValorDerecho = -16;
        }
        else if (lNum > 9)
        {
            lValorDerecho = -10;
        }
        else
        {
            lValorDerecho = -5;
        }
    }
    else
    {
        lValorIzquierdo = -5;
    }
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        lValorIzquierdo = -5;
        lValorDerecho = 0;
    }
    
    resul = UIEdgeInsetsMake(0, lValorDerecho, 0, lValorIzquierdo);
    
    return resul;
}

-(NSString*)ObtenerKeyPorIdEmpresa:(NSString *)pidEmpresa ExigeDireccionEnvio:(BOOL)pTieneDireccionEnvio
{
    return [NSString stringWithFormat:@"%@_%@",pidEmpresa,pTieneDireccionEnvio?@"true":@"false"];
}

@end
