//
//  Usuario.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "Usuario.h"
#import <libxml/xmlstring.h>
#if TARGET_OS_IPHONE
#import <CFNetwork/CFNetwork.h>
#endif
#import "NSDate+Ampliado.h"

@implementation Usuario

- (id)init
{
	if((self = [super init]))
    {
		self.Apellidos              = @"";
		self.ExisteDireccion1       = false;
		self.ExisteDireccion2       = false;
		self.Mail                   = @"";
		self.Nombre                 = @"";
		self.UsuarioId              = @"";
		self.fecha_nacimiento       = nil;
		self.sexo                   = @"";
        self.mensaje                = @"";
        self.aceptaEnvioPublicidad  = FALSE;
        self.Moneda                 = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Apellidos xmlNodeForDoc:node->doc elementName:@"Apellidos" elementNSPrefix:@"tns3"]);
	
	xmlAddChild(node, [self.Nombre xmlNodeForDoc:node->doc elementName:@"Nombre" elementNSPrefix:@"tns3"]);
	
	xmlAddChild(node, [self.UsuarioId xmlNodeForDoc:node->doc elementName:@"UsuarioId" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [[SOAP_Boolean serialize:self.aceptaEnvioPublicidad] xmlNodeForDoc:node->doc elementName:@"aceptarPubli" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [[SOAP_Boolean serialize:self.ExisteDireccion1] xmlNodeForDoc:node->doc elementName:@"direccion1" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [[SOAP_Boolean serialize:self.ExisteDireccion2] xmlNodeForDoc:node->doc elementName:@"direccion2" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [@"" xmlNodeForDoc:node->doc elementName:@"error" elementNSPrefix:@"tns3"]);
	
	xmlAddChild(node, [[self.fecha_nacimiento format:FECHA_SOAP] xmlNodeForDoc:node->doc elementName:@"fecha_nacimiento" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.mensaje xmlNodeForDoc:node->doc elementName:@"mensaje" elementNSPrefix:@"tns3"]);
	
    xmlAddChild(node, [self.Moneda.Codigo xmlNodeForDoc:node->doc elementName:@"moneda" elementNSPrefix:@"tns3"]);
    
	xmlAddChild(node, [self.sexo xmlNodeForDoc:node->doc elementName:@"sexo" elementNSPrefix:@"tns3"]);
}

+ (Usuario *)deserializeNode:(xmlNodePtr)cur
{
	Usuario *newObject = [Usuario new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"Apellidos"])
    {
        self.Apellidos = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"direccion1"])
    {
        self.ExisteDireccion1 = [SOAP_Boolean deserializeNode:pobjCur].boolValue;
    }
    else if([nodeName isEqualToString:@"direccion2"])
    {
        self.ExisteDireccion2 = [SOAP_Boolean deserializeNode:pobjCur].boolValue;
    }
    else if([nodeName isEqualToString:@"Mail"])
    {
        self.Mail = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Nombre"])
    {
        self.Nombre = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"UsuarioId"])
    {
        self.UsuarioId = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"fecha_nacimiento"])
    {
        NSString *lDato = [NSString deserializeNode:pobjCur];
        if ([lDato isEqualToString:@"0000-00-00"] == false)
        {
            self.fecha_nacimiento =[NSDate fromString:lDato WithFormat:FECHA_SOAP];
        }
    }
    else if([nodeName isEqualToString:@"sexo"])
    {
        self.sexo = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"mensaje"])
    {
        self.mensaje = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"aceptarPubli"])
    {
        self.aceptaEnvioPublicidad = [SOAP_Boolean deserializeNode:pobjCur].boolValue;
    }
    else if([nodeName isEqualToString:@"moneda"])
    {
        self.Moneda = [Moneda new];
        self.Moneda.Codigo = [NSString deserializeNode:pobjCur];
        self.Moneda.Nombre = [NSString deserializeNode:pobjCur];
    }
}

-(Usuario*) Clonar
{
    Usuario *lUsuario = [Usuario new];
    
    lUsuario.Apellidos              =  self.Apellidos;
    lUsuario.ExisteDireccion1       =  self.ExisteDireccion1;
    lUsuario.ExisteDireccion2       =  self.ExisteDireccion2;
    lUsuario.Mail                   =  self.Mail;
    lUsuario.Nombre                 =  self.Nombre;
    lUsuario.UsuarioId              =  self.UsuarioId;
    lUsuario.fecha_nacimiento       =  self.fecha_nacimiento;
    lUsuario.Apellidos              =  self.sexo;
    lUsuario.mensaje                =  self.mensaje;
    lUsuario.aceptaEnvioPublicidad  =  self.aceptaEnvioPublicidad;
    lUsuario.Moneda                 =  self.Moneda;
    
    return lUsuario;
}

@end
