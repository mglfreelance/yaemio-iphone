//
//  FiltroBusquedaProductos.h
//  Yaesmio
//
//  Created by freelance on 09/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"

typedef NS_ENUM (NSInteger, TipoProductoBusqueda)
{
    TipoProductoBusquedaProducto        = 0,
    TipoProductoBusquedaCupon           = 1,
    TipoProductoBusquedaPublicitario    = 2,
};

typedef NS_ENUM (NSInteger, OrdenacionBusqueda)
{
    OrdenacionBusquedaAscendente  = 1,
    OrdenacionBusquedaDescendente = 2,
    OrdenacionBusquedaNinguno     = 0,
};

typedef NS_ENUM (NSInteger, CampoOrdenacionBusqueda)
{
    CampoOrdenacionBusquedaEstandar   = 0,
    CampoOrdenacionBusquedaPrecio     = 1,
    CampoOrdenacionBusquedaDescuento  = 2,
    CampoOrdenacionBusquedaValoracion = 3,
    CampoOrdenacionBusquedaCercana   = 4,
};

@interface FiltroBusquedaProductos : SoapObject

+ (FiltroBusquedaProductos *)	deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong)   Provincia *Provincia;
@property (strong)   Empresa *Empresa;
@property (strong)   GrupoArticulo *GrupoArticulo;
@property (strong)   Pais *Pais;
@property (assign)   OrdenacionBusqueda Ordenacion;
@property (assign)   CampoOrdenacionBusqueda CampoOrdenacion;
@property (strong)   NSString *Texto;
@property (assign)   TipoProductoBusqueda TipoBusqueda;
@property (assign)   BOOL MostrarSoloRecomendadas;
@property (assign)   double Latitud;
@property (assign)   double Longitud;
@property (readonly) BOOL ExisteFiltro;

typedef void (^InitFiltroBusquedaProductos)(FiltroBusquedaProductos *pDato);
- (id)							initWith:(InitFiltroBusquedaProductos)pInitialize;

- (void)						AsignarPosicion:(int)pPosicion YNumeroElementos:(int)pNumeroElementos;

- (instancetype)				Clone;


@end
