//
//  GrupoArticulo.m
//  Yaesmio
//
//  Created by freelance on 08/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "GrupoArticulo.h"

@implementation GrupoArticulo

- (id)init
{
	if((self = [super init]))
    {
		self.Codigo = @"";
		self.Nombre = @"";
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Codigo xmlNodeForDoc:node->doc elementName:@"IdProducto" elementNSPrefix:@"tns3"]);
	
	xmlAddChild(node, [self.Nombre xmlNodeForDoc:node->doc elementName:@"Nombre" elementNSPrefix:@"tns3"]);
}

+ (GrupoArticulo *)deserializeNode:(xmlNodePtr)cur
{
	GrupoArticulo *newObject = [GrupoArticulo new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"IdProducto"])
    {
        self.Codigo = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Nombre"])
    {
        self.Nombre = [NSString deserializeNode:pobjCur];
    }
}

@end