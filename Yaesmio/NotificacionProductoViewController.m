//
//  NotificacionProductoViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "NotificacionProductoViewController.h"
#import "OBGradientView.h"
#import "UIAllControls.h"
#import "UIView+Genie.h"

@interface NotificacionProductoViewController ()

@property (nonatomic) IBOutlet UIImageView *controlImagenProducto;
@property (nonatomic) IBOutlet UILabel *labelDescripcion;
@property (nonatomic) IBOutlet OBGradientView *controlVistaDescpricion;

@property (nonatomic) UITapGestureRecognizer *controlGestureTap;

@end

@implementation NotificacionProductoViewController

#define GRUPO_NOTIFICACIONPRODUCTO @"VistaCabeceraCarrito"

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.typeAnimation = TDSemiModalExtensionEnumAparece;
    self.controlGestureTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(CerrarVista)];
    [self.controlVistaDescpricion addGestureRecognizer:self.controlGestureTap];
    
    [self.controlImagenProducto cargarUrlImagenAsincrono:self.urlImagen Busy:nil];
    self.labelDescripcion.text = self.Mensaje;
    
    self.controlVistaDescpricion.BorderColor = [Locator Default].Color.Gris;
    self.controlVistaDescpricion.WidthBorder = 2;
    
    [self performSelector:@selector(CerrarVista) withObject:nil afterDelay:1];
}

- (void)viewUnload
{
    [self setControlImagenProducto:nil];
    [self setLabelDescripcion:nil];
    [self setControlVistaDescpricion:nil];
}

-(void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    if ([self.view window] == nil)
    {
        // Add code to preserve data stored in the views that might be
        // needed later.
        
        // Add code to clean up other strong references to the view in
        // the view hierarchy.
        self.view = nil;
        [self viewUnload];
    }
}

+(void) MostrarNotificacion:(UIViewController*)pView Imagen:(NSString*)purlImage Descripcion:(NSString*)pMensaje Result:(ResultadoNotificacionBlock)completion
{
    NotificacionProductoViewController* lView= [pView.storyboard instantiateViewControllerWithIdentifier:@"NotificacionProductoViewController"];

    lView.urlImagen = purlImage;
    lView.Mensaje = pMensaje;
    lView.Resultado = completion;

    [pView presentSemiModalViewController:lView inView:pView.view.window];
}

-(void) CerrarVista
{
    self.view.backgroundColor = [Locator Default].Color.Clear;
    CGRect endRect = CGRectMake(260, 53, 50, 14);
    [self.controlVistaDescpricion genieInTransitionWithDuration:0.5 destinationRect:endRect destinationEdge:BCRectEdgeBottom completion:^
    {
        [self dismissSemiModalViewController:self];
        if (self.Resultado != nil)
        {
            self.Resultado();
            self.Resultado = nil;
        }
    }];
}

@end
