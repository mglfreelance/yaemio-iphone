//
//  Empresa.h
//  Yaesmio
//
//  Created by freelance on 08/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"
#import "ElementoPicker.h"

@interface Empresa : ElementoPicker

@property Media *Imagen;

+ (Empresa *)deserializeNode:(xmlNodePtr)cur;

@end
