//
//  Provincia.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "Provincia.h"

#define CERCADEMI	@"Cerca de mi"
#define TODAS		@"Todas"

@implementation Provincia

- (id)init
{
    if ((self = [super init]))
    {
        self.Codigo = @"";
        self.Nombre = @"";
    }
    
    return self;
}

- (BOOL)isEqual:(id)object
{
    if ([object isKindOfClass:[Provincia class]])
    {
        Provincia *pais = (Provincia *)object;
        
        return ([self.Codigo isEqualToString:pais.Codigo] && [self.Nombre isEqualToString:pais.Nombre]);
    }
    
    
    return [super isEqual:object];
}

+ (Provincia *)newWithId:(NSString *)pID andName:(NSString *)pName
{
    Provincia *resultado = [[Provincia alloc]init];
    
    resultado.Codigo = pID;
    resultado.Nombre = pName;
    
    return resultado;
}

+ (Provincia *)Todas
{
    return [Provincia newWithId:@"" andName:Traducir(TODAS)];
}

+ (Provincia *)CercaDeMi
{
    return [Provincia newWithId:@"" andName:Traducir(CERCADEMI)];
}

- (BOOL)esTodas
{
    return ([self.Nombre isEqualToString:Traducir(TODAS)]);
}

- (BOOL)esCercaDeMi
{
    return ([self.Nombre isEqualToString:Traducir(CERCADEMI)]);
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Codigo xmlNodeForDoc:node->doc elementName:@"ID" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Nombre xmlNodeForDoc:node->doc elementName:@"NombreProvincia" elementNSPrefix:@"tns3"]);
}

+ (Provincia *)deserializeNode:(xmlNodePtr)cur
{
    Provincia *newObject = [Provincia new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ID"] || [nodeName isEqualToString:@"IdProvincia"])
    {
        self.Codigo = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NombreProvincia"])
    {
        self.Nombre = [NSString deserializeNode:pobjCur];
    }
}

@end