//
//  PayPalViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 03/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "PayPalViewController.h"
#import "UIAllControls.h"
#import "PayPalMobile.h"

@interface PayPalViewController () <PayPalPaymentDelegate>

@property (strong) PayPalConfiguration *paypalConfig;

@end

@implementation PayPalViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewUnload
{
    [self setRespond:nil];
    [self setCarrito:nil];
    [self setMailEmpresa:nil];
    [self setUrlWebEmpresa:nil];
    [self setUsuario:nil];
    [self setDireccionEnvio:nil];
    [self setPaypalConfig:nil];
}

#pragma mark- Paypal Mobile

- (instancetype)initWithCarrito:(EmpresaCarrito *)pCarrito Usuario:(Usuario *)pUsuario
{
    if (self.paypalConfig == nil)
    {
        // Set up payPalConfig
        self.paypalConfig = [[PayPalConfiguration alloc] init];
        self.paypalConfig.acceptCreditCards = YES;
        self.paypalConfig.languageOrLocale = [Locator Default].Idioma.IdiomaDelSistema;
        self.paypalConfig.defaultUserEmail = pUsuario.Mail;
        self.paypalConfig.rememberUser = YES;
        self.paypalConfig.merchantName = Traducir(@"empresa_yaesmio");
        self.paypalConfig.merchantPrivacyPolicyURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/privacy-full"];
        self.paypalConfig.merchantUserAgreementURL = [NSURL URLWithString:@"https://www.paypal.com/webapps/mpp/ua/useragreement-full"];
        
        [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentSandbox: @"AcjphhAO9a8xbKaKEZsLYvwl2KurCRo2LRhWwOJzMTUZNH-NosMh--xlIvLN", PayPalEnvironmentProduction: [Locator Default].Preferencias.IdPaypal}];
        
        [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentProduction];
#if DEBUG
        [PayPalMobile preconnectWithEnvironment:PayPalEnvironmentSandbox];
#endif
    }
    
    NSMutableArray *ListaItems = [NSMutableArray new];
    
    for (ElementoCarrito *elemento in pCarrito.Elementos)
    {
        PayPalItem *item = [PayPalItem itemWithName:[self ObtenerNombreProductoCompleto:elemento.Producto] withQuantity:elemento.Unidades.integerValue withPrice:elemento.Producto.ProductoHijo.Precio withCurrency:pCarrito.Moneda withSku:elemento.Producto.ProductoHijo.Referencia];
        [ListaItems addObject:item];
    }
    
    NSDecimalNumber *subtotal = [PayPalItem totalPriceForItems:ListaItems];
    PayPalPaymentDetails *paymentDetails = nil;
    
    if (pCarrito.GastosEnvio.integerValue != 0)
    {
        paymentDetails = [PayPalPaymentDetails paymentDetailsWithSubtotal:subtotal withShipping:pCarrito.GastosEnvio withTax:[NSDecimalNumber zero]];
    }
    
    NSDecimalNumber *total = [subtotal decimalNumberByAdding:pCarrito.GastosEnvio];
    
    PayPalPayment *payment = [[PayPalPayment alloc] init];
    payment.amount = total;
    payment.currencyCode = pCarrito.Moneda;
    payment.shortDescription = Traducir(@"Compra en Yaesmio");
    payment.items = ListaItems;
    payment.paymentDetails = paymentDetails;
    
    if (!payment.processable)
    {
        [self FinalizarCompra:nil];
        return nil;
    }
    
    if ((self = [super initWithPayment:payment configuration:self.paypalConfig delegate:self]))
    {
    }
    
    NSLog(@"PayPal iOS SDK version: %@", [PayPalMobile libraryVersion]);
    
    return self;
}

- (void)payPalPaymentDidCancel:(PayPalPaymentViewController *)paymentViewController
{
    [self FinalizarCompra:@""];
}

- (void)payPalPaymentViewController:(PayPalPaymentViewController *)paymentViewController didCompletePayment:(PayPalPayment *)completedPayment
{
    NSString *resultado = @"";
    
    for (NSString *item in completedPayment.confirmation.allKeys)
    {
        if ([[item lowercaseString] isEqualToString:@"response"])
        {
            NSDictionary *ListResponse = completedPayment.confirmation[item];
            
            for (NSString *Dato in ListResponse.allKeys)
            {
                if ([[Dato lowercaseString] isEqualToString:@"id"])
                {
                    resultado = ListResponse[Dato];
                }
            }
        }
    }
    
    [self FinalizarCompra:resultado];
}

#pragma mark- Metodos Privados

- (NSString *)ObtenerNombreProductoCompleto:(ProductoPadre *)pProducto
{
    return [NSString stringWithFormat:@"%@ %@", pProducto.ProductoHijo.Nombre, pProducto.TextoAtributos];
}

- (void)FinalizarCompra:(NSString *)pRespuesta
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if (self.Respond != nil)
    {
        self.Respond(pRespuesta);
        [self viewUnload];         // libero todo
    }
}

@end
