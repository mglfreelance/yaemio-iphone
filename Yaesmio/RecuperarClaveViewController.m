//
//  RecuperarClaveViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import "RecuperarClaveViewController.h"
#import "UIAllControls.h"

@interface RecuperarClaveViewController ()

@property (nonatomic) IBOutlet MKGradientButton *btnRecuperarClave;
@property (nonatomic) IBOutlet UIScrollView *scrollViewDatos;
@property (nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCamposTraducir;

@property (nonatomic) ScrollManager *manager;

- (IBAction)btnRecuperarClave_click:(MKGradientButton *)sender;

@end

@implementation RecuperarClaveViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaCamposTraducir);
    
    [self.scrollViewDatos setHeightContentSizeForSubviewUntilControl:self.btnRecuperarClave];
    
    self.manager = [ScrollManager NewAndConfigureWithScrollView:self.scrollViewDatos AndDidEndTextField:self Selector:@selector(btnRecuperarClave_click:)];
    
    self.txtEmail.text = [Locator Default].Preferencias.MailUsuario;
}

- (void)viewUnload
{
    [self setTxtEmail:nil];
    [self setBtnRecuperarClave:nil];
    [self setListaCamposTraducir:nil];
    [self setManager:nil];
}

#pragma mark- Eventos Internos

- (IBAction)btnRecuperarClave_click:(MKGradientButton *)sender
{
    [[Locator Default].Alerta showBussy:Traducir(@"Reestableciendo clave.")];
    [[Locator Default].ServicioSOAP RecuperarPasswordConEmail:self.txtEmail.text Result:^(SRResultMethod pResult)
     {
         if (pResult == SRResultMethodErrConex)
         {
             [[Locator Default].Alerta showErrorConexSOAP];
         }
         else if (pResult == SRResultMethodYES)
         {
             [[Locator Default].Alerta showMessageAcept:Traducir(@"Se ha enviado la nueva contraseña a su correo electronico.") ResulBlock:^
              {
                  [self.navigationController popToViewController:self.VistaInicial animated:true];
                  
                  if (self.Resultado != nil)
                  {
                      self.Resultado(0);
                  }
              }];
         }
         else if (pResult == SRResultMethodNO)
         {
             [[Locator Default].Alerta showMessageAcept:Traducir(@"No existe ningun usuario con ese correo.")];
         }
     }];
}

@end
