//
//  ProductoCarrito.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ElementoCarrito.h"
#import "Atributo.h"

@implementation ElementoCarrito

+ (ElementoCarrito *)crear:(ProductoPadre *)pProducto Unidades:(NSDecimalNumber *)pUnidades
{
    ElementoCarrito *pobjResultado = [[ElementoCarrito alloc] init];
    
    pobjResultado.Producto = pProducto;
    pobjResultado.Unidades = pUnidades;
    
    return pobjResultado;
}

- (NSString *)TextoPropiedades
{
    return [[NSString stringWithFormat:@"%@ %@ %@", self.Producto.TextoPropiedades, self.Producto.ProductoHijo.DescUnidadesMedida, self.Unidades] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (NSDecimalNumber *)TotalPrecio
{
    return [self.Producto.ProductoHijo.Precio decimalNumberByMultiplyingBy:self.Unidades];
}

- (BOOL)ExisteSuficienteStock
{
    return (self.Producto.ProductoHijo.Stock.intValue >= self.Unidades.intValue);
}

- (BOOL)SePuedeComprar
{
    return self.numeroUnidadesMaximoCompra >= self.Unidades.intValue && self.Unidades.isGreaterZero;
}

- (long)numeroUnidadesMaximoCompra
{
    int Stock = self.Producto.ProductoHijo.Stock.intValue;
    int CantidadMaxima = self.Producto.CantidadMax.intValue;
    
    if (Stock < CantidadMaxima)
    {
        return Stock;
    }
    else
    {
        return CantidadMaxima;
    }
}

- (BOOL)sePuedeAgregarUnidadesProducto:(NSDecimalNumber *)pUnidades
{
    BOOL result = false;
    
    int lUnidades = [self.Unidades intValue] + [pUnidades intValue];
    
    result = (lUnidades < [self.Producto.CantidadMax intValue]);
    
    return result;
}

- (BOOL)isEqualTo:(ElementoCarrito *)pElemento
{
    if (self != nil && pElemento != nil)
    {
        return [self.Producto.ProductoId isEqualToString:pElemento.Producto.ProductoId] && [self.Producto.ProductoHijo.ProductoId isEqualToString:pElemento.Producto.ProductoHijo.ProductoId];
    }
    else
    {
        return false;
    }
}

#pragma mark- Metodos SOAP

- (id)init
{
    if ((self = [super init]))
    {
        self.Producto = nil;
        self.Unidades = nil;
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Producto.TipoCanalString xmlNodeForDoc:node->doc elementName:@"Canal" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Producto.ProductoHijo.Latitud xmlNodeForDoc:node->doc elementName:@"Latitud" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Producto.ProductoHijo.Longitud xmlNodeForDoc:node->doc elementName:@"Longitud" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Producto.Medio xmlNodeForDoc:node->doc elementName:@"Medio" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Producto.ProductoHijo.ProductoId xmlNodeForDoc:node->doc elementName:@"ProductoId" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Unidades xmlNodeForDoc:node->doc elementName:@"UnidadesCompradas" elementNSPrefix:@"tns3"]);
}

+ (Producto *)deserializeNode:(xmlNodePtr)cur
{
    Producto *newObject = [Producto new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)elementName
{
    return @"ProductoComprado";
}

- (NSString *)nsPrefix
{
    return @"tns3";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
}

@end
