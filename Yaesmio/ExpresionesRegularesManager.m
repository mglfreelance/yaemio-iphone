//
//  ExpresionesRegularesManager.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 30/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ExpresionesRegularesManager.h"


@implementation ExpresionesRegularesManager


-(BOOL) ComprobarMail:(NSString*)pMail
{
    return [self ComprobarCadena:pMail ConExpresion:@"^[a-zA-Z0-9._-]{2,30}@[a-zA-Z0-9_-]{2,30}.[a-zA-Z]{2,4}(.[a-zA-Z]{2,4})?$" ];
}

-(BOOL) ComprobarPassword:(NSString*)pClave
{
   return [self ComprobarCadena:pClave ConExpresion:@"^(?=.*[0-9])(?=.*[a-zA-ZñÑ]).{8,15}$"];
}

-(BOOL) ComprobarNombre:(NSString*)pNombre
{
    return [self ComprobarCadena:pNombre ConExpresion:@"^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]{3,50}$"];
}

-(BOOL) ComprobarApellidos:(NSString*)pApellidos
{
    return [self ComprobarCadena:pApellidos ConExpresion:@"^[a-zA-ZñÑáéíóúÁÉÍÓÚ ]{3,60}$"];
}

-(BOOL) ComprobarDireccion:(NSString*)pDireccion
{
    return [self ComprobarCadena:pDireccion ConExpresion:@"^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9 ]{10,100}$"];
}

-(BOOL) ComprobarTelefono:(NSString*)pTelefono
{
    return [self ComprobarCadena:pTelefono ConExpresion:@"^[0-9]{9}$"];
}


-(BOOL) ComprobarCodigoPostal:(NSString*)pCodigoPostal
{
    return [self ComprobarCadena:pCodigoPostal ConExpresion:@"^[0-9]{5}$"];
}

-(BOOL) ComprobarPoblacion:(NSString*)pPoblacion
{
    return [self ComprobarCadena:pPoblacion ConExpresion:@"^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9 ]{4,50}$"];
}

-(BOOL) ComprobarNumero:(NSString*)pNumero
{
    return [self ComprobarCadena:pNumero ConExpresion:@"^[0-9]{3}$"];
}

-(BOOL) ComprobarDatoDireccion:(NSString*)pDireccion
{
    return [self ComprobarCadena:pDireccion ConExpresion:@"^[a-zA-ZñÑáéíóúÁÉÍÓÚ0-9 ]{1,10}$"];
}



#pragma mark - Metodos Privados

-(BOOL) ComprobarCadena:(NSString*)pCadena ConExpresion:(NSString*)pExpresion
{
    BOOL lbolResultado = true;
    
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pExpresion options:NSRegularExpressionCaseInsensitive error:&error];
    
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:pCadena options:0 range:NSMakeRange(0, [pCadena length])];
    
    lbolResultado = (numberOfMatches >0);
    
    return lbolResultado;
}

@end
