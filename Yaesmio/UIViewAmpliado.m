#import "UIViewAmpliado.h"

@interface UIView (MetodosPrivados)

- (void)setAutoresizingBit:(unsigned int)bitMask toValue:(BOOL)set;

@end


@implementation UIView (Ampliado)

- (void)mgLayoutSubviewsReloadIgnoreWidth:(BOOL)pWidth Height:(BOOL)pHeight
{
    CGSize lValor = [self mgRelocateContent];
    
    CGFloat lWidth = pWidth ? self.frame.size.width : lValor.width;
    CGFloat lHeight = pHeight ? self.frame.size.height : lValor.height;
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, lWidth, lHeight);
}

- (void)mgLayoutSubviewsReload
{
    [self mgLayoutSubviewsReloadIgnoreWidth:false Height:false];
}

- (CGRect)getSize:(CGFloat)pflMaxWidth MaxHeight:(CGFloat)pflMaxHeight AgregateWidth:(CGFloat)pflAgregateWidth AgregateHeight:(CGFloat)pflAgregateHeight
{
    CGFloat lflWidth = pflMaxWidth;
    CGFloat lflHeight = pflMaxHeight;
    CGSize AnchoLabel;
    
    if (pflMaxWidth == UNCHANGED)               // si se le pasa unchanged no se cambia el ancho
    {
        lflWidth = self.frame.size.width;
    }
    
    if (pflMaxHeight == UNCHANGED)              // si se le pasa unchanged no se cambia el alto
    {
        lflHeight = self.frame.size.height;
    }
    
    if ([self isMemberOfClass:[UIView class]])  // si es una uiview calculamos diferente.
    {
        AnchoLabel = [self mgGetSizeSubViews];
    }
    else
    {
        AnchoLabel = [self sizeThatFits:CGSizeMake(lflWidth, lflHeight)];
    }
    
    // recojo el ancho y alto generado anteriormente.
    if (pflMaxWidth == UNCHANGED)               // si no podemos modificarlo
    {
        lflWidth = self.frame.size.width;
    }
    else if (pflMaxWidth == INFINITY)
    {
        lflWidth = AnchoLabel.width;
    }
    else if (AnchoLabel.width > pflMaxWidth)               // no puede superar el maximo
    {
        lflWidth = pflMaxWidth;
    }
    else
    {
        lflWidth = AnchoLabel.width;
    }
    
    if (pflMaxHeight == UNCHANGED)               // si no podemos modificarlo
    {
        lflHeight = self.frame.size.height;
    }
    else if (pflMaxHeight ==  INFINITY)
    {
        lflHeight = AnchoLabel.height;
    }
    else if (AnchoLabel.height > pflMaxHeight)               // no puede superar el maximo
    {
        lflHeight = pflMaxHeight;
    }
    else
    {
        lflHeight = AnchoLabel.height;
    }
    
    return CGRectMake(self.frame.origin.x, self.frame.origin.y, lflWidth + pflAgregateWidth, lflHeight + pflAgregateHeight);
}

// asigna un nuevo ancho alto para mostrar todo el contenido
- (void)mgSizeFitsMaxWidth:(CGFloat)pflMaxWidth MaxHeight:(CGFloat)pflMaxHeight
{
    [self mgSizeFitsMaxWidth:pflMaxWidth MaxHeight:pflMaxHeight AgregateWidth:0.0 AgregateHeight:0.0];
}

// asigna un nuevo ancho alto para mostrar todo el contenido,
// ademas da opcion de añadir en el ancho y alto.(admite unchanged)
- (void)mgSizeFitsMaxWidth:(CGFloat)pflMaxWidth MaxHeight:(CGFloat)pflMaxHeight AgregateWidth:(CGFloat)pflAgregateWidth AgregateHeight:(CGFloat)pflAgregateHeight
{
    self.frame = [self getSize:pflMaxWidth MaxHeight:pflMaxHeight AgregateWidth:pflAgregateWidth AgregateHeight:pflAgregateHeight];
}

- (void)mgCenterInSuperView
{
    float left = 0;
    float top = 0;
    
    if (self.superview != nil)
    {
        left = (self.superview.frame.size.width - self.frame.size.width) / 2;
        top = (self.superview.frame.size.height - self.frame.size.height) / 2;
        
        [self mgSetSizeX:left andY:top andWidth:UNCHANGED andHeight:UNCHANGED];
    }
}

- (void)mgMoveLeft:(int)X AndTop:(int)Y
{
    [self mgSetSizeX:X andY:Y andWidth:UNCHANGED andHeight:UNCHANGED];
}

- (void)mgMoveInSuperView:(MGPositionInViewEnum)pTipo
{
    [self mgMoveInSuperView:pTipo AgregateHorizontal:0.0 AgregateVertical:0.0];
}

- (void)mgMoveInSuperView:(MGPositionInViewEnum)pTipo AgregateHorizontal:(CGFloat)pflAgregateHorizontal AgregateVertical:(CGFloat)pflAgregateVertical
{
    float left = 0;
    float top = 0;
    
    if (self.superview != nil)
    {
        switch (pTipo)
        {
            case MGPositionInViewToTop:
                left = self.frame.origin.x + pflAgregateHorizontal;
                top = 0 + pflAgregateVertical;
                break;
                
            case MGPositionInViewToBotton:
                left = self.frame.origin.x + pflAgregateHorizontal;
                top = (self.superview.frame.size.height - self.frame.size.height - pflAgregateVertical);
                break;
                
            case MGPositionInViewToRight:
                left = (self.superview.frame.size.width - self.frame.size.width - pflAgregateHorizontal);
                top = self.frame.origin.y + pflAgregateVertical;
                break;
                
            case MGPositionInViewToLeft:
                left = 0 + pflAgregateHorizontal;
                top = self.frame.origin.y + pflAgregateVertical;
                break;
                
            case MGPositionInViewToCenter:
                left = ((self.superview.frame.size.width - self.frame.size.width) / 2) + pflAgregateHorizontal;
                top = ((self.superview.frame.size.height - self.frame.size.height) / 2)+ pflAgregateVertical;
                break;
                
            case MGPositionInViewToTopLeft:
                left = (self.superview.frame.size.width - self.frame.size.width - pflAgregateHorizontal);
                top = 0 + pflAgregateVertical;
                break;
                
            case MGPositionInViewToTopRight:
                left = 0 + pflAgregateHorizontal;
                top = 0 + pflAgregateVertical;
                break;
                
            case MGPositionInViewToCenterHorizontal:
                left = ((self.superview.frame.size.width - self.frame.size.width) / 2) + pflAgregateHorizontal;
                top = self.frame.origin.y + pflAgregateVertical;
                break;
                
            case MGPositionInViewToCenterVertical:
                left = self.frame.origin.x + pflAgregateHorizontal;
                top = ((self.superview.frame.size.height - self.frame.size.height) / 2)+ pflAgregateVertical;
                break;
                
            case MGPositionInViewToBottonLeft:
                left = (self.superview.frame.size.width - self.frame.size.width - pflAgregateHorizontal);
                top = (self.superview.frame.size.height - self.frame.size.height - pflAgregateVertical);
                break;
                
            case MGPositionInViewToBottonRight:
                left = 0 + pflAgregateHorizontal;
                top = ((self.superview.frame.size.height - self.frame.size.height) / 2)+ pflAgregateVertical;
                break;
                
            case MGPositionInViewToCenterLeft:
                left = (self.superview.frame.size.width - self.frame.size.width - pflAgregateHorizontal);
                top = ((self.superview.frame.size.height - self.frame.size.height) / 2) + pflAgregateVertical;
                break;
                
            case MGPositionInViewToCenterRight:
                left = 0 + pflAgregateHorizontal;
                top = ((self.superview.frame.size.height - self.frame.size.height) / 2) + pflAgregateVertical;
                break;
                
            default:
                break;
        }
        
        [self mgSetSizeX:left andY:top andWidth:UNCHANGED andHeight:UNCHANGED];
    }
}

// asigna el frame directamente.
- (void)mgSetSizeX:(CGFloat)pflx andY:(CGFloat)pfly andWidth:(CGFloat)pflwidth andHeight:(CGFloat)pflHeight
{
    if (pfly == UNCHANGED)
    {
        pfly = self.frame.origin.y;
    }
    
    if (pflx == UNCHANGED)
    {
        pflx = self.frame.origin.x;
    }
    
    if (pflwidth == UNCHANGED)
    {
        pflwidth = self.frame.size.width;
    }
    
    if (pflHeight == UNCHANGED)
    {
        pflHeight = self.frame.size.height;
    }
    
    
    self.frame = CGRectMake(pflx, pfly, pflwidth, pflHeight);
}

//extiende el control en alto o ancho por la Izquierda
- (void)mgExtendTopLeftForX:(CGFloat)pflx andY:(CGFloat)pfly
{
    float pflwidth = self.frame.size.width;
    float pflHeight = self.frame.size.height;
    
    if (pfly == UNCHANGED)
    {
        pfly = self.frame.origin.y;
    }
    else
    {
        pflHeight += (self.frame.origin.y - pfly);
    }
    
    if (pflx == UNCHANGED)
    {
        pflx = self.frame.origin.x;
    }
    else
    {
        pflwidth += (self.frame.origin.x - pflx);
    }
    
    self.frame = CGRectMake(pflx, pfly, pflwidth, pflHeight);
}

//extiende el control en alto o ancho por la derecha
- (void)mgExtendBottonRightForX:(CGFloat)pflx andY:(CGFloat)pfly
{
    float pflwidth = self.frame.size.width;
    float pflHeight = self.frame.size.height;
    
    if (pfly != UNCHANGED)
    {
        pflHeight += pfly;
    }
    
    if (pflx != UNCHANGED)
    {
        pflwidth += pflx;
    }
    
    [self mgSetSizeX:UNCHANGED andY:UNCHANGED andWidth:pflx andHeight:pfly];
}

- (CGSize)mgGetSizeSubViews
{
    CGFloat widestWidth = 0.0f;
    CGFloat widestHeight = 0.0f;
    CGFloat preValor = 0.0f;
    
    for (UIView *view in self.subviews)
    {
        if (view.hidden == FALSE)
        {
            // calculo el ancho
            preValor = view.frame.origin.x + view.frame.size.width;
            
            if (widestWidth < preValor)
            {
                widestWidth = preValor;
            }
            
            // calculo el alto
            preValor = view.frame.origin.y + view.frame.size.height;
            
            if (widestHeight < preValor)
            {
                widestHeight = preValor;
            }
        }
    }
    
    return CGSizeMake(widestWidth, widestHeight);
}

- (CGSize)mgRelocateContent
{
    return [self mgRelocateContentAndIgnoreControls:nil];
}

- (CGSize)mgRelocateContentAndIgnoreControls:(NSArray *)pControls
{
    GLuint runningY = 0;
    CGFloat widestWidth = 0.0;
    
    for (UIView *view in self.subviews)
    {
        if (view.hidden == FALSE && [view isKindOfClass:[UIView class]])                           // si no esta oculto se calcula
        {
            if ([self findControl:view Array:pControls] == false)
            {
                if ([view autoresizingMask] == UIViewAutoresizingFlexibleWidth)
                {
                    view.frame = CGRectMake(view.frame.origin.x, runningY, self.frame.size.width, view.frame.size.height);
                }
                else
                {
                    view.frame = CGRectMake(view.frame.origin.x, runningY, view.frame.size.width, view.frame.size.height);
                }
            }
            else
            {
                if (runningY == 0)                                                   // al no mover estos controles tengo que saber donde empezo
                {
                    runningY = view.frame.origin.y;
                }
            }
            
            runningY += view.frame.size.height;
            
            if (view.frame.size.width > widestWidth)
            {
                widestWidth = view.frame.size.width;
            }
        }
    }
    
    return CGSizeMake(widestWidth, runningY);
}

- (void)mgMoveToControl:(UIView *)pView Position:(MGPositionMoveEnum)pPosition Separation:(float)pSeparation
{
    float xdiference = 0.0;
    float ydiference = 0.0;
    
    switch (pPosition)
    {
        case MGPositionMoveToTop:
            [self mgSetSizeX:UNCHANGED andY:pView.frame.origin.y + pSeparation andWidth:UNCHANGED andHeight:UNCHANGED];
            break;
            
        case MGPositionMoveToBotton:
            [self mgSetSizeX:UNCHANGED andY:pView.frame.origin.y + pView.frame.size.height - self.frame.size.height - pSeparation andWidth:UNCHANGED andHeight:UNCHANGED];
            break;
            
        case MGPositionMoveToLeft:
            [self mgSetSizeX:pView.frame.origin.x + pSeparation andY:UNCHANGED andWidth:UNCHANGED andHeight:UNCHANGED];
            break;
            
        case MGPositionMoveToRight:
            [self mgSetSizeX:pView.frame.origin.x + pView.frame.size.width - self.frame.size.width - pSeparation andY:UNCHANGED andWidth:UNCHANGED andHeight:UNCHANGED];
            break;
            
        case MGPositionMoveToCenter:
            xdiference = (pView.frame.size.width - self.frame.size.width) / 2.0;
            ydiference = (pView.frame.size.height - self.frame.size.height) / 2.0;
            [self mgSetSizeX:pView.frame.origin.x + xdiference + pSeparation andY:pView.frame.origin.y + ydiference + pSeparation andWidth:UNCHANGED andHeight:UNCHANGED];
            break;
            
        case MGPositionMoveToTopLeft:
            [self mgMoveToControl:pView Position:MGPositionMoveToTop Separation:pSeparation];
            [self mgMoveToControl:pView Position:MGPositionMoveToLeft Separation:pSeparation];
            break;
            
        case MGPositionMoveToTopRight:
            [self mgMoveToControl:pView Position:MGPositionMoveToTop Separation:pSeparation];
            [self mgMoveToControl:pView Position:MGPositionMoveToRight Separation:pSeparation];
            break;
            
        case MGPositionMoveToBottonLeft:
            [self mgMoveToControl:pView Position:MGPositionMoveToBotton Separation:pSeparation];
            [self mgMoveToControl:pView Position:MGPositionMoveToLeft Separation:pSeparation];
            break;
            
        case MGPositionMoveToBottonRight:
            [self mgMoveToControl:pView Position:MGPositionMoveToBotton Separation:pSeparation];
            [self mgMoveToControl:pView Position:MGPositionMoveToRight Separation:pSeparation];
            break;
            
        case MGPositionMoveToCenterLeft:
            [self mgMoveToControl:pView Position:MGPositionMoveToCenter Separation:pSeparation];
            [self mgMoveToControl:pView Position:MGPositionMoveToLeft Separation:pSeparation];
            break;
            
        case MGPositionMoveToCenterRight:
            [self mgMoveToControl:pView Position:MGPositionMoveToCenter Separation:pSeparation];
            [self mgMoveToControl:pView Position:MGPositionMoveToRight Separation:pSeparation];
            break;
            
        case MGPositionMoveToBelow:
            [self mgSetSizeX:UNCHANGED andY:pView.frame.origin.y + pView.frame.size.height + pSeparation andWidth:UNCHANGED andHeight:UNCHANGED];
            break;
            
        case MGPositionMoveToBefore:
            [self mgSetSizeX:pView.frame.origin.x - self.frame.size.width - pSeparation andY:UNCHANGED andWidth:UNCHANGED andHeight:UNCHANGED];
            break;
            
        case MGPositionMoveToBehind:
            [self mgSetSizeX:pView.frame.origin.x + pView.frame.size.width + pSeparation andY:UNCHANGED andWidth:UNCHANGED andHeight:UNCHANGED];
            break;
            
        case MGPositionMoveToAdove:
            [self mgSetSizeX:UNCHANGED andY:pView.frame.origin.y - self.frame.size.height - pSeparation andWidth:UNCHANGED andHeight:UNCHANGED];
            break;
    }
}

#pragma mark - Autoresising

- (void)setAutoresizingBit:(unsigned int)bitMask toValue:(BOOL)set
{
    if (set)
    {
        [self setAutoresizingMask:([self autoresizingMask] | bitMask)];
    }
    else
    {
        [self setAutoresizingMask:([self autoresizingMask] & ~bitMask)];
    }
}

- (void)mgFixLeftEdge:(BOOL)fixed
{
    [self setAutoresizingBit:UIViewAutoresizingFlexibleLeftMargin toValue:!fixed];
}

- (void)mgFixRightEdge:(BOOL)fixed
{
    [self setAutoresizingBit:UIViewAutoresizingFlexibleRightMargin toValue:!fixed];
}

- (void)mgFixTopEdge:(BOOL)fixed
{
    [self setAutoresizingBit:UIViewAutoresizingFlexibleTopMargin toValue:!fixed];
}

- (void)mgFixBottomEdge:(BOOL)fixed
{
    [self setAutoresizingBit:UIViewAutoresizingFlexibleBottomMargin toValue:!fixed];
}

- (void)mgFixWidth:(BOOL)fixed
{
    [self setAutoresizingBit:UIViewAutoresizingFlexibleWidth toValue:!fixed];
}

- (void)mgFixHeight:(BOOL)fixed
{
    [self setAutoresizingBit:UIViewAutoresizingFlexibleHeight toValue:!fixed];
}

- (BOOL)findControl:(UIView *)pControl Array:(NSArray *)pList
{
    BOOL result = false;
    
    if (pList != nil)
    {
        for (UIView *lView in pList)
        {
            if (lView == pControl)
            {
                result = true;
                break;
            }
        }
    }
    
    return result;
}

#pragma Gesture Recognizer

// agrega Gesture Recognizer TAP
- (UIGestureRecognizer *)mgAddGestureRecognizerTap:(id)target Action:(SEL)pAction
{
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:target action:pAction];
    
    recognizer.numberOfTouchesRequired = 1;
    recognizer.delegate = target;
    recognizer.numberOfTapsRequired = 1;
    self.userInteractionEnabled = true;
    [self addGestureRecognizer:recognizer];
    return recognizer;
}

// agrega Gesture Recognizer SWIPE
- (UIGestureRecognizer *)mgAddGestureRecognizerSwipe:(id)target Action:(SEL)pAction Direccion:(UISwipeGestureRecognizerDirection)pDireccion
{
    UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:target action:pAction];
    
    recognizer.numberOfTouchesRequired = 1;
    recognizer.delegate = target;
    recognizer.direction = pDireccion;
    [self addGestureRecognizer:recognizer];
    return recognizer;
}

- (UIGestureRecognizer *)mgAddGestureRecognizerLongPress:(id)target Action:(SEL)pAction
{
    UILongPressGestureRecognizer *recognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:target action:pAction];
    
    self.userInteractionEnabled = true;
    [self addGestureRecognizer:recognizer];
    return recognizer;
}

- (UIGestureRecognizer *)mgAddGestureRecognizerPan:(id)target Action:(SEL)pAction
{
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc] initWithTarget:target action:pAction];
    
    self.userInteractionEnabled = true;
    [self addGestureRecognizer:recognizer];
    return recognizer;
}

- (void)mgRemoveAllGestureRecognizers
{
    for (UIGestureRecognizer *gesture in self.gestureRecognizers)
    {
        if (gesture != nil)
        {
            [self removeGestureRecognizer:gesture];
        }
    }
}

- (UIColor *)BorderColor
{
    return [UIColor colorWithCGColor:self.layer.borderColor];
}

- (void)setBorderColor:(UIColor *)pColor
{
    if (pColor != nil)
    {
        self.layer.borderColor = pColor.CGColor;
    }
}

@end
