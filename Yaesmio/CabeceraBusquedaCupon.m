//
//  CabeceraBusquedaCupon.m
//  Yaesmio
//
//  Created by freelance on 19/08/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "CabeceraBusquedaCupon.h"
#import "UIAllControls.h"

@interface CabeceraBusquedaCupon ()

@property (weak, nonatomic) IBOutlet UILabel *title;

@end

@implementation CabeceraBusquedaCupon

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        // Initialization code
    }
    
    return self;
}

-(void) Ocultar
{
    [self mgSetSizeX:UNCHANGED andY:UNCHANGED andWidth:UNCHANGED andHeight:0.0];
}

- (void)Configurar:(GrupoDistanciaProductoBuscado)Grupo NumeroElementos:(NSInteger)NumeroElementos
{
    [self Inicializar];
    [self mgSetSizeX:UNCHANGED andY:UNCHANGED andWidth:UNCHANGED andHeight:35.0];
    
    NSString *Texto = @"";
    
    switch (Grupo)
    {
        case GrupoDistanciaProductoBuscadoMenos200m:
            Texto = Traducir(@"0 a 200m");
            break;
            
        case GrupoDistanciaProductoBuscadoMenos500m:
            Texto = Traducir(@"200m a 500m");
            break;
            
        case GrupoDistanciaProductoBuscadoMenos3km:
            Texto = Traducir(@"500m a 3km");
            break;
            
        case GrupoDistanciaProductoBuscadoMenos10km:
            Texto = Traducir(@"3km a 10km");
            break;
            
        case GrupoDistanciaProductoBuscadoMenos50km:
            Texto = Traducir(@"10km a 50km");
            break;
            
        case GrupoDistanciaProductoBuscadoMenos200km:
            Texto = Traducir(@"50km a 200km");
            break;
            
        case GrupoDistanciaProductoBuscadoMayor200km:
            Texto = Traducir(@"mas de 200km");
            break;
    }
    
    self.title.text = [NSString stringWithFormat:@"%@ (%d productos)", Texto, (int)NumeroElementos];
}

- (void)Inicializar
{
    self.title.text = @"";
}

@end
