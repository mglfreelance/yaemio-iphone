//
//  ContactoTelefonoCell.m
//  Yaesmio
//
//  Created by freelance on 21/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ContactoTelefonoCell.h"
#import "GrupoNotificacion.h"
#import "Contacto.h"
#import "PAImageView.h"

@interface ContactoTelefonoCell ()

@property (weak, nonatomic) IBOutlet UILabel *NombreContacto;
@property (weak, nonatomic) IBOutlet PAImageView *ImageContacto;

@end

@implementation ContactoTelefonoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        [self inicializar];
    }
    
    return self;
}

- (void)inicializar
{
    [self.ImageContacto setImage:[UIImage imageNamed:@"sin_foto_usuario"] animated:NO];
    self.ImageContacto.progressColor = [Locator Default].Color.Principal;
    self.NombreContacto.text = @"";
}

- (void)configurar:(id)Objeto Seleccionado:(BOOL)pSeleccionado
{
    [self inicializar];
    
    if ([Objeto isKindOfClass:[Contacto class]])
    {
        Contacto *myContacto = (Contacto *)Objeto;
        UIImage *image = myContacto.ImagenAMostrar;
        
        if (image != nil)
        {
            [self.ImageContacto setImage:image animated:YES];
        }
        
        self.NombreContacto.text = myContacto.NombreAMostrar;
    }
    else if ([Objeto isKindOfClass:[GrupoNotificacion class]])
    {
        GrupoNotificacion *myGrupo = (GrupoNotificacion *)Objeto;
        [self.ImageContacto setImage:[Locator Default].Imagen.GrupoNotificacion animated:YES];
        self.NombreContacto.text = myGrupo.NombreGrupo;
    }
    
    self.Seleccionado = pSeleccionado;
}

- (BOOL)Seleccionado
{
    return (self.accessoryType == UITableViewCellAccessoryCheckmark);
}

- (void)setSeleccionado:(BOOL)pSeleccionado
{
    if (pSeleccionado)
    {
        self.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        self.accessoryType = UITableViewCellAccessoryNone;
    }
}

@end
