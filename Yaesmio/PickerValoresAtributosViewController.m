//
//  PickerValoresAtributosViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 19/07/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "PickerValoresAtributosViewController.h"

@interface PickerValoresAtributosViewController ()<UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic) IBOutlet UIBarButtonItem *controlBotonCancelar;
@property (nonatomic) IBOutlet UIBarButtonItem *controlBotonAceptar;
@property (nonatomic) IBOutlet UIToolbar *controlToolBar;
@property (nonatomic) IBOutlet UIPickerView *ControlPicker;

@property (nonatomic) NSMutableArray *ListaCadenaElementos1;
@property (nonatomic) NSMutableArray *ListaCadenaElementos2;
@property (readonly)  BOOL existeColumna1;
@property (readonly)  BOOL existeColumna2;

- (IBAction)btnDone_click:(id)sender;
- (IBAction)btnCancel_click:(id)sender;

@end

@implementation PickerValoresAtributosViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        self.controlToolBar.tintColor =[Locator Default].Color.Principal;
    }
    
    self.controlBotonAceptar.title = Traducir(@"Aceptar");
    self.controlBotonCancelar.title = Traducir(@"Cancelar");
    
    [self CargarListaCadenaElemento1];
    
    NSString *select = nil;
    
    if (self.ElementoSeleccionado != nil && self.existeColumna2)
    {
        select = self.ElementoSeleccionado.DescripcionAtt1;
    }
    
    [self CargarListaCadenaElemento2:select];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.ElementoSeleccionado != nil)
    {
        if (self.existeColumna1)
        {
            NSUInteger row = [self.ListaCadenaElementos1 indexOfObject:self.ElementoSeleccionado.DescripcionAtt1];
            
            if (row > 0 && row < self.ListaCadenaElementos1.count)
            {
                [self.ControlPicker selectRow:row inComponent:0 animated:NO];
            }
        }
        
        if (self.existeColumna2)
        {
            NSUInteger row = [self.ListaCadenaElementos2 indexOfObject:self.ElementoSeleccionado.DescripcionAtt2];
            
            if (row > 0 && row < self.ListaCadenaElementos2.count)
            {
                [self.ControlPicker selectRow:row inComponent:1 animated:NO];
            }
        }
    }
    else
    {
        if (self.existeColumna1 && self.ListaCadenaElementos1.count > 1)
        {
            [self.ControlPicker selectRow:1 inComponent:0 animated:NO];
        }
        
        if (self.existeColumna2 && self.ListaCadenaElementos2.count > 1)
        {
            [self.ControlPicker reloadComponent:1];
            [self.ControlPicker selectRow:1 inComponent:1 animated:NO];
        }
    }
}

- (void)viewDidUnload
{
    [self setControlPicker:nil];
    [self setControlToolBar:nil];
    [self setControlBotonCancelar:nil];
    [self setControlBotonAceptar:nil];
    [super viewDidUnload];
}

- (BOOL)shouldAutorotate
{
    return NO;
}

#pragma  mark- UIPickerView Delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return self.ListaNombreValores.count;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
    {
        return self.ListaCadenaElementos1.count;
    }
    else
    {
        return self.ListaCadenaElementos2.count;
    }
}

- (void)ActualizarBotonAceptarSegunSeleccion
{
    ValoresdeAtributo *lDato = [self getValorActualSelect];
    
    self.controlBotonAceptar.enabled = (lDato != nil);
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0 && self.existeColumna2)
    {
        [self CargarListaCadenaElemento2:self.ListaCadenaElementos1[[pickerView selectedRowInComponent:0]]];
        [pickerView reloadComponent:1];
    }
    
    [self ActualizarBotonAceptarSegunSeleccion];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *lVista;
    
    if (view != nil && [view isKindOfClass:[UILabel class]])
    {
        lVista = (UILabel *)view;
    }
    else
    {
        CGSize ancho = [pickerView rowSizeForComponent:component];
        lVista = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, ancho.width - 10, ancho.height)];
    }
    
    lVista.adjustsFontSizeToFitWidth = true;
    lVista.backgroundColor = [Locator Default].Color.Clear;
    lVista.textAlignment = NSTextAlignmentCenter;
    
    if (row == 0)
    {
        lVista.text = [self getNombreAtributo:(int)component];
        lVista.textColor = [Locator Default].Color.Azul;
        lVista.font = [Locator Default].FuenteLetra.Cabecera;
    }
    else
    {
        lVista.textColor = [Locator Default].Color.Texto;
        lVista.font = [Locator Default].FuenteLetra.Normal;
        
        if (component == 0)
        {
            lVista.text = [self getTextArray:self.ListaCadenaElementos1 Item:(int)row];
        }
        else
        {
            lVista.text = [self getTextArray:self.ListaCadenaElementos2 Item:(int)row];
        }
    }
    
    [self ActualizarBotonAceptarSegunSeleccion];
    
    return lVista;
}

#pragma mark- Eventos Objetos

- (IBAction)btnDone_click:(id)sender
{
    [self dismissSemiModalViewController:self];
    
    if (self.Respond != nil)
    {
        self.Respond([self getValorActualSelect]);
    }
}

- (IBAction)btnCancel_click:(id)sender
{
    [self dismissSemiModalViewController:self];
    
    if (self.Respond != nil)
    {
        self.Respond(nil);
    }
}

#pragma  mark- privado

- (BOOL)existeColumna2
{
    return (self.ListaNombreValores.count >= 2);
}

- (BOOL)existeColumna1
{
    return (self.ListaNombreValores.count >= 1);
}

- (void)CargarListaCadenaElemento1
{
    self.ListaCadenaElementos1 = [NSMutableArray new];
    
    if (self.existeColumna1)
    {
        [self.ListaCadenaElementos1 addObject:[self getNombreAtributo:0]];
        
        for (ValoresdeAtributo *Elmento in self.ListaElementos)
        {
            if ([self existeCadena:self.ListaCadenaElementos1 Cadena:Elmento.DescripcionAtt1] == false)
            {
                [self.ListaCadenaElementos1 addObject:Elmento.DescripcionAtt1];
            }
        }
    }
}

- (void)CargarListaCadenaElemento2:(NSString *)pSelected
{
    self.ListaCadenaElementos2 = [NSMutableArray new];
    
    if (self.existeColumna2)
    {
        [self.ListaCadenaElementos2 addObject:[self getNombreAtributo:1]];
        
        for (ValoresdeAtributo *Elmento in self.ListaElementos)
        {
            if ([Elmento.DescripcionAtt1 isEqualToString:pSelected] && [self existeCadena:self.ListaCadenaElementos2 Cadena:Elmento.DescripcionAtt2] == false)
            {
                [self.ListaCadenaElementos2 addObject:Elmento.DescripcionAtt2];
            }
        }
    }
}

- (BOOL)existeCadena:(NSArray *)pLista Cadena:(NSString *)pCadena
{
    BOOL lResultado = false;
    
    for (NSString *lTexto in pLista)
    {
        if ([lTexto isEqualToString:pCadena])
        {
            lResultado = true;
            break; // salida directa
        }
    }
    
    return lResultado;
}

//-(NSString*)getSelectedStringLabelForComponent:(int)pintComponent
//{
//    //    @try {
//    UILabel *label = (UILabel*) [self.ControlPicker viewForRow:[self.ControlPicker selectedRowInComponent:pintComponent] forComponent:pintComponent];
//
//    return label.text;
//    //    }
//    //    @catch (NSException *exception) {
//    //
//    //    }
//    //    return @"";
//}

- (ValoresdeAtributo *)getValorAtributoForAtribute1:(NSString *)pstrAttribute1 AndAttribute2:(NSString *)pAttribute2
{
    ValoresdeAtributo *lResultado = nil;
    
    for (ValoresdeAtributo *lDato in self.ListaElementos)
    {
        if ([lDato.DescripcionAtt1 isEqualToString:pstrAttribute1] && (self.existeColumna2 == false || [lDato.DescripcionAtt2 isEqualToString:pAttribute2]))
        {
            lResultado = lDato;
            break; // salida rapida
        }
    }
    
    return lResultado;
}

- (ValoresdeAtributo *)getValorActualSelect
{
    NSString *lAtribute1 = @"";
    NSString *lAtribute2 = @"";
    
    if (self.existeColumna1)
    {
        lAtribute1 = self.ListaCadenaElementos1[[self.ControlPicker selectedRowInComponent:0]];
    }
    
    if (self.existeColumna2)
    {
        lAtribute2 = self.ListaCadenaElementos2[[self.ControlPicker selectedRowInComponent:1]];
    }
    
    return [self getValorAtributoForAtribute1:lAtribute1 AndAttribute2:lAtribute2];
}

- (NSString *)getNombreAtributo:(int)pRow
{
    if (self.ListaNombreValores.count > pRow)
    {
        return ((Atributo *)self.ListaNombreValores[pRow]).NombreAtributo;
    }
    else
    {
        return @"";
    }
}

- (int)getValideRow:(ValoresdeAtributo *)lDato Row:(int)pRow Component:(int)pComponent
{
    int resul = (int)[self.ControlPicker selectedRowInComponent:pComponent];
    
    if ([self.ControlPicker selectedRowInComponent:pComponent] == 0)
    {
        [self.ControlPicker selectRow:pRow inComponent:pComponent animated:true];
        resul = pRow;
    }
    
    return resul;
}

- (NSString *)getTextArray:(NSArray *)pLista Item:(int)pPos
{
    if (pLista.count > pPos)
    {
        return pLista[pPos];
    }
    else
    {
        return @"";
    }
}

@end
