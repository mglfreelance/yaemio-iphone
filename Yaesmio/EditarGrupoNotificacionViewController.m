//
//  EditarGrupoNotificacionViewController.m
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "EditarGrupoNotificacionViewController.h"
#import "ContactoCell.h"
#import "UIAllControls.h"
#import "Contacto.h"

@interface EditarGrupoNotificacionViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelTitulo;
@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCampos;
@property (weak, nonatomic) IBOutlet UITextField *textoNombreGrupo;
@property (weak, nonatomic) IBOutlet UICollectionView *viewCollectionContactos;


- (IBAction)AgregarContactoExecute:(id)sender;
- (IBAction)GuardarGrupoNotificacionExecute:(id)sender;

@property (strong) NSMutableArray *ListaContactos;

@end

@implementation EditarGrupoNotificacionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaCampos);
    self.ListaContactos = [[NSMutableArray alloc] initWithArray:self.Grupo.ListaContactos];
    self.textoNombreGrupo.text = self.Grupo.NombreGrupo;
    [self.view mgAddGestureRecognizerTap:self Action:@selector(OcultarTeclado)];
    [self configurarTextField];
    [self CargarDatosGrupo];
    [[Locator Default].Contactos ComprobarAccesoAddressBook];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito QuitarBotonCarrito:self];
    
    if (self.ListaContactos.count == 0)
    {    // para la primera vez y cuando vuelva y no tenga datos.
        [self.viewCollectionContactos reloadData];
    }
    
    UIMenuItem *menuItem = [[UIMenuItem alloc] initWithTitle:Traducir(@"Eliminar") action:@selector(EliminarContacto:)];
    
    [[UIMenuController sharedMenuController] setMenuItems:@[menuItem]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIMenuController sharedMenuController] setMenuItems:nil];
}

- (void)viewUnload
{
    [self setLabelTitulo:nil];
    [self setListaCampos:nil];
    [self setTextoNombreGrupo:nil];
    [self setViewCollectionContactos:nil];
}

- (IBAction)AgregarContactoExecute:(id)sender
{
    [[Locator Default].Vistas MostrarSeleccionContactosTelefono:self ListaMails:[self getListaContactosActuales] MostrarGrupos:false MostrarOtros:false Result:^(NSArray *pListaContactosSeleccionados)
     {
         if (pListaContactosSeleccionados != nil)
         {
             [self.ListaContactos removeAllObjects];
             
             for (Contacto * myContacto in pListaContactosSeleccionados)
             {
                 ContactoGrupo *nuevo = [ContactoGrupo new];
                 
                 nuevo.email = myContacto.MailUsuario;
                 nuevo.IdUsuarioSap = myContacto.UsuarioId;
                 
                 [self.ListaContactos addObject:nuevo];
             }
             
             [self.viewCollectionContactos reloadData];
         }
     }];
}

- (IBAction)GuardarGrupoNotificacionExecute:(id)sender
{
    NSArray *lista = nil;
    
    if (self.Grupo.IdGrupo.isNotEmpty)
    {    //modificar grupo
        [[Locator Default].Alerta showBussy:Traducir(@"Guardando grupo")];
        
        //quitar contactos
        [self EliminarContactos: ^{
            //agregar contactos
            [self AgregarContactos: ^{
                //guardar grupo
                [[Locator Default].ServicioSOAP ModificarGrupoNotificacion:[self CrearGrupoClonado] Result: ^(SRResultMethod pResult, NSString *MensajeSAP)
                 {
                     switch (pResult)
                     {
                         case SRResultMethodYES:
                             [[Locator Default].Alerta hideBussy];
                             [self.navigationController popViewControllerAnimated:true];
                             break;
                             
                         case SRResultMethodNO:
                             [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                             break;
                             
                         case SRResultMethodErrConex:
                             [[Locator Default].Alerta showErrorConexSOAP];
                             break;
                     }
                 }];
            }];
        }];
    }
    else
    {   //nuevo grupo
        [[Locator Default].Alerta showBussy:Traducir(@"Guardando grupo")];
        lista = [self getListaIds:self.ListaContactos];
        [[Locator Default].ServicioSOAP CrearGrupoNotificacion:self.textoNombreGrupo.text Contactos:lista Result: ^(SRResultMethod pResult, NSString *MensajeSAP, GrupoNotificacion *pGrupo)
         {
             switch (pResult)
             {
                 case SRResultMethodYES:
                     [[Locator Default].Alerta hideBussy];
                     [self.navigationController popViewControllerAnimated:true];
                     break;
                     
                 case SRResultMethodNO:
                     [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                     break;
                     
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
             }
         }];
    }
}

#pragma mark- Delegate UITextField

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.textoNombreGrupo.background = [Locator Default].Imagen.FondoTextFieldSeleccionado;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.textoNombreGrupo.background = [Locator Default].Imagen.FondoTextFieldSeleccionado;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    self.textoNombreGrupo.background = [Locator Default].Imagen.FondoTextField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.textoNombreGrupo.background = [Locator Default].Imagen.FondoTextField;
}

#pragma mark- Delegate CollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.ListaContactos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ContactoCell *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ContactoCell" forIndexPath:indexPath];
    
    [myCell ConfigurarConContactoGrupo:self.ListaContactos[indexPath.row] IndexPath:indexPath Delegate: ^(NSString *Type, NSInteger section, NSInteger row)
     {
         if ([Type isEqualToString:@"EliminarContacto"])
         {
             [self.ListaContactos removeObject:self.ListaContactos[row]];
             [self.viewCollectionContactos reloadData];
         }
     }];
    
    return myCell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    return YES;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    NSLog(@"performAction");
}

#pragma mark- Metodos Privados

- (void)OcultarTeclado
{
    [self.view endEditing:YES];
}

- (void)CargarDatosGrupo
{
    self.labelTitulo.text = self.Grupo.IdGrupo.isNotEmpty ? Traducir(@"Modificar grupo") : Traducir(@"Crear grupo.");
    self.textoNombreGrupo.text = self.Grupo.NombreGrupo;
}

- (NSArray *)getListaIds:(NSArray *)pLista
{
    NSMutableArray *result = [NSMutableArray new];
    
    for (ContactoGrupo *contacto in pLista)
    {
        [result addObject:contacto.IdUsuarioSap];
    }
    
    return result;
}

- (NSArray *)getListaIdsContactosParaEliminar
{
    NSMutableArray *resul = [NSMutableArray new];
    
    for (ContactoGrupo *item in self.Grupo.ListaContactos)
    {
        if ([self.ListaContactos ExisteContactoGrupo:item] == false)
        {
            [resul addObject:item.IdUsuarioSap];
        }
    }
    
    return resul;
}

- (NSArray *)getListaIdsContactosParaAgregar
{
    NSMutableArray *resul = [NSMutableArray new];
    
    for (ContactoGrupo *item in self.ListaContactos)
    {
        if ([self.Grupo.ListaContactos ExisteContactoGrupo:item] == false)
        {
            [resul addObject:item.IdUsuarioSap];
        }
    }
    
    return resul;
}

- (NSArray *)getListaContactosActuales
{
    NSMutableArray *resul = [NSMutableArray new];
    
    for (ContactoGrupo *item in self.ListaContactos)
    {
        Contacto *nuevo = [Contacto new];
        nuevo.UsuarioId = item.IdUsuarioSap;
        [resul addObject:nuevo];
    }
    
    return resul;
}

- (GrupoNotificacion *)CrearGrupoClonado
{
    GrupoNotificacion *resul = [GrupoNotificacion new];
    
    resul.IdGrupo = [NSString stringWithString:self.Grupo.IdGrupo];
    resul.NombreGrupo = [NSString stringWithString:self.textoNombreGrupo.text];
    resul.IdUsuarioCreador = [NSString stringWithString:self.Grupo.IdUsuarioCreador];
    resul.ListaContactos = [NSArray arrayWithArray:self.ListaContactos];
    
    return resul;
}

- (void)configurarTextField
{
    self.textoNombreGrupo.background = [Locator Default].Imagen.FondoTextField;
    self.textoNombreGrupo.delegate = self;
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
    self.textoNombreGrupo.leftView = paddingView;
    self.textoNombreGrupo.leftViewMode = UITextFieldViewModeAlways;
    self.textoNombreGrupo.rightView = paddingView;
    self.textoNombreGrupo.rightViewMode = UITextFieldViewModeAlways;
}

- (void)EliminarContactos:(ResultadoBlock)pResultado
{
    NSArray *lista = [self getListaIdsContactosParaEliminar];
    
    if (lista.count > 0)
    {
        [[Locator Default].ServicioSOAP EliminarContactosDelGrupo:self.Grupo.IdGrupo ListaContactos:lista Usuario:[Locator Default].Preferencias.IdUsuario Result: ^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *pListaContactosActuales)
         {
             switch (pResult)
             {
                 case SRResultMethodYES:
                     
                     //seguimos con el proceso de guardado
                     if (pResultado != nil)
                     {
                         pResultado();
                     }
                     
                     break;
                     
                 case SRResultMethodNO:
                     [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                     break;
                     
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
             }
         }];
    }
    else
    {
        if (pResultado != nil)
        {
            pResultado();
        }
    }
}

- (void)AgregarContactos:(ResultadoBlock)pResultado
{
    NSArray *lista = [self getListaIdsContactosParaAgregar];
    
    if (lista.count > 0)
    {
        [[Locator Default].ServicioSOAP AgregarContactosAlGrupo:self.Grupo.IdGrupo ListaContactos:lista Usuario:[Locator Default].Preferencias.IdUsuario Result: ^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *pListaContactosActuales)
         {
             switch (pResult)
             {
                 case SRResultMethodYES:
                     
                     //seguimos con el proceso de guardado
                     if (pResultado != nil)
                     {
                         pResultado();
                     }
                     
                     break;
                     
                 case SRResultMethodNO:
                     [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                     break;
                     
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
             }
         }];
    }
    else
    {
        if (pResultado != nil)
        {
            pResultado();
        }
    }
}

@end
