//
//  WebViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 07/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "WebViewController.h"
#import "UIAllControls.h"

@interface WebViewController ()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *ControlWebView;
@property (weak, nonatomic) IBOutlet UIView *VistaCentradaYaesmio;
@property (weak, nonatomic) IBOutlet UIButton *btnSiguiente;
@property (weak, nonatomic) IBOutlet UIButton *btnAnterior;
@property (weak, nonatomic) IBOutlet UIView *ViewLoading;
@property (weak, nonatomic) IBOutlet UILabel *labelCargando;
@property (weak, nonatomic) IBOutlet UILabel *labelTitulo;

- (IBAction)btnCerrar_click:(id)sender;
- (IBAction)btnSiguiente_click:(id)sender;
- (IBAction)btnAnterior_click:(id)sender;

@property (nonatomic, assign) BOOL isLoading;

@end

@implementation WebViewController

@synthesize PorDefecto = _PorDefecto;


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.labelCargando);
    Traducir(self.labelTitulo);
    
    int myWidth = [self esWebViewExterna] ? self.view.frame.size.width : 146;
    
    [self.VistaCentradaYaesmio mgSetSizeX:UNCHANGED andY:UNCHANGED andWidth:myWidth andHeight:UNCHANGED];
    [self RefrescarBotonesBackForward];
    self.isLoading = false;

}

- (void)viewDidAppear:(BOOL)animated
{
    if ([self esWebViewExterna] == false)
    {
        [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:[self esWebViewExterna]];
    }
    else
    {
        UIBarButtonItem *Button = [[UIBarButtonItem alloc] initWithTitle:Traducir(@"OK") style:UIBarButtonItemStyleBordered target:self action:@selector(btnCerrar_click:)];
        
        self.navigationItem.rightBarButtonItem = Button;
    }
    
    if ([self esWebViewExterna])
    {
        self.ControlWebView.scalesPageToFit = true;
        [self.ControlWebView loadRequest:[[NSURLRequest alloc] initWithURL:self.url]];
    }
    else if (self.ContenidoWeb != nil && [self.ContenidoWeb isEqualToString:@""] == false)
    {
        self.ControlWebView.suppressesIncrementalRendering = true;
        [self.ControlWebView loadHTMLString:self.ContenidoWeb baseURL:nil];
    }
}

- (void)viewUnload
{
    [self setControlWebView:nil];
    [self setVistaCentradaYaesmio:nil];
    [self setBtnSiguiente:nil];
    [self setBtnAnterior:nil];
    [self setViewLoading:nil];
    [self setLabelCargando:nil];
}

#pragma  mark- Metodos Privados

- (void)setCadenaUrl:(NSString *)pcadenaUrl
{
    self.url = [NSURL URLWithString:pcadenaUrl];
}

- (NSString *)cadenaUrl
{
    return self.url.absoluteString;
}

- (WVCDefecto)PorDefecto
{
    return _PorDefecto;
}

- (void)setPorDefecto:(WVCDefecto)pPorDefecto
{
    _PorDefecto = pPorDefecto;
    switch (_PorDefecto)
    {
        case WVCDefecto_url:
            break;
            
        case WVCDefecto_NO_QR:
            self.cadenaUrl = @"http://www.yaesmio.com/";
            break;
    }
}

- (BOOL)esWebViewExterna
{
    return (self.url != nil || self.cadenaUrl != nil);
}

#pragma mark- Webview Delegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    self.isLoading = true;
    [self RefrescarBotonesBackForward];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    self.isLoading = false;
    [self RefrescarBotonesBackForward];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    if (![self esWebViewExterna])
    {
        [[Locator Default].Alerta showMessageAcept:Traducir(@"Ha sido imposibe mostrar la web.") ResulBlock:^{
            [self.navigationController popViewControllerAnimated:true];
        }];
    }
    
    [self RefrescarBotonesBackForward];
}

- (BOOL)isLoading
{
    return (self.ViewLoading.hidden == false);
}

- (void)setIsLoading:(BOOL)isLoading
{
    self.ViewLoading.hidden = !isLoading;
    self.view.userInteractionEnabled = !isLoading;
}

- (BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
    if (self.MostrarLinkSafari && inType == UIWebViewNavigationTypeLinkClicked)
    {
        [[Locator Default].Vistas MostrarWebView:self url:inRequest.URL.absoluteString];
        return NO;
    }
    
    return YES;
}

- (IBAction)btnCerrar_click:(id)sender
{
    [self.ControlWebView Clear];
    [self dismissViewControllerAnimated:TRUE completion:NULL];
    self.isLoading = false;
}

- (IBAction)btnSiguiente_click:(id)sender
{
    if ([self.ControlWebView canGoForward])
    {
        [self.ControlWebView goForward];
    }
    
    [self RefrescarBotonesBackForward];
}

- (IBAction)btnAnterior_click:(id)sender
{
    if ([self.ControlWebView canGoBack])
    {
        [self.ControlWebView goBack];
    }
    
    [self RefrescarBotonesBackForward];
}

- (void)RefrescarBotonesBackForward
{
    self.btnAnterior.hidden  = ![self esWebViewExterna];
    self.btnSiguiente.hidden = ![self esWebViewExterna];
    
    self.btnAnterior.enabled = self.ControlWebView.canGoBack;
    self.btnSiguiente.enabled = self.ControlWebView.canGoForward;
}

@end
