//
//  ProductosFavoritosManager.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 16/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import "ProductosFavoritosManager.h"
#import "JSBadgeView.h"
#import "Favorito.h"

@interface ProductosFavoritosManager ()

@property (nonatomic) UITabBarItem        *mActualItem;
@property (nonatomic) JSBadgeView         *mActualBadge;

@end

@implementation ProductosFavoritosManager

@synthesize NumeroFavoritosToTal = _NumeroFavoritosAgregados;

-(id) init
{
    self = [super init];
    
    if (self)
    {
    }
    
    return self;
}

-(void) ActualizarEstado
{
    if ([Locator Default].Preferencias.EstaUsuarioLogado == false)
    {
        _NumeroFavoritosAgregados = 0;
        [self ActualizarBadge:nil ResultadoMetodo:SRResultProductoFavoritoManagerYES Mensaje:@""];
    }
    else
    {
        [self ObtenerListaFavoritos:1 Result:^(SRResultProductoFavoritoManager pResult, NSArray *pLista, NSString *pMensaje, int pNumeroFavoritosTotales) {
            _NumeroFavoritosAgregados = pNumeroFavoritosTotales;
            [self ActualizarBadge:nil ResultadoMetodo:SRResultProductoFavoritoManagerYES Mensaje:@""];
        }];
    }
    
}

-(void) addTabBarItem:(UITabBarItem*)pItem andTabBar:(UIView*) pTabBar
{
    self.mActualItem = pItem;
    self.mActualBadge = [[JSBadgeView alloc] initWithParentView:pTabBar alignment: JSBadgeViewAlignmentCenter];
    
    // por defecto lo que tenga guardado en preferencias
    self.mActualBadge.badgeText = @"";
    self.mActualBadge.badgeTextFont = [self.mActualBadge.badgeTextFont fontWithSize:11];
    self.mActualBadge.badgeTextColor = [Locator Default].Color.Blanco;
    self.mActualBadge.badgeBackgroundColor = [Locator Default].Color.Clear;
    self.mActualBadge.badgeOverlayColor = [Locator Default].Color.Clear;
    self.mActualBadge.badgeTextShadowColor = [Locator Default].Color.Clear;
    self.mActualBadge.badgeAlignment = JSBadgeViewAlignmentCenter;
    self.mActualBadge.badgePositionAdjustment = CGPointMake(65, -13);
    
    self.mActualItem.badgeValue = nil;
   // [[Locator Default].ProductosFavoritos ActualizarEstado]; // obtiene numero de wbservice
}

-(void) AgregarProducto:(ProductoPadre*)pProducto Result:(ProductoFavoritoBlock)pResultado
{
    if (pProducto != nil)
    {
        [[Locator Default].ServicioSOAP AgregarFavoritoConIdUser:[self getIdUser] IdProducto:pProducto Result:^(SRResultMethod pResult, NSString *pMensaje)
         {
             SRResultProductoFavoritoManager lResultado = [self Convertir:pResult];
             if (lResultado == SRResultProductoFavoritoManagerYES)
             {
                 _NumeroFavoritosAgregados += 1; // aumento en uno
             }
             [self ActualizarBadge:pResultado ResultadoMetodo:lResultado Mensaje:pMensaje];
         }];
    }
}

-(void) EliminarProducto:(Favorito*)pProducto Result:(ProductoFavoritoBlock)pResultado
{
    if (pProducto != nil)
    {
        [[Locator Default].ServicioSOAP EliminarFavoritoConIdUser:[self getIdUser] Favorito:pProducto Result:^(SRResultMethod pResult, NSString *pMensaje)
         {
             SRResultProductoFavoritoManager lResultado = [self Convertir:pResult];
             if (lResultado == SRResultProductoFavoritoManagerYES)
             {
                 _NumeroFavoritosAgregados -=1;
             }
             [self ActualizarBadge:pResultado ResultadoMetodo:lResultado Mensaje:pMensaje];
         }];
        
    }
}

-(void) AgregarACarrito:(Favorito*)pFavorito Producto:(ProductoPadre*)pProducto Unidades:(NSDecimalNumber*)pNumero Result:(ProductoFavoritoBlock)pResultado
{
    if (pProducto != nil)
    {
        [[Locator Default].Carrito agregarProducto:pProducto Unidades:pNumero];
    }
}

-(void) ActualizarBadge:(ProductoFavoritoBlock)pResultado ResultadoMetodo:(SRResultProductoFavoritoManager)pResultadoMetodo Mensaje:(NSString*)pMensaje
{
    if (self.mActualBadge != nil)
    {
        if (self.NumeroFavoritosToTal >0)
            self.mActualBadge.badgeText = [NSString stringWithFormat:@"%d", self.NumeroFavoritosToTal];
        else
            self.mActualBadge.badgeText = @"";
    }
    
    if (pResultado)
    {
        pResultado(pResultadoMetodo, pMensaje);
    }
}

-(void)ObtenerListaFavoritos:(int)posInicio Result:(ListaFavoritosBlock)pResultado
{
    [[Locator Default].ServicioSOAP ObtenerListaFavoritosConIdUser:[Locator Default].Preferencias.IdUsuario PosInicio:posInicio Cuantos:6 Result:^(SRResultMethod pResult, NSArray *plistaProductos, int numElementosTotales, NSString *pMensaje)
     {
         SRResultProductoFavoritoManager lResult= SRResultProductoFavoritoManagerErrConex;
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 lResult = SRResultProductoFavoritoManagerErrConex;
                 break;
                 
             case SRResultMethodNO:
                  lResult = SRResultProductoFavoritoManagerNO;
                 break;
                 
             case SRResultMethodYES:
                 lResult = SRResultProductoFavoritoManagerYES;
                 break;
         }
         
         [self actualizarNumeroFavoritos:[NSString stringWithFormat:@"%d", numElementosTotales]];
         
         if (pResultado)
         {
             pResultado(lResult, plistaProductos, pMensaje, numElementosTotales);
         }
     }];
}

-(NSString*) getIdUser
{
    return [Locator Default].Preferencias.IdUsuario;
}

-(SRResultProductoFavoritoManager) Convertir:(SRResultMethod)pResult
{
    SRResultProductoFavoritoManager lResultado = SRResultProductoFavoritoManagerErrConex;
    switch (pResult)
    {
        case SRResultMethodErrConex:
            lResultado = SRResultProductoFavoritoManagerErrConex;
            break;
            
        case SRResultMethodNO:
            lResultado = SRResultProductoFavoritoManagerNO;
            break;
            
        case SRResultMethodYES:
            lResultado = SRResultProductoFavoritoManagerYES;
            break;
    }
    return lResultado;
}

-(void) actualizarNumeroFavoritos:(NSString*) pNUmeroFavoritos
{
    _NumeroFavoritosAgregados = [pNUmeroFavoritos intValue];
    [self ActualizarBadge:nil ResultadoMetodo:SRResultProductoFavoritoManagerYES Mensaje:@""];
}

@end

