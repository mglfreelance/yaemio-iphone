//
//  EmpresaCarrito.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "EmpresaCarrito.h"
@interface EmpresaCarrito ()

@property (strong) NSMutableDictionary *ListaElementos;
@property (strong) NSString *NombreMoneda;
@property (strong) Media *mLogo;
@property (strong) NSString *mUrlEmpresa;

@end

@implementation EmpresaCarrito

- (id)init:(NSString *)pIdEmpresa
{
    if (self = [super init])
    {
        self.IdEmpresa      = pIdEmpresa;
        self.ListaElementos = [NSMutableDictionary new];
        self.NombreMoneda   = @"";
        self.GastosEnvio    = [NSDecimalNumber zero];
        self.mLogo          = nil;
        self.mUrlEmpresa    = @"";
        self.IDCarrito      = @"";
    }
    
    return self;
}

- (id)init:(NSString *)pIdEmpresa Nombre:(NSString *)pNombre Elementos:(NSArray *)pElementos
{
    if (self = [self init:pIdEmpresa])
    {
        for (ElementoCarrito *lProducto in pElementos)
        {
            [self AgregarElmento:lProducto];
        }
    }
    
    return self;
}

- (void)agregarProducto:(ProductoPadre *)pProducto Unidades:(NSDecimalNumber *)pUnidades
{
    ElementoCarrito *lElemento = [self ObtenerElementoProducto:pProducto];
    
    if (lElemento == nil)
    {
        lElemento = [ElementoCarrito crear:pProducto Unidades:pUnidades];
        [self AgregarElmento:lElemento];
    }
    else
    {
        lElemento.Unidades = [lElemento.Unidades decimalNumberByAdding:pUnidades];
    }
}

- (void)AgregarElmento:(ElementoCarrito *)pElemento
{
    if (pElemento != nil)
    {
        ElementoCarrito *lProducto = [self.ListaElementos objectForKey:[self getIDProducto:pElemento.Producto]];
        
        if (lProducto == nil)
        {
            [self.ListaElementos setObject:pElemento forKey:[self getIDProducto:pElemento.Producto]];
        }
        else
        {
            NSLog(@"actual:%@ add:%@", lProducto.Unidades, pElemento.Unidades);
            lProducto.Unidades = [lProducto.Unidades decimalNumberByAdding:pElemento.Unidades];
        }
        
        if (self.NombreMoneda == nil || [self.NombreMoneda isEqualToString:@""])
        {
            self.NombreMoneda = pElemento.Producto.ProductoHijo.Moneda;
        }
        
        if (self.mLogo == nil || [self.mLogo.UrlPath isEqualToString:@""])
        {
            self.mLogo = pElemento.Producto.LogoEmpresa;
        }
        
        if (self.mUrlEmpresa == nil || [self.mUrlEmpresa isEqualToString:@""])
        {
            self.mUrlEmpresa = pElemento.Producto.UrlWebEmpresa;
        }
    }
}

- (NSArray *)Elementos
{
    return [[NSArray alloc] initWithArray:self.ListaElementos.allValues];
}

- (BOOL)existenElementos
{
    return (self.ListaElementos.count > 0);
}

- (Media *)LogoEmpresa
{
    return self.mLogo;
}

- (NSString *)UrlWebEmpresa
{
    return self.mUrlEmpresa;
}

- (BOOL)esNecesariaDireccionEnvio
{
    BOOL result = false;
    
    for (ElementoCarrito *elemento in self.Elementos)
    {
        if (elemento.Producto.SinDireccionEnvio == false)
        {
            result = true;
            break;             // salida rapida.
        }
    }
    
    return result;
}

- (NSDecimalNumber *)PrecioTotal
{
    NSDecimalNumber *lResultado = [NSDecimalNumber zero];
    
    for (ElementoCarrito *lDato in self.ListaElementos.allValues)
    {
        lResultado = [lResultado decimalNumberByAdding:lDato.TotalPrecio];
    }
    
    if (self.GastosEnvio != nil)
    {
        lResultado = [lResultado decimalNumberByAdding:self.GastosEnvio];
    }
    
    return lResultado;
}

- (NSString *)Moneda
{
    return self.NombreMoneda;
}

- (BOOL)QuitarElmento:(ElementoCarrito *)pElemento
{
    BOOL lResultado = false;
    
    if (pElemento != nil && [self getIDProducto:pElemento.Producto] != nil)
    {
        if (self.ListaElementos[[self getIDProducto:pElemento.Producto]] != nil)
        {
            [self.ListaElementos removeObjectForKey:[self getIDProducto:pElemento.Producto]];
            lResultado = true;
        }
    }
    
    return lResultado;
}

- (bool)ModificarElemento:(ElementoCarrito *)pActual Modificado:(ProductoPadre *)pElemento Unidades:(NSDecimalNumber *)pUnidades
{
    BOOL lResultado = false;
    
    if (pActual != nil && [self getIDProducto:pActual.Producto] != nil)
    {
        [self QuitarElmento:pActual];
        [self AgregarElmento:[ElementoCarrito crear:pElemento Unidades:pUnidades]];
        lResultado = true;
    }
    
    return lResultado;
}

- (ComprobarExistenciasEmpresaEnum)ComprovarExistencias:(NSArray *)pListaProducto
{
    ComprobarExistenciasEmpresaEnum lResultado = ComprobarExistenciasEmpresaEnumSOME;
    ElementoCarrito *lElemento = nil;
    int lintElementosConStock = 0;
    
    for (ProductoPadre *lDato in pListaProducto)
    {
        lElemento = self.ListaElementos[[self getIDProducto:lDato]];
        
        if (lElemento != nil && lElemento.ExisteSuficienteStock)
        {
            lintElementosConStock += 1;
        }
    }
    
    if (lintElementosConStock == 0)
    {
        lResultado = ComprobarExistenciasEmpresaEnumANY;
    }
    else if (lintElementosConStock == self.ListaElementos.count)
    {
        lResultado = ComprobarExistenciasEmpresaEnumAll;
    }
    else
    {
        lResultado = ComprobarExistenciasEmpresaEnumSOME;
    }
    
    return lResultado;
}

- (ElementoCarrito *)ObtenerElementoProducto:(ProductoPadre *)pProductoPadre
{
    ElementoCarrito *lResultado = nil;
    
    for (ElementoCarrito *lDato in self.Elementos)
    {
        if ([[self getIDProducto:lDato.Producto] isEqualToString:[self getIDProducto:pProductoPadre]])
        {
            lResultado = lDato;
            break;             // salida rapida.
        }
    }
    
    return lResultado;
}

- (NSString *)getIDProducto:(ProductoPadre *)pProducto
{
    return pProducto.ProductoHijo.ProductoId;
}

#pragma mark- Soap methods

- (void)addElementsToNode:(xmlNodePtr)node
{
    for (ElementoCarrito *Elemento in self.Elementos)
    {
        xmlAddChild(node, [Elemento xmlNodeForDoc:node->doc elementName:@"ProductoComprado" elementNSPrefix:@"tns3"]);
    }
}

@end
