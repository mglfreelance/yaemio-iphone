//
//  QueEsYaesmioViewController.m
//  yaesmio
//
//  Created by manuel garcia lopez on 06/05/13.
//  Copyright (c) 2013 manuel garcia lopez. All rights reserved.
//

#import "QueEsYaesmioViewController.h"
#import "UIAllControls.h"

@interface QueEsYaesmioViewController () <UIWebViewDelegate>

@property (nonatomic) IBOutlet UIWebView *VistaWEB;


@end

@implementation QueEsYaesmioViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = NO;
    
    [self MostrarQueEsYaesmio];
}

- (void)viewUnload
{
    [self setVistaWEB:nil];
}

-(void) viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
    [super viewWillDisappear:animated];
}

#pragma mark- Metodos Privados

- (void)MostrarQueEsYaesmio
{
    [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo datos.")];
    [[Locator Default].ServicioSOAP ObtenerTexto:@"ZWEB01" Result:^(SRResultMethod pResult, NSString *pTexto)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible obtener la informacion a mostrar.")];
                 break;
                 
             case SRResultMethodYES:
                 [[Locator Default].Alerta hideBussy];
                 self.VistaWEB.delegate = self;
                 [self.VistaWEB loadHTMLString:pTexto baseURL:nil];
                 break;
         }
     }];
}

#pragma mark - WebView Delegate

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"%@",error);
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
    if ( inType == UIWebViewNavigationTypeLinkClicked )
    {
        [[Locator Default].Vistas MostrarWebView:self url:inRequest.URL.absoluteString];
        return NO;
    }
    
    return YES;
}

@end
