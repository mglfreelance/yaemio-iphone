//
//  ProductoPageVC.h
//  Yaesmio
//
//  Created by freelance on 05/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "YaesmioViewControllers.h"
#import "ElementoCarrito.h"
#import "ProductoPadre.h"
#import "UIAllControls.h"

@class ProductoPageVC;

@protocol ProductoPageVCDelegate <NSObject>

@required

- (void)InformarCambioDePage:(ProductoPageVC *)pView Clave:(NSString *)pKey Objeto:(id)pObject;

@end

@interface ProductoPageVC : YaesmioViewController

@property (strong) ElementoCarrito *ElementoCarrito;
@property (strong) ProductoPadre *Producto;
@property (assign) BOOL VieneDeFavoritos;
@property (readonly) BOOL esModificarProduto;
@property (weak) id<ProductoPageVCDelegate> delegate;
@property (strong) NSString *NombrePage;
@property (readonly) int UnidadesSeleccionadas;


- (void)Actualizar;

@end
