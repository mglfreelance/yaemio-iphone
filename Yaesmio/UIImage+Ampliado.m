//
//  UIImage+Ampliado.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 25/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "UIImage+Ampliado.h"
#import "NSDate+Ampliado.h"
#import "NSURL+Ampliado.h"


@implementation UIImage (Ampliado)

+(void)ImageAssyncFromUrl:(NSURL*)pUrlImage Grupo:(NSString*)pGrupo Result:(getAssyncImageWithUrlBlock)pResult
{
    [self LoadImage:pResult pGrupo:pGrupo pUrlImage:pUrlImage];
}

+(void)ImageAssyncFromStringUrl:(NSString*)pUrlImage Grupo:(NSString*)pGrupo Result:(getAssyncImageWithUrlBlock)pResult
{
    [self ImageAssyncFromUrl:[NSURL URLWithString:pUrlImage] Grupo:pGrupo Result:pResult];
}

- (UIImage *)imageScaledToSize:(CGSize)newSize
{
    // Create an image context
    UIGraphicsBeginImageContext(newSize);
    
    // Draw the scaled image
    [self drawInRect:CGRectMake(0.0f, 0.0f, newSize.width, newSize.height)];
    
    // Create a new image from context
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // Pop the current context from the stack
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

-(UIImage *)scaleToWidth:(CGFloat)width
{
    UIImage *scaledImage = self;
    if (self.size.width != width)
    {
        CGFloat height = floorf(self.size.height * (width / self.size.width));
        CGSize size = CGSizeMake(width, height);
        
        // Create a new image from context
        scaledImage = [self imageScaledToSize:size];
    }
    // Return the new scaled image
    return scaledImage;
}

-(UIImage *)scaleToMaxWidth:(CGFloat)pMaxWidth AndMaxHeight:(CGFloat)pMaxHeight
{
    UIImage *scaledImage = self;
    if (self.size.width > pMaxWidth || self.size.height > pMaxHeight)
    {
        CGFloat scaleWidth = (pMaxWidth / self.size.width);
        CGFloat scaleHeigh = (pMaxHeight / self.size.height);
        
        CGSize size;
        
        if (scaleWidth < scaleHeigh)
        {
            size = CGSizeMake(self.size.width * scaleWidth, self.size.height * scaleWidth);
        }
        else
        {
            size = CGSizeMake(self.size.width * scaleHeigh, self.size.height * scaleHeigh);
        }
        
        // Create a new image from context
        scaledImage = [self imageScaledToSize:size];
    }
    // Return the new scaled image
    return scaledImage;
}

+ (void)LoadImage:(getAssyncImageWithUrlBlock)pResult pGrupo:(NSString *)pGrupo pUrlImage:(NSURL *)pUrlImage
{
    if (pUrlImage != nil)
    {
        [[Locator Default].Download RequestAssync:pUrlImage Type:DownloadManagerTypeFileImage Grupo:pGrupo ResultData:^(NSData *pDataResult)
         {
             [self CallBlockResult:pDataResult pResult:pResult];
         }];
    }
    else
    {
        [self CallBlockResult:nil pResult:pResult];
    }
}

+ (void)CallBlockResult:(NSData *)pDataResult pResult:(getAssyncImageWithUrlBlock)pResult
{
    if (pResult != nil)
    {
        pResult([UIImage imageWithData:pDataResult]);
    }
}

@end
