//
//  Historico.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 29/07/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SoapObject.h"

@interface Historico : SoapObject

@property (strong) NSString             *AbreviaturaMoneda;
@property (strong) NSDecimalNumber      *Precio;
@property (assign) BOOL                  Caducado;
@property (strong) NSString             *ProductoId;
@property (strong) NSString             *Referencia;
@property (strong) Media                *Imagen;
@property (strong) ValoresdeAtributo    *ValoresAtributos;
@property (strong) NSArray              *ListaAtributos;
@property (strong) Media                *LogoMarca;
@property (strong) NSString             *Nombre;
@property (strong) NSDecimalNumber      *NumElementos; // numero elementos historico
@property (strong) NSDate               *Fecha;
@property (strong) NSString             *ProductoIdPadre;
@property (strong) NSDecimalNumber      *Unidades; // unidades compradas
@property (strong) NSString             *NombreUnidades; // nombre de unidades compradas
@property (strong) NSString             *NombreEmpresa;
@property (strong) NSString             *IdPedido;
@property (assign) NSInteger             Valoracion; // devuelve la valoracion dada por usuario o 0 si no la ha valorado todavia

//ampliado
@property (readonly) NSString           *TextoPropiedades;
@property (readonly) Atributo           *Atributo1;
@property (readonly) Atributo           *Atributo2;

+ (Historico *)deserializeNode:(xmlNodePtr)cur;

@end
