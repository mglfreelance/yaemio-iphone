//
//  ConfigurationParameter.h
//  Yaesmio
//
//  Created by freelance on 08/07/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ElementoPicker.h"

@interface ConfigurationParameter : ElementoPicker

+(ConfigurationParameter *) deserializeNode:(xmlNodePtr)cur;
+(ConfigurationParameter *) newWithId:(NSString*)pID andName:(NSString*)pName;

@end
