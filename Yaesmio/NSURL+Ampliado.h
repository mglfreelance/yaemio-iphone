//
//  NSURL+Ampliado.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 06/11/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSURL (Ampliado)

+(NSURL*) extractForString:(NSString*)pText;

- (NSString *)parameterForKey:(NSString *)key;

- (id)objectForKeyedSubscript:(id)key NS_AVAILABLE(10_8, 6_0);

@end
