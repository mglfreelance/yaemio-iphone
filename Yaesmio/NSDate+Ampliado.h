//
//  NSDate+Ampliado.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 20/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>


#define UNCHANGEDATE -999

@interface NSDate (Ampliado)

@property (readonly) int Year;
@property (readonly) int Month;
@property (readonly) int Day;
@property (readonly) int Hour;
@property (readonly) int Minute;
@property (readonly) int Second;


- (NSString *)			format:(NSString *)pFormat;

+ (NSDate *)			fromString:(NSString *)pstrDate WithFormat:(NSString *)pFormat;

- (NSComparisonResult)	compareIgnoreTime:(NSDate *)pDate;

- (NSDate *)dateWithYear:(NSInteger)Year Month:(NSInteger)month Day:(NSInteger)day  Hour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second;

@end
