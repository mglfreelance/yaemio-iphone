//
//  UILabel+Ampliado.h
//  Yaesmio
//
//  Created by freelance on 27/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Ampliado)

- (void)mgAdjustFontSizeToFit;

@end
