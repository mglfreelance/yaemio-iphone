//
//  LoginUsuarioResponse.h
//  Yaesmio
//
//  Created by freelance on 29/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"

@interface LoginUsuarioResponse : SoapObject

+ (LoginUsuarioResponse *)deserializeNode:(xmlNodePtr)cur;

@property (strong) NSString *UserID;
@property (strong) NSString *SecurityToken;
@property (strong) NSString *BackgroundImage;
@property (strong) NSString *FavoritesNumber;
@property (strong) NSString *NotificationsUnread;
@property (strong) NSString *NotificationsSound;
@property (strong) NSString *UserName;
@property (strong) NSString *HistoricNotRated;

@end
