//
//  ElementoPicker.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 14/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoapObject.h"

@interface ElementoPicker : SoapObject <NSCoding>

@property (strong) NSString *Codigo;
@property (strong) NSString *Nombre;

+(ElementoPicker*) newWithCode:(NSString*)pCodigo andName:(NSString*)pName;
+(ElementoPicker*) newWithIntCode:(NSInteger)pCodigo andName:(NSString *)pName;

@end


@interface NSArray (ElementoPicker)

-(ElementoPicker*)ObtenerElementoPorID:(NSString*)pID;
-(int) obtenerIndiceElemento:(ElementoPicker*)pElemento;

@end