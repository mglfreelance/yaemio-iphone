//
//  ParametrosDevolverListaEmpresa.m
//  Yaesmio
//
//  Created by freelance on 22/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ParametrosDevolverListaEmpresa.h"

@implementation ParametrosDevolverListaEmpresa

+ (ParametrosDevolverListaEmpresa *)deserializeNode:(xmlNodePtr)cur
{
	ParametrosDevolverListaEmpresa *newObject = [[ParametrosDevolverListaEmpresa alloc] init];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark- SoapObject

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Modo xmlNodeForDoc:node->doc elementName:@"Modo" elementNSPrefix:@"marcas"]);
}
@end
