//
//  UIViewController+Ampliado.h
//  Yaesmio
//
//  Created by freelance on 02/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (Ampliado)

- (void) presentModalViewController:(UIViewController *)modalViewController withPushDirection: (NSString *) direction;
- (void) dismissModalViewControllerWithPushDirection:(NSString *) direction;

@end
