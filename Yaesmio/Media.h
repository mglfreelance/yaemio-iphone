//
//  Media.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 19/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SoapObject.h"

@interface Media : SoapObject <NSCoding>

typedef NS_ENUM (NSInteger, MediaTypeMIME)
{
    MediaTypeMIME_indeterminate = 0,     // no se reconoce formato
    MediaTypeMIME_image         = 1,     // tipo Imagen
    MediaTypeMIME_video         = 2,     // tipo video
};

+ (Media *)		deserializeNode:(xmlNodePtr)cur;
+ (NSArray *)	FiltrarArray:(NSArray *)pLista PorTipo:(MediaTypeMIME)pType;
+ (BOOL)		comprobarLista:(NSArray *)pLista;
+ (Media *)		createMediaImage;
+ (Media *)		createMediaVideo;

- (id)			initWithUrl:(NSString *)pUrl UrlSmall:(NSString *)pUrlSmall AndMediaType:(MediaTypeMIME)pType;
- (Media *)		Clonar;
- (BOOL)		comprobarObjeto;

/* elements */
@property (strong) NSString *UrlPath;
@property (strong) NSString *UrlPathSmall;
@property (assign) MediaTypeMIME MediaType;

@end
