//
//  ValoresdeAtributo.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 31/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ValoresdeAtributo.h"

@implementation ValoresdeAtributo


- (id)init
{
	if((self = [super init]))
    {
		self.ProductoId = @"";
		self.DescripcionAtt1 = @"";
		self.DescripcionAtt2 = @"";
	}
	
	return self;
}

- (id)init:(NSString*)pAtributoID Nombre:(NSString*)pNombre ValorID:(NSString*)pValorID
{
	if((self = [self init]))
    {
		self.ProductoId = pAtributoID;
		self.DescripcionAtt1 = pNombre;
		self.DescripcionAtt2 = pValorID;
	}
	
	return self;
}


- (void)addElementsToNode:(xmlNodePtr)node
{
	xmlAddChild(node, [self.DescripcionAtt1 xmlNodeForDoc:node->doc elementName:@"DescripcionAtt1" elementNSPrefix:@"tns3"]);
	
	xmlAddChild(node, [self.DescripcionAtt2 xmlNodeForDoc:node->doc elementName:@"DescripcionAtt2" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.ProductoId xmlNodeForDoc:node->doc elementName:@"ProductoId" elementNSPrefix:@"tns3"]);
    
    //ValorId
}

+ (ValoresdeAtributo *)deserializeNode:(xmlNodePtr)cur
{
	ValoresdeAtributo *newObject = [[ValoresdeAtributo alloc] init];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
    
    if ([newObject.DescripcionAtt1 isEqualToString:@""])
    {   // si es "" es nil
        newObject = nil;
    }
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ProductoId"])
    {
        self.ProductoId = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"DescripcionAtt1"])
    {
        self.DescripcionAtt1 = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"DescripcionAtt2"])
    {
        self.DescripcionAtt2 = [NSString deserializeNode:pobjCur];
    }
}


-(BOOL) comprobarObjeto:(NSArray*)pListaAtributos ValoresdeAtributo:(NSArray*)pListaValores
{
    BOOL resul = false;
    BOOL existeValor2 = (pListaAtributos.count>1);
    
    for (ValoresdeAtributo *lValor in pListaValores)
    {
        if ([lValor.DescripcionAtt1 isEqualToString:self.DescripcionAtt1] && (existeValor2 == false || [lValor.DescripcionAtt2 isEqualToString:self.DescripcionAtt2]))
        {
            resul = true;
        }
    }
    
    return resul;
}

@end


@implementation NSArray (ValoresdeAtributo)

-(ValoresdeAtributo*)ObtenerValordeAtributoPorID:(NSString*)pID
{
    ValoresdeAtributo *lResultado = nil;
    
    if ([pID isEqualToString:@""] == false)
    {
        for (ValoresdeAtributo* lElemento in self)
        {
            if ([lElemento.ProductoId isEqualToString:pID])
            {
                lResultado = lElemento;
                break;
            }
        }
    }
    
    return lResultado;
}

-(int) obtenerIndiceValordeAtributo:(ValoresdeAtributo*)pElemento
{
    int lintRow = 0;
    for (int i = 0; i< self.count; i++)
    {
        if ([pElemento.ProductoId isEqualToString:((ValoresdeAtributo*)self[i]).ProductoId])
        {
            lintRow = i;
            break;
        }
    }
    
    return lintRow;
}


@end