//
//  FavoritoCell.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 19/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "FavoritoCell.h"
#import "UIAllControls.h"

@interface FavoritoCell ()

@property (weak, nonatomic) IBOutlet UIImageView *ImagenProducto;
@property (weak, nonatomic) IBOutlet UILabel *LabelNombreProducto;
@property (weak, nonatomic) IBOutlet UILabel *LabelNombreEmpresa;
@property (weak, nonatomic) IBOutlet UILabel *labelMoneda;
@property (weak, nonatomic) IBOutlet UILabel *LabelPrecio;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *IndicatorBusy;
@property (weak, nonatomic) IBOutlet UILabel *labelCaducado;
@property (weak, nonatomic) IBOutlet UIView *vistaCaducado;
@property (weak, nonatomic) IBOutlet UIImageView *ImagenRecomendado;
@property (weak, nonatomic) IBOutlet UIView *VistaDescuento;
@property (weak, nonatomic) IBOutlet UILabel *labelDescuento;
@property (weak, nonatomic) IBOutlet UIView *VistaMenuContextual;
@property (weak, nonatomic) IBOutlet UIView *ViewFondoCelda;

@end

@implementation FavoritoCell

#define GRUPO_FAVORITOCELL @"FavoritoCell"

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        // Initialization code
    }
    
    return self;
}

- (void)inicializar
{
    self.LabelNombreEmpresa.text  = @"";
    self.LabelNombreProducto.text = @"";
    self.ImagenProducto.image     = nil;
    self.labelMoneda.text         = @"";
    self.LabelPrecio.text         = @"";
    self.labelCaducado.hidden     = true;
    self.labelCaducado.text       = Traducir(@"OFERTA CADUCADA");
    self.ImagenRecomendado.hidden = true;
    self.labelDescuento.text      = @"";
}

- (void)ConfigurarConFavorito:(Favorito *)pFavorito MostrarImagenGrande:(BOOL)pMostrarImagenGrande Menu:(id)Menu

{
    [self inicializar];
    
    self.LabelNombreProducto.text = pFavorito.Nombre;
    self.LabelNombreEmpresa.text = pFavorito.NombreEmpresa;
    self.labelDescuento.text = [pFavorito.Descuento toStringDiscount];
    self.ImagenRecomendado.hidden = (!pFavorito.Recomendada);
    self.labelMoneda.text = pFavorito.AbreviaturaMoneda;
    self.LabelPrecio.text = [pFavorito.Precio toStringFormaterCurrency];
    self.VistaDescuento.hidden  = (pFavorito.Precio.isZeroOrLess);
    [self.LabelPrecio mgSizeFitsMaxWidth:MAXFLOAT MaxHeight:UNCHANGED];
    [self.labelMoneda mgMoveToControl:self.LabelPrecio Position:MGPositionMoveToBehind Separation:0.0];
    
    if (pFavorito.Caducado)
    {
        self.ImagenProducto.hidden = true;
        self.ImagenRecomendado.hidden = true;
        [self.ImagenProducto cargarUrlImagenAsincrono:[self getUrlImage:pFavorito Modo:pMostrarImagenGrande] Grupo:GRUPO_FAVORITOCELL Busy:self.IndicatorBusy Result:^
         {
             self.ImagenProducto.image = [[Locator Default].Imagen ConvertBlackAndWhiteImage:self.ImagenProducto.image];
             self.ImagenProducto.hidden = false;
         }];
    }
    else
    {
        [self.ImagenProducto cargarUrlImagenAsincrono:[self getUrlImage:pFavorito Modo:pMostrarImagenGrande] Busy:self.IndicatorBusy];
    }
    
    // ocultar o mostrar segun si caducado o no tiene precio
    self.LabelPrecio.hidden = (pFavorito.Precio.isZeroOrLess || pFavorito.Caducado);
    self.labelMoneda.hidden = (pFavorito.Precio.isZeroOrLess || pFavorito.Caducado);
    self.labelCaducado.hidden = !pFavorito.Caducado;
    self.vistaCaducado.hidden = !pFavorito.Caducado;
    
    [self.VistaMenuContextual mgRemoveAllGestureRecognizers];
    [self.VistaMenuContextual mgAddGestureRecognizerTap:Menu Action:@selector(showMenuUponActivationOfGetsure:)];
    
    [self.ViewFondoCelda mgRemoveAllGestureRecognizers];
    [self.ViewFondoCelda mgAddGestureRecognizerLongPress:Menu Action:@selector(showMenuUponActivationOfGetsure:)];
}

- (NSString *)getUrlImage:(Favorito *)pFavorito Modo:(BOOL)pGrande
{
    if (pGrande)
    {
        return pFavorito.Imagen.UrlPath;
    }
    else
    {
        return pFavorito.Imagen.UrlPathSmall;
    }
}

@end
