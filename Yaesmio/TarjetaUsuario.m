//
//  TarjetaUsuario.m
//  Yaesmio
//
//  Created by freelance on 09/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "TarjetaUsuario.h"

@implementation TarjetaUsuario

- (id)init
{
    if ((self = [super init]))
    {
        self.TipoTarjeta = TipoTarjetaUsuarioOtra;
        self.ModoTarjeta = ModoTarjetaUsuarioNinguna;
        self.FechaUltimoUso = nil;
        self.Mes = @"";
        self.Anio = @"";
        self.Referencia = @"";
        self.NumeroTarjeta = @"";
    }
    
    return self;
}

+ (TarjetaUsuario *)deserializeNode:(xmlNodePtr)cur
{
    TarjetaUsuario *newObject = [[TarjetaUsuario alloc] init];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

+ (TarjetaUsuario *)Ninguna
{
    TarjetaUsuario *ninguna = [TarjetaUsuario new];
    
    ninguna.TipoTarjeta = TipoTarjetaUsuarioNinguna;
    ninguna.ModoTarjeta = ModoTarjetaUsuarioNinguna;
    return ninguna;
}

+ (TarjetaUsuario *)Paypal
{
    TarjetaUsuario *paypal = [TarjetaUsuario new];
    
    paypal.TipoTarjeta = TipoTarjetaUsuarioPaypal;
    paypal.ModoTarjeta = ModoTarjetaUsuarioNinguna;
    return paypal;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ZZCCINS"])
    {
        self.TipoTarjeta = [self getTipoTarjetaSegunString:[NSString deserializeNode:pobjCur]];
    }
    else if ([nodeName isEqualToString:@"ZZCREDEB"])
    {
        self.ModoTarjeta = [self getModoTarjetaSegunString:[NSString deserializeNode:pobjCur]];
    }
    else if ([nodeName isEqualToString:@"ZZFECHA_ULT_USO"])
    {
        NSString *lDato = [NSString deserializeNode:pobjCur];
        
        if ([lDato isEqualToString:@"0000-00-00"] == false)
        {
            if (self.FechaUltimoUso == nil)
            {
                self.FechaUltimoUso = [NSDate dateWithTimeIntervalSince1970:0];
            }
            
            NSDate *mydate = [NSDate fromString:lDato WithFormat:FECHA_SOAP];
            self.FechaUltimoUso = [self.FechaUltimoUso dateWithYear:mydate.Year Month:mydate.Month Day:mydate.Day Hour:UNCHANGEDATE minute:UNCHANGEDATE second:UNCHANGEDATE];
        }
    }
    else if ([nodeName isEqualToString:@"ZZHORA_ULT_USO"])
    {
        NSString *lDato = [NSString deserializeNode:pobjCur];
        
        if ([lDato isEqualToString:@"00:00:00"] == false)
        {
            if (self.FechaUltimoUso == nil)
            {
                self.FechaUltimoUso = [NSDate dateWithTimeIntervalSince1970:0];
            }
            
            NSDate *myFecha = [NSDate fromString:lDato WithFormat:@"HH:mm:ss"];
            self.FechaUltimoUso = [self.FechaUltimoUso dateWithYear:UNCHANGEDATE Month:UNCHANGEDATE Day:UNCHANGEDATE Hour:myFecha.Hour minute:myFecha.Minute second:myFecha.Second];
        }
    }
    else if ([nodeName isEqualToString:@"ZZMONTH"])
    {
        self.Mes = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ZZYEAR"])
    {
        self.Anio = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ZZREFER"])
    {
        self.Referencia = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ZZTARJETA"])
    {
        self.NumeroTarjeta = [NSString deserializeNode:pobjCur];
    }
}

- (TipoTarjetaUsuario)getTipoTarjetaSegunString:(NSString *)Texto
{
    if ([Texto.uppercaseString isEqualToString:@"VISA"])
    {
        return TipoTarjetaUsuarioVisa;
    }
    
    if ([Texto.uppercaseString isEqualToString:@"MC"])
    {
        return TipoTarjetaUsuarioMasterCard;
    }
    else
    {
        return TipoTarjetaUsuarioOtra;
    }
}

- (ModoTarjetaUsuario)getModoTarjetaSegunString:(NSString *)Texto
{
    if ([Texto.uppercaseString isEqualToString:@"C"])
    {
        return ModoTarjetaUsuarioCredito;
    }
    
    if ([Texto.uppercaseString isEqualToString:@"D"])
    {
        return ModoTarjetaUsuarioDebito;
    }
    else
    {
        return ModoTarjetaUsuarioNinguna;
    }
}

@end
