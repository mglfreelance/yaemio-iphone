//
//  LoginUsuarioResponse.m
//  Yaesmio
//
//  Created by freelance on 29/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "LoginUsuarioResponse.h"

@implementation LoginUsuarioResponse

- (id)init
{
	if((self = [super init]))
    {
		self.UserID              = @"";
        self.SecurityToken       = @"";
        self.BackgroundImage     = @"";
        self.FavoritesNumber     = @"";
        self.NotificationsSound  = @"";
        self.NotificationsUnread = @"";
        self.UserName            = @"";
        self.HistoricNotRated    = @"";
	}
	
	return self;
}

+ (LoginUsuarioResponse *)deserializeNode:(xmlNodePtr)cur
{
	LoginUsuarioResponse *newObject = [LoginUsuarioResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ImagenFondo"])
    {
        self.BackgroundImage = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NumeroFavoritos"])
    {
        self.FavoritesNumber = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NumeroNotificacionesNoLeidas"])
    {
        self.NotificationsUnread = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NumeroComprasNoValoradas"])
    {
        self.HistoricNotRated = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Sonido"])
    {
        self.NotificationsSound = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Token"])
    {
        self.SecurityToken = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"UsuarioId"])
    {
        self.UserID = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Nombre"])
    {
        self.UserName = [NSString deserializeNode:pobjCur];
    }
}

@end
