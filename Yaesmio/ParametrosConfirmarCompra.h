//
//  ParametrosConfirmarCompra.h
//  Yaesmio
//
//  Created by mglFreelance on 26/11/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SoapObject.h"

@interface ParametrosConfirmarCompra : SoapObject

- (id)init:(NSString*)pIdCompra Paypal:(NSString*)pIdPaypal;

/* elements */
@property (strong) NSString  *IdCompra;
@property (strong) NSString  *IdPaypal;

@end
