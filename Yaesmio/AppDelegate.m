/*
 * Copyright 2012 ZXing authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#import "AppDelegate.h"
#import "ZBarReaderView.h"
#import "WCAlertView.h"
#import "UIFont+Replacement.h"
#import "MKGradientButton.h"
#import "DownloadManager.h"
#import "M13Checkbox.h"

@implementation AppDelegate

@synthesize window = _window;
@synthesize ActivatePushNotificacions = _ActivatePushNotificacions;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Locator Default].Application = self;     //para tener acceso a push notifications
    // force view class to load so it may be referenced directly from NIB
    [ZBarReaderView class];
    
    // configuro la windows donde se mostraran el indeterminate hub
    [[Locator Default].Alerta Initialize:self.window];
    
    [self ConfigureAlerView];
    [self ConfigureTabarItems];
    [self ConfigureButton];
    [self ConfigureUiBarButtonItem];
    [self configureCheckBoxes];
    [DownloadManager ClearAssyncFileCacheLastWeek];     // borra las imagenes cacheadas antiguas
    
    self.window.backgroundColor = [Locator Default].Color.Principal;
    [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
    
    [[Locator Default].Imagen Configurar];     // carga previa de background
    
    NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    
    if (notification)
    {
        [self application:application didReceiveRemoteNotification:notification];
    }
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)ConfigureAlerView
{
    [WCAlertView setDefaultCustomiaztonBlock: ^(WCAlertView *alertView)
     {
         alertView.labelTextColor = [Locator Default].Color.Blanco;
         alertView.labelShadowColor = [Locator Default].Color.SombraTextoAlertView;
         alertView.buttonBorderColor = [Locator Default].Color.Blanco;
         alertView.buttonCornerRadius = 10;
         alertView.gradientColors = [Locator Default].Color.DegradadoAlertView;
         alertView.outerFrameColor = [Locator Default].Color.Blanco;
         alertView.buttonTextColor = [Locator Default].Color.Blanco;
         alertView.buttonShadowColor = [Locator Default].Color.SombraTextoAlertView;
     }];
}

- (void)ConfigureTabarItems
{
    [[UITabBarItem appearance] setTitleTextAttributes:
     @{
       UITextAttributeTextColor: [Locator Default].Color.Blanco,
       UITextAttributeTextShadowColor: [Locator Default].Color.Clear,
       UITextAttributeTextShadowOffset: [NSValue valueWithUIOffset:UIOffsetMake(0, 1)],
       UITextAttributeFont: [Locator Default].FuenteLetra.TabViewItem
       } forState:UIControlStateNormal];
}

- (void)configureCheckBoxes
{
    [M13Checkbox setDefaultCustomiaztonBlock: ^(M13Checkbox *check)
     {
         check.checkAlignment = M13CheckboxAlignmentLeft;
         check.ancho = 20.0;
         check.radius = 4;
         check.checkColor = [Locator Default].Color.Principal;
         check.strokeColor = [Locator Default].Color.Principal;
         check.tintColor = [Locator Default].Color.GrisClaro;
     }];
}

- (void)ConfigureButton
{
    [MKGradientButton setDefaultCustomizationEnabledBlock: ^(MKGradientButton *pGradienButton)
     {
         pGradienButton.borderColor = [Locator Default].Color.Clear;
         pGradienButton.gradientArray = [Locator Default].Color.DegradadoBoton;
         pGradienButton.disabledGradientArray = [Locator Default].Color.DegradadoBotonDesactivado;
         pGradienButton.BorderRadius = 0.0;
         [pGradienButton setTitleShadowColor:[Locator Default].Color.Clear forState:UIControlStateNormal];
         [pGradienButton setTitleShadowColor:[Locator Default].Color.Clear forState:UIControlStateSelected];
         [pGradienButton setTitleShadowColor:[Locator Default].Color.Clear forState:UIControlStateHighlighted];
         pGradienButton.titleLabel.textAlignment = NSTextAlignmentCenter;
         pGradienButton.titleLabel.minimumScaleFactor = 0.5f;
         pGradienButton.titleLabel.shadowColor = [Locator Default].Color.Clear;
         [pGradienButton.titleLabel setFont:[Locator Default].FuenteLetra.Boton];
         pGradienButton.titleLabel.adjustsFontSizeToFitWidth = true;
     }];
}

- (void)ConfigureUiBarButtonItem
{
    if (SYSTEM_VERSION_LESS_THAN(@"7"))
    {
        [[UIBarButtonItem appearance] setTintColor:[Locator Default].Color.Principal];
    }
}

#pragma  mark- PUSH NOTIFICACIONS

- (BOOL)ActivatePushNotificacions
{
    return _ActivatePushNotificacions;
}

- (void)setActivatePushNotificacions:(BOOL)Activate
{
    if (Activate)
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
    else
    {
        _ActivatePushNotificacions = Activate;
        [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *idPush = [self getStringNSData:deviceToken];
    
    if (idPush.isNotEmpty)
    {
        NSLog(@"idPush: %@", idPush);
        [[Locator Default].ServicioSOAP ActivarNotificacionesYaesmio:idPush IdUsuario:[Locator Default].Preferencias.IdUsuario MacDevice:[Locator Default].Device.DeviceID16Long Telefono:@"" Result: ^(SRResultMethod pResult, NSString *MensajeSAP)
         {
             _ActivatePushNotificacions = (pResult = SRResultMethodYES);
             
             if (_ActivatePushNotificacions == true)
             {
                 [Locator Default].Preferencias.IdPush = idPush;
             }
             else
             {
                 [Locator Default].Preferencias.IdPush = @"";
             }
         }];
    }
    else
    {
        NSLog(@"No hemos recibido idPush correcto");
        _ActivatePushNotificacions = false;
    }
}

- (NSString *)getStringNSData:(NSData *)pData
{
    return [[[pData.description stringByReplacingOccurrencesOfString:@"<" withString:@""] stringByReplacingOccurrencesOfString:@">" withString:@""]stringByReplacingOccurrencesOfString:@" " withString:@""];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    _ActivatePushNotificacions = false;
    [Locator Default].Preferencias.IdPush = @"";
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSString *Nombre = [userInfo objectForKey:@"NombreEmisior"];
    NSString *Mensaje = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];
    NSString *IdNotificacion = [userInfo objectForKey:@"idEnvio"];
    
    if (IdNotificacion.isNotEmpty)
    {
        [[Locator Default].ServicioSOAP MarcarNotificacionRecibida:IdNotificacion Idusuario:[Locator Default].Preferencias.IdUsuario Tipo:MarcadoNotificacionRecibidaRecibida Result:nil];
        
        if (application.applicationState == UIApplicationStateActive)
        {
            [[Locator Default].Alerta showNotificationWithTitle:Nombre AndMessage:Mensaje PushResult:^
             {
                 [[Locator Default].Vistas MostrarNotificaciones:self.window.rootViewController];
             }];
        }
        else
        {
            [[Locator Default].Vistas MostrarNotificaciones:self.window.rootViewController];
        }
    }
}

@end
