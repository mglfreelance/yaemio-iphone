//
//  InicioPagesViewController.m
//  Yaesmio
//
//  Created by freelance on 07/03/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "PaginasViewController.h"

@interface PaginasViewController ()<UIPageViewControllerDataSource>

@property NSArray *Lista;

@end

@implementation PaginasViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.hidden = true;
    self.Lista = @[
                   [self.storyboard instantiateViewControllerWithIdentifier:@"PaginaDestacadosViewController"],
                   [self.storyboard instantiateViewControllerWithIdentifier:@"PaginaBusquedaViewController"],
                   [self.storyboard instantiateViewControllerWithIdentifier:@"PaginaUsuarioViewController"]];
    
    self.dataSource = self;
    [self setViewControllers:@[self.Lista[0]] direction:UIPageViewControllerNavigationDirectionForward animated:true completion:nil];
}

- (void)viewUnload
{
    [super viewUnload];
    
    self.Lista = nil;
    self.dataSource = nil;
}

#pragma mark - Delegate PageViewController

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    UIViewController *anterior = [self.Lista lastObject];
    
    for (UIViewController *View in self.Lista)
    {
        if (viewController == View)
        {
            return anterior;
        }
        
        anterior = View;
    }
    
    return anterior;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    BOOL encontrado = false;
    
    for (UIViewController *View in self.Lista)
    {
        if (encontrado)
        {
            return View;
        }
        
        encontrado = (View == viewController);
    }
    
    return self.Lista[0];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    [self setupPageControlAppearance:(int)self.Lista.count];
    return self.Lista.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.Lista indexOfObject:pageViewController];
}

#pragma mark - Private Methods

- (void)setupPageControlAppearance:(int)numberOfPages
{
    UIPageControl *pageControl = [[self.view.subviews filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(class = %@)", [UIPageControl class]]] lastObject];
    
    if (pageControl != nil)
    {
        pageControl.hidden = false;
        pageControl.numberOfPages = numberOfPages;
        pageControl.pageIndicatorTintColor = [Locator Default].Color.Blanco;
        pageControl.currentPageIndicatorTintColor = [Locator Default].Color.Principal;
    }
}

@end
