//
//  NavegacionViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 09/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import "NavegacionViewController.h"
#import "PrettyNavigationBar.h"

@interface NavegacionViewController ()

@property ( nonatomic) IBOutlet PrettyNavigationBar *BarraNavegacion;


@end

@implementation NavegacionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.BarraNavegacion.gradientStartColor = [Locator Default].Color.Principal;
    self.BarraNavegacion.gradientEndColor = [Locator Default].Color.Principal;
    self.BarraNavegacion.topLineColor = [Locator Default].Color.Principal;
    self.BarraNavegacion.bottomLineColor = [Locator Default].Color.Principal;
    self.BarraNavegacion.shadowOpacity= 0.0;
    self.BarraNavegacion.shadowImage =[UIImage new];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0"))
    {
        self.BarraNavegacion.tintColor = [Locator Default].Color.Blanco;
    }
}

- (void)viewDidUnload
{
    [self setBarraNavegacion:nil];
    [super viewDidUnload];
}

- (BOOL) shouldAutorotate
{
    return NO;
}

@end
