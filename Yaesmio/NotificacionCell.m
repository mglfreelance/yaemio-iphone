//
//  NotificacionRecibidaCell.m
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "NotificacionCell.h"
#import "UIAllControls.h"

@interface NotificacionCell ()

@property (weak, nonatomic) IBOutlet UIImageView *imagenProducto;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *busyProducto;
@property (weak, nonatomic) IBOutlet UIImageView *ImagenRecomendado;
@property (weak, nonatomic) IBOutlet UIView *ViewFondoCelda;
@property (weak, nonatomic) IBOutlet UILabel *labelNombreProducto;
@property (weak, nonatomic) IBOutlet UILabel *labelNombreEmpresa;
@property (weak, nonatomic) IBOutlet UILabel *labelMoneda;
@property (weak, nonatomic) IBOutlet UILabel *labelPrecio;
@property (weak, nonatomic) IBOutlet UILabel *labelNombrePersona;
@property (weak, nonatomic) IBOutlet UILabel *labelFecha;
@property (weak, nonatomic) IBOutlet UIView *VistaMenuContextual;
@property (weak, nonatomic) IBOutlet UILabel *labelTextoMensaje;
@property (weak, nonatomic) IBOutlet UIView *viewDescuento;
@property (weak, nonatomic) IBOutlet UILabel *labelDescuento;

@end

@implementation NotificacionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        [self inicializar];
    }
    
    return self;
}

- (void)inicializar
{
    self.labelNombreProducto.text = @"";
    self.labelNombreEmpresa.text  = @"";
    self.labelMoneda.text         = @"";
    self.labelPrecio.text         = @"";
    self.labelNombrePersona.text  = @"";
    self.imagenProducto.image     = nil;
    self.labelFecha.text          = @"";
    self.labelTextoMensaje.text   = @"";
    [self.busyProducto stopAnimating];
    self.ImagenRecomendado.hidden = true;
}

- (void)Configurar:(CabeceraNotificacion *)pNotificacion Menu:(id)Menu
{
    [self inicializar];
    
    self.labelNombreProducto.text = pNotificacion.NombreProducto;
    self.labelNombreEmpresa.text = pNotificacion.Empresa;
    self.labelMoneda.text = pNotificacion.Moneda;
    self.labelPrecio.text = [pNotificacion.Precio toStringFormaterCurrency];
    self.labelDescuento.text = [pNotificacion.Descuento toStringDiscount];
    self.ImagenRecomendado.hidden = !pNotificacion.Recomendada;
    self.labelPrecio.hidden = (pNotificacion.Precio.isZeroOrLess);
    self.labelMoneda.hidden = (pNotificacion.Precio.isZeroOrLess);
    self.viewDescuento.hidden = (pNotificacion.Precio.isZeroOrLess);
    
    self.labelNombrePersona.text = [[Locator Default].Contactos getNombreContactoPorMail:pNotificacion.MailEmisior];
    [self.imagenProducto cargarUrlImagenAsincrono:pNotificacion.Imagen.UrlPathSmall Busy:self.busyProducto];
    self.labelFecha.text = [pNotificacion.Fecha format:Traducir(@"dd/mm/yyyy")];
    self.labelTextoMensaje.text = pNotificacion.TextoEnvio;
    
    //calcular tamaño labels
    [self.labelNombrePersona mgSizeFitsMaxWidth:UNCHANGED MaxHeight:50];
    [self.labelTextoMensaje mgSizeFitsMaxWidth:UNCHANGED MaxHeight:60];
    [self.labelTextoMensaje mgMoveToControl:self.labelNombrePersona Position:MGPositionMoveToBelow Separation:3.0f];
    
    // cambio color segun el tipo de notificacion (si visualizada o no)
    self.labelNombrePersona.textColor = [self getColorSegunTipoNotifiacion:pNotificacion.Tipo];
    self.labelTextoMensaje.textColor = [self getColorSegunTipoNotifiacion:pNotificacion.Tipo];
    self.labelFecha.textColor = [self getColorSegunTipoNotifiacion:pNotificacion.Tipo];
    self.labelMoneda.textColor = [self getColorSegunTipoNotifiacion:pNotificacion.Tipo];
    self.labelPrecio.textColor = [self getColorSegunTipoNotifiacion:pNotificacion.Tipo];
    self.labelNombreEmpresa.textColor = [self getColorSegunTipoNotifiacion:pNotificacion.Tipo];
    self.labelNombreProducto.textColor = [self getColorSegunTipoNotifiacion:pNotificacion.Tipo];
    
    [self.VistaMenuContextual mgRemoveAllGestureRecognizers];
    [self.VistaMenuContextual mgAddGestureRecognizerTap:Menu Action:@selector(showMenuUponActivationOfGetsure:)];
    
    [self.ViewFondoCelda mgRemoveAllGestureRecognizers];
    [self.ViewFondoCelda mgAddGestureRecognizerLongPress:Menu Action:@selector(showMenuUponActivationOfGetsure:)];
}

- (UIColor *)getColorSegunTipoNotifiacion:(SNTipoNotificacion)pTipo
{
    switch (pTipo)
    {
        case SNTipoNotificacionRecibida:
            return [Locator Default].Color.Principal;
            
        default:
            return [Locator Default].Color.Negro;
    }
}

@end
