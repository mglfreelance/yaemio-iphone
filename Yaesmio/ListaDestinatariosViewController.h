//
//  ListaDestinatariosViewController.h
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "YaesmioViewControllers.h"

@interface ListaDestinatariosViewController : YaesmioTableViewController

@property (strong)NSArray *ListaDestinatarios;

@end
