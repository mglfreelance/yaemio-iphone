//
//  ProductosFavoritosManager.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 16/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductoPadre.h"
#import "Favorito.h"

@protocol ProductosFavoritosManagerDelegate <NSObject>

-(void) CambiadoElementos;

@end

@interface ProductosFavoritosManager : NSObject

-(void) addTabBarItem:(UITabBarItem*)pItem andTabBar:(UIView*) pTabBar;

@property (readonly)  int       NumeroFavoritosToTal;
//@property (nonatomic) id<ProductosFavoritosManagerDelegate> Delegate;

typedef NS_ENUM(NSInteger, SRResultProductoFavoritoManager)
{
    SRResultProductoFavoritoManagerYES       = 1, // todo OK
    SRResultProductoFavoritoManagerNO        = 2, // devolucion no OK
    SRResultProductoFavoritoManagerErrConex  = 0, // err del servicio
};
typedef void (^ProductoFavoritoBlock)(SRResultProductoFavoritoManager pResult, NSString *pMensaje);


// metodos

-(void) AgregarProducto:(ProductoPadre*)pProducto Result:(ProductoFavoritoBlock)pResultado;
-(void) EliminarProducto:(Favorito*)pProducto Result:(ProductoFavoritoBlock)pResultado;
-(void) AgregarACarrito:(Favorito*)pFavorito Producto:(ProductoPadre*)pProducto Unidades:(NSDecimalNumber*)pNumero Result:(ProductoFavoritoBlock)pResultado;
-(void) ActualizarEstado;

typedef void (^ListaFavoritosBlock)(SRResultProductoFavoritoManager pResult, NSArray *pLista, NSString * pMensaje, int pNumeroFavoritosTotales);
-(void)ObtenerListaFavoritos:(int)posInicio Result:(ListaFavoritosBlock)pResultado;

-(void) actualizarNumeroFavoritos:(NSString*) pNUmeroFavoritos;

@end
