//
//  ProductoHijo.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 31/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ProductoPadre.h"
#import "Atributo.h"
#import "ValoresdeAtributo.h"

@interface ProductoPadre ()

@end

@implementation ProductoPadre

- (id)init
{
    if ((self = [super init]))
    {
        self.CantidadMax                    = nil;
        self.EmpresaId                      = @"";
        self.ListaAtributos                 = nil;
        self.ProductoId                     = @"";
        self.ImagenQR                       = [Media createMediaImage];
        self.LogoEmpresa                    = [Media createMediaImage];
        self.UrlWebEmpresa                  = @"";
        self.DiasEntregaMax                 = nil;
        self.ListaAtributos                 = nil;
        self.ProductoHijo                   = nil;
        self.NombreEmpresa                  = @"";
        self.UrlCondicionesLegalesEmpresa   = @"";
        self.SinDireccionEnvio              = true;
        self.UrlPoliticaPrivacidadEmpresa   = @"";
        self.Medio                          = @"";
        self.TipoCanal                      = [TipoCanal TipoCanalBusqueda];
        self.Estadisticas                   = nil;
    }
    
    return self;
}

- (Atributo *)Atributo1
{
    if (self.ListaAtributos.count >= 1)
    {
        return self.ListaAtributos[0];
    }
    else
    {
        return nil;
    }
}

- (Atributo *)Atributo2
{
    if (self.ListaAtributos.count >= 2)
    {
        return self.ListaAtributos[1];
    }
    else
    {
        return nil;
    }
}

- (NSString *)TextoPropiedades
{
    NSString *lResultado = @"";
    
    if (self.Atributo1 != nil)
    {
        lResultado = [NSString stringWithFormat:@"%@: %@", self.Atributo1.NombreAtributo, self.ProductoHijo.Atributos.DescripcionAtt1];
    }
    
    if (self.Atributo2 != nil)
    {
        lResultado = [NSString stringWithFormat:@"%@ %@: %@", lResultado, self.Atributo2.NombreAtributo, self.ProductoHijo.Atributos.DescripcionAtt2];
    }
    
    return [lResultado stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (NSString *)TextoAtributos
{
    NSString *lResultado = @"";
    
    if (self.Atributo1 != nil)
    {
        lResultado = [NSString stringWithFormat:@"%@", self.ProductoHijo.Atributos.DescripcionAtt1];
    }
    
    if (self.Atributo2 != nil)
    {
        lResultado = [NSString stringWithFormat:@"%@ %@", lResultado, self.ProductoHijo.Atributos.DescripcionAtt2];
    }
    
    return [lResultado stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

- (ProductoPadre *)Clonar:(Producto *)pHijo
{
    ProductoPadre *Resultado = [[ProductoPadre alloc] init];
    
    Resultado.CantidadMax = [NSNumber numberWithDouble:[self.CantidadMax doubleValue]];
    Resultado.EmpresaId = [NSString stringWithString:self.EmpresaId];
    Resultado.ListaValoresAtributos = [NSArray arrayWithArray:self.ListaValoresAtributos];
    Resultado.ProductoId = [NSString stringWithString:self.ProductoId];
    Resultado.ImagenQR = [self.ImagenQR Clonar];
    Resultado.LogoEmpresa = [self.LogoEmpresa Clonar];
    Resultado.UrlWebEmpresa = [NSString stringWithString:self.UrlWebEmpresa];
    Resultado.DiasEntregaMax = [NSNumber numberWithDouble:[self.DiasEntregaMax doubleValue]];
    Resultado.ListaAtributos = [NSArray arrayWithArray:self.ListaAtributos];
    pHijo.Latitud = self.ProductoHijo.Latitud;     //se guarda el gps previo
    pHijo.Longitud = self.ProductoHijo.Longitud;     // se guardan el gps previo
    Resultado.ProductoHijo = pHijo;
    Resultado.NombreEmpresa = self.NombreEmpresa;
    Resultado.UrlCondicionesLegalesEmpresa = self.UrlCondicionesLegalesEmpresa;
    Resultado.SinDireccionEnvio = self.SinDireccionEnvio;
    Resultado.UrlPoliticaPrivacidadEmpresa = self.UrlPoliticaPrivacidadEmpresa;
    Resultado.Medio = self.Medio;
    Resultado.TipoCanal = self.TipoCanal;
    Resultado.Estadisticas = [self.Estadisticas Clonar];
    
    return Resultado;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.CantidadMax xmlNodeForDoc:node->doc elementName:@"CantidadMax" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.DiasEntregaMax xmlNodeForDoc:node->doc elementName:@"DiasEntregaMax" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.EmpresaId xmlNodeForDoc:node->doc elementName:@"EmpresaId" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.ListaAtributos xmlNodeForDoc:node->doc elementName:@"ListaAtributos" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.ListaValoresAtributos xmlNodeForDoc:node->doc elementName:@"ListaValoresAtributos" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.Medio xmlNodeForDoc:node->doc elementName:@"Medio" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.ProductoId xmlNodeForDoc:node->doc elementName:@"ProductoId" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.ImagenQR.UrlPath xmlNodeForDoc:node->doc elementName:@"RutaImagenQr" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.LogoEmpresa.UrlPath xmlNodeForDoc:node->doc elementName:@"RutaLogoEmpresa" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.UrlWebEmpresa xmlNodeForDoc:node->doc elementName:@"UrlWebEmpresa" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.NombreEmpresa xmlNodeForDoc:node->doc elementName:@"NombreEmpresa" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.UrlCondicionesLegalesEmpresa xmlNodeForDoc:node->doc elementName:@"UrlCondicionesLegalesEmpresa" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.UrlPoliticaPrivacidadEmpresa xmlNodeForDoc:node->doc elementName:@"UrlPoliticaPrivacidadEmpresa" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.SinDireccionEnvio ? @"0":@"1" xmlNodeForDoc:node->doc elementName:@"SinDireccionEnvio" elementNSPrefix:@"tns3"]);
}

+ (ProductoPadre *)deserializeNode:(xmlNodePtr)cur
{
    ProductoPadre *newObject = [ProductoPadre new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"CantidadMax"])
    {
        self.CantidadMax = [NSNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"EmpresaId"])
    {
        self.EmpresaId = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"DiasEntregaMax"])
    {
        self.DiasEntregaMax = [NSNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ListaAtributos"])
    {
        self.ListaAtributos = [NSArray deserializeNode:pobjCur toClass:[Atributo class]];
    }
    else if ([nodeName isEqualToString:@"ListaValoresAtributos"])
    {
        self.ListaValoresAtributos = [NSArray deserializeNode:pobjCur toClass:[ValoresdeAtributo class]];
    }
    else if ([nodeName isEqualToString:@"ProductoId"])
    {
        self.ProductoId = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"RutaImagenQr"])
    {
        self.ImagenQR.UrlPath = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"RutaLogoEmpresa"])
    {
        self.LogoEmpresa.UrlPath = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"UrlWebEmpresa"])
    {
        self.UrlWebEmpresa = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ProdHijoDefecto"])
    {
        self.ProductoHijo = [Producto deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NombreEmpresa"])
    {
        self.NombreEmpresa = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"UrlCondLegalEmpresa"])
    {
        self.UrlCondicionesLegalesEmpresa = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"UrlPoliticaPrivacidad"])
    {
        self.UrlPoliticaPrivacidadEmpresa = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"SinDireccionEnvio"])
    {
        self.SinDireccionEnvio = [SOAP_Boolean deserializeNode:pobjCur].boolValue;
    }
    else if ([nodeName isEqualToString:@"Medio"])
    {
        self.Medio = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"EstadisticasProducto"])
    {
        self.Estadisticas = [EstadisticasProducto deserializeNode:pobjCur];
    }
}

- (BOOL)comprobarObjeto
{
    BOOL resul = true;
    
    resul = resul && (self.ProductoHijo != nil && [self.ProductoHijo comprobarObjeto:self.ListaAtributos ValoresdeAtributo:self.ListaValoresAtributos]);
    resul = resul && ((self.ListaAtributos == nil && self.ListaValoresAtributos == nil) || (self.ListaAtributos.count == 0 && self.ListaValoresAtributos.count == 0) || (self.ListaAtributos.count > 0 && self.ListaValoresAtributos.count > 0));
    resul = resul && [self revisarAtributos];
    resul = resul && [self revisarValoresAtributos];
    resul = resul && [self.LogoEmpresa comprobarObjeto];
    
    return resul;
}

- (BOOL)revisarAtributos
{
    BOOL resul = true;
    
    for (Atributo *valor in self.ListaAtributos)
    {
        resul = resul && [valor comprobarObjeto];
    }
    
    return resul;
}

- (BOOL)revisarValoresAtributos
{
    BOOL resul = true;
    
    for (ValoresdeAtributo *valor in self.ListaValoresAtributos)
    {
        resul = resul && [valor comprobarObjeto:self.ListaAtributos ValoresdeAtributo:self.ListaValoresAtributos];
    }
    
    return resul;
}

@end
