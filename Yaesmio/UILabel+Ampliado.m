//
//  UILabel+Ampliado.m
//  Yaesmio
//
//  Created by freelance on 27/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "UILabel+Ampliado.h"

@implementation UILabel (Ampliado)

- (void)mgAdjustFontSizeToFit
{
    UIFont *font = self.font;
    CGSize size = self.frame.size;
    
    for (CGFloat maxSize = self.font.pointSize; maxSize >= self.minimumScaleFactor * self.font.pointSize; maxSize -= 1.f)
    {
        font = [font fontWithSize:maxSize];
        CGSize constraintSize = CGSizeMake(size.width, MAXFLOAT);
        CGSize labelSize = [self.text sizeWithFont:font constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
        
        if (labelSize.height <= size.height)
        {
            self.font = font;
            [self setNeedsLayout];
            break;
        }
    }
    
    // set the font to the minimum size anyway
    self.font = font;
    [self setNeedsLayout];
}

@end
