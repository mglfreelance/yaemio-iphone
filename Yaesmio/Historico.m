//
//  Historico.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 29/07/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "Historico.h"

@implementation Historico


- (id)init
{
    if ((self = [super init]))
    {
        self.AbreviaturaMoneda  = @"";
        self.Precio             = [NSDecimalNumber zero];
        self.Caducado           = false;
        self.ProductoId         = @"";
        self.Referencia         = @"";
        self.Imagen             = [Media createMediaImage];
        self.ValoresAtributos   = nil;
        self.ListaAtributos     = nil;
        self.LogoMarca          = [Media createMediaImage];
        self.Nombre             = @"";
        self.NumElementos       = [NSDecimalNumber zero];
        self.ProductoIdPadre    = @"";
        self.Fecha              = nil;
        self.Unidades           = [NSDecimalNumber zero];
        self.NombreUnidades     = @"";
        self.NombreEmpresa      = @"";
        self.Valoracion         = 0;
        self.IdPedido           = @"";
    }
    
    return self;
}

- (Atributo *)Atributo1
{
    if (self.ListaAtributos.count >= 1)
    {
        return self.ListaAtributos[0];
    }
    else
    {
        return nil;
    }
}

- (Atributo *)Atributo2
{
    if (self.ListaAtributos.count >= 2)
    {
        return self.ListaAtributos[1];
    }
    else
    {
        return nil;
    }
}

+ (Historico *)deserializeNode:(xmlNodePtr)cur
{
    Historico *newObject = [Historico new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    // NSLog(@"campo: %@",[NSString stringWithUTF8String: pobjCur->name]);
    
    if ([nodeName isEqualToString:@"Caducado"])
    {
        self.Caducado = ![[NSString deserializeNode:pobjCur] isEqualToString:@""];
    }
    else if ([nodeName isEqualToString:@"Moneda"])
    {
        self.AbreviaturaMoneda = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ListaAtributos"])
    {
        if (pobjCur->children == nil)
        {
            self.ListaAtributos = nil;
        }
        else
        {
            self.ListaAtributos = [NSArray deserializeNode:pobjCur];
        }
    }
    else if ([nodeName isEqualToString:@"ListaValoresAtributos"])
    {
        if (pobjCur->children == nil)
        {
            self.ValoresAtributos = nil;
        }
        else
        {
            self.ValoresAtributos = [ValoresdeAtributo deserializeNode:pobjCur->children];
        }
    }
    else if ([nodeName isEqualToString:@"PrecioActual"])
    {
        self.Precio =  [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"LogoMarca"])
    {
        self.LogoMarca.UrlPath = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"LogoMarcaPequeno"])
    {
        self.LogoMarca.UrlPathSmall = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NumElementos"])
    {
        self.NumElementos = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"IdProd"])
    {
        self.ProductoId = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Nombre"])
    {
        self.Nombre = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Referencia"])
    {
        self.Referencia = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Imagen"])
    {
        self.Imagen.UrlPath = ComprobarImagen([NSString deserializeNode:pobjCur]);
    }
    else if ([nodeName isEqualToString:@"ImagenPequena"])
    {
        self.Imagen.UrlPathSmall = ComprobarImagen([NSString deserializeNode:pobjCur]);
    }
    else if ([nodeName isEqualToString:@"Fecha"])
    {
        NSString *lDato = [NSString deserializeNode:pobjCur];
        
        if ([lDato isEqualToString:@"0000-00-00"] == false)
        {
            self.Fecha = [NSDate fromString:lDato WithFormat:FECHA_SOAP];
        }
    }
    else if ([nodeName isEqualToString:@"IdProdPadre"])
    {
        self.ProductoIdPadre = ComprobarImagen([NSString deserializeNode:pobjCur]);
    }
    else if ([nodeName isEqualToString:@"Unidades"])
    {
        self.Unidades =  [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"DescUnidades"])
    {
        self.NombreUnidades = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NombreEmpresa"])
    {
        self.NombreEmpresa = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Puntuacion"])
    {
        self.Valoracion = [[NSNumber deserializeNode:pobjCur] intValue];
    }
    else if ([nodeName isEqualToString:@"IdPedido"])
    {
        self.IdPedido = [NSString deserializeNode:pobjCur];
    }
}

- (NSString *)TextoPropiedades
{
    NSString *lResultado = @"";
    
    if (self.Atributo1 != nil)
    {
        lResultado = [NSString stringWithFormat:@"%@: %@", self.Atributo1.NombreAtributo, self.ValoresAtributos.DescripcionAtt1];
    }
    
    if (self.Atributo2 != nil)
    {
        lResultado = [NSString stringWithFormat:@"%@ %@: %@", lResultado, self.Atributo2.NombreAtributo, self.ValoresAtributos.DescripcionAtt2];
    }
    
    if (self.Unidades.intValue > 0)
    {
        lResultado = [NSString stringWithFormat:@"%@ %@: %@", lResultado, self.NombreUnidades, self.Unidades];
    }
    
    return [lResultado stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

@end
