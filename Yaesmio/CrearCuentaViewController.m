//
//  CrearCuentaViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import "CrearCuentaViewController.h"
#import "UIAllControls.h"
#import "CondicionesUsoViewController.h"

@interface CrearCuentaViewController ()

@property (nonatomic) IBOutlet UITextField *txtEmail;
@property (nonatomic) IBOutlet UITextField *txtClave;
@property (nonatomic) IBOutlet UITextField *txtRepetirClave;
@property (nonatomic) IBOutlet MKGradientButton *btnGuardarCuenta;
@property (nonatomic) IBOutlet UIScrollView *ScrollViewDatos;
@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCamposTraducir;

- (IBAction)btnGuardarCuenta_click:(MKGradientButton *)sender;

@property (nonatomic) ScrollManager *manager;

@end

@implementation CrearCuentaViewController

typedef NS_ENUM (NSInteger, ComprobarCrearCuentaEnum)
{
    ComprobarCrearCuentaEnumOK               = 0,
    ComprobarCrearCuentaEnumMailIncorrecto   = 1,
    ComprobarCrearCuentaEnumClavesDiferentes = 2,
    ComprobarCrearCuentaEnumClaveIncorrecta  = 3,
    ComprobarCrearCuentaEnumMailLargo        = 4,
};

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaCamposTraducir);
    
    self.manager = [ScrollManager NewAndConfigureWithScrollView:self.ScrollViewDatos AndDidEndTextField:self Selector:@selector(btnGuardarCuenta_click:)];
    
    [self.ScrollViewDatos setHeightContentSizeForSubviewUntilControl:self.btnGuardarCuenta AddSize:CGSizeMake(-6, 0)];
    
    self.txtEmail.text = [Locator Default].Preferencias.MailUsuario;
}

- (void)viewUnload
{
    [self setTxtEmail:nil];
    [self setTxtClave:nil];
    [self setTxtRepetirClave:nil];
    [self setBtnGuardarCuenta:nil];
    [self setScrollViewDatos:nil];
    [self setVistaInicial:nil];
    [self setResultado:nil];
    [self setListaCamposTraducir:nil];
    [self setManager:nil];
}

#pragma mark- Metodos Privados

- (IBAction)btnGuardarCuenta_click:(MKGradientButton *)sender
{
    switch ([self comprobarCampos])
    {
        case ComprobarCrearCuentaEnumClaveIncorrecta:
            [[Locator Default].Alerta showMessageAceptWithTitle:Traducir(@"¡Ups!") AndMessage:Traducir(@"La contraseña introducida no es correcta. Recuerda que debe de tener entre 8 y 15 caracteres y al menos una letra y un numero.")];
            break;
            
        case ComprobarCrearCuentaEnumClavesDiferentes:
            [[Locator Default].Alerta showMessageAceptWithTitle:Traducir(@"¡Ups!") AndMessage:Traducir(@"Las contraseñas que has introducido no coinciden.")];
            break;
            
        case ComprobarCrearCuentaEnumMailIncorrecto:
            [[Locator Default].Alerta showMessageAceptWithTitle:Traducir(@"¡Ups!") AndMessage:Traducir(@"El email introducido no es correcto. Por favor, verifica que contenga @ y el punto [.] despues de la @")];
            break;
            
        case ComprobarCrearCuentaEnumMailLargo:
            [[Locator Default].Alerta showMessageAceptWithTitle:Traducir(@"¡Ups!") AndMessage:Traducir(@"El email introducido no es correcto. No puede superar los 40 caracteres de largo.")];
            break;
            
        case ComprobarCrearCuentaEnumOK:
            [[Locator Default].Alerta showBussy:Traducir(@"Comprobando si existe el usuario.")];
            [[Locator Default].ServicioSOAP ComprovarExisteEmail:self.txtEmail.text Result:^(SRResultMethod pResult)
             {
                 switch (pResult)
                 {
                     case SRResultMethodErrConex:
                         [[Locator Default].Alerta showErrorConexSOAP];
                         break;
                         
                     case SRResultMethodYES:
                         [[Locator Default].Alerta showMessageAcept:Traducir(@"Ya existe un usuario con este mail.")];
                         break;
                         
                     case SRResultMethodNO:
                         [[Locator Default].Alerta hideBussy];
                         [self performSegueWithIdentifier:@"segue_mostrarCondiciones" sender:self];
                         break;
                 }
             }];
            break;
    }
}

- (ComprobarCrearCuentaEnum)comprobarCampos
{
    if ([self.txtEmail.text isEqualToString:@""] || [[Locator Default].ExpresionesRegulares ComprobarMail:self.txtEmail.text] == false)
    {
        return ComprobarCrearCuentaEnumMailIncorrecto;
    }
    
    if (self.txtEmail.text.length > 40)
    {
        return ComprobarCrearCuentaEnumMailLargo;
    }
    
    if ([[Locator Default].ExpresionesRegulares ComprobarPassword:self.txtClave.text] == false)
    {
        return ComprobarCrearCuentaEnumClaveIncorrecta;
    }
    
    if ([self.txtClave.text isEqualToString:self.txtRepetirClave.text] == false)
    {
        return ComprobarCrearCuentaEnumClavesDiferentes;
    }
    
    return ComprobarCrearCuentaEnumOK;
}

#pragma mark- Metodos Privados

- (BOOL)shouldAutorotate
{
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_mostrarCondiciones"])
    {
        CondicionesUsoViewController *lVista = segue.destinationViewController;
        
        lVista.NombreUsuario = self.txtEmail.text;
        lVista.PasswordUsuario = self.txtClave.text;
        lVista.VistaInicial = self.VistaInicial;
        lVista.Resultado = self.Resultado;
    }
}

@end
