//
//  SitiosYaesmioViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 12/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SitiosYaesmioViewController.h"
#import "UIAllControls.h"
#import "SitioYaesmioCell.h"

@interface SitiosYaesmioViewController ()

@property (strong) NSArray *ListaSitios;
@property (nonatomic) IBOutlet UILabel *labelTituloSitios;
@property (strong, nonatomic) IBOutletCollection(UILabel) NSArray *ListaControles;

@end

@implementation SitiosYaesmioViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaControles);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
    
    [self obtenerListaSitios];
}

- (void)viewDidUnload
{
    [self setListaSitios:nil];
    [super viewDidUnload];
}

#pragma mark- Delegado UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ListaSitios.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SitioYaesmioCell *Celda= [tableView dequeueReusableCellWithIdentifier:@"SitioYaesmioCell"];
    float resul = [Celda obtenerPreviewHeightCelda:[self getSitioIndexPath:indexPath]];
   
    return resul;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SitioYaesmioCell *lVistaCelda = [tableView dequeueReusableCellWithIdentifier:@"SitioYaesmioCell"];
    
    [lVistaCelda ConfigurarConSitio:[self getSitioIndexPath:indexPath]];
    
    return lVistaCelda;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    SitioYaesmio *Sitio = [self getSitioIndexPath:indexPath];
    
    return ([Sitio.CodigoTextoHTML isEqualToString:@""] == FALSE);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:FALSE];
    
    SitioYaesmio *Sitio = [self getSitioIndexPath:indexPath];
    
    if ([Sitio.CodigoTextoHTML isEqualToString:@""] == false)
    {
        [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo datos.")];
        [[Locator Default].ServicioSOAP ObtenerTexto:Sitio.CodigoTextoHTML Result:^(SRResultMethod pResult, NSString *pTexto)
         {
             switch (pResult)
             {
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
                     
                 case SRResultMethodNO:
                     [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible obtener la informacion a mostrar.")];
                     break;
                     
                 case SRResultMethodYES:
                     [[Locator Default].Alerta hideBussy];
                     [[Locator Default].Vistas MostrarWebView:self ContenidoWeb:pTexto];
                     break;
             }
         }];
    }
}

#pragma mark- Privado

-(SitioYaesmio*)getSitioIndexPath:(NSIndexPath*)pIndexPath
{
    return self.ListaSitios[pIndexPath.row];
}

- (void)obtenerListaSitios
{
    [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo datos.")];
    [[Locator Default].ServicioSOAP ObtenerListaSitiosYaesmio:^(SRResultMethod pResult, NSArray *pLista)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible obtener la informacion a mostrar.")];
                 break;
                 
             case SRResultMethodYES:
                 [[Locator Default].Alerta hideBussy];
                 self.ListaSitios = pLista;
                 [self.tableView reloadData];
                 break;
         }
     }];
}


@end
