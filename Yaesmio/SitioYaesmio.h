//
//  SitioYaesmio.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 12/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SoapObject.h"

@interface SitioYaesmio : SoapObject

+ (SitioYaesmio *)deserializeNode:(xmlNodePtr)cur;


/* elements */
@property (strong) NSString *Cabecera;
@property (strong) NSString *Texto;
@property (strong) Media    *Imagen;
@property (strong) NSString *CodigoTextoHTML;


@end