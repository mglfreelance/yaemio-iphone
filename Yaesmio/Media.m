//
//  Media.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 19/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "Media.h"

@implementation Media

@synthesize UrlPath = _UrlPath;
@synthesize UrlPathSmall = _UrlPathSmall;

- (id)init
{
    if ((self = [self initWithUrl:@"" UrlSmall:@"" AndMediaType:MediaTypeMIME_indeterminate]))
    {
        _UrlPathSmall = @"";
        _UrlPath = @"";
    }
    
    return self;
}

- (id)initWithUrl:(NSString *)pUrl UrlSmall:(NSString *)pUrlSmall AndMediaType:(MediaTypeMIME)pType
{
    if ((self = [super init]))
    {
        self.UrlPath        = pUrl;
        self.UrlPathSmall   = pUrlSmall;
        self.MediaType      = pType;
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    if (!self)
    {
        return nil;
    }
    self.UrlPath      = [aDecoder decodeObjectForKey:@"UrlPath"];
    self.UrlPathSmall = [aDecoder decodeObjectForKey:@"UrlPathSmall"];
    self.MediaType    = (int)[aDecoder decodeIntegerForKey:@"MediaType"];
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.UrlPath forKey:@"UrlPath"];
    [aCoder encodeObject:self.UrlPathSmall forKey:@"UrlPathSmall"];
    [aCoder encodeInteger:self.MediaType forKey:@"MediaType"];
}

+ (NSArray *)FiltrarArray:(NSArray *)pLista PorTipo:(MediaTypeMIME)pType
{
    NSMutableArray *resul = [[NSMutableArray alloc] init];
    
    for (Media *lmedia in pLista)
    {
        if (lmedia.MediaType == pType)
        {
            [resul addObject:lmedia];
        }
    }
    
    return resul;
}

+ (Media *)createMediaImage
{
    return [[Media alloc] initWithUrl:@"" UrlSmall:@"" AndMediaType:MediaTypeMIME_image];
}

+ (Media *)createMediaVideo
{
    return [[Media alloc] initWithUrl:@"" UrlSmall:@"" AndMediaType:MediaTypeMIME_video];
}

- (void)addElementsToNode:(xmlNodePtr)node
{
}

+ (Media *)deserializeNode:(xmlNodePtr)cur
{
    Media *newObject = [Media new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    if ([newObject.UrlPath isEqualToString:@""])       // si es "" es nil
    {
        newObject = nil;
    }
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    // NSLog(@"campo: %@",[NSString stringWithUTF8String: pobjCur->name]);
    if ([nodeName isEqualToString:@"TipoMime"])
    {
        self.MediaType = [self convertirStringToTypeMime:[NSString deserializeNode:pobjCur]];
    }
    else if ([nodeName isEqualToString:@"UrlImagenProducto"])
    {
        self.UrlPath = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"UrlImagenProductoPequena"])
    {
        self.UrlPathSmall = [NSString deserializeNode:pobjCur];
    }
}

- (Media *)Clonar
{
    Media *resul = [[Media alloc] init];
    
    resul.UrlPath = [NSString stringWithString:self.UrlPath];
    resul.MediaType = self.MediaType;
    
    return resul;
}

- (MediaTypeMIME)convertirStringToTypeMime:(NSString *)pTipo
{
    MediaTypeMIME resul = MediaTypeMIME_image;
    
    if ([pTipo isEqualToString:@"MP4"])
    {
        resul = MediaTypeMIME_video;
    }
    else if ([pTipo isEqualToString:@"JPG"])
    {
        resul = MediaTypeMIME_image;
    }
    else if ([pTipo isEqualToString:@"PNG"])
    {
        resul = MediaTypeMIME_image;
    }
    else
    {
        resul = MediaTypeMIME_indeterminate;
    }
    
    return resul;
}

- (BOOL)comprobarObjeto
{
    NSRange lRango = [self.UrlPath rangeOfString:@"="];
    
    return (lRango.length + lRango.location < self.UrlPath.length);
}

+ (BOOL)comprobarLista:(NSArray *)pLista
{
    BOOL resul = true;
    
    for (Media *lmedia in pLista)
    {
        resul = resul && [lmedia comprobarObjeto];
    }
    
    return resul;
}

- (void)setUrlPath:(NSString *)pUrlPath
{
    _UrlPath = pUrlPath;
    
    if (_UrlPathSmall.isNotEmpty == false)
    {
        _UrlPathSmall = pUrlPath;
    }
}

- (NSString *)UrlPath
{
    return _UrlPath;
}

- (void)setUrlPathSmall:(NSString *)UrlPathSmall
{
    _UrlPathSmall = UrlPathSmall;
    
    if (_UrlPath.isNotEmpty == false)
    {
        _UrlPath = UrlPathSmall;
    }
}

- (NSString *)UrlPathSmall
{
    return _UrlPathSmall;
}

@end
