//
//  NSURL+Ampliado.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, DownloadManagerTypeFile)
{
    DownloadManagerTypeFileImage     = 0,  // Imagen
    DownloadManagerTypeFileAudio     = 1,  // Audio
    DownloadManagerTypeFileOther     = -1, // otro tipo de fichero
};

@interface DownloadManager : NSObject 

typedef void (^getAssyncUrlFileWithUrlBlock)(NSURL *pResult);
typedef void (^getAssyncDataWithUrlBlock)(NSData *pResult);

- (void)RequestAssync:(NSURL *)pURL Type:(DownloadManagerTypeFile)pType Grupo:(NSString*)pGrupo ResultImage:(getAssyncUrlFileWithUrlBlock)pResultImage;

- (void)RequestAssync:(NSURL *)pURL Type:(DownloadManagerTypeFile)pType Grupo:(NSString*)pGrupo ResultData:(getAssyncDataWithUrlBlock)pResultData;

+(void)ClearAssyncFileCacheLastWeek;

-(void)CancelAllDownloads:(NSString*)pGrupo;

-(void)ClearCacheImages;

-(NSString*) getFileNameCacheUrl:(NSURL*)pURL Type:(DownloadManagerTypeFile)pTipe;

@property (assign) BOOL CanceledActualDonwload;

@end
