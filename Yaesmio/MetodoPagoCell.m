//
//  MetodoPagoCell.m
//  Yaesmio
//
//  Created by freelance on 09/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "MetodoPagoCell.h"

@interface MetodoPagoCell ()

@property (weak, nonatomic) IBOutlet UIImageView *ImagenTarjeta;
@property (weak, nonatomic) IBOutlet UILabel *labelTexto;
@property (weak, nonatomic) IBOutlet UILabel *labelModoTarjeta;

@end

@implementation MetodoPagoCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        [self Inicializar];
    }
    
    return self;
}

- (void)ConfigurarConTarjetaUsuario:(TarjetaUsuario *)tarjeta
{
    [self Inicializar];
    
    switch (tarjeta.TipoTarjeta)
    {
        case TipoTarjetaUsuarioNinguna:
            [self MostrarTarjeta:Traducir(@"Nueva tarjeta de credito.") Imagen:[UIImage imageNamed:@"tarjeta_generica"] ModoTarjeta:@""];
            break;
            
        case TipoTarjetaUsuarioOtra:
            [self MostrarTarjeta:[NSString stringWithFormat:@"%@\n%@", tarjeta.NumeroTarjeta, [self getStringModoTarjeta:tarjeta]] Imagen:[UIImage imageNamed:@"tarjeta_generica"] ModoTarjeta:[NSString stringWithFormat:@"%@/%@",tarjeta.Mes, tarjeta.Anio]];
            break;
        case TipoTarjetaUsuarioVisa:
             [self MostrarTarjeta:[NSString stringWithFormat:@"%@\n%@", tarjeta.NumeroTarjeta, [self getStringModoTarjeta:tarjeta]] Imagen:[UIImage imageNamed:@"tarjeta_visa"] ModoTarjeta:[NSString stringWithFormat:@"%@/%@",tarjeta.Mes, tarjeta.Anio]];
            break;
            
        case TipoTarjetaUsuarioMasterCard:
            [self MostrarTarjeta:[NSString stringWithFormat:@"%@\n%@", tarjeta.NumeroTarjeta, [self getStringModoTarjeta:tarjeta]] Imagen:[UIImage imageNamed:@"tarjeta_mastercard"] ModoTarjeta:[NSString stringWithFormat:@"%@/%@",tarjeta.Mes, tarjeta.Anio]];            break;
            
        case TipoTarjetaUsuarioPaypal:
            [self MostrarTarjeta:Traducir(@"Paypal") Imagen:[UIImage imageNamed:@"tarjeta_paypal"] ModoTarjeta:@""];
            break;
    }
}

- (void)Inicializar
{
    self.ImagenTarjeta.image = nil;
    self.labelTexto.text = @"";
    self.labelModoTarjeta.text = @"";
}

- (NSString*)getStringModoTarjeta:(TarjetaUsuario *)tarjeta
{
    switch (tarjeta.ModoTarjeta)
    {
        case ModoTarjetaUsuarioNinguna:
            return @"";
            
        case ModoTarjetaUsuarioDebito:
            return Traducir(@"Debito");
            
        case ModoTarjetaUsuarioCredito:
           return Traducir(@"Credito");
    }
    
    return @"";
}

-(void)MostrarTarjeta:(NSString*)Texto Imagen:(UIImage*)Imagen ModoTarjeta:(NSString*) ModoTarjeta
{
    self.labelTexto.text = Texto;
    self.imageView.image = Imagen;
    self.labelModoTarjeta.text = ModoTarjeta;
}

@end
