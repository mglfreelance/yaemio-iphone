//
//  ColorManager.m
//  yaesmio
//
//  Created by manuel garcia lopez on 06/05/13.
//  Copyright (c) 2013 manuel garcia lopez. All rights reserved.
//

#import "ColorManager.h"
#import "UIColor+MKHelpers.h"

@implementation ColorManager


#define colorPrincipalApp @"#f20c75"
#define colorPrincipalOscuro @"#b72b65"
#define colorInicioDegradadoFodo @"#f20c75"
#define colorFinDegradadoFodo @"#8a2553"
#define colorInicioDegradadoBoton @"#f70b77"
#define colorMedioDegradadoBoton @"#bd1864"
#define colorFinDegradadoBoton @"#8a2453"
#define colorFinDegradadoClaroBoton @"#892553"
#define colorGrisFondo @"#e0e0e0"
#define colorInicioAlertView @"#aa115e"
#define colorMedioAlertView @"#ab105e"
#define colorFinAlertView @"#ab105f"
#define colorInicioBarraDescripcionProducto @"#c9c9c9"
#define colorFinBarraDescripcionProducto @"#585858"
#define colorInicioFondoCompartir @"#8a2551"
#define colorFinFondoCompartir @"#8b2354"
#define colorGrisClaro @"#f4f4f4"
#define colorPrincipalDesactivado @"#925e72"
#define colorVerde @"#476821"
#define colorAzul @"#2a3c6f"
#define colorTexto @"#363636"
#define colorRojo @"#d81e00"
#define colorInicioDegradadoBotonDesactivado @"#dfdfdf"
#define colorMedioDegradadoBotonDesactivado @"#b0acab"
#define colorFinDegradadoBotonDesactivado @"#8d8d8d" 
#define colorFondoCarrito @"#e6e6e6"
#define colorGrisOscuro @"#464646"


- (CAGradientLayer*) getLayerColorPrincipal:(CGRect) pFrame
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = pFrame;
    UIColor *startColour =[UIColor colorWithRed:242 green: 12 blue:117 alpha:0];
    UIColor *endColour =[UIColor colorWithRed:138 green:37 blue:83 alpha:0] ;
    
    gradient.colors = @[(id)[startColour CGColor], (id)[endColour CGColor]];
    
    return gradient;
}

-(UIColor *) Principal
{
    return [UIColor colorWithHexString:colorPrincipalApp];
}

-(UIColor *) PrincipalOscuro
{
    return [UIColor colorWithHexString:colorPrincipalOscuro];
}

-(UIColor*)PrincipalTransparente
{
    return [UIColor colorWithHexString:colorPrincipalApp Alpha:0.96f];
}

-(NSString *) PrincipalHexadecimal
{
    return colorPrincipalApp;
}

-(NSArray*) DegradadoFondo
{
    return @[[UIColor colorWithHexString:colorInicioDegradadoFodo], [UIColor colorWithHexString:colorFinDegradadoFodo]];
}

-(NSArray*) DegradadoFondoCompartir
{
    return @[[UIColor colorWithHexString:colorInicioFondoCompartir], [UIColor colorWithHexString:colorFinFondoCompartir]];
}

-(NSArray*) DegradadoBoton
{
    return @[self.Principal];
}

-(NSArray*) DegradadoBotonDesactivado
{
    return @[[UIColor colorWithHexString:colorMedioDegradadoBotonDesactivado]];
}

-(NSArray*) DegradadoAlertView
{
    return @[[UIColor colorWithHexString:colorPrincipalApp],[UIColor colorWithHexString:colorInicioAlertView], [UIColor colorWithHexString:colorPrincipalApp]];
}

-(NSArray*) DegradadoClaroBoton
{
    return @[[UIColor colorWithHexString:colorInicioDegradadoBoton], [UIColor colorWithHexString:colorFinDegradadoBoton]];
}

-(UIColor*) FondoHUB
{
    return [UIColor colorWithIntRed:242 green:12 blue:117 alpha:0.8];
}

-(NSArray*) DegradadoBarra
{
    return @[[self InicioBarra],[self FinBarra]];
}

-(NSArray*) DegradadoDescripcionProducto
{
    return @[[UIColor colorWithHexString:colorInicioBarraDescripcionProducto],[UIColor colorWithHexString:colorFinBarraDescripcionProducto]];
}

-(UIColor*) InicioBarra
{
    return [UIColor colorWithHexString:colorInicioDegradadoFodo];
}

-(UIColor*) FinBarra
{
    return [UIColor colorWithHexString:colorFinDegradadoFodo];
}

-(UIColor*) Gris
{
    return [UIColor colorWithHexString:colorGrisFondo];
}

-(NSString*) GrisHexadecimal
{
    return colorGrisFondo;
}

-(UIColor*) GrisClaro
{
    return [UIColor colorWithHexString:colorGrisClaro];
}

-(UIColor*) GrisOscuro
{
    return [UIColor colorWithHexString:colorGrisOscuro];
}

-(UIColor*) PrincipalDesactivado
{
    return [UIColor colorWithHexString:colorPrincipalDesactivado];
}

-(UIColor*) SombraTextoAlertView
{
    return [UIColor blackColor];
}

-(UIColor*) Blanco
{
    return [UIColor whiteColor];
}

-(UIColor*) Negro
{
    return [UIColor blackColor];
}

-(UIColor*)Transparente
{
    return [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.0];
}
    
-(UIColor*) Azul
{
    return [UIColor colorWithHexString:colorAzul];
}

-(UIColor*) Clear
{
    return [UIColor clearColor];
}

-(UIColor*) Rojo
{
    return [UIColor colorWithHexString:colorRojo];
}

-(NSString*) RojoHexadecimal
{
    return colorRojo;
}

-(UIColor*) Verde
{
    return [UIColor colorWithHexString:colorVerde];
}

-(UIColor*) Texto
{
    return [UIColor colorWithHexString:colorTexto];
}

-(NSString*) TextoHexadecimal
{
    return colorTexto;
}

-(UIImage *) ImageBarra
{
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0, 0, 320, 44);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor blackColor] CGColor], (id)[[UIColor whiteColor] CGColor], nil];
    
    UIGraphicsBeginImageContextWithOptions([gradient frame].size, YES, 0.0);
    [gradient renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return outputImage;
}


@end
