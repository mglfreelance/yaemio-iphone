//
//  SitioYaesmio.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 12/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SitioYaesmio.h"

@implementation SitioYaesmio

- (id)init
{
    if ((self = [super init]))
    {
        self.Cabecera           = @"";
        self.Texto              = @"";
        self.Imagen             = [Media createMediaImage];
        self.CodigoTextoHTML    = @"";
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Cabecera xmlNodeForDoc:node->doc elementName:@"Cabecera" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Texto xmlNodeForDoc:node->doc elementName:@"Texto" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Imagen.UrlPath xmlNodeForDoc:node->doc elementName:@"UrlImagen" elementNSPrefix:@"tns3"]);
}

+ (SitioYaesmio *)deserializeNode:(xmlNodePtr)cur
{
    SitioYaesmio *newObject = [SitioYaesmio new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    // NSLog(@"campo: %@",[NSString stringWithUTF8String: pobjCur->name]);
    
    if ([nodeName isEqualToString:@"Cabecera"])
    {
        self.Cabecera = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Texto"])
    {
        self.Texto = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"UrlImagen"])
    {
        self.Imagen.UrlPath = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ImagenPequena"])
    {
        self.Imagen.UrlPathSmall = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ClaveTexto"])
    {
        self.CodigoTextoHTML = [NSString deserializeNode:pobjCur];
    }
}

@end
