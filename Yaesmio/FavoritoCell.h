//
//  FavoritoCell.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 19/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Favorito.h"

@interface FavoritoCell : UICollectionViewCell

- (void)ConfigurarConFavorito:(Favorito *)pFavorito MostrarImagenGrande:(BOOL)pMostrarImagenGrande Menu:(id)Menu;

@end
