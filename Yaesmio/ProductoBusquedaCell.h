//
//  ProductoBusquedaCell.h
//  Yaesmio
//
//  Created by freelance on 09/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductoBuscado.h"

@interface ProductoBusquedaCell : UICollectionViewCell

- (void)ActualizarConProducto:(ProductoBuscado *)pProducto MostrarImagenGrande:(BOOL)pImagenGrande MostrarDistancia:(BOOL)MostrarDistancia;

@end
