//
//  FiltroBusquedaProductos.m
//  Yaesmio
//
//  Created by freelance on 09/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "FiltroBusquedaProductos.h"

@interface FiltroBusquedaProductos ()

@property (nonatomic) int PosicionBusqueda;
@property (nonatomic) int NumeroElementos;

@end

@implementation FiltroBusquedaProductos

@synthesize Pais = myPais;
@synthesize Provincia = myProvicia;
@synthesize CampoOrdenacion = myCampoOrdenacion;

- (id)init
{
    if ((self = [super init]))
    {
        self.Empresa = nil;
        self.GrupoArticulo = nil;
        self.Ordenacion = OrdenacionBusquedaNinguno;
        self.CampoOrdenacion = CampoOrdenacionBusquedaEstandar;
        self.Texto = nil;
        self.TipoBusqueda = TipoProductoBusquedaProducto;
        self.PosicionBusqueda = 0;
        self.NumeroElementos = 0;
        self.MostrarSoloRecomendadas = false;
        self.Latitud = 0.0;
        self.Longitud = 0.0;
    }
    
    return self;
}

- (id)initWith:(InitFiltroBusquedaProductos)pInitialize
{
    if ((self = [self init]))
    {
        pInitialize(self);
    }
    
    return self;
}

- (instancetype)Clone
{
    return [[FiltroBusquedaProductos alloc] initWith:^(FiltroBusquedaProductos *pDato)
            {
                pDato.Provincia = self.Provincia;
                pDato.Empresa = self.Empresa;
                pDato.GrupoArticulo = self.GrupoArticulo;
                pDato.Pais = self.Pais;
                pDato.Ordenacion = self.Ordenacion;
                pDato.CampoOrdenacion = self.CampoOrdenacion;
                pDato.Texto = self.Texto;
                pDato.TipoBusqueda = self.TipoBusqueda;
                pDato.PosicionBusqueda = self.PosicionBusqueda;
                pDato.NumeroElementos = self.NumeroElementos;
                pDato.MostrarSoloRecomendadas = self.MostrarSoloRecomendadas;
                pDato.Latitud = self.Latitud;
                pDato.Longitud = self.Longitud;
            }];
}

- (void)AsignarPosicion:(int)pPosicion YNumeroElementos:(int)pNumeroElementos
{
    self.PosicionBusqueda = pPosicion;
    self.NumeroElementos  = pNumeroElementos;
}

+ (FiltroBusquedaProductos *)deserializeNode:(xmlNodePtr)cur
{
    FiltroBusquedaProductos *newObject = [[FiltroBusquedaProductos alloc] init];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark- SoapObject

- (void)addElementsToNode:(xmlNodePtr)node
{
    if (self.Provincia != nil)
    {
        xmlAddChild(node, [self.Provincia.Codigo xmlNodeForDoc:node->doc elementName:@"IDProvincia" elementNSPrefix:@"yaes"]);
    }
    
    if (self.Empresa != nil)
    {
        xmlAddChild(node, [self.Empresa.Codigo xmlNodeForDoc:node->doc elementName:@"IdEmpresa" elementNSPrefix:@"yaes"]);
    }
    
    if (self.GrupoArticulo != nil)
    {
        xmlAddChild(node, [self.GrupoArticulo.Codigo xmlNodeForDoc:node->doc elementName:@"IdGrupoArticulo" elementNSPrefix:@"yaes"]);
    }
    
    xmlAddChild(node, [self.Pais.Codigo xmlNodeForDoc:node->doc elementName:@"IdPais" elementNSPrefix:@"yaes"]);
    
    xmlAddChild(node, [[self convertirOrdenacionAString] xmlNodeForDoc:node->doc elementName:@"ModoOrdenar" elementNSPrefix:@"yaes"]);
    
    xmlAddChild(node, [[NSString stringWithFormat:@"%d", self.NumeroElementos] xmlNodeForDoc:node->doc elementName:@"NumTotal" elementNSPrefix:@"yaes"]);
    
    xmlAddChild(node, [self.CampoOrdenacion == CampoOrdenacionBusquedaPrecio ? @"X":@"" xmlNodeForDoc:node->doc elementName:@"OrdenarPorPrecio" elementNSPrefix:@"yaes"]);
    
    xmlAddChild(node, [[NSString stringWithFormat:@"%d", self.PosicionBusqueda] xmlNodeForDoc:node->doc elementName:@"PosIni" elementNSPrefix:@"yaes"]);
    
    if (self.MostrarSoloRecomendadas)
    {
        xmlAddChild(node, [self.MostrarSoloRecomendadas ? @"X":@"" xmlNodeForDoc:node->doc elementName:@"SoloRecomendadas" elementNSPrefix:@"yaes"]);
    }
    
    xmlAddChild(node, [self.CampoOrdenacion == CampoOrdenacionBusquedaDescuento ? @"X":@"" xmlNodeForDoc:node->doc elementName:@"SortDesc" elementNSPrefix:@"yaes"]);
    
    xmlAddChild(node, [self.CampoOrdenacion == CampoOrdenacionBusquedaValoracion ? @"X":@"" xmlNodeForDoc:node->doc elementName:@"SortVal" elementNSPrefix:@"yaes"]);
    
    xmlAddChild(node, [self.Texto xmlNodeForDoc:node->doc elementName:@"Texto" elementNSPrefix:@"yaes"]);
    
    xmlAddChild(node, [[self convertirTipoProductoAString] xmlNodeForDoc:node->doc elementName:@"TipoProducto" elementNSPrefix:@"yaes"]);
}

- (BOOL)ExisteFiltro
{
    return (self.Pais != nil || self.Texto.isNotEmpty || self.Empresa != nil || self.GrupoArticulo != nil || (self.TipoBusqueda == TipoProductoBusquedaCupon && self.Provincia != nil));
}

- (NSString *)convertirTipoProductoAString
{
    switch (self.TipoBusqueda)
    {
        case TipoProductoBusquedaProducto :
            return @"P";
            
        case TipoProductoBusquedaCupon :
            return @"C";
            
        case TipoProductoBusquedaPublicitario :
            return @"X";
            
        default :
            return @"P";
    }
}

- (NSString *)convertirOrdenacionAString
{
    switch (self.Ordenacion)
    {
        case OrdenacionBusquedaAscendente:
            return @"A";
            
        case OrdenacionBusquedaDescendente:
            return @"D";
            
        default:
            return @"";
    }
}

- (Pais *)Pais
{
    return myPais;
}

- (void)setPais:(Pais *)Pais
{
    if ([myPais isEqual:Pais] == false)
    {
        myPais = Pais;
        
        if (self.TipoBusqueda == TipoProductoBusquedaCupon && self.Provincia.esCercaDeMi == false)
        {
            self.Provincia = [Provincia Todas];
        }
    }
}

- (Provincia *)Provincia
{
    return myProvicia;
}

- (void)setProvincia:(Provincia *)Provincia
{
    if ([myProvicia isEqual:Provincia] == false)
    {
        myProvicia = Provincia;
        
        if (myProvicia.esCercaDeMi)
        {
            self.CampoOrdenacion = CampoOrdenacionBusquedaCercana;
        }
        else if (myProvicia.esTodas)
        {
            self.CampoOrdenacion = CampoOrdenacionBusquedaEstandar;
        }
        else if (self.CampoOrdenacion == CampoOrdenacionBusquedaCercana)
        {
            self.CampoOrdenacion = CampoOrdenacionBusquedaEstandar;
        }
    }
}

- (CampoOrdenacionBusqueda)CampoOrdenacion
{
    return myCampoOrdenacion;
}

- (void)setCampoOrdenacion:(CampoOrdenacionBusqueda)CampoOrdenacion
{
    if (myCampoOrdenacion != CampoOrdenacion)
    {
        myCampoOrdenacion = CampoOrdenacion;
        
        if (self.TipoBusqueda == TipoProductoBusquedaCupon)
        {
            if (myCampoOrdenacion == CampoOrdenacionBusquedaCercana)
            {
                self.Provincia = [Provincia CercaDeMi];
                self.Ordenacion = OrdenacionBusquedaNinguno;
            }
            else if (myProvicia.esCercaDeMi)
            {
                self.Provincia = [Provincia Todas];
                self.Ordenacion = OrdenacionBusquedaNinguno;
            }
           
        }
    }
}

@end
