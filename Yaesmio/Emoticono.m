//
//  Emoticono.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "Emoticono.h"

@implementation Emoticono

+ (Emoticono *)getEmoticonoArray:(NSArray *)pArray withID:(int)pId
{
    Emoticono *resul = nil;
    
    if (pId < 1)
    {
        pId = 1;
    }
    
    if (pId > pArray.count)
    {
        pId = (int)pArray.count;
    }
    
    for (Emoticono *lemoticono in pArray)
    {
        if (lemoticono != nil && lemoticono.IdEmoticono == pId)
        {
            resul = lemoticono;
            break;             // salida rapida
        }
    }
    
    return resul;
}

- (id)init
{
    if ((self = [super init]))
    {
        self.IdEmoticono = 0;
        self.Imagen      = [Media createMediaImage];
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [[NSString stringWithFormat:@"%d", self.IdEmoticono] xmlNodeForDoc:node->doc elementName:@"Codigo" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Imagen.UrlPath xmlNodeForDoc:node->doc elementName:@"UrlImagen" elementNSPrefix:@"tns3"]);
}

+ (Emoticono *)deserializeNode:(xmlNodePtr)cur
{
    Emoticono *newObject = [Emoticono new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    // NSLog(@"campo: %@",[NSString stringWithUTF8String: pobjCur->name]);
    
    if ([nodeName isEqualToString:@"Codigo"])
    {
        self.IdEmoticono = [[NSString deserializeNode:pobjCur] intValue];
    }
    else if ([nodeName isEqualToString:@"UrlImagen"])
    {
        self.Imagen.UrlPath = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ImagenPequena"])
    {
        self.Imagen.UrlPathSmall = [NSString deserializeNode:pobjCur];
    }
}

@end
