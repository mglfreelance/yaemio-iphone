//
//  Contacto.h
//  Yaesmio
//
//  Created by freelance on 21/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"
#import "RHPerson.h"

@interface Contacto : SoapObject

+ (Contacto *)	deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString *MailUsuario;
@property (nonatomic, strong) NSString *UsuarioId;
@property (nonatomic, strong) NSString *Apellidos;
@property (nonatomic, strong) NSString *Nombre;
@property (nonatomic, strong) NSString *Telefono;
@property (nonatomic, strong) RHPerson *ContactoTelefono;


@property (readonly) UIImage *ImagenAMostrar;
@property (readonly) NSString *NombreAMostrar;
@property (readonly) NSString *mailPrincipal;
@property (readonly) NSString *TelefonoPrincipal;


- (instancetype)initWithPerson:(RHPerson *)Person;
- (instancetype)initWithPhone:(NSString *)phone;
- (instancetype)initWithMail:(NSString *)mail;


- (BOOL)		ContieneMailDelista:(NSArray *)pListaMail2;
- (void)		copiarDatosEn:(Contacto *)pContacto;

- (Contacto *)	BuscarEnLista:(NSArray *)pLista;
@end
