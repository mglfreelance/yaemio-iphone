//
//  UIImageView+Ampliado.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 28/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (Ampliado)

- (void)cargarUrlImagenIzquierdaAsincrono:(NSString *)pUrl Grupo:(NSString *)pGrupo;

- (void)cargarUrlImagenDerechaAsincrono:(NSString *)pUrl Grupo:(NSString *)pGrupo;

- (void)cargarUrlImagenAsincrono:(NSString *)pUrl Grupo:(NSString *)pGrupo Busy:(UIActivityIndicatorView *)pControlBusy Result:(ResultadoBlock)pResultado;

- (void)cargarUrlImagenAsincrono:(NSString *)pUrl Busy:(UIActivityIndicatorView *)pControlBusy;

@end
