//
//  SitioYaesmioCell.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 12/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SitioYaesmioCell.h"
#import "UIAllControls.h"

@interface SitioYaesmioCell ()

@property (nonatomic) IBOutlet UIImageView *controlImagen;
@property (nonatomic) IBOutlet UILabel *labelTituloSitio;
@property (nonatomic) IBOutlet UILabel *labelDescripcionSitio;
@property (nonatomic) IBOutlet UIActivityIndicatorView *controlBusy;


@property (nonatomic) SitioYaesmio *mySitio;

@end

@implementation SitioYaesmioCell

#define GRUPO_SITIOYAESMIOCELL @"SitioYaesmioCell"

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        [self inicializar];
    }
    
    return self;
}

- (void)inicializar
{
    self.labelDescripcionSitio.text  = @"";
    self.labelTituloSitio.text       = @"";
    self.controlImagen.image         = nil;
}

- (void)ConfigurarConSitio:(SitioYaesmio *)pSitio
{
    if (self.mySitio == nil || self.mySitio != pSitio)
    {
        [self inicializar];
        
        if ([pSitio.CodigoTextoHTML isEqualToString:@""])
        {
            self.accessoryType = UITableViewCellAccessoryNone;
        }
        else
        {
            self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        self.labelTituloSitio.text = pSitio.Cabecera;
        [self.labelTituloSitio mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT];
        self.labelDescripcionSitio.text = pSitio.Texto;
        [self.labelDescripcionSitio mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT];
        [self.controlImagen cargarUrlImagenAsincrono:pSitio.Imagen.UrlPathSmall Busy:self.controlBusy];
        [self.labelDescripcionSitio mgMoveToControl:self.labelTituloSitio Position:MGPositionMoveToBelow Separation:5];
        // queda alienear la imagen pero alejandro dice que asi esta bien.
    }
}

- (float)obtenerPreviewHeightCelda:(SitioYaesmio *)pSitio
{
    [self ConfigurarConSitio:pSitio];
    
    return self.labelDescripcionSitio.frame.origin.y + self.labelDescripcionSitio.frame.size.height + 10;
}

@end
