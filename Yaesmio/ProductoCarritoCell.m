//
//  ProductoCarritoCell.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ProductoCarritoCell.h"
#import "UIAllControls.h"

@interface ProductoCarritoCell ()

@property (nonatomic) IBOutlet UILabel *labelPrecio;
@property (nonatomic) IBOutlet UILabel *labelDescripcionProducto;
@property (nonatomic) IBOutlet UILabel *labelNombreProducto;
@property (weak, nonatomic) IBOutlet UIImageView *imagenProducto;
@property (weak, nonatomic) IBOutlet OBGradientView *vistaFondoGradiente;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorbusyImagen;
@property (weak, nonatomic) IBOutlet OBGradientView *vistaImagen;
@property (weak, nonatomic) IBOutlet UILabel *labelUnidades;
@property (weak, nonatomic) IBOutlet UIStepper *StepperUnidades;
@property (weak, nonatomic) IBOutlet UIView *imagenMenu;

@property (strong) ElementoCarrito *Elemento;
@property (nonatomic) UIViewController *myView;


- (IBAction)StepperChangeValue:(id)sender;

@end

@implementation ProductoCarritoCell

#define GRUPO_PRODUCTOCARRITOCELL @"ProductoCarritoCell"

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        [self inicializar];
    }
    
    return self;
}

- (void)inicializar
{
    self.labelDescripcionProducto.text = @"";
    self.labelNombreProducto.text = @"";
    self.labelPrecio.text = @"";
    self.imagenProducto.image = nil;
    self.labelUnidades.text = @"";
    self.StepperUnidades.value = 0;
    self.vistaFondoGradiente.BorderColor = [Locator Default].Color.Negro;
    self.vistaFondoGradiente.WidthBorder = 1.0;
    self.vistaImagen.BorderColor = [Locator Default].Color.Negro;
    self.vistaImagen.WidthBorder = 1.0;
}

#pragma mark- Publico

-(void)Configurar:(UIViewController*)pView Elemento:(ElementoCarrito*)pElemento menu:(M13ContextMenu*)Menu
{
    [self inicializar];
    self.Elemento = pElemento;
    self.myView = pView;
    
    NSDecimalNumber *lNumero = [pElemento.Producto.ProductoHijo.Precio decimalNumberByMultiplyingBy:pElemento.Unidades];
    
    self.StepperUnidades.value = pElemento.Unidades.longValue;
    self.StepperUnidades.stepValue = 1.0;
    self.StepperUnidades.maximumValue = pElemento.numeroUnidadesMaximoCompra;
    self.StepperUnidades.layer.cornerRadius = 4.0;
    
    self.labelDescripcionProducto.text = pElemento.Producto.TextoPropiedades;
    self.labelNombreProducto.text = pElemento.Producto.ProductoHijo.Nombre;
    self.labelPrecio.text = [NSString stringWithFormat:@"%@ %@",  [lNumero toStringFormaterCurrency], pElemento.Producto.ProductoHijo.Moneda];
    
    [self.imagenProducto cargarUrlImagenAsincrono:pElemento.Producto.ProductoHijo.ImagenPorDefecto.UrlPathSmall Busy:self.indicatorbusyImagen];
    [self.imagenProducto mgAddGestureRecognizerTap:self Action:@selector(MostrarImagenProducto)];
    [self ActualizarUnidades:pElemento];
    
    [self.imagenMenu mgRemoveAllGestureRecognizers];
    [self.imagenMenu mgAddGestureRecognizerTap:Menu Action:@selector(showMenuUponActivationOfGetsure:)];
    
    [self.vistaFondoGradiente mgRemoveAllGestureRecognizers];
    [self.vistaFondoGradiente mgAddGestureRecognizerLongPress:Menu Action:@selector(showMenuUponActivationOfGetsure:)];
}

#pragma  mark- Privado

- (void)MostrarImagenProducto
{
    [[Locator Default].Vistas MostrarGaleriaImagenes:self.myView ListaImagenes:self.Elemento.Producto.ProductoHijo.ListaMedia UrlImagenEmpresa:self.Elemento.Producto.LogoEmpresa.UrlPathSmall];
}

- (IBAction)StepperChangeValue:(id)sender
{
    self.Elemento.Unidades = [NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%f", self.StepperUnidades.value]];
    [self ActualizarUnidades:self.Elemento];
    
    if (self.DelegateCambiadaInformacionCelda != nil)
    {
        self.DelegateCambiadaInformacionCelda(self.Elemento);
    }
}

- (void)ActualizarUnidades:(ElementoCarrito *)pElemento
{
    self.labelUnidades.text = [NSString stringWithFormat:@"%@ %d", pElemento.Producto.ProductoHijo.DescUnidadesMedida, self.Elemento.Unidades.intValue];
}

@end
