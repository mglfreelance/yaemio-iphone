//
//  CrearDireccionEnvioViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Direccion.h"
#import "YaesmioViewControllers.h"

typedef NS_ENUM (NSInteger, DireccionEnvioModo)
{
    DireccionEnvioModoPrincipal   = 1, // Editar o crear dir Principal
    DireccionEnvioModoAlternativa = 2, // editar o crear dir Alternativa
    DireccionEnvioModoSoloEnvio   = 3, // devolver dir solo envio
};

typedef void (^MantenimientoDireccionEnvioBlock)(Direccion *pResult);

@interface MantenimientoDireccionEnvioViewController : YaesmioViewController

@property (strong) Direccion *Direccion;
@property (assign) DireccionEnvioModo TipoDireccion;
@property (copy)   MantenimientoDireccionEnvioBlock Resultado;
@property (assign) BOOL EsVistaCarrito;

@end
