//
//  MediaProductoCell.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import "MediaProductoCell.h"
#import "UIAllControls.h"

@interface MediaProductoCell ()

@property (nonatomic) IBOutlet UIImageView              *controlImagenProducto;
@property (nonatomic) IBOutlet UIActivityIndicatorView  *controlBusy;
@property (nonatomic) IBOutlet UIView                   *ViewVideo;
@property (nonatomic) IBOutlet UIView                   *viewControles;
@property (nonatomic) IBOutlet UIView                   *ViewTouch;
@property (strong)    Media                             *media;


- (IBAction)btnPlayPause_click:(id)sender;
- (IBAction)btnPantallaCompleta_click:(id)sender;

@property (strong, readonly) MPMoviePlayerController *VideoPlayer;
@end

@implementation MediaProductoCell

@synthesize VideoPlayer = _mPlayer;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self inicializar];
    }
    return self;
}

-(void)inicializar
{
    if (self.controlImagenProducto != nil)
    {
        self.controlImagenProducto.image  = nil;
    }
    if (_mPlayer != nil)
    {
        [self StopVideo];
        [_mPlayer.view removeFromSuperview];
        _mPlayer = nil;
    }
    self.viewControles.hidden = true;
    self.controlBusy.hidden = false;
    self.controlBusy.hidesWhenStopped = true;
    [self.controlBusy startAnimating];
}

+(NSString*) getReuseIdentifier:(Media*)pMedia
{
    return pMedia.MediaType == MediaTypeMIME_image? [self GrupoImage] : [self GrupoVideo];
}

+(NSString*)GrupoImage
{
    return @"Cell_Image";
}

+(NSString*)GrupoVideo
{
    return @"Cell_Video";
}

-(void)unloadAllControls
{
    if (self.media.MediaType == MediaTypeMIME_image)
    {
        self.controlImagenProducto.image = nil;
    }
    if (_mPlayer != nil)
    {
        [self StopVideo];
        [_mPlayer.view removeFromSuperview];
        _mPlayer = nil;
    }
    mFinalizeVideoRespon = nil;
    mTapGestureRespon = nil;
    [self.ViewTouch mgAddGestureRecognizerTap:nil Action:nil];
    [self removeNotificacionFinalizeVideo];
    [self removeNotificacionPlayVideo];
    [self setControlImagenProducto:nil];
    [self setControlBusy:nil];
    [self setViewVideo:nil];
    [self setViewControles:nil];
    [self setViewTouch:nil];
    [self setMedia:nil];
}

-(void)ConfigurarVideo:(Media*)pMedia ResultTap:(TapMediaCellResponseBlock)pResultTap ResultFinishPlayback:(ResultadoBlock)pResultFinish
{
    self.media = pMedia;
    [self inicializar];
    [self setTapResponse:pResultTap];
    [self setFinalizeVideoResponse:pResultFinish];
    [self.controlBusy startAnimating];
    
    self.VideoPlayer.contentURL = [NSURL URLWithString:pMedia.UrlPath];
    [self PlayVideo:[self getTimerActualVideo]];
    [self.ViewTouch mgAddGestureRecognizerTap:self Action:@selector(MostrarOcultarVistacontroles)];
}

-(void)ConfigurarImagen:(Media*)pMedia
{
    self.media = pMedia;
    [self inicializar];
    
    if (self.controlImagenProducto != nil)
    {
        [self.controlImagenProducto cargarUrlImagenAsincrono:self.media.UrlPathSmall Busy:self.controlBusy];
    }
}

-(void)PauseVideo
{
    if (self.media.MediaType == MediaTypeMIME_video)
    {
        if (self.VideoPlayer.playbackState == MPMoviePlaybackStatePlaying)
        {
            [self.VideoPlayer pause];
        }
    }
}

-(void)StopVideo
{
    if (self.media.MediaType == MediaTypeMIME_video && _mPlayer != nil)
    {
        if (self.VideoPlayer.playbackState == MPMoviePlaybackStatePlaying)
        {
            [self.VideoPlayer stop];
        }
        [self.VideoPlayer setContentURL:nil];
    }
}

-(NSTimeInterval) getTimerActualVideo
{
    NSTimeInterval resul;
    
    if (self.media != nil && self.media.MediaType == MediaTypeMIME_video)
    {
        resul = self.VideoPlayer.currentPlaybackTime;
    }
    else
    {
        resul = 0;
    }
    
    return resul;
}

-(void)PlayVideo:(NSTimeInterval) pTimer
{
    if (self.media.MediaType == MediaTypeMIME_video)
    {
        if (self.VideoPlayer.contentURL == nil)
        {
            self.VideoPlayer.contentURL = [NSURL URLWithString:self.media.UrlPath];
            self.VideoPlayer.initialPlaybackTime = pTimer;
            [self agregateNotificationsVideoPlayer];
            [self.VideoPlayer play];
        }
        else if (self.VideoPlayer.playbackState != MPMoviePlaybackStatePlaying)
        {
            [self agregateNotificationsVideoPlayer];
            self.VideoPlayer.initialPlaybackTime = pTimer;
            [self.VideoPlayer play];
        }
        else
        {
            [self.VideoPlayer setCurrentPlaybackTime:pTimer];
        }
    }
}

TapMediaCellResponseBlock mTapGestureRespon;
-(void) setTapResponse:(TapMediaCellResponseBlock)pResult
{
    mTapGestureRespon = pResult;
}

ResultadoBlock mFinalizeVideoRespon;
-(void) setFinalizeVideoResponse:(ResultadoBlock)pResult
{
    mFinalizeVideoRespon = pResult;
}

-(void)MostrarOcultarVistacontroles
{
    self.viewControles.hidden = ! self.viewControles.hidden;
    
    if (self.viewControles.hidden == false)
    {   // si esta mostrado ocultarse a los 3 segundos
        [self performSelector:@selector(MostrarOcultarVistacontroles) withObject:nil afterDelay:3];
    }
}

- (IBAction)btnPlayPause_click:(id)sender
{
    if (self.VideoPlayer.playbackState == MPMoviePlaybackStatePlaying)
    {
        [self PauseVideo];
    }
    else
    {
        [self PlayVideo:[self getTimerActualVideo]];
    }
}

- (IBAction)btnPantallaCompleta_click:(id)sender
{
    if (mTapGestureRespon)
    {
        mTapGestureRespon(self.media, [self getTimerActualVideo]);
    }
}

-(MPMoviePlayerController*) VideoPlayer
{
    if (_mPlayer == nil)
    {
        _mPlayer = [[MPMoviePlayerController alloc] init];
        [_mPlayer.view setFrame:CGRectMake (0, 0, self.ViewVideo.frame.size.width, self.ViewVideo.frame.size.height)];
        _mPlayer.backgroundView.backgroundColor = [Locator Default].Color.Blanco;
        _mPlayer.controlStyle = MPMovieControlStyleNone;
        _mPlayer.view.hidden = true;
        [self.ViewVideo addSubview:_mPlayer.view];
    }
    return _mPlayer;
}

#pragma mark - Nofificaciones VideoPlayer

-(void) agregateNotificationsVideoPlayer
{
    NSNotificationCenter *notification = [NSNotificationCenter defaultCenter];
    [notification addObserver:self selector:@selector(SeReproduceVideo) name:MPMoviePlayerNowPlayingMovieDidChangeNotification object:nil];
    [notification addObserver:self selector:@selector(FinalizaReproduccionVideo) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
}

- (void)removeNotificacionPlayVideo
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerNowPlayingMovieDidChangeNotification object:nil];
}

-(void) SeReproduceVideo
{
    _mPlayer.view.hidden = false;
    [self.controlBusy stopAnimating];
    [self removeNotificacionPlayVideo];
}

- (void)removeNotificacionFinalizeVideo
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
}

-(void) FinalizaReproduccionVideo
{
    if (mFinalizeVideoRespon != nil)
    {
        mFinalizeVideoRespon();
    }
    [self removeNotificacionFinalizeVideo];
}



@end
