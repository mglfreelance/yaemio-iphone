//
//  SeleccionContactosViewController.m
//  Yaesmio
//
//  Created by freelance on 21/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SeleccionContactosViewController.h"
#import "RHPerson.h"
#import "UIAllControls.h"
#import "ContactoTelefonoCell.h"
#import "Contacto.h"

@interface SeleccionContactosViewController () <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic)IBOutletCollection(UILabel) NSArray * ListaCampos;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong) NSMutableArray *ListaContactosTelefono;
@property (strong) NSMutableArray *ListaContactosYaesmio;
@property (strong) NSMutableArray *ListaGruposYaesmio;
@property (strong) NSMutableArray *ListaContactosSeleccionados;

- (IBAction)GuardarSeleccionContactos:(id)sender;
- (IBAction)CancelarSeleccionContactos:(id)sender;

@end

@implementation SeleccionContactosViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.ListaContactosTelefono = nil;
    self.ListaContactosYaesmio = nil;
    self.ListaContactosSeleccionados = nil;
    self.ListaGruposYaesmio = nil;
    
    Traducir(self.ListaCampos);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.ListaContactosTelefono == nil)                 //obtengo la lista de contactos
    {
        self.ListaContactosTelefono = [self obtenerContactosTelefono];
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
    if (self.ListaContactosSeleccionados == nil)            //busco los seleccionados
    {
        self.ListaContactosSeleccionados = [NSMutableArray new];
        
        [self ActualizarContactosSeleccionados];
    }
    
    if (self.MostrarGrupos && self.ListaGruposYaesmio == nil)
    {
        [[Locator Default].ServicioSOAP ObtenerListaGruposNotificacion: ^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *Lista)
         {
             if (pResult == SRResultMethodYES)
             {
                 self.ListaGruposYaesmio = [NSMutableArray arrayWithArray:Lista];
                 [self ActualizarContactosSeleccionados];
                 [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationAutomatic];
             }
         }];
    }
    
    if (self.ListaContactosYaesmio == nil)               //con la lista de contactos busco si son yasmiers
    {
        [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo yasmiers.")];
        [[Locator Default].ServicioSOAP ComprobarListaContactos:[self obtenerListaContactosParaComprobacion] DeUsuario:[Locator Default].Preferencias.IdUsuario Result: ^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *pListaContactos)
         {
             switch (pResult)
             {
                 case SRResultMethodYES:
                     [self actualizarContactosConYaesmio:pListaContactos];
                     [[Locator Default].Alerta hideBussy];
                     break;
                     
                 case SRResultMethodNO:
                     [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                     break;
                     
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
             }
         }];
    }
}

- (void)viewUnload
{
    [self setListaEmailsSeleccionados:nil];
    [self setListaCampos:nil];
    [self setListaContactosYaesmio:nil];
    [self setListaContactosTelefono:nil];
    [self setRespond:nil];
    [self setTableView:nil];
}

#pragma mark- Delegate UITableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSInteger resul = 1;
    
    if (self.MostrarGrupos)
    {
        resul = 2;
    }
    
    if (self.MostrarContactosOtros)
    {
        resul = 3;
    }
    
    return resul;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    switch (section)
    {
        case 1:
            
            if (self.ListaGruposYaesmio.count > 0)
            {
                return Traducir(@"Grupos");
            }
            
            break;
            
        case 2:
            
            if (self.ListaContactosTelefono.count > 0)
            {
                return Traducir(@"otros");
            }
            
            break;
            
        default:
            
            if (self.ListaContactosYaesmio.count > 0)
            {
                return Traducir(@"Yasmiers");
            }
            
            break;
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case 2:
            return self.ListaContactosTelefono.count;
            
        case 1:
            return self.ListaGruposYaesmio.count;
            
        default:
            return self.ListaContactosYaesmio.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactoTelefonoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactoTelefonoCell"];
    
    id objeto = [self getContactoSegunIndexPath:indexPath];
    
    [cell configurar:objeto Seleccionado:[self.ListaContactosSeleccionados indexOfObject:objeto] != NSNotFound];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    id objeto = [self getContactoSegunIndexPath:indexPath];
    ContactoTelefonoCell *cell = (ContactoTelefonoCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    if (cell.Seleccionado)  // se quita de la lista de seleccionados
    {
        [self.ListaContactosSeleccionados removeObject:objeto];
    }
    else                    // se añade a  la lista de seleccionados
    {
        [self.ListaContactosSeleccionados addObject:objeto];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    cell.Seleccionado = !cell.Seleccionado;
}

#pragma mark- Metodos Privados

- (id)getContactoSegunIndexPath:(NSIndexPath *)pIndex
{
    switch (pIndex.section)
    {
        case 1:
            return self.ListaGruposYaesmio[pIndex.row];
            
        case 2:
            return self.ListaContactosTelefono[pIndex.row];
            
        default:
            return self.ListaContactosYaesmio[pIndex.row];
    }
}

- (IBAction)GuardarSeleccionContactos:(id)sender
{
    [self CerrarPantalla:self.ListaContactosSeleccionados];
}

- (IBAction)CancelarSeleccionContactos:(id)sender
{
    [self CerrarPantalla:nil];
}

- (NSMutableArray *)obtenerContactosTelefono
{
    NSMutableArray *resultado = [NSMutableArray new];
    
    for (RHPerson *persona in[[Locator Default].Contactos getListaContactosConMailOTelefono])
    {
        [resultado addObject:[[Contacto alloc] initWithPerson:persona]];
    }
    
    return resultado;
}

- (NSArray *)obtenerListaContactosParaComprobacion
{
    NSMutableArray *resultado = [NSMutableArray new];
    
    for (Contacto *persona in self.ListaContactosTelefono)
    {
        if (persona.ContactoTelefono != nil)
        {
            for (NSString *mail in persona.ContactoTelefono.emails.values)
            {
                [resultado addObject:[[Contacto alloc] initWithMail:mail]];
            }
            
            for (NSString *telef in persona.ContactoTelefono.phoneNumbers.values)
            {
                [resultado addObject:[[Contacto alloc] initWithPhone:telef]];
            }
        }
    }
    
    return resultado;
}

- (void)actualizarContactosConYaesmio:(NSArray *)pLista
{
    NSMutableArray *aSeleccionar = [NSMutableArray new];
    
    for (Contacto *yasmier in pLista)
    {
        for (Contacto *telefono in self.ListaContactosTelefono)
        {
            if (yasmier.Telefono.isNotEmpty)
            {
                if ([self SeEncuentraDato:yasmier.Telefono EnLista:telefono.ContactoTelefono.phoneNumbers.values])
                {
                    [aSeleccionar addObject:telefono];
                    [yasmier copiarDatosEn:telefono];
                    break;                                         // salida del for de contactos telefono
                }
            }
            
            if (yasmier.MailUsuario.isNotEmpty)
            {
                if ([self SeEncuentraDato:yasmier.MailUsuario EnLista:telefono.ContactoTelefono.emails.values])
                {
                    [aSeleccionar addObject:telefono];
                    [yasmier copiarDatosEn:telefono];
                    break;                                         // salida del for de contactos telefono
                }
            }
        }
    }
    
    for (Contacto *contactoEliminar in aSeleccionar)
    {
        [self.ListaContactosTelefono removeObject:contactoEliminar];
    }
    
    self.ListaContactosYaesmio = [NSMutableArray arrayWithArray:aSeleccionar];
    
    [self ActualizarContactosSeleccionados];
    
    [self.tableView reloadData];
}

- (BOOL)SeEncuentraDato:(NSString *)pDato EnLista:(NSArray *)pLista
{
    for (NSString *mail in pLista)
    {
        if ([mail isEqualToString:pDato])
        {
            return true;
        }
    }
    
    return false;
}

- (void)CerrarPantalla:(NSArray *)pLista
{
    [self dismissViewControllerAnimated:TRUE completion:NULL];
    
    if (self.Respond != nil)
    {
        self.Respond(pLista);
    }
}

- (void)ActualizarContactosSeleccionados
{
    [self.ListaContactosSeleccionados removeAllObjects];
    
    for (id item in self.ListaEmailsSeleccionados)
    {
        if ([item isKindOfClass:[Contacto class]])
        {
            Contacto *actual = (Contacto *)item;
            
            Contacto *encontado = [actual BuscarEnLista:self.ListaContactosTelefono];
            
            if (encontado != nil)
            {
                [self.ListaContactosSeleccionados addObject:encontado];
            }
            else
            {
                encontado = [actual BuscarEnLista:self.ListaContactosYaesmio];
                
                if (encontado != nil)
                {
                    [self.ListaContactosSeleccionados addObject:encontado];
                }
            }
        }
        else if ([item isKindOfClass:[GrupoNotificacion class]])
        {
            GrupoNotificacion *myGrupo = (GrupoNotificacion *)item;
            GrupoNotificacion *encontado = [myGrupo BuscarEnLista:self.ListaGruposYaesmio];
            
            if (encontado != nil)
            {
                [self.ListaContactosSeleccionados addObject:encontado];
            }
        }
    }
}

@end
