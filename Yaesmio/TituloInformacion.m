//
//  TituloInformacion.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "TituloInformacion.h"

@implementation TituloInformacion

- (id)init
{
    if ((self = [super init]))
    {
        self.IdTitulo = @"";
        self.Titulo   = @"";
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.IdTitulo xmlNodeForDoc:node->doc elementName:@"Codigo" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Titulo xmlNodeForDoc:node->doc elementName:@"Titulo" elementNSPrefix:@"tns3"]);
}

+ (TituloInformacion *)deserializeNode:(xmlNodePtr)cur
{
    TituloInformacion *newObject = [TituloInformacion new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    // NSLog(@"campo: %@",[NSString stringWithUTF8String: pobjCur->name]);
    
    if ([nodeName isEqualToString:@"Codigo"])
    {
        self.IdTitulo = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Titulo"])
    {
        self.Titulo = [NSString deserializeNode:pobjCur];
    }
}

@end
