//
//  CabeceraBusquedaCupon.h
//  Yaesmio
//
//  Created by freelance on 19/08/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductoBuscado.h"

@interface CabeceraBusquedaCupon : UICollectionReusableView

-(void)Configurar:(GrupoDistanciaProductoBuscado)Grupo NumeroElementos:(NSInteger)NumeroElementos;
-(void) Ocultar;

@end
