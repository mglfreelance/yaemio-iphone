//
//  Locator.h
//  yaesmio
//
//  Created by manuel garcia lopez on 06/05/13.
//  Copyright (c) 2013 manuel garcia lopez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ColorManager.h"
#import "PreferencesManager.h"
#import "CarritoManager.h"
#import "ProductosFavoritosManager.h"
#import "ImagenManager.h"
#import "ServicioSOAPManager.h"
#import "FontManager.h"
#import "ExpresionesRegularesManager.h"
#import "IdiomaManager.h"
#import "GeoposicionamientoManager.h"
#import "ViewControllersManager.h"
#import "DownloadManager.h"
#import "AlertaManager.h"
#import "SoundManager.h"
#import "DeviceManager.h"
#import "AppDelegate.h"
#import "ContactsManager.h"

@interface Locator : NSObject

+(Locator*) Default;

@property (readonly) ColorManager                *Color;
@property (readonly) PreferencesManager          *Preferencias;
@property (readonly) CarritoManager              *Carrito;
@property (readonly) ProductosFavoritosManager   *ProductosFavoritos;
@property (readonly) ImagenManager               *Imagen;
@property (readonly) ServicioSOAPManager         *ServicioSOAP;
@property (readonly) FontManager                 *FuenteLetra;
@property (readonly) ExpresionesRegularesManager *ExpresionesRegulares;
@property (readonly) IdiomaManager               *Idioma;
@property (readonly) GeoposicionamientoManager   *GPS;
@property (readonly) ViewControllersManager      *Vistas;
@property (readonly) DownloadManager             *Download;
@property (readonly) NSString                    *VersionAndBuid;
@property (readonly) NSString                    *VersionApp;
@property (readonly) AlertaManager               *Alerta;
@property (readonly) SoundManager                *Sounds;
@property (readonly) DeviceManager               *Device;
@property (weak)     AppDelegate                 *Application;
@property (readonly) ContactsManager             *Contactos;

@end
