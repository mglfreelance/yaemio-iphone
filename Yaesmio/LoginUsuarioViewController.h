//
//  LoginUsuarioViewController.h
//  yaesmio
//
//  Created by manuel garcia lopez on 07/05/13.
//  Copyright (c) 2013 manuel garcia lopez. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YaesmioViewControllers.h"

@interface LoginUsuarioViewController : YaesmioViewController

@property (copy) ResultadoPorDefectoBlock Resultado;
@property (nonatomic) UIViewController *VistaInicial;

+(void)ActivarUsuario:(LoginUsuarioResponse*)Response Email:(NSString*)Email password:(NSString*)Password;

@end
