//
//  Moneda.m
//  Yaesmio
//
//  Created by freelance on 08/07/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "Moneda.h"

@implementation Moneda

- (id)init
{
	if((self = [super init]))
    {
		self.Codigo = @"";
		self.Nombre = @"";
	}
	
	return self;
}

-(NSString*)Abreviatura
{
    return self.Codigo;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Codigo xmlNodeForDoc:node->doc elementName:@"WAERS" elementNSPrefix:@"tns3"]);
	
	xmlAddChild(node, [self.Nombre xmlNodeForDoc:node->doc elementName:@"KTEXT" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.LTEXT xmlNodeForDoc:node->doc elementName:@"LTEXT" elementNSPrefix:@"tns3"]);
}

+ (Moneda *)deserializeNode:(xmlNodePtr)cur
{
	Moneda *newObject = [Moneda new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

+(Moneda *) newWithId:(NSString*)pID andName:(NSString*)pName
{
    Moneda* resultado = [[Moneda alloc]init];
    
    resultado.Codigo = pID;
    resultado.Nombre = pName;
    
    return resultado;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"WAERS"])
    {
        self.Codigo = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"KTEXT"])
    {
        self.Nombre = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"LTEXT"])
    {
        self.LTEXT = [NSString deserializeNode:pobjCur];
    }
}

@end
