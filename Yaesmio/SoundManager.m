//
//  SoundManager.m
//  Yaesmio
//
//  Created by freelance on 29/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoundManager.h"
#import <AudioToolbox/AudioToolbox.h>
#import <AVFoundation/AVFoundation.h>

@interface SoundManager () <AVAudioPlayerDelegate>


@end

@implementation SoundManager


AVAudioPlayer *PlayerAudio;

-(void) PlayNotificacion
{
    if(self.UrlNotificacion != nil)
    {
        [[Locator Default].Download RequestAssync:[NSURL URLWithString:self.UrlNotificacion] Type:DownloadManagerTypeFileAudio Grupo:@"login" ResultData:^(NSData *pResult)
         {
             NSError *error;
             PlayerAudio=[[AVAudioPlayer alloc] initWithData:pResult error:&error];
             PlayerAudio.delegate = self;
             if(error == nil)
             {
                 [PlayerAudio play];
                 AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
             }
         }];
    }
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    PlayerAudio.delegate = nil;
    PlayerAudio = nil;
}

@end
