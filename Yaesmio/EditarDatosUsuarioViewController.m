//
//  EditarDatosUsuarioViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 26/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "EditarDatosUsuarioViewController.h"
#import "UIAllControls.h"
#import "Moneda.h"

typedef NS_ENUM (NSInteger, EditarUsuarioSaveDatos)
{
    EditarUsuarioSaveDatosTodoOK        = 0, // todo ok
    EditarUsuarioSaveDatosErrFechaMenor = 1, // la fecha deve ser menor que hoy
    EditarUsuarioSaveDatosErrNoFecha    = 2, // es obligatorio la fecha
    EditarUsuarioSaveDatosErrNOSexo     = 3, // es obligatorio el sexo
    EditarUsuarioSaveDatosErrNOMoneda   = 4, // es obligatorio la moneda
    EditarUsuarioSaveDatosErrNombre     = 5, // es obligatorio el nombre
    EditarUsuarioSaveDatosErrApellidos  = 6, // es obligatorio los apellidos
};

@interface EditarDatosUsuarioViewController ()

@property (weak, nonatomic) IBOutlet UILabel *labelCabecera;
@property (weak, nonatomic) IBOutlet UITextField *txtUsuario;
@property (weak, nonatomic) IBOutlet UITextField *txtNombre;
@property (weak, nonatomic) IBOutlet UITextField *txtApellidos;
@property (weak, nonatomic) IBOutlet UIButton *btnFechaNacimiento;
@property (weak, nonatomic) IBOutlet M13Checkbox *checkHombre;
@property (weak, nonatomic) IBOutlet M13Checkbox *checkMujer;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollViewDatos;
@property (weak, nonatomic) IBOutlet M13Checkbox *checkPermitirPublicidad;
@property (weak, nonatomic) IBOutlet UIButton *btnMoneda;

@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCamposTraducir;


- (IBAction)btnGuardarDatos_click:(id)sender;
- (IBAction)btnFechaNacimiento_click:(id)sender;
- (IBAction)checkHombre_click:(id)sender;
- (IBAction)btnMoneda_click:(id)sender;

@property (nonatomic) ScrollManager *manager;
@property (strong) Moneda *MonedaActual;

@end

@implementation EditarDatosUsuarioViewController

@synthesize MonedaActual = _MonedaActual;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaCamposTraducir);
    
    [self.btnFechaNacimiento setBackgroundImage:[Locator Default].Imagen.FondoBotonSeleccionBlanco forState:UIControlStateNormal];
    [self.btnMoneda setBackgroundImage:[Locator Default].Imagen.FondoBotonSeleccionBlanco forState:UIControlStateNormal];
    
    self.manager = [ScrollManager NewAndConfigureWithScrollView:self.ScrollViewDatos AndDidEndTextField:self Selector:@selector(btnGuardarDatos_click:)];
    
    self.checkHombre.checkAlignment = M13CheckboxAlignmentLeft;
    self.checkMujer.checkAlignment = M13CheckboxAlignmentLeft;
    
    [self CargarDatos];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.esModal)
    {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(SalirModal)];
    }
    else
    {
        [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
    }
}

- (void)viewUnload
{
    [self setLabelCabecera:nil];
    [self setTxtUsuario:nil];
    [self setTxtNombre:nil];
    [self setTxtApellidos:nil];
    [self setBtnFechaNacimiento:nil];
    [self setCheckHombre:nil];
    [self setCheckMujer:nil];
    [self setManager:nil];
    [self setScrollViewDatos:nil];
    [self setListaCamposTraducir:nil];
    [self setBtnMoneda:nil];
}

- (void)setMonedaActual:(Moneda *)MonedaActual
{
    _MonedaActual = MonedaActual;
    self.btnMoneda.TextStateNormal = self.MonedaActual.Nombre;
}

- (Moneda *)MonedaActual
{
    return _MonedaActual;
}

#pragma  mark- Eventos Privados

- (IBAction)btnGuardarDatos_click:(id)sender
{
    Usuario *lUsuario = [self.usuario Clonar];
    
    switch ([self SaveDatos:lUsuario])
    {
        case EditarUsuarioSaveDatosErrFechaMenor:
            [[Locator Default].Alerta showMessageAcept:Traducir(@"La fecha de nacimiento no es correcta.")];
            break;
            
        case EditarUsuarioSaveDatosErrNoFecha:
            [[Locator Default].Alerta showMessageAcept:Traducir(@"La fecha de nacimiento es necesaria para realizar compras.")];
            break;
            
        case EditarUsuarioSaveDatosErrNOMoneda:
            [[Locator Default].Alerta showMessageAcept:Traducir(@"La moneda es necesaria para realizar compras.")];
            break;
            
        case EditarUsuarioSaveDatosErrApellidos:
            [[Locator Default].Alerta showMessageAcept:Traducir(@"Los apellidos son necesarios para realizar compras.")];
            break;
            
        case EditarUsuarioSaveDatosErrNombre:
            [[Locator Default].Alerta showMessageAcept:Traducir(@"El nombre es necesario para realizar compras.")];
            break;
            
        case EditarUsuarioSaveDatosErrNOSexo:
            [[Locator Default].Alerta showMessageAcept:Traducir(@"El campo sexo es necesario para realizar compras.")];
            break;
            
        case EditarUsuarioSaveDatosTodoOK:
            [[Locator Default].Alerta showBussy:Traducir(@"Guardando usuario.")];
            [[Locator Default].ServicioSOAP ModificarUsuario:lUsuario Result:^(SRResultMethod pResult, NSString *MensajeSap)
             {
                 [self ComprobarUsuario:lUsuario];
                 switch (pResult)
                 {
                     case SRResultMethodErrConex:
                         [[Locator Default].Alerta showErrorConexSOAP];
                         break;
                         
                     case SRResultMethodNO:
                         
                         if ([MensajeSap isEqualToString:@""])
                         {
                             MensajeSap = Traducir(@"No ha sido posible guardar el usuario.");
                         }
                         
                         [[Locator Default].Alerta showMessageAcept:MensajeSap];
                         break;
                         
                     case SRResultMethodYES:
                         [[Locator Default].Alerta showMessageAcept:Traducir(@"El usuario se ha guardado correctamente.") ResulBlock:^
                          {
                              [self CerrarPantalla];
                          }];
                         
                         break;
                 }
             }];
            
            break;
    }
}

- (IBAction)btnFechaNacimiento_click:(id)sender
{
    [[Locator Default].Vistas MostrarDatePickerSemiModal:self Seleccionado:[self getDate] MaxFecha:[NSDate new] Titulo:Traducir(@"Fecha Nacimiento") Result:^(NSDate *pResult)
     {
         if (pResult != nil)
         {
             self.btnFechaNacimiento.TextStateNormal = [pResult format:Traducir(@"dd/mm/yyyy")];
             [self.manager ClearScroll];
         }
     }];
}

- (IBAction)checkHombre_click:(id)sender
{
    if (self.checkHombre.checkState == M13CheckboxStateChecked && self.checkMujer.checkState == M13CheckboxStateChecked)
    {
        if (sender == self.checkHombre)
        {
            self.checkMujer.checkState = M13CheckboxStateUnchecked;
        }
        else if (sender == self.checkMujer)
        {
            self.checkHombre.checkState = M13CheckboxStateUnchecked;
        }
    }
}

- (IBAction)btnMoneda_click:(id)sender
{
    [[Locator Default].Vistas MostrarSeleccionMoneda:self Seleccionada:nil Resultado:^(Moneda *pResult)
     {
         if (pResult != nil)
         {
             self.MonedaActual = pResult;
         }
     }];
}

#pragma  mark- Metodos Privados

- (void)CargarDatos
{
    self.txtUsuario.text = [Locator Default].Preferencias.MailUsuario;
    self.txtNombre.text = self.usuario.Nombre;
    self.txtApellidos.text = self.usuario.Apellidos;
    self.btnFechaNacimiento.TextStateNormal = [self.usuario.fecha_nacimiento format:Traducir(@"dd/mm/yyyy")];
    
    self.checkHombre.checkState = M13CheckboxStateUnchecked;
    self.checkMujer.checkState = M13CheckboxStateUnchecked;
    
    if ([self.usuario.sexo isEqualToString:@"1"])
    {
        self.checkHombre.checkState = M13CheckboxStateChecked;
    }
    else if ([self.usuario.sexo isEqualToString:@"2"])
    {
        self.checkMujer.checkState = M13CheckboxStateChecked;
    }
    
    if (self.usuario.aceptaEnvioPublicidad)
    {
        self.checkPermitirPublicidad.checkState = M13CheckboxStateChecked;
    }
    else
    {
        self.checkPermitirPublicidad.checkState = M13CheckboxStateUnchecked;
    }
    
    self.MonedaActual = self.usuario.Moneda;
}

- (EditarUsuarioSaveDatos)SaveDatos:(Usuario *)lUsuario
{
    EditarUsuarioSaveDatos resul = EditarUsuarioSaveDatosTodoOK;
    NSDate *fecha = [NSDate fromString:self.btnFechaNacimiento.TextStateNormal WithFormat:Traducir(@"dd/mm/yyyy")];
    
    if ([self.txtNombre.text isEqualToString:@""])
    {
        resul = EditarUsuarioSaveDatosErrNombre;
    }
    else if ([self.txtApellidos.text isEqualToString:@""])
    {
        resul = EditarUsuarioSaveDatosErrApellidos;
    }
    else if ([fecha compareIgnoreTime:[NSDate new]] != NSOrderedDescending)
    {
        resul = EditarUsuarioSaveDatosErrFechaMenor;
    }
    else if ([self.btnFechaNacimiento.TextStateNormal isEqualToString:@""])
    {
        resul = EditarUsuarioSaveDatosErrNoFecha;
    }
    else if (self.checkHombre.checkState != M13CheckboxStateChecked && self.checkMujer.checkState != M13CheckboxStateChecked)
    {
        resul = EditarUsuarioSaveDatosErrNOSexo;
    }
    else if ([self.btnMoneda.TextStateNormal isEqualToString:@""])
    {
        resul = EditarUsuarioSaveDatosErrNOMoneda;
    }
    else
    {
        resul = EditarUsuarioSaveDatosTodoOK;
        lUsuario.Nombre = self.txtNombre.text;
        lUsuario.Apellidos = self.txtApellidos.text;
        lUsuario.fecha_nacimiento = fecha;
        
        if (self.checkHombre.checkState == M13CheckboxStateChecked)
        {
            lUsuario.sexo = @"1";
        }
        else if (self.checkMujer.checkState == M13CheckboxStateChecked)
        {
            lUsuario.sexo = @"2";
        }
        
        // lUsuario.idMoneda =  self.txtMoneda.text;
        lUsuario.aceptaEnvioPublicidad = (self.checkPermitirPublicidad.checkState == M13CheckboxStateChecked);
    }
    
    return resul;
}

- (void)ComprobarUsuario:(Usuario *)pUsuario
{
    [Locator Default].Preferencias.CompletadoDatosUsuario = !([pUsuario.Nombre isEqualToString:@""] || [pUsuario.Apellidos isEqualToString:@""] || pUsuario.fecha_nacimiento == nil || [pUsuario.sexo isEqualToString:@""]);
    
    if ([Locator Default].Preferencias.CompletadoDatosUsuario == false)
    {
        [Locator Default].Preferencias.NumeroVecesAMostrarMensajeCompletarUsuario = 10;
    }
}

- (NSDate *)getDate
{
    NSDate *lResultado = nil;
    
    if ([self.btnFechaNacimiento.TextStateNormal isEqualToString:@""] == false)
    {
        lResultado = [NSDate fromString:self.btnFechaNacimiento.TextStateNormal WithFormat:Traducir(@"dd/mm/yyyy")];
    }
    
    return lResultado;
}

- (void)CerrarPantalla
{
    if (self.esModal)
    {
        [self dismissViewControllerAnimated:true completion:nil];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:true];
    }
    
    if (self.Resultado != nil)
    {
        self.Resultado(1);
    }
}

- (void)SalirModal
{
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

@end
