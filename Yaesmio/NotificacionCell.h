//
//  NotificacionRecibidaCell.h
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CabeceraNotificacion.h"

@interface NotificacionCell : UITableViewCell

- (void)Configurar:(CabeceraNotificacion *)pNotificacion Menu:(id)Menu;

@end
