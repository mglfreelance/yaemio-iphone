//
//  ViewControllersManager.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 14/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EmpresaCarrito.h"
#import "Direccion.h"
#import "ElementoPicker.h"
#import "ProductoPadre.h"
#import "ValoresdeAtributo.h"
#import "Historico.h"
#import "VcardImporter.h"
#import "GrupoArticulo.h"
#import "Empresa.h"
#import "GrupoNotificacion.h"
#import "YaesmioViewControllers.h"
#import "FiltroBusquedaProductos.h"
#import "Moneda.h"

@interface ViewControllersManager : NSObject

- (void)MostrarListaSitiosYaesmio:(UIViewController *)pView;

- (void)MostrarWebView:(UIViewController *)pView url:(NSString *)pUrl;

- (void)MostrarWebView:(UIViewController *)pView ContenidoWeb:(NSString *)pContenido;

- (BOOL)MostrarWebSafari:(NSString *)purl;

- (BOOL)MostrarWebSafariConNSURL:(NSURL *)purl;

- (void)MostrarCarritoCompra:(UIViewController *)pView;

- (void)MostrarLoginUsuario:(UIViewController *)pView Result:(ResultadoPorDefectoBlock)pResult;

- (void)MostrarBienvenida:(UIViewController *)pView;

- (void)AutoLogarUsuario:(UIViewController *)pView MostrarOcupado:(BOOL)pMostrarOcupado Result:(void (^)())pBlock;

- (void)MostrarEditarProducto:(UIViewController *)pView Producto:(ElementoCarrito *)pProducto;

- (void)MostrarMensajeNoQR:(UIViewController *)pView Mensaje:(NSString *)pMensaje;

typedef void (^ResultadoEditarDireccionBlock)(Direccion *pResult);
- (void)MostrarEditarDireccionEnvioSoloEnvio:(UIViewController *)pView Direccion:(Direccion *)pDireccion Result:(ResultadoEditarDireccionBlock)pResult;
- (void)MostrarEditarDireccionEnvioAlternativa:(UIViewController *)pView Direccion:(Direccion *)pDireccion EsVistaCarrito:(BOOL)pEsVistaCarrito Result:(ResultadoEditarDireccionBlock)pResult;
- (void)MostrarEditarDireccionEnvioPrincipal:(UIViewController *)pView Direccion:(Direccion *)pDireccion EsVistaCarrito:(BOOL)pEsVistaCarrito Result:(ResultadoEditarDireccionBlock)pResult;

- (void)MostrarGaleriaImagenes:(UIViewController *)pView ListaImagenes:(NSArray *)pLista UrlImagenEmpresa:(NSString *)pUrlLogo;
- (void)MostrarGaleriaImagenes:(UIViewController *)pView ListaImagenes:(NSArray *)pLista NumImagenAmostrar:(int)pNumImagen UrlImagenEmpresa:(NSString *)pUrlLogo;

typedef void (^ResultTimerMostrarVideoBlock)(NSTimeInterval pTimer);
- (void)MostrarVideoAPantallaCompleta:(UIViewController *)pView urlVideo:(NSString *)pUrlVideo UrlImagenEmpresa:(NSString *)pUrlLogo Timer:(NSTimeInterval)pTimer Result:(ResultTimerMostrarVideoBlock)pResult;

typedef void (^MostrarPickerSemiModalBlock)(ElementoPicker *pResult);
- (void)MostrarPickerSemiModal:(UIViewController *)pView ListaElementos:(NSArray *)pLista Seleccionado:(ElementoPicker *)pSeleccionado Titulo:(NSString *)pTitulo Result:(MostrarPickerSemiModalBlock)pResultado;

typedef void (^MostrarValoresAtributoPickerSemiModalBlock)(ValoresdeAtributo *pResult);
- (void)MostrarValoresAtributoPickerSemiModal:(UIViewController *)pView ListaElementos:(NSArray *)pLista Seleccionado:(ValoresdeAtributo *)pSeleccionado Titulos:(NSArray *)pListaTitulos Result:(MostrarValoresAtributoPickerSemiModalBlock)pResultado;

typedef void (^MostrarDatePickerSemiModalBlock)(NSDate *pResult);
- (void)MostrarDatePickerSemiModal:(UIViewController *)pView Seleccionado:(NSDate *)pSeleccionado MaxFecha:(NSDate *)pMaxFecha Titulo:(NSString *)pTitulo Result:(MostrarDatePickerSemiModalBlock)pResultado;

- (void)MostrarCompartirSemiModal:(UIViewController *)pView Producto:(ProductoPadre *)pProducto result:(ResultadoBlock)pResult;

- (void)MostrarNotificacionProducto:(UIViewController *)pView Producto:(ProductoPadre *)pProducto Result:(void (^)())pResult;

- (void)MostrarProducto:(UIViewController *)pView Producto:(ProductoPadre *)pProductoPadre VieneDeFavoritos:(BOOL)pVieneDeFavoritos;

- (void)MostrarProductoModal:(UIViewController *)pView Producto:(ProductoPadre *)pProductoPadre VieneDeFavoritos:(BOOL)pVieneDeFavoritos;

- (void)MostrarEdicionUsuario:(UIViewController *)pView Usuario:(Usuario *)pUsuario Result:(ResultadoPorDefectoBlock)pResult;

typedef void (^MostrarPantallaPayPalBlock)(NSString *pResult);
- (void)MostrarPantallaPayPal:(UIViewController *)pView Carrito:(EmpresaCarrito *)pCarrito Usuario:(Usuario *)pUsuario Result:(MostrarPantallaPayPalBlock)pResult;

- (void)MostrarValoracionProductoHistorico:(UIViewController *)pView Historico:(Historico *)pHistorico Emoticonos:(NSArray *)pLista Resultado:(ResultadoPorDefectoBlock)pResult;

- (void)MostrarPantallaContacto:(UIViewController *)pView Datos:(VcardImporter *)pContacto;

- (void)MostrarPantallaSMS:(UIViewController *)pView Telefono:(NSString *)pTelefono Texto:(NSString *)pMensaje;

typedef NS_ENUM (NSInteger, TipoListaPaises)
{
    TipoListaPaisesGenerica    = 1,
    TipoListaPaisesParaCupones = 3,
};
typedef void (^MostrarSeleccionPaisBlock)(Pais *pResult);
- (void)MostrarSeleccionPais:(UIViewController *)pView Tipo:(TipoListaPaises)Tipo PaisSeleccionado:(Pais *)PaisSeleccionado Resultado:(MostrarSeleccionPaisBlock)pResult;

typedef void (^MostrarSeleccionProvinciaBlock)(Provincia *pResult);
- (void)MostrarSeleccionProvincia:(UIViewController *)pView Pais:(Pais *)pPais ProvinciaSeleccionada:(Provincia *)ProvinciaSeleccionada MostrarTodas:(BOOL)pMostrarTodas MostrarCercaDeMi:(BOOL)pMostrarCercaDeMi Resultado:(MostrarSeleccionProvinciaBlock)pResultadoFuncion;

typedef void (^MostrarSeleccionCategoriaProductoBlock)(GrupoArticulo *pResult);
- (void)MostrarSeleccionCategoriaProducto:(UIViewController *)pView Resultado:(MostrarSeleccionCategoriaProductoBlock)pResultadoFuncion;

typedef void (^MostrarSeleccionEmpresaBusquedaBlock)(Empresa *pResult);
- (void)MostrarSeleccionEmpresaBusqueda:(UIViewController *)pView Resultado:(MostrarSeleccionEmpresaBusquedaBlock)pResultadoFuncion;

typedef void (^MostrarSeleccionMonedaBlock)(Moneda *pResult);
- (void)MostrarSeleccionMoneda:(UIViewController *)pView Seleccionada:(Moneda *)MonedaSeleccionada Resultado:(MostrarSeleccionMonedaBlock)pResult;

- (void)MostrarDestinatariosNotificacionEnviada:(UIViewController *)pView ListaDestinatarios:(NSArray *)pLista;

- (void)MostrarEdicionGrupoNotificacion:(UIViewController *)pView Grupo:(GrupoNotificacion *)pGrupo;

typedef void (^MostrarSeleccionContactosTelefonoBlock)(NSArray *pListaContactosSeleccionados);
- (void)MostrarSeleccionContactosTelefono:(UIViewController *)pView ListaMails:(NSArray *)pListaMails MostrarGrupos:(BOOL)pMostrarGrupos MostrarOtros:(BOOL)pMostrarOtros Result:(MostrarSeleccionContactosTelefonoBlock)pResult;

- (void)MostrarEnvioNotificacion:(UIViewController *)pView Producto:(ProductoPadre *)pProducto;

- (void)MostrarFavoritos:(YaesmioViewController *)pView;

- (void)MostrarNotificaciones:(UIViewController *)pView;

- (void)MostrarMisDatos:(YaesmioViewController *)pView;

- (void)MostrarInformacion:(YaesmioViewController *)pView;

- (void)MostrarMisAjustes:(YaesmioViewController *)pView;

- (void)MostrarMiHistoricoDeCompras:(YaesmioViewController *)pView;

- (void)MostrarBusquedaProductos:(YaesmioViewController *)pView Filtro:(FiltroBusquedaProductos *)filtro;

- (void)MostrarCapturaQR:(YaesmioViewController *)pView;

- (void)ObtenerConfiguracionApp:(ResultadoPorDefectoBlock)Resultado;

@end
