//
//  ProductoCompraCell.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductoCompraCell : UITableViewCell

-(void)Configurar:(UIViewController*)pView Elemento:(ElementoCarrito*)pElemento;

@end
