//
//  ConfigurationParameter.m
//  Yaesmio
//
//  Created by freelance on 08/07/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ConfigurationParameter.h"

@implementation ConfigurationParameter

- (id)init
{
	if((self = [super init]))
    {
		self.Codigo = @"";
		self.Nombre = @"";
	}
	
	return self;
}

-(NSString*)Abreviatura
{
    return self.Codigo;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Codigo xmlNodeForDoc:node->doc elementName:@"ZCAMPO" elementNSPrefix:@"tns3"]);
	
	xmlAddChild(node, [self.Nombre xmlNodeForDoc:node->doc elementName:@"ZVALOR" elementNSPrefix:@"tns3"]);
}

+ (ConfigurationParameter *)deserializeNode:(xmlNodePtr)cur
{
	ConfigurationParameter *newObject = [ConfigurationParameter new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

+(ConfigurationParameter *) newWithId:(NSString*)pID andName:(NSString*)pName
{
    ConfigurationParameter* resultado = [[ConfigurationParameter alloc]init];
    
    resultado.Codigo = pID;
    resultado.Nombre = pName;
    
    return resultado;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ZCAMPO"])
    {
        self.Codigo = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"ZVALOR"])
    {
        self.Nombre = [NSString deserializeNode:pobjCur];
    }
}

@end
