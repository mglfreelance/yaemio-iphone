//
//  MostrarImagenViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 05/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "GaleriaImagenesViewController.h"
#import "UIAllControls.h"
#import "Media.h"
#import "ImageCaleryCell.h"


@interface GaleriaImagenesViewController () <FGalleryPhotoViewDelegate>

@property (nonatomic) IBOutlet UICollectionView *collectioViewImagenes;
@property (nonatomic) IBOutlet UIImageView *imgLogoEmpresa;
@property (nonatomic) IBOutlet UIPageControl *PageControlImagenes;
@property (nonatomic) IBOutlet UIView *VistaRotatoria;

- (IBAction)PageControl_ChangedPage:(id)sender;

@property (assign) UIDeviceOrientation mCurrentOrientation;
@property (assign) UIDeviceOrientation mPreviousOrientation;
@property (assign) int mDegree;
@property (assign) int mHeight;
@property (assign) int mWidht;
@end

@implementation GaleriaImagenesViewController

#define GRUPO_GALERIAIMAGENES @"GaleriaImagenesViewController"

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.imgLogoEmpresa cargarUrlImagenIzquierdaAsincrono:self.UrlImageEmpresa Grupo:GRUPO_GALERIAIMAGENES];
    self.PageControlImagenes.numberOfPages = self.ListaMedia.count;
    
    [[Locator Default].Carrito QuitarBotonCarrito:self];
    
    self.mCurrentOrientation = UIDeviceOrientationPortrait;
    self.mPreviousOrientation = UIDeviceOrientationPortrait;
    self.mHeight = self.view.frame.size.height - 40;
    self.mWidht = self.view.frame.size.width;
    [self.VistaRotatoria setCenter:CGPointMake(self.mWidht / 2, self.mHeight / 2)];
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    self.collectioViewImagenes.hidden = (self.NumeroImagenActual > 0);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self MostrarImagenActual:false];
}

- (void)deviceOrientationDidChange:(NSNotification *)notification
{
    //Obtaining the current device orientation
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    
    //Ignoring specific orientations
    if (orientation == UIDeviceOrientationFaceUp || orientation == UIDeviceOrientationFaceDown || orientation == UIDeviceOrientationUnknown || self.mCurrentOrientation == orientation)
    {
        return;
    }
    
    //Responding only to changes in landscape or portrait
    self.mPreviousOrientation = self.mCurrentOrientation;
    self.mCurrentOrientation = orientation;
    //
    [self performSelector:@selector(orientationChangedMethod) withObject:nil afterDelay:0];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
}

- (void)viewUnload
{
    [self setCollectioViewImagenes:nil];
}

#pragma mark- Metodos Privados

- (void)orientationChangedMethod
{
    int lDegree = [self getDegrees];
    
    self.mDegree += lDegree;
    lDegree = (self.mDegree / 90) % 2;
    NSIndexPath *actualIndex = [self getActualIndexPath];
    
    [UIView animateWithDuration:0.5 animations:^
     {
         if (lDegree == 0)
         {
             [self.VistaRotatoria setBounds:CGRectMake(0, 0, self.mWidht, self.mHeight)];
         }
         else
         {
             [self.VistaRotatoria setBounds:CGRectMake(0, 0, self.mHeight, self.mWidht)];
         }
         
         [self.VistaRotatoria setTransform:CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(self.mDegree))];
         [self.collectioViewImagenes reloadData];
         [self.collectioViewImagenes scrollToItemAtIndexPath:actualIndex atScrollPosition:UICollectionViewScrollPositionRight animated:true];
     }];
}

- (NSIndexPath *)getActualIndexPath
{
    CGFloat pageWidth = self.collectioViewImagenes.frame.size.width - 10;
    int row = self.collectioViewImagenes.contentOffset.x / pageWidth;
    
    return [NSIndexPath indexPathForRow:row inSection:0];
}

- (int)getDegrees
{
    int result = 0;
    
    switch (self.mPreviousOrientation)
    {
        case UIDeviceOrientationPortrait:
            
            if (self.mCurrentOrientation == UIDeviceOrientationLandscapeLeft)
            {
                result = 90;
            }
            else if (self.mCurrentOrientation == UIDeviceOrientationLandscapeRight)
            {
                result = -90;
            }
            else if (self.mCurrentOrientation == UIDeviceOrientationPortraitUpsideDown)
            {
                result = -180;
            }
            
            break;
            
        case UIDeviceOrientationLandscapeLeft:
            
            if (self.mCurrentOrientation == UIDeviceOrientationPortrait)
            {
                result = -90;
            }
            else if (self.mCurrentOrientation == UIDeviceOrientationLandscapeRight)
            {
                result = -180;
            }
            else if (self.mCurrentOrientation == UIDeviceOrientationPortraitUpsideDown)
            {
                result = 90;
            }
            
            break;
            
        case UIDeviceOrientationLandscapeRight:
            
            if (self.mCurrentOrientation == UIDeviceOrientationLandscapeLeft)
            {
                result = 180;
            }
            else if (self.mCurrentOrientation == UIDeviceOrientationPortrait)
            {
                result = 90;
            }
            else if (self.mCurrentOrientation == UIDeviceOrientationPortraitUpsideDown)
            {
                result = -90;
            }
            
            break;
            
        case UIDeviceOrientationPortraitUpsideDown:
            
            if (self.mCurrentOrientation == UIDeviceOrientationLandscapeLeft)
            {
                result = -90;
            }
            else if (self.mCurrentOrientation == UIDeviceOrientationLandscapeRight)
            {
                result = 90;
            }
            else if (self.mCurrentOrientation == UIDeviceOrientationPortrait)
            {
                result = 180;
            }
            
            break;
            
        default:
            result = 0;
            break;
    }
    
    return result;
}

- (void)MostrarImagenActual:(BOOL)pAnimated
{
    if (self.NumeroImagenActual >= self.ListaMedia.count)
    {
        self.NumeroImagenActual = 0;
    }
    else if (self.NumeroImagenActual < 0)
    {
        self.NumeroImagenActual = self.ListaMedia.count - 1.0;
    }
    
    NSIndexPath *nuevo = [NSIndexPath indexPathForRow:self.NumeroImagenActual inSection:0];
    
    [self.collectioViewImagenes scrollToItemAtIndexPath:nuevo atScrollPosition:UICollectionViewScrollPositionNone animated:pAnimated];
}

- (void)MostrarSiguienteImagen
{
    self.NumeroImagenActual++;
    [self MostrarImagenActual:true];
}

- (void)MostrarAnteriorImagen
{
    self.NumeroImagenActual--;
    [self MostrarImagenActual:true];
}

- (IBAction)PageControl_ChangedPage:(id)sender
{
    self.NumeroImagenActual  = (int)self.PageControlImagenes.currentPage;
    [self MostrarImagenActual:true];
}

- (void)didSwipePhotoView:(FGalleryPhotoView *)photoview SwipeLef:(BOOL)pSwipeLef SwipeRight:(BOOL)pSwipeRight
{
    if (pSwipeLef)
    {
        [self MostrarAnteriorImagen];
    }
    else
    {
        [self MostrarSiguienteImagen];
    }
}

- (Media *)getMedia:(NSIndexPath *)pIndex
{
    return self.ListaMedia[pIndex.row];
}

#pragma mark - UICollectionView Datasource

- (NSInteger)collectionView:(UICollectionView *)view numberOfItemsInSection:(NSInteger)section
{
    return self.ListaMedia.count;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)cv cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    Media *lMedia = [self getMedia:indexPath];
    
    ImageCaleryCell *cell = [cv dequeueReusableCellWithReuseIdentifier:[ImageCaleryCell getReuseIdentifier] forIndexPath:indexPath];
    
    [cell ConfigurarImagen:lMedia];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.collectioViewImagenes.frame.size.width, self.collectioViewImagenes.frame.size.height);
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.PageControlImagenes.currentPage = [self getActualIndexPath].row;
    
    if (self.collectioViewImagenes.hidden == true)
    {
        [UIView animateWithDuration:500 animations:^{
            self.collectioViewImagenes.hidden = false;
        }];
    }
}

@end
