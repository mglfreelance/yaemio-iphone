//
//  UIImageView+Ampliado.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 28/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "UIImageView+Ampliado.h"
#import "UIImage+Ampliado.h"
#import "UIViewAmpliado.h"
#import "UIImage+animatedGIF.h"
#import "UIImageView+AFNetworking.h"

@implementation UIImageView (Ampliado)

- (void)cargarUrlImagenAsincrono:(NSString *)pUrl Busy:(UIActivityIndicatorView *)pControlBusy
{
    
    [self setImageWithURL:[NSURL URLWithString:pUrl]];
    
    if (pControlBusy != nil)
    {
        pControlBusy.hidden = true;
        [pControlBusy stopAnimating];
    }
}

- (void)cargarUrlImagenAsincrono:(NSString *)pUrl Grupo:(NSString *)pGrupo Busy:(UIActivityIndicatorView *)pControlBusy
{
    [self cargarUrlImagenAsincrono:pUrl Grupo:pGrupo Busy:pControlBusy Result:Nil];
}

- (void)cargarUrlImagenIzquierdaAsincrono:(NSString *)pUrl Grupo:(NSString *)pGrupo
{
    [self cargarStringUrlImagenIzquierdaAsincrono:pUrl Grupo:pGrupo Result:nil Busy:nil];
}

- (void)cargarStringUrlImagenIzquierdaAsincrono:(NSString *)pUrl Grupo:(NSString *)pGrupo Result:(ResultadoBlock)pResultado Busy:(UIActivityIndicatorView *)pControlBusy
{
    [self cargarUrlImagenAsincrono:pUrl Grupo:pGrupo Busy:pControlBusy Result: ^
     {
         [self PonerAlaIzquierda:pResultado];
     }];
}

- (void)cargarUrlImagenDerechaAsincrono:(NSString *)pUrl Grupo:(NSString *)pGrupo
{
    [self cargarStringUrlImagenDerechaAsincrono:pUrl Grupo:pGrupo Result:nil Busy:nil];
}

- (void)cargarStringUrlImagenDerechaAsincrono:(NSString *)pUrl Grupo:(NSString *)pGrupo Result:(ResultadoBlock)pResultado Busy:(UIActivityIndicatorView *)pControlBusy
{
    [self cargarUrlImagenAsincrono:pUrl Grupo:pGrupo Busy:pControlBusy Result: ^
     {
         CGSize size = [self sizeThatFits:self.frame.size];
         
         CGSize actualSize;
         actualSize.height = self.frame.size.height;
         actualSize.width = size.width / (1.0 * (size.height / self.frame.size.height));
         
         if (isnan(actualSize.width))
         {
             actualSize.width = 0;
         }
         
         if (isnan(actualSize.height))
         {
             actualSize.height = 0;
         }
         
         
         CGRect frame = self.frame;
         frame.origin.x = self.frame.origin.x + self.frame.size.width - actualSize.width;
         frame.size = actualSize;
         
         if (frame.origin.x == NAN)
         {
             frame.origin.x = 0;
         }
         
         [self setFrame:frame];
         
         if (pResultado != nil)
         {
             pResultado();
         }
     }];
}

- (void)cargarUrlImagenAsincrono:(NSString *)pUrl Grupo:(NSString *)pGrupo Busy:(UIActivityIndicatorView *)pControlBusy Result:(ResultadoBlock)pResultado
{
    if (pControlBusy != nil)
    {
        pControlBusy.hidden = false;
        [pControlBusy startAnimating];
    }
    
    [UIImage ImageAssyncFromStringUrl:pUrl Grupo:pGrupo Result: ^(UIImage *pImage)
     {
         if (pControlBusy != nil)
         {
             pControlBusy.hidden = true;
             [pControlBusy stopAnimating];
         }
         
         self.image = pImage;
         
         if (pResultado != NULL)
         {
             pResultado();
         }
     }];
}

- (void)PonerAlaIzquierda:(ResultadoBlock)pResultado
{
    CGSize size = [self sizeThatFits:self.frame.size];
    
    CGSize actualSize;
    
    actualSize.height = self.frame.size.height;
    actualSize.width = size.width / (1.0 * (size.height / self.frame.size.height));
    
    if (isnan(actualSize.width))
    {
        actualSize.width = 0;
    }
    
    if (isnan(actualSize.height))
    {
        actualSize.height = 0;
    }
    
    CGRect frame = self.frame;
    frame.size = actualSize;
    [self setFrame:frame];
    
    if (pResultado != nil)
    {
        pResultado();
    }
}

@end
