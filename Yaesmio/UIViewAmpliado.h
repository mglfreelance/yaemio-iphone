#import <Foundation/Foundation.h>

#define UNCHANGED -1 // para no cambiar el valor

@interface UIView (Ampliado)

typedef NS_ENUM (NSInteger, MGPositionMoveEnum)
{
    MGPositionMoveToBefore      =  0,           // se movera a la izquierda del control
    MGPositionMoveToBehind      =  1,           // se movera a la derecha del control
    MGPositionMoveToAdove       =  2,           // se movera a arriba del control
    MGPositionMoveToBelow       =  3,           // se movera a abajo del control
    MGPositionMoveToTop         =  4,           // se movera arriba del control padre
    MGPositionMoveToBotton      =  5,           // se movera abajo del control padre
    MGPositionMoveToLeft        =  6,           // se movera a la izquierda del control
    MGPositionMoveToRight       =  7,           // se movera a la derecha del control
    MGPositionMoveToCenter      =  8,           // se movera al centro del control
    MGPositionMoveToTopLeft     =  9,           // se movera arriba y a la izquierda
    MGPositionMoveToTopRight    = 10,           // se movera arriba y a la derecha
    MGPositionMoveToBottonLeft  = 11,           // se movera abajo y a la izquierda
    MGPositionMoveToBottonRight = 12,           // se movera abajo y a la derecha
    MGPositionMoveToCenterLeft  = 13,           // se movera al centro izquierda
    MGPositionMoveToCenterRight = 14,           // se movera al centro derecha
};

typedef NS_ENUM (NSInteger, MGPositionInViewEnum)
{
    MGPositionInViewToTop         =  0,         // se movera arriba del control padre
    MGPositionInViewToBotton      =  1,         // se movera abajo del control padre
    MGPositionInViewToLeft        =  2,         // se movera a la izquierda del control padre
    MGPositionInViewToRight       =  3,         // se movera a la derecha del control padre
    MGPositionInViewToCenter      =  4,         // se movera al centro del control padre
    MGPositionInViewToTopLeft     =  5,         // se movera arriba y a la izquierda del padre
    MGPositionInViewToTopRight    =  6,         // se movera arriba y a la derecha del padre
    MGPositionInViewToBottonLeft  =  7,         // se movera abajo y a la izquierda del padre
    MGPositionInViewToBottonRight =  8,         // se movera abajo y a la derecha del padre
    MGPositionInViewToCenterLeft  =  9,         // se movera al centro izquierda del padre
    MGPositionInViewToCenterRight = 10,         // se movera al centro derecha del padre
    MGPositionInViewToCenterHorizontal = 11, // se movera al centro en horizontal,
    MGPositionInViewToCenterVertical = 12,  // se movera al centro en vertical
};

// asigna un nuevo ancho alto para mostrar todo el contenido
- (void)					mgSizeFitsMaxWidth:(CGFloat)pflMaxWidth MaxHeight:(CGFloat)pflMaxHeight;

// asigna un nuevo ancho alto para mostrar todo el contenido, ademas da opcion de añadir en el ancho y alto.(admite unchanged)
- (void)					mgSizeFitsMaxWidth:(CGFloat)pflMaxWidth MaxHeight:(CGFloat)pflMaxHeight AgregateWidth:(CGFloat)pflAgregateWidth AgregateHeight:(CGFloat)pflAgregateHeight;

- (CGSize)					mgGetSizeSubViews;

// asigna el frame directamente.
- (void)					mgSetSizeX:(CGFloat)pflx andY:(CGFloat)pfly andWidth:(CGFloat)pflwidth andHeight:(CGFloat)pflHeight;

//extiende el control en alto o ancho por la Izquierda
- (void)					mgExtendTopLeftForX:(CGFloat)pflx andY:(CGFloat)pfly;

//extiende el control en alto o ancho por la derecha
- (void)					mgExtendBottonRightForX:(CGFloat)pflx andY:(CGFloat)pfly;

// autoresising de la izquierda
- (void)					mgFixLeftEdge:(BOOL)fixed;
// autoresising de la derecha
- (void)					mgFixRightEdge:(BOOL)fixed;
// autoresising Arriba
- (void)					mgFixTopEdge:(BOOL)fixed;
// autoresising Abajo
- (void)					mgFixBottomEdge:(BOOL)fixed;
// autoresising Ancho
- (void)					mgFixWidth:(BOOL)fixed;
// autoresising Alto
- (void)					mgFixHeight:(BOOL)fixed;
// mover in the parent view.
- (void)					mgMoveInSuperView:(MGPositionInViewEnum)pTipo;
- (void)					mgMoveInSuperView:(MGPositionInViewEnum)pTipo AgregateHorizontal:(CGFloat)pflAgregateHorizontal AgregateVertical:(CGFloat)pflAgregateVertical;
// mover a una coordenadas x e y
- (void)					mgMoveLeft:(int)X AndTop:(int)Y;

// modifica el contentsize adaptandolo al contenido.
- (void)					mgLayoutSubviewsReload;
- (void)					mgLayoutSubviewsReloadIgnoreWidth:(BOOL)pWidth Height:(BOOL)pHeight;

// reordena las subvistas como hace linearscrollview.
- (CGSize)					mgRelocateContent;
- (CGSize)					mgRelocateContentAndIgnoreControls:(NSArray *)pControls;

// mover control a encima,debajo,atras o alante de pview y con separacion
- (void)					mgMoveToControl:(UIView *)pView Position:(MGPositionMoveEnum)pPosition Separation:(float)pSeparation;

// agrega Gesture Recognizer TAP
- (UIGestureRecognizer *)	mgAddGestureRecognizerTap:(id)target Action:(SEL)pAction;

// agrega Gesture Recognizer SWIPE
- (UIGestureRecognizer *)	mgAddGestureRecognizerSwipe:(id)target Action:(SEL)pAction Direccion:(UISwipeGestureRecognizerDirection)pDireccion;

/**
 Add gesture Long Press
 
 @param target  control to target gesture
 @param pAction Method to run with the user press long.
 
 @return the GestureRecognizer.
 */
- (UIGestureRecognizer *)	mgAddGestureRecognizerLongPress:(id)target Action:(SEL)pAction;

/**
 Remove all Gesture Recogniziers from UIView.
 */
- (void)					mgRemoveAllGestureRecognizers;

/**
 Add gesture Pan
 
 @param target  control to target gesture
 @param pAction Method to run with the user pag gesture.
 
 @return the GestureRecognizer.
 */
- (UIGestureRecognizer *)	mgAddGestureRecognizerPan:(id)target Action:(SEL)pAction;

@property (strong) UIColor *BorderColor;

@end
