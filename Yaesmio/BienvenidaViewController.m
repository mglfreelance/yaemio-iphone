//
//  BienvenidaViewController.m
//  yaesmio
//
//  Created by manuel garcia lopez on 06/05/13.
//  Copyright (c) 2013 manuel garcia lopez. All rights reserved.
//

#import "BienvenidaViewController.h"
#import "UIAllControls.h"


@interface BienvenidaViewController ()

@property (nonatomic) IBOutlet MKGradientButton *btnEntrar;
@property (nonatomic) IBOutlet MKGradientButton *btnQue;
@property (nonatomic) IBOutlet OBGradientView *VistaFondoDegradado;

- (IBAction)btnEntrar_click:(MKGradientButton *)sender;

@end

@implementation BienvenidaViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.btnEntrar setBorderColor:[Locator Default].Color.Blanco];
    [self.btnQue setBorderColor:[Locator Default].Color.Blanco];
    self.VistaFondoDegradado.colors = [Locator Default].Color.DegradadoFondo;
    Traducir(self.btnEntrar);
    Traducir(self.btnQue);
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewUnload
{
    [self setVistaFondoDegradado:nil];
    [self setBtnEntrar:nil];
    [self setBtnQue:nil];
}

- (IBAction)btnEntrar_click:(MKGradientButton *)sender
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [Locator Default].Preferencias.MostrarPantallaQR = true;
    [self dismissViewControllerAnimated:true completion:nil];
}

@end
