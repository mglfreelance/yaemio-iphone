//
//  EditarFiltroViewController.m
//  Yaesmio
//
//  Created by freelance on 22/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "EditarFiltroViewController.h"
#import "UIAllControls.h"

@interface EditarFiltroViewController ()

@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCampos;

@property (weak, nonatomic) IBOutlet UIButton *botonCategoria;
@property (weak, nonatomic) IBOutlet UIButton *botonMarca;
@property (weak, nonatomic) IBOutlet UIButton *botonPais;
@property (weak, nonatomic) IBOutlet UIButton *botonProvincia;
@property (weak, nonatomic) IBOutlet UIView *FondoSeleccionDatos;
@property (weak, nonatomic) IBOutlet UILabel *labelProvincia;
@property (weak, nonatomic) IBOutlet UIView *viewContenido;
@property (weak, nonatomic) IBOutlet UISwitch *SwitchRecomendadas;

- (IBAction)botonCategoriaExecute:(id)sender;
- (IBAction)botonMarcaExecute:(id)sender;
- (IBAction)botonProvinciaExecute:(id)sender;
- (IBAction)botonGuardarFiltroExecute:(id)sender;
- (IBAction)botonQuitarFiltroExecute:(id)sender;
- (IBAction)botonPaisExecute:(id)sender;

@property (strong) Empresa *EmpresaActual;
@property (strong) GrupoArticulo *CategoriaActual;
@property (strong) Pais *PaisActual;
@property (strong) Provincia *ProvinciaActual;

@end

@implementation EditarFiltroViewController

@synthesize EmpresaActual   = _EmpresaActual;
@synthesize CategoriaActual = _CategoriaActual;
@synthesize PaisActual      = _PaisActual;
@synthesize ProvinciaActual = _ProvinciaActual;

- (void)viewDidLoad
{
    [super viewDidLoad];
    Traducir(self.ListaCampos);
    
    self.CategoriaActual = self.Filtro.GrupoArticulo;
    self.EmpresaActual = self.Filtro.Empresa;
    self.PaisActual = self.Filtro.Pais;
    self.ProvinciaActual = self.Filtro.Provincia;
    
    [self.botonCategoria setBackgroundImage:[Locator Default].Imagen.FondoBotonSeleccionBlanco forState:UIControlStateNormal];
    [self.botonMarca setBackgroundImage:[Locator Default].Imagen.FondoBotonSeleccionBlanco forState:UIControlStateNormal];
    [self.botonPais setBackgroundImage:[Locator Default].Imagen.FondoBotonSeleccionBlanco forState:UIControlStateNormal];
    [self.botonProvincia setBackgroundImage:[Locator Default].Imagen.FondoBotonSeleccionBlanco forState:UIControlStateNormal];
    
    self.SwitchRecomendadas.on = self.Filtro.MostrarSoloRecomendadas;
    
    if (self.Filtro.TipoBusqueda != TipoProductoBusquedaCupon)
    {
        self.botonProvincia.hidden = true;
        self.labelProvincia.hidden = true;
        [self.FondoSeleccionDatos mgSizeFitsMaxWidth:UNCHANGED MaxHeight:174.f AgregateWidth:0.f AgregateHeight:12.f];
        [self.viewContenido mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT AgregateWidth:0.f AgregateHeight:20.f];
    }
}

- (void)viewUnload
{
    [super viewUnload];
    [[Locator Default].Carrito QuitarBotonCarrito:self];
    self.Filtro = nil;
    self.Resultado = nil;
    _EmpresaActual = nil;
    _CategoriaActual = nil;
    _PaisActual = nil;
    _ProvinciaActual = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
}

#pragma mark- Eventos Controles

- (IBAction)botonCategoriaExecute:(id)sender
{
    [[Locator Default].Vistas MostrarSeleccionCategoriaProducto:self Resultado: ^(GrupoArticulo *pResult)
     {
         if (pResult != nil)
         {
             self.CategoriaActual = pResult;
         }
     }];
}

- (IBAction)botonMarcaExecute:(id)sender
{
    [[Locator Default].Vistas MostrarSeleccionEmpresaBusqueda:self Resultado: ^(Empresa *pResult)
     {
         if (pResult != nil)
         {
             self.EmpresaActual = pResult;
         }
     }];
}

- (IBAction)botonProvinciaExecute:(id)sender
{
    [[Locator Default].Vistas MostrarSeleccionProvincia:self Pais:self.Filtro.Pais ProvinciaSeleccionada:nil MostrarTodas:true MostrarCercaDeMi:true Resultado: ^(Provincia *pResult)
     {
         if (pResult != nil)
         {
             self.ProvinciaActual = pResult;
         }
     }];
}

- (IBAction)botonPaisExecute:(id)sender
{
    TipoListaPaises tipo = (self.Filtro.TipoBusqueda == TipoProductoBusquedaCupon) ? TipoListaPaisesParaCupones : TipoListaPaisesGenerica;
    
    [[Locator Default].Vistas MostrarSeleccionPais:self Tipo:tipo PaisSeleccionado:nil Resultado: ^(Pais *pResult)
     {
         if (pResult != nil)
         {
             self.PaisActual = pResult;
         }
     }];
}

- (IBAction)botonGuardarFiltroExecute:(id)sender
{
    self.Filtro.Empresa = self.EmpresaActual;
    self.Filtro.GrupoArticulo = self.CategoriaActual;
    self.Filtro.Pais = self.PaisActual;
    self.Filtro.Provincia = self.ProvinciaActual;
    self.Filtro.MostrarSoloRecomendadas = self.SwitchRecomendadas.on;
    
    [Locator Default].Preferencias.PaisBusqueda = self.Filtro.Pais;
    [Locator Default].Preferencias.ProvinciaBusqueda = self.Filtro.Provincia;
    
    [self CerrarView:true];
}

- (IBAction)botonQuitarFiltroExecute:(id)sender
{
    self.EmpresaActual = nil;
    self.CategoriaActual = nil;
    self.ProvinciaActual = nil;
    self.PaisActual = nil;
    self.SwitchRecomendadas.on = false;
}

#pragma mark- Propiedades Internas

- (Provincia *)ProvinciaActual
{
    return _ProvinciaActual;
}

- (void)setProvinciaActual:(Provincia *)pProvinciaActual
{
    _ProvinciaActual = pProvinciaActual;
    self.botonProvincia.TextStateNormal = [self ComponerString:pProvinciaActual.Nombre];
}

- (Pais *)PaisActual
{
    return _PaisActual;
}

- (void)setPaisActual:(Pais *)pPaisActual
{
    _PaisActual = pPaisActual;
    self.botonPais.TextStateNormal = [self ComponerString:pPaisActual.Nombre];
}

- (Empresa *)EmpresaActual
{
    return _EmpresaActual;
}

- (void)setEmpresaActual:(Empresa *)pEmpresaActual
{
    _EmpresaActual = pEmpresaActual;
    self.botonMarca.TextStateNormal = [self ComponerString:pEmpresaActual.Nombre];
}

- (GrupoArticulo *)CategoriaActual
{
    return _CategoriaActual;
}

- (void)setCategoriaActual:(GrupoArticulo *)pCategoriaActual
{
    _CategoriaActual = pCategoriaActual;
    self.botonCategoria.TextStateNormal = [self ComponerString:pCategoriaActual.Nombre];
}

#pragma mark- Private

- (NSString *)ComponerString:(NSString *)pDato
{
    if (pDato == nil || [pDato isEqualToString:@""])
    {
        return Traducir(@"Todas");
    }
    
    return pDato;
}

- (void)CerrarView:(int)pValor
{
    [self.navigationController popViewControllerAnimated:true];
    
    if (self.Resultado != nil)
    {
        self.Resultado(pValor);
    }
}

@end
