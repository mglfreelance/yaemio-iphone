//
//  DestacadosViewController.m
//  Yaesmio
//
//  Created by freelance on 20/08/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "PaginaDestacadosViewController.h"
#import "UIAllControls.h"
#import "JSBadgeView.h"
#import "FiltroBusquedaProductos.h"
#import <UIKit/UIKit.h>
#import "ProductoDestacado.h"
#import "ProductoDestacadoCell.h"
#import "GLGooglePlusLikeLayout.h"

@interface PaginaDestacadosViewController () <UIImagePickerControllerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UINavigationControllerDelegate>
@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCamposTraducir;
@property (weak, nonatomic) IBOutlet UILabel *labelCarrito;
@property (weak, nonatomic) IBOutlet UICollectionView *TablaDestacados;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *Loading;


- (IBAction)MostrarCarritoCompra:(id)sender;

@property (strong) NSArray *ListaActual;
@property (assign) float AnchoEstandar;
@property (assign) NSInteger PaginaActual;
@property (assign) NSInteger NumeroPaginas;

@end

@implementation PaginaDestacadosViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [self.Loading stopAnimating];
    self.PaginaActual = 1;
    self.NumeroPaginas = 1;
    self.ListaActual = [Locator Default].Preferencias.ListaDestacados;
    self.AnchoEstandar = floorf((CGRectGetWidth(self.view.bounds) / 2));
    [self.TablaDestacados setCollectionViewLayout:[self CrearViewLayout]];
    self.navigationController.navigationBar.hidden = true;
    
    [[Locator Default].Contactos ComprobarAccesoAddressBook];
    
    self.view.backgroundColor = [Locator Default].Color.Transparente;
    
    Traducir(self.ListaCamposTraducir);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = true;
    self.labelCarrito.text = [NSString stringWithFormat:@"%d", [Locator Default].Carrito.NumeroProductosAgregados];
    
    [self LlamarMetodoListaDestacados];
}

- (void)viewDidDisappear:(BOOL)animated
{
    //para que no busque mas
}

- (void)viewUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self setListaActual:nil];
    [self setListaCamposTraducir:nil];
    
    [super viewUnload];
}

- (IBAction)MostrarCarritoCompra:(id)sender
{
    [[Locator Default].Vistas MostrarCarritoCompra:self];
}

#pragma mark- Delegate CollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.ListaActual.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductoDestacado *producto = self.ListaActual[indexPath.row];
    int fila = 1;
    int columna = 1;
    
    if (producto)
    {
        fila = producto.Filas;
        columna = producto.Columnas;
    }
    
    if (fila < 1)
    {
        fila = 1;
    }
    
    if (columna < 1)
    {
        columna = 1;
    }
    
    return CGSizeMake(self.AnchoEstandar * columna, self.AnchoEstandar * fila);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *myLista = self.ListaActual;
    
    if (myLista.count >= indexPath.row)
    {
        [[Locator Default].Alerta showBussy:Traducir(@"Cargando Producto.")];
        ProductoDestacado *pBuscado = myLista[indexPath.row];
        [[Locator Default].ServicioSOAP ObtenerProducto:pBuscado.ProductoId Medio:@"" Canal:TipoCanalProductoBusqueda Resultado: ^(SRResultObtenerProducto pResult, NSString *pMensajeError, ProductoPadre *pProductoPadre)
         {
             switch (pResult)
             {
                 case SRResultObtenerProductoNoYaesmio:
                 case SRResultObtenerProductoErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
                     
                 case SRResultObtenerProductoNoExiste:
                     [[Locator Default].Alerta showMessageAcept:pMensajeError];
                     break;
                     
                 case SRResultObtenerProductoYES:
                     [[Locator Default].Alerta hideBussy];
                     [[Locator Default].Vistas MostrarProductoModal:self Producto:pProductoPadre VieneDeFavoritos:false];
                     break;
             }
         }];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductoDestacadoCell *lVistaCelda = [collectionView dequeueReusableCellWithReuseIdentifier:@"Celda_ProductoDestacado" forIndexPath:indexPath];
    
    [lVistaCelda ActualizarConProducto:self.ListaActual[indexPath.row]];
    
    [self performSelector:@selector(PararAnimacionLoading) withObject:self afterDelay:1];
    
    return lVistaCelda;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsZero;
}

#pragma mark- Privado

-(void)PararAnimacionLoading
{
    [self.Loading stopAnimating];
}

- (void)LlamarMetodoListaDestacados
{
    if (self.Loading.isHidden)
    {
        [self.Loading startAnimating];
        [[Locator Default].ServicioSOAP ObtenerListaDestacados:self.PaginaActual Dispositivo:@"M" Result: ^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *ListaDestacados, NSInteger Paginas)
         {
             if (pResult == SRResultMethodYES)
             {
                 self.NumeroPaginas = Paginas;
                 self.ListaActual = [self CambiarTamaniosT1:ListaDestacados];
                 [Locator Default].Preferencias.ListaDestacados = self.ListaActual;
                 [self.TablaDestacados setHidden:self.ListaActual.count == 0];
                 
                 [UIView performWithoutAnimation:^
                  {
                      [self.TablaDestacados reloadSections:[NSIndexSet indexSetWithIndex:0]];
                  }];
                 self.PaginaActual++;
                 
                 if (self.PaginaActual > self.NumeroPaginas)
                 {
                     self.PaginaActual  = 1;
                 }
             }
         }];
    }
}

- (GLGooglePlusLikeLayout *)CrearViewLayout
{
    GLGooglePlusLikeLayout *layout = [[GLGooglePlusLikeLayout alloc] init];
    
    layout.minimumItemSize = CGSizeMake(self.AnchoEstandar, self.AnchoEstandar);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.interitemSpacing = 15.0f;
    layout.minimumLineSpacing = 0.0f;
    layout.interSectionSpacing = 15.0f;
    layout.minimumInteritemSpacing = 0.0f;
    return layout;
}

#pragma  mark- a Eliminar este codigo

- (NSArray *)CambiarTamaniosT1:(NSArray *)lista
{
    int i = 0;
    
    self.NumeroPaginas = 2;
    
    if (self.PaginaActual % 2)
    {
        for (ProductoDestacado *item in lista)
        {
            i++;
            switch (i)
            {
                case 1:
                    item.Filas = 2;
                    item.Columnas = 1;
                    break;
                    
                case 4:
                    item.Filas = 1;
                    item.Columnas = 2;
                    break;
                    
                case 5:
                    item.Filas = 2;
                    item.Columnas = 2;
                    break;
                    
                case 6:
                    item.Filas = 2;
                    item.Columnas = 1;
                    break;
                    
                case 7:
                    item.Filas = 2;
                    item.Columnas = 1;
                    break;
                    
                default:
                    item.Filas = 1;
                    item.Columnas = 1;
                    break;
            }
        }
    }
    else
    {
        for (ProductoDestacado *item in lista)
        {
            i++;
            switch (i)
            {
                case 1:
                    item.Filas = 2;
                    item.Columnas = 2;
                    break;
                    
                case 4:
                    item.Filas = 1;
                    item.Columnas = 2;
                    break;
                    
                case 5:
                    item.Filas = 1;
                    item.Columnas = 1;
                    break;
                    
                case 6:
                    item.Filas = 2;
                    item.Columnas = 1;
                    break;
                    
                case 7:
                    item.Filas = 1;
                    item.Columnas = 1;
                    break;
                    
                default:
                    item.Filas = 1;
                    item.Columnas = 1;
                    break;
            }
        }
    }
    
    
    return lista;
}

@end
