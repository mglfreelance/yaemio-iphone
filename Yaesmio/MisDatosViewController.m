//
//  MisDatosViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 25/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "MisDatosViewController.h"
#import "UIAllControls.h"
#import "EditarDatosUsuarioViewController.h"

@interface MisDatosViewController ()

@property (nonatomic) IBOutlet UITableViewCell *celdaDireccionEnvioPrincipal;
@property (nonatomic) IBOutlet UITableViewCell *celdaDireccionEnvioAlternativa;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *ListaCamposTraducir;

- (IBAction)CerrarModalExecute:(id)sender;

@property (strong) Usuario   *usuario;

@end

@implementation MisDatosViewController

- (void)ObtenerDatosUsuario
{
    [[Locator Default].Alerta showBussy:Traducir(@"Cargando Datos Usuario")];
    [[Locator Default].ServicioSOAP ObtenerUsuario:[Locator Default].Preferencias.IdUsuario Result:^(SRResultMethod pResult,NSString *pMensaje, Usuario *pUsuario)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 [[Locator Default].Alerta showMessageAcept:pMensaje];
                 break;
                 
             case SRResultMethodYES:
                 self.usuario = pUsuario;
                 [self MostrarOcultarDireccionesEnvio];
                 [[Locator Default].Alerta hideBussy];
                 break;
         }
     }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.celdaDireccionEnvioAlternativa.hidden = true;
    Traducir(self.ListaCamposTraducir);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self ObtenerDatosUsuario];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
}

- (void)viewDidUnload
{
    [self setCeldaDireccionEnvioPrincipal:nil];
    [self setCeldaDireccionEnvioAlternativa:nil];
    [self setListaCamposTraducir:nil];
    [super viewDidUnload];
}

- (IBAction)CerrarModalExecute:(id)sender
{
    [self CerrarModal];
}

- (void)MostrarPantallaDirEnvioPrincipal:(Direccion *)pDireccion
{
    [[Locator Default].Vistas MostrarEditarDireccionEnvioPrincipal:self Direccion:pDireccion EsVistaCarrito:false Result:nil];
}

- (void)MostrarPantallaDirEnvioAlternativa:(Direccion *)pDireccion
{
    [[Locator Default].Vistas MostrarEditarDireccionEnvioAlternativa:self Direccion:pDireccion EsVistaCarrito:false Result:nil];
}

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *lResultado = nil;
    
    if (indexPath.row == 2)
    {   // direcion principal
        if (self.usuario.ExisteDireccion1 == false)
        {
            [self MostrarPantallaDirEnvioPrincipal:nil];
        }
        else
        {
            [[Locator Default].Alerta showBussy:Traducir(@"Cargando dirección envio.")];
            [[Locator Default].ServicioSOAP ObtenerDirecionPrincipalUsuario:^(SRResultMethod pResult, Direccion *pDireccion)
             {
                 if ([self HaHabidoError:pResult] == false)
                 {
                     [[Locator Default].Alerta hideBussy];
                     [self MostrarPantallaDirEnvioPrincipal:pDireccion];
                 }
             }];
        }
    }
    else if (indexPath.row == 3)
    {   //direccion alternativa
        if (self.usuario.ExisteDireccion2 == false)
        {
            [self MostrarPantallaDirEnvioAlternativa:nil];
        }
        else
        {
            [[Locator Default].Alerta showBussy:Traducir(@"Cargando dirección envio.")];
            [[Locator Default].ServicioSOAP ObtenerDirecionAlternativaUsuario:^(SRResultMethod pResult, Direccion *pDireccion)
             {
                 if ([self HaHabidoError:pResult] == false)
                 {
                     [[Locator Default].Alerta hideBussy];
                     [self MostrarPantallaDirEnvioAlternativa:pDireccion];
                 }
             }];
        }
    }
    else
    {
        // los dos primeros no hay que obtener nada
        lResultado = indexPath;
    }
    
    return lResultado;
}

-(BOOL)HaHabidoError:(SRResultMethod)pResult
{
    BOOL lBolResultado = true;
    
    switch (pResult)
    {
        case SRResultMethodErrConex:
            [[Locator Default].Alerta showErrorConexSOAP];
            break;
            
        case SRResultMethodNO:
            [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible obtener la direccion de envio. Por favor, verifique su conexion a internet.")];
            break;
            
        case SRResultMethodYES:
            lBolResultado = false;
            break;
    }
    return lBolResultado;
}

-(void)MostrarOcultarDireccionesEnvio
{
    self.celdaDireccionEnvioAlternativa.hidden = !(self.usuario.ExisteDireccion2 || self.usuario.ExisteDireccion1);
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_editar_datos_usuario"])
    {
        EditarDatosUsuarioViewController *lview = segue.destinationViewController;
        lview.usuario = self.usuario;
    }
}

@end
