//
//  SeleccionDireccionEnvioViewController.h
//  Yaesmio
//
//  Created by freelance on 06/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "YaesmioViewControllers.h"
#import "Usuario.h"
#import "Direccion.h"

typedef void (^ResultadoSeleccionDireccionEnvioBlock)(Direccion *Direccion);

@interface SeleccionDireccionEnvioViewController : YaesmioTableViewController

-(void)configurarConUsuario:(Usuario*)usuario Resultado:(ResultadoSeleccionDireccionEnvioBlock)resultado;

@end
