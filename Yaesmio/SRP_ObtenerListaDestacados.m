//
//  SRP_ObtenerDestacados.m
//  Yaesmio
//
//  Created by freelance on 20/08/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SRP_ObtenerListaDestacados.h"
#import "ProductoDestacado.h"

@implementation SRP_ObtenerListaDestacados

- (id)init
{
    if ((self = [super init]))
    {
        self.Pagina  = 0;
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [@"0000000019" xmlNodeForDoc: node->doc elementName: @"usu" elementNSPrefix: @"ServicioRegistroSvc"]);
    
    xmlAddChild(node, [[NSString stringWithFormat:@"%d", 1] xmlNodeForDoc:node->doc elementName:@"posIni" elementNSPrefix:@"ServicioRegistroSvc"]);
    
    xmlAddChild(node, [[NSString stringWithFormat:@"%d", 100] xmlNodeForDoc:node->doc elementName:@"numElem" elementNSPrefix:@"ServicioRegistroSvc"]);
}

+ (SRP_ObtenerListaDestacados *)deserializeNode:(xmlNodePtr)cur
{
    SRP_ObtenerListaDestacados *newObject = [SRP_ObtenerListaDestacados new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"DevolverFavoritos"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IYaesmio/DevolverFavoritos";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_ObtenerListaDestacadosResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"DevolverFavoritosResponse";
}

@end


@implementation SRP_ObtenerListaDestacadosResponse

@synthesize MensajeSap = _MensajeSap;
@synthesize CodigoResultado = _CodigoResultado;
@synthesize NumeroFilas = _NumeroFilas;
@synthesize NumeroColumnas = _NumeroColumnas;
@synthesize PaginasTotales = _PaginasTotales;
@synthesize ListaDestacados = _ListaDestacados;

NSMutableArray *mLista;

- (id)init
{
    if ((self = [super init]))
    {
        _CodigoResultado = 0;
        _MensajeSap = @"";
        _NumeroColumnas = 0;
        _NumeroFilas = 0;
    }
    
    return self;
}

+ (SRP_ObtenerListaDestacadosResponse *)deserializeNode:(xmlNodePtr)cur
{
    SRP_ObtenerListaDestacadosResponse *newObject = [SRP_ObtenerListaDestacadosResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"DevolverFavoritosResult"])
    {
        _ListaDestacados = [NSArray deserializeNode:pobjCur toClass:[ProductoDestacado class]];
    }
}

@end
