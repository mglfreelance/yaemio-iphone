//
//  ProductoBusquedaCell.m
//  Yaesmio
//
//  Created by freelance on 09/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ProductoBusquedaCell.h"
#import "UIAllControls.h"

@interface ProductoBusquedaCell ()

@property (weak, nonatomic) IBOutlet UIImageView *ImagenProducto;
@property (weak, nonatomic) IBOutlet UILabel *LabelNombreProducto;
@property (weak, nonatomic) IBOutlet UILabel *LabelNombreEmpresa;
@property (weak, nonatomic) IBOutlet UILabel *labelMoneda;
@property (weak, nonatomic) IBOutlet UILabel *LabelPrecio;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *IndicatorBusy;
@property (weak, nonatomic) IBOutlet UIImageView *ImagenRecomendado;
@property (weak, nonatomic) IBOutlet UIView *viewDescuento;
@property (weak, nonatomic) IBOutlet UILabel *labelDescuento;
@property (weak, nonatomic) IBOutlet UILabel *labelDistancia;
@property (weak, nonatomic) IBOutlet UIView *viewImagen;

@property (strong) NSString *IdImagen;

@end

@implementation ProductoBusquedaCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        // Initialization code
    }
    
    return self;
}

- (void)ActualizarConProducto:(ProductoBuscado *)pProducto MostrarImagenGrande:(BOOL)pImagenGrande MostrarDistancia:(BOOL)MostrarDistancia
{
    [self BorrarDatos];
    
    NSString *urlImagen = pImagenGrande ? pProducto.Imagen.UrlPath : pProducto.Imagen.UrlPathSmall;
    
    self.IdImagen = [self IdCarGaAsincronaProducto:pProducto];
    [self.ImagenProducto cargarUrlImagenAsincrono:urlImagen Busy:self.IndicatorBusy];
    self.LabelNombreProducto.text = pProducto.Nombre;
    self.LabelNombreEmpresa.text  = pProducto.NombreEmpresa;
    self.labelMoneda.text = pProducto.Moneda;
    self.LabelPrecio.text = [pProducto.Precio toStringFormaterCurrency];
    self.labelDescuento.text = [pProducto.Descuento toStringDiscount];
    self.labelMoneda.hidden = (pProducto.Precio.integerValue == 0);
    self.LabelPrecio.hidden = (pProducto.Precio.integerValue == 0);
    self.ImagenRecomendado.hidden = !pProducto.Recomendada;
    self.viewDescuento.hidden = (pProducto.Precio.isZeroOrLess);
    [self.LabelPrecio mgSizeFitsMaxWidth:100.0f MaxHeight:UNCHANGED];
    [self.labelMoneda mgSizeFitsMaxWidth:100.0f MaxHeight:UNCHANGED];
    [self.labelMoneda mgMoveToControl:self.LabelPrecio Position:MGPositionMoveToBehind Separation:3.0f];
    
    if (MostrarDistancia)
    {
        self.labelDistancia.hidden = (pProducto.Distancia == nil || pProducto.Distancia.doubleValue == 0.0);
        self.labelDistancia.text = [self CalcularDistancia:pProducto.Distancia];
    }
}

- (NSString *)IdCarGaAsincronaProducto:(ProductoBuscado *)pProducto
{
    return [NSString stringWithFormat:@"Busqueda_%@_%@", pProducto.IdProductoPadre, pProducto.IdProductoHijo];
}

- (void)BorrarDatos
{
    if (self.IdImagen != nil)
    {
        [[Locator Default].Download CancelAllDownloads:self.IdImagen];
    }
    
    self.ImagenProducto.image = nil;
    self.labelMoneda.text = @"";
    self.LabelNombreEmpresa.text = @"";
    self.LabelNombreProducto.text = @"";
    self.LabelPrecio.text = @"";
    self.ImagenRecomendado.hidden = true;
    self.labelDistancia.text = @"";
    self.labelDistancia.hidden = true;
}

- (NSString *)CalcularDistancia:(NSDecimalNumber*)distancia
{
    NSString *resultado = @"";
    double distanciaDouble = distancia.doubleValue;
    
    if ((1.0 - distanciaDouble) > 0)
    {
        resultado = [NSString stringWithFormat:Traducir(@"%d m"),(int)(distanciaDouble *1000.0)];
    }
    else
    {
        resultado = [NSString stringWithFormat:Traducir(@"%@ km"), [distancia toStringFormaterWithDecimals:1]];
    }
    
    
    return resultado;
}

@end
