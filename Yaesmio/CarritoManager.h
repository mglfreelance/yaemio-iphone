//
//  CarritoManager.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 16/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ElementoCarrito.h"
#import "EmpresaCarrito.h"
#import "ProductoPadre.h"
#import "Favorito.h"

@protocol CarritoManagerDelegate <NSObject>

-(void) CambiadoElementosCarrito;

@end

@interface CarritoManager : NSObject

-(void) agregarBotonCarrito:(UIViewController*)pView EsVistaCarrito:(BOOL)pEsCarrito;
-(void) QuitarBotonCarrito:(UIViewController*)pView;

@property (readonly)  int                        NumeroProductosAgregados;
@property (readonly)  int                        NumeroEmpresasAgregadas;
@property (readonly)  NSArray                    *EmpresasCarrito;
@property (nonatomic) id<CarritoManagerDelegate> delegate;

-(void) agregarProducto:(ProductoPadre*)pProducto Unidades:(NSDecimalNumber*)pUnidades;

- (NSArray *)obtenerListaProductosPorEmpresa:(NSString *)pIdEmpresa ExigeDireccionEnvio:(BOOL)pTieneDireccionEnvio;

-(int) obtenerNumeroProductosPorEmpresa:(NSString*)pIdEmpresa ExigeDireccionEnvio:(BOOL)pTieneDireccionEnvio;

-(BOOL) QuitarElemento:(ElementoCarrito*)pElemento;

-(bool) ModificarProducto:(ElementoCarrito*)pActual Modificado:(ProductoPadre*)pProductoCarrito Unidades:(NSDecimalNumber*)pUnidades;

-(bool) QuitarEmpresa:(EmpresaCarrito*)pEmpresa;

-(BOOL) ExisteProductoEnCarrito:(ProductoPadre*)pProducto;

-(BOOL) ExisteFavoritoEnCarrito:(Favorito*)pFavorito;

-(void) ActualizarEstado;

-(BOOL) sePuedeAgregarUnidadesProducto:(ProductoPadre*)pProducto Unidades:(NSDecimalNumber*)pUnidades;

-(BOOL) sePuedeAgregarUnidadesFavorito:(Favorito*)pProducto Unidades:(NSDecimalNumber*)pUnidades;

@end
