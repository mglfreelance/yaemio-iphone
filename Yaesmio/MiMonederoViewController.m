//
//  MiMonederoViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "MiMonederoViewController.h"
#import "UIAllControls.h"

@interface MiMonederoViewController ()

@property (nonatomic) IBOutlet UILabel          *labelCabecera;
@property (nonatomic) IBOutlet UILabel          *labelDescripcionCabecera;
@property (nonatomic) IBOutlet UILabel          *labelTienesAcumulados;
@property (nonatomic) IBOutlet UILabel          *labelEurosAcumulados;
@property (nonatomic) IBOutlet MKGradientButton *btnSeguirAcumulando;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *ListaCamposTraducir;

- (IBAction)btnSeguirAcumulando_click:(id)sender;

@end

@implementation MiMonederoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.labelEurosAcumulados.text = @"";
    Traducir(self.ListaCamposTraducir);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
    
    [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo euros yaesmio!")];
    [[Locator Default].ServicioSOAP ConsultarEurosYaesmioPorEmail:[Locator Default].Preferencias.MailUsuario Result:^(SRResultMethod pResult, NSNumber *pEuros)
    {
        switch (pResult)
        {
            case SRResultMethodErrConex:
                [[Locator Default].Alerta showErrorConexSOAP];
                break;
                
            case SRResultMethodNO:
                [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible obtener sus euros yaesmio!. Por favor, revise su conexion a internet.")];
                break;
                
            case SRResultMethodYES:
                self.labelEurosAcumulados.text = [NSString stringWithFormat:@"¡%@ euros!",pEuros];
                [[Locator Default].Alerta hideBussy];
                break;
        }
    }];
}

- (void)viewUnload
{
    [self setLabelCabecera:nil];
    [self setLabelDescripcionCabecera:nil];
    [self setLabelTienesAcumulados:nil];
    [self setLabelEurosAcumulados:nil];
    [self setBtnSeguirAcumulando:nil];
    [self setListaCamposTraducir:nil];
}

- (IBAction)btnSeguirAcumulando_click:(id)sender
{
    
}

@end
