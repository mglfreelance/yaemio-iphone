//
//  FontManager.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 29/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FontManager : NSObject

@property (readonly) UIFont *Normal;
@property (readonly) UIFont *Cabecera;
@property (readonly) UIFont *CabeceraGrande;
@property (readonly) UIFont *Boton;
@property (readonly) UIFont *TabViewItem;
@property (readonly) UIFont *BotonTextoLargo;
@property (readonly) UIFont *Defecto16;
@property (readonly) UIFont *WebView;
@property (readonly) UIFont *PickerView;
@property (readonly) UIFont *PickerViewCabecera;
@property (readonly) UIFont *ProductoCabecera;
@property (readonly) UIFont *Defecto12;
@property (readonly) UIFont *Defecto14;

- (NSAttributedString *)getTextoJustificado:(NSString *)pTexto;
- (NSAttributedString *)getTextoTachado:(NSString *)pTexto;
- (NSAttributedString *)getTextoTachadoDerecha:(NSString *)pTexto;
- (NSAttributedString *)getTextoLink:(NSString *)pTexto;
- (NSAttributedString *)getTextoMail:(NSString *)pTexto Mail:(NSString *)pMail;
- (NSAttributedString *)getTextoSubrayado:(NSString *)Texto;

- (UIFont *)			FuenteNegrita:(GLfloat)Size;
- (UIFont *)			FuenteNormal:(GLfloat)Size;

- (NSString *)			getStringHtmlLink:(NSString *)ptexto Url:(NSString *)pUrl;
- (NSString *)			getStringHtmlLink:(NSString *)ptexto Url:(NSString *)pUrl AppendText:(NSString *)pAppendText;
- (NSAttributedString *)getTextoJustificado:(NSString *)pTexto Fuente:(UIFont *)pFuente;
- (NSString *)			getStringHtmlLink:(NSString *)ptexto Url:(NSString *)pUrl FontSize:(int)pSize;

@end
