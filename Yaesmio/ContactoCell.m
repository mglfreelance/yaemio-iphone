//
//  ContactoCell.m
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ContactoCell.h"
#import "UIAllControls.h"

@interface ContactoCell ()

@property (weak, nonatomic) IBOutlet UILabel *labelNombreContacto;
@property (weak, nonatomic) IBOutlet PAImageView *ImageContacto;

@property (assign) NSInteger row;
@property (assign) NSInteger section;
@property (copy) GenericViewCellBlock delegate;

@end

@implementation ContactoCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self inicializar];
    }
    
    return self;
}

- (void)inicializar
{
    self.labelNombreContacto.text = @"";
    [self.ImageContacto setImage:nil animated:NO];
}

- (void)ConfigurarConContactoGrupo:(ContactoGrupo *)pContacto IndexPath:(NSIndexPath *)pIndex Delegate:(GenericViewCellBlock)pDelegate
{
    [self inicializar];
    
    self.row = pIndex.row;
    self.section = pIndex.section;
    self.delegate = pDelegate;
    
    self.labelNombreContacto.text = [[Locator Default].Contactos getNombreContactoPorMail:pContacto.email];
    [self.ImageContacto setImage:[[Locator Default].Contactos getImagenContactoPorMail:pContacto.email] animated:YES];
    self.ImageContacto.BorderColor = [Locator Default].Color.Principal;
}

- (void)EliminarContacto:(id)sender
{
    if (self.delegate != nil)
    {
        self.delegate(@"EliminarContacto", self.section, self.row);
    }
}

// Must implement this method either here or in the UIViewController
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    BOOL result = NO;
    
    if (action == @selector(EliminarContacto:))
    {
        result = YES;
    }
    
    return result;
}

@end
