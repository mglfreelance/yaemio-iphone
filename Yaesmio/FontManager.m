//
//  FontManager.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 29/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "FontManager.h"

#define FUENTE_ROBOTO			@"Roboto"
#define FUENTE_ROBOTO_NEGRITA	@"Roboto-Bold"


@implementation FontManager

- (UIFont *)Normal
{
    return [self FuenteNormal:14.0];
}

- (UIFont *)Defecto16
{
    return [self FuenteNormal:16.0];
}

- (UIFont *)Defecto12
{
    return [self FuenteNormal:12.0];
}

- (UIFont *)Defecto14
{
    return [self FuenteNormal:14.0];
}

- (UIFont *)Cabecera
{
    return [self FuenteNegrita:18.0];
}

- (UIFont *)CabeceraGrande
{
    return [self FuenteNegrita:20.0];
}

- (UIFont *)WebView
{
    return [self FuenteNegrita:11.0];
}

- (UIFont *)Boton
{
    return [self FuenteNormal:14.0];
}

- (UIFont *)BotonTextoLargo
{
    return [self FuenteNegrita:12.0];
}

- (UIFont *)TabViewItem
{
    return [self FuenteNormal:12.0];
}

- (UIFont *)PickerView
{
    return [self FuenteNormal:18.0];
}

- (UIFont *)PickerViewCabecera
{
    return [self FuenteNegrita:24.0];
}

- (UIFont *)ProductoCabecera
{
    return [self FuenteNormal:34.0];
}

- (UIFont *)FuenteNegrita:(GLfloat)Size
{
    return [UIFont fontWithName:FUENTE_ROBOTO_NEGRITA size:Size];
}

- (UIFont *)FuenteNormal:(GLfloat)Size
{
    return [UIFont fontWithName:FUENTE_ROBOTO size:Size];
}

- (NSAttributedString *)getTextoJustificado:(NSString *)pTexto
{
    NSMutableParagraphStyle *result = [[NSMutableParagraphStyle alloc] init];
    
    result.alignment = NSTextAlignmentJustified;
    result.lineBreakMode = NSLineBreakByWordWrapping;
    result.lineHeightMultiple = 0.8;
    return [[NSAttributedString alloc] initWithString:pTexto
                                           attributes:
            @{
              NSFontAttributeName: [Locator Default].FuenteLetra.TabViewItem,
              NSStrokeColorAttributeName: [UIColor blackColor],
              NSParagraphStyleAttributeName: result
              }];
}

- (NSAttributedString *)getTextoJustificado:(NSString *)pTexto Fuente:(UIFont *)pFuente
{
    NSMutableParagraphStyle *result = [[NSMutableParagraphStyle alloc] init];
    
    result.alignment = NSTextAlignmentJustified;
    result.lineBreakMode = NSLineBreakByWordWrapping;
    result.lineHeightMultiple = 1;
    return [[NSAttributedString alloc] initWithString:pTexto attributes:
            @{
              NSFontAttributeName: pFuente,
              NSStrokeColorAttributeName: [UIColor blackColor],
              NSParagraphStyleAttributeName: result
              }];
}

- (NSAttributedString *)getTextoTachadoDerecha:(NSString *)pTexto
{
    return [self getTextoTachado:pTexto Posicion:NSTextAlignmentRight];
}

- (NSAttributedString *)getTextoTachado:(NSString *)pTexto
{
    return [self getTextoTachado:pTexto Posicion:NSTextAlignmentLeft];
}

- (NSAttributedString *)getTextoTachado:(NSString *)pTexto Posicion:(NSTextAlignment)pPosicion
{
    NSMutableParagraphStyle *result = [[NSMutableParagraphStyle alloc] init];
    
    result.alignment = pPosicion;
    result.lineBreakMode = NSLineBreakByTruncatingTail;
    return [[NSAttributedString alloc] initWithString:pTexto
                                           attributes:
            @{
              NSFontAttributeName: [Locator Default].FuenteLetra.TabViewItem,
              NSStrokeColorAttributeName: [UIColor blackColor],
              NSParagraphStyleAttributeName: result,
              NSStrikethroughStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]
              }];
}

- (NSAttributedString *)getTextoLink:(NSString *)pTexto
{
    NSMutableParagraphStyle *result = [[NSMutableParagraphStyle alloc] init];
    
    result.alignment = NSTextAlignmentLeft;
    result.lineBreakMode = NSLineBreakByTruncatingTail;
    return [[NSAttributedString alloc] initWithString:pTexto
                                           attributes:
            @{
              NSFontAttributeName: [Locator Default].FuenteLetra.TabViewItem,
              NSStrokeColorAttributeName: [UIColor blueColor],
              NSParagraphStyleAttributeName: result,
              NSUnderlineStyleAttributeName: [NSNumber numberWithInt:NSUnderlineStyleSingle]
              }];
}

- (NSAttributedString *)getTextoMail:(NSString *)pTexto Mail:(NSString *)pMail
{
    NSMutableParagraphStyle *result = [[NSMutableParagraphStyle alloc] init];
    
    result.alignment = NSTextAlignmentLeft;
    result.lineBreakMode = NSLineBreakByTruncatingTail;
    NSString *text = [NSString stringWithFormat:pTexto, pMail];
    NSMutableAttributedString *mutable = [[NSMutableAttributedString alloc] initWithString:text attributes:@{
                                                                                                             NSFontAttributeName: [Locator Default].FuenteLetra.Cabecera,
                                                                                                             NSStrokeColorAttributeName: [Locator Default].Color.Texto,
                                                                                                             NSParagraphStyleAttributeName: result
                                                                                                             }];
    [mutable addAttribute:NSForegroundColorAttributeName value:[Locator Default].Color.Principal range:[text rangeOfString:pMail]];
    
    
    return mutable;
}

- (NSString *)getStringHtmlLink:(NSString *)ptexto Url:(NSString *)pUrl
{
    return [self getStringHtmlLink:ptexto Url:pUrl AppendText:@""];
}

- (NSString *)getStringHtmlLink:(NSString *)ptexto Url:(NSString *)pUrl FontSize:(int)pSize
{
    NSString *result = @"";
    
    if (ptexto.isNotEmpty && pUrl.isNotEmpty)
    {
        result = [NSString stringWithFormat:@"<a href='%@'><font face='%@' size=%d color=%@>%@</font></a>", pUrl, FUENTE_ROBOTO, pSize, [Locator Default].Color.PrincipalHexadecimal, ptexto];
    }
    
    return result;
}

- (NSString *)getStringHtmlLink:(NSString *)ptexto Url:(NSString *)pUrl AppendText:(NSString *)pAppendText
{
    NSString *result = [self getStringHtmlLink:ptexto Url:pUrl FontSize:11];
    
    if (pAppendText.isNotEmpty)
    {
        result = [result stringByAppendingString:pAppendText];
    }
    
    return result;
}

- (NSAttributedString *)getTextoSubrayado:(NSString *)Texto
{
    NSDictionary *underlineAttribute = @{
                                         NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
                                         };
    
    return [[NSAttributedString alloc] initWithString:Texto attributes:underlineAttribute];
}

@end
