//
//  Atributo.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 31/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoapObject.h"

@interface Atributo : SoapObject

+ (Atributo *)deserializeNode:(xmlNodePtr)cur;

- (id)init:(NSString*)pAtributoID Nombre:(NSString*)pNombreAtributo ValorID:(NSString*)pTraduccionID;

/* elements */
@property (strong) NSString * AtributoId;
@property (strong) NSString * NombreAtributo;
@property (strong) NSString * TraduccionId;

-(BOOL) comprobarObjeto;

@end
