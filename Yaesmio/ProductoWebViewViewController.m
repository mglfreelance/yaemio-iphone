//
//  ProductoWebViewViewController.m
//  Yaesmio
//
//  Created by freelance on 05/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ProductoWebViewViewController.h"

@interface ProductoWebViewViewController () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UILabel *labelTitulo;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *IndicatorBusy;

@end

@implementation ProductoWebViewViewController

- (void)viewUnload
{
    [self.webView Clear];
    [self setWebView:nil];
    [self setLabelTitulo:nil];
    [self setScrollView:nil];
    [self setIndicatorBusy:nil];
    [super viewUnload];
}

- (void)LoadDatos
{
    self.webView.delegate = self;
    self.webView.scalesPageToFit = false;
    self.webView.scrollView.scrollEnabled = false;
    self.webView.scrollView.bounces = false;
    [self.webView loadHTMLString:self.Producto.ProductoHijo.Descripcion2 baseURL:nil];
    self.webView.hidden = false;
    [self.webView mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT];
}

- (void)RellenarPantallaConDatosProducto
{
    Traducir(self.labelTitulo);
    
    [self.webView loadHTMLString:@" " baseURL:nil];
    [self.IndicatorBusy startAnimating];
    [self performSelector:@selector(LoadDatos) withObject:self afterDelay:2];
}

#pragma mark - WebView Delegate

- (void)webViewDidFinishLoad:(UIWebView *)aWebView
{
    [self.webView mgSetSizeX:UNCHANGED andY:UNCHANGED andWidth:UNCHANGED andHeight:3];
    [self.webView mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT];
    [self.scrollView mgRelocateContent];// recoloco todas las subvistas
    [self.scrollView setHeightContentSizeForSubviewUntilControl:self.webView];// calculo alto scroll
    [self.IndicatorBusy stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"%@", error);
    [self.IndicatorBusy stopAnimating];
}

- (BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
    if (inType == UIWebViewNavigationTypeLinkClicked)
    {
        [[Locator Default].Vistas MostrarWebView:self url:inRequest.URL.absoluteString];
        return NO;
    }
    
    return YES;
}

@end
