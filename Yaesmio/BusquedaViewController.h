//
//  BusquedaViewController.h
//  Yaesmio
//
//  Created by freelance on 08/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "YaesmioViewControllers.h"
#import "FiltroBusquedaProductos.h"

@interface BusquedaViewController : YaesmioViewController

@property (strong) FiltroBusquedaProductos *Filtro;

@end
