//
//  TarjetaUsuario.h
//  Yaesmio
//
//  Created by freelance on 09/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"

typedef NS_ENUM (NSInteger, TipoTarjetaUsuario)
{
    TipoTarjetaUsuarioNinguna    = 0,
    TipoTarjetaUsuarioVisa       = 1,
    TipoTarjetaUsuarioMasterCard = 2,
    TipoTarjetaUsuarioPaypal     = 3,
    TipoTarjetaUsuarioOtra       = 4,
};

typedef NS_ENUM (NSInteger, ModoTarjetaUsuario)
{
    ModoTarjetaUsuarioNinguna   = 0,
    ModoTarjetaUsuarioDebito    = 1,
    ModoTarjetaUsuarioCredito   = 2,
};

@interface TarjetaUsuario : SoapObject

+ (TarjetaUsuario *) deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, assign) TipoTarjetaUsuario TipoTarjeta;
@property (nonatomic, assign) ModoTarjetaUsuario ModoTarjeta;
@property (nonatomic, strong) NSDate *FechaUltimoUso;
@property (nonatomic, strong) NSString *Mes;
@property (nonatomic, strong) NSString *Anio;
@property (nonatomic, strong) NSString *Referencia;
@property (nonatomic, strong) NSString *NumeroTarjeta;

+(TarjetaUsuario*)Ninguna;
+(TarjetaUsuario*)Paypal;

@end
