//
//  CompartirViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 07/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAllControls.h"
#import "ProductoPadre.h"

@interface CompartirViewController : TDSemiModalViewController

@property (strong) ProductoPadre            *producto;
@property (copy)   ResultadoPorDefectoBlock Respond;
@end
