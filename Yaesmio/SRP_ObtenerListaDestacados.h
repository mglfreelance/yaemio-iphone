//
//  SRP_ObtenerDestacados.h
//  Yaesmio
//
//  Created by freelance on 20/08/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SRP_ObtenerListaDestacados : SoapMethod

+ (SRP_ObtenerListaDestacados *)		deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (assign) NSInteger Pagina;
@property (strong) NSString *TipoDispositivo;

@end



@interface SRP_ObtenerListaDestacadosResponse : SoapMethod

+ (SRP_ObtenerListaDestacadosResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (readonly) NSArray *ListaDestacados;
@property (readonly) int CodigoResultado;
@property (readonly) NSString *MensajeSap;
@property (readonly) int NumeroFilas;
@property (readonly) int NumeroColumnas;
@property (readonly) int PaginasTotales;

@end
