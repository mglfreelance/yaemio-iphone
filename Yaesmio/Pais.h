//
//  Pais.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ElementoPicker.h"

@interface Pais : ElementoPicker

+(Pais *) deserializeNode:(xmlNodePtr)cur;
+(Pais *) newWithId:(NSString*)pID andName:(NSString*)pName;

@end

