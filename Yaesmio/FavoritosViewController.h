//
//  FavoritosViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 20/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YaesmioViewControllers.h"

@interface FavoritosViewController : YaesmioViewController

@property (assign) BOOL RefrescarLista;

@end
