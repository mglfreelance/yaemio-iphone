//
//  ProductoCarritoCell.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ElementoCarrito.h"

typedef void (^CambiadoProductoCarritoCellBlock)(ElementoCarrito __weak *Elemento);

@interface ProductoCarritoCell : UITableViewCell

@property(copy) CambiadoProductoCarritoCellBlock DelegateCambiadaInformacionCelda;

-(void)Configurar:(UIViewController*)pView Elemento:(ElementoCarrito*)pElemento menu:(id)Menu;

@end
