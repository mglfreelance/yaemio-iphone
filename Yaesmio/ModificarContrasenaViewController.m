//
//  ModificarContrasenaViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 26/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ModificarContrasenaViewController.h"
#import "UIAllControls.h"

@interface ModificarContrasenaViewController ()

@property (nonatomic) IBOutlet UILabel            *labelCabecera;
@property (nonatomic) IBOutlet UILabel            *labelDescripcionCabecera;
@property (nonatomic) IBOutlet UITextField        *txtContrasena;
@property (nonatomic) IBOutlet UITextField        *txtRepetirContrasena;
@property (nonatomic) IBOutlet UILabel            *labelContrasena;
@property (nonatomic) IBOutlet UILabel            *labelRepetirContrasena;
@property (nonatomic) IBOutlet MKGradientButton   *btnGuardarContrasena;
@property (nonatomic) IBOutlet UILabel            *labelAnteriorContrasena;
@property (nonatomic) IBOutlet UITextField        *txtAnteriorContrasena;
@property (nonatomic) IBOutlet UIScrollView *scrollViewDatos;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *ListaCamposTraduccion;

- (IBAction)btnGuardarContrasena_click:(id)sender;
@property (nonatomic) ScrollManager *manager;

@end

@implementation ModificarContrasenaViewController

typedef NS_ENUM(NSInteger, ComprobarModificarContrasenaEnum)
{
    ComprobarModificarContrasenaEnumOK               = 0,
    ComprobarModificarContrasenaEnumClavesDiferentes = 1,
    ComprobarModificarContrasenaEnumClaveIncorrecta  = 2,
};

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaCamposTraduccion);
    
    self.manager = [ScrollManager NewAndConfigureWithScrollView:self.scrollViewDatos AndDidEndTextField:self Selector:@selector(btnGuardarContrasena_click:)];
    
    self.txtContrasena.text = @"";
    self.txtRepetirContrasena.text = @"";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
}

- (void)viewUnload
{
    [self setLabelCabecera:nil];
    [self setLabelDescripcionCabecera:nil];
    [self setTxtContrasena:nil];
    [self setTxtRepetirContrasena:nil];
    [self setLabelContrasena:nil];
    [self setBtnGuardarContrasena:nil];
    [self setLabelRepetirContrasena:nil];
    [self setLabelAnteriorContrasena:nil];
    [self setTxtAnteriorContrasena:nil];
    [self setScrollViewDatos:nil];
    [self setListaCamposTraduccion:nil];
    [self setManager:nil];
}

#pragma mark- Eventos Privados

- (IBAction)btnGuardarContrasena_click:(id)sender
{
    switch ([self comprobarCampos])
    {
        case ComprobarModificarContrasenaEnumClaveIncorrecta:
            [[Locator Default].Alerta showMessageAceptWithTitle:Traducir(@"¡Ups!") AndMessage:Traducir(@"La contraseña introducida no es correcta. Recuerda que debe de tener entre 8 y 15 caracteres y al menos una letra y un numero.")];
            break;
            
        case ComprobarModificarContrasenaEnumClavesDiferentes:
            [[Locator Default].Alerta showMessageAceptWithTitle:Traducir(@"¡Ups!") AndMessage:Traducir(@"Las contraseñas que has introducido no coinciden.")];
            break;
            
        case ComprobarModificarContrasenaEnumOK:
            [[Locator Default].Alerta showBussy:Traducir(@"Guardando contraseña")];
            [[Locator Default].ServicioSOAP EditarPassword:[Locator Default].Preferencias.MailUsuario ConPasswordAntigua:self.txtAnteriorContrasena.text YPasswordNuevo:self.txtContrasena.text Result:^(SRResultEditPassword pResult)
             {
                 switch (pResult)
                 {
                     case SRResultEditPasswordErrConex:
                         [[Locator Default].Alerta showErrorConexSOAP];
                         break;
                         
                     case SRResultEditPasswordNewPasswordIncorrect:
                         [[Locator Default].Alerta showMessageAcept:Traducir(@"La nueva contraseña no es correcta.")];
                         break;
                         
                     case SRResultEditPasswordOldPasswordIncorrect:
                         [[Locator Default].Alerta showMessageAcept:Traducir(@"La contraseña antigua no es correcta.")];
                         break;
                         
                     case SRResultEditPasswordYES:
                         [[Locator Default].Alerta showMessageAcept:Traducir(@"La contraseña ha sido guardada correctamente.")];
                         [[Locator Default].Preferencias GuardarUsuario:[Locator Default].Preferencias.IdUsuario NombreUsuario:[Locator Default].Preferencias.NombreUsuario Mail:[Locator Default].Preferencias.MailUsuario ConPassword:self.txtContrasena.text];
                         // guardo la contraseña nueva
                         [self.navigationController popViewControllerAnimated:true];
                         break;
                 }
             }];
            break;
    }
}

#pragma  mark- Metodos Privados

-(ComprobarModificarContrasenaEnum) comprobarCampos
{
    if ([[Locator Default].ExpresionesRegulares ComprobarPassword:self.txtContrasena.text] == false)
    {
        return ComprobarModificarContrasenaEnumClaveIncorrecta;
    }
    
    if ([self.txtContrasena.text isEqualToString:self.txtRepetirContrasena.text] == false)
    {
        return ComprobarModificarContrasenaEnumClavesDiferentes;
    }
    
    return ComprobarModificarContrasenaEnumOK;
}

@end
