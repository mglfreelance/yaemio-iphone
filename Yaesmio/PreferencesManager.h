//
//  PreferencesManager.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 09/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Pais.h"
#import "Provincia.h"
#import "UIImage+Ampliado.h"


extern NSString *const PreferencesKeyNombreUsuario;
extern NSString *const PreferencesKeyNumeroNotificaciones;
extern NSString *const PreferencesKeyLogadoUsuario;

@interface PreferencesManager : NSObject

@property (readonly) BOOL ExisteUsuario;
@property (assign)   BOOL EstaUsuarioLogado;
@property (strong)   NSString *MailUsuario;
@property (readonly) NSString *Password;
@property (readonly) NSString *IdUsuario;
@property (assign)   BOOL MostrarPantallaQR;
@property (assign)   BOOL GeolocalizacionActiva;
@property (assign)   BOOL AutoLoginUsuario;
@property (readonly) BOOL EsPosibleAutologar;
@property (assign)   BOOL CompletadoDatosUsuario;
@property (assign)   int NumeroVecesAMostrarMensajeCompletarUsuario;
@property (strong)   NSString *UrlImagenFondoPantallaPrincipal;
@property (strong)   NSString *IdPush;
@property (assign)   BOOL EscucharSonidoAlRecibirNotificacion;
@property (strong)   NSString *NombreUsuario;
@property (assign)   NSInteger NumeroNotificacionesSinLeer;
@property (readonly) NSString *IdApp;
@property (strong)   NSString *urlPasarelaPagoWeb;
@property (strong)   NSString *IdPaypal;
@property (assign)   NSInteger NumeroComprasNoValoradas;
@property (strong)   Pais *PaisBusqueda;
@property (strong)   Provincia *ProvinciaBusqueda;
@property (strong)   UIImage *ImagenUsuario;
@property (strong)   NSArray *ListaDestacados;


- (void)GuardarUsuario:(NSString *)pIdUsuario NombreUsuario:(NSString *)pNombre Mail:(NSString *)pMail ConPassword:(NSString *)pPassword;

@end
