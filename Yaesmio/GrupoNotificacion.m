//
//  GrupoNotificacion.m
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "GrupoNotificacion.h"
#import "ContactoGrupo.h"

@implementation GrupoNotificacion

- (id)init
{
    if ((self = [super init]))
    {
        self.NombreGrupo      = @"";
        self.IdGrupo          = @"";
        self.ListaContactos   = nil;
        self.IdUsuarioCreador = @"";
    }
    
    return self;
}

- (GrupoNotificacion *)BuscarEnLista:(NSArray *)pLista
{
    for (GrupoNotificacion *actual in pLista)
    {
        if ([actual.IdGrupo isEqualToString:self.IdGrupo])
        {
            return actual;
        }
    }
    
    return nil;
}

+ (GrupoNotificacion *)deserializeNode:(xmlNodePtr)cur
{
    GrupoNotificacion *newObject = [[GrupoNotificacion alloc] init];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"Grupo"])
    {
        self.NombreGrupo = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"IdGrupo"])
    {
        self.IdGrupo = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"IdUsuario"])
    {
        self.IdUsuarioCreador = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ListadoContactos"])
    {
        self.ListaContactos = [NSArray deserializeNode:pobjCur toClass:[ContactoGrupo class]];
    }
}

@end
