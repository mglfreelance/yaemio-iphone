//
//  MKGradientButton+Ampliado.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 16/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "MKGradientButton+Ampliado.h"

@implementation MKGradientButton (Ampliado)


-(void) setStyleWithGradient:(NSArray*)pArrayGradient andBorderColor:(UIColor*) pColor andBorderRadius:(float) pRadius
{
    self.gradientArray = pArrayGradient;
    self.borderColor =pColor;
    self.BorderRadius = pRadius;
}

-(void) setStyleWithBackgroundColor:(UIColor*)pColor andBorderColor:(UIColor*) pBorderColor andButtonColor:(UIColor*) pButtonColor andBorderRadius:(float) pRadius
{
    self.borderColor = pBorderColor;
    self.backgroundColor = pColor;
    self.buttonColor =pButtonColor;
    self.BorderRadius =pRadius;
}


@end
