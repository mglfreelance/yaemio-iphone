//
//  CarritoViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 16/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YaesmioViewControllers.h"
#import "EmpresaCarrito.h"

@interface CarritoEmpresaViewController : YaesmioViewController


@property (strong) EmpresaCarrito *Carrito;
@property (copy)  ResultadoBlock ActualizarTotal;
@property (strong) UIViewController *Padre; //no tiene navigation y no puede hacer ni modal ni mostrar por eso es necesario el padre que es la navegacion

-(void)Refrescar;

@end