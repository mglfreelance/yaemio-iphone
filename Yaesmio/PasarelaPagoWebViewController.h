//
//  PasarelaPagoWebViewController.h
//  Yaesmio
//
//  Created by freelance on 09/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "YaesmioViewControllers.h"
#import "EmpresaCarrito.h"

typedef void (^ResultadoPasarelaPagoWebBlock)(NSString *pIdPago);

@interface PasarelaPagoWebViewController : YaesmioViewController

-(void)ConfigurarConCarrito:(EmpresaCarrito*)carrito Referencia:(NSString*)referencia Resultado:(ResultadoPasarelaPagoWebBlock)resultado;

@end
