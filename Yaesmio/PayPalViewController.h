//
//  PayPalViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 03/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EmpresaCarrito.h"
#import "Usuario.h"
#import "Direccion.h"
#import "PayPalPaymentViewController.h"

@interface PayPalViewController : PayPalPaymentViewController

typedef void (^PayPalViewControllerBlock)(NSString *pResult);

@property (strong) EmpresaCarrito *Carrito;
@property (copy)   PayPalViewControllerBlock Respond;
@property (strong) NSString *MailEmpresa;
@property (strong) NSString *UrlWebEmpresa;
@property (strong) Usuario *Usuario;
@property (strong) Direccion *DireccionEnvio;

- (instancetype)initWithCarrito:(EmpresaCarrito*)pCarrito Usuario:(Usuario*)pUsuario;
@end
