//
//  Producto.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 31/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "Producto.h"
#import "Atributo.h"
#import "Media.h"

@implementation Producto

@synthesize ListaMediaVideo = _ListaMediaVideo;
@synthesize ListaMediaImagen = _ListaMediaImagen;

- (id)init
{
    if ((self = [super init]))
    {
        self.Moneda                 = @"";
        self.DescUnidadesMedida     = @"";
        self.Precio                 = nil;
        self.PrecioAnterior         = nil;
        self.ProductoId             = @"";
        self.Referencia             = @"";
        self.ListaMedia             = nil;
        self.Stock                  = @0;
        self.Atributos              = nil;
        self.Latitud                = nil;
        self.Longitud               = nil;
        self.ImagenPorDefecto       = Nil;
        self.Nombre                 = @"";
        self.Titulo1                = @"";
        self.Titulo2                = @"";
        self.Titulo3                = @"";
        self.Descripcion2           = @"";
        self.Descripcion3           = @"";
        self.Descripcion1           = @"";
        self.Tipo                   = TipoProductoOtro;
        self.Recomendada            = false;
        self.Descuento              = [NSDecimalNumber zero];
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Moneda xmlNodeForDoc:node->doc elementName:@"DescMoneda" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.DescUnidadesMedida xmlNodeForDoc:node->doc elementName:@"DescUnidadesMedida" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.Latitud xmlNodeForDoc:node->doc elementName:@"Latitud" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [@[self.Atributos] xmlNodeForDoc: node->doc elementName: @"ListaValoresAtributos" elementNSPrefix: @"tns3"]);
    xmlAddChild(node, [@[self.Longitud] xmlNodeForDoc: node->doc elementName: @"Longitud" elementNSPrefix: @"tns3"]);
    xmlAddChild(node, [self.Precio xmlNodeForDoc:node->doc elementName:@"Precio" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.PrecioAnterior xmlNodeForDoc:node->doc elementName:@"PrecioAnterior" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.ProductoId xmlNodeForDoc:node->doc elementName:@"ProductoId" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.Referencia xmlNodeForDoc:node->doc elementName:@"Referencia" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.ListaMedia xmlNodeForDoc:node->doc elementName:@"RutaImagen" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.Stock xmlNodeForDoc:node->doc elementName:@"Stock" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.Nombre xmlNodeForDoc:node->doc elementName:@"Nombre" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.Descripcion1 xmlNodeForDoc:node->doc elementName:@"Descripcion1" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.Descripcion2 xmlNodeForDoc:node->doc elementName:@"Descripcion2" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.Descripcion3 xmlNodeForDoc:node->doc elementName:@"Descripcion3" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.Titulo1 xmlNodeForDoc:node->doc elementName:@"Titulo1" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.Titulo2 xmlNodeForDoc:node->doc elementName:@"Titulo2" elementNSPrefix:@"tns3"]);
    xmlAddChild(node, [self.Titulo3 xmlNodeForDoc:node->doc elementName:@"Titulo3" elementNSPrefix:@"tns3"]);
}

+ (Producto *)deserializeNode:(xmlNodePtr)cur
{
    Producto *newObject = [Producto new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (BOOL)esPublicitario
{
    return self.Tipo == TipoProductoPublicitario;
}

- (BOOL)esCupon
{
    return self.Tipo == TipoProductoCupon;
}

- (BOOL)esCuponConPrecio
{
    return self.Tipo == TipoProductoCuponConPrecio;
}

- (BOOL)esUnico
{
    return self.Tipo == TipoProductoUnico;
}

- (BOOL)esConAtributos
{
    return self.Tipo == TipoProductoConAtributos;
}

- (BOOL)esProducto
{
    return (self.esConAtributos || self.esUnico || self.esCuponConPrecio);
}

- (BOOL)TieneVideos
{
    return (self.ListaMediaVideo != nil && self.ListaMediaVideo.count > 0);
}

- (BOOL)TieneImagenes
{
    return (self.ListaMediaImagen != nil && self.ListaMediaImagen.count > 0);
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    // NSLog(@"campo: %@",[NSString stringWithUTF8String: pobjCur->name]);
    
    if ([nodeName isEqualToString:@"DescMoneda"])
    {
        self.Moneda = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"DescUnidadesMedida"])
    {
        self.DescUnidadesMedida = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ListaValoresAtributos"])
    {
        if (pobjCur->children == nil)
        {
            self.Atributos = [ValoresdeAtributo new];
        }
        else
        {
            self.Atributos = [ValoresdeAtributo deserializeNode:pobjCur->children];
        }
    }
    else if ([nodeName isEqualToString:@"Precio"])
    {
        self.Precio =  [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"PrecioAnterior"])
    {
        self.PrecioAnterior = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ProductoId"])
    {
        self.ProductoId = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Latitud"])
    {
        self.Latitud = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Longitud"])
    {
        self.Longitud = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Referencia"])
    {
        self.Referencia = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"RutaImagen"])
    {
        self.ListaMedia = [NSArray deserializeNode:pobjCur toClass:[Media class]];
        _ListaMediaImagen = [Media FiltrarArray:self.ListaMedia PorTipo:MediaTypeMIME_image];
        _ListaMediaVideo = [Media FiltrarArray:self.ListaMedia PorTipo:MediaTypeMIME_video];
        self.ImagenPorDefecto = [self ObtenerPrimeroOrNill:self.ListaMediaImagen];
        self.VideoPorDefecto = [self ObtenerPrimeroOrNill:self.ListaMediaVideo];
    }
    else if ([nodeName isEqualToString:@"Stock"])
    {
        self.Stock = [NSNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Nombre"])
    {
        self.Nombre = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Descripcion1"])
    {
        self.Descripcion1 = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Descripcion2"])
    {
        self.Descripcion2 = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Descripcion3"])
    {
        self.Descripcion3 = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Titulo1"])
    {
        self.Titulo1 = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Recomendada"])
    {
        self.Recomendada = [SOAP_Boolean deserializeNode:pobjCur].boolValue;
    }
    else if ([nodeName isEqualToString:@"ZZDESCUENTO"])
    {
        self.Descuento = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Titulo2"])
    {
        self.Titulo2 = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Titulo3"])
    {
        self.Titulo3 = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"EsPublicitario"])
    {
        NSString *myTipo = [[NSString deserializeNode:pobjCur] lowercaseString];
        
        if ([myTipo isEqualToString:@"x"])
        {
            self.Tipo = TipoProductoPublicitario;
        }
        else if ([myTipo isEqualToString:@"c"])
        {
            self.Tipo = TipoProductoCupon;
        }
        else if ([myTipo isEqualToString:@"u"])
        {
            self.Tipo = TipoProductoUnico;
        }
        else if ([myTipo isEqualToString:@"a"])
        {
            self.Tipo = TipoProductoConAtributos;
        }
        else if ([myTipo isEqualToString:@"l"])
        {
            self.Tipo = TipoProductoCuponConPrecio;
        }
        else
        {
            self.Tipo = TipoProductoOtro;
        }
    }
}

- (Media *)ObtenerPrimeroOrNill:(NSArray *)pLista
{
    if (pLista != nil && pLista.count > 0)
    {
        return pLista.firstObject;
    }
    
    return nil;
}

- (BOOL)comprobarObjeto:(NSArray *)pListaAtributos ValoresdeAtributo:(NSArray *)pListaValores
{
    BOOL resul = true;
    
    resul = resul && (self.ListaMedia.count > 0);
    resul = resul && ([self comprobarAtributo:self.Atributos] || (self.Atributos != nil && [self.Atributos comprobarObjeto:pListaAtributos ValoresdeAtributo:pListaValores]));
    resul = resul && (self.esPublicitario || (self.ImagenPorDefecto != Nil && [self.ImagenPorDefecto comprobarObjeto]));
    resul = resul && ([self.ProductoId isEqualToString:@""] == false);
    resul = resul && [Media comprobarLista:self.ListaMedia];
    
    return resul;
}

- (BOOL)comprobarAtributo:(ValoresdeAtributo *)pAtributo
{
    BOOL resul = true;
    
    if (pAtributo != nil)
    {
        resul = [pAtributo.DescripcionAtt1 isEqualToString:@""];
    }
    
    return resul;
}

@end
