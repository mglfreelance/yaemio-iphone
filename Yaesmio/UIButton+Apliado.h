//
//  UIButton+Apliado.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 28/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (Apliado)

-(void) cargarFondoUrlImagenAsincrono:(NSURL*)pURl Grupo:(NSString*)pGrupo;
-(void) cargarFondoStringUrlImagenAsincrono:(NSString*)pUrl Grupo:(NSString*)pGrupo;

@property (nonatomic) NSString *TextStateNormal;
@property (nonatomic) UIImage  *BackgroundImageAllStates;
@property (nonatomic) UIImage  *ImageStateNormal;
@property (nonatomic) NSAttributedString *AttributedTextStateNormal;

@end
