//
//  MostrarImagenViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 05/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YaesmioViewControllers.h"

@interface GaleriaImagenesViewController : YaesmioViewController

@property (strong) NSArray  *ListaMedia;
@property (assign) int      NumeroImagenActual;
@property (strong) NSString *UrlImageEmpresa;

@end
