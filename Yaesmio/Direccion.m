//
//  Direccion.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "Direccion.h"

@implementation Direccion
- (id)init
{
	if((self = [super init]))
    {
		self.Bloque             = @"";
		self.Cod_postal         = @"";
		self.TextoDescripcion   = @"";
		self.Escalera           = @"";
		self.LaDireccion        = @"";
		self.Numero             = @"";
		self.Pais               = @"";
		self.Piso               = @"";
		self.Poblacion          = @"";
		self.Provincia          = @"";
		self.Puerta             = @"";
		self.Telefono           = @"";
        self.PaisID             = @"";
        self.ProvinciaID        = @"";
        self.nsPrefix           = @"tns3";
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Bloque xmlNodeForDoc:node->doc elementName:@"Bloque" elementNSPrefix:self.nsPrefix]);
	
    xmlAddChild(node, [self.Cod_postal xmlNodeForDoc:node->doc elementName:@"Cod_postal" elementNSPrefix:self.nsPrefix]);
	
	xmlAddChild(node, [self.TextoDescripcion xmlNodeForDoc:node->doc elementName:@"Comentarios" elementNSPrefix:self.nsPrefix]);
	
	xmlAddChild(node, [self.Escalera xmlNodeForDoc:node->doc elementName:@"Escalera" elementNSPrefix:self.nsPrefix]);
	
	xmlAddChild(node, [self.LaDireccion xmlNodeForDoc:node->doc elementName:@"LaDireccion" elementNSPrefix:self.nsPrefix]);
	
	xmlAddChild(node, [self.Numero xmlNodeForDoc:node->doc elementName:@"Numero" elementNSPrefix:self.nsPrefix]);
	
	xmlAddChild(node, [self.Pais xmlNodeForDoc:node->doc elementName:@"Pais" elementNSPrefix:self.nsPrefix]);
    
    xmlAddChild(node, [self.PaisID xmlNodeForDoc:node->doc elementName:@"PaisId" elementNSPrefix:self.nsPrefix]);
	
	xmlAddChild(node, [self.Piso xmlNodeForDoc:node->doc elementName:@"Piso" elementNSPrefix:self.nsPrefix]);
	
	xmlAddChild(node, [self.Poblacion xmlNodeForDoc:node->doc elementName:@"Poblacion" elementNSPrefix:self.nsPrefix]);
	
	xmlAddChild(node, [self.Provincia xmlNodeForDoc:node->doc elementName:@"Provincia" elementNSPrefix:self.nsPrefix]);
    
    xmlAddChild(node, [self.ProvinciaID xmlNodeForDoc:node->doc elementName:@"ProvinciaId" elementNSPrefix:self.nsPrefix]);
	
	xmlAddChild(node, [self.Puerta xmlNodeForDoc:node->doc elementName:@"Puerta" elementNSPrefix:self.nsPrefix]);
	
	xmlAddChild(node, [self.Telefono xmlNodeForDoc:node->doc elementName:@"Telefono" elementNSPrefix:self.nsPrefix]);  
}

+ (Direccion *)deserializeNode:(xmlNodePtr)cur
{
	Direccion *newObject = nil;
    
    if (cur->children != nil)
    {
        newObject = [Direccion new];
        
        [newObject deserializeAttributesFromNode:cur];
        [newObject deserializeElementsFromNode:cur];
    }
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"Bloque"])
    {
        self.Bloque = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Cod_postal"])
    {
        self.Cod_postal = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Comentarios"])
    {
        self.TextoDescripcion = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Escalera"])
    {
        self.Escalera = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"LaDireccion"])
    {
        self.LaDireccion = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Numero"])
    {
        self.Numero = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Pais"])
    {
        self.Pais = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Piso"])
    {
        self.Piso = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Poblacion"])
    {
        self.Poblacion = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Provincia"])
    {
        self.Provincia = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Puerta"])
    {
        self.Puerta = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Telefono"])
    {
        self.Telefono = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"ProvinciaId"])
    {
        self.ProvinciaID = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"PaisId"])
    {
        self.PaisID = [NSString deserializeNode:pobjCur];
    }
}


@end
