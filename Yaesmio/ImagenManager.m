//
//  ImageManager.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 17/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ImagenManager.h"
#import "UIImage+Ampliado.h"

@implementation ImagenManager

@synthesize ImagenFondoPantallaPrincipal = _ImagenFondoPantallaPrincipal;
@synthesize UrlImagenFondoPantallaPrincipal = _UrlImagenFondoPantallaPrincipal;

- (id)init
{
    if (self = [super init])
    {
        self.UrlImagenFondoPantallaPrincipal = @"";
    }
    
    return self;
}

- (void)Configurar
{
    self.UrlImagenFondoPantallaPrincipal = [Locator Default].Preferencias.UrlImagenFondoPantallaPrincipal;
}

- (UIImage *)FondoBotonSeleccionBlanco
{
    return [[UIImage imageNamed:@"FondoBoton"] resizableImageWithCapInsets:UIEdgeInsetsMake(5.0, 5.0, 5.0, 15.0)];
}

- (UIImage *)FondoTextFieldSeleccionado
{
    return [[UIImage imageNamed:@"FondoTextoResaltado"] resizableImageWithCapInsets:UIEdgeInsetsMake(5.0, 5.0, 5.0, 15.0)];
}

- (UIImage *)FondoTextField
{
    return [[UIImage imageNamed:@"FondoTexto"] resizableImageWithCapInsets:UIEdgeInsetsMake(5.0, 5.0, 5.0, 15.0)];
}

- (UIImage *)FondoTextFieldDesactivado
{
    return [[UIImage imageNamed:@"FondoBotonGris"] resizableImageWithCapInsets:UIEdgeInsetsMake(5.0, 5.0, 5.0, 15.0)];
}

- (UIImage *)Estrella
{
    UIImage *result = [UIImage imageNamed:@"star-template"];
    
    if ([result respondsToSelector:@selector(imageWithRenderingMode:)])
    {
        return [result imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    else
    {
        return result;
    }
}

- (UIImage *)EstrellaSeleccionada
{
    UIImage *result = [UIImage imageNamed:@"star-highlighted-template"];
    
    if ([result respondsToSelector:@selector(imageWithRenderingMode:)])
    {
        return [result imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    }
    else
    {
        return result;
    }
}

- (UIImage *)FondoBotonGris
{
    return [[UIImage imageNamed:@"FondoBotonGris"] resizableImageWithCapInsets:UIEdgeInsetsMake(5.0, 5.0, 5.0, 15.0)];
}

- (UIImage *)ImagenBotonCarrito
{
    return [UIImage imageNamed:@"botonCarrito"];
}

- (UIImage *)ImagenCarritoChico
{
    return [UIImage imageNamed:@"carrito"];
}

- (UIImage *)LedEncendido
{
    id resul = [UIImage imageNamed:@"led_encendido.png"];
    
    return resul;
}

- (UIImage *)LedApagado
{
    return [UIImage imageNamed:@"led_apagado.png"];
}

- (UIImage *)SinContacto
{
    return [UIImage imageNamed:@"sin_foto_usuario"];
}

- (UIImage *)SinImagen
{
    return [UIImage imageNamed:@"SinImagen"];
}

- (UIImage *)GrupoNotificacion
{
    return [UIImage imageNamed:@"grupo_notificacion"];
}

- (id)ComprobarImagen:(id)pImage
{
    if ([pImage isKindOfClass:[NSArray class]])
    {
        NSArray *pLista = pImage;
        
        if (pLista == nil || pLista.count == 0)
        {
            //devuelvo un array de una imagen
            return @[[self getNSurlSinImagen]];
        }
    }
    else if ([pImage isKindOfClass:[UIImage class]] && pImage == nil)
    {
        return [self SinImagen];
    }
    else if ([pImage isKindOfClass:[NSString class]] && pImage == nil)
    {
        return [self getNSurlSinImagen];
    }
    else if ([pImage isKindOfClass:[NSURL class]] && pImage == nil)
    {
        return [NSURL URLWithString:[self getNSurlSinImagen]];
    }
    
    return pImage;
}

- (NSString *)getNSurlSinImagen
{
    NSURL *MyURL = [[NSBundle mainBundle] URLForResource:@"SinImagen" withExtension:@"png"];
    
    return [MyURL absoluteString];
}

- (UIImage *)ImagenFondoPantallaPrincipal
{
    if (_ImagenFondoPantallaPrincipal == nil)
    {
        _ImagenFondoPantallaPrincipal = [UIImage imageNamed:@"fondo.jpg"];
    }
    
    return _ImagenFondoPantallaPrincipal;
}

- (void)setUrlImagenFondoPantallaPrincipal:(NSString *)pImagen
{
    [UIImage ImageAssyncFromUrl:[NSURL URLWithString:pImagen] Grupo:@"Background" Result: ^(UIImage *pResult)
     {
         if (pResult != nil)
         {
             _ImagenFondoPantallaPrincipal = pResult;
             [Locator Default].Preferencias.UrlImagenFondoPantallaPrincipal = pImagen;
         }
     }];
}

- (NSString *)UrlImagenFondoPantallaPrincipal
{
    return _UrlImagenFondoPantallaPrincipal;
}

- (UIImage *)Black
{
    CGSize imageSize = CGSizeMake(64, 64);
    UIColor *fillColor = [UIColor blackColor];
    
    UIGraphicsBeginImageContextWithOptions(imageSize, YES, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [fillColor setFill];
    CGContextFillRect(context, CGRectMake(0, 0, imageSize.width, imageSize.height));
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (UIImage *)ConvertBlackAndWhiteImage:(UIImage *)pImage
{
    CGColorSpaceRef colorSapce = CGColorSpaceCreateDeviceGray();
    CGContextRef context = CGBitmapContextCreate(nil, pImage.size.width,     pImage.size.height, 8, pImage.size.width, colorSapce, kCGBitmapByteOrderDefault);
    
    CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    CGContextSetShouldAntialias(context, NO);
    CGContextDrawImage(context, CGRectMake(0, 0, pImage.size.width, pImage.size.height), [pImage CGImage]);
    CGImageRef bwImage = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSapce);
    
    UIImage *resultImage = [UIImage imageWithCGImage:bwImage]; // This is result B/W image.
    CGImageRelease(bwImage);
    
    return resultImage;
}

@end
