//
//  PickerValoresAtributosViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 19/07/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAllControls.h"
#import "ValoresdeAtributo.h"

@interface PickerValoresAtributosViewController : TDSemiModalViewController

typedef void (^PickerValoresAtributoRespondBlock)(ValoresdeAtributo *pResult);

@property (nonatomic, strong) NSArray            *ListaElementos;
@property (nonatomic, strong) NSArray            *ListaNombreValores;
@property (copy)              PickerValoresAtributoRespondBlock Respond;
@property (nonatomic, strong) ValoresdeAtributo  *ElementoSeleccionado;

@end
