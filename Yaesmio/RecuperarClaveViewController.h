//
//  RecuperarClaveViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YaesmioViewControllers.h"

@interface RecuperarClaveViewController : YaesmioViewController

@property (copy) ResultadoPorDefectoBlock Resultado;
@property (nonatomic) UIViewController *VistaInicial;

@end
