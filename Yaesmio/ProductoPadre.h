//
//  ProductoHijo.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 31/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoapObject.h"
#import "Producto.h"
#import "Atributo.h"
#import "Media.h"
#import "EstadisticasProducto.h"
#import "TipoCanal.h"


@interface ProductoPadre : SoapObject

@property (strong)   NSNumber *CantidadMax;
@property (strong)   NSString *EmpresaId;
@property (strong)   NSNumber *DiasEntregaMax;
@property (strong)   NSString *ProductoId;
@property (strong)   Media *ImagenQR;
@property (strong)   Media *LogoEmpresa;
@property (strong)   NSString *UrlWebEmpresa;
@property (strong)   NSString *UrlCondicionesLegalesEmpresa;
@property (strong)   NSString *UrlPoliticaPrivacidadEmpresa;
@property (assign)   BOOL SinDireccionEnvio;
@property (strong)   NSString *NombreEmpresa;
@property (strong)   NSArray *ListaAtributos;
@property (strong)   NSArray *ListaValoresAtributos;
@property (strong)   Producto *ProductoHijo;
@property (strong)   NSString *Medio;
@property (strong)   TipoCanal *TipoCanal;
@property (strong)   EstadisticasProducto *Estadisticas;

// ampliado
@property (readonly) Atributo *Atributo1;
@property (readonly) Atributo *Atributo2;
@property (readonly) NSString *TextoPropiedades;
@property (readonly) NSString *TextoAtributos;
@property (readonly) NSString *TipoCanalString;

+ (ProductoPadre *) deserializeNode:(xmlNodePtr)cur;

- (ProductoPadre *) Clonar:(Producto *)pHijo;
- (BOOL)			comprobarObjeto;

@end
