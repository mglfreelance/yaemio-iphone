//
//  ServicioSOAP+Ampliado.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/07/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ServicioSOAP+Ampliado.h"
#include <ifaddrs.h>
#include <arpa/inet.h>

@implementation ServicioSOAP (Ampliado)

- (void)EjecutarMetodoAsincrono:(SoapObject *)aParameters delegate:(id<SoapBindingResponseDelegate>)responseDelegate
{
#ifdef SHOW_DEBUG_SOAP
    NSLog(@"soap init: %@", [NSDate new]);
#endif
    self.HeaderIP = [self getIPAddress];
    
    [self CallMethodAsyncUsingParameters:aParameters delegate:responseDelegate];
}

- (void)EjecutarMetodoAsincrono:(SoapMethod *)pMethod Response:(SoapMethodResponseBlock)pResponse
{
#ifdef SHOW_DEBUG_SOAP
    NSLog(@"soap init: %@", [NSDate new]);
#endif
    self.HeaderIP = [self getIPAddress];
    pMethod.SoapMethodResponse = pResponse;
    
    [self CallMethodAsyncUsingParameters:pMethod delegate:pMethod];
}

- (NSString *)getIPAddress
{
    NSString *myAddress = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *myAddressEN = @"";
    NSString *myAddress3G = @"";
    int success = 0;
    
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    
    if (success == 0)
    {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        
        while (temp_addr != NULL)
        {
            if (temp_addr->ifa_addr->sa_family == AF_INET)
            {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if ([[NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)] isEqualToString:@"127.0.0.1"] == false)
                {
                    if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"])
                    {
                        // Get NSString from C String
                        myAddressEN = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    }
                    else
                    {
                        myAddress3G = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    }
                    
#ifdef SHOW_DEBUG_SOAP
                    NSLog(@"%@->%@", [NSString stringWithUTF8String:temp_addr->ifa_name], [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)]);
#endif
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    
    if ([myAddressEN isEqualToString:@""] == false)
    {
        myAddress = myAddressEN;
    }
    else
    {
        myAddress = myAddress3G;
    }
    
    return myAddress;
}

@end
