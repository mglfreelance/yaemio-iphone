//
//  NotificacionProductoViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+TDSemiModalExtension.h"

@interface NotificacionProductoViewController : TDSemiModalViewController

typedef void (^ResultadoNotificacionBlock)();

+(void) MostrarNotificacion:(UIViewController*)pView Imagen:(NSString*)purlImage Descripcion:(NSString*)pMensaje Result:(ResultadoNotificacionBlock)pResult;

@property (strong) NSString *urlImagen;
@property (strong) NSString *Mensaje;
@property (copy) ResultadoNotificacionBlock Resultado;

@end
