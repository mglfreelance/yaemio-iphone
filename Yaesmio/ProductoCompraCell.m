//
//  ProductoCompraCell.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ProductoCompraCell.h"
#import "UIAllControls.h"

@interface ProductoCompraCell ()

@property (nonatomic) IBOutlet UILabel *labelNombreProducto;
@property (nonatomic) IBOutlet UILabel *labelPrecioProducto;
@property (nonatomic) IBOutlet UILabel *labelReferencia;
@property (nonatomic) IBOutlet UILabel *labelDescripcionProducto;
@property (weak, nonatomic) IBOutlet UIButton *btnImagenProducto;

- (IBAction)btnImagenProducto_click:(id)sender;

@property (nonatomic) UIViewController *myVista;
@property (nonatomic) ElementoCarrito *Elemento;
@end

@implementation ProductoCompraCell

#define GRUPO_PRODUCTOCOMPRACELL @"ProductoCompraCell"

- (void)Configurar:(UIViewController *)pView Elemento:(ElementoCarrito *)pElemento
{
    [self inicializar];
    self.myVista = pView;
    self.Elemento = pElemento;
    
    NSDecimalNumber *lNumero = [pElemento.Producto.ProductoHijo.Precio decimalNumberByMultiplyingBy:pElemento.Unidades];
    
    self.labelDescripcionProducto.text = pElemento.TextoPropiedades;
    self.labelNombreProducto.text = pElemento.Producto.ProductoHijo.Nombre;
    self.labelPrecioProducto.text = [NSString stringWithFormat:@"%@ %@",  [lNumero toStringFormaterCurrency], pElemento.Producto.ProductoHijo.Moneda];
    self.labelReferencia.text = [NSString stringWithFormat:Traducir(@"Ref: %@"), pElemento.Producto.ProductoHijo.Referencia];
    self.labelReferencia.hidden = ([pElemento.Producto.ProductoHijo.Referencia isEqualToString:@""]);
    [self.btnImagenProducto cargarFondoStringUrlImagenAsincrono:pElemento.Producto.ProductoHijo.ImagenPorDefecto.UrlPathSmall Grupo:GRUPO_PRODUCTOCOMPRACELL];
    
    [self.labelDescripcionProducto mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT];
    
    if (self.labelReferencia.hidden == true)
    {
        [self.labelDescripcionProducto mgSetSizeX:UNCHANGED andY:self.labelReferencia.frame.origin.y + 5 andWidth:UNCHANGED andHeight:UNCHANGED];
    }
    else
    {
        [self.labelDescripcionProducto mgSetSizeX:UNCHANGED andY:58 andWidth:UNCHANGED andHeight:UNCHANGED];
    }
}

- (void)inicializar
{
    self.labelDescripcionProducto.text = @"";
    self.labelNombreProducto.text = @"";
    self.labelPrecioProducto.text = @"";
    self.labelReferencia.text = @"";
    self.btnImagenProducto.BackgroundImageAllStates = nil;
}

- (IBAction)btnImagenProducto_click:(id)sender
{
    [[Locator Default].Vistas MostrarGaleriaImagenes:self.myVista ListaImagenes:self.Elemento.Producto.ProductoHijo.ListaMedia UrlImagenEmpresa:self.Elemento.Producto.LogoEmpresa.UrlPathSmall];
}

@end
