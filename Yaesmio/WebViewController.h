//
//  WebViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 07/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YaesmioViewControllers.h"

typedef NS_ENUM(NSInteger, WVCDefecto)
{
    WVCDefecto_url      = 0, // viene dado por url
    WVCDefecto_NO_QR    = 2, // no se encuentra codigo QR
};

@interface WebViewController : YaesmioViewController

@property (strong) NSString     *cadenaUrl;
@property (strong) NSURL        *url;
@property (assign) WVCDefecto   PorDefecto;
@property (strong) NSString     *ContenidoWeb;
@property (assign) BOOL         MostrarLinkSafari;


@end
