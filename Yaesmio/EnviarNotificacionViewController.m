//
//  EnviarNotificacionViewController.m
//  Yaesmio
//
//  Created by freelance on 21/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "EnviarNotificacionViewController.h"
#import "UIAllControls.h"
#import "Contacto.h"
#import "ContactoGrupo.h"

typedef void (^EncontradoContactoEnListaBlock)(Contacto *pContacto);
typedef void (^EncontradoGrupoEnListaBlock)(GrupoNotificacion *pContacto);
typedef void (^EncontradoOtroEnListaBlock)(id pDato);

@interface EnviarNotificacionViewController ()

@property (strong, nonatomic)IBOutletCollection(UILabel) NSArray * ListaCampos;
@property (weak, nonatomic) IBOutlet UIButton *botonAgregarContactos;
@property (weak, nonatomic) IBOutlet UILabel *labelNombreProducto;
@property (weak, nonatomic) IBOutlet UILabel *labelEmpresa;
@property (weak, nonatomic) IBOutlet UILabel *labelDescripcionProducto;
@property (weak, nonatomic) IBOutlet UIImageView *imageProducto;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorBusy;
@property (weak, nonatomic) IBOutlet UITextField *textViewContactos;
@property (weak, nonatomic) IBOutlet UITextView *textMensaje;
@property (weak, nonatomic) IBOutlet UIImageView *imageFondoPantalla;
@property (nonatomic) IBOutlet UIScrollView *ScrollViewDatos;

- (IBAction)GestionContactosExecute:(id)sender;
- (IBAction)CancelarEnvioExecute:(id)sender;
- (IBAction)EnviarNotificacionExecute:(id)sender;


@property (nonatomic) ScrollManager *manager;
@property (strong) NSMutableArray *ListaContactos;

@end

@implementation EnviarNotificacionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.manager = [ScrollManager NewAndConfigureWithScrollView:self.ScrollViewDatos AndDidEndTextField:self Selector:nil];
    
    self.ListaContactos = [NSMutableArray new];
    [self CargarDatosProducto];
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 25, 20)];
    self.textViewContactos.rightView = paddingView;
    self.textViewContactos.rightViewMode = UITextFieldViewModeAlways;
    
    [[Locator Default].Contactos ComprobarAccesoAddressBook];
}

- (void)viewUnload
{
    [self setProducto:nil];
    [self setManager:nil];
    [self setListaCampos:nil];
    [self setListaContactos:nil];
    [self setLabelNombreProducto:nil];
    [self setLabelEmpresa:nil];
    [self setLabelDescripcionProducto:nil];
    [self setImageProducto:nil];
    [self setIndicatorBusy:nil];
    [self setTextViewContactos:nil];
    [self setTextMensaje:nil];
    [self setImageFondoPantalla:nil];
    [self setScrollViewDatos:nil];
}

- (IBAction)GestionContactosExecute:(id)sender
{
    self.botonAgregarContactos.highlighted = false;
    self.botonAgregarContactos.selected = false;
    [[Locator Default].Vistas MostrarSeleccionContactosTelefono:self ListaMails:self.ListaContactos MostrarGrupos:true MostrarOtros:true Result:^(NSArray *pListaContactosSeleccionados)
     {
         if (pListaContactosSeleccionados != nil)
         {
             [self.ListaContactos removeAllObjects];
             [self.ListaContactos addObjectsFromArray:pListaContactosSeleccionados];
             [self ActualizarCampoContactos];
         }
     }];
}

- (IBAction)CancelarEnvioExecute:(id)sender
{
    [self dismissViewControllerAnimated:TRUE completion:NULL];
}

- (IBAction)EnviarNotificacionExecute:(id)sender
{
    if (self.ListaContactos.count == 0)
    {
        [[Locator Default].Alerta showMessageAcept:Traducir(@"Se necesita un contacto como minimo para poder enviar un yasmy.")];
    }
    else
    {
        [[Locator Default].Alerta showBussy:Traducir(@"Enviando yasmy")];
        [[Locator Default].ServicioSOAP EnviarNotificacionPush:self.Producto DeUsuario:[Locator Default].Preferencias.IdUsuario ListaIdContactos:[self getListaIdContactos] ListaIdGrupos:[self getListaIdGrupos] ListaMails:[self getListaMails] Mensaje:self.textMensaje.text Result:^(SRResultMethod pResult, NSString *MensajeSAP)
         {
             switch (pResult)
             {
                 case SRResultMethodYES:
                     [[Locator Default].Alerta hideBussy];
                     [self dismissViewControllerAnimated:TRUE completion:NULL];
                     break;
                     
                 case SRResultMethodNO:
                     [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                     break;
                     
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
             }
         }];
    }
}

#pragma mark- Metodos Privados

- (void)CargarDatosProducto
{
    if (self.Producto != nil)
    {
        self.labelNombreProducto.text = self.Producto.ProductoHijo.Nombre;
        [self.imageProducto cargarUrlImagenAsincrono:self.Producto.ProductoHijo.ImagenPorDefecto.UrlPathSmall Busy:self.indicatorBusy];
        self.labelEmpresa.text = self.Producto.NombreEmpresa;
        self.labelDescripcionProducto.text = self.Producto.ProductoHijo.Descripcion1;
        [self.imageFondoPantalla cargarUrlImagenAsincrono:self.Producto.ProductoHijo.ImagenPorDefecto.UrlPath Busy:nil];
    }
}

- (void)ActualizarCampoContactos
{
    NSString __block *Texto = @"";
    
    [self RecorrerLista:self.ListaContactos EncontadoContacto:^(Contacto *pContacto)
     {
         Texto = [NSString stringWithFormat:@"%@ %@;", Texto, pContacto.NombreAMostrar];
     }         EncontradoGrupo :^(GrupoNotificacion *pContacto)
     {
         Texto = [NSString stringWithFormat:@"%@ %@;", Texto, pContacto.NombreGrupo];
     }	EncontradoOtro	:^(id pDato)
     {
         NSLog(@"err: %@", pDato);
     }];
    
    self.textViewContactos.text = Texto;
}

- (NSArray *)getListaIdContactos
{
    NSMutableArray *result = [NSMutableArray new];
    
    [self RecorrerLista:self.ListaContactos EncontadoContacto:^(Contacto *pContacto)
     {
         if (pContacto.UsuarioId.isNotEmpty)
         {
             [result addObject:pContacto.UsuarioId];
         }
     }       EncontradoGrupo:nil	EncontradoOtro:nil];
    
    return result;
}

- (NSArray *)getListaIdGrupos
{
    NSMutableArray *result = [NSMutableArray new];
    
    [self RecorrerLista:self.ListaContactos EncontadoContacto:nil EncontradoGrupo:^(GrupoNotificacion *pGrupo)
     {
         if (pGrupo.IdGrupo.isNotEmpty)
         {
             [result addObject:pGrupo.IdGrupo];
         }
     }       EncontradoOtro:nil];
    
    return result;
}

- (NSArray *)getListaMails
{
    NSMutableArray *result = [NSMutableArray new];
    
    [self RecorrerLista:self.ListaContactos EncontadoContacto:^(Contacto *pContacto)
     {
         if (pContacto.UsuarioId.isNotEmpty == false)
         {
             [result addObject:pContacto.mailPrincipal];
         }
     }       EncontradoGrupo:nil	EncontradoOtro:nil];
    
    return result;
}

- (void)RecorrerLista:(NSArray *)plista EncontadoContacto:(EncontradoContactoEnListaBlock)pContacto EncontradoGrupo:(EncontradoGrupoEnListaBlock)pGrupo EncontradoOtro:(EncontradoOtroEnListaBlock)pOtro
{
    for (id item in plista)
    {
        if ([item isKindOfClass:[Contacto class]] && pContacto != nil)
        {
            pContacto((Contacto *)item);
        }
        else if ([item isKindOfClass:[GrupoNotificacion class]] && pGrupo != nil)
        {
            pGrupo((GrupoNotificacion *)item);
        }
        else if (pOtro != nil)
        {
            pOtro(item);
        }
    }
}

@end
