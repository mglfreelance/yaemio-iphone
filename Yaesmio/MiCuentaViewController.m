//
//  MiCuentaViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 19/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "MiCuentaViewController.h"

@interface MiCuentaViewController ()

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *ListaCamposTraducir;

@end

@implementation MiCuentaViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaCamposTraducir);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
}

- (void)viewDidUnload
{
    [self setListaCamposTraducir:nil];
    [super viewDidUnload];
}

- (BOOL) shouldAutorotate
{
    return NO;
}
@end
