//
//  ProductoHistoricoCell.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ProductoHistoricoCell.h"
#import "UIAllControls.h"
#import "Emoticono.h"

@interface ProductoHistoricoCell ()

@property (nonatomic) IBOutlet UILabel *labelPrecio;
@property (nonatomic) IBOutlet UILabel *labelDescripcionProducto;
@property (nonatomic) IBOutlet UILabel *labelNombreProducto;
@property (nonatomic) IBOutlet UIImageView *imageEmpresa;
@property (nonatomic) IBOutlet UILabel *labelFechaCompra;
@property (nonatomic) IBOutlet UIImageView *imageProducto;
@property (nonatomic) IBOutlet UILabel *labelEmpresa;
@property (nonatomic) IBOutlet UIImageView *imageCarita;
@property (nonatomic) IBOutlet UIView *vistaYaValorado;
@property (nonatomic) IBOutlet UILabel *labelValoracion;
@property (nonatomic) IBOutlet UILabel *labelValora;
@property (nonatomic) IBOutlet UIActivityIndicatorView *controlBusy;

@property (strong)    Historico *myHistorico;
@property (nonatomic) UIViewController *myView;
@property (nonatomic) NSArray *mListaEmoticonos;

@end

@implementation ProductoHistoricoCell

#define GRUPO_HISTORICOCELL @"ProductoHistoricoCell"

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        [self inicializar];
    }
    
    return self;
}

- (void)inicializar
{
    self.labelDescripcionProducto.text  = @"";
    self.labelNombreProducto.text       = @"";
    self.labelPrecio.text               = @"";
    self.labelFechaCompra.text          = @"";
    self.labelEmpresa.text              = @"";
    self.imageProducto.image            = nil;
    self.imageEmpresa.image             = nil;
    self.vistaYaValorado.hidden         = true;
    self.imageCarita.image              = nil;
    
    Traducir(self.labelValora);
    Traducir(self.labelValoracion);
    
    [self.vistaYaValorado mgSetSizeX:UNCHANGED andY:95 andWidth:UNCHANGED andHeight:UNCHANGED];
}

- (void)Configurar:(UIViewController *)pView Emoticonos:(NSArray *)pLista Elemento:(Historico *)pHistorico
{
    if (self.myHistorico == nil || self.myHistorico != pHistorico)
    {
        [self inicializar];
        self.myHistorico = pHistorico;
        self.myView = pView;
        self.mListaEmoticonos = pLista;
        
        self.labelDescripcionProducto.text = pHistorico.TextoPropiedades;
        self.labelNombreProducto.text = pHistorico.Nombre;
        self.labelPrecio.text = [NSString stringWithFormat:Traducir(@"Precio: %@ %@"),  [pHistorico.Precio toStringFormaterCurrency], pHistorico.AbreviaturaMoneda];
        self.labelFechaCompra.text = [pHistorico.Fecha format:Traducir(@"dd/mm/yyyy")];
        self.labelEmpresa.text = pHistorico.NombreEmpresa;
        [self.imageEmpresa cargarUrlImagenAsincrono:pHistorico.LogoMarca.UrlPathSmall Busy:nil];
        [self.imageProducto cargarUrlImagenAsincrono:pHistorico.Imagen.UrlPathSmall Busy:self.controlBusy];
        [self CargaImagenEmoticono];
        self.vistaYaValorado.hidden = (pHistorico.Valoracion == 0);
        self.labelPrecio.hidden = (pHistorico.Precio.intValue == 0);
    }
}

- (void)CargaImagenEmoticono
{
    Emoticono *lemoticono = [Emoticono getEmoticonoArray:self.mListaEmoticonos withID:(int)self.myHistorico.Valoracion];
    
    if (lemoticono != nil)
    {
        [self.imageCarita cargarUrlImagenAsincrono:lemoticono.Imagen.UrlPathSmall Busy:nil];
    }
}

@end
