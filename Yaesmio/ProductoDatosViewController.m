//
//  ProductoDatosViewController.m
//  Yaesmio
//
//  Created by freelance on 05/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ProductoDatosViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface ProductoDatosViewController () <RTLabelDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (weak, nonatomic) IBOutlet UILabel *labelNombreProducto;
@property (weak, nonatomic) IBOutlet UILabel *labelRestoUnidadesProducto;
@property (weak, nonatomic) IBOutlet UIImageView *ImagenPrincipal;
@property (weak, nonatomic) IBOutlet UILabel *labelPrecioAntes;
@property (weak, nonatomic) IBOutlet UILabel *labelPrecio;
@property (weak, nonatomic) IBOutlet UILabel *labelOfrecidoPor;
@property (weak, nonatomic) IBOutlet UIButton *botonAgregarCarrito;
@property (weak, nonatomic) IBOutlet UILabel *labelMoneda;
@property (weak, nonatomic) IBOutlet UILabel *labelTitulo;
@property (weak, nonatomic) IBOutlet UILabel *labelDescripcion;
@property (weak, nonatomic) IBOutlet UILabel *labelPropiedades;
@property (weak, nonatomic) IBOutlet UIButton *botonPropiedades;
@property (weak, nonatomic) IBOutlet UILabel *labelEntregaMaxima;
@property (weak, nonatomic) IBOutlet UILabel *labelIdProducto;
@property (weak, nonatomic) IBOutlet UIView *viewImagenPrecioAgregarCarrito;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *ActivityIndicatorBusy;
@property (weak, nonatomic) IBOutlet UIView *viewAtributosProducto;
@property (weak, nonatomic) IBOutlet UIView *viewDiasEntrega;
@property (weak, nonatomic) IBOutlet UIView *viewIdProducto;
@property (weak, nonatomic) IBOutlet RTLabel *labelCondicionesGenerales;
@property (weak, nonatomic) IBOutlet UIButton *botonAgregaraFavoritos;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *IndicatorBusyVideo;
@property (weak, nonatomic) IBOutlet UIView *vistaBotonesVideo;
@property (weak, nonatomic) IBOutlet UIView *vistaPrecio;
@property (weak, nonatomic) IBOutlet UILabel *labelPrecioSolo;
@property (weak, nonatomic) IBOutlet UILabel *labelAntesPrecioSolo;
@property (weak, nonatomic) IBOutlet UILabel *labelMonedaSolo;
@property (weak, nonatomic) IBOutlet UIView *viewPromocion;
@property (weak, nonatomic) IBOutlet UIView *viewPrecioImagen;
@property (weak, nonatomic) IBOutlet UIView *viewImagenProducto;
@property (weak, nonatomic) IBOutlet UIView *ViewVideo;
@property (weak, nonatomic) IBOutlet UIView *ViewVideoYControles;
@property (weak, nonatomic) IBOutlet UIButton *botonAceptarPromocion;


- (IBAction)botonAgregarCarritoExecute:(id)sender;
- (IBAction)botonPropiedadesExecute:(id)sender;
- (IBAction)botonAgregarAFavoritosExecute:(id)sender;
- (IBAction)botonCompartir:(id)sender;
- (IBAction)botonPlayPauseVideoExecute:(id)sender;
- (IBAction)botonMaximizarVideoExecute:(id)sender;
- (IBAction)botonAceptarPromocionExecute:(id)sender;
- (IBAction)botonEnviarYasmyExecute:(id)sender;

@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCampos;

@property (strong) MPMoviePlayerController *VideoPlayer;
@property (assign) BOOL EstadoAnteriorPlaybackVideo;

@end

@implementation ProductoDatosViewController

- (void)viewUnload
{
    [self setScrollView:nil];
    [self setLabelNombreProducto:nil];
    [self setLabelRestoUnidadesProducto:nil];
    [self setImagenPrincipal:nil];
    [self setLabelPrecioAntes:nil];
    [self setLabelPrecio:nil];
    [self setLabelOfrecidoPor:nil];
    [self setBotonAgregarCarrito:nil];
    [self setLabelMoneda:nil];
    [self setLabelTitulo:nil];
    [self setLabelDescripcion:nil];
    [self setLabelPropiedades:nil];
    [self setBotonPropiedades:nil];
    [self setLabelEntregaMaxima:nil];
    [self setLabelIdProducto:nil];
    [self setViewImagenPrecioAgregarCarrito:nil];
    [self setActivityIndicatorBusy:nil];
    [self setViewAtributosProducto:nil];
    [self setViewDiasEntrega:nil];
    [self setViewIdProducto:nil];
    [self setLabelCondicionesGenerales:nil];
    [self setBotonAgregaraFavoritos:nil];
    [self setListaCampos:nil];
    [self setVideoPlayer:nil];
    
    [super viewUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self ActivarMedia:true];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self ActivarMedia:false];
}

#pragma mark- Properties

#pragma mark- Private Events

- (IBAction)botonAgregarAFavoritosExecute:(id)sender
{
    if ([Locator Default].Preferencias.EstaUsuarioLogado == false)
    {
        [[Locator Default].Alerta showMessageLoginIfNotLogin:self Message:Traducir(@"Por favor, inicia sesion o registrate para añadire este producto a tus favoritos.") Result: ^(int pResult)
         {
             if (pResult == 1)   //OK
             {
                 [self RealmenteAgregarAFavoritos];
             }
         }];
    }
    else
    {
        [self RealmenteAgregarAFavoritos];
    }
}

- (IBAction)botonCompartir:(id)sender
{
    [self ActivarMedia:false];
    [[Locator Default].Vistas MostrarCompartirSemiModal:self Producto:self.Producto result: ^
     {
         [self ActivarMedia:true];
     }];
}

- (IBAction)botonPropiedadesExecute:(id)sender
{
    [self ActivarMedia:false];
    
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(InformarCambioDePage:Clave:Objeto:)])
    {
        [self.delegate InformarCambioDePage:self Clave:@"SeleccionAtributos" Objeto:self.Producto.ProductoHijo.Atributos];
    }
}

- (IBAction)botonAgregarCarritoExecute:(id)sender
{
    if (self.Producto.ProductoHijo.esPublicitario == false)
    {
        if (self.Producto.ProductoHijo.Stock.integerValue == 0)
        {
            [[Locator Default].Alerta showMessageAcept:Traducir(@"No hay suficiente stock para comprar este producto.")];
        }
        else if (self.esModificarProduto)    // se modifica el producto
        {
            [[Locator Default].Carrito ModificarProducto:self.ElementoCarrito Modificado:self.Producto Unidades:self.ElementoCarrito.Unidades];
            [self.navigationController dismissViewControllerAnimated:true completion:nil];
        }
        else                            // se agrega un producto al carrito
        {
            if ([Locator Default].Preferencias.EstaUsuarioLogado == false)
            {
                [[Locator Default].Alerta showMessageLoginIfNotLogin:self Message:Traducir(@"Por favor, inicia sesion o registrate para añadire este producto a tu carrito de compra.") Result: ^(int pResult)
                 {
                     if (pResult == 1)   //OK
                     {
                         [self agregarACarritoYMostrarNotificacion];
                     }
                 }];
            }
            else
            {
                [self agregarACarritoYMostrarNotificacion];
            }
        }
    }
}

- (IBAction)botonPlayPauseVideoExecute:(id)sender
{
    if (self.VideoPlayer.playbackState == MPMoviePlaybackStatePlaying)
    {
        [self PauseVideo];
    }
    else
    {
        [self PlayVideo:[self getTimerActualVideo]];
    }
}

- (IBAction)botonMaximizarVideoExecute:(id)sender
{
    if (self.Producto.ProductoHijo.TieneVideos)
    {
        BOOL estadoActual = (self.VideoPlayer.playbackState == MPMoviePlaybackStatePlaying);
        [[Locator Default].Vistas MostrarVideoAPantallaCompleta:self urlVideo:self.Producto.ProductoHijo.VideoPorDefecto.UrlPath UrlImagenEmpresa:self.Producto.LogoEmpresa.UrlPath Timer:[self getTimerActualVideo] Result: ^(NSTimeInterval pTimer)
         {
             if (estadoActual)
             {
                 [self.IndicatorBusyVideo startAnimating];
             }
             
             self.EstadoAnteriorPlaybackVideo = estadoActual;
             [self ActivarMedia:true];
         }];
    }
}

- (IBAction)botonAceptarPromocionExecute:(id)sender
{
    if (self.Producto.ProductoHijo.esCupon)
    {
        [[Locator Default].Alerta showBussy:Traducir(@"Adquiriendo promoción")];
        [[Locator Default].ServicioSOAP RealizarCompraPrecioCero:self.Producto Unidades:[NSDecimalNumber one] Usuario:[Locator Default].Preferencias.IdUsuario Result: ^(SRResultMethod pResult, NSString *MensajeSAP)
         {
             switch (pResult)
             {
                 case SRResultMethodYES:
                     [[Locator Default].Alerta showMessageAcept:Traducir(@"Mensaje_compra_cupon")];
                     break;
                     
                 case SRResultMethodNO:
                     [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                     break;
                     
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
             }
         }];
    }
}

- (IBAction)botonEnviarYasmyExecute:(id)sender
{
    if ([Locator Default].Preferencias.EstaUsuarioLogado == false)
    {
        [[Locator Default].Alerta showMessageLoginIfNotLogin:self Message:Traducir(@"Por favor, inicia sesion o registrate para enviar un yasmy de este producto.") Result: ^(int pResult)
         {
             if (pResult == 1)   //OK
             {
                 [[Locator Default].Vistas MostrarEnvioNotificacion:self Producto:self.Producto];
             }
         }];
    }
    else
    {
        [[Locator Default].Vistas MostrarEnvioNotificacion:self Producto:self.Producto];
    }
}

#pragma mark- Private

- (void)LoadDatos
{
    [self.botonPropiedades setBackgroundImage:[Locator Default].Imagen.FondoBotonSeleccionBlanco forState:UIControlStateNormal];
    
    // Atributos Producto
    self.viewAtributosProducto.hidden = (self.Producto.ProductoHijo.esUnico || !self.Producto.ProductoHijo.esProducto || self.Producto.ListaAtributos.count == 0);
    self.labelPropiedades.text = [self obtenerTituloAtributos:self.Producto];
    self.botonPropiedades.TextStateNormal = [self obtenerTextoAtributos:self.Producto];
    
    //imagen del cupon
    if (self.Producto.ProductoHijo.esProducto == false)
    {
        self.viewPrecioImagen.hidden = true;
        [self.viewImagenProducto mgSetSizeX:UNCHANGED andY:UNCHANGED andWidth:self.viewImagenPrecioAgregarCarrito.frame.size.width andHeight:150];
        [self.viewImagenPrecioAgregarCarrito mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT AgregateWidth:0 AgregateHeight:0];
    }
    
    // recuadro tiempo envio
    self.viewDiasEntrega.hidden = self.Producto.DiasEntregaMax.integerValue > 0;
    self.labelEntregaMaxima.attributedText = [[Locator Default].FuenteLetra getTextoJustificado:[NSString stringWithFormat:Traducir(@"%@ dias"), self.Producto.DiasEntregaMax] Fuente:[Locator Default].FuenteLetra.Normal];
    self.labelEntregaMaxima.hidden = (self.Producto.DiasEntregaMax > 0);
    
    
    self.viewImagenPrecioAgregarCarrito.hidden = self.Producto.ProductoHijo.TieneVideos;
    self.ViewVideoYControles.hidden = !self.Producto.ProductoHijo.TieneVideos;
    self.vistaPrecio.hidden = !(self.Producto.ProductoHijo.TieneVideos && self.Producto.ProductoHijo.esProducto);
    self.viewPromocion.hidden = !self.Producto.ProductoHijo.esCupon;
    
    
    // Nombre e Imagen del Producto
    self.labelNombreProducto.text = self.Producto.ProductoHijo.Nombre;
    [self.ImagenPrincipal cargarUrlImagenAsincrono:self.Producto.ProductoHijo.ImagenPorDefecto.UrlPathSmall Busy:self.ActivityIndicatorBusy];
    [self.ImagenPrincipal mgAddGestureRecognizerTap:self Action:@selector(MostrarImagenProducto)];
    
    // mensaje Resto Unidades
    [self ConfigurarLaberlRestoUnidades];
    
    //recuadro precio y precio anterior cuando es imagen
    self.labelPrecio.text = [NSString stringWithFormat:@"%@",  [self.Producto.ProductoHijo.Precio toStringFormaterCurrency]];
    self.labelMoneda.text = self.Producto.ProductoHijo.Moneda;
    self.labelPrecioAntes.text = [NSString stringWithFormat:Traducir(@"Descuento %@"), [self.Producto.ProductoHijo.Descuento toStringFormaterWithPercent]];
    self.labelPrecioAntes.hidden = (self.Producto.ProductoHijo.PrecioAnterior == nil || self.Producto.ProductoHijo.PrecioAnterior.isZeroOrLess);
    
    //recuadro precio y precio anterior cuando es video
    self.labelPrecioSolo.text = [NSString stringWithFormat:@"%@",  [self.Producto.ProductoHijo.Precio toStringFormaterCurrency]];
    self.labelMonedaSolo.text = self.Producto.ProductoHijo.Moneda;
    self.labelAntesPrecioSolo.text = [NSString stringWithFormat:Traducir(@"Descuento %@"), [self.Producto.ProductoHijo.Descuento toStringFormaterWithPercent]];
    self.labelAntesPrecioSolo.hidden = (self.Producto.ProductoHijo.PrecioAnterior == nil || self.Producto.ProductoHijo.PrecioAnterior.isZeroOrLess);
    [self.labelPrecioSolo mgSizeFitsMaxWidth:113 MaxHeight:UNCHANGED];
    [self.labelMonedaSolo mgMoveToControl:self.labelPrecioSolo Position:MGPositionMoveToBehind Separation:5.0];
    
    // recuadro de Texto 1
    self.labelTitulo.text = self.Producto.ProductoHijo.Titulo1;
    [self.labelTitulo mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT AgregateWidth:0.0 AgregateHeight:7.0];
    self.labelDescripcion.attributedText = [[Locator Default].FuenteLetra getTextoJustificado:self.Producto.ProductoHijo.Descripcion1 Fuente:[Locator Default].FuenteLetra.Normal];
    [self.labelDescripcion mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT];
    
    //Ofrecido por
    self.labelOfrecidoPor.text = [NSString stringWithFormat:Traducir(@"Vendido por %@"), self.Producto.NombreEmpresa];
    
    //Condiciones legales y politica de privacidad
    self.labelCondicionesGenerales.hidden = self.Producto.ProductoHijo.esPublicitario;
    self.labelCondicionesGenerales.textColor = [Locator Default].Color.Principal;
    self.labelCondicionesGenerales.text = [NSString stringWithFormat:@"%@%@%@", [[Locator Default].FuenteLetra getStringHtmlLink:Traducir(@"Condiciones legales.") Url:self.Producto.UrlCondicionesLegalesEmpresa FontSize:14], self.Producto.UrlCondicionesLegalesEmpresa.isNotEmpty ? @" y ":@"", [[Locator Default].FuenteLetra getStringHtmlLink:Traducir(@"Politica de privacidad.") Url:self.Producto.UrlPoliticaPrivacidadEmpresa FontSize:14]];
    self.labelCondicionesGenerales.delegate = self;
    
    if ([self esModificarProduto])
    {
        self.botonAgregarCarrito.TextStateNormal = Traducir(@"Modificar");
    }
    
    // Video
    [self ConfigurarVideoSiExiste];
    self.ViewVideoYControles.hidden = !self.Producto.ProductoHijo.TieneVideos;
    
    // Boton Favoritos
    self.botonAgregaraFavoritos.enabled = (!self.VieneDeFavoritos);
    
    //Stock Producto
    self.botonAgregarCarrito.enabled = (self.Producto.ProductoHijo.Stock.isGreaterZero);
    self.botonAceptarPromocion.enabled = (self.Producto.ProductoHijo.Stock.isGreaterZero);
    
    //ID Producto
    self.labelIdProducto.text = [NSString stringWithFormat:@"%@ %d", Traducir(@"Id Producto:"), self.Producto.ProductoHijo.ProductoId.intValue];
    
    [self.ScrollView mgRelocateContent];                 // recoloco todas las subvistas
    
    if (self.labelCondicionesGenerales.hidden == false)
    {
        [self.ScrollView setContentSize:CGSizeMake(self.ScrollView.frame.size.width, self.labelCondicionesGenerales.frame.origin.y + self.labelCondicionesGenerales.frame.size.height)];
    }
    else
    {
        [self.ScrollView setContentSize:CGSizeMake(self.ScrollView.frame.size.width, self.labelIdProducto.frame.origin.y + self.labelIdProducto.frame.size.height)];
    }
    
    [self.ScrollView setContentOffset:CGPointZero animated:YES];
}

- (void)RellenarPantallaConDatosProducto
{
    Traducir(self.ListaCampos);
    [self LimpiarPantalla];
    
    [self LoadDatos];
}

- (void)LimpiarPantalla
{
    self.botonPropiedades.TextStateNormal = @"";
    self.labelNombreProducto.text = @"";
    self.labelPrecio.text = @"";
    self.labelPrecioAntes.text = @"";
    self.labelTitulo.text = @"";
    self.labelDescripcion.text = @"";
    self.labelEntregaMaxima.text = @"";
}

- (NSString *)obtenerTextoAtributos:(ProductoPadre *)pProducto
{
    NSString *resultado = @"";
    
    if (pProducto.ProductoHijo.esConAtributos && pProducto.ProductoHijo.Atributos != nil)
    {
        resultado = [NSString stringWithFormat:@"%@ %@ %@", pProducto.ProductoHijo.Atributos.DescripcionAtt1, pProducto.ProductoHijo.Atributos.DescripcionAtt2.isNotEmpty ? @"-":@"", pProducto.ProductoHijo.Atributos.DescripcionAtt2.isNotEmpty ? pProducto.ProductoHijo.Atributos.DescripcionAtt2:@""];
    }
    
    return resultado;
}

- (void)rtLabel:(id)rtLabel didSelectLinkWithURL:(NSURL *)url
{
    [[Locator Default].Vistas MostrarWebView:self url:url.description];
}

- (NSString *)obtenerTituloAtributos:(ProductoPadre *)pProducto
{
    NSString *resultado = @"";
    
    if (pProducto.ProductoHijo.esConAtributos && pProducto.Atributo1 != nil)
    {
        resultado = [NSString stringWithFormat:@"%@ %@ %@", pProducto.Atributo1.NombreAtributo, pProducto.Atributo2 != nil ? @"y":@"", pProducto.Atributo2 != nil ? pProducto.Atributo2.NombreAtributo:@""];
    }
    
    return resultado;
}

- (void)ConfigurarLaberlRestoUnidades
{
    BOOL esMinimo = ([self.Producto.ProductoHijo.Stock intValue] < 10 && self.Producto.ProductoHijo.Stock.isGreaterZero);
    
    BOOL esSinStock = (self.Producto.ProductoHijo.Stock.isZeroOrLess);
    
    self.labelRestoUnidadesProducto.hidden = !(esMinimo || esSinStock || self.Producto.ProductoHijo.esPublicitario);
    
    if (self.Producto.ProductoHijo.esPublicitario)
    {
        self.labelRestoUnidadesProducto.text = Traducir(@"Producto promocional");
    }
    else if (esSinStock)
    {
        if (self.Producto.ProductoHijo.esCupon)
        {
            self.labelRestoUnidadesProducto.text = Traducir(@"Promoción agotada");
        }
        else if (self.Producto.ProductoHijo.esProducto)
        {
            self.labelRestoUnidadesProducto.text = Traducir(@"Sin Stock");
        }
    }
    else if (esMinimo)
    {
        if (self.Producto.ProductoHijo.esCupon)
        {
            self.labelRestoUnidadesProducto.text = [NSString stringWithFormat:Traducir(@"¡Quedan %@ cupones!"), self.Producto.ProductoHijo.Stock];
        }
        else if (self.Producto.ProductoHijo.esProducto)
        {
            self.labelRestoUnidadesProducto.text = [NSString stringWithFormat:Traducir(@"¡Quedan %@ productos!"), self.Producto.ProductoHijo.Stock];
        }
    }
    else
    {
        self.labelRestoUnidadesProducto.text = @"";
    }
}

- (void)agregarACarritoYMostrarNotificacion
{
    if ([[Locator Default].Carrito ExisteProductoEnCarrito:self.Producto])
    {
        [[Locator Default].Alerta showMessage:Traducir(@"Ya existe este producto en su carrito. ¿Quiere agregar las unidades?") cancelButtonTitle:Traducir(@"Cancelar") AceptButtonTitle:Traducir(@"Aceptar") completionBlock: ^(NSUInteger buttonIndex)
         {
             if (buttonIndex == 1)
             {
                 [self AgregarProductoCarrito];
             }
         }];
    }
    else
    {
        [self AgregarProductoCarrito];
    }
}

- (void)AgregarProductoCarrito
{
    [[Locator Default].Vistas MostrarNotificacionProducto:self Producto:self.Producto Result: ^
     {
         [[Locator Default].Carrito agregarProducto:self.Producto Unidades:[NSDecimalNumber decimalNumberWithString:[NSString stringWithFormat:@"%d", self.UnidadesSeleccionadas]]];
     }];
}

#pragma mark- Private Methods

- (void)ConfigurarVideoSiExiste
{
    if (self.Producto.ProductoHijo.TieneVideos)
    {
        if (self.VideoPlayer == nil)
        {
            self.VideoPlayer = [[MPMoviePlayerController alloc] init];
            [self.VideoPlayer.view mgSetSizeX:0 andY:0 andWidth:self.ViewVideo.frame.size.width andHeight:self.ViewVideo.frame.size.height];
            self.VideoPlayer.backgroundView.backgroundColor = [Locator Default].Color.Blanco;
            self.VideoPlayer.controlStyle = MPMovieControlStyleNone;
            self.VideoPlayer.view.hidden = true;
            [self.ViewVideo addSubview:self.VideoPlayer.view];
        }
        
        [self.ViewVideoYControles mgAddGestureRecognizerTap:self Action:@selector(MostrarOcultarVistaBotonesVideo)];
        self.vistaBotonesVideo.hidden = true;
        self.VideoPlayer.contentURL = nil;
        [self.IndicatorBusyVideo startAnimating];
        [self PlayVideo:[self getTimerActualVideo]];
    }
    else
    {
        if (self.VideoPlayer != nil)
        {
            [self.ViewVideo willRemoveSubview:self.VideoPlayer.view];
        }
        
        [self.IndicatorBusyVideo stopAnimating];
        self.VideoPlayer = nil;
    }
}

- (void)MostrarOcultarVistaBotonesVideo
{
    self.vistaBotonesVideo.hidden = !self.vistaBotonesVideo.hidden;
    
    if (self.vistaBotonesVideo.hidden == false)       // si esta mostrado ocultarse a los 3 segundos
    {
        [self performSelector:@selector(OcultarBotonesVideo) withObject:nil afterDelay:3];
    }
}

- (void)OcultarBotonesVideo
{
    if (self.vistaBotonesVideo.hidden == false)
    {
        self.vistaBotonesVideo.hidden = true;
    }
}

- (void)PlayVideo:(NSTimeInterval)pTimer
{
    if (self.Producto.ProductoHijo.TieneVideos)
    {
        if (self.VideoPlayer.contentURL == nil)
        {
            self.VideoPlayer.contentURL = [NSURL URLWithString:self.Producto.ProductoHijo.VideoPorDefecto.UrlPath];
            self.VideoPlayer.initialPlaybackTime = pTimer;
            [self agregateNotificationsVideoPlayer];
            [self.VideoPlayer play];
        }
        else if (self.VideoPlayer.playbackState != MPMoviePlaybackStatePlaying)
        {
            [self agregateNotificationsVideoPlayer];
            self.VideoPlayer.initialPlaybackTime = pTimer;
            [self.VideoPlayer play];
        }
        else
        {
            [self.VideoPlayer setCurrentPlaybackTime:pTimer];
        }
    }
}

- (void)agregateNotificationsVideoPlayer
{
    NSNotificationCenter *notification = [NSNotificationCenter defaultCenter];
    
    [notification addObserver:self selector:@selector(SeReproduceVideo) name:MPMoviePlayerNowPlayingMovieDidChangeNotification object:nil];
    [notification addObserver:self selector:@selector(FinalizaReproduccionVideo) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
}

- (void)removeNotificacionPlayVideo
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerNowPlayingMovieDidChangeNotification object:nil];
}

- (void)SeReproduceVideo
{
    self.VideoPlayer.view.hidden = false;
    [self.IndicatorBusyVideo stopAnimating];
    [self removeNotificacionPlayVideo];
}

- (void)FinalizaReproduccionVideo
{
    [self removeNotificacionFinalizeVideo];
}

- (void)removeNotificacionFinalizeVideo
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
}

- (void)PauseVideo
{
    if (self.Producto.ProductoHijo.TieneVideos && self.VideoPlayer != nil)
    {
        if (self.VideoPlayer.playbackState == MPMoviePlaybackStatePlaying)
        {
            [self.VideoPlayer pause];
        }
    }
}

- (void)StopVideo
{
    if (self.Producto.ProductoHijo.TieneVideos && self.VideoPlayer != nil)
    {
        if (self.VideoPlayer.playbackState == MPMoviePlaybackStatePlaying)
        {
            [self.VideoPlayer stop];
        }
        
        [self.VideoPlayer setContentURL:nil];
    }
}

- (NSTimeInterval)getTimerActualVideo
{
    NSTimeInterval resul;
    
    if (self.Producto.ProductoHijo.TieneVideos && self.VideoPlayer != nil)
    {
        resul = self.VideoPlayer.currentPlaybackTime;
    }
    else
    {
        resul = 0;
    }
    
    return resul;
}

- (void)ActivarMedia:(BOOL)pActivar
{
    if (self.Producto.ProductoHijo.TieneVideos && self.VideoPlayer != nil)
    {
        if (pActivar && self.EstadoAnteriorPlaybackVideo)
        {
            [self PlayVideo:[self getTimerActualVideo]];
        }
        else if (pActivar == false)
        {
            if (self.VideoPlayer != nil)
            {
                self.EstadoAnteriorPlaybackVideo = (self.VideoPlayer.playbackState == MPMoviePlaybackStatePlaying);
            }
            
            [self PauseVideo];
        }
    }
}

- (void)MostrarImagenProducto
{
    [[Locator Default].Vistas MostrarGaleriaImagenes:self ListaImagenes:self.Producto.ProductoHijo.ListaMediaImagen NumImagenAmostrar:0 UrlImagenEmpresa:self.Producto.LogoEmpresa.UrlPath];
}

-(void)RealmenteAgregarAFavoritos
{
    [[Locator Default].Alerta showBussy:Traducir(@"Agregando a favoritos.")];
    [[Locator Default].ProductosFavoritos AgregarProducto:self.Producto Result: ^(SRResultProductoFavoritoManager pResult, NSString *pMensaje)
     {
         UIView *boton = [NSKeyedUnarchiver unarchiveObjectWithData:[NSKeyedArchiver archivedDataWithRootObject:self.botonAgregaraFavoritos]];
         int valor = 0;
         
         switch (pResult)
         {
             case SRResultProductoFavoritoManagerErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultProductoFavoritoManagerNO:
                 [[Locator Default].Alerta showMessageAcept:pMensaje];
                 break;
                 
             case SRResultProductoFavoritoManagerYES:
                 [self.botonAgregaraFavoritos.superview addSubview:boton];
                 self.botonAgregaraFavoritos.hidden = true;
                 valor = [self.ScrollView convertRect:boton.bounds fromView:nil].origin.y;
                 [boton genieInTransitionWithDuration:0.7 destinationRect:CGRectMake(153, valor, 80, 20) destinationEdge:BCRectEdgeTop completion: ^
                  {
                      boton.hidden = true;
                      self.botonAgregaraFavoritos.alpha = 0;
                      self.botonAgregaraFavoritos.hidden = false;
                      self.botonAgregaraFavoritos.enabled = false;
                      [UIView animateWithDuration:1.0 animations: ^{
                          self.botonAgregaraFavoritos.alpha = 1.0;
                      }];
                  }];
                 break;
         }
         [[Locator Default].Alerta hideBussy];
     }];
}

@end
