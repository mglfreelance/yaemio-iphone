//
//  EvaluacionesProducto.h
//  Yaesmio
//
//  Created by freelance on 07/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"

@interface EvaluacionProducto : SoapObject

@property (strong) NSDate *Fecha;
@property (strong) NSString *NombreUsuario;
@property (strong) NSDecimalNumber *Nota;
@property (strong) NSString *Comentario;

+ (EvaluacionProducto *)deserializeNode:(xmlNodePtr)cur;

- (EvaluacionProducto *)Clonar;

@end
