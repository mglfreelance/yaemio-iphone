//
//  CarritoViewController.m
//  Yaesmio
//
//  Created by freelance on 04/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "CarritoViewController.h"
#import "CarritoEmpresaViewController.h"
#import "UIAllControls.h"
#import "DMLazyScrollView.h"
#import "SeleccionDireccionEnvioViewController.h"
#import "CompraViewController.h"

@interface CarritoViewController () <DMLazyScrollViewDelegate, CarritoManagerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelLinea2Direccion;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *BusyGastosDeEnvio;
@property (weak, nonatomic) IBOutlet UILabel *labelLinea1Direccion;
@property (weak, nonatomic) IBOutlet UIPageControl *PageControlEmpresas;
@property (weak, nonatomic) IBOutlet UILabel *LabelLinea3Direccion;
@property (weak, nonatomic) IBOutlet DMLazyScrollView *Scrollview;
@property (weak, nonatomic) IBOutlet UIView *ViewSinEmpresas;
@property (weak, nonatomic) IBOutlet UILabel *labelPrecioTotal;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *BussyDireccion;
@property (weak, nonatomic) IBOutlet UIButton *buttonCambiarDireccionEnvio;
@property (weak, nonatomic) IBOutlet UILabel *labelGastosEnvio;
@property (weak, nonatomic) IBOutlet UILabel *labelTitulo;

- (IBAction)PageControlEmpresasChangeValue:(id)sender;
- (IBAction)CerrarPantallaExecute:(id)sender;

@property (strong) NSMutableArray *Lista;
@property (strong) NSMutableArray *ListaComprobarGastosEnvio;
@property (nonatomic) EmpresaCarrito *EmpresaActual;
@property (strong) Direccion *DireccionEnvio;
@property (strong) Usuario *Usuario;
@property (assign) BOOL ComprobacionCompraCorrecta;

@end

@implementation CarritoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.labelTitulo);
    self.ComprobacionCompraCorrecta = false;
    [Locator Default].Carrito.delegate = self;
    [self.ViewSinEmpresas mgSetSizeX:0 andY:0 andWidth:UNCHANGED andHeight:UNCHANGED];
    [self RefrescarPantalla];
    [self CargarDireccionPorDefecto];
}

- (void)viewUnload
{
    [Locator Default].Carrito.delegate = nil;
    [self setLista:nil];
    [self setPageControlEmpresas:nil];
    [self setScrollview:nil];
    [self setViewSinEmpresas:nil];
    [self setLabelPrecioTotal:nil];
    [self setDireccionEnvio:nil];
    [self setBussyDireccion:nil];
    [self setUsuario:nil];
    [self setLabelLinea1Direccion:nil];
    [self setLabelLinea2Direccion:nil];
    [self setLabelLinea3Direccion:nil];
    [self setLabelGastosEnvio:nil];
    [self setBussyDireccion:nil];
    [self setListaComprobarGastosEnvio:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_seleccion_direccion_envio"])
    {
        SeleccionDireccionEnvioViewController *vista = segue.destinationViewController;
        
        if (vista != nil)
        {
            [vista configurarConUsuario:self.Usuario Resultado:^(Direccion *Direccion)
             {
                 if (Direccion != nil)
                 {
                     self.ComprobacionCompraCorrecta = false;
                     self.DireccionEnvio = Direccion;
                     [self ChequearCompra];
                     [self ActualizarTotalYDireccionEnvio];
                 }
             }];
        }
    }
    else if ([segue.identifier isEqualToString:@"segue_CompraViewController"])
    {
        CompraViewController *vista = segue.destinationViewController;
        
        if (vista != nil)
        {
            [vista ConfigurarConCarrito:self.EmpresaActual Direccion:self.DireccionEnvio Usuario:self.Usuario Resultado:^(int pResult)
             {
                 if (pResult == true)
                 {
                     [self dismissViewControllerAnimated:true completion:nil];
                 }
             }];
        }
    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"segue_CompraViewController"])
    {
        if (self.DireccionEnvio == nil && self.EmpresaActual.esNecesariaDireccionEnvio == true)
        {
            [[Locator Default].Alerta showMessageAcept:Traducir(@"No has seleccionado una direccion de envio. Por favor, revisa tus datos y asigna una direccion de envio.")];
        }
        
        return self.EmpresaActual != nil;
    }
    else if ([identifier isEqualToString:@"segue_seleccion_direccion_envio"])
    {
        return true;
    }
    
    return false;
}

- (IBAction)CerrarPantallaExecute:(id)sender
{
    [self dismissViewControllerAnimated:TRUE completion:NULL];
}

- (EmpresaCarrito *)EmpresaActual
{
    if (self.PageControlEmpresas.currentPage < [Locator Default].Carrito.EmpresasCarrito.count)
    {
        return [Locator Default].Carrito.EmpresasCarrito[self.PageControlEmpresas.currentPage];
    }
    else
    {
        return nil;
    }
}

#pragma mark- Delegate Carrito

- (void)CambiadoElementosCarrito
{
    [self RefrescarPantalla];
}

#pragma mark- Delegate LazyScrollView

- (void)lazyScrollView:(DMLazyScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex
{
    if (self.Scrollview.currentPage != self.PageControlEmpresas.currentPage)
    {
        self.PageControlEmpresas.currentPage = currentPageIndex;
    }
    
    if (self.DireccionEnvio != nil)
    {
        [self ChequearCompra];
    }
    
    [self ActualizarTotalYDireccionEnvio];
}

- (UIViewController *)lazyScrollViewDataSource:(NSUInteger)index
{
    if (index < self.Lista.count)
    {
        return self.Lista[index];
    }
    else
    {
        return nil;
    }
}

#pragma mark-Metodos Privados

- (BOOL)RegenerarListaPagesPorProducto
{
    self.Lista = [NSMutableArray new];
    self.ListaComprobarGastosEnvio = [NSMutableArray new];
    
    self.PageControlEmpresas.numberOfPages = [Locator Default].Carrito.EmpresasCarrito.count;
    self.PageControlEmpresas.currentPage = 1;
    
    for (EmpresaCarrito *empresa in[Locator Default].Carrito.EmpresasCarrito)
    {
        [self.Lista addObject:[self getViewControllerInitialize:empresa]];
        [self.ListaComprobarGastosEnvio addObject:empresa]; // para comprobar Gastos envio
    }
    
    return (self.Lista.count > 0);
}

- (CarritoEmpresaViewController *)getViewControllerInitialize:(EmpresaCarrito *)pEmpresa
{
    CarritoEmpresaViewController *myViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"CarritoEmpresaViewController"];
    
    if (myViewController != nil)
    {
        myViewController.Carrito = pEmpresa;
        myViewController.Padre = self;
        myViewController.ActualizarTotal = ^()
        {
            [self ActualizarTotalYDireccionEnvio];
        };
    }
    
    return myViewController;
}

- (IBAction)PageControlEmpresasChangeValue:(id)sender
{
    if (self.Scrollview.currentPage != self.PageControlEmpresas.currentPage)
    {
        [self.Scrollview setPage:self.PageControlEmpresas.currentPage animated:true];
    }
    
    [self ActualizarTotalYDireccionEnvio];
}

- (void)ActualizarTotalYDireccionEnvio
{
    EmpresaCarrito *actual = self.EmpresaActual;
    
    self.labelLinea1Direccion.text = @"";
    self.labelLinea2Direccion.text = @"";
    self.LabelLinea3Direccion.text = @"";
    self.labelGastosEnvio.text = @"";
    self.labelPrecioTotal.text = @"";
    self.buttonCambiarDireccionEnvio.TextStateNormal = @"";
    self.buttonCambiarDireccionEnvio.hidden = (self.EmpresaActual.esNecesariaDireccionEnvio == false);
    
    //direccion
    if (self.EmpresaActual.esNecesariaDireccionEnvio == false)
    {
        self.labelLinea1Direccion.text = Traducir(@"Tu compra será enviada a:");
        self.labelLinea2Direccion.text = [Locator Default].Preferencias.MailUsuario;
    }
    else if (self.DireccionEnvio != nil)
    {
        self.labelLinea1Direccion.text = [NSString stringWithFormat:@"%@ %@", self.Usuario.Nombre, self.Usuario.Apellidos];
        self.labelLinea2Direccion.text =  [NSString stringWithFormat:@"%@ %@, %@ %@ %@ %@", self.DireccionEnvio.LaDireccion, self.DireccionEnvio.Numero, self.DireccionEnvio.Bloque, self.DireccionEnvio.Piso, self.DireccionEnvio.Escalera, self.DireccionEnvio.Puerta];
        self.LabelLinea3Direccion.text =  [NSString stringWithFormat:@"%@ %@ %@ %@", self.DireccionEnvio.Cod_postal, self.DireccionEnvio.Poblacion, self.DireccionEnvio.Provincia, self.DireccionEnvio.Pais];
        self.buttonCambiarDireccionEnvio.AttributedTextStateNormal = [[Locator Default].FuenteLetra getTextoSubrayado:Traducir(@"Cambiar")];
    }
    else
    {
        self.buttonCambiarDireccionEnvio.TextStateNormal = Traducir(@"Añadir una");
        self.labelLinea1Direccion.text = Traducir(@"No tienes ninguna dirección.");
        self.buttonCambiarDireccionEnvio.AttributedTextStateNormal = [[Locator Default].FuenteLetra getTextoSubrayado:Traducir(@"Añadir una")];
    }
    
    if (actual != nil)
    {
        if (actual.esNecesariaDireccionEnvio)
        {
            self.labelGastosEnvio.text = [NSString stringWithFormat:Traducir(@"Gastos de envío: %@ %@"), [actual.GastosEnvio toStringFormaterCurrency], actual.Moneda];
        }
        
        self.labelPrecioTotal.text = [NSString stringWithFormat:Traducir(@"Total: %@ %@"), [actual.PrecioTotal toStringFormaterCurrency], actual.Moneda];
    }
}

- (void)RefrescarPantalla
{
    if ([self RegenerarListaPagesPorProducto])
    {
        self.Scrollview.dataSource = nil;
        self.Scrollview.controlDelegate = self;
        [self.Scrollview setEnableCircularScroll:true];
        self.Scrollview.numberOfPages = self.Lista.count;
        [self.Scrollview reloadData];
        [self.Scrollview setPage:0 animated:false];
        [self.Scrollview setAutoPlay:false];
        [self.Scrollview.visibleViewController viewDidLayoutSubviews];
    }
    
    [self ActualizarTotalYDireccionEnvio];
    self.ViewSinEmpresas.hidden = (self.Lista.count > 0);
}

- (void)CargarDireccionPorDefecto
{
    [self.BussyDireccion startAnimating];
    
    [[Locator Default].ServicioSOAP ObtenerUsuario:[Locator Default].Preferencias.IdUsuario Result: ^(SRResultMethod pResultUsuario, NSString *pMensaje, Usuario *pUsuario)
     {
         switch (pResultUsuario)
         {
             case SRResultMethodNO:
                 [[Locator Default].Alerta showMessageAcept:pMensaje];
                 [self.navigationController popViewControllerAnimated:true];
                 break;
                 
             case SRResultMethodErrConex:
                 [self MostrarErrorGrave];
                 break;
                 
             case SRResultMethodYES:
                 self.Usuario = pUsuario;
                 
                 if ([self esDatosUsuarioCompletado] == false)
                 {
                     [[Locator Default].Alerta showMessage:Traducir(@"No has introducido tu nombre o apellidos, estos campos son necesarios para el envio correcto de lo que compres.") cancelButtonTitle:Traducir(@"Cancelar") AceptButtonTitle:Traducir(@"Ir") completionBlock: ^(NSUInteger buttonIndex)
                      {
                          if (buttonIndex == 1)
                          {
                              [[Locator Default].Vistas MostrarEdicionUsuario:self Usuario:self.Usuario Result: ^(int pResult)
                               {
                                   [self ObtenerDireccionEnvioPorDefecto];
                               }];
                          }
                      }];
                 }
                 else
                 {
                     [self ObtenerDireccionEnvioPorDefecto];
                 }
                 
                 break;
         }
     }];
}

- (void)MostrarErrorGrave
{
    [[Locator Default].Alerta showErrorConexSOAP: ^
     {
         [self.navigationController popViewControllerAnimated:true];
     }];
}

- (BOOL)esDatosUsuarioCompletado
{
    return ([self.Usuario.Nombre isEqualToString:@""] == false && [self.Usuario.Apellidos isEqualToString:@""] == false);
}

- (void)ObtenerDireccionEnvioPorDefecto
{
    if (self.Usuario.ExisteDireccion1)
    {
        [[Locator Default].ServicioSOAP ObtenerDirecionPrincipalUsuario: ^(SRResultMethod pResult, Direccion *pDireccion)
         {
             switch (pResult)
             {
                 case SRResultMethodErrConex:
                     [self MostrarErrorGrave];
                     break;
                     
                 case SRResultMethodNO:
                     
                     break;
                     
                 case SRResultMethodYES:
                     self.DireccionEnvio = pDireccion;
                     [self.ListaComprobarGastosEnvio removeAllObjects];
                     [self.ListaComprobarGastosEnvio addObjectsFromArray:[Locator Default].Carrito.EmpresasCarrito];
                     [self ChequearCompra];
                     break;
             }
             [self ActualizarTotalYDireccionEnvio];
             [self.BussyDireccion stopAnimating];
         }];
    }
    else
    {
        [self.BussyDireccion stopAnimating];
        [self ActualizarTotalYDireccionEnvio];
    }
}

- (void)ChequearCompra
{
    if ([self.ListaComprobarGastosEnvio indexOfObject:self.EmpresaActual] != NSNotFound && self.EmpresaActual.esNecesariaDireccionEnvio)
    {
        self.labelGastosEnvio.text = Traducir(@"Calculando gastos envío.");
        [self.labelGastosEnvio mgMoveToControl:self.BusyGastosDeEnvio Position:MGPositionMoveToBefore Separation:3.0];
        [self.BusyGastosDeEnvio startAnimating];
        
        self.EmpresaActual.GastosEnvio = [NSDecimalNumber zero];
        [[Locator Default].ServicioSOAP ChequearCompra:self.EmpresaActual Usuario:self.Usuario DireccionEnvio:self.DireccionEnvio Result: ^(SRResultMethod pResult, NSString *pMensaje, NSDecimalNumber *pGastosEnvio, NSString *pMailPaypal)
         {
             switch (pResult)
             {
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
                     
                 case SRResultMethodNO:
                     [[Locator Default].Alerta showMessageAceptWithTitle:Traducir(@"¡Ups!") AndMessage:pMensaje];
                     break;
                     
                 case SRResultMethodYES:
                     self.EmpresaActual.GastosEnvio = pGastosEnvio;
                     [self.ListaComprobarGastosEnvio removeObject:self.EmpresaActual];
                     break;
             }
             [self.labelGastosEnvio mgMoveToControl:self.labelPrecioTotal Position:MGPositionMoveToRight Separation:0.0];
             [self.BusyGastosDeEnvio stopAnimating];
             [self ActualizarTotalYDireccionEnvio];
         }];
    }
}

@end
