//
//  NSString+Ampliado.m
//  Yaesmio
//
//  Created by freelance on 23/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "NSString+Ampliado.h"

@implementation NSString (Ampliado)

- (BOOL)isNotEmpty
{
    @try
    {
        return ![self isEqualToString:@""];
    }
    @catch (NSException *e)
    {
    }
    
    return false;
}

@end
