//
//  Pagina1ViewController.m
//  Yaesmio
//
//  Created by freelance on 07/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "PaginaBusquedaViewController.h"
#import "UIAllControls.h"
#import "FiltroBusquedaProductos.h"
#import <UIKit/UIKit.h>
#import "UIAllControls.h"
#import <MobileCoreServices/MobileCoreServices.h>

@interface PaginaBusquedaViewController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCamposTraducir;
@property (weak, nonatomic) IBOutlet UILabel *labelTituloPantalla;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollBotones;
@property (weak, nonatomic) IBOutlet UILabel *labelNombreUsuario;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *BusyFotoUsuario;
@property (weak, nonatomic) IBOutlet UIImageView *PinUsuario;
@property (weak, nonatomic) IBOutlet UIView *ViewDestacados;
@property (weak, nonatomic) IBOutlet PAImageView *ImagenUsuario;

- (IBAction)MostrarBusquedaProductosExecute:(id)sender;
- (IBAction)MostrarCapturaQRExecute:(id)sender;


@property (assign) BOOL seAcabaDeLogar;

@end

@implementation PaginaBusquedaViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = true;
    self.view.backgroundColor = [Locator Default].Color.Transparente;
    self.ImagenUsuario.containerImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    if ([Locator Default].Preferencias.EstaUsuarioLogado == false)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CambiadoNombreUsuario) name:PreferencesKeyNombreUsuario object:[Locator Default].Preferencias];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(SeLogoUsuario) name:PreferencesKeyLogadoUsuario object:[Locator Default].Preferencias];
    }
    else
    {
        self.seAcabaDeLogar = true;
    }
    
    Traducir(self.ListaCamposTraducir);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = true;
    
    if (self.seAcabaDeLogar)
    {
        [self MostrarAnimacionLogado];
    }
}

- (void)viewUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self setListaCamposTraducir:nil];
    [self setLabelTituloPantalla:nil];
    [self setScrollBotones:nil];
    [self setLabelNombreUsuario:nil];
    
    [super viewUnload];
}

- (IBAction)MostrarBusquedaProductosExecute:(id)sender
{
    UIButton *boton = sender;
    
    FiltroBusquedaProductos *Filtro = [[FiltroBusquedaProductos alloc] initWith: ^(FiltroBusquedaProductos *pDato) {
        pDato.Provincia = [Locator Default].Preferencias.ProvinciaBusqueda;
        pDato.Pais = [Locator Default].Preferencias.PaisBusqueda;
        
        if (boton != nil)
        {
            if ([boton.titleLabel.text isEqualToString:Traducir(@"Disfruta")])
            {
                pDato.TipoBusqueda = TipoProductoBusquedaCupon;
            }
            else if ([boton.titleLabel.text isEqualToString:Traducir(@"Mira")])
            {
                pDato.TipoBusqueda = TipoProductoBusquedaPublicitario;
            }
            else
            {
                pDato.TipoBusqueda = TipoProductoBusquedaProducto;
            }
        }
    }];
    
    [[Locator Default].Vistas MostrarBusquedaProductos:self Filtro:Filtro];
}

- (IBAction)MostrarCapturaQRExecute:(id)sender
{
    [[Locator Default].Vistas MostrarCapturaQR:self];
}

- (void)CambiadoNombreUsuario
{
    self.labelNombreUsuario.text = [self ObtenerNombreUsuario];
}

- (void)SeLogoUsuario
{
    self.seAcabaDeLogar = true;
}

- (void)MostrarAnimacionLogado
{
    if ([Locator Default].Preferencias.EstaUsuarioLogado)
    {
        [self.ImagenUsuario setImage:[UIImage imageNamed:@"sin_foto_usuario"] animated:NO];
        [self CambiadoNombreUsuario];
        [self MostrarImagenUsuario];
        [self.ImagenUsuario mgAddGestureRecognizerTap:self Action:@selector(MostrarSelfie)];
    }
    
//    CGRect oldFrame = self.ImagenUsuario.frame;
//    self.ImagenUsuario.frame = CGRectZero;
//    self.ImagenUsuario.center = self.ImagenUsuario.center;
//    self.ImagenUsuario.hidden = YES;
//    self.PinUsuario.hidden = true;
//    self.labelTituloPantalla.hidden = YES;
//    [self.labelNombreUsuario mgSetSizeX:330.0 andY:UNCHANGED andWidth:UNCHANGED andHeight:UNCHANGED];
//    
//    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionAllowUserInteraction animations:^
//     {
//         self.labelTituloPantalla.hidden = NO;
//         [self.labelNombreUsuario mgSetSizeX:96.0 andY:UNCHANGED andWidth:UNCHANGED andHeight:UNCHANGED];
//         self.ImagenUsuario.frame = oldFrame;
//         self.ImagenUsuario.hidden = NO;
//         self.PinUsuario.hidden = false;
//         self.labelNombreUsuario.hidden = false;
//     }
//     
//                     completion:^(BOOL finished)
//     {
//     }];
    self.seAcabaDeLogar = false;
}

- (void)MostrarSelfie
{
    [[Locator Default].Alerta showMessage:Traducir(@"texto_hacer_selfie") cancelButtonTitle:Traducir(@"Accede a tus fotos") AceptButtonTitle:Traducir(@"Hazte una foto") completionBlock: ^(NSUInteger buttonIndex)
     {
         [self MostrarImagePicker:buttonIndex != 0];
     }];
}

- (void)MostrarImagenUsuario
{
    [self.ImagenUsuario setImage:[UIImage imageNamed:@"sin_foto_usuario"] animated:NO];
    
    if ([Locator Default].Preferencias.EstaUsuarioLogado)
    {
        //primero muestro la de preferencias si no es null
        UIImage *Imagen = [Locator Default].Preferencias.ImagenUsuario;
        
        if (Imagen != nil)
        {
            [self.ImagenUsuario setImage:Imagen animated:YES];
        }
        else
        {
            UIImage *Imagen = [[Locator Default].Contactos getImagenContactoPorMail:[Locator Default].Preferencias.MailUsuario];
            
            if (Imagen != nil)
            {
                [self.ImagenUsuario setImage:Imagen animated:YES];
                [Locator Default].Preferencias.ImagenUsuario = Imagen;
            }
        }
    }
}

- (void)MostrarImagePicker:(BOOL)mostrarCamara
{
    UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
    
    imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.delegate = self;
    
    if (mostrarCamara)
    {
        imagePickerController.sourceType =  UIImagePickerControllerSourceTypeCamera;
        imagePickerController.cameraDevice = UIImagePickerControllerCameraDeviceFront;
    }
    else
    {
        imagePickerController.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
    }
    
    [self presentViewController:imagePickerController animated:true completion:nil];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.ImagenUsuario setImage:[UIImage imageNamed:@"sin_foto_usuario"] animated:NO];
    [self.BusyFotoUsuario startAnimating];
    [picker dismissViewControllerAnimated:true completion: ^
     {
         NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
         UIImage *imageToUse;
         
         // Handle a still image picked from a photo album
         if (CFStringCompare((CFStringRef)mediaType, kUTTypeImage, 0) == kCFCompareEqualTo)
         {
             imageToUse = [info objectForKey:UIImagePickerControllerEditedImage];
             
             if (imageToUse == nil)
             {
                 imageToUse = [info objectForKey:UIImagePickerControllerOriginalImage];
             }
             
             if (imageToUse)
             {
                 [self.ImagenUsuario setImage:imageToUse animated:YES];
                 [Locator Default].Preferencias.ImagenUsuario = imageToUse;
             }
             
             [self.BusyFotoUsuario stopAnimating];
         }
     }];
}

- (NSString *)ObtenerNombreUsuario
{
    NSString *nombre = [Locator Default].Preferencias.NombreUsuario;
    
    if (nombre.isNotEmpty && [Locator Default].Preferencias.EstaUsuarioLogado)
    {
        return [NSString stringWithFormat:@"%@ %@", Traducir(@"Hola"), nombre];
    }
    else
    {
        return @"";
    }
}

@end
