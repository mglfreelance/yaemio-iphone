//
//  Usuario.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "SOAP_USGlobals.h"
#import "SoapObject.h"
#import "Moneda.h"

@interface Usuario : SoapObject

+ (Usuario *)	deserializeNode:(xmlNodePtr)cur;

- (Usuario *)	Clonar; // devuelve usuario clonado

/* elements */
@property (strong) NSString *Apellidos;
@property (assign) BOOL ExisteDireccion1;
@property (assign) BOOL ExisteDireccion2;
@property (strong) NSString *Mail;
@property (strong) NSString *Nombre;
@property (strong) NSString *UsuarioId;
@property (strong) NSDate *fecha_nacimiento;
@property (strong) NSString *sexo;
@property (strong) NSString *mensaje;
@property (assign) BOOL aceptaEnvioPublicidad;
@property (strong) Moneda *Moneda;

@end
