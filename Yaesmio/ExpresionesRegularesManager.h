//
//  ExpresionesRegularesManager.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 30/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ExpresionesRegularesManager : NSObject

-(BOOL) ComprobarMail:(NSString*)pMail;

-(BOOL) ComprobarPassword:(NSString*)pClave;

-(BOOL) ComprobarNombre:(NSString*)pNombre;

-(BOOL) ComprobarApellidos:(NSString*)pApellidos;

-(BOOL) ComprobarDireccion:(NSString*)pDireccion;

-(BOOL) ComprobarTelefono:(NSString*)pTelefono;

-(BOOL) ComprobarCodigoPostal:(NSString*)pCodigoPostal;

-(BOOL) ComprobarPoblacion:(NSString*)pPoblacion;

-(BOOL) ComprobarNumero:(NSString*)pNumero;

-(BOOL) ComprobarDatoDireccion:(NSString*)pDireccion;

@end
