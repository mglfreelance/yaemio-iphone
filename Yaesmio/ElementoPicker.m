//
//  ElementoPicker.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 14/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ElementoPicker.h"

@implementation ElementoPicker

+ (ElementoPicker *)newWithCode:(NSString *)pCodigo andName:(NSString *)pName
{
    ElementoPicker *resultado = [ElementoPicker new];
    
    resultado.Codigo = pCodigo;
    resultado.Nombre = pName;
    
    return resultado;
}

+ (ElementoPicker *)newWithIntCode:(NSInteger)pCodigo andName:(NSString *)pName
{
    return [self newWithCode:[NSString stringWithFormat:@"%d", (int)pCodigo] andName:pName];
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.Codigo forKey:@"Codigo"];
    [aCoder encodeObject:self.Nombre forKey:@"Nombre"];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    if (!self)
    {
        return nil;
    }
    
    self.Codigo = [aDecoder decodeObjectForKey:@"Codigo"];
    self.Nombre = [aDecoder decodeObjectForKey:@"Nombre"];
    
    return self;
}

@end


@implementation NSArray (ElementoPicker)

- (ElementoPicker *)ObtenerElementoPorID:(NSString *)pID
{
    ElementoPicker *lResultado = nil;
    
    for (ElementoPicker *lElemento in self)
    {
        if ([lElemento.Codigo isEqualToString:pID])
        {
            lResultado = lElemento;
            break;
        }
    }
    
    return lResultado;
}

- (int)obtenerIndiceElemento:(ElementoPicker *)pElemento
{
    int lintRow = 0;
    
    for (int i = 0; i < self.count; i++)
    {
        if ([pElemento.Codigo isEqualToString:((ElementoPicker *)self[i]).Codigo])
        {
            lintRow = i;
            break;
        }
    }
    
    return lintRow;
}

@end