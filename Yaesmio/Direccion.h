//
//  Direccion.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "SOAP_USGlobals.h"
#import "SoapObject.h"

@interface Direccion : SoapObject
{
}


+ (Direccion *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * Bloque;
@property (strong) NSString * Cod_postal;
@property (strong) NSString * TextoDescripcion;
@property (strong) NSString * Escalera;
@property (strong) NSString * LaDireccion;
@property (strong) NSString * Numero;
@property (strong) NSString * Pais;
@property (strong) NSString * Piso;
@property (strong) NSString * Poblacion;
@property (strong) NSString * Provincia;
@property (strong) NSString * Puerta;
@property (strong) NSString * Telefono;
@property (strong) NSString * PaisID;
@property (strong) NSString * ProvinciaID;

@end
