//
//  AlertaManager.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 22/10/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "AlertaManager.h"
#import "WCAlertView.h"
#import "MBProgressHUD.h"
#import "UIAllControls.h"
#import "TWMessageBarManager.h"

@interface AlertaManager ()

@property (strong) WCAlertView *Alerta;
@property (strong) MBProgressHUD *StaticProgres;
@property (strong) UIWindow *StaticWindow;

@end

@implementation AlertaManager

- (void)Initialize:(UIWindow *)window
{
    self.StaticWindow = window;
}

- (void)showErrorConexSOAP
{
    [self showErrorConexSOAP:nil];
}

- (void)showErrorConexSOAP:(void (^)())pblock
{
    [self showMessageAceptWithTitle:Traducir(@"Error de conexión") AndMessage:Traducir(@"No ha sido posible conectar con Yaesmio! Por favor, verifique que tiene conexion a internet.") ResulBlock:pblock];
}

- (void)showMessageAcept:(NSString *)pMensaje
{
    [self showMessageAcept:pMensaje ResulBlock:nil];
}

- (void)showMessageAcept:(NSString *)pMensaje ResulBlock:(void (^)())pblock
{
    [self showMessageAceptWithTitle:@"" AndMessage:pMensaje ResulBlock: ^{
        if (pblock)
        {
            pblock();
        }
    }];
}

- (void)showMessageAceptWithTitle:(NSString *)pTitle AndMessage:(NSString *)pMensaje
{
    [self showMessageAceptWithTitle:pTitle AndMessage:pMensaje ResulBlock:nil];
}

- (void)showMessageAceptWithTitle:(NSString *)pTitle AndMessage:(NSString *)pMensaje ResulBlock:(void (^)())pblock
{
    [self showMessage:pTitle Message:pMensaje cancelButtonTitle:Traducir(@"Aceptar") AceptButtonTitle:nil completionBlock: ^(NSUInteger buttonIndex) {
        if (pblock)
        {
            pblock();
        }
    }];
}

- (void)showNotificationWithTitle:(NSString *)pTitle AndMessage:(NSString *)pMessage PushResult:(ResultadoBlock)pResult
{
    [[Locator Default].Sounds PlayNotificacion];
    [TWMessageBarManager setColorsForMessagType:TWMessageBarMessageTypeInfo ColorFont:[Locator Default].Color.Blanco ColorBackground:[Locator Default].Color.PrincipalTransparente ImageName:@"boton_yasmy"];
    [[TWMessageBarManager sharedInstance] showMessageWithTitle:pTitle description:pMessage type:TWMessageBarMessageTypeInfo callback:^{
        if (pResult != nil)
        {
            pResult();
        }
    }];
}

- (void)showMessage:(NSString *)pMessage cancelButtonTitle:(NSString *)cancelButtonTitle AceptButtonTitle:(NSString *)otherButtonTitles completionBlock:(void (^)(NSUInteger buttonIndex))block
{
    [self showMessage:@"" Message:pMessage cancelButtonTitle:cancelButtonTitle AceptButtonTitle:otherButtonTitles completionBlock:block];
}

- (void)showMessage:(NSString *)pTitle Message:(NSString *)pMessage cancelButtonTitle:(NSString *)cancelButtonTitle AceptButtonTitle:(NSString *)otherButtonTitles completionBlock:(void (^)(NSUInteger buttonIndex))block
{
    [[Locator Default].Alerta hideBussy];
    [self MostrarAlerta:pTitle Mensaje:pMessage TituloCancel:cancelButtonTitle Resultado:block OtrosTitulosBoton:otherButtonTitles, nil];
}

- (void)showMessageLoginIfNotLogin:(UIViewController *)pView Message:(NSString *)pMessage Result:(ResultadoPorDefectoBlock)pResult
{
    [[Locator Default].Vistas AutoLogarUsuario:pView MostrarOcupado:true Result: ^
     {
         if ([Locator Default].Preferencias.EstaUsuarioLogado)
         {
             if (pResult)
             {
                 pResult(1);
             }
         }
         else
         {
             [self showMessage:Traducir(@"Inica sesión o registrate") Message:pMessage cancelButtonTitle:Traducir(@"Cancelar") AceptButtonTitle:Traducir(@"Iniciar sesión") completionBlock: ^(NSUInteger buttonIndex)
              {
                  if (buttonIndex == 1)
                  {
                      [[Locator Default].Vistas MostrarLoginUsuario:pView Result: ^(int pSalida)
                       {
                           if (pResult)
                           {
                               pResult(pSalida);
                           }
                       }];
                  }
              }];
         }
     }];     // devuelve true si ya esta logado
}

- (void)showBussy:(NSString *)pMensaje
{
    [self getProgress].labelText = pMensaje;
    
    if ([self getProgress].isShow == false)
    {
        [[self getProgress] show:true];
    }
}

- (void)hideBussy
{
    [[self getProgress] hide:true];
    
    //    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
    //        // Do something...
    //        dispatch_async(dispatch_get_main_queue(), ^{
    //            [[self getProgress] hide:true];
    //        });
    //    });
}

- (MBProgressHUD *)getProgress
{
    if (self.StaticProgres == nil)
    {
        self.StaticProgres = [MBProgressHUD createHubToView:self.StaticWindow];
        self.StaticProgres.mode = MBProgressHUDModeIndeterminate;
        self.StaticProgres.animationType = MBProgressHUDAnimationZoom;
        self.StaticProgres.color = [Locator Default].Color.FondoHUB;
        self.StaticProgres.labelFont = [Locator Default].FuenteLetra.Normal;
    }
    
    return self.StaticProgres;
}

- (void)MostrarAlerta:(NSString *)pTitulo Mensaje:(NSString *)pMensaje TituloCancel:(NSString *)pTituloCancel Resultado:(void (^)(NSUInteger buttonIndex))pResult OtrosTitulosBoton:(NSString *)otherButtonTitles, ...
{
    self.Alerta = [WCAlertView showAlertWithTitle:pTitulo
                                      message				:pMensaje
                              customizationBlock	:nil
                                completionBlock		: ^(NSUInteger buttonIndex)
                   {
                       if (pResult != nil)
                       {
                           pResult(buttonIndex);
                       }
                       
                       self.Alerta = nil;
                   }
                   
                               cancelButtonTitle	:pTituloCancel
                               otherButtonTitles	:otherButtonTitles, nil];
}

@end
