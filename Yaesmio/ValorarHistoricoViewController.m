//
//  ValorarHistoricoViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ValorarHistoricoViewController.h"
#import "UIAllControls.h"
#import "Emoticono.h"

@interface ValorarHistoricoViewController ()
@property (nonatomic) IBOutlet UILabel *labelPrecio;
@property (nonatomic) IBOutlet UILabel *labelDescripcionProducto;
@property (nonatomic) IBOutlet UILabel *labelNombreProducto;
@property (nonatomic) IBOutlet UIImageView *imageEmpresa;
@property (nonatomic) IBOutlet UILabel *labelFechaCompra;
@property (nonatomic) IBOutlet UIImageView *imageProducto;
@property (nonatomic) IBOutlet UILabel *labelEmpresa;
@property (nonatomic) IBOutlet UIImageView *imageCarita;
@property (nonatomic) IBOutlet UISlider *sliderValoracion;
@property (nonatomic) IBOutlet UIView *viewDatosProducto;
@property (nonatomic) IBOutlet UIView *viewProducto;
@property (nonatomic) IBOutlet UIScrollView *vistaScrollView;
@property (nonatomic) IBOutlet UILabel *labelNumeroValor;
@property (nonatomic) IBOutlet UILabel *labelTextoValoracion;
@property (nonatomic) IBOutlet UIView *viewTextoValora;
@property (nonatomic) IBOutlet MKGradientButton *btnValorar;
@property (nonatomic) IBOutlet UIActivityIndicatorView *controlBusy;
@property (weak, nonatomic) IBOutlet UITextView *textoValoracion;
@property (weak, nonatomic) IBOutlet UIView *viewValorarCompra;

@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaControles;

- (IBAction)SliderValoracion_changed:(id)sender;
- (IBAction)btnValorar_click:(id)sender;

@property (nonatomic) ScrollManager *manager;

@end

@implementation ValorarHistoricoViewController

#define GRUPO_VALORARHISTORICO @"ValorarHistoricoViewController"

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self Configurar];
    self.labelTextoValoracion.text = @"";
    Traducir(self.ListaControles);
    [[Locator Default].ServicioSOAP ObtenerTexto:@"ZVAL1" Result: ^(SRResultMethod pResult, NSString *pTexto)
     {
         if (pResult == SRResultMethodYES)
         {
             self.labelTextoValoracion.attributedText = [[Locator Default].FuenteLetra getTextoJustificado:pTexto];
             [self.labelTextoValoracion mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT];
             [self.viewTextoValora mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT];
             [self.view mgRelocateContent];       // recoloco todas las subvistas
             [self.vistaScrollView setHeightContentSizeForSubviewUntilControl:self.viewValorarCompra];       // calculo alto scroll
         }
     }];
    
    self.manager = [ScrollManager NewAndConfigureWithScrollView:self.vistaScrollView AndDidEndTextField:self Selector:@selector(btnValorar_click:)];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:true];
}

- (void)viewUnload
{
    [self setLabelPrecio:nil];
    [self setLabelDescripcionProducto:nil];
    [self setLabelNombreProducto:nil];
    [self setImageEmpresa:nil];
    [self setLabelFechaCompra:nil];
    [self setImageProducto:nil];
    [self setLabelEmpresa:nil];
    [self setImageCarita:nil];
    [self setSliderValoracion:nil];
    [self setViewDatosProducto:nil];
    [self setViewProducto:nil];
    [self setVistaScrollView:nil];
    [self setLabelNumeroValor:nil];
    [self setLabelTextoValoracion:nil];
    [self setViewTextoValora:nil];
    [self setBtnValorar:nil];
    [self setControlBusy:nil];
    [self setListaControles:nil];
    [self setTextoValoracion:nil];
    [self setManager:nil];
}

#pragma mark- Metodos Internos

- (void)inicializar
{
    self.labelDescripcionProducto.text  = @"";
    self.labelNombreProducto.text       = @"";
    self.labelPrecio.text               = @"";
    self.labelFechaCompra.text          = @"";
    self.labelEmpresa.text              = @"";
    self.imageProducto.image            = nil;
    self.imageEmpresa.image             = nil;
    self.textoValoracion.text           = @"";
}

- (void)Configurar
{
    if (self.ProductoHistorico != nil)
    {
        [self inicializar];
        
        self.labelDescripcionProducto.text = self.ProductoHistorico.TextoPropiedades;
        self.labelNombreProducto.text = self.ProductoHistorico.Nombre;
        self.labelPrecio.text = [NSString stringWithFormat:Traducir(@"Precio: %@ %@"),  [self.ProductoHistorico.Precio toStringFormaterCurrency], self.ProductoHistorico.AbreviaturaMoneda];
        self.labelFechaCompra.text = [self.ProductoHistorico.Fecha format:Traducir(@"dd/mm/yyyy")];
        self.labelEmpresa.text = self.ProductoHistorico.NombreEmpresa;
        [self.imageEmpresa cargarUrlImagenDerechaAsincrono:self.ProductoHistorico.LogoMarca.UrlPathSmall Grupo:GRUPO_VALORARHISTORICO];
        [self.imageProducto cargarUrlImagenAsincrono:self.ProductoHistorico.Imagen.UrlPathSmall Busy:self.controlBusy];
        [self CambiarEmoticono];
        [self.labelNombreProducto mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT];
        [self.labelDescripcionProducto mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT];
        [self.viewDatosProducto mgRelocateContent];
        [self.viewDatosProducto mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT];
        [self.viewProducto mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT AgregateWidth:0 AgregateHeight:10];
        [self.labelTextoValoracion mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT];
        [self.viewTextoValora mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT AgregateWidth:0 AgregateHeight:10];
        self.textoValoracion.BorderColor = [Locator Default].Color.Negro;
        [self.view mgRelocateContent]; // recoloco todas las subvistas
        [self.vistaScrollView setHeightContentSizeForSubviewUntilControl:self.viewValorarCompra]; // calculo alto scroll
    }
}

- (void)CambiarEmoticono
{
    Emoticono *lemoticono = [Emoticono getEmoticonoArray:self.ListaEmoticonos withID:[self getValorSlider]];
    
    if (lemoticono != nil)
    {
        [self.imageCarita cargarUrlImagenAsincrono:lemoticono.Imagen.UrlPathSmall Busy:nil];
    }
    
    self.labelNumeroValor.text = [NSString stringWithFormat:@"%d", [self getValorSlider]];
}

- (IBAction)SliderValoracion_changed:(id)sender
{
    [self CambiarEmoticono];
}

- (IBAction)btnValorar_click:(id)sender
{
    [[Locator Default].Alerta showBussy:Traducir(@"Guardando Valoración.")];
    [[Locator Default].ServicioSOAP ValorarProductoCompra:self.ProductoHistorico.ProductoId Pedido:self.ProductoHistorico.IdPedido Valoracion:[self getValorSlider] TextoValoracion:self.textoValoracion.text Result: ^(SRResultMethod pResult, NSString *pTexto)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 [[Locator Default].Alerta showMessageAcept:pTexto];
                 break;
                 
             case SRResultMethodYES:
                 self.ProductoHistorico.Valoracion = [self getValorSlider];
                 [[Locator Default].Alerta hideBussy];
                 [self.navigationController popViewControllerAnimated:true];
                 [Locator Default].Preferencias.NumeroComprasNoValoradas--;
                 
                 if (self.Resultado != Nil)
                 {
                     self.Resultado(1);
                 }
                 
                 break;
         }
     }];
}

- (int)getValorSlider
{
    int resul  = 0;
    float lvalor = self.sliderValoracion.value;
    
    if (lvalor < 0)
    {
        lvalor = 0.0;
    }
    
    if (lvalor >= 1)
    {
        lvalor = 0.9;
    }
    
    resul = (lvalor * 10) + 1;
    return resul;
}

@end
