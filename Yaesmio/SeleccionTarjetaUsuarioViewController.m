//
//  SeleccionTarjetaUsuarioViewController.m
//  Yaesmio
//
//  Created by freelance on 09/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SeleccionTarjetaUsuarioViewController.h"
#import "MetodoPagoCell.h"

@interface SeleccionTarjetaUsuarioViewController ()

@property (strong) NSArray *ListaTarjetas;
@property (copy) ResultadoSeleccionTarjetaUsuarioBlock Resultado;

@end

@implementation SeleccionTarjetaUsuarioViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewUnload
{
}

- (void)ConfigurarConListaTarjetas:(NSArray *)lista MostrarOtraTarjeta:(BOOL)muestraOtraTarjeta MostrarPaypal:(BOOL)muestraPaypal Resultado:(ResultadoSeleccionTarjetaUsuarioBlock)resultado
{
    NSMutableArray *prelista = [[NSMutableArray alloc] initWithArray:lista];
    
    if (muestraOtraTarjeta)
    {
        TarjetaUsuario *otra = [TarjetaUsuario Ninguna];
        [prelista addObject:otra];
    }
    
    if (muestraPaypal)
    {
        TarjetaUsuario *paypal = [TarjetaUsuario Paypal];
        [prelista addObject:paypal];
    }
    
    self.ListaTarjetas = prelista;
    self.Resultado = resultado;
}

#pragma mark- Delegate UITableView

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TarjetaUsuario *tarjeta = [self getTarjetaWithIndexPath:indexPath];
    
    if (tarjeta != nil && self.Resultado != nil)
    {
        self.Resultado(tarjeta);
        [self.navigationController popViewControllerAnimated:true];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ListaTarjetas.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MetodoPagoCell *celda = [tableView dequeueReusableCellWithIdentifier:@"MetodoPagoCell"];
    
    [celda ConfigurarConTarjetaUsuario:[self getTarjetaWithIndexPath:indexPath]];
    
    return celda;
}

#pragma mark- Privado

- (TarjetaUsuario *)getTarjetaWithIndexPath:(NSIndexPath *)index
{
    if (index.row < self.ListaTarjetas.count)
    {
        return self.ListaTarjetas[index.row];
    }
    
    return nil;
}

@end
