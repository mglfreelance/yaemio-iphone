//
//  Pagina2ViewController.m
//  Yaesmio
//
//  Created by freelance on 07/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "PaginaUsuarioViewController.h"


@interface PaginaUsuarioViewController ()
@property (strong, nonatomic)IBOutletCollection(UIButton) NSArray * ListaCamposTraducir;

@property (weak, nonatomic) IBOutlet UILabel *labelMisCompras;
@property (weak, nonatomic) IBOutlet UILabel *labelMisFavoritos;
@property (weak, nonatomic) IBOutlet UILabel *labelMisYasmys;
@property (weak, nonatomic) IBOutlet UIButton *botonInfo;
@property (weak, nonatomic) IBOutlet UIButton *botonAjustes;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *Loading;

- (IBAction)MostrarEdicionUsuarioExecute:(id)sender;
- (IBAction)MostrarHistoricoUsuarioExecute:(id)sender;
- (IBAction)MostrarNotificacionesRecibidadExecute:(id)sender;
- (IBAction)MostrarFavoritosExecute:(id)sender;
- (IBAction)MostrarInformacion:(id)sender;
- (IBAction)MostrarAjustes:(id)sender;

@end

@implementation PaginaUsuarioViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.Loading stopAnimating];
    self.view.backgroundColor = [Locator Default].Color.Transparente;
    self.navigationController.navigationBar.hidden = true;
    Traducir(self.ListaCamposTraducir);
    self.botonInfo.titleLabel.attributedText = [[Locator Default].FuenteLetra getTextoSubrayado:Traducir(@"Info")];
    self.botonAjustes.titleLabel.attributedText = [[Locator Default].FuenteLetra getTextoSubrayado:Traducir(@"Ajustes")];
    [self ObtenerDatosDePreferencias];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CambiadoNumeroNotificaciones:) name:PreferencesKeyNumeroNotificaciones object:[Locator Default].Preferencias];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = true;
    
    if (self.Loading.isHidden && [Locator Default].Preferencias.EstaUsuarioLogado)
    {
        [self.Loading startAnimating];
        [[Locator Default].ServicioSOAP ObtenerNumeroNotificacionesNoLeidas: ^(SRResultMethod pResult, NSString *MensajeSAP, NSInteger NumeroNotificacionesNoLeidas)
         {
             if (pResult == SRResultMethodYES)
             {
                 [Locator Default].Preferencias.NumeroNotificacionesSinLeer = NumeroNotificacionesNoLeidas;
             }
             else
             {
                 self.labelMisYasmys.text = @"";
             }
         }];
        
        [self ObtenerDatosDePreferencias];
    }
}

- (void)viewUnload
{
    [self setListaCamposTraducir:nil];
}

- (IBAction)MostrarEdicionUsuarioExecute:(id)sender
{
    if ([Locator Default].Preferencias.EstaUsuarioLogado)
    {
        [[Locator Default].Vistas MostrarMisDatos:self];
    }
    else
    {
        [[Locator Default].Alerta showMessageLoginIfNotLogin:self Message:Traducir(@"Por favor, inicia sesion o registrate para acceder a tus datos.") Result: ^(int pResult)
         {
             if (pResult == 1)          //OK
             {
                 [self MostrarEdicionUsuarioExecute:nil];
             }
         }];
    }
}

- (IBAction)MostrarHistoricoUsuarioExecute:(id)sender
{
    if ([Locator Default].Preferencias.EstaUsuarioLogado)
    {
        [[Locator Default].Vistas MostrarMiHistoricoDeCompras:self];
    }
    else
    {
        [[Locator Default].Alerta showMessageLoginIfNotLogin:self Message:Traducir(@"Por favor, inicia sesion o registrate para acceder a tu histórico de compras.") Result: ^(int pResult)
         {
             if (pResult == 1)          //OK
             {
                 [self MostrarHistoricoUsuarioExecute:nil];
             }
         }];
    }
}

- (IBAction)MostrarFavoritosExecute:(id)sender
{
    if ([Locator Default].Preferencias.EstaUsuarioLogado)
    {
        [[Locator Default].Vistas MostrarFavoritos:self];
    }
    else
    {
        [[Locator Default].Alerta showMessageLoginIfNotLogin:self Message:Traducir(@"Por favor, inicia sesion o registrate para acceder a tus favoritos.") Result: ^(int pResult)
         {
             if (pResult == 1)                         //OK
             {
                 [self MostrarFavoritosExecute:nil];
             }
         }];
    }
}

- (IBAction)MostrarInformacion:(id)sender
{
    [[Locator Default].Vistas MostrarInformacion:self];
}

- (IBAction)MostrarAjustes:(id)sender
{
    [[Locator Default].Vistas MostrarMisAjustes:self];
}

- (IBAction)MostrarNotificacionesRecibidadExecute:(id)sender
{
    if ([Locator Default].Preferencias.EstaUsuarioLogado)
    {
        [[Locator Default].Vistas MostrarNotificaciones:self];
    }
    else
    {
        [[Locator Default].Alerta showMessageLoginIfNotLogin:self Message:Traducir(@"Por favor, inicia sesion o registrate para acceder a tus yasmys.") Result: ^(int pResult)
         {
             if (pResult == 1)          //OK
             {
                 [self MostrarFavoritosExecute:nil];
             }
         }];
    }
}

- (void)CambiadoNumeroNotificaciones:(NSNotification *)pNotification
{
    [self ObtenerDatosDePreferencias];
}

- (void)ObtenerDatosDePreferencias
{
    self.labelMisYasmys.text = @"";
    self.labelMisCompras.text = @"";
    self.labelMisFavoritos.text = @"";
    
    if ([Locator Default].Preferencias.EstaUsuarioLogado)
    {
        if ([Locator Default].Preferencias.NumeroNotificacionesSinLeer > 0)
        {
            self.labelMisYasmys.text = [NSString stringWithFormat:Traducir(@"%d sin leer."), (int)[Locator Default].Preferencias.NumeroNotificacionesSinLeer];
        }
        
        if ([Locator Default].ProductosFavoritos.NumeroFavoritosToTal > 0)
        {
            self.labelMisFavoritos.text = [NSString stringWithFormat:Traducir(@"%d agregados."), [Locator Default].ProductosFavoritos.NumeroFavoritosToTal];
        }
        
        if ([Locator Default].Preferencias.NumeroComprasNoValoradas > 0)
        {
            self.labelMisCompras.text = [NSString stringWithFormat:Traducir(@"%d sin valorar."), [Locator Default].Preferencias.NumeroComprasNoValoradas];
        }
    }
    
    [self performSelector:@selector(PararAnimacionLoading) withObject:self afterDelay:1];
}

- (void)PararAnimacionLoading
{
    [self.Loading stopAnimating];
}

@end
