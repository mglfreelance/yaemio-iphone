//
//  VcardImporter.m
//  AddressBookVcardImport
//
//  Created by Alan Harper on 20/10/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "VcardImporter.h"
#import "BaseSixtyFour.h"

@implementation VcardImporter

NSArray *components;


+(BOOL)isFormatVcard:(NSString *)pString
{
    NSString *texto = [pString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return ([texto hasPrefix:@"BEGIN:VCARD"] &&  [texto hasSuffix:@"END:VCARD"]);
}

- (id) init
{
    if (self = [super init])
    {
        CFErrorRef myErr;
        self.addressBook = ABAddressBookCreateWithOptions(nil, &myErr);
    }
    
    return self;
}

- (void) dealloc
{
    CFRelease(self.addressBook);
}


-(void) parseString:(NSString*)pString
{
    NSArray *lines = [pString componentsSeparatedByString:@"\n"];
    
    for(NSString* line in lines)
    {
        [self parseLine:line];
    }
}

- (void) parseFilesVcfWithPath:(NSString*)path
{
    [self emptyAddressBook];
    
    NSString *filename = [[NSBundle mainBundle] pathForResource:path ofType:@"vcf"];
    NSLog(@"openning file %@", filename);
    NSData *stringData = [NSData dataWithContentsOfFile:filename];
    NSString *vcardString = [[NSString alloc] initWithData:stringData encoding:NSUTF8StringEncoding];
    
    [self parseString:vcardString];
    ABAddressBookSave(self.addressBook, NULL);
}

- (void) parseLine:(NSString *)line {
    if (self.base64image && [line hasPrefix:@"  "]) {
        NSString *trimmedLine = [line stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        self.base64image = [self.base64image stringByAppendingString:trimmedLine];
    } else if (self.base64image) {
        // finished contatenating image string
        [self parseImage];
    } else if ([line hasPrefix:@"BEGIN"]) {
        self.personRecord = ABPersonCreate();
    } else if ([line hasPrefix:@"END"]) {
        //ABAddressBookAddRecord(self.addressBook, self.personRecord, NULL);
    } else if ([line hasPrefix:@"N:"]) {
        [self parseName:line];
    } else if ([line hasPrefix:@"ORG:"]) {
        [self parseORG:line];
    } else if ([line hasPrefix:@"URL:"]) {
        [self parseURL:line];
    } else if ([line hasPrefix:@"TEL"]) {
        [self parseTel:line];
    } else if ([line hasPrefix:@"EMAIL;"]) {
        [self parseEmail:line];
    } else if ([line hasPrefix:@"ADR:"]) {
        [self parseDir:line];
    } else if ([line hasPrefix:@"PHOTO;BASE64"]) {
        self.base64image = [NSString string];
    }
}

- (void) parseName:(NSString *)line {
    NSArray *upperComponents = [line componentsSeparatedByString:@":"];
    NSArray *components = [[upperComponents objectAtIndex:1] componentsSeparatedByString:@";"];
    
    if (components.count>0)
        ABRecordSetValue (self.personRecord, kABPersonLastNameProperty,(__bridge CFTypeRef)([components objectAtIndex:0]), NULL);
    if (components.count>1)
        ABRecordSetValue (self.personRecord, kABPersonFirstNameProperty,(__bridge CFTypeRef)([components objectAtIndex:1]), NULL);
    if (components.count>2)
        ABRecordSetValue (self.personRecord, kABPersonPrefixProperty,(__bridge CFTypeRef)([components objectAtIndex:2]), NULL);
}

- (void) parseDir:(NSString *)line {
    NSArray *upperComponents = [line componentsSeparatedByString:@":"];
    if (upperComponents.count >1)
    {
        components = [[upperComponents objectAtIndex:1] componentsSeparatedByString:@";"];
        
        
        ABMutableMultiValueRef address = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
        NSMutableDictionary *addressDict = [[NSMutableDictionary alloc] init];
        if (components.count>2 && [components[2] isEqualToString:@""] == false)
            [addressDict setObject:components[2] forKey:(NSString *)kABPersonAddressStreetKey];
        if (components.count>3 && [components[3] isEqualToString:@""] == false)
            [addressDict setObject:components[3] forKey:(NSString *)kABPersonAddressCityKey];
        if (components.count>4 && [components[4] isEqualToString:@""] == false)
            [addressDict setObject:components[4] forKey:(NSString *)kABPersonAddressStateKey];
        if (components.count>5 && [components[5] isEqualToString:@""] == false)
            [addressDict setObject:components[5] forKey:(NSString *)kABPersonAddressZIPKey];
        if (components.count>6 && [components[6] isEqualToString:@""] == false)
            [addressDict setObject:components[6] forKey:(NSString *)kABPersonAddressCountryKey];
        ABMultiValueAddValueAndLabel(address, CFBridgingRetain(addressDict), kABWorkLabel, NULL);
        ABRecordSetValue(self.personRecord, kABPersonAddressProperty, address, NULL);
        CFRelease(address);
    }
}

-(void) parseORG:(NSString*)line
{
    line = [line stringByReplacingOccurrencesOfString:@"ORG:" withString:@""];
    ABRecordSetValue(self.personRecord, kABPersonOrganizationProperty,(__bridge CFTypeRef) line, NULL);
}

-(void) parseURL:(NSString*)line
{
    NSString *strURl = [line stringByReplacingOccurrencesOfString:@"URL:" withString:@""];
    ABMutableMultiValueRef multiweb = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiweb, CFBridgingRetain(strURl), kABWorkLabel, NULL);
    ABRecordSetValue(self.personRecord, kABPersonURLProperty, multiweb, NULL);
}

- (void) parseEmail:(NSString *)line {
    NSArray *mainComponents = [line componentsSeparatedByString:@":"];
    NSString *emailAddress = [mainComponents objectAtIndex:1];
    CFStringRef label;
    ABMutableMultiValueRef multiEmail;
    
    if ([line rangeOfString:@"WORK"].location != NSNotFound) {
        label = kABWorkLabel;
    } else if ([line rangeOfString:@"HOME"].location != NSNotFound) {
        label = kABHomeLabel;
    } else {
        label = kABOtherLabel;
    }
    
    ABMultiValueRef immutableMultiEmail = ABRecordCopyValue(self.personRecord, kABPersonEmailProperty);
    if (immutableMultiEmail) {
        multiEmail = ABMultiValueCreateMutableCopy(immutableMultiEmail);
    } else {
        multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    }
    ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)(emailAddress), label, NULL);
    ABRecordSetValue(self.personRecord, kABPersonEmailProperty, multiEmail,nil);
    
    CFRelease(multiEmail);
    if (immutableMultiEmail) {
        CFRelease(immutableMultiEmail);
    }
}

- (void) parseTel:(NSString *)line {
    NSArray *mainComponents = [line componentsSeparatedByString:@":"];
    NSString *emailAddress = [mainComponents objectAtIndex:1];
    CFStringRef label;
    ABMutableMultiValueRef multiEmail;
    
    if ([line rangeOfString:@"FAX"].location != NSNotFound) {
        label = kABWorkLabel;
    } else if ([line rangeOfString:@"CELL"].location != NSNotFound) {
        label = kABHomeLabel;
    } else {
        label = kABOtherLabel;
    }
    
    ABMultiValueRef immutableMultiEmail = ABRecordCopyValue(self.personRecord, kABPersonPhoneProperty);
    if (immutableMultiEmail) {
        multiEmail = ABMultiValueCreateMutableCopy(immutableMultiEmail);
    } else {
        multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    }
    ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)(emailAddress), label, NULL);
    ABRecordSetValue(self.personRecord, kABPersonPhoneProperty, multiEmail,nil);
    
    CFRelease(multiEmail);
    if (immutableMultiEmail) {
        CFRelease(immutableMultiEmail);
    }
}

- (void) parseImage {
    NSData *imageData = [BaseSixtyFour decode:self.base64image];
    self.base64image = nil;
    ABPersonSetImageData(self.personRecord, (__bridge CFDataRef)imageData, NULL);
    
}
- (void) emptyAddressBook {
    CFArrayRef people = ABAddressBookCopyArrayOfAllPeople(self.addressBook);
    CFIndex arrayCount = CFArrayGetCount(people);
    ABRecordRef abrecord;
    
    for (int i = 0; i < arrayCount; i++) {
        abrecord = CFArrayGetValueAtIndex(people, i);
        ABAddressBookRemoveRecord(self.addressBook,abrecord, NULL);
    }
    CFRelease(people);
}
@end
