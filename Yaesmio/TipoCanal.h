//
//  TipoCanal.h
//  Yaesmio
//
//  Created by freelance on 12/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM (NSInteger, TipoCanalProducto)
{
    TipoCanalProductoEscanear     = 0,
    TipoCanalProductoBusqueda     = 1,
    TipoCanalProductoNotificacion = 2,
    TipoCanalProductoFavorito     = 3,
};

@interface TipoCanal : NSObject

@property (assign)    TipoCanalProducto TipoCanal;
@property (readonly)  NSString *ID;

- (BOOL)		isEqualTo:(TipoCanal *)pTipo;
- (id)init:(TipoCanalProducto)pTipo;

+ (instancetype)TipoCanalEscanear;
+ (instancetype)TipoCanalBusqueda;
+ (instancetype)TipoCanalNotificacion;
+ (instancetype)TipoCanalFavorito;

@end
