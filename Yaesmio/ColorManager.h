//
//  ColorManager.h
//  yaesmio
//
//  Created by manuel garcia lopez on 06/05/13.
//  Copyright (c) 2013 manuel garcia lopez. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface ColorManager : NSObject

@property (readonly) UIColor *Principal;
@property (readonly) UIColor *PrincipalOscuro;
@property (readonly) NSString *PrincipalHexadecimal;
@property (readonly) UIColor *Gris;
@property (readonly) NSString *GrisHexadecimal;
@property (readonly) UIColor *GrisClaro;
@property (readonly) UIColor *GrisOscuro;
@property (readonly) NSArray *DegradadoFondo;
@property (readonly) NSArray *DegradadoBoton;
@property (readonly) NSArray *DegradadoBotonDesactivado;
@property (readonly) NSArray *DegradadoClaroBoton;
@property (readonly) NSArray *DegradadoBarra;
@property (readonly) UIImage *ImageBarra;
@property (readonly) UIColor *InicioBarra;
@property (readonly) UIColor *FinBarra;
@property (readonly) UIColor *SombraTextoAlertView;
@property (readonly) UIColor *Clear;
@property (readonly) UIColor *Blanco;
@property (readonly) NSArray *DegradadoAlertView;
@property (readonly) NSArray *DegradadoDescripcionProducto;
@property (readonly) UIColor *FondoHUB;
@property (readonly) NSArray *DegradadoFondoCompartir;
@property (readonly) UIColor *PrincipalDesactivado;
@property (readonly) UIColor *PrincipalTransparente;
@property (readonly) UIColor *Rojo;
@property (readonly) NSString *RojoHexadecimal;
@property (readonly) UIColor *Verde;
@property (readonly) UIColor *Azul;
@property (readonly) UIColor *Texto;
@property (readonly) NSString *TextoHexadecimal;
@property (readonly) UIColor *Transparente;
@property (readonly) UIColor *Negro;

- (CAGradientLayer *)getLayerColorPrincipal:(CGRect)pFrame;

@end
