//
//  VideoViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/10/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <MediaPlayer/MediaPlayer.h>
#import <UIKit/UIKit.h>

@interface VideoViewController : MPMoviePlayerViewController

typedef void (^ResultTimerVideoBlock)(NSTimeInterval pTimer);

@property (strong) NSString                 *UrlVideo;
@property (strong) NSString                 *UrlImageEmpresa;
@property (assign) NSTimeInterval           Timer;
@property (copy)   ResultTimerVideoBlock    Result;

-(id)initWithContentURL:(NSURL *)contentURL LogoEmpresa:(NSString*)pUrlLogoEmpresa Timer:(NSTimeInterval)pTimer Resul:(ResultTimerVideoBlock) pResult;

@end
