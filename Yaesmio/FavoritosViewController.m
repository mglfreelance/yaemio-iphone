//
//  FavoritosViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 20/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "FavoritosViewController.h"
#import "UIAllControls.h"
#import "FavoritoCell.h"

typedef NS_ENUM (NSInteger, ModoVistaFavoritos)
{
    ModoVistaFavoritosLista   = 0,
    ModoVistaFavoritosMediano = 1,
    ModoVistaFavoritosGrande  = 2,
};

@interface FavoritosViewController () <ProductosFavoritosManagerDelegate, UICollectionViewDelegate, UICollectionViewDataSource, M13ContextMenuDelegate>

@property (nonatomic) IBOutlet UILabel *labelNumeroProductosLista;
@property (nonatomic) IBOutlet UICollectionView *controlTablaFavoritos;
@property (nonatomic) IBOutlet UIView *VistaSinFavoritos;
@property (nonatomic) IBOutlet UILabel *labelNohayFavoritos;
@property (nonatomic) IBOutlet UIView *ViewLoading;
@property (nonatomic) IBOutlet UILabel *labelLoading;
@property (weak, nonatomic) IBOutlet UIView *viewBotonera;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButtonAtras;
@property (weak, nonatomic) IBOutlet UIToolbar *ToolbarFavoritos;

@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCamposTraduccion;

- (IBAction)CambiarTipoVistaExecute:(id)sender;
- (IBAction)CerrarModalExecute:(id)sender;

@property (nonatomic, assign) BOOL NoExisteMasFavoritos;
@property (strong, readonly)  NSMutableArray *ListaFavoritos;
@property (nonatomic) ModoVistaFavoritos ModoVisualizacionVista;
@property (strong) M13ContextMenu *menu;
@property (strong) M13ContextMenuItemIOS7 *MenuBorrar;

@end

@implementation FavoritosViewController

@synthesize ListaFavoritos = _ListaFavoritos;
@synthesize RefrescarLista;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaCamposTraduccion);
    Traducir(self.barButtonAtras);
    self.VistaSinFavoritos.hidden = false;
    self.NoExisteMasFavoritos = false;
    self.labelNumeroProductosLista.text = @"";
    self.ViewLoading.hidden = true;
    self.RefrescarLista = true;
    self.ModoVisualizacionVista = ModoVistaFavoritosMediano;
    [self configurarContextMenu];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
    
    if (self.RefrescarLista)
    {
        _ListaFavoritos = nil;
        self.VistaSinFavoritos.hidden = false;
        self.NoExisteMasFavoritos = false;
        self.labelNumeroProductosLista.text = [self getCadenaCompraRealizada:[Locator Default].ProductosFavoritos.NumeroFavoritosToTal];
        self.ViewLoading.hidden = true;
        [self CambiadoElementos];
        self.RefrescarLista = false;
    }
    
    UIMenuItem *menuItem = [[UIMenuItem alloc] initWithTitle:Traducir(@"Eliminar") action:@selector(EliminarFavorito:)];
    [[UIMenuController sharedMenuController] setMenuItems:@[menuItem]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIMenuController sharedMenuController] setMenuItems:nil];
}

- (void)viewUnload
{
    [self setLabelNumeroProductosLista:nil];
    [self setControlTablaFavoritos:nil];
    [self setVistaSinFavoritos:nil];
    [self setListaCamposTraduccion:nil];
    [self setLabelNohayFavoritos:nil];
    [super viewUnload];
}

- (BOOL)canBecomeFirstResponder
{
    // NOTE: This menu item will not show if this is not YES!
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    // The selector(s) should match your UIMenuItem selector
    if (action == @selector(EliminarFavorito:))
    {
        return YES;
    }
    
    return NO;
}

- (IBAction)CambiarTipoVistaExecute:(id)sender
{
    switch (self.ModoVisualizacionVista)
    {
        case ModoVistaFavoritosGrande:
            self.ModoVisualizacionVista = ModoVistaFavoritosMediano;
            break;
            
        case ModoVistaFavoritosMediano:
            self.ModoVisualizacionVista = ModoVistaFavoritosLista;
            break;
            
        case ModoVistaFavoritosLista:
            self.ModoVisualizacionVista = ModoVistaFavoritosGrande;
            break;
    }
    
    [self.controlTablaFavoritos performBatchUpdates:nil completion:nil];
    [self.controlTablaFavoritos reloadSections:[[NSIndexSet alloc] initWithIndex:0]];
}

- (IBAction)CerrarModalExecute:(id)sender
{
    [self CerrarModal];
}

#pragma mark- Delegado ProductosFavoritos

- (void)CambiadoElementos
{
    _ListaFavoritos = [NSMutableArray new];
    self.NoExisteMasFavoritos = FALSE;
    self.labelNumeroProductosLista.hidden = true;
    [self ObtenerSiguienteListaFavoritos];
}

#pragma mark- Delegate CollectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.ListaFavoritos.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    FavoritoCell *lVistaCelda = [collectionView dequeueReusableCellWithReuseIdentifier:[self getIDCell] forIndexPath:indexPath];
    
    if (self.ListaFavoritos.count > indexPath.row)
    {
        Favorito *lFavorito = self.ListaFavoritos[indexPath.row];
        
        [lVistaCelda ConfigurarConFavorito:lFavorito MostrarImagenGrande:(self.ModoVisualizacionVista == ModoVistaFavoritosGrande) Menu:self.menu];
        
        if (indexPath.row == self.ListaFavoritos.count - 1)
        {
            [self ObtenerSiguienteListaFavoritos];
        }
    }
    
    return lVistaCelda;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.ModoVisualizacionVista)
    {
        case ModoVistaFavoritosGrande:
            return CGSizeMake(320.f, 197.f);
            
        case ModoVistaFavoritosMediano:
            return CGSizeMake(156.f, 150.f);
            
        case ModoVistaFavoritosLista:
            return CGSizeMake(320.f, 67.f);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Favorito *pBuscado = self.ListaFavoritos[indexPath.row];
    
    if (pBuscado != nil && pBuscado.Caducado == false)
    {
        [[Locator Default].Alerta showBussy:Traducir(@"Cargando Producto.")];
        [[Locator Default].ServicioSOAP ObtenerProducto:pBuscado.ProductoId Medio:@"" Canal:TipoCanalProductoFavorito Resultado: ^(SRResultObtenerProducto pResult, NSString *pMensajeError, ProductoPadre *pProductoPadre)
         {
             switch (pResult)
             {
                 case SRResultObtenerProductoNoYaesmio:
                 case SRResultObtenerProductoErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
                     
                 case SRResultObtenerProductoNoExiste:
                     [[Locator Default].Alerta showMessageAcept:pMensajeError];
                     break;
                     
                 case SRResultObtenerProductoYES:
                     [[Locator Default].Alerta hideBussy];
                     [[Locator Default].Vistas MostrarProducto:self Producto:pProductoPadre VieneDeFavoritos:true];
                     break;
             }
         }];
    }
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    Favorito *pBuscado = self.ListaFavoritos[indexPath.row];
    
    if (pBuscado != nil)
    {
        return (pBuscado.Caducado == false);
    }
    
    return false;
}

#pragma  mark- Delegate ContextMenu

- (BOOL)shouldShowContextMenu:(M13ContextMenu *)contextMenu atPoint:(CGPoint)point
{
    return true;
}

- (void)contextMenu:(M13ContextMenu *)contextMenu atPoint:(CGPoint)point didSelectItemAtIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [self.controlTablaFavoritos indexPathForItemAtPoint:point];
    
    if (indexPath != nil && indexPath.row < self.ListaFavoritos.count)
    {
        Favorito *pBuscado = self.ListaFavoritos[indexPath.row];
        
        if (pBuscado)
        {
            [self EliminarFavorito:pBuscado];
        }
    }
}

#pragma mark- Metodos Privados

- (NSString *)getIDCell
{
    switch (self.ModoVisualizacionVista)
    {
        case ModoVistaFavoritosGrande:
            return @"Celda_Favorito_Grande";
            
        case ModoVistaFavoritosMediano:
            return @"Celda_Favorito_Mediana";
            
        case ModoVistaFavoritosLista:
            return @"Celda_Favorito_Lista";
    }
}

- (void)MostrarLoading:(BOOL)mostrar
{
    self.ViewLoading.hidden = mostrar;
    self.labelNohayFavoritos.hidden = !mostrar;
    self.ToolbarFavoritos.hidden = !mostrar;
}

- (void)ObtenerSiguienteListaFavoritos
{
    if (self.NoExisteMasFavoritos == false)
    {
        self.labelLoading.text =  Traducir(@"Obteniendo Favoritos.");
        [self MostrarLoading:false];
        [[Locator Default].ProductosFavoritos ObtenerListaFavoritos:self.ListaFavoritos.count + 1.0 Result: ^(SRResultProductoFavoritoManager pResult, NSArray *pLista, NSString *pMensaje, int pNumeroFavoritosTotales)
         {
             [self MostrarLoading:true];
             switch (pResult)
             {
                 case SRResultProductoFavoritoManagerNO:
                     self.VistaSinFavoritos.hidden = false;
                     self.NoExisteMasFavoritos = false;
                     self.labelNumeroProductosLista.text = @"";
                     [[Locator Default].Alerta showMessageAcept:pMensaje];
                     break;
                     
                 case SRResultProductoFavoritoManagerErrConex:
                     self.VistaSinFavoritos.hidden = false;
                     self.NoExisteMasFavoritos = false;
                     self.labelNumeroProductosLista.text = @"";
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
                     
                 case SRResultProductoFavoritoManagerYES:
                     [self ActualizarCollectioview:pLista pNumeroFavoritosTotales:pNumeroFavoritosTotales];
                     break;
             }
         }];
    }
}

- (void)ActualizarCollectioview:(NSArray *)pLista pNumeroFavoritosTotales:(int)pNumeroFavoritosTotales
{
    [self.controlTablaFavoritos performBatchUpdates: ^
     {
         NSInteger resultsSize = self.ListaFavoritos.count;
         [self.ListaFavoritos addObjectsFromArray:pLista];
         NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
         
         for (NSInteger i = resultsSize; i < resultsSize + pLista.count; i++)
         {
             [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
         }
         
         self.NoExisteMasFavoritos = (self.ListaFavoritos.count == pNumeroFavoritosTotales);
         [self.controlTablaFavoritos insertItemsAtIndexPaths:arrayWithIndexPaths];
     }							completion:nil];
    self.labelNumeroProductosLista.hidden = false;
    self.labelNumeroProductosLista.text = [self getCadenaCompraRealizada:-1];
    self.VistaSinFavoritos.hidden = (self.ListaFavoritos.count != 0);
    self.viewBotonera.hidden = (self.ListaFavoritos.count == 0);
}

- (NSMutableArray *)ListaFavoritos
{
    if (_ListaFavoritos == nil)
    {
        _ListaFavoritos = [NSMutableArray new];
    }
    
    return _ListaFavoritos;
}

- (NSString *)getCadenaCompraRealizada:(int)pNumCompras
{
    if (pNumCompras == -1)
    {
        if (self.ListaFavoritos.count > 0)
        {
            Favorito *myFavorito = [self.ListaFavoritos firstObject];
            
            if (myFavorito != nil)
            {
                return [self getCadenaCompraRealizada:myFavorito.NumElementos.intValue];
            }
        }
    }
    else if (pNumCompras == 1)
    {
        return [NSString stringWithFormat:Traducir(@"Tienes %d producto en tu lista."), pNumCompras];
    }
    else
    {
        return [NSString stringWithFormat:Traducir(@"Tienes %d productos en tu lista."), pNumCompras];
    }
    
    return @"";
}

- (void)EliminarFavorito:(Favorito *)sender
{
    [[Locator Default].Alerta showMessage:Traducir(@"Borrar Favorito") Message:Traducir(@"Realmente quieres eliminar este producto de tus favoritos?") cancelButtonTitle:Traducir(@"Cancelar") AceptButtonTitle:Traducir(@"Aceptar") completionBlock: ^(NSUInteger buttonIndex)
     {
         if (buttonIndex == 1)
         {
             [[Locator Default].Alerta showBussy:Traducir(@"Borrado Producto en Favoritos.")];
             [[Locator Default].ProductosFavoritos EliminarProducto:sender Result: ^(SRResultProductoFavoritoManager pResult, NSString *pMensaje)
              {
                  switch (pResult)
                  {
                      case SRResultProductoFavoritoManagerErrConex:
                          [[Locator Default].Alerta showErrorConexSOAP];
                          break;
                          
                      case SRResultProductoFavoritoManagerNO:
                          [[Locator Default].Alerta showMessageAcept:pMensaje];
                          break;
                          
                      case SRResultProductoFavoritoManagerYES:
                          [self.ListaFavoritos removeObject:sender];
                          [self.controlTablaFavoritos reloadData];
                          break;
                  }
                  [[Locator Default].Alerta hideBussy];
              }];
         }
     }];
}

- (void)configurarContextMenu
{
    //Create the items
    self.MenuBorrar = [[M13ContextMenuItemIOS7 alloc] initWithUnselectedIcon:[UIImage imageNamed:@"MenuPapelera"] selectedIcon:[UIImage imageNamed:@"MenuPapeleraSeleccionada"]];
    
    self.MenuBorrar.tintColor = [Locator Default].Color.Principal;
    
    //Create the menu
    self.menu = [[M13ContextMenu alloc] initWithMenuItems:@[self.MenuBorrar]];
    self.menu.delegate = self;
    self.menu.tintColor = [Locator Default].Color.Principal;
    self.menu.BorderColor = [Locator Default].Color.Principal;
    self.menu.originationCircleStrokeColor = [Locator Default].Color.GrisOscuro;
}

@end
