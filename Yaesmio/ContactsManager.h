//
//  ContactsManager.h
//  Yaesmio
//
//  Created by freelance on 21/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContactsManager : NSObject

@property (readonly) BOOL seTieneAcceso;

// siempre llamarlo en el load de la view controller
- (void)		ComprobarAccesoAddressBook;

// obtiene el nombre del addressbook por mail
- (NSString *)	getNombreContactoPorMail:(NSString *)pMail;

// obtiene la foto del contacto si no existe devuelve una generica
- (UIImage *)	getImagenContactoPorMail:(NSString *)pMail;

// devuelve una lista de contactos que tienen mail
- (NSArray *)	getListaContactosConMailOTelefono;
@end
