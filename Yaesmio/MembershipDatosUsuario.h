//
//  MembershipDatosUsuario.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "SOAP_USGlobals.h"
#import "SoapObject.h"

@interface MembershipDatosUsuario : SoapObject
{
}

+ (MembershipDatosUsuario *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * Apellidos;
@property (strong) NSString * Fecha_nacimiento;
@property (strong) NSString * Idioma;
@property (strong) NSString * Login;
@property (strong) NSString * Nombre;
@property (strong) NSString * Password;
@property (strong) NSString * PreguntaPassword;
@property (strong) NSString * RespuestaPassword;
@property (strong) NSString * Sexo;
@property (assign) BOOL       aceptaEnvioPublicidad;

@end

