//
//  ProductoDestacadoCell.h
//  Yaesmio
//
//  Created by freelance on 21/08/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductoDestacado.h"

@interface ProductoDestacadoCell : UICollectionViewCell

- (void)ActualizarConProducto:(ProductoDestacado *)pProducto;

@end
