//
//  MainViewController.m
//  Yaesmio
//
//  Created by freelance on 08/03/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "MainViewController.h"
#import "UIAllControls.h"
#import "ConfigurationParameter.h"

@interface MainViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *FondoImagen;

@property (strong) NSString *urlImagenFondoApp;

@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.hidden = true;
    self.FondoImagen.image = [Locator Default].Imagen.ImagenFondoPantallaPrincipal;
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    if ([Locator Default].Preferencias.MostrarPantallaQR == false)
    {
        [self performSelector:@selector(loadAuthenticateViewController) withObject:nil afterDelay:0.0];
    }
    else
    {
        // para obtener el identifacionID y asi poder hacer las llamadas a producto
        [[Locator Default].Vistas AutoLogarUsuario:self MostrarOcupado:false Result: ^
         {
             [self ActualizarFondoVista:[Locator Default].Imagen.ImagenFondoPantallaPrincipal];
             
             if ([Locator Default].Preferencias.EstaUsuarioLogado == false && [Locator Default].Preferencias.IdPush.isNotEmpty && [Locator Default].Preferencias.IdUsuario.isNotEmpty)
             {     //si tenemos idpush y no se autologa desactivamos notificaciones
                 [[Locator Default].ServicioSOAP DesactivarNotificacionesPush:[Locator Default].Preferencias.IdPush DeUsuario:[Locator Default].Preferencias.IdUsuario MacDevice:[Locator Default].Device.DeviceID16Long Telefono:@"" Result:nil];
             }
         }];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.hidden = true;
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
    [self RefrescarConfiguracionApp];
}

- (void)viewUnload
{
    [super viewUnload];
}

- (void)loadAuthenticateViewController
{
    [[Locator Default].Vistas MostrarBienvenida:self];
}

- (void)ActualizarFondoVista:(UIImage *)pImage
{
    [UIView transitionWithView:self.FondoImagen duration:3.0f options:UIViewAnimationOptionTransitionCrossDissolve animations: ^
     {
         self.FondoImagen.image = pImage;
     }		completion:nil];
}

- (void)RefrescarConfiguracionApp
{
    [[Locator Default].Vistas ObtenerConfiguracionApp:^(int pResult)
     {
         if (pResult == true)
         {
             if ([Locator Default].Imagen.UrlImagenFondoPantallaPrincipal.isNotEmpty && [self.urlImagenFondoApp isEqualToString:[Locator Default].Imagen.UrlImagenFondoPantallaPrincipal] == false)
             {
                 self.urlImagenFondoApp = [Locator Default].Imagen.UrlImagenFondoPantallaPrincipal;
                 [UIImage ImageAssyncFromUrl:[NSURL URLWithString:[Locator Default].Imagen.UrlImagenFondoPantallaPrincipal] Grupo:@"main" Result:^(UIImage *pResult)
                  {
                      [self ActualizarFondoVista:pResult];
                  }];
             }
         }
     }];
}

@end
