//
//  UIWebView+Ampliado.m
//  Yaesmio
//
//  Created by mglFreelance on 04/12/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "UIWebView+Ampliado.h"

@implementation UIWebView (Ampliado)


-(void) Clear
{
    if ([self isLoading])
        [self stopLoading];
    
    [self loadHTMLString: @"" baseURL: nil];
}

@end
