//
//  Contacto.m
//  Yaesmio
//
//  Created by freelance on 21/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "Contacto.h"

@implementation Contacto

- (id)init
{
    if ((self = [super init]))
    {
        self.MailUsuario      = @"";
        self.UsuarioId        = @"";
        self.Apellidos        = @"";
        self.Nombre           = @"";
        self.Telefono         = @"";
        self.ContactoTelefono = nil;
        self.nsPrefix         = @"tns3";
    }
    
    return self;
}

- (instancetype)initWithPerson:(RHPerson *)Person
{
    if ((self = [self init]))
    {
        self.ContactoTelefono = Person;
    }
    
    return self;
}

- (instancetype)initWithMail:(NSString *)mail
{
    if ((self = [self init]))
    {
        self.MailUsuario = mail;
    }
    
    return self;
}

- (instancetype)initWithPhone:(NSString *)phone
{
    if ((self = [self init]))
    {
        self.Telefono = phone;
    }
    
    return self;
}

- (void)copiarDatosEn:(Contacto *)pContacto
{
    pContacto.MailUsuario =  self.MailUsuario;
    pContacto.UsuarioId = self.UsuarioId;
    pContacto.Apellidos = self.Apellidos;
    pContacto.Nombre = self.Nombre;
    pContacto.Telefono = self.Telefono;
}

+ (Contacto *)deserializeNode:(xmlNodePtr)cur
{
    Contacto *newObject = [[Contacto alloc] init];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (UIImage *)ImagenAMostrar
{
    if (self.ContactoTelefono != nil && self.ContactoTelefono.hasImage)
    {
        return self.ContactoTelefono.originalImage;
    }
    else
    {
        return nil;
    }
}

- (NSString *)NombreAMostrar
{
    if (self.ContactoTelefono != nil && self.ContactoTelefono.name.isNotEmpty)
    {
        return self.ContactoTelefono.name;
    }
    else if (self.Nombre.isNotEmpty)
    {
        return self.Nombre;
    }
    else if (self.MailUsuario.isNotEmpty)
    {
        return self.MailUsuario;
    }
    else
    {
        return self.Telefono;
    }
}

- (NSString *)TelefonoPrincipal
{
    if (self.Telefono.isNotEmpty)
    {
        return self.Telefono;
    }
    else if (self.ContactoTelefono != nil)
    {
        return self.ContactoTelefono.phoneNumbers.values.firstObject;
    }
    else
    {
        return @"";
    }
}

- (NSString *)mailPrincipal
{
    if (self.MailUsuario.isNotEmpty)
    {
        return self.MailUsuario;
    }
    else if (self.ContactoTelefono != nil)
    {
        return self.ContactoTelefono.emails.values.firstObject;
    }
    else
    {
        return @"";
    }
}

- (BOOL)ContieneMailDelista:(NSArray *)pListaMail2
{
    NSString *mail = self.mailPrincipal;
    
    if (mail.isNotEmpty)
    {
        for (NSString *email2 in pListaMail2)
        {
            if ([mail isEqualToString:email2])
            {
                return true;
            }
        }
    }
    
    if (self.ContactoTelefono != nil)
    {
        for (NSString *email1 in self.ContactoTelefono.emails.values)
        {
            for (NSString *email2 in pListaMail2)
            {
                if ([email1 isEqualToString:email2])
                {
                    return true;
                }
            }
        }
    }
    
    return false;
}

- (Contacto *)BuscarEnLista:(NSArray *)pLista
{
    for (Contacto *actual in pLista)
    {
        if (self.UsuarioId.isNotEmpty && [actual.UsuarioId isEqualToString:self.UsuarioId])
        {
            return actual;
        }
        else if ([self.mailPrincipal isEqualToString:actual.mailPrincipal])
        {
            return actual;
        }
        else if (actual.ContactoTelefono != nil && [self ContieneMailDelista:actual.ContactoTelefono.emails.values])
        {
            return actual;
        }
    }
    
    return nil;
}

#pragma mark- SOAPOBJECT

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Apellidos xmlNodeForDoc:node->doc elementName:@"Apellidos" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.MailUsuario xmlNodeForDoc:node->doc elementName:@"MailUsuario" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Nombre xmlNodeForDoc:node->doc elementName:@"Nombre" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Telefono xmlNodeForDoc:node->doc elementName:@"Telefono" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.UsuarioId xmlNodeForDoc:node->doc elementName:@"UsuarioId" elementNSPrefix:@"tns3"]);
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"Apellidos"])
    {
        self.Apellidos = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"MailUsuario"])
    {
        self.MailUsuario = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Nombre"])
    {
        self.Nombre = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Telefono"])
    {
        self.Telefono = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"UsuarioId"])
    {
        self.UsuarioId = [NSString deserializeNode:pobjCur];
    }
}

@end
