//
//  CrearCuentaViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YaesmioViewControllers.h"

@interface CrearCuentaViewController : YaesmioViewController

@property (copy) ResultadoPorDefectoBlock Resultado;
@property (nonatomic) UIViewController *VistaInicial;

@end
