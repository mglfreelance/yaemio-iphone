//
//  PickerFechaViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 26/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "PickerFechaViewController.h"

@interface PickerFechaViewController ()

@property (nonatomic) IBOutlet UIBarButtonItem *controlBotonCancelar;
@property (nonatomic) IBOutlet UIBarButtonItem *controlBotonAceptar;
@property (nonatomic) IBOutlet UIToolbar       *controlToolBar;
@property (nonatomic) IBOutlet UIDatePicker    *ControlPicker;

- (IBAction)btnDone_click:(id)sender;
- (IBAction)btnCancel_click:(id)sender;
- (IBAction)DatePicker_ValueChanged:(id)sender;


@end

@implementation PickerFechaViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.controlBotonAceptar.title = Traducir(@"Aceptar");
    self.controlBotonCancelar.title = Traducir(@"Cancelar");
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        self.controlToolBar.tintColor =[Locator Default].Color.Principal;
    }
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (self.FechaSeleccionada != nil)
    {
        [self.ControlPicker setDate:self.FechaSeleccionada animated:true];
    }
    else
    {
        [self.ControlPicker setDate:[NSDate new] animated:true];
    }
    
    if (self.FechaMaxima != nil)
    {
        [self.ControlPicker setMaximumDate:self.FechaMaxima];
    }
}

- (void)viewDidUnload
{
    [self setControlPicker:nil];
    [self setControlToolBar:nil];
    [self setControlBotonCancelar:nil];
    [self setControlBotonAceptar:nil];
    [super viewDidUnload];
}

- (BOOL) shouldAutorotate
{
    return NO;
}

#pragma mark- Eventos Objetos

- (IBAction)btnDone_click:(id)sender
{
    [self dismissSemiModalViewController:self];
    
    if (self.Respond != nil)
        self.Respond(self.ControlPicker.date);
}

- (IBAction)btnCancel_click:(id)sender
{
    [self dismissSemiModalViewController:self];
    
    if (self.Respond != nil)
        self.Respond(nil);
}

- (IBAction)DatePicker_ValueChanged:(id)sender
{

}


@end
