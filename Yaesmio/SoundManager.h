//
//  SoundManager.h
//  Yaesmio
//
//  Created by freelance on 29/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SoundManager : NSObject

@property (strong) NSString *UrlNotificacion;

-(void)PlayNotificacion;

@end
