//
//  ImageManager.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 17/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImagenManager : NSObject

@property (readonly) UIImage *FondoBotonSeleccionBlanco;
@property (readonly) UIImage *FondoBotonGris;
@property (readonly) UIImage *ImagenCarritoChico;
@property (readonly) UIImage *ImagenBotonCarrito;
@property (readonly) UIImage *SinImagen;
@property (readonly) UIImage *FondoTextFieldSeleccionado;
@property (readonly) UIImage *FondoTextField;
@property (readonly) UIImage *FondoTextFieldDesactivado;
@property (readonly) UIImage *LedEncendido;
@property (readonly) UIImage *LedApagado;
@property (strong)   NSString *UrlImagenFondoPantallaPrincipal;
@property (readonly) UIImage *ImagenFondoPantallaPrincipal;
@property (readonly) UIImage *Estrella;
@property (readonly) UIImage *EstrellaSeleccionada;
@property (readonly) UIImage *Black;
@property (readonly) UIImage *SinContacto;
@property (readonly) UIImage *GrupoNotificacion;


- (id)	ComprobarImagen:(id)pImage;

- (void)Configurar;

//devuelve la imagen en blanco y negro.
-(UIImage*)ConvertBlackAndWhiteImage:(UIImage*)pImage;

@end
