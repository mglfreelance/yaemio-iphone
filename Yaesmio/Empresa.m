//
//  Empresa.m
//  Yaesmio
//
//  Created by freelance on 08/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "Empresa.h"

@implementation Empresa

- (id)init
{
	if((self = [super init]))
    {
		self.Codigo = @"";
		self.Nombre = @"";
        self.Imagen = [[Media alloc] initWithUrl:@"" UrlSmall:@"" AndMediaType:MediaTypeMIME_image];
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Codigo xmlNodeForDoc:node->doc elementName:@"NombreMarca" elementNSPrefix:@"tns3"]);
	
	xmlAddChild(node, [self.Nombre xmlNodeForDoc:node->doc elementName:@"Descripcion" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Imagen.UrlPath xmlNodeForDoc:node->doc elementName:@"Logo" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Imagen.UrlPathSmall xmlNodeForDoc:node->doc elementName:@"LogoThumbnail" elementNSPrefix:@"tns3"]);
}

+ (Empresa *)deserializeNode:(xmlNodePtr)cur
{
	Empresa *newObject = [Empresa new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"NombreMarca"])
    {
        self.Codigo = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Descripcion"])
    {
        self.Nombre = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"LogoThumbnail"])
    {
        self.Imagen.UrlPathSmall = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Logo"])
    {
        self.Imagen.UrlPath = [NSString deserializeNode:pobjCur];
    }
}

@end
