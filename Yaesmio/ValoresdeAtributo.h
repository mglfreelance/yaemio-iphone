//
//  ValoresdeAtributo.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 31/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoapObject.h"

@interface ValoresdeAtributo : SoapObject

+ (ValoresdeAtributo *)deserializeNode:(xmlNodePtr)cur;

- (id)init:(NSString*)pAtributoID Nombre:(NSString*)pNombre ValorID:(NSString*)pValorID;

/* elements */
@property (nonatomic, strong) NSString * ProductoId;
@property (nonatomic, strong) NSString * DescripcionAtt1;
@property (nonatomic, strong) NSString * DescripcionAtt2;

-(BOOL) comprobarObjeto:(NSArray*)pListaAtributos ValoresdeAtributo:(NSArray*)pListaValores;

@end


@interface NSArray (ValoresdeAtributo)

-(ValoresdeAtributo*)ObtenerValordeAtributoPorID:(NSString*)pID;
-(int) obtenerIndiceValordeAtributo:(ValoresdeAtributo*)pElemento;



@end