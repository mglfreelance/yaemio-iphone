//
//  ContactoGrupo.h
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"

@interface ContactoGrupo : SoapObject

+ (ContactoGrupo *) deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *IdUsuarioSap;

@end


@interface NSArray (ContactoGrupo)

- (BOOL)			ExisteContactoGrupo:(ContactoGrupo *)pGrupo;

@end
