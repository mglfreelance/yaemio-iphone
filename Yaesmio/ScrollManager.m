//
//  ScrollManager.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"

#import "ScrollManager.h"
#import "UIScrollView+Ampliado.h"

// en un futuro esta clase deberia desparacer y convertirse en una clase que heredara de scrollview

@interface ScrollManager()<UITextFieldDelegate,UITextViewDelegate>

@property (nonatomic) UIGestureRecognizer   *tapper;
@property (nonatomic) UIView                *view;
@property (nonatomic) UIScrollView          *scrollView;
@property (nonatomic) NSArray               *listTextFields;
@property (strong)    id                    Delegate;

@end

@implementation ScrollManager

CGPoint svos;

-(ScrollManager*) initWithView:(UIView*)pview andScrollView:(UIScrollView*) pScrollView andTextFields:(NSArray*) pListTextfields
{
    self = [super init];
    
    self.view = pview;
    self.scrollView = pScrollView;
    self.listTextFields = pListTextfields;
    svos = self.scrollView.contentOffset;
    
    self.tapper = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(TapGesture_click:)];
    self.tapper.cancelsTouchesInView = FALSE;
    [self.view addGestureRecognizer:self.tapper];
    [self.scrollView setHeightContentSizeForSubviewUntilControl:nil];
    
    return self;
}

-(ScrollManager*) initWithView:(UIView*)pview andScrollView:(UIScrollView*) pScrollView
{
    return [self initWithView:pview andScrollView:pScrollView andTextFields:[self getTextFieldsInScrollView:pScrollView]];
}

-(ScrollManager*) initWithScrollView:(UIScrollView*) pScrollView
{
    return [self initWithView:pScrollView andScrollView:pScrollView andTextFields:[self getTextFieldsInScrollView:pScrollView]];
}

+(ScrollManager*) NewAndConfigureWithScrollView:(UIScrollView *)pScrollView AndDidEndTextField:(id)pdelegate Selector:(SEL)pSel
{
    ScrollManager *lScroll = [[ScrollManager alloc] initWithScrollView:pScrollView];
    [lScroll ConfigureDidEndTextField:pdelegate Selector:pSel];
    return lScroll;
}

- (void)TapGesture_click:(UITapGestureRecognizer *)sender
{
    [self.view endEditing:true];
}

-(void) ClearScroll
{
    [self.scrollView setContentOffset:svos animated:YES];
}

- (void) scrollToTextField:(UIView *)textField
{
    CGRect rc = [textField convertRect:[textField bounds] toView:self.scrollView];
    CGPoint pt = CGPointMake(0, rc.origin.y+ rc.size.height -145);
    
    [self.scrollView setContentOffset:pt animated:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.tag != 0 && textField.text.length >= textField.tag && range.length == 0)
    {
    	return NO; // return NO to not change text
    }

    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (textView.tag != 0 && textView.text.length >= textView.tag && range.length == 0)
    {
    	return NO; // return NO to not change text
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self setTextEditing:textField IsEditing:true];
    [self scrollToTextField:textField];
    [self.scrollView setContentInset:UIEdgeInsetsMake(0, 0, 150, 0)];
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    [self setTextEditing:textView IsEditing:true];
    [self scrollToTextField:textView];
    [self.scrollView setContentInset:UIEdgeInsetsMake(0, 0, 150, 0)];
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    [self setTextEditing:textView IsEditing:false];
    [self.scrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self setTextEditing:textField IsEditing:false];
    [self.scrollView setContentInset:UIEdgeInsetsMake(0, 0, 0, 0)];
}

-(void) setListTextFields:(NSArray *)plistTextFields
{
    _listTextFields = plistTextFields;
    
    for (UITextField* myText in plistTextFields)
    {
        if ([myText isMemberOfClass:[UIButton class]] == false)
        {
            myText.delegate = self;
        }
        [self setTextEditing:myText IsEditing:false];
    }
}

-(NSArray*) getTextFieldsInScrollView:(UIView*) pscroll
{
    NSMutableArray *lcolResultado =[[NSMutableArray alloc] init];
    
    for (UIView *lview in pscroll.subviews)
    {
        if ([lview isMemberOfClass:[UITextField class]] || [lview isMemberOfClass:[UITextView class]] || [lview isMemberOfClass:[UIButton class]])
        {
            [lcolResultado addObject:lview];
        }
        else if ([lview.subviews count]>0)
        {
            [lcolResultado addObjectsFromArray:[self getTextFieldsInScrollView:lview]];
        }
    }
    
    return lcolResultado;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    UIView *ltextAnterior =nil;
    
    if (textField == self.listTextFields.lastObject)
    {   //estamos en el ultimo texto, no hay que saltar a otro.
        [textField resignFirstResponder];
        [self CallSelector:textField];
    }
    else
    {
        for (UIView *ltext in self.listTextFields)
        {
            if (ltextAnterior !=nil)
            {
                [ltext becomeFirstResponder];
                if ([ltext isMemberOfClass:[UIButton class]])
                {
                    [self.view endEditing:true];;//oculto el teclado
                    [self scrollToTextField:ltext];
                    [((UIButton*)ltext) sendActionsForControlEvents: UIControlEventTouchUpInside];
                }
                break;
            }
            else if ( ltext == textField) // es el mismo
            {
                ltextAnterior = ltext;
            }
        }
    }
    
    return NO;
}

-(void) CallSelector:(UITextField*)ptextfield
{
    if (self.Delegate != nil && mySelector !=nil)
    {
        [self.Delegate performSelector:mySelector withObject:ptextfield];
    }
}

SEL mySelector;
-(void)ConfigureDidEndTextField:(id)pdelegate Selector:(SEL) pSel
{
    self.Delegate = pdelegate;
    mySelector = pSel;
    
}

-(void) setTextEditing:(id)pControl IsEditing:(BOOL)pEditing
{
    UIImage *image = Nil;
    
    if (pEditing)
    {
        image = [Locator Default].Imagen.FondoTextFieldSeleccionado;
    }
    else
    {
        image = [Locator Default].Imagen.FondoTextField;
    }
    
    if ([pControl isMemberOfClass:[UITextField class]])
    {
        UITextField *myText = (UITextField*)pControl;
        myText.borderStyle = UITextBorderStyleNone;
        
        if (myText.enabled == false)
        {
            image = [Locator Default].Imagen.FondoTextFieldDesactivado;
        }
        myText.background = image;
        myText.backgroundColor = [Locator Default].Color.Clear;
        if (myText.leftView == nil)
        {
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
            myText.leftView = paddingView;
            myText.leftViewMode = UITextFieldViewModeAlways;
        }
        if (myText.rightView == nil)
        {
            UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 20)];
            myText.rightView = paddingView;
            myText.rightViewMode = UITextFieldViewModeAlways;
        }
    }
}

@end
