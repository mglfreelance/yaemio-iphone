//
//  MetodoPagoCell.h
//  Yaesmio
//
//  Created by freelance on 09/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TarjetaUsuario.h"

@interface MetodoPagoCell : UITableViewCell

-(void)ConfigurarConTarjetaUsuario:(TarjetaUsuario*)tarjeta;

@end
