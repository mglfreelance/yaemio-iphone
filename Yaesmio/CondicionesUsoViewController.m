//
//  CondicionesUsoViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import "CondicionesUsoViewController.h"
#import "UIAllControls.h"
#import "MembershipDatosUsuario.h"
#import "Direccion.h"

@interface CondicionesUsoViewController () <UIWebViewDelegate>

@property (nonatomic) IBOutlet MKGradientButton *btnAceptarCondiciones;
@property (nonatomic) IBOutlet UIWebView *controlWebView;
@property (nonatomic) IBOutlet UILabel *labelTexto;
@property (nonatomic) IBOutlet UILabel *labelTitulo;
@property (nonatomic) IBOutlet UIView *viewCondiciones;
@property (nonatomic) IBOutlet M13Checkbox *checkPermitirPublicidad;

@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCamposTraducir;

- (IBAction)btnGuardarCuenta_click:(id)sender;

@end

@implementation CondicionesUsoViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaCamposTraducir);
    
    self.viewCondiciones.layer.borderColor = [Locator Default].Color.Gris.CGColor;
    self.viewCondiciones.layer.borderWidth = 2;
    self.viewCondiciones.layer.cornerRadius = 0;
    
    self.labelTexto.attributedText = [[Locator Default].FuenteLetra getTextoJustificado:Traducir(@"Al pulsar en \"Acepto\", usted acepta quedar vinculado por los términos y las condiciones de uso de \"Yaesmio!\", y confirma que ha leido y acepta nuestra Política de Protección de Datos.")];
    
    [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo Información.")];
    [[Locator Default].ServicioSOAP ObtenerTexto:@"ZLEGAL1" Result:^(SRResultMethod pResult, NSString *pTexto)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible obtener la informacion a mostrar.")];
                 break;
                 
             case SRResultMethodYES:
                 [[Locator Default].Alerta hideBussy];
                 self.controlWebView.delegate = self;
                 [self.controlWebView loadHTMLString:pTexto baseURL:Nil];
                 break;
         }
     }];
}

- (void)viewUnload
{
    self.VistaInicial = nil;
    self.Resultado = nil;
    [self setListaCamposTraducir:nil];
}

- (IBAction)btnGuardarCuenta_click:(id)sender
{
    if ([self.NombreUsuario isEqualToString:@""] || [self.PasswordUsuario isEqualToString:@""])
    {
        [[Locator Default].Alerta showMessageAcept:Traducir(@"Lo datos de usuario estan vacios.")];
    }
    else
    {
        [[Locator Default].Alerta showBussy:Traducir(@"Creando Usuario.")];
        MembershipDatosUsuario *lUsuario = [[MembershipDatosUsuario alloc] init];
        lUsuario.Login = self.NombreUsuario;
        lUsuario.Password = self.PasswordUsuario;
        lUsuario.aceptaEnvioPublicidad = (self.checkPermitirPublicidad.checkState == M13CheckboxStateChecked);
        
        [[Locator Default].ServicioSOAP CrearUsuario:lUsuario Result:^(SRResultCreateUser pResult, NSString *MensajeSap)
         {
             switch (pResult)
             {
                 case SRResultCreateUserErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
                     
                 case SRResultCreateUserExists:
                     [[Locator Default].Alerta showMessageAcept:Traducir(@"Ya existe un usuario dado de alta.")];
                     break;
                     
                 case SRResultCreateUserNO:
                     [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible crear el usuario.")];
                     break;
                     
                 case SRResultCreateUserYES:
                     [Locator Default].Preferencias.MailUsuario = self.NombreUsuario;
                     [[Locator Default].Alerta showMessageAceptWithTitle:Traducir(@"¡Enhorabuena!") AndMessage:Traducir(@"Ya eres usuario de yaesmio!\nEntra en tu correo electronico y verifica tu registro.") ResulBlock:^
                      {
                          [self.navigationController popToViewController:self.VistaInicial animated:true];
                          
                          if (self.Resultado != nil)
                          {
                              self.Resultado(0);
                          }
                          
                          self.Resultado = nil;
                          self.VistaInicial = nil;
                      }];
                     break;
             }
         }];
    }
}

#pragma mark - WebView Delegate

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"%@", error);
}

- (BOOL)webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType
{
    if (inType == UIWebViewNavigationTypeLinkClicked)
    {
        [[Locator Default].Vistas MostrarWebView:self url:inRequest.URL.absoluteString];
        return NO;
    }
    
    return YES;
}

@end
