//
//  DeviceManager.h
//  Yaesmio
//
//  Created by freelance on 14/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceManager : NSObject

@property (readonly) NSString *IdDevice;
@property (readonly) NSString *IPAddress;
@property (readonly) NSString *DeviceID16Long;
@property (readonly) NSString *MemoryApp;

@end
