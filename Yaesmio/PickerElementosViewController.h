//
//  PickerValorAtributoViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 05/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIAllControls.h"
#import "ElementoPicker.h"

@interface PickerElementosViewController : TDSemiModalViewController

typedef void (^PickerRespondBlock)(ElementoPicker *pResult);

@property (nonatomic, strong) NSArray            *ListaElementos;
@property (nonatomic, strong) NSString           *NombrePantalla;
@property (copy)              PickerRespondBlock Respond;
@property (nonatomic, strong) ElementoPicker     *ElementoSeleccionado;


@end
