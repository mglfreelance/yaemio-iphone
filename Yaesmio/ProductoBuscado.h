//
//  ProductoBuscado.h
//  Yaesmio
//
//  Created by freelance on 09/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"
#import "Media.h"

typedef NS_ENUM(NSInteger, GrupoDistanciaProductoBuscado)
{
    GrupoDistanciaProductoBuscadoMenos200m  = 0,
    GrupoDistanciaProductoBuscadoMenos500m  = 1,
    GrupoDistanciaProductoBuscadoMenos3km   = 2,
    GrupoDistanciaProductoBuscadoMenos10km  = 3,
    GrupoDistanciaProductoBuscadoMenos50km  = 4,
    GrupoDistanciaProductoBuscadoMenos200km = 5,
    GrupoDistanciaProductoBuscadoMayor200km = 6,
};

@interface ProductoBuscado  : SoapObject

+ (ProductoBuscado *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong)   NSString          * IdProductoPadre;
@property (strong)   NSString          * IdProductoHijo;
@property (strong)   NSString          * Nombre;
@property (strong)   NSString          * NombreEmpresa;
@property (strong)   Media             * Imagen;
@property (strong)   NSString          * Moneda;
@property (strong)   NSDecimalNumber   * Precio;
@property (strong)   NSDecimalNumber   * Descuento;
@property (assign)   BOOL                Recomendada;
@property (strong)   NSDecimalNumber   * Distancia;
@property (strong)   NSDecimalNumber   * Latitud;
@property (strong)   NSDecimalNumber   * Longitud;
@property (readonly) GrupoDistanciaProductoBuscado GrupoDistancia;
@end