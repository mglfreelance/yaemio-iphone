//
//  CALayer+Ampliada.h
//  Yaesmio
//
//  Created by freelance on 03/07/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (Ampliada)

@property (strong) UIColor *BorderColorFromUIColor;

@end
