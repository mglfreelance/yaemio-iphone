//
//  DestinatarioNotificacion.h
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"
#import "CabeceraNotificacion.h"

@interface DestinatarioNotificacion : SoapObject


+ (DestinatarioNotificacion *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString *NombreGrupo;
@property (nonatomic, strong) NSString *MailDestinatario;
@property (nonatomic, assign) SNTipoNotificacion Tipo;


@end
