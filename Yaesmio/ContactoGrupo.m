//
//  ContactoGrupo.m
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ContactoGrupo.h"

@implementation ContactoGrupo

- (id)init
{
    if ((self = [super init]))
    {
        self.email        = @"";
        self.IdUsuarioSap = @"";
    }
    
    return self;
}

+ (ContactoGrupo *)deserializeNode:(xmlNodePtr)cur
{
    ContactoGrupo *newObject = [[ContactoGrupo alloc] init];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.email xmlNodeForDoc:node->doc elementName:@"EmailContacto" elementNSPrefix:@"grupos"]);
    
    xmlAddChild(node, [self.IdUsuarioSap xmlNodeForDoc:node->doc elementName:@"IdUsuarioContacto" elementNSPrefix:@"grupos"]);
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"EmailContacto"])
    {
        self.email = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"IdUsuarioContacto"])
    {
        self.IdUsuarioSap = [NSString deserializeNode:pobjCur];
    }
}

@end

@implementation NSArray  (ContactoGrupo)

- (BOOL)ExisteContactoGrupo:(ContactoGrupo *)pGrupo
{
    for (ContactoGrupo *item in self)
    {
        if ([item.IdUsuarioSap isEqualToString:pGrupo.IdUsuarioSap])
        {
            return true;
        }
    }
    
    return false;
}

@end
