//
//  SitioYaesmioCell.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 12/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SitioYaesmio.h"

@interface SitioYaesmioCell : UITableViewCell

-(float) obtenerPreviewHeightCelda:(SitioYaesmio*)pSitio;

-(void)ConfigurarConSitio:(SitioYaesmio*)pSitio;

@end
