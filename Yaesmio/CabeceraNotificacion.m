//
//  CabeceraNotificacion.m
//  Yaesmio
//
//  Created by freelance on 16/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "CabeceraNotificacion.h"

@implementation CabeceraNotificacion

- (id)init
{
    if ((self = [super init]))
    {
        self.Fecha  = nil;
        self.Cuerpo = @"";
        self.Empresa = @"";
        self.IdProducto = @"";
        self.idNotificacion = @"";
        self.Imagen = [Media createMediaImage];
        self.MailEmisior = @"";
        self.Medio = @"";
        self.Moneda = @"";
        self.NombreProducto = @"";
        self.Precio = [NSDecimalNumber zero];
        self.TextoEnvio = @"";
        self.Tipo = SNTipoNotificacionNinguna;
        self.Descuento = nil;
        self.Recomendada = false;
    }
    
    return self;
}

+ (CabeceraNotificacion *)deserializeNode:(xmlNodePtr)cur
{
    CabeceraNotificacion *newObject = [CabeceraNotificacion new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"Fecha"])
    {
        NSString *lDato = [NSString deserializeNode:pobjCur];
        
        if ([lDato isEqualToString:@"0000-00-00"] == false)
        {
            if (self.Fecha == nil)
            {
                self.Fecha = [NSDate dateWithTimeIntervalSince1970:0];
            }
            
            NSDate *mydate = [NSDate fromString:lDato WithFormat:FECHA_SOAP];
            self.Fecha = [self.Fecha dateWithYear:mydate.Year Month:mydate.Month Day:mydate.Day Hour:UNCHANGEDATE minute:UNCHANGEDATE second:UNCHANGEDATE];
        }
    }
    else if ([nodeName isEqualToString:@"Hora"])
    {
        NSString *lDato = [NSString deserializeNode:pobjCur];
        
        if ([lDato isEqualToString:@"00:00:00"] == false)
        {
            if (self.Fecha == nil)
            {
                self.Fecha = [NSDate dateWithTimeIntervalSince1970:0];
            }
            
            NSDate *myFecha = [NSDate fromString:lDato WithFormat:@"HH:mm:ss"];
            self.Fecha = [self.Fecha dateWithYear:UNCHANGEDATE Month:UNCHANGEDATE Day:UNCHANGEDATE Hour:myFecha.Hour minute:myFecha.Minute second:myFecha.Second];
        }
    }
    else if ([nodeName isEqualToString:@"Empresa"])
    {
        self.Empresa = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Cuerpo"])
    {
        self.Cuerpo = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Hijo"])
    {
        self.IdProducto = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"IdEnvio"])
    {
        self.idNotificacion = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Img"])
    {
        self.Imagen.UrlPath = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"MailEmisor"])
    {
        self.MailEmisior = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Medio"])
    {
        self.Medio = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Moneda"])
    {
        self.Moneda = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NameHijo"])
    {
        self.NombreProducto = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Precio"])
    {
        self.Precio = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ZZDESCUENTO"])
    {
        self.Descuento = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Recomendada"])
    {
        self.Recomendada = [SOAP_Boolean deserializeNode:pobjCur].boolValue;
    }
    else if ([nodeName isEqualToString:@"Strecep"])
    {
        self.tipo = [NSString deserializeNode:pobjCur].intValue;
    }
    else if ([nodeName isEqualToString:@"Thumbnail"])
    {
        self.Imagen.UrlPathSmall = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Txtenvio"])
    {
        self.TextoEnvio = [NSString deserializeNode:pobjCur];
    }
}

@end
