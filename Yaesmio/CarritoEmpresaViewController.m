//
//  CarritoViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 16/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "CarritoEmpresaViewController.h"
#import "ProductoCarritoCell.h"
#import "CarritoManager.h"
#import "UIAllControls.h"


@interface CarritoEmpresaViewController () <UITableViewDataSource, UITableViewDelegate, CarritoManagerDelegate, M13ContextMenuDelegate>

@property (nonatomic) IBOutlet UITableView *controlTablaView;
@property (weak, nonatomic) IBOutlet UIImageView *LogoEmpresa;
@property (weak, nonatomic) IBOutlet UILabel *labelTitulo;

@property (strong) M13ContextMenu *menu;
@property (strong) M13ContextMenuItemIOS7 *MenuModificarProducto;
@property (strong) M13ContextMenuItemIOS7 *MenuBorrarProducto;

@end

@implementation CarritoEmpresaViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.labelTitulo);
    [self MostrarMensajeProductosAgregados];
    
    [self configurarContextMenu];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:true];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIMenuController sharedMenuController] setMenuItems:nil];
    [UIMenuController sharedMenuController].arrowDirection = UIMenuControllerArrowDefault;
}

- (void)viewUnload
{
    [self setControlTablaView:nil];
    [self setLogoEmpresa:nil];
    [self setMenu:nil];
    [self setMenuModificarProducto:nil];
    [self setMenuBorrarProducto:nil];
    [self setCarrito:nil];
    [self setPadre:nil];
    [self setActualizarTotal:nil];
    [Locator Default].Carrito.delegate = nil;
}

-(void)Refrescar
{
    [self MostrarMensajeProductosAgregados];
    [self.controlTablaView reloadData];
}

#pragma mark- Delegado UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.Carrito.Elementos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProductoCarritoCell *lVistaCelda = [tableView dequeueReusableCellWithIdentifier:@"CeldaProducto"];
    
    [lVistaCelda Configurar:self.Padre Elemento:[self obtenerProductoDeIndexPath:indexPath] menu:self.menu];
    lVistaCelda.DelegateCambiadaInformacionCelda = ^(ElementoCarrito *pElemento)
    {
        [self.controlTablaView reloadRowsAtIndexPaths:@[[self getIndexpahtFromElementoCarrito:pElemento]] withRowAnimation:UITableViewRowAnimationNone];
        
        if (self.ActualizarTotal != nil)
        {
            self.ActualizarTotal();
        }
    };
    
    return lVistaCelda;
}

#pragma  mark- Delegate ContextMenu

- (BOOL)shouldShowContextMenu:(M13ContextMenu *)contextMenu atPoint:(CGPoint)point
{
    NSIndexPath *indexPath = [self.controlTablaView indexPathForRowAtPoint:point];
    
    if (indexPath != nil && indexPath.row < self.Carrito.Elementos.count)
    {
        ElementoCarrito *actual = [self obtenerProductoDeIndexPath:indexPath];
        
        if (actual != nil && actual.Producto.ListaValoresAtributos.count < 2)
        {
            self.MenuModificarProducto.HideMenu = true;
        }
        else
        {
            self.MenuModificarProducto.HideMenu = false;
        }
        
        return true;
    }
    
    return false;
}

- (void)contextMenu:(M13ContextMenu *)contextMenu atPoint:(CGPoint)point didSelectItemAtIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [self.controlTablaView indexPathForRowAtPoint:point];
    
    if (indexPath != nil && indexPath.row < self.Carrito.Elementos.count)
    {
        ElementoCarrito *actual = [self obtenerProductoDeIndexPath:indexPath];
        
        if (actual != nil)
        {
            switch (index)
            {
                case 0: // Modificacion
                    [[Locator Default].Vistas MostrarEditarProducto:self.Padre Producto:actual];
                    break;
                    
                case 1: //Eliminacion
                    [[Locator Default].Alerta showMessage:Traducir(@"¡Atención!") Message:Traducir(@"¿Seguro que quieres eliminar este producto?") cancelButtonTitle:Traducir(@"Cancelar") AceptButtonTitle:Traducir(@"Aceptar") completionBlock: ^(NSUInteger buttonIndex)
                     {
                         if (buttonIndex == 1)
                         {   // aceptar
                             [[Locator Default].Carrito QuitarElemento:actual];
                             [self.controlTablaView reloadData];
                         }
                     }];
                    break;
            }
        }
    }
}

#pragma mark- Metodos Privados

- (void)configurarContextMenu
{
    //Create the items
    self.MenuModificarProducto = [[M13ContextMenuItemIOS7 alloc] initWithUnselectedIcon:[UIImage imageNamed:@"MenuModificar"] selectedIcon:[UIImage imageNamed:@"MenuModificarSeleccionado"]];
    self.MenuBorrarProducto = [[M13ContextMenuItemIOS7 alloc] initWithUnselectedIcon:[UIImage imageNamed:@"MenuPapelera"] selectedIcon:[UIImage imageNamed:@"MenuPapeleraSeleccionada"]];
    
    self.MenuModificarProducto.tintColor = [Locator Default].Color.Principal;
    self.MenuBorrarProducto.tintColor = [Locator Default].Color.Principal;
    
    //Create the menu
    self.menu = [[M13ContextMenu alloc] initWithMenuItems:@[self.MenuModificarProducto, self.MenuBorrarProducto]];
    self.menu.delegate = self;
    self.menu.tintColor = [Locator Default].Color.Principal;
    self.menu.BorderColor = [Locator Default].Color.Principal;
    self.menu.originationCircleStrokeColor = [Locator Default].Color.GrisOscuro;
}

- (ElementoCarrito *)obtenerProductoDeIndexPath:(NSIndexPath *)pIndexPath
{
    ElementoCarrito *lResultado = self.Carrito.Elementos[pIndexPath.row];
    
    return lResultado;
}

- (NSIndexPath *)getIndexpahtFromElementoCarrito:(ElementoCarrito *)pElemento
{
    NSInteger row = [self.Carrito.Elementos indexOfObject:pElemento];
    
    NSIndexPath *result = [NSIndexPath indexPathForRow:row inSection:0];
    
    return result;
}

- (void)MostrarMensajeProductosAgregados
{
    [self.LogoEmpresa cargarUrlImagenIzquierdaAsincrono:self.Carrito.LogoEmpresa.UrlPathSmall Grupo:@"carrito"];
}

#pragma mark- Delegate Carrito

- (void)CambiadoElementosCarrito
{
    [self MostrarMensajeProductosAgregados];
    [self.controlTablaView reloadData];
}

@end
