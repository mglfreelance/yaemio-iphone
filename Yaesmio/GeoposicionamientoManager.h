//
//  GeoposicionamientoManager.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 03/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface GeoposicionamientoManager : NSObject

typedef NS_ENUM (NSInteger, GPSManager)
{
    GPSManagerOK  = 1,     // todo OK
    GPSManagerNO  = 2,     // NO GPS
    GPSManagerErr = 0,     // Err GPS
};

typedef void (^getLocationBlock)(GPSManager pResult, double pLatitud, double pLongitud);
- (void)getLocation:(getLocationBlock)pBlock;


typedef void (^getLocationAndPostalCodeBlock)(GPSManager pResult, double pLatitud, double pLongitud, CLPlacemark *pSituacion);
- (void)getLocationAndPostalCode:(getLocationAndPostalCodeBlock)pBlock;

@property (readonly) GPSManager Authorized;

@end
