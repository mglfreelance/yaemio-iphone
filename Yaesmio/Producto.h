//
//  Producto.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 31/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoapObject.h"
#import "ValoresdeAtributo.h"
#import "Media.h"

typedef NS_ENUM (NSInteger, TipoProducto)
{
    TipoProductoCupon          = 0,
    TipoProductoCuponConPrecio = 1,
    TipoProductoUnico          = 2,
    TipoProductoConAtributos   = 3,
    TipoProductoPublicitario   = 4,
    TipoProductoOtro           = 5,
};

@interface Producto : SoapObject

@property (strong) NSString *Moneda;
@property (strong) NSString *DescUnidadesMedida;
@property (strong) NSDecimalNumber *Precio;
@property (strong) NSDecimalNumber *PrecioAnterior;
@property (strong) NSString *ProductoId;
@property (strong) NSString *Referencia;
@property (strong) NSArray *ListaMedia;
@property (strong) NSNumber *Stock;
@property (strong) ValoresdeAtributo *Atributos;
@property (strong) NSString *Latitud;
@property (strong) NSString *Longitud;
@property (strong) NSString *Nombre;
@property (strong) NSString *Descripcion1;
@property (strong) NSString *Descripcion2;
@property (strong) NSString *Descripcion3;
@property (strong) NSString *Titulo1;
@property (strong) NSString *Titulo2;
@property (strong) NSString *Titulo3;
@property (assign) TipoProducto Tipo;
@property (strong) NSDecimalNumber *Descuento;
@property (assign) BOOL Recomendada;


//Ampliado
@property (strong)   Media *ImagenPorDefecto;
@property (strong)   Media *VideoPorDefecto;
@property (readonly) BOOL esPublicitario;
@property (readonly) BOOL esCupon;
@property (readonly) BOOL esUnico;
@property (readonly) BOOL esCuponConPrecio;
@property (readonly) BOOL esConAtributos;
@property (readonly) BOOL esProducto;
@property (readonly) NSArray *ListaMediaImagen;
@property (readonly) NSArray *ListaMediaVideo;
@property (readonly) BOOL TieneImagenes;
@property (readonly) BOOL TieneVideos;

+ (Producto *)	deserializeNode:(xmlNodePtr)cur;

- (BOOL)		comprobarObjeto:(NSArray *)pListaAtributos ValoresdeAtributo:(NSArray *)pListaValores;

@end
