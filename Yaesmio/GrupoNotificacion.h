//
//  GrupoNotificacion.h
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"

@interface GrupoNotificacion : SoapObject

+ (GrupoNotificacion *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString *NombreGrupo;
@property (nonatomic, strong) NSString *IdGrupo;
@property (nonatomic, strong) NSString *IdUsuarioCreador;
@property (nonatomic, strong) NSArray  *ListaContactos;

- (GrupoNotificacion *)BuscarEnLista:(NSArray *)pLista;

@end
