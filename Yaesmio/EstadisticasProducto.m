//
//  EstadisticasProducto.m
//  Yaesmio
//
//  Created by freelance on 07/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "EstadisticasProducto.h"
#import "EvaluacionProducto.h"

@implementation EstadisticasProducto

- (id)init
{
    if ((self = [super init]))
    {
        self.NotaMediaEmpresa           = [NSDecimalNumber zero];
        self.NotaMediaProducto          = [NSDecimalNumber zero];
        self.NumeroValoracionesEmpresa  = [NSDecimalNumber zero];
        self.NumeroValoracionesProducto = [NSDecimalNumber zero];
        self.NumeroVecesComprado        = [NSDecimalNumber zero];
        self.NumeroVecesEnFavoritos     = [NSDecimalNumber zero];
        self.NumeroVecesVisto           = [NSDecimalNumber zero];
        self.Evaluaciones               = nil;
        self.NumeroVecesCompartido      = [NSDecimalNumber zero];
    }
    
    return self;
}

+ (EstadisticasProducto *)deserializeNode:(xmlNodePtr)cur
{
    EstadisticasProducto *newObject = [EstadisticasProducto new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (EstadisticasProducto *)Clonar
{
    EstadisticasProducto *resultado = [EstadisticasProducto new];
    
    resultado.NotaMediaEmpresa           = self.NotaMediaEmpresa;
    resultado.NotaMediaProducto          = self.NotaMediaProducto;
    resultado.NumeroValoracionesEmpresa  = self.NumeroValoracionesEmpresa;
    resultado.NumeroValoracionesProducto = self.NumeroValoracionesProducto;
    resultado.NumeroVecesComprado        = self.NumeroVecesComprado;
    resultado.NumeroVecesEnFavoritos     = self.NumeroVecesEnFavoritos;
    resultado.NumeroVecesVisto           = self.NumeroVecesVisto;
    resultado.NumeroVecesCompartido      = self.NumeroVecesCompartido;
    
    NSMutableArray *lista = [NSMutableArray new];
    
    for (EvaluacionProducto *item in self.Evaluaciones)
    {
        [lista addObject:[item Clonar]];
    }
    
    resultado.Evaluaciones               = [[NSArray alloc] initWithArray:lista];
    
    return resultado;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    // NSLog(@"campo: %@",[NSString stringWithUTF8String: pobjCur->name]);
    
    if ([nodeName isEqualToString:@"NotaMediaPorEmpresa"])
    {
        self.NotaMediaEmpresa = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NotaMediaPorProducto"])
    {
        self.NotaMediaProducto = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NumeroValoracionesPorEmpresa"])
    {
        self.NumeroValoracionesEmpresa = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NumeroValoracionesPorProducto"])
    {
        self.NumeroValoracionesProducto = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NumeroVecesComprado"])
    {
        self.NumeroVecesComprado = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NumeroVecesEnFavoritos"])
    {
        self.NumeroVecesEnFavoritos = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NumeroVecesVisto"])
    {
        self.NumeroVecesVisto = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"NumeroVecesCompartido"])
    {
        self.NumeroVecesCompartido = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ListaValoraciones"])
    {
        self.Evaluaciones = [NSArray deserializeNode:pobjCur toClass:[EvaluacionProducto class]];
    }
}

@end
