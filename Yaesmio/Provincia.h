//
//  Provincia.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ElementoPicker.h"

@interface Provincia : ElementoPicker


@property (readonly) BOOL esTodas;
@property (readonly) BOOL esCercaDeMi;

+(Provincia *) deserializeNode:(xmlNodePtr)cur;
+(Provincia *) newWithId:(NSString*)pID andName:(NSString*)pName;
+(Provincia *) Todas;
+(Provincia *) CercaDeMi;

@end

