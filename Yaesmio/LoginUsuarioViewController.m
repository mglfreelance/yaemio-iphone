//
//  LoginUsuarioViewController.m
//  yaesmio
//
//  Created by manuel garcia lopez on 07/05/13.
//  Copyright (c) 2013 manuel garcia lopez. All rights reserved.
//

#import "LoginUsuarioViewController.h"
#import "UIAllControls.h"
#import "RecuperarClaveViewController.h"
#import "CrearCuentaViewController.h"


@interface LoginUsuarioViewController ()

- (IBAction)btnEntrar_click:(id)sender;

@property (nonatomic) IBOutlet UITextField *txtPassword;
@property (nonatomic) IBOutlet UITextField *txtMail;
@property (nonatomic) IBOutlet MKGradientButton *btnOlvidadoClave;
@property (nonatomic) IBOutlet MKGradientButton *btnEntrar;
@property (nonatomic) IBOutlet MKGradientButton *btnCrearCuenta;
@property (nonatomic) IBOutlet UIScrollView *scrollViewLogin;
@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCamposTraducir;

- (IBAction)btnCancelar_click:(id)sender;

@property (strong) ScrollManager *manager;

@end

@implementation LoginUsuarioViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaCamposTraducir);
    
    [self.scrollViewLogin setHeightContentSizeForSubviewUntilControl:self.btnCrearCuenta];
    
    self.manager = [ScrollManager NewAndConfigureWithScrollView:self.scrollViewLogin AndDidEndTextField:self Selector:@selector(btnEntrar_click:)];
    
    self.txtMail.text = [Locator Default].Preferencias.MailUsuario;
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.manager ConfigureDidEndTextField:self Selector:@selector(btnEntrar_click:)];
}

- (void)viewUnload
{
    [self setTxtPassword:nil];
    [self setTxtMail:nil];
    [self setBtnOlvidadoClave:nil];
    [self setBtnEntrar:nil];
    [self setBtnCrearCuenta:nil];
    [self setScrollViewLogin:nil];
    [self setManager:nil];
    [self setListaCamposTraducir:nil];
    [self setManager:nil];
}

#pragma mark- Eventos Internos

- (IBAction)btnEntrar_click:(id)sender
{
    [[Locator Default].Alerta showBussy:Traducir(@"Login Usuario")];
    [Locator Default].Preferencias.EstaUsuarioLogado = false;
    [Locator Default].Preferencias.MailUsuario = self.txtMail.text;
    
    [[Locator Default].ServicioSOAP IdentificacionUsuarioWithEmail:self.txtMail.text AndPassword:self.txtPassword.text Result:^(SRResultIdentificacion pResult, LoginUsuarioResponse *Response)
     {
         switch (pResult)
         {
             case SRResultIdentificacionErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultIdentificacionInactivo:
                 [[Locator Default].Alerta showMessageAcept:Traducir(@"Usuario Inactivo, por favor, revise su correo.")];
                 break;
                 
             case SRResultIdentificacionNO:
                 [[Locator Default].Alerta showMessageAcept:Traducir(@"Usuario y/o contraseña Incorrecto.")];
                 break;
                 
             case SRResultIdentificacionBloqueado:
                 [[Locator Default].Alerta showMessageAcept:Traducir(@"Has superado el numero de intentos para acceder a Yaesmio. Por favor, envia un mail a info@yaesmio.com para activar tu usuario.")];
                 break;
                 
             case SRResultIdentificacionYES:
                 [[Locator Default].Alerta hideBussy];
                 [Locator Default].Preferencias.GeolocalizacionActiva = TRUE;
                 [LoginUsuarioViewController ActivarUsuario:Response Email:self.txtMail.text password:self.txtPassword.text];
                 [self CerrarView:1];
                 break;
         }
     }];
}

- (IBAction)btnCancelar_click:(id)sender
{
    [self CerrarView:0];
}

+(void)ActivarUsuario:(LoginUsuarioResponse*)Response Email:(NSString*)Email password:(NSString*)Password
{
    [Locator Default].Preferencias.AutoLoginUsuario = TRUE;
    [[Locator Default].Preferencias GuardarUsuario:Response.UserID NombreUsuario:Response.UserName Mail:Email ConPassword:Password];
    [[Locator Default].ProductosFavoritos actualizarNumeroFavoritos:Response.FavoritesNumber];
    [Locator Default].Imagen.UrlImagenFondoPantallaPrincipal = Response.BackgroundImage;
    [Locator Default].Preferencias.NumeroComprasNoValoradas = Response.HistoricNotRated.intValue;
    [Locator Default].Sounds.UrlNotificacion = Response.NotificationsSound;
    [Locator Default].Application.ActivatePushNotificacions = true;
    [Locator Default].Preferencias.NumeroNotificacionesSinLeer = Response.NotificationsUnread.integerValue;
}

#pragma mark- Metodos Privados

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_RecuperarClave"])
    {
        RecuperarClaveViewController *lView = segue.destinationViewController;
        lView.VistaInicial = self;
        lView.Resultado = self.Resultado;
    }
    else if ([segue.identifier isEqualToString:@"segue_CrearCuenta"])
    {
        CrearCuentaViewController *lView = segue.destinationViewController;
        lView.VistaInicial = self;
        lView.Resultado = nil;
    }
}

- (void)CerrarView:(int)pValor
{
    [self dismissViewControllerAnimated:TRUE completion:NULL];
    
    if (self.Resultado != nil)
    {
        self.Resultado(pValor);
    }
}

@end
