//
//  ServicioSOAPManager.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 21/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ServicioSOAPManager.h"
#import "ServicioSOAP+Ampliado.h"
#import "MetodosServicioSOAP.h"
#import "Favorito.h"
#import "Historico.h"

@interface ServicioSOAPManager ()

@property (readonly) ServicioSOAP *ServicioRegistro;
@property (readonly) ServicioSOAP *ServicioProductos;
@property (readonly) ServicioSOAP *ServicioMedia;
@property (readonly) ServicioSOAP *ServicioNotificaciones;

@end

@implementation ServicioSOAPManager

#pragma mark - Web Services

@synthesize ServicioRegistro = mServicioRegistro;

- (ServicioSOAP *)ServicioRegistro
{
    if (mServicioRegistro == NULL)
    {
        mServicioRegistro = [[ServicioSOAP alloc]initWithAddress:PRIV_URL_WSDL_REGISTRO];
        [self ConfigurarServicioSOAP:mServicioRegistro Nombre:@"usuario"];
    }
    
    return mServicioRegistro;
}

@synthesize ServicioProductos = mServicioProductos;

- (ServicioSOAP *)ServicioProductos
{
    if (mServicioProductos == NULL)
    {
        mServicioProductos = [[ServicioSOAP alloc]initWithAddress:PRIV_URL_WSDL_PRODUCTOS];
        
        [self ConfigurarServicioSOAP:mServicioProductos Nombre:@"productos"];
    }
    
    return mServicioProductos;
}

@synthesize ServicioMedia = mServicioMedia;

- (ServicioSOAP *)ServicioMedia
{
    if (mServicioMedia == NULL)
    {
        mServicioMedia = [[ServicioSOAP alloc]initWithAddress:PRIV_URL_WSDL_MEDIA];
        [self ConfigurarServicioSOAP:mServicioMedia Nombre:@"media"];
    }
    
    return mServicioMedia;
}

@synthesize ServicioNotificaciones = mServicioNotificaciones;

- (ServicioSOAP *)ServicioNotificaciones
{
    if (mServicioNotificaciones == NULL)
    {
        mServicioNotificaciones = [[ServicioSOAP alloc]initWithAddress:PRIV_URL_WSDL_NOTIFICACIONES];
        [self ConfigurarServicioSOAP:mServicioNotificaciones Nombre:@"notificaciones"];
    }
    
    return mServicioNotificaciones;
}

- (void)ConfigurarServicioSOAP:(ServicioSOAP *)pServicio Nombre:(NSString *)pNombre
{
#if DEBUG
#ifdef SHOW_DEBUG_SOAP
    pServicio.logXMLInOut = YES;
    NSLog(@"url wsdl %@:%@", pNombre, pServicio.address.description);
#endif
#endif
    pServicio.HeaderIdioma = [Locator Default].Idioma.IdiomaAppSegunSistema;
    pServicio.HeaderLongitud = self.HeaderLongitud;
    pServicio.HeaderLatitud = self.HeaderLatitud;
    pServicio.HeaderPlataforma = @"IOS";
    pServicio.HeaderIdApp = [Locator Default].Preferencias.IdApp;
    pServicio.HeaderMoneda = @"EUR";         // por ahora por defecto
    pServicio.HeaderVersionApp = [Locator Default].VersionApp;
}

#pragma mark - propiedades header

@synthesize HeaderLatitud = _HeaderLatitud;
@synthesize HeaderLongitud = _HeaderLongitud;

- (void)setHeaderLatitud:(NSString *)HeaderLatitud
{
    _HeaderLatitud = HeaderLatitud;
    
    ServicioSOAP *actual = [self getServicioSoapCreado];
    
    if (actual != nil)                                   //son propiedades staticas, las comparten todos.
    {
        actual.HeaderLatitud = HeaderLatitud;
    }
}

- (void)setHeaderLongitud:(NSString *)HeaderLongitud
{
    _HeaderLongitud = HeaderLongitud;
    
    ServicioSOAP *actual = [self getServicioSoapCreado];
    
    if (actual != nil)                                   //son propiedades staticas, las comparten todos.
    {
        actual.HeaderLongitud = HeaderLongitud;
    }
}

- (void)saveHeaderUser:(NSString *)UserID AndToken:(NSString *)Token
{
    ServicioSOAP *actual = [self getServicioSoapCreado];
    
    if (actual != nil)                                   //son propiedades staticas, las comparten todos.
    {
        actual.HeaderSecurityToken = Token;
        actual.HeaderIdUsuario = UserID;
    }
}

- (ServicioSOAP *)getServicioSoapCreado
{
    if (self.ServicioRegistro != Nil)
    {
        return self.ServicioRegistro;
    }
    else if (self.ServicioNotificaciones != nil)
    {
        return self.ServicioNotificaciones;
    }
    else if (self.ServicioMedia != nil)
    {
        return self.ServicioMedia;
    }
    else
    {
        return self.ServicioProductos;
    }
}

#pragma mark - Identificacion Usuario

- (void)IdentificacionUsuarioWithEmail:(NSString *)email AndPassword:(NSString *)password Result:(IdentificacionUsuarioBlock)pBlock
{
    SRM_Identificacion *Identificacion = [SRM_Identificacion new];
    
    Identificacion.email = email.lowercaseString;
    Identificacion.password = password;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:Identificacion Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultIdentificacion lbolResult = SRResultIdentificacionErrConex;
         LoginUsuarioResponse *LoginResponse = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_IdentificacionResponse *lresponse = [pResponse getResultObjectForClass:[SRM_IdentificacionResponse class]];
             switch (lresponse.CodeResult)
             {
                 case 2:
                     lbolResult = SRResultIdentificacionNO;
                     break;
                     
                 case 3:
                     lbolResult = SRResultIdentificacionInactivo;
                     break;
                     
                 case 5:
                     lbolResult = SRResultIdentificacionBloqueado;
                     break;
                     
                 default:
                     lbolResult = SRResultIdentificacionYES;
                     LoginResponse = lresponse.Response;
                     [self saveHeaderUser:LoginResponse.UserID AndToken:LoginResponse.SecurityToken];
                     break;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lbolResult, LoginResponse);
         }
     }];
}

#pragma mark - Recuperar Password

- (void)RecuperarPasswordConEmail:(NSString *)email Result:(RecuperarPasswordConEmailBlock)pBlock
{
    SRM_RecuperarContrasena *pMetodo = [SRM_RecuperarContrasena new];
    
    pMetodo.email = email.lowercaseString;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lbolResult = SRResultMethodErrConex;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_RecuperarContrasenaResponse *lresponse = [pResponse getResultObjectForClass:[SRM_RecuperarContrasenaResponse class]];
             
             lbolResult = [lresponse.RecuperarContrasenaResult intValue];
         }
         
         if (pBlock != Nil)
         {
             pBlock(lbolResult);
         }
     }];
}

#pragma mark - Comprobar Email

- (void)ComprovarExisteEmail:(NSString *)email Result:(ComprovarExisteEmailBlock)pBlock
{
    SRM_ComprobarExisteEmail *pMetodo = [SRM_ComprobarExisteEmail new];
    
    pMetodo.email = email.lowercaseString;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lbolResult = SRResultMethodErrConex;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_ComprobarExisteEmailResponse *lresponse = [pResponse getResultObjectForClass:[SRM_ComprobarExisteEmailResponse class]];
             
             lbolResult = lresponse.ComprobarExisteEmailResult ? SRResultMethodYES : SRResultMethodNO;
         }
         
         if (pBlock != Nil)
         {
             pBlock(lbolResult);
         }
     }];
}

#pragma mark - Comprobar Euros Yaesmio

- (void)ConsultarEurosYaesmioPorEmail:(NSString *)email Result:(ConsultarEurosYaesmioBlock)pBlock
{
    SRM_ConsultarEurosYaesmio *pMetodo = [SRM_ConsultarEurosYaesmio new];
    
    pMetodo.emailUsu = email.lowercaseString;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         NSNumber *lNumberResult = nil;
         SRResultMethod lbolResult = SRResultMethodErrConex;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_ConsultarEurosYaesmioResponse *lresponse = [pResponse getResultObjectForClass:[SRM_ConsultarEurosYaesmioResponse class]];
             
             lNumberResult = lresponse.ConsultarEurosYaesmioResult;
             lbolResult = SRResultMethodYES;
         }
         
         if (pBlock != Nil)
         {
             pBlock(lbolResult, lNumberResult);
         }
     }];
}

#pragma mark - Devolver Lista Pais

- (void)DevolverListaPais:(SRTipoListaPaisAObtener)Tipo Resultado:(DevolverListaPaisBlock)pBlock
{
    SRP_ObtenerListadoPaisesPorUso *pMetodo = [SRP_ObtenerListadoPaisesPorUso new];
    
    pMetodo.CodigoTipo = [NSString stringWithFormat:@"%d", (int)Tipo];
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         NSArray *lResult     = nil;
         SRResultMethod lbolResult = SRResultMethodErrConex;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_ObtenerListadoPaisesPorUsoResponse *lresponse = [pResponse getResultObjectForClass:[SRP_ObtenerListadoPaisesPorUsoResponse class]];
             
             lResult = [NSArray arrayWithArray:lresponse.ListaPaisesResult];
             lbolResult = SRResultMethodYES;
         }
         
         if (pBlock != Nil)
         {
             pBlock(lbolResult, lResult);
         }
     }];
}

#pragma mark - Devolver Lista Provincia

- (void)DevolverListaProvinciaPorPais:(NSString *)pIDPais Result:(DevolverListaProvinciaBlock)pBlock
{
    SRM_DevolverListaProvincias *pMetodo = [SRM_DevolverListaProvincias new];
    
    pMetodo.pais = pIDPais;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         NSArray *lResult     = nil;
         SRResultMethod lbolResult = SRResultMethodErrConex;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_DevolverListaProvinciasResponse *lresponse = [pResponse getResultObjectForClass:[SRM_DevolverListaProvinciasResponse class]];
             
             lResult = [NSArray arrayWithArray:lresponse.DevolverListaProvinciasResult];
             lbolResult = SRResultMethodYES;
         }
         
         if (pBlock != Nil)
         {
             pBlock(lbolResult, lResult);
         }
     }];
}

#pragma mark - Crear Usuario con Direccion

- (void)CrearUsuario:(MembershipDatosUsuario *)pUsuario Result:(CrearUsuarioBlock)pBlock
{
    SRM_CrearUsuarioMovil *pMetodo = [SRM_CrearUsuarioMovil new];
    
    pMetodo.usuario = pUsuario;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultCreateUser lResult = SRResultCreateUserErrConex;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_CrearUsuarioMovilResponse *lresponse = [pResponse getResultObjectForClass:[SRM_CrearUsuarioMovilResponse class]];
             
             lResult = [lresponse.CrearUsurioWebResult integerValue];
             MensajeSap = lresponse.MensajeSap;
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, MensajeSap);
         }
     }];
}

#pragma mark - Editar Password Usuario

- (void)EditarPassword:(NSString *)pMailUsuario ConPasswordAntigua:(NSString *)pPasswordAntiguo YPasswordNuevo:(NSString *)pPasswordNuevo Result:(EditarPasswordBlock)pBlock
{
    SRM_EditarPassword *pMetodo = [SRM_EditarPassword new];
    
    pMetodo.mail = pMailUsuario.lowercaseString;
    pMetodo.oldpass = pPasswordAntiguo;
    pMetodo.nuevapass = pPasswordNuevo;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultEditPassword lResult = SRResultEditPasswordErrConex;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_EditarPasswordResponse *lresponse = [pResponse getResultObjectForClass:[SRM_EditarPasswordResponse class]];
             
             lResult = [lresponse.EditarPasswordResult integerValue];
             
             if (lResult == SRResultEditPasswordYES)
             {
                 [self saveHeaderUser:self.ServicioRegistro.HeaderIdUsuario AndToken:lresponse.SecurityToken];
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult);
         }
     }];
}

#pragma mark - Modificar Usuario

- (void)ModificarUsuario:(Usuario *)pUsuario Result:(ModificarUsuarioBlock)pBlock
{
    SRM_ModificarUsuario *pMetodo = [SRM_ModificarUsuario new];
    
    pMetodo.usu = pUsuario;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lbolResult = SRResultMethodErrConex;
         NSString *Mensaje = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_ModificarUsuarioResponse *lresponse = [pResponse getResultObjectForClass:[SRM_ModificarUsuarioResponse class]];
             
             switch ([lresponse.ModificarUsuarioResult intValue])
             {
                 case SoapGenericMethodResponseWarning:
                 case SoapGenericMethodResponseOK:
                     lbolResult = SRResultMethodYES;
                     break;
                     
                 default:
                     lbolResult = SRResultMethodNO;
                     Mensaje = lresponse.MensajeSap;
                     break;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lbolResult, Mensaje);
         }
     }];
}

#pragma mark - Obtener Usuario

- (void)ObtenerUsuario:(NSString *)pIdUsuario Result:(ObtenerUsuarioBlock)pBlock
{
    SRM_ObtenerDatosUsuario *pMetodo = [SRM_ObtenerDatosUsuario new];
    
    pMetodo.IdUsuario = pIdUsuario;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         Usuario *lResult       = nil;
         SRResultMethod lErrService    = SRResultMethodErrConex;
         NSString *lMensajeError = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_ObtenerDatosUsuarioResponse *lresponse = [pResponse getResultObjectForClass:[SRM_ObtenerDatosUsuarioResponse class]];
             
             lResult = lresponse.ObtenerDatosUsuarioResult;
             lResult.Mail = [Locator Default].Preferencias.MailUsuario;                                                                                                                                                           //chapuza por SAP
             lMensajeError = lResult.mensaje;
             
             if (lResult != nil && [lMensajeError isEqualToString:@""])
             {
                 lErrService = SRResultMethodYES;
             }
             else
             {
                 lErrService = SRResultMethodNO;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lErrService, lMensajeError, lResult);
         }
     }];
}

#pragma mark - Obtener Direccion Principal Usuario

- (void)ObtenerDirecionPrincipalUsuario:(ObtenerDirecionPrincipalUsuarioBlock)pBlock
{
    SRM_ObtenerDatosDireccionPrincipalUsuario *pMetodo = [SRM_ObtenerDatosDireccionPrincipalUsuario new];
    
    pMetodo.IdUsuario = [Locator Default].Preferencias.IdUsuario;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         Direccion *lResult = nil;
         SRResultMethod lErrService = SRResultMethodErrConex;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_ObtenerDatosDireccionPrincipalUsuarioResponse *lresponse = [pResponse getResultObjectForClass:[SRM_ObtenerDatosDireccionPrincipalUsuarioResponse class]];
             
             lResult = lresponse.Direccion;
             
             if (lResult != nil)
             {
                 lErrService = SRResultMethodYES;
             }
             else
             {
                 lErrService = SRResultMethodNO;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lErrService, lResult);
         }
     }];
}

#pragma mark - Obtener Direccion Alternativa Usuario

- (void)ObtenerDirecionAlternativaUsuario:(ObtenerDirecionAlternativaUsuarioBlock)pBlock
{
    SRM_ObtenerDatosDireccionAlternativaUsuario *pMetodo = [SRM_ObtenerDatosDireccionAlternativaUsuario new];
    
    pMetodo.IdUsuario = [Locator Default].Preferencias.IdUsuario;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         Direccion *lResult = nil;
         SRResultMethod lErrService = SRResultMethodErrConex;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_ObtenerDatosDireccionAlternativaUsuarioResponse *lresponse = [pResponse getResultObjectForClass:[SRM_ObtenerDatosDireccionAlternativaUsuarioResponse class]];
             
             lResult = lresponse.Direccion;
             
             if (lResult != nil)
             {
                 lErrService = SRResultMethodYES;
             }
             else
             {
                 lErrService = SRResultMethodNO;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lErrService, lResult);
         }
     }];
}

#pragma mark - Modificar Direccion Principal

- (void)ModificarDireccionPrincipal:(Direccion *)pDireccion Result:(ModificarDireccionPrincipalBlock)pBlock
{
    SRM_ModificarDireccionPrincipal *pMetodo = [SRM_ModificarDireccionPrincipal new];
    
    pMetodo.primeraDir = pDireccion;
    pMetodo.IdUsuario = [Locator Default].Preferencias.IdUsuario;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lResult = SRResultMethodErrConex;
         NSString *pMensaje = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_ModificarDireccionPrincipalResponse *lresponse = [pResponse getResultObjectForClass:[SRM_ModificarDireccionPrincipalResponse class]];
             
             if ([lresponse.ModificarDireccionPrincipalResult isEqualToString:@"1"])
             {
                 lResult = SRResultMethodYES;
             }
             else if ([lresponse.ModificarDireccionPrincipalResult isEqualToString:@"2"])
             {
                 pMensaje = Traducir(@"No ha sido posible guardar la dirección de envio.");
             }
             else
             {
                 pMensaje = lresponse.ModificarDireccionPrincipalResult;
             }
             
             lResult = [pMensaje isEqualToString:@""] ? SRResultMethodYES : SRResultMethodNO;
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, pMensaje);
         }
     }];
}

#pragma mark - Modificar Direccion Alternativa

- (void)ModificarDireccionAlternativa:(Direccion *)pDireccion Result:(ModificarDireccionAlternativaBlock)pBlock
{
    SRM_ModificarDireccionAlternativa *pMetodo = [SRM_ModificarDireccionAlternativa new];
    
    pMetodo.segundaDir = pDireccion;
    pMetodo.IdUsuario = [Locator Default].Preferencias.IdUsuario;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lResult = SRResultMethodErrConex;
         NSString *pMensaje = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_ModificarDireccionAlternativaResponse *lresponse = [pResponse getResultObjectForClass:[SRM_ModificarDireccionAlternativaResponse class]];
             
             if ([lresponse.ModificarDireccionAlternativaResult isEqualToString:@"1"])
             {
                 lResult = SRResultMethodYES;
             }
             else if ([lresponse.ModificarDireccionAlternativaResult isEqualToString:@"2"])
             {
                 pMensaje = Traducir(@"No ha sido posible guardar la dirección de envio.");
             }
             else
             {
                 pMensaje = lresponse.ModificarDireccionAlternativaResult;
             }
             
             lResult = [pMensaje isEqualToString:@""] ? SRResultMethodYES : SRResultMethodNO;
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, pMensaje);
         }
     }];
}

#pragma mark- Metodo Obtener Producto

- (void)ObtenerProducto:(NSString *)pUrlProducto Result:(ObtenerProductoBlock)pBlock
{
    SRP_ObtenerDatosProductoPorQR *pMetodo = [SRP_ObtenerDatosProductoPorQR new];
    
    pMetodo.UrlProducto = pUrlProducto;
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultObtenerProducto lResult = SRResultObtenerProductoErrConex;
         ProductoPadre *lProducto = nil;
         NSString *lMensajeError = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_ObtenerDatosProductoPorQRResponse *lresponse = [pResponse getResultObjectForClass:[SRP_ObtenerDatosProductoPorQRResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseOK:
                     lResult = SRResultObtenerProductoYES;
                     lProducto = lresponse.ProductoPadreResponse;
                     lProducto.ProductoHijo.Latitud = self.HeaderLatitud;
                     lProducto.ProductoHijo.Longitud = self.HeaderLongitud;
                     break;
                     
                 case SoapGenericMethodResponseWarning:
                 case SoapGenericMethodResponseErrorSer:
                     lResult = SRResultObtenerProductoNoYaesmio;
                     lMensajeError = lresponse.RespuestaMensajeSap;
                     break;
                     
                 default:
                     lResult = SRResultObtenerProductoNoExiste;
                     lMensajeError = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, lMensajeError, lProducto);
         }
     }];
}

#pragma mark- Metodo Obtener Producto por ID

- (void)ObtenerProductoHijo:(NSString *)pidProducto Result:(ObtenerProductoPorIDBlock)pBlock
{
    SRP_ObtenerProductoHijo *pMetodo = [SRP_ObtenerProductoHijo new];
    
    pMetodo.IdProducto = pidProducto;
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultObtenerProducto lResult = SRResultObtenerProductoErrConex;
         Producto *lProducto = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_ObtenerProductoHijoResponse *lresponse = [pResponse getResultObjectForClass:[SRP_ObtenerProductoHijoResponse class]];
             lProducto = lresponse.ProductoResponse;
             
             if (lProducto == nil)
             {
                 lResult = SRResultObtenerProductoNoExiste;
             }
             else if ([lProducto.ProductoId isEqualToString:@""] == false)
             {
                 lResult = SRResultObtenerProductoYES;
             }
             else
             {
                 lResult = SRResultObtenerProductoNoExiste;
             }
             
             if (lResult != SRResultObtenerProductoYES)
             {
                 lProducto = nil;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, lProducto);
         }
     }];
}

#pragma mark- Metodo Comprar lista Productos (solo apartar)

- (void)IniciarCompraConListaProductos:(NSArray *)plistaProductos Usuario:(Usuario *)pUsuario DireccionEnvio:(Direccion *)pDireccion Result:(ComprarListaProductosBlock)pBlock
{
    SRP_GuardarCompras *pMetodo = [SRP_GuardarCompras new];
    
    pMetodo.Direccion = pDireccion;
    pMetodo.IdUsuario = pUsuario.UsuarioId;
    pMetodo.ListaProductos = plistaProductos;
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *lID_Venta = nil;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_GuardarComprasResponse *lresponse = [pResponse getResultObjectForClass:[SRP_GuardarComprasResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     lID_Venta = lresponse.IDCarrito;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(resultado, MensajeSap, lID_Venta);
         }
     }];
}

#pragma mark- Metodo Finalizar Compra (confirmar compra)

- (void)FinalizarCompraConID:(NSString *)pIdCompra IDPaypal:(NSString *)pIdPaypal Result:(FinalizarCompraConIDBlock)pBlock
{
    SRP_ConfirmarCompra *pMetodo = [SRP_ConfirmarCompra new];
    
    pMetodo.Parametros = [[ParametrosConfirmarCompra alloc] init:pIdCompra Paypal:pIdPaypal];
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lResult = SRResultMethodErrConex;
         NSString *lMensaje = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_ConfirmarCompraResponse *lresponse = [pResponse getResultObjectForClass:[SRP_ConfirmarCompraResponse class]];
             lMensaje = lresponse.ActualizarComprasResult;
             
             if (lMensaje == nil || [lMensaje isEqualToString:@""] == false)
             {
                 lResult = SRResultMethodNO;
             }
             else
             {
                 lResult = SRResultMethodYES;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, lMensaje);
         }
     }];
}

#pragma mark- Metodo Obtener Lista Favoritos

- (void)ObtenerListaFavoritosConIdUser:(NSString *)pIdUser PosInicio:(int)pIntInicio Cuantos:(int)pintCantidad Result:(ObtenerListaFavoritosBlock)pBlock
{
    SRP_ObtenerFavoritos *pMetodo = [SRP_ObtenerFavoritos new];
    
    pMetodo.IdUsuario = pIdUser;
    pMetodo.NumeroRegistros = pintCantidad;
    pMetodo.PosicionInicio = pIntInicio;
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lResult = SRResultMethodErrConex;
         NSArray *lResultado = nil;
         int lintNum = 0;
         NSString *lMensaje = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_ObtenerFavoritosResponse *lresponse = [pResponse getResultObjectForClass:[SRP_ObtenerFavoritosResponse class]];
             lResultado = lresponse.ListaFavoritos;
             
             Favorito *lFav = [lResultado lastObject];
             
             if (lFav != nil)
             {
                 lintNum = [lFav.NumElementos intValue];
                 
                 if (lResultado.count == 1 && [lFav.ProductoIdPadre isEqualToString:@"FXXX"])
                 {
                     lMensaje = lFav.Nombre;
                     lResultado = nil;
                     lintNum = 0;
                     
                     if ([lMensaje isEqualToString:@""])
                     {
                         lResultado = [NSArray new];
                         // cuando viene @"" es que no hay favoritos no es un error.
                     }
                 }
             }
             
             if (lResultado == nil)
             {
                 lResult = SRResultMethodNO;
             }
             else
             {
                 lResult = SRResultMethodYES;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, lResultado, lintNum, lMensaje);
         }
     }];
}

#pragma mark- Metodo Agregar Favorito

- (void)AgregarFavoritoConIdUser:(NSString *)pIdUser IdProducto:(ProductoPadre *)pProducto Result:(AgregarFavoritoBlock)pBlock
{
    SRP_AgregarFavorito *pMetodo = [SRP_AgregarFavorito new];
    
    pMetodo.IdUsuario = pIdUser;
    pMetodo.IdProducto = pProducto.ProductoHijo.ProductoId;
    pMetodo.Medio = pProducto.Medio;
    pMetodo.Latitud = pProducto.ProductoHijo.Latitud.floatValue;
    pMetodo.Longitud = pProducto.ProductoHijo.Longitud.floatValue;
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_AgregarFavoritoResponse *lresponse = [pResponse getResultObjectForClass:[SRP_AgregarFavoritoResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(resultado, MensajeSap);
         }
     }];
}

#pragma mark- Metodo Eliminar Favorito

- (void)EliminarFavoritoConIdUser:(NSString *)pIdUser Favorito:(Favorito *)pFavorito Result:(EliminarFavoritoBlock)pBlock
{
    SRP_EliminarFavorito *pMetodo = [SRP_EliminarFavorito new];
    
    pMetodo.IdUsuario = pIdUser;
    pMetodo.IdFavorito = pFavorito.ProductoId;
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lResult = SRResultMethodErrConex;
         NSString *lMensaje = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_EliminarFavoritoResponse *lresponse = [pResponse getResultObjectForClass:[SRP_EliminarFavoritoResponse class]];
             
             lMensaje = lresponse.MensajeResultado;
             
             if (lresponse.esCorrecto)
             {
                 lResult = SRResultMethodYES;
             }
             else
             {
                 lResult = SRResultMethodNO;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, lMensaje);
         }
     }];
}

#pragma mark- Metodo Obtener Historico Compras

- (void)ObtenerListaHistoricoComprasConIdUser:(NSString *)pIdUser PosInicio:(int)pIntInicio Cuantos:(int)pintCantidad TipoProductosVer:(SRTipoHistoricoAObtener)TipoProductosVer Result:(ObtenerListaHistoricoComprasBlock)pBlock
{
    SRP_HistorialComprasUsuario *pMetodo = [SRP_HistorialComprasUsuario new];
    
    pMetodo.NumeroRegistros = pintCantidad;
    pMetodo.PosicionInicio = pIntInicio;
    switch (TipoProductosVer)
    {
        case SRTipoHistoricoAObtenerProductos:
            pMetodo.TipoProducto = @"P";
            break;
            
        case SRTipoHistoricoAObtenerCupones:
            pMetodo.TipoProducto = @"C";
            break;
            
        case SRTipoHistoricoAObtenerTodos:
        default:
            pMetodo.TipoProducto = @"";
            break;
    }
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lResult = SRResultMethodErrConex;
         NSArray *lResultado = nil;
         NSString *lMensaje = @"";
         int NumeroTotal = 0;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_HistorialComprasUsuarioResponse *lresponse = [pResponse getResultObjectForClass:[SRP_HistorialComprasUsuarioResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:     // advertencia
                 case SoapGenericMethodResponseOK:
                     lResult = SRResultMethodYES;
                     lResultado = lresponse.ListaProductos;
                     NumeroTotal = (int)lresponse.NumeroElementos;
                     break;
                     
                 default:
                     lResult = SRResultMethodNO;
                     lMensaje = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, lMensaje, lResultado, NumeroTotal);
         }
     }];
}

- (Historico *)ObtenerHistoricoSinHistorico:(NSArray *)pLista
{
    Historico *resul = nil;
    
    if (pLista != nil && pLista.count > 0)
    {
        if (pLista.count == 1)
        {
            Historico *lHistorico = pLista[0];
            
            if ([lHistorico.ProductoIdPadre isEqualToString:@"XXX"])
            {
                resul = lHistorico;
            }
        }
    }
    
    return resul;
}

#pragma mark- Metodo Chequear Compra

- (void)ChequearCompra:(EmpresaCarrito *)pCarrito Usuario:(Usuario *)pUsuario DireccionEnvio:(Direccion *)pDireccion Result:(ChequearCompraBlock)pBlock
{
    SRP_ChequearCompra *pMetodo = [SRP_ChequearCompra new];
    
    pMetodo.Direccion = pDireccion;
    pMetodo.IdUsuario = pUsuario.UsuarioId;
    pMetodo.ListaProductos = pCarrito.Elementos;
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         NSDecimalNumber *lGastosEnvio = nil;
         NSString *lMailPaypal = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_ChequearCompraResponse *lresponse = [pResponse getResultObjectForClass:[SRP_ChequearCompraResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:      // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     lGastosEnvio = lresponse.GastosEnvioResponse;
                     lMailPaypal = lresponse.MailPaypalResponse;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     lGastosEnvio = nil;
                     lMailPaypal = @"";
                     break;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(resultado, MensajeSap, lGastosEnvio, lMailPaypal);
         }
     }];
}

#pragma mark- Metodo Cancelar Compra

- (void)CancelarCompra:(NSString *)pIdCompra Result:(CancelarCompraBlock)pBlock
{
    SRP_CancelarCompra *pMetodo = [SRP_CancelarCompra new];
    
    pMetodo.IdCompra = pIdCompra;
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lResult = SRResultMethodErrConex;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_ChequearCompraResponse *lresponse = [pResponse getResultObjectForClass:[SRP_ChequearCompraResponse class]];
             
             if (lresponse != NULL)
             {
                 lResult = SRResultMethodYES;
             }
             else
             {
                 lResult = SRResultMethodNO;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult);
         }
     }];
}

#pragma mark- Metodo Obtener Textos

- (void)ObtenerTexto:(NSString *)pID Result:(ObtenerTextoBlock)pBlock
{
    SRM_ObtenerTextos *pMetodo = [SRM_ObtenerTextos new];
    
    pMetodo.CodigoMenu = pID;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lResult = SRResultMethodErrConex;
         NSString *lTexto = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_ObtenerTextosResponse *lresponse = [pResponse getResultObjectForClass:[SRM_ObtenerTextosResponse class]];
             
             if (lresponse != NULL)
             {
                 lResult = SRResultMethodYES;
                 lTexto = lresponse.Texto;
             }
             else
             {
                 lResult = SRResultMethodNO;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, lTexto);
         }
     }];
}

#pragma mark- Metodo Valorar Producto Comprado Historico

- (void)ValorarProductoCompra:(NSString *)pIdProducto Pedido:(NSString *)pIdPedido Valoracion:(int)pValor TextoValoracion:(NSString *)pTextoValoracion Result:(ValorarProductoCompraBlock)pBlock
{
    SRP_ValorarProductoCompra *pMetodo = [SRP_ValorarProductoCompra new];
    
    pMetodo.Parametros.IDPedido         = pIdPedido;
    pMetodo.Parametros.IDProducto       = pIdProducto;
    pMetodo.Parametros.Valor            = pValor;
    pMetodo.Parametros.TextoValoracion  = pTextoValoracion,
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lResult = SRResultMethodErrConex;
         NSString *lTexto = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_ValorarProductoCompraResponse *lresponse = [pResponse getResultObjectForClass:[SRP_ValorarProductoCompraResponse class]];
             
             switch (lresponse.Codigo)
             {
                 case SoapGenericMethodResponseOK:
                     lResult = SRResultMethodYES;
                     break;
                     
                 case SoapGenericMethodResponseWarning:
                     lResult = SRResultMethodYES;
                     break;
                     
                 default:
                     lResult = SRResultMethodNO;
                     lTexto = lresponse.Mensaje;
                     break;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, lTexto);
         }
     }];
}

#pragma mark- Metodo Valorar Producto Comprado Historico

- (void)ObtenerListaEmoticonos:(ObtenerListaEmoticonosBlock)pBlock
{
    SRP_DevolverEmoticonos *pMetodo = [SRP_DevolverEmoticonos new];
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lResult = SRResultMethodErrConex;
         NSArray *lLista = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_DevolverEmoticonosResponse *lresponse = [pResponse getResultObjectForClass:[SRP_DevolverEmoticonosResponse class]];
             lLista = lresponse.ListaEmoticonos;
             
             if (lLista != nil)
             {
                 lResult = SRResultMethodYES;
             }
             else
             {
                 lResult = SRResultMethodNO;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, lLista);
         }
     }];
}

#pragma mark- Metodo Valorar Producto Comprado Historico

- (void)ObtenerListaTitulosInformacion:(ObtenerListaTitulosInformacionBlock)pBlock
{
    SRM_ObtenerTitulosInformacion *pMetodo = [SRM_ObtenerTitulosInformacion new];
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lResult = SRResultMethodErrConex;
         NSArray *lLista = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_ObtenerTitulosInformacionResponse *lresponse = [pResponse getResultObjectForClass:[SRM_ObtenerTitulosInformacionResponse class]];
             lLista = lresponse.ListaTitulos;
             
             if (lLista != nil)
             {
                 lResult = SRResultMethodYES;
             }
             else
             {
                 lResult = SRResultMethodNO;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, lLista);
         }
     }];
}

#pragma mark- Metodo Obtener Lista Sitios Yaesmio

- (void)ObtenerListaSitiosYaesmio:(ObtenerListaSitiosYaesmioBlock)pBlock
{
    SRM_ObtenerSitiosYaesmio *pMetodo = [SRM_ObtenerSitiosYaesmio new];
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lResult = SRResultMethodErrConex;
         NSArray *lLista = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_ObtenerSitiosYaesmioResponse *lresponse = [pResponse getResultObjectForClass:[SRM_ObtenerSitiosYaesmioResponse class]];
             lLista = lresponse.ListaSitios;
             
             if (lLista != nil)
             {
                 lResult = SRResultMethodYES;
             }
             else
             {
                 lResult = SRResultMethodNO;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, lLista);
         }
     }];
}

#pragma mark- Metodo Obtener Lista Productos con filtro

- (void)ObtenerListaProductos:(FiltroBusquedaProductos *)pFiltro PosicionInicial:(int)pPosicionInicial NumeroElementos:(int)pNumeroElementos Result:(ObtenerListaProductosBlock)pBlock
{
    SRP_ObtenerListaProductos *pMetodo = [SRP_ObtenerListaProductos new];
    
    [pFiltro AsignarPosicion:pPosicionInicial YNumeroElementos:pNumeroElementos];
    pMetodo.filtro = pFiltro;
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod lResult = SRResultMethodErrConex;
         NSArray *lLista = nil;
         NSString *MensajeSap = @"";
         int NumeroElementos = 0;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_ObtenerListaProductosResponse *lresponse = [pResponse getResultObjectForClass:[SRP_ObtenerListaProductosResponse class]];
             lLista = lresponse.ListaProductos;
             
             if (lresponse.CodigoRetorno == 0)
             {
                 lResult = SRResultMethodYES;
                 NumeroElementos = lresponse.NumeroElementosEncontrados;
             }
             else
             {
                 lResult = SRResultMethodNO;
                 MensajeSap = lresponse.MensajeSAP;
             }
         }
         
         if (pBlock != Nil)
         {
             pBlock(lResult, MensajeSap, lLista, NumeroElementos);
         }
     }];
}

#pragma mark- Metodo Obtener Lista Categorias Producto

- (void)DevolverListaCategoriasProducto:(DevolverListaCategoriasProductoBlock)pBlock
{
    SRM_DevolverListaGrupoArticulo *pMetodo = [SRM_DevolverListaGrupoArticulo new];
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         NSArray *lResult    = nil;
         SRResultMethod lbolResult  = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_DevolverListaGrupoArticuloResponse *lresponse = [pResponse getResultObjectForClass:[SRM_DevolverListaGrupoArticuloResponse class]];
             
             lResult = lresponse.ListaGrupoArticuloResult;
             lbolResult = lresponse.CodigoRetorno == 0 ? SRResultMethodYES : SRResultMethodNO;
             MensajeSap = lresponse.MensajeSAP;
         }
         
         if (pBlock != Nil)
         {
             pBlock(lbolResult, MensajeSap, lResult);
         }
     }];
}

#pragma mark- Metodo Obtener Lista Empresas Busqueda

- (void)DevolverListaEmpresaBusqueda:(DevolverListaEmpresaBusquedaBlock)pBlock
{
    SRM_DevolverListaEmpresas *pMetodo = [SRM_DevolverListaEmpresas new];
    
    pMetodo.Parametros.Modo = @"B";
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         NSArray *lResult    = nil;
         SRResultMethod lbolResult  = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_DevolverListaEmpresasResponse *lresponse = [pResponse getResultObjectForClass:[SRM_DevolverListaEmpresasResponse class]];
             
             lResult = lresponse.ListaEmpresaResult;
             lbolResult = lresponse.CodigoRetorno == 0 ? SRResultMethodYES : SRResultMethodNO;
             MensajeSap = lresponse.MensajeSAP;
         }
         
         if (pBlock != Nil)
         {
             pBlock(lbolResult, MensajeSap, lResult);
         }
     }];
}

#pragma mark- Metodo Obtener Producto por Codigo

- (void)ObtenerProducto:(NSString *)pCodigo Medio:(NSString *)pMedio Canal:(TipoCanalProducto)pTipoCanal Resultado:(ObtenerProductoBlock)pBlock
{
    SRP_ObtenerDatosProductoPorID *pMetodo = [SRP_ObtenerDatosProductoPorID new];
    
    pMetodo.Parametros.Codigo = pCodigo;
    pMetodo.Parametros.Medio = pMedio;
    pMetodo.Parametros.Origen = pTipoCanal;
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         ProductoPadre *lResult    = nil;
         SRResultObtenerProducto lbolResult  = SRResultObtenerProductoErrConex;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_ObtenerDatosProductoPorIDResponse *lresponse = [pResponse getResultObjectForClass:[SRP_ObtenerDatosProductoPorIDResponse class]];
             
             lResult = lresponse.ProductoPadreResponse;
             lbolResult = [self getResultProductoPadre:lresponse.CodigoRetorno Producto:lResult];
             MensajeSap = lresponse.MensajeSAP;
         }
         
         if (pBlock != Nil)
         {
             pBlock(lbolResult, MensajeSap, lResult);
         }
     }];
}

- (SRResultObtenerProducto)getResultProductoPadre:(int)pCodigo Producto:(ProductoPadre *)pProducto
{
    SRResultObtenerProducto lResult = SRResultObtenerProductoErrConex;
    
    switch (pCodigo)
    {
        case SoapGenericMethodResponseOK:
            lResult = SRResultObtenerProductoYES;
            pProducto.ProductoHijo.Latitud = self.HeaderLatitud;
            pProducto.ProductoHijo.Longitud = self.HeaderLongitud;
            break;
            
        case SoapGenericMethodResponseWarning:
            lResult = SRResultObtenerProductoNoYaesmio;
            break;
            
        default:
            lResult = SRResultObtenerProductoNoExiste;
            break;
    }
    
    return lResult;
}

#pragma mark- Metodo Obtener Configuracion App

- (void)ObtenerConfiguracionApp:(ObtenerConfiguracionAppBlock)pBlock
{
    SRA_ObtenerConfiguracionApp *pMetodo = [SRA_ObtenerConfiguracionApp new];
    pMetodo.IdApp = self.ServicioMedia.HeaderIdApp;
    
    [self.ServicioMedia EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         NSString *UrlImagen = @"";
         NSString *UrlSonido = @"";
         NSArray *ListaParametros = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRA_ObtenerConfiguracionAppResponse *lresponse = [pResponse getResultObjectForClass:[SRA_ObtenerConfiguracionAppResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:        // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     UrlImagen = lresponse.UrlImagenFondoApp;
                     UrlSonido = lresponse.UrlSonido;
                     ListaParametros = lresponse.Parametros;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap, UrlImagen, UrlSonido, ListaParametros);
         }
     }];
}

#pragma mark- Realizar compra Precio 0

- (void)RealizarCompraPrecioCero:(ProductoPadre *)pProducto Unidades:(NSDecimalNumber *)pUnidades Usuario:(NSString *)pidUsuario Result:(RealizarCompraPrecioCeroBlock)pBlock
{
    SRP_RealizarCompraPrecioCero *pMetodo = [SRP_RealizarCompraPrecioCero new];
    
    pMetodo.Parametros.Producto = [ElementoCarrito crear:pProducto Unidades:pUnidades];
    pMetodo.Parametros.IdUsuario = pidUsuario;
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_RealizarCompraPrecioCeroResponse *lresponse = [pResponse getResultObjectForClass:[SRP_RealizarCompraPrecioCeroResponse class]];
             
             switch (lresponse.CodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.MensajeSAP;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap);
         }
     }];
}

#pragma mark- Activar notificaciones
- (void)ActivarNotificacionesYaesmio:(NSString *)pIdPush IdUsuario:(NSString *)pIdUsuario MacDevice:(NSString *)pMacDevice Telefono:(NSString *)pTelefono Result:(ActivarNotificacionesYaesmioBlock)pBlock
{
    SNT_ActivarNotificacionPush *pMetodo = [SNT_ActivarNotificacionPush new];
    
    pMetodo.IdPush = pIdPush;
    pMetodo.IdUsuario = pIdUsuario;
    pMetodo.MacDevice = pMacDevice;
    pMetodo.Telefono = pTelefono;
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_ActivarNotificacionPushResponse *lresponse = [pResponse getResultObjectForClass:[SNT_ActivarNotificacionPushResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap);
         }
     }];
}

#pragma mark- Lista Notificaciones Recibidas
- (void)ObtenerListaNotificacionesRecibidas:(NSString *)pIdUsuario NumeroLineas:(int)pNumeroLineas PosicionInicial:(int)pPosicionInicial Visto:(BOOL)pVisto Result:(ObtenerListaNotificacionesRecibidasBlock)pBlock
{
    SNT_ObtenerListaNotificacionesRecibidas *pMetodo = [SNT_ObtenerListaNotificacionesRecibidas new];
    
    pMetodo.IdUsuario = pIdUsuario;
    pMetodo.NumeroLineas = pNumeroLineas;
    pMetodo.PosicionInicial = pPosicionInicial;
    pMetodo.Visto = pVisto;
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         NSArray *ListaNotificaciones = nil;
         int numeroTotal = 0;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_ObtenerListaNotificacionesRecibidasResponse *lresponse = [pResponse getResultObjectForClass:[SNT_ObtenerListaNotificacionesRecibidasResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     ListaNotificaciones = lresponse.ListaNotificaciones;
                     numeroTotal = lresponse.NumeroTotal;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap, ListaNotificaciones, numeroTotal);
         }
     }];
}

#pragma mark- Marcar notificacion Recibida como leida
- (void)MarcarNotificacionRecibida:(NSString *)pIdNotificacion Idusuario:(NSString *)pIdUsuario Tipo:(MarcadoNotificacionRecibida)pTipo Result:(MarcarNotificacionRecibidaComoLeidaBlock)pBlock
{
    SNT_ModificarEstadoRecibidoNotificacion *pMetodo = [SNT_ModificarEstadoRecibidoNotificacion new];
    
    pMetodo.IdUsuario = pIdUsuario;
    pMetodo.IdNotificacion = pIdNotificacion;
    switch (pTipo)
    {
        case MarcadoNotificacionRecibidaLeida:
            pMetodo.EstadoNotificacion = SNTipoNotificacionLeida;
            break;
            
        case MarcadoNotificacionRecibidaEliminada:
            pMetodo.EstadoNotificacion = SNTipoNotificacionEliminada;
            break;
            
        case MarcadoNotificacionRecibidaRecibida:
            pMetodo.EstadoNotificacion = SNTipoNotificacionRecibida;
    }
    
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_ModificarEstadoRecibidoNotificacionResponse *lresponse = [pResponse getResultObjectForClass:[SNT_ModificarEstadoRecibidoNotificacionResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap);
         }
     }];
}

#pragma mark- Lista Notificaciones Enviadas
- (void)ObtenerListaNotificacionesEnviadas:(NSString *)pIdUsuario NumeroLineas:(int)pNumeroLineas PosicionInicial:(int)pPosicionInicial Visto:(BOOL)pVisto Result:(ObtenerListaNotificacionesEnviadasBlock)pBlock
{
    SNT_ObtenerListaNotificacionesEmitidas *pMetodo = [SNT_ObtenerListaNotificacionesEmitidas new];
    
    pMetodo.IdUsuario = pIdUsuario;
    pMetodo.NumeroLineas = pNumeroLineas;
    pMetodo.PosicionInicial = pPosicionInicial;
    pMetodo.Visto = pVisto;
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         NSArray *ListaNotificaciones = nil;
         int numeroTotal = 0;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_ObtenerListaNotificacionesEmitidasResponse *lresponse = [pResponse getResultObjectForClass:[SNT_ObtenerListaNotificacionesEmitidasResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     ListaNotificaciones = lresponse.ListaNotificaciones;
                     numeroTotal = lresponse.NumeroTotal;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap, ListaNotificaciones, numeroTotal);
         }
     }];
}

#pragma mark- Marcar notificacion enviada como eliminada
- (void)MarcarNotificacionEnviadaEliminada:(NSString *)pIdNotificacion Result:(MarcarNotificacionEnviadaEliminadaBlock)pBlock
{
    SNT_ModificarEstadoEnviadoNotificacion *pMetodo = [SNT_ModificarEstadoEnviadoNotificacion new];
    
    pMetodo.IdNotificacion = pIdNotificacion;
    pMetodo.EstadoNotificacion = SNTipoNotificacionEliminada;
    
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_ModificarEstadoEnviadoNotificacionResponse *lresponse = [pResponse getResultObjectForClass:[SNT_ModificarEstadoEnviadoNotificacionResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap);
         }
     }];
}

#pragma mark- Lista Destinatarios Notificacion Enviada
- (void)ObtenerListaDestinatariosNotificacionEnviada:(NSString *)pIdNotificacion Result:(ObtenerListaDestinatariosNotificacionEnviadaBlock)pBlock
{
    SNT_ConsultarDestinatariosNotificacion *pMetodo = [SNT_ConsultarDestinatariosNotificacion new];
    
    pMetodo.IdNotificacion = pIdNotificacion;
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         NSArray *ListaNotificaciones = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_ConsultarDestinatariosNotificacionResponse *lresponse = [pResponse getResultObjectForClass:[SNT_ConsultarDestinatariosNotificacionResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     ListaNotificaciones = lresponse.ListaDestinatarios;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap, ListaNotificaciones);
         }
     }];
}

#pragma mark- Lista Grupos Notificacion creados por el usuario
- (void)ObtenerListaGruposNotificacion:(ObtenerListaGruposNotificacionlock)pBlock
{
    SNT_ObtenerListadoGruposContactos *pMetodo = [SNT_ObtenerListadoGruposContactos new];
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         NSArray *Lista = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_ObtenerListadoGruposContactosResponse *lresponse = [pResponse getResultObjectForClass:[SNT_ObtenerListadoGruposContactosResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     Lista = lresponse.ListaGrupos;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap, Lista);
         }
     }];
}

#pragma mark- Eliminar Grupo Notificacion
- (void)EliminarGrupoNotificacion:(NSString *)pidGrupo Result:(EliminarGrupoNotificacionBlock)pBlock
{
    SNT_EliminarGrupo *pMetodo = [SNT_EliminarGrupo new];
    
    pMetodo.IdGrupo = pidGrupo;
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_EliminarGrupoResponse *lresponse = [pResponse getResultObjectForClass:[SNT_EliminarGrupoResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap);
         }
     }];
}

#pragma mark- Crear Grupo Notificacion
- (void)CrearGrupoNotificacion:(NSString *)pNombreGrupo Contactos:(NSArray *)pListaContactos Result:(CrearGrupoNotificacionBlock)pBlock
{
    SNT_CrearGrupo *pMetodo = [SNT_CrearGrupo new];
    
    pMetodo.NombreGrupo = pNombreGrupo;
    pMetodo.ListaIdContactos = pListaContactos;
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         GrupoNotificacion *grupo = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_CrearGrupoResponse *lresponse = [pResponse getResultObjectForClass:[SNT_CrearGrupoResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     grupo = lresponse.Grupo;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap, grupo);
         }
     }];
}

#pragma mark- Modificar Grupo Notificacion
- (void)ModificarGrupoNotificacion:(GrupoNotificacion *)pGrupo Result:(ModificarGrupoNotificacionBlock)pBlock
{
    SNT_ModificarGrupo *pMetodo = [SNT_ModificarGrupo new];
    
    pMetodo.Grupo = pGrupo;
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_ModificarGrupoResponse *lresponse = [pResponse getResultObjectForClass:[SNT_ModificarGrupoResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap);
         }
     }];
}

#pragma mark- Crear o Eliminar Contacto en Grupo Notificacion
- (void)EliminarContactosDelGrupo:(NSString *)pIdGrupo ListaContactos:(NSArray *)pListaContactos Usuario:(NSString *)pIdUsuario Result:(CrearEliminarContactosDelGrupoBlock)pBlock
{
    [self CrearOEliminarContactoDeGrupo:pIdGrupo ListaContactos:pListaContactos Escrear:@"" Usuario:pIdUsuario Result:pBlock];
}

- (void)AgregarContactosAlGrupo:(NSString *)pIdGrupo ListaContactos:(NSArray *)pListaContactos Usuario:(NSString *)pIdUsuario Result:(CrearEliminarContactosDelGrupoBlock)pBlock
{
    [self CrearOEliminarContactoDeGrupo:pIdGrupo ListaContactos:pListaContactos Escrear:@"X" Usuario:pIdUsuario Result:pBlock];
}

- (void)CrearOEliminarContactoDeGrupo:(NSString *)pIdGrupo ListaContactos:(NSArray *)pListaContactos Escrear:(NSString *)pEsCrear Usuario:(NSString *)pIdUsuario Result:(CrearEliminarContactosDelGrupoBlock)pBlock
{
    SNT_CrearOEliminarContatoEnGrupo *pMetodo = [SNT_CrearOEliminarContatoEnGrupo new];
    
    pMetodo.EsCrear = pEsCrear;
    pMetodo.IdGrupo = pIdGrupo;
    pMetodo.IdUsuario = pIdUsuario;
    pMetodo.ListaIdUsuario = pListaContactos;
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         NSArray *Lista = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_CrearOEliminarContatoEnGrupoResponse *lresponse = [pResponse getResultObjectForClass:[SNT_CrearOEliminarContatoEnGrupoResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     Lista = lresponse.ListaContactos;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap, Lista);
         }
     }];
}

#pragma mark- Comprobar Contactos
- (void)ComprobarListaContactos:(NSArray *)pLista DeUsuario:(NSString *)pIdUsuario Result:(ComprobarListaContactosBlock)pBlock
{
    SNT_ComprobarContactos *pMetodo = [SNT_ComprobarContactos new];
    
    pMetodo.ListaContactos = pLista;
    pMetodo.IdUsuario = pIdUsuario;
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         NSArray *Lista = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_ComprobarContactosResponse *lresponse = [pResponse getResultObjectForClass:[SNT_ComprobarContactosResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     Lista = lresponse.ListaContactos;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap, Lista);
         }
     }];
}

#pragma mark- Enviar Notificacion
- (void)EnviarNotificacionPush:(ProductoPadre *)pProducto DeUsuario:(NSString *)pIdUsuario ListaIdContactos:(NSArray *)pListaIdContactos ListaIdGrupos:(NSArray *)pListaIdGrupos ListaMails:(NSArray *)pListaMails Mensaje:(NSString *)pMensaje Result:(EnviarNotificacionPushBlock)pBlock
{
    SNT_EnviarNotificacionesPush *pMetodo = [SNT_EnviarNotificacionesPush new];
    
    pMetodo.IdProducto = pProducto.ProductoHijo.ProductoId;
    pMetodo.Medio = pProducto.Medio;
    pMetodo.IdUsuario = pIdUsuario;
    pMetodo.ListaMails = pListaMails;
    pMetodo.ListaIdGruposContactos = pListaIdGrupos;
    pMetodo.ListaIdContactosSap = pListaIdContactos;
    pMetodo.TextoMensaje = pMensaje;
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_EnviarNotificacionesPushResponse *lresponse = [pResponse getResultObjectForClass:[SNT_EnviarNotificacionesPushResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap);
         }
     }];
}

#pragma mark- Desactivar Notificaciones Push
- (void)DesactivarNotificacionesPush:(NSString *)pIdPush DeUsuario:(NSString *)pIdUsuario MacDevice:(NSString *)pMacDevice Telefono:(NSString *)pTelefono Result:(DesactivarNotificacionesPushBlock)pBlock
{
    SNT_DesactivarNotificacionesPush *pMetodo = [SNT_DesactivarNotificacionesPush new];
    
    pMetodo.IdUsuario = pIdUsuario;
    pMetodo.IdPush = pIdPush;
    pMetodo.MacDevice = pMacDevice;
    pMetodo.Telefono = pTelefono;
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_DesactivarNotificacionesPushResponse *lresponse = [pResponse getResultObjectForClass:[SNT_DesactivarNotificacionesPushResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap);
         }
     }];
}

#pragma mark- Obtener Provincia de latitud y longitud
- (void)ObtenerProvinciaDeLatitud:(double)pLatitud Longitud:(double)pLongitud OCodigoPostal:(NSString *)pCodigoPostal IdPais:(NSString *)pIdPais Result:(ObtenerProvinciaDeLatitudLongitudBlock)pBlock
{
    SRM_ObtenerProvinciaDeGeolocalizacion *pMetodo = [SRM_ObtenerProvinciaDeGeolocalizacion new];
    
    pMetodo.Latitud = pLatitud;
    pMetodo.Logintud = pLongitud;
    pMetodo.CodigoPostal = pCodigoPostal;
    pMetodo.IdPais = pIdPais;
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         Provincia *pProvincia = nil;
         Pais *pPais = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_ObtenerProvinciaDeGeolocalizacionResponse *lresponse = [pResponse getResultObjectForClass:[SRM_ObtenerProvinciaDeGeolocalizacionResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     pPais = [Pais new];
                     pProvincia = [Provincia new];
                     pProvincia.Codigo = lresponse.IdProvincia;
                     pProvincia.Nombre = lresponse.NombreProvincia;
                     pPais.Nombre = lresponse.NombrePais;
                     pPais.Codigo = lresponse.IdPais;
                     break;
                     
                 case SoapGenericMethodResponseWarning:
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap, pPais, pProvincia);
         }
     }];
}

#pragma mark- Obtener Numero Notificaciones No Leidas
- (void)ObtenerNumeroNotificacionesNoLeidas:(ObtenerNumeroNotificacionesNoLeidasBlock)pBlock
{
    SNT_ObtenerNumeroNotificacionesNoLeidas *pMetodo = [SNT_ObtenerNumeroNotificacionesNoLeidas new];
    
    [self.ServicioNotificaciones EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         NSInteger numero = 0;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SNT_ObtenerNumeroNotificacionesNoLeidasResponse *lresponse = [pResponse getResultObjectForClass:[SNT_ObtenerNumeroNotificacionesNoLeidasResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     numero = lresponse.NumeroNotificacionesSinLeer;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap, numero);
         }
     }];
}

#pragma mark- Obtener Lista Tarjeta Pago Usuario
- (void)ObtenerListaTarjetasUsuario:(ObtenerListaTarjetasUsuarioBlock)pBlock
{
    SRM_ObtenerTarjetas *pMetodo = [SRM_ObtenerTarjetas new];
    
    [self.ServicioRegistro EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         NSArray *Lista = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRM_ObtenerTarjetasResponse *lresponse = [pResponse getResultObjectForClass:[SRM_ObtenerTarjetasResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:                 // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     Lista = lresponse.ListaTarjetas;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap, Lista);
         }
     }];
}

#pragma mark- Obtener Lista Monedas
- (void)ObtenerListaMonedas:(ObtenerListaMonedasBlock)pBlock
{
    SRP_ObtenerListadoMonedasPorUso *pMetodo = [SRP_ObtenerListadoMonedasPorUso new];
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         NSArray *Lista = nil;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_ObtenerListadoMonedasPorUsoResponse *lresponse = [pResponse getResultObjectForClass:[SRP_ObtenerListadoMonedasPorUsoResponse class]];
             
             switch (lresponse.RespuestaCodigoRetorno)
             {
                 case SoapGenericMethodResponseWarning:        // advertencia
                 case SoapGenericMethodResponseOK:
                     resultado = SRResultMethodYES;
                     Lista = lresponse.ListaMonedas;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.RespuestaMensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap, Lista);
         }
     }];
}

#pragma mark- Obtener Lista Destacados
- (void)ObtenerListaDestacados:(NSInteger)Pagina Dispositivo:(NSString *)TipoDispositivo Result:(ObtenerListaDestacadosBlock)pBlock
{
    SRP_ObtenerListaDestacados *pMetodo = [SRP_ObtenerListaDestacados new];
    
    pMetodo.Pagina = Pagina;
    pMetodo.TipoDispositivo = TipoDispositivo;
    
    [self.ServicioProductos EjecutarMetodoAsincrono:pMetodo Response: ^(ServicioSoapBindingResponse *pResponse)
     {
         SRResultMethod resultado = SRResultMethodErrConex;
         NSString *MensajeSap = @"";
         NSArray *Lista = nil;
         NSInteger numeroPaginas = 1;
         
         if (pResponse != nil && pResponse.error == nil)
         {
             SRP_ObtenerListaDestacadosResponse *lresponse = [pResponse getResultObjectForClass:[SRP_ObtenerListaDestacadosResponse class]];
             
             switch (lresponse.CodigoResultado)
             {
                 case 0:
                     resultado = SRResultMethodYES;
                     Lista = lresponse.ListaDestacados;
                     numeroPaginas = lresponse.PaginasTotales;
                     break;
                     
                 default:
                     resultado = SRResultMethodNO;
                     MensajeSap = lresponse.MensajeSap;
                     break;
             }
         }
         
         if (pBlock != nil)
         {
             pBlock(resultado, MensajeSap, Lista, numeroPaginas);
         }
     }];
}

@end
