//
//  EvaluacionesProducto.m
//  Yaesmio
//
//  Created by freelance on 07/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EvaluacionProducto.h"

@implementation EvaluacionProducto


- (id)init
{
    if ((self = [super init]))
    {
        self.Fecha          = nil;
        self.NombreUsuario  = @"";
        self.Comentario     = @"";
        self.Nota           = [NSDecimalNumber zero];
    }
    
    return self;
}

+ (EvaluacionProducto *)deserializeNode:(xmlNodePtr)cur
{
    EvaluacionProducto *newObject = [EvaluacionProducto new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (EvaluacionProducto *)Clonar
{
    EvaluacionProducto *resultado = [EvaluacionProducto new];
    
    resultado.Fecha          = self.Fecha;
    resultado.NombreUsuario  = self.NombreUsuario;
    resultado.Comentario     = self.Comentario;
    resultado.Nota           = self.Nota;
    
    return resultado;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"Fecha"])
    {
        NSString *lDato = [NSString deserializeNode:pobjCur];
        
        if ([lDato isEqualToString:@"0000-00-00"] == false)
        {
            if (self.Fecha == nil)
            {
                self.Fecha = [NSDate dateWithTimeIntervalSince1970:0];
            }
            
            NSDate *mydate = [NSDate fromString:lDato WithFormat:FECHA_SOAP];
            self.Fecha = [self.Fecha dateWithYear:mydate.Year Month:mydate.Month Day:mydate.Day Hour:UNCHANGEDATE minute:UNCHANGEDATE second:UNCHANGEDATE];
        }
    }
    else if ([nodeName isEqualToString:@"Hora"])
    {
        NSString *lDato = [NSString deserializeNode:pobjCur];
        
        if ([lDato isEqualToString:@"00:00:00"] == false)
        {
            if (self.Fecha == nil)
            {
                self.Fecha = [NSDate dateWithTimeIntervalSince1970:0];
            }
            
            NSDate *myFecha = [NSDate fromString:lDato WithFormat:@"HH:mm:ss"];
            self.Fecha = [self.Fecha dateWithYear:UNCHANGEDATE Month:UNCHANGEDATE Day:UNCHANGEDATE Hour:myFecha.Hour minute:myFecha.Minute second:myFecha.Second];
        }
    }
    else if ([nodeName isEqualToString:@"NombreUsuario"])
    {
        self.NombreUsuario = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Nota"])
    {
        self.Nota = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"TextoEvaluacion"])
    {
        self.Comentario = [NSString deserializeNode:pobjCur];
    }
}

@end
