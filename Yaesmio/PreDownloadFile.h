//
//  PreDownloadFile.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 24/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DownloadManager.h"

@interface PreDownloadFile : NSObject

typedef void (^ResultDownloadBlock)(PreDownloadFile *pResult);

@property (copy)     getAssyncDataWithUrlBlock       DataBlockResult;
@property (copy)     getAssyncUrlFileWithUrlBlock    FileBlockResult;
@property (strong)   NSURLRequest                    *Request;
@property (strong)   NSURLConnection                 *Connection;
@property (strong)   NSMutableData                   *Data;
@property (strong)   NSString                        *FilePath;
@property (assign)   DownloadManagerTypeFile         FileType;
@property (strong)   NSURL                           *Url;
@property (assign)   BOOL                            DownloadCorrect;
@property (strong)   NSString                        *Grupo;
@property (readonly) NSString                        *ID;

-(PreDownloadFile*) initWithURL:(NSURL*) pUrl Type:(DownloadManagerTypeFile)pType FilePath:(NSString*)pFilePath Grupo:(NSString*)pGrupo FileBlock:(getAssyncUrlFileWithUrlBlock)pFileBlock DataBlock:(getAssyncDataWithUrlBlock) pDataBlock;

-(void) InitDownloadWithResult:(ResultDownloadBlock)pResult;

@end
