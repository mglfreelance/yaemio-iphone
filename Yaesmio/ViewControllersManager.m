//
//  ViewControllersManager.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 14/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ViewControllersManager.h"
#import "WebViewController.h"
#import "LoginUsuarioViewController.h"
#import "CarritoEmpresaViewController.h"
#import "NavegacionViewController.h"
#import "CompraViewController.h"
#import "MantenimientoDireccionEnvioViewController.h"
#import "GaleriaImagenesViewController.h"
#import "PickerElementosViewController.h"
#import "CompartirViewController.h"
#import "NotificacionProductoViewController.h"
#import "FavoritosViewController.h"
#import "PickerFechaViewController.h"
#import "EditarDatosUsuarioViewController.h"
#import "PickerValoresAtributosViewController.h"
#import "PayPalViewController.h"
#import "ValorarHistoricoViewController.h"
#import "SitiosYaesmioViewController.h"
#import "VideoViewController.h"
#import "BienvenidaViewController.h"
#import <AddressBookUI/AddressBookUI.h>
#import <MessageUI/MessageUI.h>
#import "ProductoPageViewController.h"
#import "ListaDestinatariosViewController.h"
#import "EditarGrupoNotificacionViewController.h"
#import "SeleccionContactosViewController.h"
#import "EnviarNotificacionViewController.h"
#import "NotificacionesViewController.h"
#import "QRCapturaViewController.h"
#import "BusquedaViewController.h"
#import "CarritoViewController.h"
#import "ConfigurationParameter.h"

@interface ViewControllersManager () <ABNewPersonViewControllerDelegate, MFMessageComposeViewControllerDelegate>

@end

@implementation ViewControllersManager

- (void)PresentModal:(UIViewController *)pView actual:(UIViewController *)actual
{
    NavegacionViewController *nav = [pView.storyboard instantiateViewControllerWithIdentifier:@"NavegacionViewController"];
    
    [nav setViewControllers:@[actual]];
    
    [pView presentViewController:nav animated:true completion:Nil];
}

- (void)PresentSpecialModal:(UIViewController *)pView actual:(UIViewController *)actual
{
    NavegacionViewController *nav = [pView.storyboard instantiateViewControllerWithIdentifier:@"NavegacionViewController"];
    
    [nav setViewControllers:@[actual]];
    
    if ([actual respondsToSelector:@selector(MuestraModal)])
    {
        [actual performSelector:@selector(MuestraModal)];
    }
    
    [pView presentModalViewController:nav withPushDirection:kCATransitionFromRight];
}

#pragma mark- WebView

- (void)MostrarListaSitiosYaesmio:(UIViewController *)pView
{
    SitiosYaesmioViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"SitiosYaesmioViewController"];
    
    [pView.navigationController pushViewController:lView animated:true];
}

- (void)MostrarWebView:(UIViewController *)pView url:(NSString *)pUrl
{
    WebViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    
    lView.cadenaUrl = [self componerUrlCorrecta:pUrl];
    lView.MostrarLinkSafari = false;
    
    [self PresentModal:pView actual:lView];
}

- (void)MostrarWebView:(UIViewController *)pView ContenidoWeb:(NSString *)pContenido
{
    WebViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"WebViewController"];
    
    lView.ContenidoWeb = pContenido;
    lView.MostrarLinkSafari = true;
    
    [pView.navigationController pushViewController:lView animated:true];
}

- (BOOL)MostrarWebSafari:(NSString *)purl
{
    purl = [self componerUrlCorrecta:purl];
    NSURL *myurl = [NSURL URLWithString:purl];
    return [self MostrarWebSafariConNSURL:myurl];
}

- (BOOL)MostrarWebSafariConNSURL:(NSURL *)purl
{
    BOOL b =  [[UIApplication sharedApplication] openURL:purl];
    
    if (b == false)
    {
        NSLog(@"%@%@", @"Failed to open url:", [purl description]);
    }
    
    return b;
}

- (NSString *)componerUrlCorrecta:(NSString *)pUrl_p
{
    NSString *result = pUrl_p;
    
    if ([pUrl_p hasPrefix:@"http://"] == FALSE && [pUrl_p hasPrefix:@"https://"] == FALSE)
    {
        result = [NSString stringWithFormat:@"http://%@", pUrl_p];
    }
    
    return result;
}

#pragma mark- Bienvenido

- (void)MostrarBienvenida:(UIViewController *)pView
{
    BienvenidaViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"BienvenidaViewController"];
    
    [self PresentModal:pView actual:lView];
}

#pragma mark- Login Usuario

- (void)MostrarLoginUsuario:(UIViewController *)pView Result:(ResultadoPorDefectoBlock)pResult
{
    [self AutoLogarUsuario:pView MostrarOcupado:true Result: ^()
     {
         if ([Locator Default].Preferencias.EstaUsuarioLogado)
         {
             if (pResult != nil)
             {
                 pResult([Locator Default].Preferencias.EstaUsuarioLogado ? 1 : 0);
             }
         }
         else
         {
             LoginUsuarioViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"LoginUsuarioViewController"];
             lView.Resultado = ^(int pResultado)
             {
                 //cuando se loge el usuario mostrar favoritos guardados.
                 [[Locator Default].ProductosFavoritos ActualizarEstado];
                 [self ComprobarDatosUsuarioCompletados:pView];
                 
                 if (pResult)
                 {
                     pResult(pResultado);
                 }
             };
             lView.VistaInicial = pView;
             
             [self PresentModal:pView actual:lView];
         }
     }];
}

- (void)AutoLogarUsuario:(UIViewController *)pView MostrarOcupado:(BOOL)pMostrarOcupado Result:(void (^)())pBlock
{
    if ([Locator Default].Preferencias.EstaUsuarioLogado)
    {
        if (pBlock != nil)
        {
            pBlock();
        }
    }
    else if ([Locator Default].Preferencias.EsPosibleAutologar)
    {
        if (pMostrarOcupado)
        {
            [[Locator Default].Alerta showBussy:@"Iniciando sesion"];
        }
        
        [[Locator Default].ServicioSOAP IdentificacionUsuarioWithEmail:[Locator Default].Preferencias.MailUsuario AndPassword:[Locator Default].Preferencias.Password Result: ^(SRResultIdentificacion pResult, LoginUsuarioResponse *response)
         {
             [LoginUsuarioViewController ActivarUsuario:response Email:[Locator Default].Preferencias.MailUsuario password:[Locator Default].Preferencias.Password];
             
             [self ComprobarDatosUsuarioCompletados:pView];
             
             if (pMostrarOcupado)
             {
                 [[Locator Default].Alerta hideBussy];
             }
             
             if (pBlock != nil)
             {
                 pBlock();
             }
         }];
    }
    else if (pBlock != nil)
    {
        pBlock();
    }
}

- (void)ComprobarDatosUsuarioCompletados:(UIViewController *)pView
{
    BOOL UsuarioLogado = [Locator Default].Preferencias.EstaUsuarioLogado;
    BOOL CompletadoDatosUsuario = [Locator Default].Preferencias.CompletadoDatosUsuario;
    int NumeroVecesParaMostrarmensaje = [Locator Default].Preferencias.NumeroVecesAMostrarMensajeCompletarUsuario;
    
    if (UsuarioLogado && CompletadoDatosUsuario == false && NumeroVecesParaMostrarmensaje == 0)
    {
        [[Locator Default].ServicioSOAP ObtenerUsuario:[Locator Default].Preferencias.IdUsuario Result: ^(SRResultMethod pResultUsuario, NSString *pMensaje, Usuario *pUsuario)
         {
             if (pResultUsuario == SRResultMethodYES)
             {
                 if ([pUsuario.Nombre isEqualToString:@""] || [pUsuario.Apellidos isEqualToString:@""])
                 {
                     [[Locator Default].Alerta showMessage:Traducir(@"¡Bienvenido a yaesmio!") Message:Traducir(@"Completa tus datos personales y podás participar en las promociones yaesmio!") cancelButtonTitle:Traducir(@"Recordár más tarde") AceptButtonTitle:Traducir(@"Aceptar") completionBlock: ^(NSUInteger buttonIndex)
                      {
                          if (buttonIndex == 0)
                          {
                              [Locator Default].Preferencias.NumeroVecesAMostrarMensajeCompletarUsuario = 10;
                          }
                          else
                          {
                              [[Locator Default].Vistas MostrarEdicionUsuario:pView Usuario:pUsuario Result:Nil];
                          }
                      }];
                 }
                 else
                 {
                     [Locator Default].Preferencias.CompletadoDatosUsuario = true;
                 }
             }
         }];
    }
    else if (UsuarioLogado && CompletadoDatosUsuario == false && NumeroVecesParaMostrarmensaje > 0)
    {
        [Locator Default].Preferencias.NumeroVecesAMostrarMensajeCompletarUsuario = [Locator Default].Preferencias.NumeroVecesAMostrarMensajeCompletarUsuario - 1;
    }
}

#pragma mark- Carrito Compra

- (void)MostrarCarritoCompra:(UIViewController *)pView
{
    if ([Locator Default].Preferencias.EstaUsuarioLogado)
    {
        CarritoViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"CarritoViewController"];
        
        [self PresentModal:pView actual:lView];
    }
    else
    {
        [[Locator Default].Alerta showMessageLoginIfNotLogin:pView Message:Traducir(@"Por favor, inicia sesion o registrate para acceder a tu carrito de compra.") Result: ^(int pResult)
         {
             if (pResult == 1)                      //OK
             {
                 [self MostrarCarritoCompra:pView];
             }
         }];
    }
}

#pragma mark- Editar Producto

- (void)MostrarEditarProducto:(UIViewController *)pView Producto:(ElementoCarrito *)pProducto
{
    ProductoPageViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"ProductoPageViewController"];
    
    lView.ElementoCarrito = pProducto;
    
    [self PresentModal:pView actual:lView];
}

#pragma mark- Compra

#pragma mark- Editar Direccion Envio

- (void)MostrarEditarDireccionEnvio:(UIViewController *)pView Direccion:(Direccion *)pDireccion Tipo:(DireccionEnvioModo)pModo EsVistaCarrito:(BOOL)pEsVistaCarrito Result:(ResultadoEditarDireccionBlock)pResult
{
    MantenimientoDireccionEnvioViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"CrearDireccionEnvio"];
    
    lView.Direccion = pDireccion;
    lView.TipoDireccion = pModo;
    lView.Resultado = pResult;
    lView.EsVistaCarrito = pEsVistaCarrito;
    
    [pView.navigationController pushViewController:lView animated:true];
}

- (void)MostrarEditarDireccionEnvioPrincipal:(UIViewController *)pView Direccion:(Direccion *)pDireccion EsVistaCarrito:(BOOL)pEsVistaCarrito Result:(ResultadoEditarDireccionBlock)pResult
{
    [self MostrarEditarDireccionEnvio:pView Direccion:pDireccion Tipo:DireccionEnvioModoPrincipal EsVistaCarrito:pEsVistaCarrito Result:pResult];
}

- (void)MostrarEditarDireccionEnvioAlternativa:(UIViewController *)pView Direccion:(Direccion *)pDireccion EsVistaCarrito:(BOOL)pEsVistaCarrito Result:(ResultadoEditarDireccionBlock)pResult
{
    [self MostrarEditarDireccionEnvio:pView Direccion:pDireccion Tipo:DireccionEnvioModoAlternativa EsVistaCarrito:pEsVistaCarrito Result:pResult];
}

- (void)MostrarEditarDireccionEnvioSoloEnvio:(UIViewController *)pView Direccion:(Direccion *)pDireccion Result:(ResultadoEditarDireccionBlock)pResult
{
    [self MostrarEditarDireccionEnvio:pView Direccion:pDireccion Tipo:DireccionEnvioModoSoloEnvio EsVistaCarrito:true Result:pResult];
}

#pragma mark- Galeria Imagenes

- (void)MostrarGaleriaImagenes:(UIViewController *)pView ListaImagenes:(NSArray *)pLista UrlImagenEmpresa:(NSString *)pUrlLogo
{
    [self MostrarGaleriaImagenes:pView ListaImagenes:pLista NumImagenAmostrar:0 UrlImagenEmpresa:pUrlLogo];
}

- (void)MostrarGaleriaImagenes:(UIViewController *)pView ListaImagenes:(NSArray *)pLista NumImagenAmostrar:(int)pNumImagen UrlImagenEmpresa:(NSString *)pUrlLogo
{
    GaleriaImagenesViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"MostrarImagenViewController"];
    
    lView.ListaMedia = pLista;
    lView.NumeroImagenActual = pNumImagen;
    lView.UrlImageEmpresa = pUrlLogo;
    
    [pView.navigationController pushViewController:lView animated:true];
}

#pragma mark- Mostrar video Pantalla Completa
- (void)MostrarVideoAPantallaCompleta:(UIViewController *)pView urlVideo:(NSString *)pUrlVideo UrlImagenEmpresa:(NSString *)pUrlLogo Timer:(NSTimeInterval)pTimer Result:(ResultTimerMostrarVideoBlock)pResult
{
    VideoViewController *lvideo = [[VideoViewController alloc] initWithContentURL:[NSURL URLWithString:pUrlVideo] LogoEmpresa:pUrlLogo Timer:pTimer Resul: ^(NSTimeInterval pTimer)
                                   {
                                       if (pResult != nil)
                                       {
                                           pResult(pTimer);
                                       }
                                   }];
    
    [pView presentViewController:lvideo animated:true completion:Nil];
    lvideo = nil;
}

#pragma mark- Vista Picker

PickerElementosViewController *mPicker;
- (void)MostrarPickerSemiModal:(UIViewController *)pView ListaElementos:(NSArray *)pLista Seleccionado:(ElementoPicker *)pSeleccionado Titulo:(NSString *)pTitulo Result:(MostrarPickerSemiModalBlock)pResultado
{
    mPicker = [pView.storyboard instantiateViewControllerWithIdentifier:@"PickerElementosViewController"];
    
    mPicker.ListaElementos = pLista;
    mPicker.NombrePantalla = pTitulo;
    mPicker.ElementoSeleccionado = pSeleccionado;
    mPicker.Respond = ^(ElementoPicker *pResult)
    {             // se guarda el viewcontroller para que no lo libere IOS y ahora lo libero yo
        pResultado(pResult);
        mPicker = nil;
    };
    
    [pView presentSemiModalViewController:mPicker inView:pView.view.window];
}

#pragma mark- Vista Elementos Atributo Picker

PickerValoresAtributosViewController *mPickerValoresAtributo;
- (void)MostrarValoresAtributoPickerSemiModal:(UIViewController *)pView ListaElementos:(NSArray *)pLista Seleccionado:(ValoresdeAtributo *)pSeleccionado Titulos:(NSArray *)pListaTitulos Result:(MostrarValoresAtributoPickerSemiModalBlock)pResultado
{
    mPickerValoresAtributo = [pView.storyboard instantiateViewControllerWithIdentifier:@"PickerValoresAtributosViewController"];
    
    mPickerValoresAtributo.ListaElementos = pLista;
    mPickerValoresAtributo.ListaNombreValores = pListaTitulos;
    mPickerValoresAtributo.ElementoSeleccionado = pSeleccionado;
    mPickerValoresAtributo.Respond = ^(ValoresdeAtributo *pResult)
    {               // se guarda el viewcontroller para que no lo libere IOS y ahora lo libero yo
        pResultado(pResult);
        mPickerValoresAtributo = nil;
    };
    
    [pView presentSemiModalViewController:mPickerValoresAtributo inView:pView.view.window];
}

#pragma mark- Vista DatePicker

PickerFechaViewController *mDatePicker;
- (void)MostrarDatePickerSemiModal:(UIViewController *)pView Seleccionado:(NSDate *)pSeleccionado MaxFecha:(NSDate *)pMaxFecha Titulo:(NSString *)pTitulo Result:(MostrarDatePickerSemiModalBlock)pResultado
{
    mDatePicker = [pView.storyboard instantiateViewControllerWithIdentifier:@"PickerFechaViewController"];
    
    mDatePicker.NombrePantalla = pTitulo;
    mDatePicker.FechaSeleccionada = pSeleccionado;
    mDatePicker.FechaMaxima = pMaxFecha;
    mDatePicker.Respond = ^(NSDate *pResult)
    {             // se guarda el viewcontroller para que no lo libere IOS y ahora lo libero yo
        pResultado(pResult);
        mDatePicker = nil;
    };
    
    [pView presentSemiModalViewController:mDatePicker inView:pView.view.window];
}

#pragma mark- Vista Compartir

CompartirViewController *mCompartir;
- (void)MostrarCompartirSemiModal:(UIViewController *)pView Producto:(ProductoPadre *)pProducto result:(ResultadoBlock)pResultado
{
    mCompartir = [pView.storyboard instantiateViewControllerWithIdentifier:@"CompartirViewController"];
    
    mCompartir.producto = pProducto;
    
    mCompartir.Respond = ^(int pResult)
    {             // se guarda el viewcontroller para que no lo libere IOS y ahora lo libero yo
        mCompartir = nil;
        
        if (pResultado != nil)
        {
            pResultado();
        }
    };
    
    [pView presentSemiModalViewController:mCompartir inView:pView.view.window];
}

#pragma mark- Vista Notificacion Producto Agregado Carrito

- (void)MostrarNotificacionProducto:(UIViewController *)pView Producto:(ProductoPadre *)pProducto Result:(void (^)())pResult
{
    [NotificacionProductoViewController MostrarNotificacion:pView Imagen:pProducto.ProductoHijo.ImagenPorDefecto.UrlPathSmall Descripcion:[Traducir(@"Has añadido @ al carrito de la compra.") stringByReplacingOccurrencesOfString:@"@" withString:pProducto.ProductoHijo.Nombre] Result:pResult];
}

#pragma mark- Mostrar Producto

- (void)MostrarProducto:(UIViewController *)pView Producto:(ProductoPadre *)pProductoPadre VieneDeFavoritos:(BOOL)pVieneDeFavoritos
{
    ProductoPageViewController *actual = [pView.storyboard instantiateViewControllerWithIdentifier:@"ProductoPageViewController"];
    
    actual.Producto = pProductoPadre;
    actual.VieneDeFavoritos = pVieneDeFavoritos;
    
    [pView.navigationController pushViewController:actual animated:true];
}

- (void)MostrarProductoModal:(UIViewController *)pView Producto:(ProductoPadre *)pProductoPadre VieneDeFavoritos:(BOOL)pVieneDeFavoritos
{
    ProductoPageViewController *actual = [pView.storyboard instantiateViewControllerWithIdentifier:@"ProductoPageViewController"];
    
    actual.Producto = pProductoPadre;
    actual.VieneDeFavoritos = pVieneDeFavoritos;
    
    [self PresentSpecialModal:pView actual:actual];
}

#pragma  mark- Mostrar Edicion Usuario
- (void)MostrarEdicionUsuario:(UIViewController *)pView Usuario:(Usuario *)pUsuario Result:(ResultadoPorDefectoBlock)pResult
{
    EditarDatosUsuarioViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"EditarDatosUsuarioViewController"];
    
    lView.usuario = pUsuario;
    lView.esModal = true;
    lView.Resultado = pResult;
    
    [self PresentModal:pView actual:lView];
}

- (void)MostrarMensajeNoQR:(UIViewController *)pView Mensaje:(NSString *)pMensaje
{
    [[Locator Default].Alerta showMessageAceptWithTitle:Traducir(@"¡Ups!") AndMessage:pMensaje ResulBlock: ^
     {
         [[Locator Default].Vistas MostrarListaSitiosYaesmio:pView];
     }];
}

#pragma  mark- Mostrar Pago PayPal
PayPalViewController *mPaypal;

- (void)MostrarPantallaPayPal:(UIViewController *)pView Carrito:(EmpresaCarrito *)pCarrito Usuario:(Usuario *)pUsuario Result:(MostrarPantallaPayPalBlock)pResult
{
    PayPalViewController *lView = [[PayPalViewController alloc] initWithCarrito:pCarrito Usuario:pUsuario];
    
    lView.Respond = ^(NSString *pIdPaypal)
    {
        if (pResult != nil)
        {
            pResult(pIdPaypal);
        }
    };
    
    if (lView != nil)
    {
        [pView presentViewController:lView animated:true completion:Nil];
    }
}

#pragma mark- Mostrar Pantalla Valorar producto historico

- (void)MostrarValoracionProductoHistorico:(UIViewController *)pView Historico:(Historico *)pHistorico Emoticonos:(NSArray *)pLista Resultado:(ResultadoPorDefectoBlock)pResult
{
    ValorarHistoricoViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"ValorarHistoricoViewController"];
    
    lView.ProductoHistorico = pHistorico;
    lView.Resultado = pResult;
    lView.ListaEmoticonos = pLista;
    
    [pView.navigationController pushViewController:lView animated:true];
}

#pragma mark- Mostrar Pantalla Nuevo Contacto

VcardImporter *mContacto;
- (void)MostrarPantallaContacto:(UIViewController *)pView Datos:(VcardImporter *)pContacto
{
    mContacto = pContacto;
    ABNewPersonViewController *newPersonViewController = [[ABNewPersonViewController alloc] init];
    newPersonViewController.displayedPerson = pContacto.personRecord;
    newPersonViewController.newPersonViewDelegate = self;
    
    [self PresentModal:pView actual:newPersonViewController];
}

- (void)newPersonViewController:(ABNewPersonViewController *)newPersonView didCompleteWithNewPerson:(ABRecordRef)person
{
    [newPersonView dismissViewControllerAnimated:TRUE completion:NULL];
    mContacto = nil;
}

#pragma mark- Mostrar Pantalla Nuevo Contacto

- (void)MostrarPantallaSMS:(UIViewController *)pView Telefono:(NSString *)pTelefono Texto:(NSString *)pMensaje
{
    MFMessageComposeViewController *controller = [[MFMessageComposeViewController alloc] init];
    
    if ([MFMessageComposeViewController canSendText])
    {
        controller.body = pMensaje;
        controller.recipients = [NSArray arrayWithObjects:pTelefono, nil];
        controller.messageComposeDelegate = self;
        [pView presentViewController:controller animated:true completion:Nil];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [controller dismissViewControllerAnimated:TRUE completion:NULL];
}

#pragma mark- Mostrar Pantalla Seleccion Paises

- (void)MostrarSeleccionPais:(UIViewController *)pView Tipo:(TipoListaPaises)Tipo PaisSeleccionado:(Pais *)PaisSeleccionado Resultado:(MostrarSeleccionPaisBlock)pResultadoFuncion
{
    [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo Paises.")];
    [[Locator Default].ServicioSOAP DevolverListaPais:(SRTipoListaPaisAObtener)Tipo Resultado:^(SRResultMethod pResult, NSArray *pListaPais)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible obtener la lista de Paises. Por favor revise su conexion a Internet.")];
                 break;
                 
             case SRResultMethodYES:
                 [[Locator Default].Alerta hideBussy];
                 [[Locator Default].Vistas MostrarPickerSemiModal:pView ListaElementos:pListaPais Seleccionado:[pListaPais ObtenerElementoPorID:PaisSeleccionado.Codigo] Titulo:Traducir(@"Paises") Result: ^(ElementoPicker *pResult)
                  {
                      if (pResultadoFuncion != nil)
                      {
                          pResultadoFuncion((Pais *)[pListaPais ObtenerElementoPorID:pResult.Codigo]);
                      }
                  }];
                 break;
         }
     }];
}

#pragma mark- Mostrar Pantalla Seleccion Provincias

- (void)MostrarSeleccionProvincia:(UIViewController *)pView Pais:(Pais *)pPais ProvinciaSeleccionada:(Provincia *)ProvinciaSeleccionada MostrarTodas:(BOOL)pMostrarTodas MostrarCercaDeMi:(BOOL)pMostrarCercaDeMi Resultado:(MostrarSeleccionProvinciaBlock)pResultadoFuncion
{
    [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo Provincias")];
    [[Locator Default].ServicioSOAP DevolverListaProvinciaPorPais:pPais.Codigo Result: ^(SRResultMethod pResult, NSArray *pListaProvincia)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible obtener la lista de Pronvicias. Revise su conexion a Internet.")];
                 break;
                 
             case SRResultMethodYES:
                 [[Locator Default].Alerta hideBussy];
                 NSMutableArray *lista = [NSMutableArray new];
                 
                 if (pMostrarCercaDeMi)
                 {
                     Provincia *prov = [[Provincia alloc] init];
                     prov.Codigo = @"-1";
                     prov.Nombre = [Provincia CercaDeMi].Nombre;
                     [lista addObject:prov];
                 }
                 
                 if (pMostrarTodas)
                 {
                     Provincia *prov = [[Provincia alloc] init];
                     prov.Codigo = @"-2";
                     prov.Nombre = [Provincia Todas].Nombre;
                     [lista addObject:prov];
                 }
                 
                 for (id prov in pListaProvincia)
                 {
                     [lista addObject:prov];
                 }
                 
                 [[Locator Default].Vistas MostrarPickerSemiModal:pView ListaElementos:lista Seleccionado:[lista ObtenerElementoPorID:ProvinciaSeleccionada.Codigo] Titulo:Traducir(@"Provincias") Result: ^(ElementoPicker *pResult)
                  {
                      if (pResultadoFuncion != nil)
                      {
                          Provincia *prov = (Provincia *)[lista ObtenerElementoPorID:pResult.Codigo];
                          
                          if ([prov.Codigo isEqualToString:@"-1"])
                          {
                              prov = [Provincia CercaDeMi];
                          }
                          
                          if ([prov.Codigo isEqualToString:@"-2"])
                          {
                              prov = [Provincia Todas];
                          }
                          
                          pResultadoFuncion(prov);
                      }
                  }];
                 break;
         }
     }];
}

#pragma mark- Mostrar Pantalla Seleccion Categoria Producto

- (void)MostrarSeleccionCategoriaProducto:(UIViewController *)pView Resultado:(MostrarSeleccionCategoriaProductoBlock)pResultadoFuncion
{
    [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo categorías.")];
    [[Locator Default].ServicioSOAP DevolverListaCategoriasProducto: ^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *pListaCategoriasProducto)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 
                 if (MensajeSAP == nil || [MensajeSAP isEqualToString:@""])
                 {
                     MensajeSAP = Traducir(@"No ha sido posible obtener la lista de categorías. Por favor revise su conexion a Internet.");
                 }
                 
                 [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                 break;
                 
             case SRResultMethodYES:
                 [[Locator Default].Alerta hideBussy];
                 [[Locator Default].Vistas MostrarPickerSemiModal:pView ListaElementos:pListaCategoriasProducto Seleccionado:nil Titulo:Traducir(@"Categorías") Result: ^(ElementoPicker *pResult)
                  {
                      if (pResultadoFuncion != nil)
                      {
                          pResultadoFuncion((GrupoArticulo *)pResult);
                      }
                  }];
                 break;
         }
     }];
}

#pragma mark- Mostrar Pantalla Seleccion Empresa Busqueda

- (void)MostrarSeleccionEmpresaBusqueda:(UIViewController *)pView Resultado:(MostrarSeleccionEmpresaBusquedaBlock)pResultadoFuncion
{
    [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo marcas.")];
    [[Locator Default].ServicioSOAP DevolverListaEmpresaBusqueda: ^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *pListaEmpresaBusqueda)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 
                 if (MensajeSAP == nil || [MensajeSAP isEqualToString:@""])
                 {
                     MensajeSAP = Traducir(@"No ha sido posible obtener la lista de marcas. Por favor revise su conexion a Internet.");
                 }
                 
                 [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                 break;
                 
             case SRResultMethodYES:
                 [[Locator Default].Alerta hideBussy];
                 [[Locator Default].Vistas MostrarPickerSemiModal:pView ListaElementos:pListaEmpresaBusqueda Seleccionado:nil Titulo:Traducir(@"Marcas") Result: ^(ElementoPicker *pResult)
                  {
                      if (pResultadoFuncion != nil)
                      {
                          pResultadoFuncion((Empresa *)pResult);
                      }
                  }];
                 break;
         }
     }];
}

#pragma mark- Mostrar Pantalla Seleccion Moneda
- (void)MostrarSeleccionMoneda:(UIViewController *)pView Seleccionada:(Moneda *)MonedaSeleccionada Resultado:(MostrarSeleccionMonedaBlock)pResultadoFuncion
{
    [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo monedas.")];
    [[Locator Default].ServicioSOAP ObtenerListaMonedas:^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *ListaMonedas)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 
                 if (MensajeSAP == nil || [MensajeSAP isEqualToString:@""])
                 {
                     MensajeSAP = Traducir(@"No ha sido posible obtener la lista de monedas. Por favor revise su conexion a Internet.");
                 }
                 
                 [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                 break;
                 
             case SRResultMethodYES:
                 [[Locator Default].Alerta hideBussy];
                 [[Locator Default].Vistas MostrarPickerSemiModal:pView ListaElementos:ListaMonedas Seleccionado:MonedaSeleccionada Titulo:Traducir(@"Monedas") Result: ^(ElementoPicker *pResult)
                  {
                      if (pResultadoFuncion != nil)
                      {
                          pResultadoFuncion((Moneda *)pResult);
                      }
                  }];
                 break;
         }
     }];
}

- (void)MostrarDestinatariosNotificacionEnviada:(UIViewController *)pView ListaDestinatarios:(NSArray *)pLista
{
    ListaDestinatariosViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"ListaDestinatariosViewController"];
    
    lView.ListaDestinatarios = pLista;
    
    [pView.navigationController pushViewController:lView animated:true];
}

- (void)MostrarEdicionGrupoNotificacion:(UIViewController *)pView Grupo:(GrupoNotificacion *)pGrupo
{
    EditarGrupoNotificacionViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"EditarGrupoNotificacionViewController"];
    
    lView.Grupo = pGrupo;
    
    [pView.navigationController pushViewController:lView animated:true];
}

- (void)MostrarSeleccionContactosTelefono:(UIViewController *)pView ListaMails:(NSArray *)pListaMails MostrarGrupos:(BOOL)pMostrarGrupos MostrarOtros:(BOOL)pMostrarOtros Result:(MostrarSeleccionContactosTelefonoBlock)pResult
{
    SeleccionContactosViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"SeleccionContactosViewController"];
    
    lView.ListaEmailsSeleccionados = pListaMails;
    lView.MostrarContactosOtros = pMostrarOtros;
    lView.MostrarGrupos = pMostrarGrupos;
    
    lView.Respond = ^(NSArray *pListaSeleccionados)
    {
        if (pResult != nil)
        {
            pResult(pListaSeleccionados);
        }
    };
    
    [self PresentModal:pView actual:lView];
}

- (void)MostrarEnvioNotificacion:(UIViewController *)pView Producto:(ProductoPadre *)pProducto
{
    EnviarNotificacionViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"EnviarNotificacionViewController"];
    
    lView.Producto = pProducto;
    
    [self PresentModal:pView actual:lView];
}

- (void)MostrarFavoritos:(YaesmioViewController *)pView
{
    UIViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"FavoritosViewController"];
    
    [self PresentSpecialModal:pView actual:lView];
}

- (void)MostrarMisDatos:(YaesmioViewController *)pView
{
    UIViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"MisDatosViewController"];
    
    [self PresentSpecialModal:pView actual:lView];
}

- (void)MostrarMiHistoricoDeCompras:(YaesmioViewController *)pView
{
    UIViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"HistoricoComprasViewController"];
    
    [self PresentSpecialModal:pView actual:lView];
}

- (void)MostrarMisAjustes:(YaesmioViewController *)pView
{
    UIViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"AjustesViewController"];
    
    [self PresentSpecialModal:pView actual:lView];
}

- (void)MostrarInformacion:(YaesmioViewController *)pView
{
    UIViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"InformacionViewController"];
    
    [self PresentSpecialModal:pView actual:lView];
}

- (void)MostrarNotificaciones:(UIViewController *)pView
{
    UIViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"NotificacionesViewController"];
    
    [self PresentSpecialModal:pView actual:lView];
}

- (void)MostrarBusquedaProductos:(YaesmioViewController *)pView Filtro:(FiltroBusquedaProductos *)filtro
{
    BusquedaViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"BusquedaViewController"];
    
    lView.Filtro = filtro;
    
    [self PresentSpecialModal:pView actual:lView];
}

- (void)MostrarCapturaQR:(YaesmioViewController *)pView
{
    QRCapturaViewController *lView = [pView.storyboard instantiateViewControllerWithIdentifier:@"QRCapturaViewController"];
    
    [self PresentSpecialModal:pView actual:lView];
}

- (void)ObtenerConfiguracionApp:(ResultadoPorDefectoBlock)Resultado
{
    [[Locator Default].ServicioSOAP ObtenerConfiguracionApp:^(SRResultMethod pResult, NSString *MensajeSAP, NSString *urlImagenFondoApp, NSString *urlSonido, NSArray *ListaParametros)
     {
         if (pResult == SRResultMethodYES)
         {
             [Locator Default].Sounds.UrlNotificacion = urlSonido;
             
             [Locator Default].Imagen.UrlImagenFondoPantallaPrincipal = urlImagenFondoApp;
             
             [Locator Default].Preferencias.urlPasarelaPagoWeb = [self ObtenerValorCampo:@"PASARELA" DeListaParametros:ListaParametros oValor:[Locator Default].Preferencias.urlPasarelaPagoWeb];
             
             [Locator Default].Preferencias.IdPaypal = [self ObtenerValorCampo:@"ID_PAYPAL" DeListaParametros:ListaParametros oValor:[Locator Default].Preferencias.IdPaypal];
         }
         
         if (Resultado != nil)
         {
             Resultado(pResult == SRResultMethodYES);
         }
     }];
}

- (NSString *)ObtenerValorCampo:(NSString *)pKey DeListaParametros:(NSArray *)Lista oValor:(NSString *)ValorSiNull
{
    NSString *resultado = ValorSiNull;
    
    for (ConfigurationParameter *parametro in Lista)
    {
        if (parametro != nil && [parametro.Codigo isEqualToString:pKey])
        {
            resultado = parametro.Nombre;
            break; // salida directa
        }
    }
    
    return resultado;
}

@end
