//
//  OpinionProductoCell.h
//  Yaesmio
//
//  Created by freelance on 07/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OpinionProductoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *labelFecha;
@property (weak, nonatomic) IBOutlet UILabel *labelNombreUsuario;
@property (weak, nonatomic) IBOutlet UILabel *labelComentario;
@property (weak, nonatomic) IBOutlet UILabel *labelValoracion;

@end
