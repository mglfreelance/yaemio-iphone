//
//  DeviceManager.m
//  Yaesmio
//
//  Created by freelance on 14/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "DeviceManager.h"
#include <sys/sysctl.h>
#include <net/if.h>
#include <arpa/inet.h>
#include <mach/mach.h>
#import <sys/socket.h>
#import <net/if_dl.h>
#import <ifaddrs.h>
#import <sys/xattr.h>
#define IFT_ETHER 0x6

@implementation DeviceManager

- (NSString *)IdDevice
{
  return [UIDevice currentDevice].identifierForVendor.UUIDString;
}

- (NSString *)IPAddress
{
    NSString *myAddress = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    NSString *myAddressEN = @"";
    NSString *myAddress3G = @"";
    int success = 0;
    
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    
    if (success == 0)
    {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        
        while (temp_addr != NULL)
        {
            if (temp_addr->ifa_addr->sa_family == AF_INET)
            {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if ([[NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)] isEqualToString:@"127.0.0.1"] == false)
                {
                    if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"])
                    {
                        // Get NSString from C String
                        myAddressEN = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    }
                    else
                    {
                        myAddress3G = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                    }
                    
#ifdef SHOW_DEBUG_SOAP
                    NSLog(@"%@->%@", [NSString stringWithUTF8String:temp_addr->ifa_name], [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)]);
#endif
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    
    if ([myAddressEN isEqualToString:@""] == false)
    {
        myAddress = myAddressEN;
    }
    else
    {
        myAddress = myAddress3G;
    }
    
    return myAddress;
}

- (NSString *)DeviceID16Long
{
    NSString *valor = [NSString stringWithFormat:@"000000000000000%@",[ self.IdDevice stringByReplacingOccurrencesOfString:@"-" withString:@""]];
    
    return [valor substringFromIndex:valor.length - 16];
}

- (NSString *)MemoryApp
{
    vm_statistics_data_t vmStats;
    mach_msg_type_number_t infoCount = HOST_VM_INFO_COUNT;
    kern_return_t kernReturn = host_statistics(mach_host_self(), HOST_VM_INFO, (host_info_t)&vmStats, &infoCount);
    
    if (kernReturn == KERN_SUCCESS)
    {
        return [NSString stringWithFormat:@"%0.1f MB", (vmStats.wire_count / 1024.0)/8];
    }
    else
    {
        return @"0";
    }
}

@end
