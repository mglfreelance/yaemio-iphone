//
//  NotificacionesRecibidasViewController.m
//  Yaesmio
//
//  Created by freelance on 08/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "NotificacionesViewController.h"
#import "NotificacionCell.h"
#import "CabeceraNotificacion.h"
#import "GrupoNotificacionCell.h"
#import "UIAllControls.h"

typedef NS_ENUM (NSInteger, TipoPantallaSeleccionado)
{
    TipoPantallaNotificacionesRecibidas = 0,
    TipoPantallaNotificacionesEnviadas  = 1,
    TipoPantallaGestionGrupos           = 2,
};

#define ValorMenuModificar	0
#define ValorMenuEliminar	1

@interface NotificacionesViewController () <UITableViewDelegate, UITableViewDataSource, M13ContextMenuDelegate,RTLabelDelegate>

@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCampos;
@property (weak, nonatomic) IBOutlet UITableView *tableNotificaciones;
@property (weak, nonatomic) IBOutlet UIView *VistaSinDatos;
@property (weak, nonatomic) IBOutlet RTLabel *labelAgregarGrupo;
@property (weak, nonatomic) IBOutlet UIImageView *ImagenFondoSinDatos;
@property (weak, nonatomic) IBOutlet UIView *ViewLoading;
@property (weak, nonatomic) IBOutlet UISegmentedControl *SegmentedNotificaciones;

- (IBAction)CambioTipoNotificacionExecute:(id)sender;
- (IBAction)CerrarModalExecute:(id)sender;

@property (strong) NSMutableArray *ListaNotificacionesRecibidas;
@property (assign) int TotalNotificacionesRecibidas;
@property (strong) NSMutableArray *ListaNotificacionesEnviadas;
@property (assign) int TotalNotificacionesEnviadas;
@property (strong) NSMutableArray *ListaGrupos;
@property (strong) M13ContextMenu *menu;
@property (strong) M13ContextMenuItemIOS7 *MenuVer;
@property (strong) M13ContextMenuItemIOS7 *MenuBorrar;


@property (readonly) TipoPantallaSeleccionado TipoActual;
@property (readonly) NSArray *ListaActual;

@end

@implementation NotificacionesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaCampos);
    self.ListaNotificacionesRecibidas = [NSMutableArray new];
    self.TotalNotificacionesRecibidas = 0;
    self.ListaNotificacionesEnviadas = [NSMutableArray new];
    self.TotalNotificacionesEnviadas = 0;
    self.ListaGrupos = [NSMutableArray new];
    self.labelAgregarGrupo.hidden = true;
    [self.tableNotificaciones mgExtendTopLeftForX:UNCHANGED andY:46.0];
    [[Locator Default].Contactos ComprobarAccesoAddressBook];
    [self SetRTLabel:self.labelAgregarGrupo];
    
    [self configurarContextMenu];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.SegmentedNotificaciones.tintColor = [Locator Default].Color.Principal;
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
    
    [self ActualizarFondoSegunTipo];
    
    if (self.ListaActual.count == 0 || self.TipoActual == TipoPantallaGestionGrupos)
    { // para la primera vez y cuando vuelva y no tenga datos.
        [self.ListaGrupos removeAllObjects]; // para que siempre los pida
        [self PedirDatosSegunTipo];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIMenuController sharedMenuController] setMenuItems:nil];
}

- (void)viewUnload
{
    if (self.ListaNotificacionesRecibidas != nil)
    {
        [self.ListaNotificacionesRecibidas removeAllObjects];
    }
    
    [self setSegmentedNotificaciones:nil];
    [self setListaCampos:nil];
    [self setTableNotificaciones:nil];
    [self setVistaSinDatos:nil];
    [self setImagenFondoSinDatos:nil];
    [self setListaNotificacionesRecibidas:nil];
    [self setListaNotificacionesEnviadas:nil];
    [self setListaGrupos:nil];
    [self setMenu:nil];
    [self setMenuBorrar:nil];
    [self setMenuVer:nil];
    
    [super viewUnload];
}

- (IBAction)CambioTipoNotificacionExecute:(id)sender
{
    [self.tableNotificaciones reloadData];
    [self PedirDatosSegunTipo];
    self.labelAgregarGrupo.hidden = (self.TipoActual != TipoPantallaGestionGrupos);
    [self.tableNotificaciones mgExtendTopLeftForX:UNCHANGED andY:(self.TipoActual != TipoPantallaGestionGrupos) ? 46.0:84.0];
}

- (IBAction)CerrarModalExecute:(id)sender
{
    [self dismissModalViewControllerWithPushDirection:kCATransitionFromLeft];
}

- (NSArray *)ListaActual
{
    switch (self.TipoActual)
    {
        case TipoPantallaNotificacionesEnviadas :
            return self.ListaNotificacionesEnviadas;
            
        case TipoPantallaGestionGrupos:
            return self.ListaGrupos;
            
        default:                                 // recibidas
            return self.ListaNotificacionesRecibidas;
    }
}

- (TipoPantallaSeleccionado)TipoActual
{
    switch (self.SegmentedNotificaciones.selectedSegmentIndex)
    {
        case  1:
            return TipoPantallaNotificacionesEnviadas;
            
        case  2:
            return TipoPantallaGestionGrupos;
            
        default:// recibidas
            return TipoPantallaNotificacionesRecibidas;
    }
}

#pragma mark- Delegado UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ListaActual.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.TipoActual)
    {
        case TipoPantallaGestionGrupos:
            return 66;
            
        default://recibidas y enviadas
            return 135;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.TipoActual)
    {
        case TipoPantallaNotificacionesEnviadas:
            return [self getCeldaNotificacionesEnviadas:tableView notificacion:self.ListaActual[indexPath.row] indexPath:indexPath];
            
        case TipoPantallaGestionGrupos:
            return [self getCeldaGrupoNotificacion:tableView notificacion:self.ListaActual[indexPath.row]];
            
        default:                                                 // recibida
            return [self getCeldaNotificacionesRecibidas:tableView notificacion:self.ListaActual[indexPath.row] indexPath:indexPath];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.TipoActual)
    {
        case TipoPantallaNotificacionesEnviadas:
            [self MostrarPantallaProducto:indexPath];
            break;
            
        case TipoPantallaGestionGrupos:
            [[Locator Default].Vistas MostrarEdicionGrupoNotificacion:self Grupo:self.ListaActual[indexPath.row]];
            break;
            
        default:                                 //recibidas
            [self MostrarPantallaProducto:indexPath];
            break;
    }
}

#pragma  mark- Delegate ContextMenu

- (BOOL)shouldShowContextMenu:(M13ContextMenu *)contextMenu atPoint:(CGPoint)point
{
    self.MenuVer.HideMenu = (self.TipoActual != TipoPantallaNotificacionesEnviadas);
    
    return true;
}

- (void)contextMenu:(M13ContextMenu *)contextMenu atPoint:(CGPoint)point didSelectItemAtIndex:(NSInteger)index
{
    NSIndexPath *indexPath = [self.tableNotificaciones indexPathForRowAtPoint:point];
    
    if (indexPath != nil && indexPath.row < self.ListaActual.count)
    {
        id actual = self.ListaActual[indexPath.row];
        
        if (self.TipoActual != TipoPantallaGestionGrupos && [actual isKindOfClass:[CabeceraNotificacion class]])
        {
            switch (index)
            {
                case ValorMenuModificar:
                    [self VerDestinatarios:actual];
                    break;
                    
                case ValorMenuEliminar:
                    [self EliminarNotificacion:actual];
                    break;
            }
        }
        else if (self.TipoActual == TipoPantallaGestionGrupos && [actual isKindOfClass:[GrupoNotificacion class]])
        {
            if (index == ValorMenuEliminar)
            {
                [self EliminarGrupo:actual];
            }
        }
    }
}

#pragma mark- Delegate NotificacionRecibidaCell

- (void)EliminarNotificacion:(CabeceraNotificacion *)sender
{
    [[Locator Default].Alerta showMessage:Traducir(@"¡Atención!") Message:Traducir(@"¿Seguro que quieres eliminar este yasmy?") cancelButtonTitle:Traducir(@"Cancelar") AceptButtonTitle:Traducir(@"Aceptar") completionBlock: ^(NSUInteger buttonIndex)
     {
         if (buttonIndex == 1)                        // aceptar
         {
             switch (self.TipoActual)
             {
                 case TipoPantallaNotificacionesRecibidas:
                     [self MarcarNotificacionRecibidaComoEliminada:sender];
                     break;
                     
                 case TipoPantallaNotificacionesEnviadas:
                     [self MarcarNotificacionEnviadaComoEliminada:sender];
                     break;
                     
                 case TipoPantallaGestionGrupos:
                     break;
             }
         }
     }];
}

- (void)VerDestinatarios:(CabeceraNotificacion *)sender
{
    [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo destinatarios.")];
    [[Locator Default].ServicioSOAP ObtenerListaDestinatariosNotificacionEnviada:sender.idNotificacion Result: ^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *Lista)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                 break;
                 
             case SRResultMethodYES:
                 [[Locator Default].Alerta hideBussy];
                 [[Locator Default].Vistas MostrarDestinatariosNotificacionEnviada:self ListaDestinatarios:Lista];
                 break;
         }
     }];
}

#pragma mark- Notificaciones Recibidas

- (void)ObtenerSiguienteListaNotificacionesRecibidas
{
    if (self.TotalNotificacionesRecibidas == 0 || self.ListaNotificacionesRecibidas.count < self.TotalNotificacionesRecibidas)
    {
        self.ViewLoading.hidden = false;
        [[Locator Default].ServicioSOAP ObtenerListaNotificacionesRecibidas:[Locator Default].Preferencias.IdUsuario NumeroLineas:6 PosicionInicial:self.ListaNotificacionesRecibidas.count + 1.0 Visto:false Result: ^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *Lista, int NumeroTotalNotificaciones)
         {
             self.ViewLoading.hidden = true;
             switch (pResult)
             {
                 case SRResultMethodNO:
                     [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                     break;
                     
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
                     
                 case SRResultMethodYES:
                     self.TotalNotificacionesRecibidas = NumeroTotalNotificaciones;
                     [self.ListaNotificacionesRecibidas addObjectsFromArray:Lista];
                     [self.tableNotificaciones reloadData];
                     break;
             }
             [self ActualizarFondoSegunTipo];
         }];
    }
}

- (void)MarcarNotificacionRecibidaComoLeida:(CabeceraNotificacion *)pNotificacion indexPath:(NSIndexPath *)pIndex
{
    if (pNotificacion != nil && pNotificacion.Tipo == SNTipoNotificacionRecibida)
    {
        [[Locator Default].ServicioSOAP MarcarNotificacionRecibida:pNotificacion.idNotificacion Idusuario:[Locator Default].Preferencias.IdUsuario Tipo:MarcadoNotificacionRecibidaLeida Result: ^(SRResultMethod pResult, NSString *MensajeSAP)
         {
             if (pResult == SRResultMethodYES)
             {
                 pNotificacion.Tipo = SNTipoNotificacionLeida;
                 [self.tableNotificaciones reloadRowsAtIndexPaths:@[pIndex] withRowAnimation:UITableViewRowAnimationFade];
             }
         }];
    }
}

- (void)MarcarNotificacionRecibidaComoEliminada:(CabeceraNotificacion *)pNotificacion
{
    if (pNotificacion != nil)
    {
        NSInteger row = [self.ListaNotificacionesRecibidas indexOfObject:pNotificacion];
        NSIndexPath *index = [NSIndexPath indexPathForRow:row inSection:0];
        [[Locator Default].ServicioSOAP MarcarNotificacionRecibida:pNotificacion.idNotificacion Idusuario:[Locator Default].Preferencias.IdUsuario Tipo:MarcadoNotificacionRecibidaEliminada Result: ^(SRResultMethod pResult, NSString *MensajeSAP)
         {
             switch (pResult)
             {
                 case SRResultMethodYES:
                     [self.ListaNotificacionesRecibidas removeObject:pNotificacion];
                     [self MostrarAnimacionBorradoCeldaTableview:index];
                     break;
                     
                 case SRResultMethodNO:
                     [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                     break;
                     
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
             }
         }];
    }
}

- (NotificacionCell *)getCeldaNotificacionesRecibidas:(UITableView *)tableView notificacion:(CabeceraNotificacion *)pNotificacion indexPath:(NSIndexPath *)indexPath
{
    NotificacionCell *lVistaCelda = [tableView dequeueReusableCellWithIdentifier:@"NotificacionCell"];
    
    [lVistaCelda Configurar:pNotificacion Menu:self.menu];
    
    if (indexPath.row == self.ListaActual.count - 1)
    {
        [self ObtenerSiguienteListaNotificacionesRecibidas];
    }
    
    return lVistaCelda;
}

#pragma mark- Notificaciones Enviadas

- (void)ObtenerSiguienteListaNotificacionesEnviadas
{
    if (self.TotalNotificacionesEnviadas == 0 || self.ListaNotificacionesEnviadas.count < self.TotalNotificacionesEnviadas)
    {
        self.ViewLoading.hidden = false;
        [[Locator Default].ServicioSOAP ObtenerListaNotificacionesEnviadas:[Locator Default].Preferencias.IdUsuario NumeroLineas:6 PosicionInicial:self.ListaNotificacionesEnviadas.count + 1.0 Visto:false Result: ^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *Lista, int NumeroTotalNotificaciones)
         {
             self.ViewLoading.hidden = true;
             switch (pResult)
             {
                 case SRResultMethodNO:
                     [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                     break;
                     
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
                     
                 case SRResultMethodYES:
                     self.TotalNotificacionesEnviadas = NumeroTotalNotificaciones;
                     [self.ListaNotificacionesEnviadas addObjectsFromArray:Lista];
                     [self.tableNotificaciones reloadData];
                     break;
             }
             [self ActualizarFondoSegunTipo];
         }];
    }
}

- (void)MarcarNotificacionEnviadaComoEliminada:(CabeceraNotificacion *)pNotificacion
{
    if (pNotificacion != nil)
    {
        NSInteger row = [self.ListaNotificacionesEnviadas indexOfObject:pNotificacion];
        NSIndexPath *index = [NSIndexPath indexPathForRow:row inSection:0];
        [[Locator Default].ServicioSOAP MarcarNotificacionEnviadaEliminada:pNotificacion.idNotificacion Result: ^(SRResultMethod pResult, NSString *MensajeSAP)
         {
             switch (pResult)
             {
                 case SRResultMethodYES:
                     [self.ListaNotificacionesEnviadas removeObject:pNotificacion];
                     [self MostrarAnimacionBorradoCeldaTableview:index];
                     break;
                     
                 case SRResultMethodNO:
                     [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                     break;
                     
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
             }
         }];
    }
}

- (NotificacionCell *)getCeldaNotificacionesEnviadas:(UITableView *)tableView notificacion:(CabeceraNotificacion *)pNotificacion indexPath:(NSIndexPath *)indexPath
{
    NotificacionCell *lVistaCelda = [tableView dequeueReusableCellWithIdentifier:@"NotificacionCell"];
    
    [lVistaCelda Configurar:pNotificacion Menu:self.menu];
    
    if (indexPath.row == self.ListaActual.count - 1)
    {
        [self ObtenerSiguienteListaNotificacionesEnviadas];
    }
    
    return lVistaCelda;
}

#pragma mark- Grupos Notificaciones

- (void)ObtenerListaGruposNotificaciones
{
    if (self.ListaGrupos.count == 0)
    {
        self.ViewLoading.hidden = false;
        [[Locator Default].ServicioSOAP ObtenerListaGruposNotificacion: ^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *Lista)
         {
             self.ViewLoading.hidden = true;
             switch (pResult)
             {
                 case SRResultMethodNO:
                     [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                     break;
                     
                 case SRResultMethodErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
                     
                 case SRResultMethodYES:
                     [self.ListaGrupos addObjectsFromArray:Lista];
                     [self.tableNotificaciones reloadData];
                     break;
             }
             [self ActualizarFondoSegunTipo];
         }];
    }
}

- (GrupoNotificacionCell *)getCeldaGrupoNotificacion:(UITableView *)tableView notificacion:(GrupoNotificacion *)pGrupo
{
    GrupoNotificacionCell *lVistaCelda = [tableView dequeueReusableCellWithIdentifier:@"GrupoNotificacionCell"];
    
    [lVistaCelda Configurar:pGrupo Menu:self.menu];
    
    return lVistaCelda;
}

- (void)EliminarGrupo:(GrupoNotificacion *)pGrupo
{
    if (pGrupo != nil)
    {
        [[Locator Default].Alerta showMessage:Traducir(@"Esta seguro que quiere eliminar el grupo seleccionado?") cancelButtonTitle:Traducir(@"Cancelar") AceptButtonTitle:Traducir(@"Aceptar") completionBlock: ^(NSUInteger buttonIndex)
         {
             if (buttonIndex == 1)                    //pulsado aceptar para eliminar grupo
             {
                 NSInteger row = [self.ListaGrupos indexOfObject:pGrupo];
                 NSIndexPath *index = [NSIndexPath indexPathForRow:row inSection:0];
                 [[Locator Default].ServicioSOAP EliminarGrupoNotificacion:pGrupo.IdGrupo Result: ^(SRResultMethod pResult, NSString *MensajeSAP)
                  {
                      switch (pResult)
                      {
                          case SRResultMethodYES:
                              [self.ListaGrupos removeObject:pGrupo];
                              [self MostrarAnimacionBorradoCeldaTableview:index];
                              break;
                              
                          case SRResultMethodNO:
                              [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                              break;
                              
                          case SRResultMethodErrConex:
                              [[Locator Default].Alerta showErrorConexSOAP];
                              break;
                      }
                  }];
             }
         }];
    }
}

#pragma mark- Delegate RTLabel

- (void)rtLabel:(id)rtLabel didSelectLinkWithURL:(NSURL *)url
{
    [self.view endEditing:true];
    
    if ([url.description isEqualToString:@"AgregaGrupo"])
    {
        GrupoNotificacion *grupo = [GrupoNotificacion new];
        
        grupo.IdUsuarioCreador = [Locator Default].Preferencias.IdUsuario;
        [[Locator Default].Vistas MostrarEdicionGrupoNotificacion:self Grupo:grupo];
    }
}

#pragma mark- Privado

- (UIImage *)obtenerImagenFondoSegunTipo
{
    switch (self.TipoActual)
    {
        case TipoPantallaNotificacionesEnviadas:
            return [UIImage imageNamed:@"fondo_notificaciones_enviadas"];
            
        case TipoPantallaGestionGrupos:
            return [UIImage imageNamed:@"fondo_grupos_notificaciones"];
            
        default:
            return [UIImage imageNamed:@"fondo_notificaciones_recibidas"];
    }
}

- (void)MostrarPantallaProducto:(NSIndexPath *)indexPath
{
    CabeceraNotificacion *pBuscado = self.ListaActual[indexPath.row];
    
    if (pBuscado != nil)
    {
        [[Locator Default].Alerta showBussy:Traducir(@"Cargando Producto.")];
        [[Locator Default].ServicioSOAP ObtenerProducto:pBuscado.IdProducto Medio:pBuscado.Medio Canal:TipoCanalProductoNotificacion Resultado: ^(SRResultObtenerProducto pResult, NSString *pMensajeError, ProductoPadre *pProductoPadre)
         {
             switch (pResult)
             {
                 case SRResultObtenerProductoNoYaesmio:
                 case SRResultObtenerProductoErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
                     
                 case SRResultObtenerProductoNoExiste:
                     [[Locator Default].Alerta showMessageAcept:pMensajeError];
                     break;
                     
                 case SRResultObtenerProductoYES:
                     [[Locator Default].Alerta hideBussy];
                     [self MarcarNotificacionRecibidaComoLeida:pBuscado indexPath:indexPath];
                     [[Locator Default].Vistas MostrarProducto:self Producto:pProductoPadre VieneDeFavoritos:false];
                     break;
             }
         }];
    }
}

- (void)ActualizarFondoSegunTipo
{
    self.ImagenFondoSinDatos.image = [self obtenerImagenFondoSegunTipo];
    self.tableNotificaciones.hidden = (self.ListaActual.count == 0);
    self.VistaSinDatos.hidden = (self.ListaActual.count != 0);
}

- (void)PedirDatosSegunTipo
{
    [self ActualizarFondoSegunTipo];
    
    if (self.ListaActual.count == 0)
    {
        switch (self.TipoActual)
        {
            case TipoPantallaNotificacionesRecibidas:
                [self ObtenerSiguienteListaNotificacionesRecibidas];
                break;
                
            case TipoPantallaNotificacionesEnviadas:
                [self ObtenerSiguienteListaNotificacionesEnviadas];
                break;
                
            case TipoPantallaGestionGrupos:
                [self ObtenerListaGruposNotificaciones];
                break;
        }
    }
}

- (void)MostrarAnimacionBorradoCeldaTableview:(NSIndexPath *)index
{
    [self.tableNotificaciones beginUpdates];
    [self.tableNotificaciones deleteRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationLeft];
    [self.tableNotificaciones endUpdates];
}

- (void)configurarContextMenu
{
    //Create the items
    self.MenuVer = [[M13ContextMenuItemIOS7 alloc] initWithUnselectedIcon:[UIImage imageNamed:@"MenuVer"] selectedIcon:[UIImage imageNamed:@"MenuVerSeleccionado"]];
    self.MenuBorrar = [[M13ContextMenuItemIOS7 alloc] initWithUnselectedIcon:[UIImage imageNamed:@"MenuPapelera"] selectedIcon:[UIImage imageNamed:@"MenuPapeleraSeleccionada"]];
    
    self.MenuVer.tintColor = [Locator Default].Color.Principal;
    self.MenuBorrar.tintColor = [Locator Default].Color.Principal;
    
    //Create the menu
    self.menu = [[M13ContextMenu alloc] initWithMenuItems:@[self.MenuVer, self.MenuBorrar]];
    self.menu.delegate = self;
    self.menu.tintColor = [Locator Default].Color.Principal;
    self.menu.BorderColor = [Locator Default].Color.Principal;
    self.menu.originationCircleStrokeColor = [Locator Default].Color.GrisOscuro;
}

- (void)SetRTLabel:(RTLabel *)myLabel
{
    myLabel.text =  [NSString stringWithFormat:Traducir(@"%@"), [[Locator Default].FuenteLetra getStringHtmlLink:Traducir(@"Crear grupo") Url:@"AgregaGrupo" FontSize:16]];
    myLabel.textColor = [Locator Default].Color.PrincipalOscuro;
    myLabel.font = [Locator Default].FuenteLetra.Defecto16;
    myLabel.lineBreakMode = RTTextLineBreakModeWordWrapping;
    myLabel.delegate = self;
    myLabel.textAlignment = RTTextAlignmentRight;
}

@end
