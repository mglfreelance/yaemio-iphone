//
//  ProductoEstadisticasViewController.m
//  Yaesmio
//
//  Created by freelance on 05/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ProductoEstadisticasViewController.h"
#import "OpinionProductoCell.h"
#import "EvaluacionProducto.h"

@interface ProductoEstadisticasViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic)IBOutletCollection(UILabel) NSArray * ListaCampos;
@property (weak, nonatomic) IBOutlet UIView *VistaValoracionEmpresa;
@property (weak, nonatomic) IBOutlet EDStarRating *ratingEmpresa;
@property (weak, nonatomic) IBOutlet UILabel *labelValoracionEmrpesa;
@property (weak, nonatomic) IBOutlet UILabel *labelValoracionProducto;
@property (weak, nonatomic) IBOutlet UILabel *labelValoracionTextoProducto;
@property (weak, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (weak, nonatomic) IBOutlet UILabel *labelVecesVisualizadoProducto;
@property (weak, nonatomic) IBOutlet UITableView *TableViewOpiniones;
@property (weak, nonatomic) IBOutlet UILabel *labelVecesCompradoProducto;
@property (weak, nonatomic) IBOutlet UILabel *labelFavoritosProducto;
@property (weak, nonatomic) IBOutlet UILabel *labelSinOpiniones;
@property (weak, nonatomic) IBOutlet UILabel *labelSinValorar;
@property (weak, nonatomic) IBOutlet UIView *viewYaesmioRecomienda;
@property (weak, nonatomic) IBOutlet UILabel *labelVecesCompartido;

@end

@implementation ProductoEstadisticasViewController

- (void)viewUnload
{
    [self setListaCampos:nil];
    [self setVistaValoracionEmpresa:nil];
    [self setRatingEmpresa:nil];
    [self setLabelValoracionEmrpesa:nil];
    [self setLabelValoracionProducto:nil];
    [self setLabelValoracionTextoProducto:nil];
    [self setScrollView:nil];
    [self setLabelVecesVisualizadoProducto:nil];
    [self setTableViewOpiniones:nil];
    [self setLabelVecesCompradoProducto:nil];
    [self setLabelFavoritosProducto:nil];
    [self setLabelSinOpiniones:nil];
    [self setLabelSinValorar:nil];
    [super viewUnload];
}

- (void)RellenarPantallaConDatosProducto
{
    Traducir(self.ListaCampos);
    
    self.VistaValoracionEmpresa.hidden = (self.Producto.Estadisticas.NotaMediaEmpresa.isZeroOrLess);
    self.labelValoracionEmrpesa.text = [NSString stringWithFormat:Traducir(@"%@ valoraciones"), [self.Producto.Estadisticas.NumeroValoracionesEmpresa toStringFormaterWithoutDecimals]];
    self.ratingEmpresa.backgroundColor  = [Locator Default].Color.Transparente;
    self.ratingEmpresa.starImage = [Locator Default].Imagen.Estrella;
    self.ratingEmpresa.starHighlightedImage = [Locator Default].Imagen.EstrellaSeleccionada;
    
    //Rating de empresa
    self.ratingEmpresa.maxRating = 5.0;
    self.ratingEmpresa.horizontalMargin = 15.0;
    self.ratingEmpresa.editable = NO;
    [self.ratingEmpresa setNeedsDisplay];
    self.ratingEmpresa.displayMode = EDStarRatingDisplayHalf;
    if ([self.ratingEmpresa respondsToSelector:@selector(setTintColor:)])
    {
        self.ratingEmpresa.tintColor = [Locator Default].Color.Blanco;
    }
    self.ratingEmpresa.rating = [self.Producto.Estadisticas.NotaMediaEmpresa floatValue]/ 2;
    
    //Valoracion Producto
    self.labelValoracionTextoProducto.text = [NSString stringWithFormat:Traducir(@"Valorado por %@ usuarios."), [self.Producto.Estadisticas.NumeroValoracionesProducto toStringFormaterWithoutDecimals]];
    self.labelVecesVisualizadoProducto.text = [NSString stringWithFormat:Traducir(@"Visualizado %@ veces."), [self.Producto.Estadisticas.NumeroVecesVisto toStringFormaterWithoutDecimals]];
    self.labelVecesCompradoProducto.text = [NSString stringWithFormat:Traducir(@"Comprado %@ veces."), [self.Producto.Estadisticas.NumeroVecesComprado toStringFormaterWithoutDecimals]];
    self.labelVecesCompartido.text = [NSString stringWithFormat:Traducir(@"Compartido %@ veces."), [self.Producto.Estadisticas.NumeroVecesCompartido toStringFormaterWithoutDecimals]];
    self.labelFavoritosProducto.text = [NSString stringWithFormat:Traducir(@"Favorito de %@ usuarios."), [self.Producto.Estadisticas.NumeroVecesEnFavoritos toStringFormaterWithoutDecimals]];
    self.labelValoracionProducto.text =  [self.Producto.Estadisticas.NotaMediaProducto toStringFormaterOnceDecimal];
    self.labelValoracionProducto.hidden = (self.Producto.Estadisticas.NotaMediaProducto.isZeroOrLess);
    self.labelSinValorar.hidden = (self.Producto.Estadisticas.NotaMediaProducto.isGreaterZero);

    //Opiniones
    self.labelSinOpiniones.hidden = (self.Producto.Estadisticas.Evaluaciones.count != 0|| self.Producto.ProductoHijo.Recomendada);
    self.TableViewOpiniones.hidden = (self.Producto.Estadisticas.Evaluaciones.count == 0);
    self.viewYaesmioRecomienda.hidden = (!self.Producto.ProductoHijo.Recomendada);
    
    if (self.Producto.Estadisticas.Evaluaciones.count > 0)
    {
        [self.TableViewOpiniones mgSetSizeX:UNCHANGED andY:UNCHANGED andWidth:UNCHANGED andHeight:108 * self.Producto.Estadisticas.Evaluaciones.count ];
        [self.TableViewOpiniones reloadData];
    }
    [self.TableViewOpiniones mgSizeFitsMaxWidth:UNCHANGED MaxHeight:MAXFLOAT];
    
    [self.ScrollView mgRelocateContent];     // recoloco todas las subvistas
    [self.ScrollView setHeightContentSizeForSubviewUntilControl:self.TableViewOpiniones];
}

- (EvaluacionProducto *)getOpinion:(int)pNum
{
    if (self.Producto.Estadisticas.Evaluaciones.count >= pNum)
    {
        return self.Producto.Estadisticas.Evaluaciones[pNum];
    }
    
    return nil;
}

#pragma mark- UITABLEVIEW DATASOURCE

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.Producto.Estadisticas.Evaluaciones.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    OpinionProductoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OpinionProductoCell"];
    EvaluacionProducto *eval = [self getOpinion:(int)indexPath.row];
    
    cell.labelFecha.text = [eval.Fecha format:Traducir(@"dd/mm/yyyy HH:mm:ss")];
    cell.labelComentario.text = eval.Comentario;
    cell.labelNombreUsuario.text = eval.NombreUsuario;
    cell.labelValoracion.text = [NSString stringWithFormat:Traducir(@"Valoración %@"), [eval.Nota toStringFormaterWithoutDecimals]];
    
    return cell;
}

@end
