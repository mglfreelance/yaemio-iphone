//
//  MembershipDatosUsuario.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "MembershipDatosUsuario.h"

@implementation MembershipDatosUsuario
- (id)init
{
    if ((self = [super init]))
    {
        self.Apellidos              = @"";
        self.Fecha_nacimiento       = @"";
        self.Idioma                 = @"";
        self.Login                  = @"";
        self.Nombre                 = @"";
        self.Password               = @"";
        self.PreguntaPassword       = @"";
        self.RespuestaPassword      = @"";
        self.Sexo                   = @"";
        self.aceptaEnvioPublicidad  = FALSE;
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [[SOAP_Boolean serialize:self.aceptaEnvioPublicidad] xmlNodeForDoc:node->doc elementName:@"AceptarPubli" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Apellidos xmlNodeForDoc:node->doc elementName:@"Apellidos" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Fecha_nacimiento xmlNodeForDoc:node->doc elementName:@"Fecha_nacimiento" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Idioma xmlNodeForDoc:node->doc elementName:@"Idioma" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Login xmlNodeForDoc:node->doc elementName:@"Login" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Nombre xmlNodeForDoc:node->doc elementName:@"Nombre" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Password xmlNodeForDoc:node->doc elementName:@"Password" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.PreguntaPassword xmlNodeForDoc:node->doc elementName:@"PreguntaPassword" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.RespuestaPassword xmlNodeForDoc:node->doc elementName:@"RespuestaPassword" elementNSPrefix:@"tns3"]);
    
    xmlAddChild(node, [self.Sexo xmlNodeForDoc:node->doc elementName:@"Sexo" elementNSPrefix:@"tns3"]);
}

+ (MembershipDatosUsuario *)deserializeNode:(xmlNodePtr)cur
{
    MembershipDatosUsuario *newObject = [MembershipDatosUsuario new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"AceptarPubli"])
    {
        self.aceptaEnvioPublicidad = [SOAP_Boolean deserializeNode:pobjCur].boolValue;
    }
    else if ([nodeName isEqualToString:@"Apellidos"])
    {
        self.Apellidos = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Fecha_nacimiento"])
    {
        self.Fecha_nacimiento = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"UsuarioId"])
    {
        self.Login = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Idioma"])
    {
        self.Idioma = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Nombre"])
    {
        self.Nombre = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Password"])
    {
        self.Password = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"PreguntaPassword"])
    {
        self.PreguntaPassword = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"RespuestaPassword"])
    {
        self.RespuestaPassword = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Sexo"])
    {
        self.Sexo = [NSString deserializeNode:pobjCur];
    }
}

@end
