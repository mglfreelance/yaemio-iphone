//
//  ParametrosDevolverListaEmpresa.h
//  Yaesmio
//
//  Created by freelance on 22/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"

@interface ParametrosDevolverListaEmpresa : SoapObject

+ (ParametrosDevolverListaEmpresa *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString *Modo;

@end
