//
//  YaesmioViewController.h
//  Yaesmio
//
//  Created by freelance on 08/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+Ampliado.h"

@interface YaesmioViewController : UIViewController

-(void) viewUnload;
-(void) CerrarModal;

@end

@interface YaesmioTableViewController : UITableViewController

-(void) viewUnload;

@end

@interface YaesmioPageViewController : UIPageViewController

-(void) viewUnload;

@end

@interface UIViewController (ModalPresenter)

-(void) CerrarModal;

@end