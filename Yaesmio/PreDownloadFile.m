//
//  PreDownloadFile.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 24/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "PreDownloadFile.h"
#import "UIImage+Ampliado.h"

@interface PreDownloadFile () <NSURLConnectionDelegate>

@property (copy) ResultDownloadBlock    ResulBlock;

@end

@implementation PreDownloadFile

-(PreDownloadFile*) initWithURL:(NSURL*) pUrl Type:(DownloadManagerTypeFile)pType FilePath:(NSString*)pFilePath Grupo:(NSString*)pGrupo FileBlock:(getAssyncUrlFileWithUrlBlock)pFileBlock DataBlock:(getAssyncDataWithUrlBlock) pDataBlock
{
    self = [self init];
    
    if (self)
    {
        self.Url                = pUrl;
        self.FileType           = pType;
        self.DataBlockResult    = pDataBlock;
        self.FileBlockResult    = pFileBlock;
        self.FilePath           = pFilePath;
        self.Grupo              = pGrupo;
    }
    
    return self;
}

-(id)init
{
    self = [super init];
    
    if (self)
    {
        self.DataBlockResult    = nil;
        self.FileBlockResult    = nil;
        self.Request            = nil;
        self.Connection         = nil;
        self.Data               = nil;
        self.FilePath           = nil;
        self.FileType           = DownloadManagerTypeFileOther;
        self.Url                = nil;
        self.ResulBlock         = nil;
        self.DownloadCorrect    = false;
    }
    
    return self;
}

-(void) InitDownloadWithResult:(ResultDownloadBlock)pResult
{
    self.ResulBlock = pResult;
    
    // Create request
    self.Request = [[NSURLRequest alloc] initWithURL:self.Url cachePolicy:NSURLRequestReturnCacheDataElseLoad  timeoutInterval:15.0f];
    
    // Begin download
    self.Data = nil;
    self.Connection = [[NSURLConnection alloc] initWithRequest:self.Request delegate:self startImmediately:YES];
}

-(NSString*)ID
{
    return self.Url.absoluteString;
}

#pragma mark-Connection Delegate

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if (error.code == -1001)
    {
        [self InitDownloadWithResult:self.ResulBlock];
    }
    else
    {
        self.DownloadCorrect = false;
        if (self.ResulBlock != nil)
        {
            self.ResulBlock(self);
        }
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
	// Create bucket if it doesn't exist.
	if (!self.Data)
    {
		self.Data = [[NSMutableData alloc] init];
	}
	
	// Add to the bucket.
	[self.Data appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
	// Create image from data
    if ([[NSFileManager defaultManager] fileExistsAtPath:self.FilePath] == NO)
    {
        /* file doesn't exist, so create it */
        [[NSFileManager defaultManager] createFileAtPath:self.FilePath contents:self.Data attributes:nil];
    }
    
    self.DownloadCorrect = true;
    if (self.ResulBlock != nil)
    {
        self.ResulBlock(self);
    }
}


@end
