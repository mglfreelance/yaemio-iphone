//
//  TituloInformacionCell.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "TituloInformacionCell.h"
#import "UIAllControls.h"
#import "TituloInformacion.h"

#define texto_negrita @"<b>%@</b>"
#define texto_celda @"<p line-height=0.6 text-align=justify text-justify=inter-word;><font face='Roboto' size=14 color=#434343>%@</font></p>"
#define palabra_yaesmio @"Yaesmio!"


@interface TituloInformacionCell ()

@property (nonatomic) IBOutlet RTLabel *labelTitulo;
@property (nonatomic) IBOutlet UILabel *labelNumero;

@property (strong) TituloInformacion *TituloInformacion;

@end

@implementation TituloInformacionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        [self inicializar];
    }
    return self;
}

-(void)inicializar
{
    self.labelNumero.text  = @"";
    self.labelTitulo.text  = @"";
}

-(void)ConfigurarConPosicion:(int)pPosicion Elemento:(TituloInformacion*)pTitulo
{
    if (self.TituloInformacion == nil || self.TituloInformacion != pTitulo)
    {
        [self inicializar];
        self.TituloInformacion = pTitulo;
        
        self.labelNumero.text = [NSString stringWithFormat:@"%d.",pPosicion];
        self.labelTitulo.text = [self HacerNegritaYaesmio:pTitulo.Titulo];
    }
}

-(NSString*)HacerNegritaYaesmio:(NSString*)pTexto
{
    NSString *result = [pTexto stringByReplacingOccurrencesOfString:palabra_yaesmio withString:[NSString stringWithFormat:texto_negrita,palabra_yaesmio]];
    
    result = [NSString stringWithFormat:texto_celda, result];
    
    return result;
}


@end
