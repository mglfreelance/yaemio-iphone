//
//  CompraViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "CompraViewController.h"
#import "UIAllControls.h"
#import "ProductoCompraCell.h"
#import "TarjetaUsuario.h"
#import "PasarelaPagoWebViewController.h"
#import "SeleccionTarjetaUsuarioViewController.h"

@interface CompraViewController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *labelCabeceraCompra;
@property (weak, nonatomic) IBOutlet UILabel *labelPrecioTotal;
@property (weak, nonatomic) IBOutlet UITableView *controlTableView;
@property (weak, nonatomic) IBOutlet UILabel *labelNumeroTarjeta;
@property (weak, nonatomic) IBOutlet UIImageView *imageTipoTarjeta;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *BusyFormaPago;
@property (weak, nonatomic) IBOutlet UILabel *labelModoTarjeta;
@property (weak, nonatomic) IBOutlet UIButton *buttonCambiarFormaPago;
@property (strong, nonatomic)IBOutletCollection(id) NSArray * ListaCamposTraducir;

- (IBAction)btnComprar_click:(id)sender;

@property (strong) Usuario *Usuario;
@property (strong) EmpresaCarrito *Empresa;
@property (strong) Direccion *DireccionEnvio;
@property (copy) ResultadoPorDefectoBlock Resultado;
@property (strong) NSArray *ListaTarjetas;
@property (strong) TarjetaUsuario *TarjetaSeleccionada;

@end

@implementation CompraViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaCamposTraducir);
    
    [self CargarDatosPantalla];
    [self ObtenerListaTarjetas];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:true];
    
    if([Locator Default].Preferencias.urlPasarelaPagoWeb.isNotEmpty == false || [Locator Default].Preferencias.IdPaypal.isNotEmpty == false)
    { // cargar los datos para pagar
        [[Locator Default].Vistas ObtenerConfiguracionApp:nil];
    }
}

- (void)viewUnload
{
    [self setLabelCabeceraCompra:nil];
    [self setLabelPrecioTotal:nil];
    [self setListaCamposTraducir:nil];
    [self setControlTableView:nil];
    [self setUsuario:nil];
    [self setLabelNumeroTarjeta:nil];
    [self setImageTipoTarjeta:nil];
    [self setButtonCambiarFormaPago:nil];
    [self setUsuario:nil];
    [self setEmpresa:nil];
    [self setDireccionEnvio:nil];
    [self setResultado:nil];
    [self setListaTarjetas:nil];
    [self setTarjetaSeleccionada:nil];
    [self setLabelNumeroTarjeta:nil];
}

- (void)ConfigurarConCarrito:(EmpresaCarrito *)carrito Direccion:(Direccion *)direccion Usuario:(Usuario *)usuario Resultado:(ResultadoPorDefectoBlock)resultado
{
    self.Empresa = carrito;
    self.DireccionEnvio = direccion;
    self.Resultado = resultado;
    self.Usuario = usuario;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_pasarela_pago_web"])
    {
        PasarelaPagoWebViewController *vista = segue.destinationViewController;
        [[Locator Default].Alerta hideBussy];
        self.view.userInteractionEnabled = false;
        
        if (vista != nil)
        {
            [vista ConfigurarConCarrito:self.Empresa Referencia:self.TarjetaSeleccionada.Referencia Resultado: ^(NSString *pIdPago)
             {
                 [self FinalizarPago:pIdPago];
             }];
        }
    }
    else if ([segue.identifier isEqualToString:@"segue_formas_de_pago"])
    {
        SeleccionTarjetaUsuarioViewController *vista = segue.destinationViewController;
        
        if (vista != nil)
        {
            [vista ConfigurarConListaTarjetas:self.ListaTarjetas MostrarOtraTarjeta:true MostrarPaypal:true Resultado: ^(TarjetaUsuario *Tarjeta)
             {
                 if (Tarjeta != nil)
                 {
                     self.TarjetaSeleccionada = Tarjeta;
                     [self MostrarTarjetaSeleccionada];
                 }
             }];
        }
    }
}

#pragma mark- Eventos Privados

- (IBAction)btnComprar_click:(id)sender
{
    if (self.TarjetaSeleccionada.TipoTarjeta == TipoTarjetaUsuarioPaypal && [Locator Default].Preferencias.IdPaypal.isNotEmpty == false)
    {
        [[Locator Default].Alerta showMessageAcept:Traducir(@"No es posible la compra. No existe configuracion PayPal requerida.")];
    }
    else if([Locator Default].Preferencias.urlPasarelaPagoWeb.isNotEmpty == false)
    {
         [[Locator Default].Alerta showMessageAcept:Traducir(@"No es posible la compra. No existe configuracion de la pasarela de pago requerida.")];
    }
    else
    {
    [[Locator Default].Alerta showBussy:Traducir(@"Confirmando compra")];
    [[Locator Default].ServicioSOAP IniciarCompraConListaProductos:self.Empresa.Elementos Usuario:self.Usuario DireccionEnvio:self.DireccionEnvio Result: ^(SRResultMethod pResult, NSString *pMensaje, NSString *pIdCompra)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 
                 if (pMensaje.isNotEmpty == false)
                 {
                     pMensaje = Traducir(@"No ha sido posible hacer la compra.");
                 }
                 
                 [[Locator Default].Alerta showMessageAcept:pMensaje];
                 break;
                 
             case SRResultMethodYES:
                 self.Empresa.IDCarrito = pIdCompra;
                 
                 if (self.TarjetaSeleccionada.TipoTarjeta == TipoTarjetaUsuarioPaypal)
                 {
                     [self PagarConPaypal];
                 }
                 else
                 {
                     [self performSegueWithIdentifier:@"segue_pasarela_pago_web" sender:self];
                 }
                 
                 break;
         }
     }];
    }
}

#pragma mark- Delegado UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.Empresa.Elementos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProductoCompraCell *lVistaCelda = [tableView dequeueReusableCellWithIdentifier:@"celdaProductoCompra"];
    
    [lVistaCelda Configurar:self Elemento:[self obtenerProductoDeIndexPath:indexPath]];
    
    return lVistaCelda;
}

#pragma mark- Modulo PayPal

- (void)PagarConPaypal
{
    [[Locator Default].Alerta hideBussy];
    self.view.userInteractionEnabled = false;
    
    [[Locator Default].Vistas MostrarPantallaPayPal:self Carrito:self.Empresa Usuario:self.Usuario Result: ^(NSString *pResult)
     {
         [self FinalizarPago:pResult];
     }];
}

#pragma mark- Metodos Privados

- (void)FinalizarCompra:(NSString *)IDPagoPaypal
{
    [[Locator Default].Alerta hideBussy];
    [[Locator Default].Alerta showBussy:Traducir(@"Finalizando la compra.")];
    [[Locator Default].ServicioSOAP FinalizarCompraConID:self.Empresa.IDCarrito IDPaypal:IDPagoPaypal Result: ^(SRResultMethod pResult, NSString *pMensaje)
     {
         self.view.userInteractionEnabled = true;
         switch (pResult)
         {
             case SRResultMethodNO:
                 [[Locator Default].Alerta showMessageAcept:pMensaje];
                 break;
                 
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodYES:
                 [[Locator Default].Carrito QuitarEmpresa:self.Empresa];
                 self.Empresa = nil;
                 [[Locator Default].Alerta showMessageAcept:Traducir(@"Gracias por hacer tu compra en yaesmio!\n Revisa tu correo electrónico y te informaremos de todos los detalles") ResulBlock: ^{
                     [self.navigationController popViewControllerAnimated:true];
                     
                     if (self.Resultado != nil)
                     {
                         self.Resultado(true);
                     }
                 }];
                 break;
         }
     }];
}

- (ElementoCarrito *)obtenerProductoDeIndexPath:(NSIndexPath *)pIndexPath
{
    ElementoCarrito *lResultado = nil;
    
    if (self.Empresa != nil)
    {
        if (self.Empresa.Elementos.count > pIndexPath.row)
        {
            lResultado = self.Empresa.Elementos[pIndexPath.row];
        }
    }
    
    return lResultado;
}

- (void)CargarDatosPantalla
{
    self.labelPrecioTotal.text = [NSString stringWithFormat:Traducir(@"Precio total: %@ %@"), [self.Empresa.PrecioTotal toStringFormaterCurrency], self.Empresa.Moneda];
}

- (void)ObtenerListaTarjetas
{
    self.imageTipoTarjeta.image = nil;
    self.labelNumeroTarjeta.text = Traducir(@"Obteniendo formas de pago.");
    self.buttonCambiarFormaPago.hidden = true;
    self.labelModoTarjeta.text = @"";
    [self.BusyFormaPago startAnimating];
    
    [[Locator Default].ServicioSOAP ObtenerListaTarjetasUsuario: ^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *ListaTarjetas)
     {
         switch (pResult)
         {
             case SRResultMethodYES:
                 self.ListaTarjetas = ListaTarjetas;
                 break;
                 
             case SRResultMethodNO:
                 [[Locator Default].Alerta showMessageAcept:MensajeSAP];
                 break;
                 
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
         }
         [self SeleccionarTarjetaPrimeraOporDefecto];
         [self MostrarTarjetaSeleccionada];
     }];
}

- (void)SeleccionarTarjetaPrimeraOporDefecto
{
    if (self.ListaTarjetas.count > 0)
    {
        self.TarjetaSeleccionada = self.ListaTarjetas.firstObject;
    }
    else
    {
        self.TarjetaSeleccionada = [TarjetaUsuario Ninguna];
    }
}

- (void)MostrarTarjetaSeleccionada
{
    [self.BusyFormaPago stopAnimating];
    self.imageTipoTarjeta.image = [self getImageSegunTipoTarjeta:self.TarjetaSeleccionada.TipoTarjeta];
    self.labelNumeroTarjeta.text = [self getTextoSegunTipoTarjeta:self.TarjetaSeleccionada.TipoTarjeta];
    self.labelModoTarjeta.text = [self getTextoSegunModoTarjeta:self.TarjetaSeleccionada.ModoTarjeta];
    self.buttonCambiarFormaPago.hidden = false;
    
    if (self.TarjetaSeleccionada.TipoTarjeta == TipoTarjetaUsuarioOtra)
    {
        [self.labelModoTarjeta mgMoveToControl:self.imageTipoTarjeta Position:MGPositionMoveToTop Separation:0];
    }
    else
    {
        [self.labelModoTarjeta mgMoveToControl:self.imageTipoTarjeta Position:MGPositionMoveToBottonLeft Separation:0];
    }
}

- (UIImage *)getImageSegunTipoTarjeta:(TipoTarjetaUsuario)Tipo
{
    switch (Tipo)
    {
        case TipoTarjetaUsuarioOtra:
        case TipoTarjetaUsuarioNinguna:
            return [UIImage imageNamed:@"tarjeta_generica"];
            
        case TipoTarjetaUsuarioVisa:
            return [UIImage imageNamed:@"tarjeta_visa"];
            
        case TipoTarjetaUsuarioMasterCard:
            return [UIImage imageNamed:@"tarjeta_mastercard"];
            
        case TipoTarjetaUsuarioPaypal:
            return [UIImage imageNamed:@"tarjeta_paypal"];
    }
    return nil;
}

- (NSString *)getTextoSegunTipoTarjeta:(TipoTarjetaUsuario)Tipo
{
    switch (Tipo)
    {
        case TipoTarjetaUsuarioNinguna:
            return Traducir(@"Tarjeta de credito.");
            
        case TipoTarjetaUsuarioOtra:
        case TipoTarjetaUsuarioMasterCard:
        case TipoTarjetaUsuarioVisa:
            return [NSString stringWithFormat:@"%@\n%@/%@", self.TarjetaSeleccionada.NumeroTarjeta, self.TarjetaSeleccionada.Mes, self.TarjetaSeleccionada.Anio];
            
        case TipoTarjetaUsuarioPaypal:
            return Traducir(@"Paypal.");
    }
    return @"";
}

- (NSString *)getTextoSegunModoTarjeta:(ModoTarjetaUsuario)Modo
{
    switch (Modo)
    {
        case ModoTarjetaUsuarioNinguna:
            return @"";
            
        case ModoTarjetaUsuarioDebito:
            return Traducir(@"Debito");
            
        case ModoTarjetaUsuarioCredito:
            return Traducir(@"Credito");
    }
    return @"";
}

- (void)FinalizarPago:(NSString *)pIdPago
{
    if (pIdPago.isNotEmpty)
    {
        [self performSelector:@selector(FinalizarCompra:) withObject:pIdPago afterDelay:1];
    }
    else
    {
        self.view.userInteractionEnabled = true;
        [[Locator Default].ServicioSOAP CancelarCompra:self.Empresa.IDCarrito Result:nil];
         self.Empresa.IDCarrito = @"";
    }
}

@end
