//
//  BaseSixtyFour.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 03/11/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BaseSixtyFour : NSObject {
    
}
+ (NSString*) encode:(const uint8_t*) input length:(NSInteger) length;
+ (NSString*) encode:(NSData*) rawBytes;
+ (NSData*) decode:(const char*) string length:(NSInteger) inputLength;
+ (NSData*) decode:(NSString*) string;

@end
