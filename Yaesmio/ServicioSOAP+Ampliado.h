//
//  ServicioSOAP+Ampliado.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/07/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface ServicioSOAP (Ampliado)

- (void)EjecutarMetodoAsincrono:(SoapObject *)aParameters delegate:(id<SoapBindingResponseDelegate>)responseDelegate;

-(void) EjecutarMetodoAsincrono:(SoapMethod*)pMethod Response:(SoapMethodResponseBlock) pResponse;

@end
