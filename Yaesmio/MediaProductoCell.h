//
//  MediaProductoCell.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Media.h"

@interface MediaProductoCell : UICollectionViewCell

typedef void (^TapMediaCellResponseBlock)(Media *pMedia, NSTimeInterval pTimer);

-(void)ConfigurarVideo:(Media*)pMedia ResultTap:(TapMediaCellResponseBlock)pResultTap ResultFinishPlayback:(ResultadoBlock)pResultFinish;

-(void)ConfigurarImagen:(Media*)pMedia;

-(NSTimeInterval) getTimerActualVideo;

-(void)unloadAllControls;
-(void)inicializar;

+(NSString*) getReuseIdentifier:(Media*)pMedia;

-(void)StopVideo;
-(void)PauseVideo;
-(void)PlayVideo:(NSTimeInterval) pTimer;
+(NSString*)GrupoImage;
+(NSString*)GrupoVideo;

@end
