//
//  IdiomaManager.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 30/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IdiomaManager : NSObject

// devuelve lista de idiomas configuradas en la app
-(NSArray*)ListaIdiomasAPP;

// devuelve idioma del sistema en formato 2 caracteres
// ejemplo es, en etc....
-(NSString*)IdiomaDelSistema;

// devuelve idioma segun la config de la app
// si no lo encuentra devuelve es que es por defecto
-(NSString*)IdiomaAppSegunSistema;

// traduce el objeto pasado (NSString, UIlabel, UIbutton, NSArray)
-(NSString*)Traducir:(id)pObjeto;
@end
