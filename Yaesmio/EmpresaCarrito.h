//
//  EmpresaCarrito.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ElementoCarrito.h"
#import "ProductoPadre.h"
#import "SoapObject.h"

typedef NS_ENUM (NSInteger, ComprobarExistenciasEmpresaEnum)
{
    ComprobarExistenciasEmpresaEnumAll  = 0, // todo OK
    ComprobarExistenciasEmpresaEnumSOME = 1, // algunos
    ComprobarExistenciasEmpresaEnumANY  = 3, // ninguno
};

@interface EmpresaCarrito : SoapObject

@property (nonatomic) NSString *IdEmpresa;
@property (readonly)  NSArray *Elementos;
@property (readonly)  NSDecimalNumber *PrecioTotal;
@property (readonly)  NSString *Moneda;
@property (nonatomic) NSDecimalNumber *GastosEnvio;
@property (readonly)  BOOL existenElementos;
@property (readonly)  Media *LogoEmpresa;
@property (readonly)  NSString *UrlWebEmpresa;
@property (readonly)  BOOL esNecesariaDireccionEnvio;
@property (strong)    NSString *IDCarrito;

- (id)								init:(NSString *)pIdEmpresa;

- (id)								init:(NSString *)pIdEmpresa Nombre:(NSString *)pNombre Elementos:(NSArray *)pElementos;

- (void)							agregarProducto:(ProductoPadre *)pProducto Unidades:(NSDecimalNumber *)pUnidades;

- (BOOL)							QuitarElmento:(ElementoCarrito *)pElemento;

- (bool)							ModificarElemento:(ElementoCarrito *)pActual Modificado:(ProductoPadre *)pElemento Unidades:(NSDecimalNumber *)pUnidades;

- (ComprobarExistenciasEmpresaEnum) ComprovarExistencias:(NSArray *)pListaProducto;

- (ElementoCarrito *)				ObtenerElementoProducto:(ProductoPadre *)pProductoPadre;

@end
