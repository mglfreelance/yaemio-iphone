//
//  SeleccionContactosViewController.h
//  Yaesmio
//
//  Created by freelance on 21/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "YaesmioViewControllers.h"

typedef void (^SeleccionContactosBlock)(NSArray *pListaContactosSeleccionados);

@interface SeleccionContactosViewController : YaesmioViewController

@property (strong) NSArray *ListaEmailsSeleccionados;
@property (copy) SeleccionContactosBlock Respond;
@property (assign) BOOL MostrarContactosOtros;
@property (assign) BOOL MostrarGrupos;

@end
