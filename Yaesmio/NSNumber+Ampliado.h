//
//  NSNumber+Ampliado.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 04/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Ampliado)

// devuelve el numero formateado 2 decimales
- (NSString *)	toStringFormaterCurrency;

// devuelve el numero sin decimales
- (NSString *)	toStringFormaterWithoutDecimals;

// devuelve el numero formateado con 1 decimal
- (NSString *)	toStringFormaterOnceDecimal;

//devuelve el numero formateado con numdecimals especificado
- (NSString *)	toStringFormaterWithDecimals:(int)numDecimals;

//devuelve el numero con %
- (NSString *)	toStringFormaterWithPercent;

//devuelve el numero con -XX en dos digitos maximo ejem: -2
- (NSString *)	toStringDiscount;

@property (readonly) BOOL isZeroOrLess;
@property (readonly) BOOL isGreaterZero;
@end
