//
//  DestinatarioNotificacionCell.m
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "DestinatarioNotificacionCell.h"
#import "UIAllControls.h"

@interface DestinatarioNotificacionCell ()

@property (weak, nonatomic) IBOutlet UILabel *labelGrupo;
@property (weak, nonatomic) IBOutlet UIImageView *imageEstadoNotificacion;
@property (weak, nonatomic) IBOutlet UILabel *labelNombreDestinatario;
@property (weak, nonatomic) IBOutlet PAImageView *ImageContacto;

@end

@implementation DestinatarioNotificacionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        // Initialization code
    }
    
    return self;
}

- (void)inicializar
{
    self.labelGrupo.text = @"";
    self.imageEstadoNotificacion.image = nil;
    self.labelNombreDestinatario.text = @"";
    [self.ImageContacto setImage:nil animated:NO];
}

- (void)ConfigurarConDestinatario:(DestinatarioNotificacion *)pDestinatario
{
    [self inicializar];
    
    self.labelGrupo.text = pDestinatario.NombreGrupo;
    self.labelNombreDestinatario.text = [[Locator Default].Contactos getNombreContactoPorMail:pDestinatario.MailDestinatario];
    [self.ImageContacto setImage:[[Locator Default].Contactos getImagenContactoPorMail:pDestinatario.MailDestinatario] animated:YES];
    self.imageEstadoNotificacion.image = [self getImagenTipoNotificacion:pDestinatario.Tipo];
}

- (UIImage *)getImagenTipoNotificacion:(SNTipoNotificacion)pTipo
{
    switch (pTipo)
    {
        case SNTipoNotificacionRecibida:
            return [UIImage imageNamed:@"enviado"];
            
        case SNTipoNotificacionLeida:
            return [UIImage imageNamed:@"visualizado"];
            
        case SNTipoNotificacionEliminada:
            return [UIImage imageNamed:@"eliminado"];
            
        default:
            return nil;
    }
}

@end
