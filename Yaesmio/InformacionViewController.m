//
//  InformacionViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 19/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "InformacionViewController.h"
#import "UIAllControls.h"
#import "TituloInformacionCell.h"


@interface InformacionViewController ()

@property (nonatomic) IBOutlet UITableView *controlTabla;
@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *ListaCamposTraducir;

- (IBAction)CerrarModalExecute:(id)sender;

@property (strong) NSArray *ListaTitulos;

@end

@implementation InformacionViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    Traducir(self.ListaCamposTraducir);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
    
    [self obtenerListaTitulos];
}

- (void)viewUnload
{
    [self setControlTabla:nil];
    [self setListaCamposTraducir:nil];
    [self setListaTitulos:nil];
}

- (IBAction)CerrarModalExecute:(id)sender
{
    [self CerrarModal];
}

#pragma mark- Delegado UITableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.ListaTitulos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TituloInformacionCell *lVistaCelda = [tableView dequeueReusableCellWithIdentifier:@"TituloInformacionCell"];
    
    [lVistaCelda ConfigurarConPosicion:indexPath.row +1.0 Elemento:self.ListaTitulos[indexPath.row]];
    
    return lVistaCelda;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TituloInformacion *myTitulo = self.ListaTitulos[indexPath.row];
    
    if ([myTitulo.IdTitulo isEqualToString:@"ZWEB03"])
    {
        [[Locator Default].Vistas MostrarListaSitiosYaesmio:self];
    }
    else
    {
        [self MostrarInformacion:myTitulo];
    }
}


#pragma mark- Privado

- (void)MostrarInformacion:(TituloInformacion*)pTitulo
{
    [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo Información.")];
    [[Locator Default].ServicioSOAP ObtenerTexto:pTitulo.IdTitulo Result:^(SRResultMethod pResult, NSString *pTexto)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible obtener la informacion a mostrar.")];
                 break;
                 
             case SRResultMethodYES:
                 [[Locator Default].Alerta hideBussy];
                 [[Locator Default].Vistas MostrarWebView:self ContenidoWeb:pTexto];
                 break;
         }
     }];
}

- (void)obtenerListaTitulos
{
    [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo datos.")];
    [[Locator Default].ServicioSOAP ObtenerListaTitulosInformacion:^(SRResultMethod pResult, NSArray *pLista)
     {
         switch (pResult)
         {
             case SRResultMethodErrConex:
                 [[Locator Default].Alerta showErrorConexSOAP];
                 break;
                 
             case SRResultMethodNO:
                 [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible obtener la informacion a mostrar.")];
                 break;
                 
             case SRResultMethodYES:
                 [[Locator Default].Alerta hideBussy];
                 self.ListaTitulos = pLista;
                 [self.controlTabla reloadData];
                 break;
         }
         
     }];
}

@end
