//
//  BusquedaViewController.m
//  Yaesmio
//
//  Created by freelance on 08/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "BusquedaViewController.h"
#import "ProductoBusquedaCell.h"
#import "UIAllControls.h"
#import "EditarFiltroViewController.h"
#import "EditarOrdenacionFiltroViewController.h"
#import "CabeceraBusquedaCupon.h"

typedef NS_ENUM (NSInteger, ModoVistaProductoBusqueda)
{
    ModoVistaProductoBusquedaLista   = 0,
    ModoVistaProductoBusquedaMediano = 1,
    ModoVistaProductoBusquedaGrande  = 2,
};

#define SELECCIONAR_PROVINCIA	@"SELECCIONAR_PROVINCIA"
#define SELECCIONAR_PAIS		@"SELECCIONAR_PAIS"
#define ANCHO_TEXTO_BUSQUEDA	25

@interface BusquedaViewController () <UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, RTLabelDelegate>

@property (weak, nonatomic) IBOutlet UIView *vistaBusqueda;
@property (weak, nonatomic) IBOutlet UITextField *TextoBusqueda;
@property (weak, nonatomic) IBOutlet UICollectionView *CollectionView;
@property (strong, nonatomic)IBOutletCollection(UIView) NSArray * ListaCampos;
@property (weak, nonatomic) IBOutlet UIView *viewMensaje;
@property (weak, nonatomic) IBOutlet RTLabel *labelMensaje;
@property (weak, nonatomic) IBOutlet UIView *VistaLoading;
@property (weak, nonatomic) IBOutlet UIView *viewSinElementos;
@property (weak, nonatomic) IBOutlet UILabel *labelCargando;
@property (weak, nonatomic) IBOutlet UILabel *labelTitulo;
@property (weak, nonatomic) IBOutlet UIImageView *ImagenTitulo;
@property (weak, nonatomic) IBOutlet UIToolbar *ToolBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *ToolBarButtonGeolocalizar;

- (IBAction)CambiarTipoVistaExecute:(id)sender;
- (IBAction)BusquedaExecute:(id)sender;
- (IBAction)CerrarModalExecute:(id)sender;
- (IBAction)GeoposicionarExecute:(id)sender;


@property (nonatomic) int NumeroElementosTotales;
@property (strong) NSMutableArray *ListaActual;
@property (readonly) BOOL ExistenMasElementosQueObtener;
@property (nonatomic) ModoVistaProductoBusqueda ModoVisualizacionVista;
@property (assign) BOOL showHeader;
@property (assign) CGFloat BeginContentOffset;
@property (assign) BOOL EstaEjecutandoLLamadaSoap;
@property (assign) NSInteger NumeroGrupos;

@end

@implementation BusquedaViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.NumeroGrupos = 1;
    self.showHeader = YES;
    self.TextoBusqueda.text = @"";
    [self VistaBusy:@"" Mostrar:false];
    self.ListaActual = [NSMutableArray new];
    self.ModoVisualizacionVista = ModoVistaProductoBusquedaMediano;
    [self SetRTLabel:self.labelMensaje];
    [self.CollectionView mgExtendTopLeftForX:UNCHANGED andY:0.0];
    self.CollectionView.hidden = true;
    self.CollectionView.contentInset = UIEdgeInsetsMake(self.vistaBusqueda.frame.size.height, 0, self.CollectionView.contentInset.bottom, 0);
    
    if (self.Filtro.TipoBusqueda == TipoProductoBusquedaCupon && self.Filtro.Provincia == nil)                               //si no hay provincia en cupon poner todas.
    {
        [self ModificarFiltroConPaisProvincia:self.Filtro.Pais pProvincia:[Provincia Todas]];
    }
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0"))
    {
        self.ToolBar.tintColor = [Locator Default].Color.Principal;
    }
    
    switch (self.Filtro.TipoBusqueda)
    {
        case TipoProductoBusquedaProducto:
            self.labelTitulo.text = Traducir(@"Compra");
            self.ImagenTitulo.image = [UIImage imageNamed:@"boton_compra"];
            [self OcultarBotonGeolocalizar];
            break;
            
        case TipoProductoBusquedaCupon:
            self.labelTitulo.text = Traducir(@"Disfruta");
            self.ImagenTitulo.image = [UIImage imageNamed:@"boton_disfruta"];
            break;
            
        case TipoProductoBusquedaPublicitario:
            self.labelTitulo.text = Traducir(@"Mira");
            self.ImagenTitulo.image = [UIImage imageNamed:@"boton_mira"];
            [self OcultarBotonGeolocalizar];
            break;
    }
    
    [self PrimerInicioViewController];
}

- (void)viewUnload
{
    [super viewUnload];
    [self setTextoBusqueda:nil];
    [self setCollectionView:nil];
    [self setListaCampos:nil];
    [self setLabelMensaje:nil];
    [self setVistaLoading:nil];
    [[Locator Default].Carrito QuitarBotonCarrito:self];
    self.Filtro = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
}

- (IBAction)CerrarModalExecute:(id)sender
{
    [self CerrarModal];
}

#pragma mark- Propiedades

- (BOOL)ExistenMasElementosQueObtener
{
    int Total = self.NumeroElementosTotales;
    
    return (Total != self.ListaActual.count || self.ListaActual.count == 0);
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    self.BeginContentOffset = scrollView.contentOffset.y;
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    CGFloat contentOffset = scrollView.contentOffset.y;
    
    // When you scroll up to the point that the header can be seen, reveal it
    // Adjust the offset limit to suit your need
    if (contentOffset <= self.BeginContentOffset && !self.showHeader)
    {
        [UIView animateWithDuration:.5 animations: ^
         {
             [self.vistaBusqueda mgSetSizeX:UNCHANGED andY:0 andWidth:UNCHANGED andHeight:UNCHANGED];
         }		completion: ^(BOOL finished)
         {
             self.CollectionView.contentInset = UIEdgeInsetsMake(self.vistaBusqueda.frame.size.height, 0, self.CollectionView.contentInset.bottom, 0);
         }];
        self.showHeader = YES;
    }
    
    if (contentOffset > self.BeginContentOffset && self.showHeader)
    {
        [UIView animateWithDuration:.5 animations: ^
         {
             [self.vistaBusqueda mgSetSizeX:UNCHANGED andY:-(self.vistaBusqueda.frame.size.height - ANCHO_TEXTO_BUSQUEDA) andWidth:UNCHANGED andHeight:UNCHANGED];
         }		completion: ^(BOOL finished) {
             self.CollectionView.contentInset = UIEdgeInsetsMake(ANCHO_TEXTO_BUSQUEDA, 0, self.CollectionView.contentInset.bottom, 0);
         }];
        self.showHeader = NO;
    }
}

#pragma mark- Eventos Controles

- (IBAction)CambiarTipoVistaExecute:(id)sender
{
    switch (self.ModoVisualizacionVista)
    {
        case ModoVistaProductoBusquedaGrande:
            self.ModoVisualizacionVista = ModoVistaProductoBusquedaMediano;
            break;
            
        case ModoVistaProductoBusquedaMediano:
            self.ModoVisualizacionVista = ModoVistaProductoBusquedaLista;
            break;
            
        case ModoVistaProductoBusquedaLista:
            self.ModoVisualizacionVista = ModoVistaProductoBusquedaGrande;
            break;
    }
    
    [self.CollectionView performBatchUpdates:nil completion:nil];
    [self.CollectionView reloadSections:[[NSIndexSet alloc] initWithIndex:0]];
    
    self.viewSinElementos.hidden = (self.ListaActual.count == 0);
}

- (IBAction)BusquedaExecute:(id)sender
{
    [self.TextoBusqueda resignFirstResponder];
    
    if (self.Filtro.ExisteFiltro)
    {
        self.Filtro.Texto = self.TextoBusqueda.text;
        [self.ListaActual removeAllObjects];
        
        [self LLamarMetodoSoapBusqueda];
    }
}

- (IBAction)GeoposicionarExecute:(id)sender
{
    self.Filtro.CampoOrdenacion = CampoOrdenacionBusquedaCercana;
    [self GeolocalizarYObtenerDatosBusqueda];
}

- (BOOL)textFieldShouldReturn:(UITextField *)aTextField
{
    [self BusquedaExecute:nil];
    
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"segue_mostrar_filtro"])
    {
        EditarFiltroViewController *ViewController = segue.destinationViewController;
        
        self.Filtro.Texto = self.TextoBusqueda.text;
        ViewController.Filtro = self.Filtro;
        ViewController.Resultado = ^(int pResultado)
        {
            if (pResultado == true)
            {
                [self VolverALLamarServicioWeb];
            }
        };
    }
    else if ([segue.identifier isEqualToString:@"segue_mostrar_Ordenacion"])
    {
        EditarOrdenacionFiltroViewController *ViewController = segue.destinationViewController;
        
        self.Filtro.Texto = self.TextoBusqueda.text;
        ViewController.Filtro = self.Filtro;
        ViewController.Resultado = ^(int pResultado)
        {
            if (pResultado == true)
            {
                [self VolverALLamarServicioWeb];
            }
        };
    }
}

- (void)rtLabel:(id)rtLabel didSelectLinkWithURL:(NSURL *)url
{
    [self.view endEditing:true];
    
    if ([url.description isEqualToString:SELECCIONAR_PROVINCIA])
    {
        [self MostrarSeleccionProvincia];
    }
    else if ([url.description isEqualToString:SELECCIONAR_PAIS])
    {
        [self MostrarSeleccionPais];
    }
}

#pragma mark- Delegate CollectionView

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.NumeroGrupos;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    self.viewSinElementos.hidden = (self.ListaActual.count != 0);
    
    return [self NumeroElementosEnGrupo:section];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
    if ([self MostrarAgrupadoModoCercaDeMi])
    {
        return CGSizeMake(self.CollectionView.frame.size.width, 45.0);
    }
    else
    {
        return CGSizeMake(0.0, 0.0);
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.ModoVisualizacionVista)
    {
        case ModoVistaProductoBusquedaGrande:
            return CGSizeMake(320.f, 230.f);
            
        case ModoVistaProductoBusquedaMediano:
            return CGSizeMake(160.f, 155.f);
            
        case ModoVistaProductoBusquedaLista:
            return CGSizeMake(320.f, 67.f);
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *myLista = self.ListaActual;
    
    if (myLista.count >= indexPath.row)
    {
        [[Locator Default].Alerta showBussy:Traducir(@"Cargando Producto.")];
        ProductoBuscado *pBuscado = myLista[indexPath.row];
        [[Locator Default].ServicioSOAP ObtenerProducto:pBuscado.IdProductoHijo Medio:@"" Canal:TipoCanalProductoBusqueda Resultado: ^(SRResultObtenerProducto pResult, NSString *pMensajeError, ProductoPadre *pProductoPadre)
         {
             switch (pResult)
             {
                 case SRResultObtenerProductoNoYaesmio:
                 case SRResultObtenerProductoErrConex:
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
                     
                 case SRResultObtenerProductoNoExiste:
                     [[Locator Default].Alerta showMessageAcept:pMensajeError];
                     break;
                     
                 case SRResultObtenerProductoYES:
                     [[Locator Default].Alerta hideBussy];
                     [[Locator Default].Vistas MostrarProducto:self Producto:pProductoPadre VieneDeFavoritos:false];
                     break;
             }
         }];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ProductoBusquedaCell *lVistaCelda = [self.CollectionView dequeueReusableCellWithReuseIdentifier:[self getIDCell] forIndexPath:indexPath];
    
    [lVistaCelda ActualizarConProducto:[self geElementoGrupo:indexPath.section Posicion:indexPath.row] MostrarImagenGrande:(self.ModoVisualizacionVista == ModoVistaProductoBusquedaGrande) MostrarDistancia:[self MostrarAgrupadoModoCercaDeMi]];
    
    if (indexPath.row == self.ListaActual.count - 10 && self.ListaActual.count != self.NumeroElementosTotales && !self.EstaEjecutandoLLamadaSoap)
    {
        [self LLamarMetodoSoapBusqueda];
    }
    
    if (indexPath.row == self.ListaActual.count - 3 && self.ListaActual.count != self.NumeroElementosTotales && self.EstaEjecutandoLLamadaSoap)
    {
        [self VistaBusy:Traducir(@"Cargando") Mostrar:true];
    }
    
    return lVistaCelda;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader)
    {
        CabeceraBusquedaCupon *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CabeceraBusquedaCupon" forIndexPath:indexPath];
        reusableview = headerView;
        
        if ([self MostrarAgrupadoModoCercaDeMi])
        {
            [headerView Configurar:indexPath.section NumeroElementos:[self NumeroElementosEnGrupo:indexPath.section]];
        }
    }
    
    return reusableview;
}

#pragma mark- Private

- (NSString *)getIDCell
{
    switch (self.ModoVisualizacionVista)
    {
        case ModoVistaProductoBusquedaGrande:
            return @"Celda_BusquedaProducto_Grande";
            
        case ModoVistaProductoBusquedaMediano:
            return @"Celda_BusquedaProducto_Mediana";
            
        case ModoVistaProductoBusquedaLista:
            return @"Celda_BusquedaProducto_Lista";
    }
}

- (void)LLamarMetodoSoapBusqueda
{
    if (self.Filtro.ExisteFiltro && [self ExistenMasElementosQueObtener] && self.EstaEjecutandoLLamadaSoap == false)
    {
        self.EstaEjecutandoLLamadaSoap = true;
        [[Locator Default].ServicioSOAP ObtenerListaProductos:self.Filtro PosicionInicial:self.ListaActual.count + 1.0 NumeroElementos:20 Result: ^(SRResultMethod pResult, NSString *MensajeSAP, NSArray *pLista, int NumeroProductosEncontrados)
         {
             switch (pResult)
             {
                 case SRResultMethodErrConex:
                     [self.ListaActual removeAllObjects];
                     [[Locator Default].Alerta showErrorConexSOAP];
                     break;
                     
                 case SRResultMethodNO:
                     [self.ListaActual removeAllObjects];
                     break;
                     
                 case SRResultMethodYES:
                     [self.ListaActual addObjectsFromArray:pLista];
                     break;
             }
             self.NumeroElementosTotales = NumeroProductosEncontrados;
             self.CollectionView.hidden = (self.ListaActual.count == 0);
             [self.CollectionView reloadData];
             [self VistaBusy:@"" Mostrar:false];
             self.EstaEjecutandoLLamadaSoap = false;
             [self ActualizarMensajeSegunTipo:MensajeSAP];
         }];
    }
    else
    {
        self.CollectionView.hidden = (self.ListaActual.count == 0);
        [self ActualizarMensajeSegunTipo:@""];
    }
}

- (void)MostrarNoGeolocalizado
{
    [self.ListaActual removeAllObjects];
    [self.CollectionView reloadData];
    self.CollectionView.hidden = true;
    self.labelMensaje.text =  [NSString stringWithFormat:Traducir(@"¿%@ quieres buscar?"), [[Locator Default].FuenteLetra getStringHtmlLink:Traducir(@"Dónde") Url:SELECCIONAR_PAIS FontSize:16]];
    [self VistaBusy:@"" Mostrar:false];
}

- (void)ActualizarMensajeSegunTipo:(NSString *)MensajeSap
{
    NSInteger NumeroElementos = self.ListaActual.count;
    
    if (self.Filtro.TipoBusqueda == TipoProductoBusquedaCupon)
    {
        if (self.Filtro.Provincia == nil)
        {
            self.labelMensaje.text =  [NSString stringWithFormat:Traducir(@"¿%@ quieres buscar?"), [[Locator Default].FuenteLetra getStringHtmlLink:Traducir(@"Dónde") Url:SELECCIONAR_PROVINCIA FontSize:16]];
        }
        else if (self.Filtro.Provincia.esTodas)
        {
            self.labelMensaje.text = [NSString stringWithFormat:Traducir(@"%d encontrados. %@."), self.NumeroElementosTotales, [[Locator Default].FuenteLetra getStringHtmlLink:Traducir(@"Cambiar provincia") Url:SELECCIONAR_PROVINCIA FontSize:16]];
        }
        else if (self.Filtro.Provincia.esCercaDeMi || [self MostrarAgrupadoModoCercaDeMi])
        {
            self.labelMensaje.text = [NSString stringWithFormat:Traducir(@"%d encontrados %@."), self.NumeroElementosTotales, [[Locator Default].FuenteLetra getStringHtmlLink:Traducir(@"cerca de mi") Url:SELECCIONAR_PROVINCIA FontSize:16]];
        }
        else if (NumeroElementos > 1)
        {
            self.labelMensaje.text = [NSString stringWithFormat:Traducir(@"%d encontrados en %@"), self.NumeroElementosTotales, [[Locator Default].FuenteLetra getStringHtmlLink:self.Filtro.Provincia.Nombre Url:SELECCIONAR_PROVINCIA FontSize:16]];
        }
        else if (NumeroElementos == 1)
        {
            self.labelMensaje.text = [NSString stringWithFormat:Traducir(@"1 encontrado en %@"), [[Locator Default].FuenteLetra getStringHtmlLink:self.Filtro.Provincia.Nombre Url:SELECCIONAR_PROVINCIA FontSize:16]];
        }
        else if (NumeroElementos == 0)
        {
            self.labelMensaje.text = [NSString stringWithFormat:Traducir(@"En %@. %@"), [[Locator Default].FuenteLetra getStringHtmlLink:self.Filtro.Provincia.Nombre Url:SELECCIONAR_PROVINCIA FontSize:16], MensajeSap];
        }
    }
    else
    {
        if (NumeroElementos > 1)
        {
            self.labelMensaje.text = [NSString stringWithFormat:Traducir(@"%d encontrados."), self.NumeroElementosTotales];
        }
        else if (NumeroElementos == 1)
        {
            self.labelMensaje.text = Traducir(@"Uno encontrado.");
        }
        else if (NumeroElementos == 0)
        {
            self.labelMensaje.text = MensajeSap;
        }
    }
}

- (void)MostrarSeleccionProvincia
{
    [[Locator Default].Vistas MostrarSeleccionProvincia:self Pais:self.Filtro.Pais ProvinciaSeleccionada:self.Filtro.Provincia MostrarTodas:true MostrarCercaDeMi:true Resultado: ^(Provincia *pResult)
     {
         if (pResult != nil)
         {
             [self ModificarFiltroConPaisProvincia:self.Filtro.Pais pProvincia:pResult];
             [self ReiniciarTodasListas];
         }
     }];
}

- (void)MostrarSeleccionPais
{
    if (self.Filtro.Pais == nil)
    {
        TipoListaPaises tipo = (self.Filtro.TipoBusqueda == TipoProductoBusquedaCupon) ? TipoListaPaisesParaCupones : TipoListaPaisesGenerica;
        
        [[Locator Default].Vistas MostrarSeleccionPais:self Tipo:tipo PaisSeleccionado:nil Resultado: ^(Pais *pResult)
         {
             if (pResult != nil)
             {
                 [self ModificarFiltroConPaisProvincia:pResult pProvincia:self.Filtro.Provincia];
                 
                 if (self.Filtro.TipoBusqueda == TipoProductoBusquedaCupon)
                 {
                     [self MostrarSeleccionProvincia];
                 }
                 else
                 {
                     [self ReiniciarTodasListas];
                 }
             }
         }];
    }
    else
    {
        [self MostrarSeleccionProvincia];
    }
}

- (void)ReiniciarTodasListas
{
    self.labelMensaje.text = @"...";
    self.NumeroElementosTotales = 0;
    [self.ListaActual removeAllObjects];
    [self LLamarMetodoSoapBusqueda];
}

- (void)VistaBusy:(NSString *)Texto Mostrar:(BOOL)mostrar
{
    self.labelCargando.text = Texto;
    self.VistaLoading.hidden = !mostrar;
    self.ToolBar.hidden = mostrar;
}

- (void)SetRTLabel:(RTLabel *)myLabel
{
    myLabel.text = @"";
    myLabel.textColor = [Locator Default].Color.PrincipalOscuro;
    myLabel.font = [Locator Default].FuenteLetra.Defecto16;
    myLabel.lineBreakMode = RTTextLineBreakModeWordWrapping;
    myLabel.delegate = self;
}

- (void)OcultarBotonGeolocalizar
{
    NSMutableArray *lista = [[NSMutableArray alloc] initWithArray:self.ToolBar.items];
    
    [lista removeObject:self.ToolBarButtonGeolocalizar];
    
    [self.ToolBar setItems:lista];
}

- (void)ModificarFiltroConPaisProvincia:(Pais *)pPais pProvincia:(Provincia *)pProvincia
{
    self.Filtro.Pais = pPais;
    self.Filtro.Provincia = pProvincia;
    [Locator Default].Preferencias.PaisBusqueda = pPais;
    [Locator Default].Preferencias.ProvinciaBusqueda = pProvincia;
}

- (void)VolverALLamarServicioWeb
{
    if ([self MostrarAgrupadoModoCercaDeMi])
    {
        [self GeoposicionarExecute:nil];
    }
    else
    {
        [self ReiniciarTodasListas];
    }
}

- (BOOL)MostrarAgrupadoModoCercaDeMi
{
    return (self.Filtro.TipoBusqueda == TipoProductoBusquedaCupon && self.Filtro.CampoOrdenacion == CampoOrdenacionBusquedaCercana);
}

- (NSInteger)NumeroElementosEnGrupo:(NSInteger)grupo
{
    NSInteger resultado = self.ListaActual.count;
    
    if ([self MostrarAgrupadoModoCercaDeMi])
    {
        resultado = 0;
        
        for (ProductoBuscado *item in self.ListaActual)
        {
            if (item.GrupoDistancia == grupo)
            {
                resultado++;
            }
        }
    }
    
    return resultado;
}

- (ProductoBuscado *)geElementoGrupo:(NSInteger)Grupo Posicion:(NSInteger)posicion
{
    NSInteger actual = 0;
    
    for (ProductoBuscado *item in self.ListaActual)
    {
        if (item.GrupoDistancia == Grupo)
        {
            if (posicion == actual)
            {
                return item;
            }
            
            actual++;
        }
    }
    
    return nil;
}

- (void)GeolocalizarYObtenerDatosBusqueda
{
    if (self.Filtro.TipoBusqueda == TipoProductoBusquedaCupon && self.Filtro.CampoOrdenacion == CampoOrdenacionBusquedaCercana)
    {
        [self VistaBusy:Traducir(@"Geolocalizando tu provincia.") Mostrar:true];
        [[Locator Default].GPS getLocation: ^(GPSManager pResult, double pLatitud, double pLongitud) {
            if (pResult == GPSManagerOK)
            {
                self.Filtro.Latitud = pLatitud;
                self.Filtro.Longitud = pLongitud;
                self.Filtro.Provincia = [Provincia CercaDeMi];
                [self ReiniciarTodasListas];
            }
            else
            {
                [self MostrarNoGeolocalizado];
            }
        }];
    }
    else
    {
        [self ReiniciarTodasListas];
    }
}

- (void)PrimerInicioViewController
{
    if (self.Filtro.Pais == nil)
    {
        [self MostrarNoGeolocalizado];
    }
    else
    {
        [self VistaBusy:Traducir(@"Cargando") Mostrar:true];
        [self GeolocalizarYObtenerDatosBusqueda];
    }
}

@end
