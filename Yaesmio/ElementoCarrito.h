//
//  ProductoCarrito.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProductoPadre.h"
#import "ValoresdeAtributo.h"
#import "SoapObject.h"

@interface ElementoCarrito : SoapObject

@property (strong)    ProductoPadre    *Producto;
@property (nonatomic) NSDecimalNumber  *Unidades;
@property (readonly)  NSString         *TextoPropiedades;
@property (readonly)  NSDecimalNumber  *TotalPrecio;
@property (readonly)  BOOL             ExisteSuficienteStock;
@property (readonly)  BOOL             SePuedeComprar;
@property (readonly)  long             numeroUnidadesMaximoCompra;

+(ElementoCarrito*) crear:(ProductoPadre*)pProducto Unidades:(NSDecimalNumber*) pUnidades;

+ (ElementoCarrito *)deserializeNode:(xmlNodePtr)cur;

-(BOOL) sePuedeAgregarUnidadesProducto:(NSDecimalNumber*)pUnidades;

-(BOOL) isEqualTo:(ElementoCarrito*)pElemento;

@end
