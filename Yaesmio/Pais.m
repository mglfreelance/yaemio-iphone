//
//  Pais.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "Pais.h"

@implementation Pais

- (id)init
{
	if((self = [super init]))
    {
		self.Codigo = @"";
		self.Nombre = @"";
	}
	
	return self;
}

-(BOOL)isEqual:(id)object
{
    if([object isKindOfClass:[Pais class]])
    {
        Pais *pais = (Pais*)object;
        
        return [self.Codigo isEqualToString:pais.Codigo];
    }
    
    
    return [super isEqual:object];
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Codigo xmlNodeForDoc:node->doc elementName:@"LAND1" elementNSPrefix:@"tns3"]);
	
	xmlAddChild(node, [self.Nombre xmlNodeForDoc:node->doc elementName:@"LANDX" elementNSPrefix:@"tns3"]);
}

+ (Pais *)deserializeNode:(xmlNodePtr)cur
{
	Pais *newObject = [Pais new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

+(Pais *) newWithId:(NSString*)pID andName:(NSString*)pName
{
    Pais* resultado = [[Pais alloc]init];
    
    resultado.Codigo = pID;
    resultado.Nombre = pName;
    
    return resultado;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"LAND1"])
    {
        self.Codigo = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"LANDX"])
    {
        self.Nombre = [NSString deserializeNode:pobjCur];
    }
}

@end