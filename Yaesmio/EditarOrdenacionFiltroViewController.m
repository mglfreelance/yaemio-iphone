//
//  EditarOrdenacionFiltroViewController.m
//  Yaesmio
//
//  Created by freelance on 22/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "EditarOrdenacionFiltroViewController.h"
#import "UIAllControls.h"
#import "ElementoPicker.h"

@interface EditarOrdenacionFiltroViewController ()

@property (strong, nonatomic)IBOutletCollection(UIView) NSArray  * ListaCampos;
@property (weak, nonatomic)   IBOutlet UIButton *botonCampo;
@property (weak, nonatomic)   IBOutlet UIButton *botonTipo;

- (IBAction)botonCampoExecute:(id)sender;
- (IBAction)botonTipoExecute:(id)sender;
- (IBAction)botonGuardarOrdenacionExecute:(id)sender;

@property (assign) OrdenacionBusqueda ModoOrdenar;
@property (assign) CampoOrdenacionBusqueda CampoOrdenar;

@end

@implementation EditarOrdenacionFiltroViewController

NSString *TIPO_PRECIO = @"Precio";
NSString *TIPO_VALORACION = @"Valoracion";
NSString *TIPO_DESCUENTO = @"Descuento";
NSString *TIPO_ESTANDAR = @"Estandar";
NSString *TIPO_CERCADEMI = @"Cercania";
NSString *ORDEN_ASCENDENTE = @"Ascendente";
NSString *ORDEN_DESCENDENTE = @"Descendente";

@synthesize ModoOrdenar = _ModoOrdenar;
@synthesize CampoOrdenar = _CampoOrdenar;

- (void)viewDidLoad
{
    [super viewDidLoad];
    Traducir(self.ListaCampos);
    
    self.CampoOrdenar = self.Filtro.CampoOrdenacion;
    self.ModoOrdenar = self.Filtro.Ordenacion;
    
    [self.botonTipo setBackgroundImage:[Locator Default].Imagen.FondoBotonSeleccionBlanco forState:UIControlStateNormal];
    [self.botonCampo setBackgroundImage:[Locator Default].Imagen.FondoBotonSeleccionBlanco forState:UIControlStateNormal];
}

- (void)viewUnload
{
    [super viewUnload];
    
    [[Locator Default].Carrito QuitarBotonCarrito:self];
    self.Filtro = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
}

#pragma mark- Propiedades Internas

- (OrdenacionBusqueda)ModoOrdenar
{
    return _ModoOrdenar;
}

- (CampoOrdenacionBusqueda)CampoOrdenar
{
    return _CampoOrdenar;
}

- (void)setCampoOrdenar:(CampoOrdenacionBusqueda)pOrdenarPorNombre
{
    _CampoOrdenar = pOrdenarPorNombre;
    switch (_CampoOrdenar)
    {
        case CampoOrdenacionBusquedaEstandar:
            self.botonCampo.TextStateNormal = Traducir(TIPO_ESTANDAR);
            break;
            
        case CampoOrdenacionBusquedaPrecio:
            self.botonCampo.TextStateNormal = Traducir(TIPO_PRECIO);
            break;
            
        case CampoOrdenacionBusquedaValoracion:
            self.botonCampo.TextStateNormal = Traducir(TIPO_VALORACION);
            break;
            
        case CampoOrdenacionBusquedaDescuento:
            self.botonCampo.TextStateNormal = Traducir(TIPO_DESCUENTO);
            break;
            
        case CampoOrdenacionBusquedaCercana:
            if (self.Filtro.TipoBusqueda == TipoProductoBusquedaCupon)
            {
                self.botonCampo.TextStateNormal = Traducir(TIPO_CERCADEMI);
            }
            else
            {
                self.botonCampo.TextStateNormal = Traducir(TIPO_ESTANDAR);
            }
            
            break;
    }
    
    self.botonTipo.enabled = (self.CampoOrdenar != CampoOrdenacionBusquedaCercana && self.CampoOrdenar != CampoOrdenacionBusquedaEstandar);
}

- (void)setModoOrdenar:(OrdenacionBusqueda)pOrdenarAscendente
{
    _ModoOrdenar = pOrdenarAscendente;
    
    if (self.CampoOrdenar == CampoOrdenacionBusquedaCercana || self.CampoOrdenar == CampoOrdenacionBusquedaEstandar)
    {
        self.botonTipo.TextStateNormal = @"";
    }
    else
    {
        switch (_ModoOrdenar)
        {
            case OrdenacionBusquedaAscendente:
                self.botonTipo.TextStateNormal = Traducir(ORDEN_ASCENDENTE);
                break;
                
            case OrdenacionBusquedaDescendente:
                self.botonTipo.TextStateNormal = Traducir(ORDEN_DESCENDENTE);
                break;
                
            case OrdenacionBusquedaNinguno:
                self.botonTipo.TextStateNormal = @"";
                break;
        }
    }
}

#pragma mark- Eventos Controles

- (IBAction)botonCampoExecute:(id)sender
{
    [[Locator Default].Vistas MostrarPickerSemiModal:self ListaElementos:[self ListaElementosPickerCampo] Seleccionado:nil Titulo:Traducir(@"Campo") Result: ^(ElementoPicker *pResult)
     {
         if (pResult != nil)
         {
             self.CampoOrdenar = pResult.Codigo.intValue;
             
             if (self.CampoOrdenar == CampoOrdenacionBusquedaEstandar || self.CampoOrdenar == CampoOrdenacionBusquedaCercana)
             {
                 self.ModoOrdenar = OrdenacionBusquedaNinguno;
             }
             else if (self.ModoOrdenar == OrdenacionBusquedaNinguno)
             {
                 self.ModoOrdenar = OrdenacionBusquedaAscendente;
             }
         }
     }];
}

- (IBAction)botonTipoExecute:(id)sender
{
    if (self.CampoOrdenar != CampoOrdenacionBusquedaEstandar && self.CampoOrdenar != CampoOrdenacionBusquedaCercana)
    {
        [[Locator Default].Vistas MostrarPickerSemiModal:self ListaElementos:[self ListaElementosPickerOrden] Seleccionado:nil Titulo:Traducir(@"Tipo") Result: ^(ElementoPicker *pResult)
         {
             if (pResult != nil)
             {
                 self.ModoOrdenar = pResult.Codigo.intValue;
             }
         }];
    }
}

- (IBAction)botonGuardarOrdenacionExecute:(id)sender
{
    self.Filtro.Ordenacion = self.ModoOrdenar;
    self.Filtro.CampoOrdenacion = self.CampoOrdenar;
    
    if (self.CampoOrdenar == CampoOrdenacionBusquedaCercana)
    {
        self.Filtro.Provincia = [Provincia CercaDeMi];
    }
    else if (self.CampoOrdenar != CampoOrdenacionBusquedaCercana && self.Filtro.Provincia.esCercaDeMi)
    {
        self.Filtro.Provincia = [Provincia Todas];
    }
    
    [Locator Default].Preferencias.ProvinciaBusqueda = self.Filtro.Provincia;
    
    [self CerrarView:true];
}

#pragma mark- Private

- (NSArray *)ListaElementosPickerCampo
{
    NSMutableArray *lista = [[NSMutableArray alloc] initWithObjects:[ElementoPicker newWithIntCode:CampoOrdenacionBusquedaDescuento andName:Traducir(TIPO_DESCUENTO)], [ElementoPicker newWithIntCode:CampoOrdenacionBusquedaValoracion andName:Traducir(TIPO_VALORACION)], nil];
    
    if (self.Filtro.TipoBusqueda == TipoProductoBusquedaCupon)
    {
        [lista addObject:[ElementoPicker newWithIntCode:CampoOrdenacionBusquedaCercana andName:Traducir(TIPO_CERCADEMI)]];
    }
    
    [lista addObject:[ElementoPicker newWithIntCode:CampoOrdenacionBusquedaEstandar andName:Traducir(TIPO_ESTANDAR)]];
    
    return lista;
}

- (NSArray *)ListaElementosPickerOrden
{
    return @[[ElementoPicker newWithIntCode:OrdenacionBusquedaAscendente andName:Traducir(ORDEN_ASCENDENTE)], [ElementoPicker newWithIntCode:OrdenacionBusquedaDescendente andName:Traducir(ORDEN_DESCENDENTE)]];
}

- (void)CerrarView:(int)pValor
{
    [self.navigationController popViewControllerAnimated:true];
    
    if (self.Resultado != nil)
    {
        self.Resultado(pValor);
    }
}

@end
