//
//  DestinatarioNotificacion.m
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "DestinatarioNotificacion.h"

@implementation DestinatarioNotificacion
- (id)init
{
    if ((self = [super init]))
    {
        self.NombreGrupo = @"";
        self.MailDestinatario = @"";
        self.Tipo = SNTipoNotificacionNinguna;
    }
    
    return self;
}

+ (DestinatarioNotificacion *)deserializeNode:(xmlNodePtr)cur
{
    DestinatarioNotificacion *newObject = [[DestinatarioNotificacion alloc] init];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"Grupotxt"])
    {
        self.NombreGrupo = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Maildest"])
    {
        self.MailDestinatario = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Strecep"])
    {
        self.Tipo = [NSString deserializeNode:pobjCur].intValue;
    }
}

@end
