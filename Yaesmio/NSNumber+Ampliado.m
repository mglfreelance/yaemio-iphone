//
//  NSNumber+Ampliado.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 04/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "NSNumber+Ampliado.h"

@implementation NSNumber (Ampliado)

- (NSString *)toStringFormaterCurrency
{
    return [self toStringFormaterWithDecimals:2];
}

- (NSString *)toStringFormaterWithoutDecimals
{
    return [self toStringFormaterWithDecimals:0];
}

- (NSString *)toStringDiscount
{
    return [NSString stringWithFormat:@"-%@", [self toStringFormaterWithDecimals:0]];
}

- (NSString *)toStringFormaterWithPercent
{
    return [NSString stringWithFormat:@"%@%%", [self toStringFormaterWithDecimals:0]];
}

- (NSString *)toStringFormaterOnceDecimal
{
    return [self toStringFormaterWithDecimals:1];
}

- (NSString *)toStringFormaterWithDecimals:(int)numDecimals
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setMaximumFractionDigits:numDecimals];
    [formatter setMinimumFractionDigits:numDecimals];
    
    return [formatter stringFromNumber:self];
}

- (BOOL)isZeroOrLess
{
    return (self.intValue <= 0);
}

- (BOOL)isGreaterZero
{
    return !self.isZeroOrLess;
}

@end
