//
//  EstadisticasProducto.h
//  Yaesmio
//
//  Created by freelance on 07/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"

@interface EstadisticasProducto : SoapObject

@property (strong) NSDecimalNumber *NotaMediaEmpresa;
@property (strong) NSDecimalNumber *NotaMediaProducto;
@property (strong) NSDecimalNumber *NumeroValoracionesEmpresa;
@property (strong) NSDecimalNumber *NumeroValoracionesProducto;
@property (strong) NSDecimalNumber *NumeroVecesComprado;
@property (strong) NSDecimalNumber *NumeroVecesEnFavoritos;
@property (strong) NSDecimalNumber *NumeroVecesVisto;
@property (strong) NSDecimalNumber *NumeroVecesCompartido;
@property (strong) NSArray         *Evaluaciones;

+(EstadisticasProducto *) deserializeNode:(xmlNodePtr)cur;
- (EstadisticasProducto *)Clonar;

@end
