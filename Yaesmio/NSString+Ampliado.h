//
//  NSString+Ampliado.h
//  Yaesmio
//
//  Created by freelance on 23/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Ampliado)

@property (readonly) BOOL isNotEmpty;

@end
