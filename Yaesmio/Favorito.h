//
//  Favorito.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 25/07/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SoapObject.h"
#import "ProductoPadre.h"

@interface Favorito : SoapObject

@property (strong) NSString *AbreviaturaMoneda;
@property (strong) NSDecimalNumber *Precio;
@property (assign) BOOL Caducado;
@property (strong) NSString *ProductoId;
@property (strong) NSString *Referencia;
@property (strong) Media *Imagen;
@property (strong) ValoresdeAtributo *ValoresAtributos;
@property (strong) NSArray *ListaAtributos;
@property (strong) Media *LogoMarca;
@property (strong) NSString *Nombre;
@property (strong) NSDecimalNumber *NumElementos;
@property (strong) NSDate *Fecha;
@property (strong) NSString *ProductoIdPadre;
@property (strong) NSString *Latitud;
@property (strong) NSString *Longitud;
@property (strong) NSString *NombreEmpresa;
@property (strong) NSNumber *CantidadMax;
@property (strong) TipoCanal *TipoCanal;
@property (strong) NSDecimalNumber *Descuento;
@property (assign) BOOL Recomendada;

//ampliado
@property (readonly) NSString *TextoPropiedades;
@property (readonly) Atributo *Atributo1;
@property (readonly) Atributo *Atributo2;

+ (Favorito *)deserializeNode:(xmlNodePtr)cur;

@end
