//
//  AjustesViewController.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "AjustesViewController.h"
#import "UIAllControls.h"

@interface AjustesViewController ()

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *ListaCamposTraducir;

@property (nonatomic) IBOutlet UISwitch *wsitchGeolocalizacion;
@property (nonatomic) IBOutlet UISwitch *switchSesion;
@property (weak, nonatomic) IBOutlet UISwitch *switchEscucharSonidoNotificacion;
@property (nonatomic) IBOutlet UILabel *labelVersionApp;
@property (nonatomic) IBOutlet UILabel *labelSesion;

- (IBAction)switchGeolocalizacion:(UISwitch *)sender;
- (IBAction)switchSesion:(id)sender;
- (IBAction)btnBorrarCacheImagenes_click:(id)sender;
- (IBAction)switchEscucharSonidoNotificacionExecute:(id)sender;
- (IBAction)CerrarModalExecute:(id)sender;

@end

@implementation AjustesViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.switchSesion.on = [Locator Default].Preferencias.AutoLoginUsuario;
    self.wsitchGeolocalizacion.on = [Locator Default].Preferencias.GeolocalizacionActiva;
    self.switchEscucharSonidoNotificacion.on = [Locator Default].Preferencias.EscucharSonidoAlRecibirNotificacion;
    
    if ([Locator Default].Preferencias.EstaUsuarioLogado == false)
    {
        self.labelSesion.enabled  = false;
        self.switchSesion.enabled = false;
        self.switchSesion.on = false;
    }
    
    Traducir(self.ListaCamposTraducir);
    
    self.labelVersionApp.text = [NSString stringWithFormat:Traducir(@"Version App: %@"), [Locator Default].VersionAndBuid];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
}

- (void)viewUnload
{
    [self setListaCamposTraducir:nil];
    [self setSwitchEscucharSonidoNotificacion:nil];
    [self setLabelSesion:nil];
    [self setWsitchGeolocalizacion:nil];
    [self setSwitchSesion:nil];
    [self setLabelVersionApp:nil];
}

- (IBAction)switchGeolocalizacion:(UISwitch *)sender
{
    [Locator Default].Preferencias.GeolocalizacionActiva = self.wsitchGeolocalizacion.on;
}

- (IBAction)switchSesion:(id)sender
{
    [Locator Default].Preferencias.AutoLoginUsuario = self.switchSesion.on;
}

- (IBAction)btnBorrarCacheImagenes_click:(id)sender
{
    [[Locator Default].Alerta showBussy:Traducir(@"Borrando cache imagenes.")];
    [[Locator Default].Download ClearCacheImages];
    [[Locator Default].Alerta showMessageAcept:Traducir(@"Borrado de cache completado con existo")];
    
    [self simulateLowMemoryWarning];
}

- (IBAction)switchEscucharSonidoNotificacionExecute:(id)sender
{
    [Locator Default].Preferencias.EscucharSonidoAlRecibirNotificacion = self.switchEscucharSonidoNotificacion.on;
}

- (IBAction)CerrarModalExecute:(id)sender
{
    [self CerrarModal];
}

- (void)simulateLowMemoryWarning
{
#ifdef DEBUG
    // Send out MemoryWarningNotification
    [[UIApplication sharedApplication] performSelector:@selector(_performMemoryWarning)];
#endif
}

@end
