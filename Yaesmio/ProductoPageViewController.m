//
//  ProductoPageViewController.m
//  Yaesmio
//
//  Created by freelance on 05/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ProductoPageViewController.h"
#import "UIAllControls.h"
#import "MediaProductoCell.h"
#import "ProductoPageVC.h"
#import "ProductoDatosViewController.h"

@interface ProductoPageViewController () <UIPageViewControllerDataSource, ProductoPageVCDelegate>


@property (weak, nonatomic) IBOutlet UIImageView *LogoEmpresa;
- (IBAction)PulsadoLogoEmpresa_execute:(id)sender;

@property (strong) NSMutableArray *Lista;
@property (strong) UIImageView *ImagenFondo;
@property (assign) BOOL SeMuestraModal;

@end

@implementation ProductoPageViewController

@synthesize ElementoCarrito = _ElementoCarrito;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self RegenerarListaPagesPorProducto];
    [self ConfigurarViewsDeFondo];
    
    self.dataSource = self;
    [self setViewControllers:@[self.Lista[0]] direction:UIPageViewControllerNavigationDirectionForward animated:true completion:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if(self.esModificarProduto)
    {
        [[Locator Default].Carrito QuitarBotonCarrito:self];
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:Traducir(@"Cancelar") style:UIBarButtonItemStyleBordered target:self action:@selector(CancelarModificarProducto)];
    }
    else
    {
       [[Locator Default].Carrito agregarBotonCarrito:self EsVistaCarrito:false];
    }
    
    if(self.SeMuestraModal)
    {
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:Traducir(@"Atrás") style:UIBarButtonItemStyleBordered target:self action:@selector(CerrarModalExecute)];
    }
    
    if (self.isMovingToParentViewController)
    {
        [self ConfigurarViewsDeFondo];
    }
}

- (void)viewUnload
{
    if (self.Lista.count > 0)
    {
        [self.Lista removeAllObjects];
    }
    
    [self setProducto:nil];
    [self setLista:nil];
    [self setImagenFondo:nil];
    [self setElementoCarrito:nil];
}

- (BOOL)esModificarProduto
{
    return (self.ElementoCarrito != nil);
}

- (void)CerrarModalExecute
{
    [self CerrarModal];
}

-(void)MuestraModal
{
    self.SeMuestraModal = true;
}

#pragma mark- PagerViewController Delegate

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    UIViewController *anterior = [self.Lista lastObject];
    
    for (UIViewController *View in self.Lista)
    {
        if (viewController == View)
        {
            return anterior;
        }
        
        anterior = View;
    }
    
    return anterior;
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    BOOL encontrado = false;
    
    for (UIViewController *View in self.Lista)
    {
        if (encontrado)
        {
            return View;
        }
        
        encontrado = (View == viewController);
    }
    
    return self.Lista[0];
}

- (NSInteger)presentationCountForPageViewController:(UIPageViewController *)pageViewController
{
    [self setupPageControlAppearance:(int)self.Lista.count];
    return self.Lista.count;
}

- (NSInteger)presentationIndexForPageViewController:(UIPageViewController *)pageViewController
{
    return [self.Lista indexOfObject:pageViewController];
}

#pragma mark- ProductoPage Delegate

- (void)InformarCambioDePage:(ProductoPageVC *)pView Clave:(NSString *)pKey Objeto:(id)pObject
{
    if ([pView.NombrePage isEqualToString:@"ProductoDatosViewController"])
    {
        if ([pKey isEqualToString:@"SeleccionAtributos"])
        {
            [[Locator Default].Vistas MostrarValoresAtributoPickerSemiModal:self ListaElementos:self.Producto.ListaValoresAtributos Seleccionado:self.Producto.ProductoHijo.Atributos Titulos:self.Producto.ListaAtributos Result: ^(ValoresdeAtributo *pResult)
             {
                 if (pResult != nil)
                 {
                     [self CargarProductoHijo:(id)pResult View:pView];
                 }
             }];
        }
    }
}

#pragma  mark- Properties

- (void)setElementoCarrito:(ElementoCarrito *)pElementoCarrito
{
    _ElementoCarrito = pElementoCarrito;
    
    if (pElementoCarrito != nil)
    {
        self.Producto = pElementoCarrito.Producto;
    }
}

- (ElementoCarrito *)ElementoCarrito
{
    return _ElementoCarrito;
}

#pragma mark- Private

- (void)setupPageControlAppearance:(int)numberOfPages
{
    UIPageControl *pageControl = [[self.view.subviews filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"(class = %@)", [UIPageControl class]]] lastObject];
    
    if (pageControl != nil)
    {
        pageControl.hidden = false;
        pageControl.numberOfPages = numberOfPages;
        pageControl.pageIndicatorTintColor = [Locator Default].Color.Blanco;
        pageControl.currentPageIndicatorTintColor = [Locator Default].Color.Principal;
    }
}

- (void)ConfigurarViewsDeFondo
{
    self.view.backgroundColor = [Locator Default].Color.Negro;
    
    //Imagen de fondo
    if (self.ImagenFondo == nil)
    {
        self.ImagenFondo = [[UIImageView alloc] initWithFrame:CGRectMake(-20, -20, self.view.frame.size.width + 40, self.view.frame.size.height + 40)];
        [self.ImagenFondo setContentMode:UIViewContentModeScaleAspectFill];
        [self.ImagenFondo setAlpha:0.2f];
        [self.view addSubview:self.ImagenFondo];
        [self.view sendSubviewToBack:self.ImagenFondo];
    }
    
    [UIImage ImageAssyncFromUrl:[NSURL URLWithString:self.Producto.ProductoHijo.ImagenPorDefecto.UrlPath] Grupo:@"producto" Result: ^(UIImage *pResult)
     {
         if (pResult == nil)
         {
             pResult = [Locator Default].Imagen.ImagenFondoPantallaPrincipal;
         }
         
         [UIView transitionWithView:self.ImagenFondo duration:1.0f options:UIViewAnimationOptionTransitionCrossDissolve animations: ^
          {
              self.ImagenFondo.hidden = false;
              self.ImagenFondo.image = pResult;
          }		completion:nil];
     }];
    
    
    self.LogoEmpresa.contentMode = UIViewContentModeScaleAspectFit;
    self.LogoEmpresa.image = nil;
    [self.LogoEmpresa cargarUrlImagenAsincrono:self.Producto.LogoEmpresa.UrlPath Busy:nil];
}

- (ProductoPageVC *)getViewControllerInitialize:(NSString *)pName
{
    ProductoPageVC *myViewController = [self.storyboard instantiateViewControllerWithIdentifier:pName];
    
    if (myViewController != nil)
    {
        myViewController.NombrePage = pName;
        myViewController.Producto = self.Producto;
        myViewController.ElementoCarrito = self.ElementoCarrito;
        myViewController.VieneDeFavoritos = self.VieneDeFavoritos;
        myViewController.delegate = self;
    }
    
    return myViewController;
}

- (IBAction)PulsadoLogoEmpresa_execute:(id)sender
{
    if (![self.Producto.UrlWebEmpresa isEqualToString:@""])
    {
        [[Locator Default].Vistas MostrarWebView:self url:self.Producto.UrlWebEmpresa];
    }
}

- (void)CargarProductoHijo:(ValoresdeAtributo *)pAtributo1 View:(ProductoPageVC *)pView
{
    [[Locator Default].Alerta showBussy:Traducir(@"Obteniendo Producto.")];
    [[Locator Default].ServicioSOAP ObtenerProductoHijo:pAtributo1.ProductoId Result: ^(SRResultObtenerProducto pResult, Producto *pProducto)
     {
         [[Locator Default].Alerta hideBussy];
         
         if (pResult == SRResultObtenerProductoYES)
         {
             self.Producto = [self.Producto Clonar:pProducto];                 // al no devolver el servicio un padre lo tengo que clonar, cosas de SAP
             [self RegenerarListaPagesPorProducto];
             
             [UIImage ImageAssyncFromUrl:[NSURL URLWithString:self.Producto.ProductoHijo.ImagenPorDefecto.UrlPath] Grupo:@"producto" Result: ^(UIImage *pResult)
              {
                  [UIView transitionWithView:self.view duration:1.0f options:UIViewAnimationOptionShowHideTransitionViews | UIViewAnimationOptionTransitionCrossDissolve animations: ^
                   {
                       pView.view.hidden = false;
                       self.ImagenFondo.image = pResult;
                       self.ImagenFondo.hidden = false;
                   }		completion:nil];
              }];
         }
         else
         {
             [[Locator Default].Alerta showMessageAcept:Traducir(@"No ha sido posible obtener los datos.") ResulBlock: ^
              {
                  [self.navigationController popToRootViewControllerAnimated:TRUE];
              }];
         }
     }];
}

- (ProductoPageVC *)getProductoPage:(NSArray *)pLista Clave:(NSString *)pKey
{
    if (pLista != nil && pLista.count > 0)
    {
        for (ProductoPageVC *Page in pLista)
        {
            if (Page != nil && [Page.NombrePage isEqualToString:pKey])
            {
                Page.Producto = self.Producto;
                Page.VieneDeFavoritos = self.VieneDeFavoritos;
                [Page Actualizar];
                return Page;
            }
        }
    }
    
    return [self getViewControllerInitialize:pKey];
}

- (void)RegenerarListaPagesPorProducto
{
    NSArray *listaOld = self.Lista;
    
    self.Lista = [[NSMutableArray alloc] init];
    
    [self.Lista addObject:[self getProductoPage:listaOld Clave:@"ProductoDatosViewController"]];
    
    
    if (self.Producto.ProductoHijo.Descripcion2.isNotEmpty)       // contiene html
    {
        [self.Lista addObject:[self getProductoPage:listaOld Clave:@"ProductoWebViewViewController"]];
    }
    
    if (self.Producto.ProductoHijo.ListaMediaImagen.count > 1)       //contiene mas de una imagen
    {
        [self.Lista addObject:[self getProductoPage:listaOld Clave:@"ProductoImagenesViewController"]];
    }
    
    [self.Lista addObject:[self getProductoPage:listaOld Clave:@"ProductoEstadisticasViewController"]];
}

-(void)CancelarModificarProducto
{
    [self.navigationController dismissViewControllerAnimated:true completion:nil];
}

@end
