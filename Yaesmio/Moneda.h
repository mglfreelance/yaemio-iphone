//
//  Moneda.h
//  Yaesmio
//
//  Created by freelance on 08/07/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ElementoPicker.h"

@interface Moneda : ElementoPicker

@property (readonly) NSString *Abreviatura;
@property (strong) NSString *LTEXT;

+(Moneda *) deserializeNode:(xmlNodePtr)cur;
+(Moneda *) newWithId:(NSString*)pID andName:(NSString*)pName;

@end
