//
//  ContactoTelefonoCell.h
//  Yaesmio
//
//  Created by freelance on 21/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactoTelefonoCell : UITableViewCell

- (void)configurar:(id)Objeto Seleccionado:(BOOL)pSeleccionado;

@property (assign) BOOL Seleccionado;

@end
