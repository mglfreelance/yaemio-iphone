//
//  UIScrollView+Ampliado.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import "UIScrollView+Ampliado.h"

@implementation UIScrollView (Ampliado)


- (void)setContentSizeForSubviewUntilControl:(UIView*)pControl AddSize:(CGSize)pSize
{
    CGSize lValor = [self getSubViewsContenSizeUntilControl:pControl];
    
    self.contentSize = CGSizeMake(pSize.width + lValor.width, pSize.height + lValor.height);
}

- (void)setWidthContentSizeForSubviewAddSize:(float)pWidth
{
    CGSize lValor = [self getSubViewsContenSize];
    
    self.contentSize = CGSizeMake(lValor.width + pWidth, self.frame.size.height);
}

- (void)setHeightContentSizeForSubviewAddSize:(float)pHeight
{
    CGSize lValor = [self getSubViewsContenSize];
    
    self.contentSize = CGSizeMake(self.frame.size.width, lValor.height + pHeight);
}

- (void)setContentSizeForSubviewUntilControl:(UIView*)pControl
{
    [self setContentSizeForSubviewUntilControl:pControl AddSize:CGSizeMake(0, 0)];
}

- (void)setWidthContentSizeForSubview
{
    [self setWidthContentSizeForSubviewAddSize:0];
}

- (void)setHeightContentSizeForSubview
{
    [self setHeightContentSizeForSubviewAddSize:0];
}

- (void)setHeightContentSizeForSubviewUntilControl:(UIView *)pControl
{
    [self setHeightContentSizeForSubviewUntilControl:pControl AddHeight:0.0f];
}

- (void)setHeightContentSizeForSubviewUntilControl:(UIView *)pControl AddSize:(CGSize)pSize
{
    CGSize lValor = [self getSubViewsContenSizeUntilControl:pControl];
    
    self.contentSize = CGSizeMake(self.frame.size.width + pSize.width, lValor.height + pSize.height);
}

- (CGSize)getSubViewsContenSizeUntilControl:(UIView*)pControl
{
    return [self getContenSizeForList:self.subviews ToControl:pControl];
}

- (void)setHeightContentSizeForSubviewUntilControl:(UIView *)pControl AddHeight:(float)pSize
{
    [self setHeightContentSizeForSubviewUntilControl:pControl AddSize:CGSizeMake(0, pSize)];
}

- (CGSize)getSubViewsContenSize
{
    return [self getContenSizeForList:self.subviews ToControl:nil];
}

- (CGSize)getContenSizeForList:(NSArray *)pList ToControl:(id)pControl
{
    float lSizeWidth  = 0.0f;
    float lSizeHeight = 0.0f;
    float myWidth     = 0.0f;
    float myHeight    = 0.0f;
    
    if (pControl == nil)
    {
        pControl = pList.lastObject;
    }
    
    for (UIView *lview in pList)
    {
        if (lview.hidden == false)
        {
            myWidth = lview.frame.origin.x + lview.frame.size.width;
            myHeight = lview.frame.origin.y + lview.frame.size.height;
            
            if (lSizeWidth < myWidth)
            {
                lSizeWidth = myWidth;
            }
            
            if (lSizeHeight < myHeight)
            {
                lSizeHeight = myHeight;
            }
        }
        
        if (lview == pControl)
        {
            break;
        }
    }
    
    return CGSizeMake(lSizeWidth, lSizeHeight);
}

@end
