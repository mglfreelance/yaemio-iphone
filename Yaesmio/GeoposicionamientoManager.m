//
//  GeoposicionamientoManager.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 03/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "GeoposicionamientoManager.h"

@interface GeoposicionamientoManager () <CLLocationManagerDelegate>

@property (strong) CLLocationManager *mLocation;
@property (copy)   getLocationBlock mLocationBlock;
@property (copy)   getLocationAndPostalCodeBlock mLocationAndPostalBlock;
@end


@implementation GeoposicionamientoManager


- (GPSManager)Authorized
{
    if ([CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied)
    {
        return GPSManagerOK;
    }
    
    return GPSManagerNO;
}

- (void)getLocation:(getLocationBlock)pBlock
{
    self.mLocationBlock = pBlock;
    
    if ([self ActivarGeolocalizacion] == false)       // no esta activado gps o no ha dado permiso
    {
        [self LLamadaBloqueGPS:GPSManagerNO Latitud:0.0 Longitud:0.0];
    }
}

- (void)getLocationAndPostalCode:(getLocationAndPostalCodeBlock)pBlock
{
    self.mLocationAndPostalBlock = pBlock;
    
    if ([self ActivarGeolocalizacion] == false)
    {   // no esta activado gps o no ha dado permiso
        [self LLamadaBloqueGPS:GPSManagerNO Latitud:0.0 Longitud:0.0];
    }
}

- (void)LLamadaBloqueGPS:(GPSManager)pResult Latitud:(double)pLatitud Longitud:(double)pLongitud
{
    if(pResult != GPSManagerErr)
    {
        [self FinalizarGeolocalizacion];
    }
    
    if (self.mLocationBlock != nil)
    {
        self.mLocationBlock(pResult, pLatitud, pLongitud);
        self.mLocationBlock = nil;
    }
    
    if (pResult == GPSManagerOK)
    {
        CLLocation *local = [[CLLocation alloc] initWithLatitude:pLatitud longitude:pLongitud];
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder reverseGeocodeLocation:local completionHandler: ^(NSArray *placemarks, NSError *error)
         {
             GPSManager resultado = GPSManagerNO;
             CLPlacemark *placemark =  nil;
             
             if (error == nil && placemarks.count > 0)
             {
                 placemark = [placemarks firstObject];
                 
                 if (placemark != nil)
                 {
                     resultado = GPSManagerOK;
                 }
             }
             
             [self LLamarLocationAndPostalBlock:resultado Latitud:pLatitud Longitud:pLongitud Situacion:placemark];
         }];
    }
    else
    {
        [self LLamarLocationAndPostalBlock:pResult Latitud:0.0 Longitud:0.0 Situacion:nil];
    }
}

- (void)LLamarLocationAndPostalBlock:(GPSManager)resultado Latitud:(double)pLatitud Longitud:(double)pLongitud Situacion:(CLPlacemark *)pSituacion
{
    if (self.mLocationAndPostalBlock != nil)
    {
        self.mLocationAndPostalBlock(resultado, pLatitud, pLongitud, pSituacion);
        self.mLocationAndPostalBlock = nil;
    }
}

- (BOOL)ActivarGeolocalizacion
{
    BOOL resultado = false;
    
    if ([Locator Default].Preferencias.GeolocalizacionActiva)
    {
        self.mLocation = [[CLLocationManager alloc] init];
        self.mLocation.delegate = self;
        self.mLocation.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        self.mLocation.distanceFilter = kCLDistanceFilterNone;
        [self.mLocation startUpdatingLocation];
        resultado = true;
    }
    
    return resultado;
}

- (void)FinalizarGeolocalizacion
{
    //desactivar gps
    [self.mLocation stopUpdatingLocation];
    self.mLocation = nil;
}

#pragma mark- Location Delegate

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    CLLocation *lUtimo = [locations lastObject];
    GPSManager lResult = GPSManagerErr;
    double lLatitud = 0.0;
    double lLongitud = 0.0;
    
    if (lUtimo != nil)
    {
        lLatitud = lUtimo.coordinate.latitude;
        lLongitud = lUtimo.coordinate.longitude;
        
        if (lLatitud == 0 && lLongitud == 0)
        {
            lResult = GPSManagerNO;
        }
        else
        {
            lResult = GPSManagerOK;
        }
    }
    
    [self LLamadaBloqueGPS:lResult Latitud:lLatitud Longitud:lLongitud];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    [self LLamadaBloqueGPS:GPSManagerErr Latitud:0.0f Longitud:0.0f];
}

@end
