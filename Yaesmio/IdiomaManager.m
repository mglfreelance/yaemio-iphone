//
//  IdiomaManager.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 30/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIAllControls.h"

@implementation IdiomaManager


-(NSArray*)ListaIdiomasAPP
{
    NSArray* languages = [[NSBundle mainBundle] localizations];
    
    return languages;
}

-(NSString*) IdiomaAppSegunSistema
{
    NSString *lStrResultado = self.IdiomaDelSistema; // por defecto del sistema
    BOOL lbolEncontrado = false;
    
    for (NSString *LDato in self.ListaIdiomasAPP)
    {
        if ([LDato isEqualToString:lStrResultado])
        {
            lbolEncontrado = true;
            break; // salida directa
        }
    }
    
    if (lbolEncontrado == false)
    {
        lStrResultado =@"es";
    }
    
    return lStrResultado;
}

-(NSString*) IdiomaDelSistema
{
    NSString *userLocale = [[NSLocale currentLocale] objectForKey:NSLocaleLanguageCode];
    return userLocale;
}

-(NSString*)TraducirTexto:(NSString*)pTexto
{
    return NSLocalizedString(pTexto, @"");
}

-(NSString*)Traducir:(id)pObjeto
{
    NSString *lResultado = @"";
    
    if (pObjeto != nil)
    {
        if ([pObjeto isKindOfClass:[NSString class]])
        {
            NSString *pTexto = pObjeto;
            lResultado = [self TraducirTexto:pTexto];
        }
        else if ([pObjeto isKindOfClass:[UIBarButtonItem class]])
        {
            UIBarButtonItem *button= pObjeto;
            button.title = Traducir(button.title);
        }
        else if ([pObjeto isKindOfClass:[UILabel class]])
        {
            UILabel *pLabel = pObjeto;
            lResultado = [self TraducirTexto:pLabel.text];
            pLabel.text = lResultado;
        }
        else if ([pObjeto isKindOfClass:[UIButton class]])
        {
            UIButton *pButton = pObjeto;
            lResultado = [self TraducirTexto:pButton.TextStateNormal];
            pButton.TextStateNormal = lResultado;
        }
        else if ([pObjeto isKindOfClass:[NSArray class]])
        {
            NSArray *pLista = pObjeto;
            for (id pDato in pLista)
            {
                [self Traducir:pDato];
            }
        }
        else if ([pObjeto isKindOfClass:[UITabBarItem class]])
        {
            UITabBarItem *lItem = pObjeto;
            lResultado = [self TraducirTexto:lItem.title];
            lItem.title = lResultado;
        }
        else if ([pObjeto isKindOfClass:[UISegmentedControl class]])
        {
            UISegmentedControl *segmented = (UISegmentedControl*)pObjeto;
            for (int i =0; i< segmented.numberOfSegments; i++)
            {
                [segmented setTitle:[self Traducir:[segmented titleForSegmentAtIndex:i]] forSegmentAtIndex:i];
            }
        }
        else
        {
            NSLog(@"No hay traduccion para tipo objeto %@",[pObjeto class]);
        }
    }
    
    return lResultado;
}

@end
