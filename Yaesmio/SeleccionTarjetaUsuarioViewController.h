//
//  SeleccionTarjetaUsuarioViewController.h
//  Yaesmio
//
//  Created by freelance on 09/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "YaesmioViewControllers.h"
#import "TarjetaUsuario.h"

typedef void (^ResultadoSeleccionTarjetaUsuarioBlock)(TarjetaUsuario *Tarjeta);

@interface SeleccionTarjetaUsuarioViewController : YaesmioTableViewController

-(void)ConfigurarConListaTarjetas:(NSArray*)lista MostrarOtraTarjeta:(BOOL)muestraOtraTarjeta MostrarPaypal:(BOOL)muestraPaypal Resultado:(ResultadoSeleccionTarjetaUsuarioBlock)resultado;

@end
