//
//  GrupoNotificacionCell.m
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "GrupoNotificacionCell.h"
#import "UIAllControls.h"

@interface GrupoNotificacionCell ()

@property (weak, nonatomic) IBOutlet UILabel *labelNombreGrupo;
@property (weak, nonatomic) IBOutlet UIView *VistaMenuContextual;
@property (weak, nonatomic) IBOutlet UIView *ViewFondoCelda;

@end

@implementation GrupoNotificacionCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        [self Inicializar];
    }
    
    return self;
}

- (void)Inicializar
{
    self.labelNombreGrupo.text = @"";
}

- (void)Configurar:(GrupoNotificacion *)pGrupo Menu:(id)Menu
{
    self.labelNombreGrupo.text = pGrupo.NombreGrupo;
    
    [self.VistaMenuContextual mgRemoveAllGestureRecognizers];
    [self.VistaMenuContextual mgAddGestureRecognizerTap:Menu Action:@selector(showMenuUponActivationOfGetsure:)];
    
    [self.ViewFondoCelda mgRemoveAllGestureRecognizers];
    [self.ViewFondoCelda mgAddGestureRecognizerLongPress:Menu Action:@selector(showMenuUponActivationOfGetsure:)];
}


@end
