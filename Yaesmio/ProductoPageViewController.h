//
//  ProductoPageViewController.h
//  Yaesmio
//
//  Created by freelance on 05/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YaesmioViewControllers.h"

@interface ProductoPageViewController : YaesmioPageViewController

@property (strong) ElementoCarrito *ElementoCarrito;
@property (strong) ProductoPadre *Producto;
@property (assign) BOOL VieneDeFavoritos;
@property (readonly) BOOL esModificarProduto;

@end
