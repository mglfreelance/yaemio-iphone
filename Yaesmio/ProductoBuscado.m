//
//  ProductoBuscado.m
//  Yaesmio
//
//  Created by freelance on 09/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ProductoBuscado.h"

@implementation ProductoBuscado

@synthesize GrupoDistancia = _GrupoDistancia;

- (id)init
{
    if ((self = [super init]))
    {
        self.IdProductoPadre    = @"";
        self.IdProductoHijo     = @"";
        self.Nombre             = @"";
        self.NombreEmpresa      = @"";
        self.Imagen             = [Media createMediaImage];
        self.Moneda             = @"";
        self.Precio             = nil;
        self.Descuento          = nil;
        self.Recomendada        = false;
        self.Distancia          = nil; //[NSDecimalNumber decimalNumberWithString:@"89.45"];
        self.Latitud            = nil;
        self.Longitud           = nil;
        _GrupoDistancia         = GrupoDistanciaProductoBuscadoMenos200m;
    }
    
    return self;
}

+ (ProductoBuscado *)deserializeNode:(xmlNodePtr)cur
{
    ProductoBuscado *newObject = [[ProductoBuscado alloc] init];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"Descpadre"])
    {
        self.Nombre = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"IdEmpresa"])
    {
        self.NombreEmpresa = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Idhijodef"])
    {
        self.IdProductoHijo = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Idmgppalhijo"])
    {
        self.Imagen.UrlPath = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Idpadre"])
    {
        self.IdProductoPadre = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Thumbnailhijo"])
    {
        self.Imagen.UrlPathSmall = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Zzmoneda"])
    {
        self.Moneda = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Zzprecioactual"])
    {
        self.Precio =  [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ZZDESCUENTO"])
    {
        self.Descuento = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Recomendada"])
    {
        self.Recomendada = [SOAP_Boolean deserializeNode:pobjCur].boolValue;
    }
    else if ([nodeName isEqualToString:@"Distancia"])
    {
        self.Distancia = [NSDecimalNumber deserializeNode:pobjCur];
        _GrupoDistancia = [self CalcularGrupo];
    }
    else if ([nodeName isEqualToString:@"Latitud"])
    {
        self.Latitud = [NSDecimalNumber deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Longitud"])
    {
        self.Longitud = [NSDecimalNumber deserializeNode:pobjCur];
    }
}

- (GrupoDistanciaProductoBuscado)CalcularGrupo
{
    NSInteger distancia = 0;
    GrupoDistanciaProductoBuscado resultado = GrupoDistanciaProductoBuscadoMenos200m;
    
    if (self.Distancia != nil)
    {
        distancia = self.Distancia.integerValue;
    }
    
    if (distancia < 200)
    {
        resultado = GrupoDistanciaProductoBuscadoMenos200m;
    }
    else if (distancia < 500)
    {
        resultado = GrupoDistanciaProductoBuscadoMenos500m;
    }
    else if (distancia < 3000)
    {
        resultado = GrupoDistanciaProductoBuscadoMenos3km;
    }
    else if (distancia < 10000)
    {
        resultado = GrupoDistanciaProductoBuscadoMenos10km;
    }
    else if (distancia < 50000)
    {
        resultado = GrupoDistanciaProductoBuscadoMenos50km;
    }
    else if (distancia < 200000)
    {
        resultado = GrupoDistanciaProductoBuscadoMenos200km;
    }
    else
    {
        resultado = GrupoDistanciaProductoBuscadoMayor200km;
    }
    
    return resultado;
}

@end
