//
//  TipoCanal.m
//  Yaesmio
//
//  Created by freelance on 12/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "TipoCanal.h"

@implementation TipoCanal

- (id)init
{
    if ((self = [super init]))
    {
        self.TipoCanal = TipoCanalProductoBusqueda;
    }
    
    return self;
}

- (id)init:(TipoCanalProducto)pTipo
{
    if ((self = [super init]))
    {
        self.TipoCanal = pTipo;
    }
    
    return self;
}

-(BOOL) isEqualTo:(TipoCanal*)pTipo
{
    if(pTipo != nil)
        return self.TipoCanal ==pTipo.TipoCanal;
    
    return false;
}

+(instancetype) TipoCanalEscanear
{
    return [[TipoCanal alloc] init:TipoCanalProductoEscanear];
}

+(instancetype) TipoCanalBusqueda
{
    return [[TipoCanal alloc] init:TipoCanalProductoBusqueda];
}

+(instancetype) TipoCanalNotificacion
{
    return [[TipoCanal alloc] init:TipoCanalProductoNotificacion];
}

+(instancetype) TipoCanalFavorito
{
    return [[TipoCanal alloc] init:TipoCanalProductoFavorito];
}


-(NSString*)ID
{
    switch (self.TipoCanal)
    {
        case TipoCanalProductoEscanear:
            return @"P";
            break;
        case TipoCanalProductoBusqueda:
            return @"B";
            break;
        case TipoCanalProductoNotificacion:
            return @"N";
            break;
        case TipoCanalProductoFavorito:
            return @"P";
            break;
    }
}

@end
