//
//  GrupoNotificacionCell.h
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GrupoNotificacion.h"

@interface GrupoNotificacionCell : UITableViewCell

- (void)Configurar:(GrupoNotificacion *)pGrupo Menu:(id)Menu;

@end
