//
//  Destacado.h
//  Yaesmio
//
//  Created by freelance on 20/08/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"

@interface ProductoDestacado : SoapObject <NSCoding>

@property (strong) NSString *NombreEmpresa;
@property (strong) NSString *ProductoId;
@property (strong) NSString *Nombre;
@property (strong) Media *Imagen;
@property (strong) NSDecimalNumber *Precio;
@property (strong) NSString *AbreviaturaMoneda;
@property (strong) NSDecimalNumber *Descuento;
@property (assign) BOOL Recomendada;
@property (assign) int Filas;
@property (assign) int Columnas;

+ (ProductoDestacado *)deserializeNode:(xmlNodePtr)cur;

@end
