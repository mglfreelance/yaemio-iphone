//
//  EditarGrupoNotificacionViewController.h
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "YaesmioViewControllers.h"
#import "GrupoNotificacion.h"

@interface EditarGrupoNotificacionViewController : UIViewController

@property (strong) GrupoNotificacion *Grupo;

@end
