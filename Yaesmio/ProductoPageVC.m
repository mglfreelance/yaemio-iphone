//
//  ProductoPageVC.m
//  Yaesmio
//
//  Created by freelance on 05/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ProductoPageVC.h"

@interface ProductoPageVC ()

@end

@implementation ProductoPageVC

@synthesize ElementoCarrito = _ElementoCarrito;

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.view.backgroundColor isEqual:[Locator Default].Color.Transparente] == false )
    {
        self.view.backgroundColor = [Locator Default].Color.Transparente;
        [self RellenarPantallaConDatosProducto];
    }
}

- (void)viewUnload
{
    [self setProducto:nil];
    [self setElementoCarrito:nil];
    [self setNombrePage:nil];
    [self setDelegate:nil];
}

- (void)RellenarPantallaConDatosProducto
{
}

-(void) Actualizar
{
    [self RellenarPantallaConDatosProducto];
}

- (BOOL)esModificarProduto
{
    return (self.ElementoCarrito != nil);
}

- (void)setElementoCarrito:(ElementoCarrito *)pElementoCarrito
{
    _ElementoCarrito = pElementoCarrito;
    
    if (pElementoCarrito != nil)
    {
        self.Producto = pElementoCarrito.Producto;
    }
}

- (ElementoCarrito *)ElementoCarrito
{
    return _ElementoCarrito;
}

-(int) UnidadesSeleccionadas
{
    if(self.esModificarProduto)
        return self.ElementoCarrito.Unidades.intValue;
    
    return 1;
}

@end
