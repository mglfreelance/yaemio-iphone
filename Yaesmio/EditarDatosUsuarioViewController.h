//
//  EditarDatosUsuarioViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 26/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Usuario.h"
#import "YaesmioViewControllers.h"

@interface EditarDatosUsuarioViewController : YaesmioViewController

@property (strong) Usuario *usuario;
@property (assign) BOOL esModal;
@property (copy)   ResultadoPorDefectoBlock Resultado;

@end
