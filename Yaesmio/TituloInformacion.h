//
//  TituloInformacion.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SoapObject.h"

@interface TituloInformacion : SoapObject

+ (TituloInformacion *)deserializeNode:(xmlNodePtr)cur;


/* elements */
@property (strong) NSString *IdTitulo;
@property (strong) NSString *Titulo;


@end