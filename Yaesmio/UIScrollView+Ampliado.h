//
//  UIScrollView+Ampliado.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewAmpliado.h"

@interface UIScrollView (Ampliado)

- (void)setContentSizeForSubviewUntilControl:(UIView*)pControl;
- (void)setContentSizeForSubviewUntilControl:(UIView*)pControl AddSize:(CGSize)pSize;
- (void)setWidthContentSizeForSubview;
- (void)setWidthContentSizeForSubviewAddSize:(float)pWidth;
- (void)setHeightContentSizeForSubviewUntilControl:(UIView *)pControl;
- (void)setHeightContentSizeForSubviewUntilControl:(UIView *)pControl AddSize:(CGSize)pSize;
- (void)setHeightContentSizeForSubviewUntilControl:(UIView *)pControl AddHeight:(float)pSize;
- (CGSize)getSubViewsContenSizeUntilControl:(UIView*)pControl;
@end
