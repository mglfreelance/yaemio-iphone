//
//  CondicionesUsoViewController.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 13/05/13.
//  Copyright (c) 2013 Draconis Software. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YaesmioViewControllers.h"

@interface CondicionesUsoViewController : YaesmioViewController

@property (nonatomic) NSString *NombreUsuario;
@property (nonatomic) NSString *PasswordUsuario;
@property (copy)      ResultadoPorDefectoBlock Resultado;
@property (nonatomic) UIViewController *VistaInicial;

@end
