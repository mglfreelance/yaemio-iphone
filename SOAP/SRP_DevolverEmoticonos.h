//
//  SRP_DevolverEmoticonos.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SRP_DevolverEmoticonos : SoapMethod

+ (SRP_DevolverEmoticonos *)deserializeNode:(xmlNodePtr)cur;

@end


@interface SRP_DevolverEmoticonosResponse : SoapMethod

+ (SRP_DevolverEmoticonosResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (readonly) NSArray * ListaEmoticonos;

@end
