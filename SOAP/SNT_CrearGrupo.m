//
//  SNT_CrearGrupo.m
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_CrearGrupo.h"

@implementation SNT_CrearGrupo

- (id)init
{
    if ((self = [super init]))
    {
        self.NombreGrupo = @"";
        self.ListaIdContactos = nil;
    }
    
    return self;
}

+ (SNT_CrearGrupo *)deserializeNode:(xmlNodePtr)cur
{
    SNT_CrearGrupo *newObject = [SNT_CrearGrupo new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"grupos";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:[self getListaIds] WithKey:@"IdContactos"];
    [self.ParametrosLLamada addObject:self.NombreGrupo WithKey:@"NombreGrupo"];
    
    return true;
}

-(ArrayOfSoapObject*) getListaIds
{
    ArrayOfSoapObject * resul = [ArrayOfSoapObject new];
    resul.Name = @"IdContactos";
    resul.ChilldnsPrefix = @"tns1";
    resul.nsPrefix = @"ServicioRegistroSvc";
    
    for (NSString *item in self.ListaIdContactos)
    {
        [resul addObject:item WithKey:@"string"];
    }

    return resul;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"CrearGrupo"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/CrearGrupo";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_CrearGrupoResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"CrearGrupoResponse";
}

@end


@implementation SNT_CrearGrupoResponse

+ (SNT_CrearGrupoResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_CrearGrupoResponse *newObject = [SNT_CrearGrupoResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"CrearGrupoResult";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"Grupo"])
    {
        self.Grupo = [GrupoNotificacion deserializeNode:pobjCur];
    }
}

@end
