//
//  ServicioRegistro_ObtenerDatosDireccionPrincipalUsuario.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"
#import "Direccion.h"

@interface SRM_ObtenerDatosDireccionPrincipalUsuario : SoapMethod
{
}

+ (SRM_ObtenerDatosDireccionPrincipalUsuario *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * IdUsuario;

@end

@interface SRM_ObtenerDatosDireccionPrincipalUsuarioResponse : SoapMethod
{
}

+ (SRM_ObtenerDatosDireccionPrincipalUsuarioResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) Direccion * Direccion;

@end
