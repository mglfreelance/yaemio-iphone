//
//  SNT_EliminarGrupo.h
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SNT_EliminarGrupo : SoapGenericMethod

+ (SNT_EliminarGrupo *)			deserializeNode:(xmlNodePtr)cur;

@property (strong) NSString *IdGrupo;

@end


@interface SNT_EliminarGrupoResponse : SoapGenericMethod

+ (SNT_EliminarGrupoResponse *)	deserializeNode:(xmlNodePtr)cur;

@end
