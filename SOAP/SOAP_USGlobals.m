#import "SOAP_USGlobals.h"

SOAP_USGlobals *mSharedInstance = nil;

@implementation SOAP_USGlobals

@synthesize wsdlStandardNamespaces;

+ (SOAP_USGlobals *)sharedInstance
{
	if(mSharedInstance == nil) {
		mSharedInstance = [[SOAP_USGlobals alloc] init];
	}
	
	return mSharedInstance;
}

- (id)init
{
	if((self = [super init])) {
		wsdlStandardNamespaces = [[NSMutableDictionary alloc] init];
	}
	
	return self;
}

@end
