#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "SOAP_USGlobals.h"
#import "SoapObject.h"

@interface ArrayOfSoapObject : SoapObject

+ (ArrayOfSoapObject *) deserializeNode:(xmlNodePtr)cur;

- (void)				addObject:(id)pObject WithKey:(NSString *)pKey;
- (void)				addInt:(int)pNumero WithKey:(NSString *)pKey;
- (void)				addBool:(BOOL)pNumero WithKey:(NSString *)pKey;
- (void)				addFloat:(float)pNumero WithKey:(NSString *)pKey;

@property (strong) NSString *ChilldnsPrefix;
@property (strong) NSString *Name;

@end
