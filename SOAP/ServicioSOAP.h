#import <Foundation/Foundation.h>
#import "USAdditions.h"
#import <libxml/tree.h>
#import "SOAP_USGlobals.h"
#import <libxml/parser.h>
#import "SoapObject.h"
#import "ArrayOfSoapObject.h"

@class ServicioSoapBindingResponse;
@class ServicioRegistroBindingOperation;

@protocol SoapBindingResponseDelegate <NSObject>
- (void)operation:(ServicioRegistroBindingOperation *)operation completedWithResponse:(ServicioSoapBindingResponse *)response;
@end


@interface ServicioSOAP : NSObject <SoapBindingResponseDelegate>
{
    NSURL *address;
    NSTimeInterval defaultTimeout;
    NSMutableArray *cookies;
    BOOL logXMLInOut;
    BOOL synchronousOperationComplete;
    NSString *authUsername;
    NSString *authPassword;
}
@property (copy) NSURL *address;
@property (assign) BOOL logXMLInOut;
@property (assign) NSTimeInterval defaultTimeout;
@property (nonatomic, strong) NSMutableArray *cookies;
@property (nonatomic, strong) NSString *authUsername;
@property (nonatomic, strong) NSString *authPassword;

// se agregaran a la cabecera httpRequest.
@property (nonatomic, strong) NSString *HeaderIdioma;
@property (nonatomic, strong) NSString *HeaderLatitud;
@property (nonatomic, strong) NSString *HeaderLongitud;
@property (nonatomic, strong) NSString *HeaderGps; //obsoleto
@property (nonatomic, strong) NSString *HeaderSecurityToken;
@property (nonatomic, strong) NSString *HeaderIP;
@property (nonatomic, strong) NSString *HeaderIdUsuario;
@property (nonatomic, strong) NSString *HeaderPlataforma;
@property (nonatomic, strong) NSString *HeaderIdApp;
@property (nonatomic, strong) NSString *HeaderMoneda;
@property (nonatomic, strong) NSString *HeaderVersionApp;

- (id)									initWithAddress:(NSString *)anAddress;

- (void)								sendHTTPCallUsingBody:(NSString *)body soapAction:(NSString *)soapAction forOperation:(ServicioRegistroBindingOperation *)operation;

- (void)								addCookie:(NSHTTPCookie *)toAdd;

- (void)								CallMethodAsyncUsingParameters:(SoapObject *)aParameters delegate:(id <SoapBindingResponseDelegate> )responseDelegate;

@end

@interface ServicioRegistroBindingOperation : NSOperation
{
}

@property (strong) ServicioSOAP *binding;
@property (readonly) ServicioSoapBindingResponse *response;
@property (nonatomic) id <SoapBindingResponseDelegate> delegate;
@property (nonatomic, strong) NSMutableData *responseData;
@property (nonatomic, strong) NSURLConnection *urlConnection;
@property (strong) SoapObject *parameters;

- (id)									initWithBinding:(ServicioSOAP *)aBinding delegate:(id <SoapBindingResponseDelegate> )aDelegate parameters:(SoapObject *)param;

@end

@interface ServicioRegistroBinding_envelope : NSObject
{
}
+ (ServicioRegistroBinding_envelope *)	sharedInstance;
- (NSString *)							serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements;
@end


@interface ServicioSoapBindingResponse : NSObject
{
}

@property (strong) NSArray *headers;
@property (strong) NSArray *bodyParts;
@property (strong) NSError *error;

- (id)									getResultObjectForClass:(Class)pClass;

@end


typedef void (^SoapMethodResponseBlock)(ServicioSoapBindingResponse *pResponse);

@interface SoapMethod : SoapObject <SoapBindingResponseDelegate>

@property (copy) SoapMethodResponseBlock SoapMethodResponse;

- (NSString *)							NameMetohd; // nombre del metodo a llamar

- (NSString *)							getSoapAction;

- (const xmlChar *)						getMethodResponse;

@end


typedef NS_ENUM (NSInteger, SoapGenericMethodResponse)
{
    SoapGenericMethodResponseOK       = 0, // todo OK
    SoapGenericMethodResponseError    = 4, // Error de Sap
    SoapGenericMethodResponseWarning  = 8, // Warning
    SoapGenericMethodResponseErrorSer = 3, // Error del servicio
};

@interface SoapGenericMethod : SoapMethod

//Entrada
@property (strong) NSString *NameParametrosLLamada;
@property (strong, readonly) ArrayOfSoapObject *ParametrosLLamada;

//Salida
@property (assign) SoapGenericMethodResponse RespuestaCodigoRetorno;
@property (strong) NSString *RespuestaMensajeSap;

- (NSString *)							getNameResult;

@end
