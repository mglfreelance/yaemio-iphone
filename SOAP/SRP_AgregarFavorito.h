//
//  SRP_AgregarFavorito.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 20/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRP_AgregarFavorito : SoapGenericMethod

+ (SRP_AgregarFavorito *)			deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *IdUsuario;
@property (strong) NSString *IdProducto;
@property (assign) float Latitud;
@property (assign) float Longitud;
@property (strong) NSString *Medio;

@end

@interface SRP_AgregarFavoritoResponse : SoapGenericMethod

+ (SRP_AgregarFavoritoResponse *)	deserializeNode:(xmlNodePtr)cur;

@end
