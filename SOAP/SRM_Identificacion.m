//
//  ServicioRegistro_Identificacion.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 24/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_Identificacion.h"

@implementation SRM_Identificacion
- (id)init
{
    if ((self = [super init]))
    {
        self.email = @"";
        self.password = @"";
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    if (self.email != 0)
    {
        xmlAddChild(node, [self.email xmlNodeForDoc:node->doc elementName:@"eMail" elementNSPrefix:@"ServicioRegistroSvc"]);
    }
    
    if (self.password != 0)
    {
        xmlAddChild(node, [self.password xmlNodeForDoc:node->doc elementName:@"password" elementNSPrefix:@"ServicioRegistroSvc"]);
    }
}

+ (SRM_Identificacion *)deserializeNode:(xmlNodePtr)cur
{
    SRM_Identificacion *newObject = [SRM_Identificacion new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"eMail"])
    {
        self.email = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"password"])
    {
        self.password = [NSString deserializeNode:pobjCur];
    }
}

#pragma mark - BindingOperation

- (void)getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"IdentificacionV2"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/IdentificacionV2";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_IdentificacionResponse deserializeNode:bodyNode AndHeader:headerNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"IdentificacionV2Response";
}

@end




@implementation SRM_IdentificacionResponse

- (id)init
{
    if ((self = [super init]))
    {
        self.CodeResult = 0;
        self.Response   = nil;
    }
    
    return self;
}


+ (SRM_IdentificacionResponse *)deserializeNode:(xmlNodePtr)cur AndHeader:(xmlNodePtr)header
{
    SRM_IdentificacionResponse *newObject = [SRM_IdentificacionResponse new];
    
    [newObject deserializeElementsFromNode:cur];
    [newObject deserializeHeaderFromNode:header];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"IdentificacionV2Result"])
    {
        self.Response = [LoginUsuarioResponse deserializeNode:pobjCur];
        //vuelvo a desserializar el objeto
        [self deserializeElementsFromNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"CodigoRetorno"])
    {
        self.CodeResult = [[NSString deserializeNode:pobjCur] intValue];
    }
}

- (void)ElementForHeaderNode:(NSString*)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"SecurityToken"])
    {
        self.Response.SecurityToken = [NSString deserializeNode:pobjCur];
    }
}

@end
