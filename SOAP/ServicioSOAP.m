#import "ServicioSOAP.h"
#import <libxml/xmlstring.h>
#if TARGET_OS_IPHONE
#import <CFNetwork/CFNetwork.h>
#endif


@implementation ServicioSOAP

@synthesize address;
@synthesize defaultTimeout;
@synthesize logXMLInOut;
@synthesize cookies;
@synthesize authUsername;
@synthesize authPassword;

static NSString *mHeaderIdioma;
static NSString *mHeaderLatitud;
static NSString *mHeaderLongitud;
static NSString *mHeaderGps;
static NSString *mHeaderSecurityToken;
static NSString *mHeaderIdUsuario;
static NSString *mHeaderIdApp;
static NSString *mHeaderMoneda;
static NSString *mHeaderVersionApp;

- (id)init
{
    if ((self = [super init]))
    {
        address = nil;
        cookies = nil;
        defaultTimeout = 10;                //seconds
        logXMLInOut = NO;
        synchronousOperationComplete = NO;
    }
    
    return self;
}

- (id)initWithAddress:(NSString *)anAddress
{
    if ((self = [self init]))
    {
        self.address = [NSURL URLWithString:anAddress];
    }
    
    return self;
}

- (void)addCookie:(NSHTTPCookie *)toAdd
{
    if (toAdd != nil)
    {
        if (cookies == nil)
        {
            cookies = [[NSMutableArray alloc] init];
        }
        
        [cookies addObject:toAdd];
    }
}

- (ServicioSoapBindingResponse *)performSynchronousOperation:(ServicioRegistroBindingOperation *)operation
{
    synchronousOperationComplete = NO;
    [operation start];
    
    // Now wait for response
    NSRunLoop *theRL = [NSRunLoop currentRunLoop];
    
    while (!synchronousOperationComplete && [theRL runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]])
        ;
    return operation.response;
}

- (void)performAsynchronousOperation:(ServicioRegistroBindingOperation *)operation
{
    [operation start];
}

- (void)operation:(ServicioRegistroBindingOperation *)operation completedWithResponse:(ServicioSoapBindingResponse *)response
{
    synchronousOperationComplete = YES;
}

- (void)CallMethodAsyncUsingParameters:(SoapObject *)aParameters delegate:(id <SoapBindingResponseDelegate> )responseDelegate
{
    [self performAsynchronousOperation:[(ServicioRegistroBindingOperation *)[ServicioRegistroBindingOperation alloc] initWithBinding : self delegate : responseDelegate parameters : aParameters]];
}

- (void)sendHTTPCallUsingBody:(NSString *)outputBody soapAction:(NSString *)soapAction forOperation:(ServicioRegistroBindingOperation *)operation
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.address cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:self.defaultTimeout];
    
    //campos que se agregaran a la cabecera HTTP
    [self agregarACabezeraHTTPSiExisteDato:request DelCampo:@"Idioma" ConValor:self.HeaderIdioma];
    [self agregarACabezeraHTTPSiExisteDato:request DelCampo:@"Latitud" ConValor:self.HeaderLatitud];
    [self agregarACabezeraHTTPSiExisteDato:request DelCampo:@"Longitud" ConValor:self.HeaderLongitud];
    [self agregarACabezeraHTTPSiExisteDato:request DelCampo:@"Gps" ConValor:self.HeaderGps];
    [self agregarACabezeraHTTPSiExisteDato:request DelCampo:@"SecurityToken" ConValor:self.HeaderSecurityToken];
    [self agregarACabezeraHTTPSiExisteDato:request DelCampo:@"IP" ConValor:self.HeaderIP];
    [self agregarACabezeraHTTPSiExisteDato:request DelCampo:@"UsuarioId" ConValor:self.HeaderIdUsuario];
    [self agregarACabezeraHTTPSiExisteDato:request DelCampo:@"Plataforma" ConValor:self.HeaderPlataforma];
    [self agregarACabezeraHTTPSiExisteDato:request DelCampo:@"App" ConValor:self.HeaderIdApp];
    [self agregarACabezeraHTTPSiExisteDato:request DelCampo:@"Moneda" ConValor:self.HeaderMoneda];
    [self agregarACabezeraHTTPSiExisteDato:request DelCampo:@"VersionApp" ConValor:self.HeaderVersionApp];
    
    NSData *bodyData = [outputBody dataUsingEncoding:NSUTF8StringEncoding];
    
    if (cookies != nil)
    {
        [request setAllHTTPHeaderFields:[NSHTTPCookie requestHeaderFieldsWithCookies:cookies]];
    }
    
    [request setValue:@"wsd2objc" forHTTPHeaderField:@"User-Agent"];
    [request setValue:soapAction forHTTPHeaderField:@"SOAPAction"];
    [request setValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [request setValue:[NSString stringWithFormat:@"%u", (int)[bodyData length]] forHTTPHeaderField:@"Content-Length"];
    
    if (self.address.port == NULL || self.address.port == 0)
    {
        [request setValue:[NSString stringWithFormat:@"%@", self.address.host] forHTTPHeaderField:@"Host"];
    }
    else
    {
        [request setValue:[NSString stringWithFormat:@"%@:%@", self.address.host, self.address.port] forHTTPHeaderField:@"Host"];
    }
    
    [request setHTTPMethod:@"POST"];
    // set version 1.1 - how?
    [request setHTTPBody:bodyData];
    
    if (self.logXMLInOut)
    {
        NSLog(@"OutputHeaders:\n%@", [request allHTTPHeaderFields]);
        NSLog(@"OutputBody:\n%@", outputBody);
    }
    
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:operation];
    
    operation.urlConnection = connection;
}

- (void)agregarACabezeraHTTPSiExisteDato:(NSMutableURLRequest *)request DelCampo:(NSString *)pCampo ConValor:(NSString *)pValor
{
    if (pValor != nil && [pValor isEqualToString:@""] == false)
    {
        [request setValue:pValor forHTTPHeaderField:pCampo];
    }
}

- (NSString *)HeaderIdioma
{
    return mHeaderIdioma;
}

- (void)setHeaderIdioma:(NSString *)headerIdioma
{
    mHeaderIdioma  = headerIdioma;
}

- (NSString *)HeaderLatitud
{
    return mHeaderLatitud;
}

- (void)setHeaderLatitud:(NSString *)HeaderLatitud
{
    mHeaderLatitud = HeaderLatitud;
}

- (NSString *)HeaderLongitud
{
    return mHeaderLongitud;
}

- (void)setHeaderLongitud:(NSString *)HeaderLongitud
{
    mHeaderLongitud = HeaderLongitud;
}

- (NSString *)HeaderGps
{
    return mHeaderGps;
}

- (void)setHeaderGps:(NSString *)HeaderGps
{
    mHeaderGps = HeaderGps;
}

- (NSString *)HeaderSecurityToken
{
    return mHeaderSecurityToken;
}

- (void)setHeaderSecurityToken:(NSString *)HeaderSecurityToken
{
    mHeaderSecurityToken = HeaderSecurityToken;
}

- (NSString *)HeaderIdUsuario
{
    return mHeaderIdUsuario;
}

- (void)setHeaderIdUsuario:(NSString *)HeaderIdUsuario
{
    mHeaderIdUsuario = HeaderIdUsuario;
}

- (NSString *)HeaderIdApp
{
    return mHeaderIdApp;
}

- (void)setHeaderIdApp:(NSString *)HeaderIdApp
{
    mHeaderIdApp = HeaderIdApp;
}

- (NSString *)HeaderMoneda
{
    return mHeaderMoneda;
}

- (void)setHeaderMoneda:(NSString *)HeaderMoneda
{
    mHeaderMoneda = HeaderMoneda;
}

-(NSString*)HeaderVersionApp
{
    return mHeaderVersionApp;
}

-(void)setHeaderVersionApp:(NSString *)HeaderVersionApp
{
    mHeaderVersionApp = HeaderVersionApp;
}


@end

@implementation ServicioRegistroBindingOperation

- (id)initWithBinding:(ServicioSOAP *)aBinding delegate:(id <SoapBindingResponseDelegate> )aDelegate parameters:(SoapObject *)param
{
    if ((self = [super init]))
    {
        self.binding = aBinding;
        _response = nil;
        self.delegate = aDelegate;
        self.responseData = nil;
        self.urlConnection = nil;
        self.parameters = param;
    }
    
    return self;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    if ([challenge previousFailureCount] == 0)
    {
        NSURLCredential *newCredential;
        newCredential = [NSURLCredential credentialWithUser:self.binding.authUsername
                                                  password	:self.binding.authPassword
                                               persistence :NSURLCredentialPersistenceForSession];
        [[challenge sender] useCredential:newCredential
               forAuthenticationChallenge:challenge];
    }
    else
    {
        [[challenge sender] cancelAuthenticationChallenge:challenge];
        NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"Authentication Error" forKey:NSLocalizedDescriptionKey];
        NSError *authError = [NSError errorWithDomain:@"Connection Authentication" code:0 userInfo:userInfo];
        [self connection:connection didFailWithError:authError];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)urlResponse
{
    NSHTTPURLResponse *httpResponse;
    
    if ([urlResponse isKindOfClass:[NSHTTPURLResponse class]])
    {
        httpResponse = (NSHTTPURLResponse *)urlResponse;
    }
    else
    {
        httpResponse = nil;
    }
    
    if (self.binding.logXMLInOut)
    {
        NSLog(@"ResponseStatus: %u\n", (int)[httpResponse statusCode]);
        NSLog(@"ResponseHeaders:\n%@", [httpResponse allHeaderFields]);
    }
    
    NSMutableArray *cookies = [[NSHTTPCookie cookiesWithResponseHeaderFields:[httpResponse allHeaderFields] forURL:self.binding.address] mutableCopy];
    
    self.binding.cookies = cookies;
    
    if ([urlResponse.MIMEType rangeOfString:@"text/xml"].length == 0)
    {
        NSError *error = nil;
        [connection cancel];
        
        if ([httpResponse statusCode] >= 400)
        {
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:[NSHTTPURLResponse localizedStringForStatusCode:[httpResponse statusCode]] forKey:NSLocalizedDescriptionKey];
            
            error = [NSError errorWithDomain:@"BasicHttpBinding_IServicioRegistroBindingResponseHTTP" code:[httpResponse statusCode] userInfo:userInfo];
        }
        else
        {
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:
                                      [NSString stringWithFormat:@"Unexpected response MIME type to SOAP call:%@", urlResponse.MIMEType]
                                                                 forKey:NSLocalizedDescriptionKey];
            error = [NSError errorWithDomain:@"BasicHttpBinding_IServicioRegistroBindingResponseHTTP" code:1 userInfo:userInfo];
        }
        
        [self connection:connection didFailWithError:error];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (self.responseData == nil)
    {
        self.responseData = [data mutableCopy];
    }
    else
    {
        [self.responseData appendData:data];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if (self.binding.logXMLInOut)
    {
        NSLog(@"ResponseError:\n%@", error);
    }
    
    self.response.error = error;
    [self.delegate operation:self completedWithResponse:self.response];
}

- (void)dealloc
{
    self.delegate = nil;
}

- (void)main
{
    _response = [ServicioSoapBindingResponse new];
    
    ServicioRegistroBinding_envelope *envelope = [ServicioRegistroBinding_envelope sharedInstance];
    
    NSMutableDictionary *headerElements = nil;
    headerElements = [NSMutableDictionary dictionary];
    [self.parameters getHeaderElements:headerElements];
    
    NSMutableDictionary *bodyElements = nil;
    bodyElements = [NSMutableDictionary dictionary];
    [self.parameters getBodyElements:bodyElements];
    
    NSString *operationXMLString = [envelope serializedFormUsingHeaderElements:headerElements bodyElements:bodyElements];
    
    [self.binding sendHTTPCallUsingBody:operationXMLString soapAction:[self.parameters getSoapAction] forOperation:self];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
#ifdef SHOW_DEBUG_SOAP
    NSLog(@"soap response: %@", [NSDate new]);
#endif
    
    if (self.responseData != nil && self.delegate != nil)
    {
        xmlDocPtr doc;
        xmlNodePtr cur;
        
        if (self.binding.logXMLInOut)
        {
            NSLog(@"ResponseBody:\n%@", [[NSString alloc] initWithData:self.responseData encoding:NSUTF8StringEncoding]);
        }
        
        doc = xmlParseMemory([self.responseData bytes], (int)[self.responseData length]);
        
        if (doc == NULL)
        {
            NSDictionary *userInfo = [NSDictionary dictionaryWithObject:@"Errors while parsing returned XML" forKey:NSLocalizedDescriptionKey];
            
            self.response.error = [NSError errorWithDomain:@"BasicHttpBinding_IServicioRegistroBindingResponseXML" code:1 userInfo:userInfo];
            [self.delegate operation:self completedWithResponse:self.response];
        }
        else
        {
            cur = xmlDocGetRootElement(doc);
            cur = cur->children;
            xmlNodePtr HeaderNode = nil;
            
            for (; cur != NULL; cur = cur->next)
            {
                if (cur->type == XML_ELEMENT_NODE)
                {
                    if (xmlStrEqual(cur->name, (const xmlChar *)"Header"))
                    {
                        HeaderNode = cur;
                    }
                    else if (xmlStrEqual(cur->name, (const xmlChar *)"Body"))
                    {
                        NSMutableArray *responseBodyParts = [NSMutableArray array];
                        
                        xmlNodePtr bodyNode;
                        
                        for (bodyNode = cur->children; bodyNode != NULL; bodyNode = bodyNode->next)
                        {
                            if (cur->type == XML_ELEMENT_NODE)
                            {
                                if (xmlStrEqual(bodyNode->name, (const xmlChar *)[self.parameters getMethodResponse]))
                                {
                                    SoapObject *bodyObject = [self.parameters CreateResponse:HeaderNode AndBody:bodyNode];
                                    
                                    if (bodyObject != nil)
                                    {
                                        [responseBodyParts addObject:bodyObject];
                                    }
                                }
                                
                                if (xmlStrEqual(bodyNode->ns->prefix, cur->ns->prefix) &&
                                    xmlStrEqual(bodyNode->name, (const xmlChar *)"Fault"))
                                {
                                    SOAP_Fault *bodyObject = [SOAP_Fault deserializeNode:bodyNode];
                                    
                                    if (bodyObject != nil)
                                    {
                                        [responseBodyParts addObject:bodyObject];
                                        self.response.error =  [[NSError alloc] initWithDomain:bodyObject.faultcode code:-1 userInfo:nil];
                                    }
                                }
                            }
                        }
                        
                        self.response.bodyParts = responseBodyParts;
                    }
                }
            }
            
            xmlFreeDoc(doc);
        }
        
        xmlCleanupParser();
        [self.delegate operation:self completedWithResponse:self.response];
    }
}

@end


static ServicioRegistroBinding_envelope *BasicHttpBinding_IServicioRegistroBindingSharedEnvelopeInstance = nil;
@implementation ServicioRegistroBinding_envelope
+ (ServicioRegistroBinding_envelope *)sharedInstance
{
    if (BasicHttpBinding_IServicioRegistroBindingSharedEnvelopeInstance == nil)
    {
        BasicHttpBinding_IServicioRegistroBindingSharedEnvelopeInstance = [ServicioRegistroBinding_envelope new];
    }
    
    return BasicHttpBinding_IServicioRegistroBindingSharedEnvelopeInstance;
}

- (NSString *)serializedFormUsingHeaderElements:(NSDictionary *)headerElements bodyElements:(NSDictionary *)bodyElements
{
    xmlDocPtr doc;
    
    doc = xmlNewDoc((const xmlChar *)XML_DEFAULT_VERSION);
    
    if (doc == NULL)
    {
        NSLog(@"Error creating the xml document tree");
        return @"";
    }
    
    xmlNodePtr root = xmlNewDocNode(doc, NULL, (const xmlChar *)"Envelope", NULL);
    xmlDocSetRootElement(doc, root);
    
    xmlNsPtr soapEnvelopeNs = xmlNewNs(root, (const xmlChar *)"http://schemas.xmlsoap.org/soap/envelope/", (const xmlChar *)"soap");
    xmlSetNs(root, soapEnvelopeNs);
    
    xmlNsPtr xslNs = xmlNewNs(root, (const xmlChar *)"http://www.w3.org/1999/XSL/Transform", (const xmlChar *)"xsl");
    xmlNewNs(root, (const xmlChar *)"http://www.w3.org/2001/XMLSchema-instance", (const xmlChar *)"xsi");
    
    xmlNewNsProp(root, xslNs, (const xmlChar *)"version", (const xmlChar *)"1.0");
    
    xmlNewNs(root, (const xmlChar *)"http://www.w3.org/2001/XMLSchema", (const xmlChar *)"xs");
    xmlNewNs(root, (const xmlChar *)"http://tempuri.org/", (const xmlChar *)"ServicioRegistroSvc");
    xmlNewNs(root, (const xmlChar *)"http://tempuri.org/Imports", (const xmlChar *)"ns1");
    xmlNewNs(root, (const xmlChar *)"http://schemas.microsoft.com/2003/10/Serialization/Arrays", (const xmlChar *)"tns1");
    xmlNewNs(root, (const xmlChar *)"http://schemas.microsoft.com/2003/10/Serialization/", (const xmlChar *)"tns2");
    xmlNewNs(root, (const xmlChar *)"http://schemas.datacontract.org/2004/07/Yaesmio.Servicios.Entidades", (const xmlChar *)"tns3");
    xmlNewNs(root, (const xmlChar *)"http://schemas.datacontract.org/2004/07/Yaesmio.Servicios.Entidades.Parametros", (const xmlChar *)"tnsParametros");
    xmlNewNs(root, (const xmlChar *)"http://schemas.datacontract.org/2004/07/Yaesmio.Entidades.Yaesmio", (const xmlChar *)"yaes");
    xmlNewNs(root, (const xmlChar *)"http://schemas.datacontract.org/2004/07/Yaesmio.Entidades.Marcas", (const xmlChar *)"marcas");
    xmlNewNs(root, (const xmlChar *)"http://schemas.datacontract.org/2004/07/Yaesmio.Servicios.Entidades.Yaesmio.Productos", (const xmlChar *)"productos");
    xmlNewNs(root, (const xmlChar *)"http://schemas.datacontract.org/2004/07/Yaesmio.Entidades.Notificaciones", (const xmlChar *)"notificaciones");
    xmlNewNs(root, (const xmlChar *)"http://schemas.datacontract.org/2004/07/Yaesmio.Entidades.Notificaciones.Grupos", (const xmlChar *)"grupos");
    xmlNewNs(root, (const xmlChar *)"http://schemas.datacontract.org/2004/07/Yaesmio.Entidades.Media", (const xmlChar *)"yaesMedia");
    
    
    
    if ((headerElements != nil) && ([headerElements count] > 0))
    {
        xmlNodePtr headerNode = xmlNewDocNode(doc, soapEnvelopeNs, (const xmlChar *)"Header", NULL);
        xmlAddChild(root, headerNode);
        
        for (NSString *key in[headerElements allKeys])
        {
            id header = [headerElements objectForKey:key];
            xmlAddChild(headerNode, [header xmlNodeForDoc:doc elementName:key elementNSPrefix:nil]);
        }
    }
    
    if ((bodyElements != nil) && ([bodyElements count] > 0))
    {
        xmlNodePtr bodyNode = xmlNewDocNode(doc, soapEnvelopeNs, (const xmlChar *)"Body", NULL);
        xmlAddChild(root, bodyNode);
        
        for (NSString *key in[bodyElements allKeys])
        {
            id body = [bodyElements objectForKey:key];
            xmlAddChild(bodyNode, [body xmlNodeForDoc:doc elementName:key elementNSPrefix:nil]);
        }
    }
    
    xmlChar *buf;
    int size;
    xmlDocDumpFormatMemory(doc, &buf, &size, 1);
    
    NSString *serializedForm = [NSString stringWithCString:(const char *)buf encoding:NSUTF8StringEncoding];
    xmlFree(buf);
    
    xmlFreeDoc(doc);
    return serializedForm;
}

@end
@implementation ServicioSoapBindingResponse
@synthesize headers;
@synthesize bodyParts;
@synthesize error;

- (id)init
{
    if ((self = [super init]))
    {
        headers = nil;
        bodyParts = nil;
        error = nil;
    }
    
    return self;
}

- (id)getResultObjectForClass:(Class)pClass
{
    id lResult = nil;
    
    if (self.error == nil)
    {
        for (id tns_ in self.bodyParts)
        {
            if ([tns_ isKindOfClass:pClass])
            {
                lResult = tns_;
                break;                 // salida rapida
            }
        }
    }
    
    return lResult;
}

@end

@implementation SoapMethod

- (void)operation:(ServicioRegistroBindingOperation *)operation completedWithResponse:(ServicioSoapBindingResponse *)response
{
#ifdef SHOW_DEBUG_SOAP
    NSLog(@"soap finish: %@", [NSDate new]);
#endif
    
    if (self.SoapMethodResponse != nil)
    {
        self.SoapMethodResponse(response);
    }
}

- (NSString *)NameMetohd
{
    return @"";
}

- (NSString *)getSoapAction
{
    return @"";
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"";
}

@end

@implementation SoapGenericMethod

@synthesize ParametrosLLamada = _ParametrosLLamada;
@synthesize RespuestaCodigoRetorno = _RespuestaCodigoRetorno;
@synthesize RespuestaMensajeSap = _RespuestaMensajeSap;


- (instancetype)init
{
    if ((self = [super init]))
    {
        _RespuestaCodigoRetorno = SoapGenericMethodResponseError;
        _RespuestaMensajeSap = @"";
        self.NameParametrosLLamada = @"parametros";
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    if ([self agregateChildParametrosLLamada])
    {
        xmlAddChild(node, [self.ParametrosLLamada xmlNodeForDoc:node->doc elementName:self.ParametrosLLamada.Name elementNSPrefix:self.ParametrosLLamada.nsPrefix]);
    }
}

- (BOOL)agregateChildParametrosLLamada
{
    return false;
}

- (ArrayOfSoapObject *)ParametrosLLamada
{
    if (_ParametrosLLamada == nil)
    {
        _ParametrosLLamada = [ArrayOfSoapObject new];
    }
    
    return _ParametrosLLamada;
}

- (NSString *)getNameResult
{
    return @"";
}

- (void)deserializeElementsFromNode:(xmlNodePtr)cur
{
    for (cur = cur->children; cur != NULL; cur = cur->next)
    {
        if (cur->type == XML_ELEMENT_NODE)
        {
            NSString *Name = [NSString stringWithUTF8String:(char *)cur->name];
            
            if ([Name isEqualToString:[self getNameResult]])
            {
                //vuelvo a desserializar el objeto
                [self deserializeElementsFromNode:cur];
            }
            else if ([Name isEqualToString:@"CodigoRetorno"])
            {
                _RespuestaCodigoRetorno = [self DevolverCorrectamente:[NSString deserializeNode:cur].intValue];
            }
            else if ([Name isEqualToString:@"MensajeSap"])
            {
                _RespuestaMensajeSap = [NSString deserializeNode:cur];
            }
            else
            {
                [self ElementForNode:Name Node:cur];
            }
        }
    }
}

-(SoapGenericMethodResponse)DevolverCorrectamente:(int)pValor
{
    switch (pValor)
    {
        case 0:
            return SoapGenericMethodResponseOK;
            
        case 8:
            return SoapGenericMethodResponseWarning;
            
        case 3:
            return SoapGenericMethodResponseErrorSer;
            
        default:
             return SoapGenericMethodResponseError;
    }
}

@end
