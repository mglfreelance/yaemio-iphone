//
//  SNT_ModificarGrupo.h
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"
#import "GrupoNotificacion.h"

@interface SNT_ModificarGrupo : SoapGenericMethod

+ (SNT_ModificarGrupo *)		deserializeNode:(xmlNodePtr)cur;

@property (strong) GrupoNotificacion *Grupo;
@property (strong) NSString *IdUsuario;

@end


@interface SNT_ModificarGrupoResponse : SoapGenericMethod

+ (SNT_ModificarGrupoResponse *)deserializeNode:(xmlNodePtr)cur;

@end
