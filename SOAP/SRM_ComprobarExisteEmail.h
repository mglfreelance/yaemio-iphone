//
//  ServicioRegistro_ComprobarExisteEmail.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRM_ComprobarExisteEmail : SoapMethod
{
}

+ (SRM_ComprobarExisteEmail *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * email;

@end





@interface SRM_ComprobarExisteEmailResponse : SoapMethod
{
}

+ (SRM_ComprobarExisteEmailResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (assign) BOOL ComprobarExisteEmailResult;

@end
