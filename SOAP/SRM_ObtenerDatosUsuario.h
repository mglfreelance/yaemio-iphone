//
//  ServicioRegistro_ObtenerDatosUsuario.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"
#import "Usuario.h"

@interface SRM_ObtenerDatosUsuario : SoapMethod
{
}

+ (SRM_ObtenerDatosUsuario *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * IdUsuario;

@end

@interface SRM_ObtenerDatosUsuarioResponse : SoapMethod
{
}

+ (SRM_ObtenerDatosUsuarioResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) Usuario * ObtenerDatosUsuarioResult;

@end

