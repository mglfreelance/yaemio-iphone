//
//  SRP_GuardarCompras.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 17/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_GuardarCompras.h"

@implementation SRP_GuardarCompras

- (id)init
{
    if ((self = [super init]))
    {
        self.IdUsuario      = @"";
        self.Direccion      = nil;
        self.ListaProductos = nil;
    }
    
    return self;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"tnsParametros";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.Direccion WithKey:@"Direccion"];
    [self.ParametrosLLamada addObject:self.ListaProductos WithKey:@"ListaProductos"];
    [self.ParametrosLLamada addObject:self.IdUsuario WithKey:@"UsuarioId"];
    
    return true;
}

+ (SRP_GuardarCompras *)deserializeNode:(xmlNodePtr)cur
{
    SRP_GuardarCompras *newObject = [SRP_GuardarCompras new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"GuardarCompra"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IYaesmio/GuardarCompra";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_GuardarComprasResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"GuardarCompraResponse";
}

@end



@implementation SRP_GuardarComprasResponse


- (id)init
{
    if ((self = [super init]))
    {
        self.IDCarrito = nil;
    }
    
    return self;
}

+ (SRP_GuardarComprasResponse *)deserializeNode:(xmlNodePtr)cur
{
    SRP_GuardarComprasResponse *newObject = [SRP_GuardarComprasResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"GuardarCompraResult"])
    {
        NSArray *lista = [NSArray deserializeNode:pobjCur toClass:[NSString class]];
        
        if (lista != nil)
        {
            if (lista.count >= 1)
            {
                self.RespuestaMensajeSap = lista[0];
            }
            
            if (lista.count >= 2)
            {
                self.IDCarrito = lista[1];
                self.RespuestaCodigoRetorno = (self.RespuestaMensajeSap.isNotEmpty || self.IDCarrito.isNotEmpty ? SoapGenericMethodResponseOK :SoapGenericMethodResponseError);
            }
        }
    }
}

@end
