//
//  SRP_HistorialComprasUsuario.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRP_HistorialComprasUsuario : SoapGenericMethod

+ (SRP_HistorialComprasUsuario *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (assign) int       PosicionInicio;
@property (assign) int       NumeroRegistros;
@property (strong) NSString  *TipoProducto;

@end


@interface SRP_HistorialComprasUsuarioResponse : SoapGenericMethod

+ (SRP_HistorialComprasUsuarioResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (readonly) NSArray * ListaProductos;
@property (readonly) NSInteger NumeroElementos;
@end
