//
//  SRP_ObtenerDatosProducto.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 17/07/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ServicioSOAP.h"
#import "ProductoPadre.h"

@interface SRP_ObtenerDatosProductoPorQR : SoapGenericMethod

+ (SRP_ObtenerDatosProductoPorQR *)			deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *UrlProducto;

@end



@interface SRP_ObtenerDatosProductoPorQRResponse : SoapGenericMethod

+ (SRP_ObtenerDatosProductoPorQRResponse *) deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) ProductoPadre *ProductoPadreResponse;

@end
