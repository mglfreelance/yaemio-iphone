//
//  SRP_ActualizarCompras.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 17/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_ConfirmarCompra.h"

@implementation SRP_ConfirmarCompra

- (id)init
{
	if((self = [super init]))
    {
		self.Parametros = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Parametros xmlNodeForDoc:node->doc elementName:@"parametros" elementNSPrefix:@"ServicioRegistroSvc"]);
    
}

+ (SRP_ConfirmarCompra *)deserializeNode:(xmlNodePtr)cur
{
	SRP_ConfirmarCompra *newObject = [SRP_ConfirmarCompra new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark - BindingOperation

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ConfirmarCompra"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IYaesmio/ConfirmarCompra";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_ConfirmarCompraResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ConfirmarCompraResponse";
}

@end



@implementation SRP_ConfirmarCompraResponse

- (id)init
{
	if((self = [super init]))
    {
        self.ActualizarComprasResult = @"";
	}
	
	return self;
}

+ (SRP_ConfirmarCompraResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRP_ConfirmarCompraResponse *newObject = [SRP_ConfirmarCompraResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ConfirmarCompraResult"])
    {
        self.ActualizarComprasResult = [NSString deserializeNode:pobjCur];
    }
}

@end
