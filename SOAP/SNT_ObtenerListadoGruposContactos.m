//
//  SNT_ObtenerListadoGruposContactos.m
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_ObtenerListadoGruposContactos.h"
#import "GrupoNotificacion.h"

@implementation SNT_ObtenerListadoGruposContactos

- (id)init
{
    if ((self = [super init]))
    {
    }
    
    return self;
}

+ (SNT_ObtenerListadoGruposContactos *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ObtenerListadoGruposContactos *newObject = [SNT_ObtenerListadoGruposContactos new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerListadoGruposContactos"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/ObtenerListadoGruposContactos";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_ObtenerListadoGruposContactosResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ObtenerListadoGruposContactosResponse";
}

@end


@implementation SNT_ObtenerListadoGruposContactosResponse

+ (SNT_ObtenerListadoGruposContactosResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ObtenerListadoGruposContactosResponse *newObject = [SNT_ObtenerListadoGruposContactosResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ObtenerListadoGruposContactosResult";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"Grupos"])
    {
        self.ListaGrupos = [NSArray deserializeNode:pobjCur toClass:[GrupoNotificacion class]];
    }
}

@end
