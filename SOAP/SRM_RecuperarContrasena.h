//
//  ServicioRegistro_RecuperarContrasena.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 24/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRM_RecuperarContrasena : SoapMethod

+ (SRM_RecuperarContrasena *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * email;

@end

@interface SRM_RecuperarContrasenaResponse : SoapMethod

+ (SRM_RecuperarContrasenaResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSNumber * RecuperarContrasenaResult;

@end

