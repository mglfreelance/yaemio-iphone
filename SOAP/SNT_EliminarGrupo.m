//
//  SNT_EliminarGrupo.m
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_EliminarGrupo.h"

@implementation SNT_EliminarGrupo


- (id)init
{
    if ((self = [super init]))
    {
        self.IdGrupo = @"";
    }
    
    return self;
}

+ (SNT_EliminarGrupo *)deserializeNode:(xmlNodePtr)cur
{
    SNT_EliminarGrupo *newObject = [SNT_EliminarGrupo new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"grupos";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.IdGrupo WithKey:@"IdGrupo"];
    
    return true;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"EliminarGrupo"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/EliminarGrupo";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_EliminarGrupoResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"EliminarGrupoResponse";
}

@end


@implementation SNT_EliminarGrupoResponse

+ (SNT_EliminarGrupoResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_EliminarGrupoResponse *newObject = [SNT_EliminarGrupoResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"EliminarGrupoResult";
}

@end
