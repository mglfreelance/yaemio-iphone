//
//  SRP_EliminarFavorito.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 20/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_EliminarFavorito.h"

@implementation SRP_EliminarFavorito

- (id)init
{
	if((self = [super init]))
    {
		self.IdUsuario = @"";
        self.IdFavorito    = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.IdUsuario xmlNodeForDoc:node->doc elementName:@"usu" elementNSPrefix:@"ServicioRegistroSvc"]);
    
    xmlAddChild(node, [self.IdFavorito xmlNodeForDoc:node->doc elementName:@"idProducto" elementNSPrefix:@"ServicioRegistroSvc"]);
    
}

+ (SRP_EliminarFavorito *)deserializeNode:(xmlNodePtr)cur
{
	SRP_EliminarFavorito *newObject = [SRP_EliminarFavorito new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark - BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"BorrarFavorito"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IYaesmio/BorrarFavorito";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_EliminarFavoritoResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "BorrarFavoritoResponse";
}

@end


@implementation SRP_EliminarFavoritoResponse


- (id)init
{
	if((self = [super init]))
    {
		self.MensajeResultado = @"";
        self.esCorrecto       = false;
	}
	
	return self;
}

+ (SRP_EliminarFavoritoResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRP_EliminarFavoritoResponse *newObject = [SRP_EliminarFavoritoResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
    
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"BorrarFavoritoResult"])
    {
        NSArray *Valor = [NSArray deserializeNode:pobjCur toClass:[NSString class]];
        if (Valor.count >= 1)
        {
            self.esCorrecto = [Valor[0] isEqualToString:@"0"];
        }
        if (Valor.count >= 2)
        {
            self.MensajeResultado = Valor[1];
        }
    }
}

@end
