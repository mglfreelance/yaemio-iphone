//
//  SRP_ValorarProductoCompra.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ServicioSOAP.h"
#import "ParametrosGuardarPuntuacionCompra.h"

@interface SRP_ValorarProductoCompra : SoapMethod

+ (SRP_ValorarProductoCompra *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) ParametrosGuardarPuntuacionCompra *Parametros;

@end


@interface SRP_ValorarProductoCompraResponse : SoapMethod

+ (SRP_ValorarProductoCompraResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (readonly) NSString *Mensaje;
@property (readonly) int      Codigo;

@end
