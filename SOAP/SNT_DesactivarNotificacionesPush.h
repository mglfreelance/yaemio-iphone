//
//  SNT_DesactivarNotificacionesPush.h
//  Yaesmio
//
//  Created by freelance on 22/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SNT_DesactivarNotificacionesPush : SoapGenericMethod

+ (SNT_DesactivarNotificacionesPush *)			deserializeNode:(xmlNodePtr)cur;

@property (strong) NSString *IdUsuario;
@property (strong) NSString *IdPush;
@property (strong) NSString *MacDevice;
@property (strong) NSString *Telefono;

@end


@interface SNT_DesactivarNotificacionesPushResponse : SoapGenericMethod

+ (SNT_DesactivarNotificacionesPushResponse *)	deserializeNode:(xmlNodePtr)cur;

@end
