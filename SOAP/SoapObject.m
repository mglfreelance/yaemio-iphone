#import "SoapObject.h"

@implementation SoapObject


- (NSString *)NameMetohd
{
    return @"";
}

- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix
{
    NSString *nodeName = nil;
    
    if (elNSPrefix != nil && [elNSPrefix length] > 0)
    {
        nodeName = [NSString stringWithFormat:@"%@:%@", elNSPrefix, elName];
    }
    else
    {
        nodeName = [NSString stringWithFormat:@"%@:%@", @"ServicioRegistroSvc", elName];
    }
    
    xmlNodePtr node = xmlNewDocNode(doc, NULL, [nodeName xmlString], NULL);
    
    
    [self addAttributesToNode:node];
    
    [self addElementsToNode:node];
    
    return node;
}

- (void)addAttributesToNode:(xmlNodePtr)node
{
}

- (void)addElementsToNode:(xmlNodePtr)node
{
}

/* attributes */
- (NSDictionary *)attributes
{
    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
    
    return attributes;
}

+ (SoapObject *)deserializeNode:(xmlNodePtr)cur
{
    SoapObject *newObject = [SoapObject new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)deserializeAttributesFromNode:(xmlNodePtr)cur
{
}

- (void)deserializeElementsFromNode:(xmlNodePtr)cur
{
    for (cur = cur->children; cur != NULL; cur = cur->next)
    {
        if (cur->type == XML_ELEMENT_NODE)
        {
            NSString *Name = [NSString stringWithUTF8String:(char *)cur->name];
            [self ElementForNode:Name Node:cur];
        }
    }
}

- (void)deserializeHeaderFromNode:(xmlNodePtr)cur
{
    for (cur = cur->children; cur != NULL; cur = cur->next)
    {
        if (cur->type == XML_ELEMENT_NODE)
        {
            NSString *Name = [NSString stringWithUTF8String:(char *)cur->name];
            [self ElementForHeaderNode:Name Node:cur];
        }
    }
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
}

- (void)ElementForHeaderNode:(NSString*)nodeName Node:(xmlNodePtr)pobjCur
{
}

#pragma mark - Binding Operation

- (void)getHeaderElements:(NSMutableDictionary *)headerElements
{
}

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
}

- (NSString *)getSoapAction
{
    return @"";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return nil;
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"";
}

#pragma mark - NSCoding

- (void)encodeWithCoder:(NSCoder *)pobjCoder
{
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super init];
    
    if (self)
    {
        self.nsPrefix = @"";
    }
    
    return self;
}

- (NSString *)elementName
{
    return [[self class] description];
}

@end
