//
//  SRP_ObtenerDatosProducto.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 17/07/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_ObtenerDatosProductoPorID.h"

@implementation SRP_ObtenerDatosProductoPorID

- (id)init
{
    if ((self = [super init]))
    {
        self.Parametros = [ParametrosObtenerPorCodigo new];
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Parametros xmlNodeForDoc:node->doc elementName:@"parametros" elementNSPrefix:@"ServicioRegistroSvc"]);
}

+ (SRP_ObtenerDatosProductoPorID *)deserializeNode:(xmlNodePtr)cur
{
    SRP_ObtenerDatosProductoPorID *newObject = [SRP_ObtenerDatosProductoPorID new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation


- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerPorCodigo"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IYaesmio/ObtenerPorCodigo";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_ObtenerDatosProductoPorIDResponse deserializeNode:bodyNode Canal:self.Parametros.Origen];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ObtenerPorCodigoResponse";
}

@end



@implementation SRP_ObtenerDatosProductoPorIDResponse

- (id)init
{
    if ((self = [super init]))
    {
        self.ProductoPadreResponse = nil;
        self.CodigoRetorno = 0;
        self.MensajeSAP  = @"";
    }
    
    return self;
}

+ (SRP_ObtenerDatosProductoPorIDResponse *)deserializeNode:(xmlNodePtr)cur Canal:(TipoCanalProducto)pTipo
{
    SRP_ObtenerDatosProductoPorIDResponse *newObject = [SRP_ObtenerDatosProductoPorIDResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    if (newObject.ProductoPadreResponse != nil)
    {
        newObject.ProductoPadreResponse.TipoCanal = [[TipoCanal alloc] init:pTipo];
    }
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ObtenerPorCodigoResult"])
    {
        [self deserializeElementsFromNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"CodigoRetorno"])
    {
        self.CodigoRetorno = [[NSString deserializeNode:pobjCur] intValue];
    }
    else if ([nodeName isEqualToString:@"MensajeSap"])
    {
        self.MensajeSAP = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Producto"])
    {
        self.ProductoPadreResponse = [ProductoPadre deserializeNode:pobjCur];
        
       // BOOL resultado =  [self.ProductoPadreResponse comprobarObjeto];
    }
}

@end
