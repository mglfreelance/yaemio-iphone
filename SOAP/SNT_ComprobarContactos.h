//
//  SNT_ComprobarContactos.h
//  Yaesmio
//
//  Created by freelance on 21/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SNT_ComprobarContactos : SoapGenericMethod

+ (SNT_ComprobarContactos *)		deserializeNode:(xmlNodePtr)cur;

@property (strong) NSArray *ListaContactos;
@property (strong) NSString *IdUsuario;

@end


@interface SNT_ComprobarContactosResponse : SoapGenericMethod

+ (SNT_ComprobarContactosResponse *)deserializeNode:(xmlNodePtr)cur;

@property (strong) NSArray *ListaContactos;

@end
