#import <Foundation/Foundation.h>
#import <libxml/tree.h>

@interface SoapObject : NSObject <NSCoding>

@property (strong) NSString  *nsPrefix; //prefijo del servicio siempre es WS

- (NSString*) NameMetohd; // nombre del metodo a llamar

- (void) ElementForNode:(NSString*)nodeName Node:(xmlNodePtr)pobjCur; // a reescribir se llama por cada nodo

- (void) ElementForHeaderNode:(NSString*)nodeName Node:(xmlNodePtr)pobjCur; // a reescribir se llama por cada nodo del header

- (xmlNodePtr)xmlNodeForDoc:(xmlDocPtr)doc elementName:(NSString *)elName elementNSPrefix:(NSString *)elNSPrefix; 

- (void)addAttributesToNode:(xmlNodePtr)node;

- (void)addElementsToNode:(xmlNodePtr)node;

+ (SoapObject *)deserializeNode:(xmlNodePtr)cur;

- (void)deserializeAttributesFromNode:(xmlNodePtr)cur;

- (void)deserializeElementsFromNode:(xmlNodePtr)cur;

- (void)deserializeHeaderFromNode:(xmlNodePtr)cur;

-(void) getHeaderElements:(NSMutableDictionary *)headerElements;

-(void)getBodyElements:(NSMutableDictionary *)bodyElements;

-(NSString*) getSoapAction;

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode;

-(const xmlChar *) getMethodResponse;

/* attributes */
- (NSDictionary *)attributes;

-(NSString*) elementName; // metodo a sobreescribir si quieres que aparezca otro nombre como nombre de clase en xml

@end

