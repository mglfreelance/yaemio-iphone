//
//  SRM_ObtenerDatosDireccionAlternativaUsuario.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 28/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_ObtenerDatosDireccionAlternativaUsuario.h"

@implementation SRM_ObtenerDatosDireccionAlternativaUsuario

- (id)init
{
	if((self = [super init]))
    {
		self.IdUsuario = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
	if(self.IdUsuario != 0)
    {
		xmlAddChild(node, [self.IdUsuario xmlNodeForDoc:node->doc elementName:@"sapId" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_ObtenerDatosDireccionAlternativaUsuario *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerDatosDireccionAlternativaUsuario *newObject = [SRM_ObtenerDatosDireccionAlternativaUsuario new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"sapId"])
    {
        self.IdUsuario = [NSString deserializeNode:pobjCur];
    }
}

#pragma mark- BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerDatosDireccionAlternativaUsuario"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/ObtenerDatosDireccionAlternativaUsuario";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_ObtenerDatosDireccionAlternativaUsuarioResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ObtenerDatosDireccionAlternativaUsuarioResponse";
}

@end






@implementation SRM_ObtenerDatosDireccionAlternativaUsuarioResponse
- (id)init
{
	if((self = [super init]))
    {
		self.Direccion = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
	if(self.Direccion != 0)
    {
		xmlAddChild(node, [self.Direccion xmlNodeForDoc:node->doc elementName:@"ObtenerDatosDireccionAlternativaUsuarioResponse" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_ObtenerDatosDireccionAlternativaUsuarioResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerDatosDireccionAlternativaUsuarioResponse *newObject = [SRM_ObtenerDatosDireccionAlternativaUsuarioResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ObtenerDatosDireccionAlternativaUsuarioResult"] && pobjCur->children != nil)
    {
        self.Direccion = [Direccion deserializeNode:pobjCur];
    }
}

@end
