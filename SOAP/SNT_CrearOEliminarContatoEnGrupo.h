//
//  SNT_CrearOEliminarContatoEnGrupo.h
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SNT_CrearOEliminarContatoEnGrupo : SoapGenericMethod

+ (SNT_CrearOEliminarContatoEnGrupo *)		deserializeNode:(xmlNodePtr)cur;

@property (strong) NSString *EsCrear;
@property (strong) NSArray  *ListaIdUsuario;
@property (strong) NSString *IdGrupo;
@property (strong) NSString *IdUsuario;

@end


@interface SNT_CrearOEliminarContatoEnGrupoResponse : SoapGenericMethod

+ (SNT_CrearOEliminarContatoEnGrupoResponse *)deserializeNode:(xmlNodePtr)cur;

@property (strong)NSArray *ListaContactos;

@end
