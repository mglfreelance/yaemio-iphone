//
//  SRM_ObtenerTextos.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SRM_ObtenerTextos : SoapMethod

+ (SRM_ObtenerTextos *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString  *CodigoMenu;

@end


@interface SRM_ObtenerTextosResponse : SoapMethod

+ (SRM_ObtenerTextosResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * Texto;

@end
