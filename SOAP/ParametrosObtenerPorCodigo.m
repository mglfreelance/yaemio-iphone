//
//  ParametrosObtenerPorCodigo.m
//  Yaesmio
//
//  Created by freelance on 23/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ParametrosObtenerPorCodigo.h"

@implementation ParametrosObtenerPorCodigo

- (id)init
{
    if ((self = [super init]))
    {
        self.Codigo = @"";
        self.Medio = @"";
        self.Origen = TipoCanalProductoBusqueda;
    }
    
    return self;
}

+ (ParametrosObtenerPorCodigo *)deserializeNode:(xmlNodePtr)cur
{
    ParametrosObtenerPorCodigo *newObject = [[ParametrosObtenerPorCodigo alloc] init];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark- SoapObject

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Codigo xmlNodeForDoc:node->doc elementName:@"Codigo" elementNSPrefix:@"productos"]);
    
    xmlAddChild(node, [self.Medio xmlNodeForDoc:node->doc elementName:@"Medio" elementNSPrefix:@"productos"]);
    
    xmlAddChild(node, [self.Origen == TipoCanalProductoFavorito ? @"F":@"H" xmlNodeForDoc:node->doc elementName:@"Origen" elementNSPrefix:@"productos"]);
}

@end
