//
//  SRP_CancelarCompra.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/08/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SRP_CancelarCompra : SoapMethod
{
}

+ (SRP_CancelarCompra *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString    *IdCompra;

@end



@interface SRP_CancelarCompraResponse : SoapMethod
{
}

+ (SRP_CancelarCompraResponse *)deserializeNode:(xmlNodePtr)cur;

@end
