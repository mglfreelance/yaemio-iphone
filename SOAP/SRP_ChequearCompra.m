//
//  SRP_ChequearCompra.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/07/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_ChequearCompra.h"

@implementation SRP_ChequearCompra

- (id)init
{
    if ((self = [super init]))
    {
        self.IdUsuario      = @"";
        self.Direccion      = nil;
        self.ListaProductos = nil;
    }
    
    return self;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"tnsParametros";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.Direccion WithKey:@"Direccion"];
    [self.ParametrosLLamada addObject:self.ListaProductos WithKey:@"ListaProductosCompra"];
    [self.ParametrosLLamada addObject:self.IdUsuario WithKey:@"UsuarioId"];
    
    return true;
}

+ (SRP_ChequearCompra *)deserializeNode:(xmlNodePtr)cur
{
    SRP_ChequearCompra *newObject = [SRP_ChequearCompra new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ChequearCompra"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IYaesmio/ChequearCompra";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_ChequearCompraResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ChequearCompraResponse";
}

@end



@implementation SRP_ChequearCompraResponse

- (id)init
{
    if ((self = [super init]))
    {
        self.GastosEnvioResponse = [NSDecimalNumber zero];
        self.MailPaypalResponse = @"";
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
}

+ (SRP_ChequearCompraResponse *)deserializeNode:(xmlNodePtr)cur
{
    SRP_ChequearCompraResponse *newObject = [SRP_ChequearCompraResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ChequearCompraResult"])
    {
        NSArray *lista = [NSArray deserializeNode:pobjCur toClass:[NSString class]];
        
        if (lista != nil)
        {
            if (lista.count >= 1)
            {
                self.RespuestaMensajeSap = lista[0];
                self.RespuestaCodigoRetorno = (self.RespuestaMensajeSap.isNotEmpty ? SoapGenericMethodResponseError : SoapGenericMethodResponseOK);
            }
            
            if (lista.count >= 2)
            {
                self.GastosEnvioResponse = [NSDecimalNumber decimalNumberWithString:lista[1]];
            }
            
            if (lista.count >= 3)
            {
                self.MailPaypalResponse = lista[2];
            }
        }
    }
}

@end
