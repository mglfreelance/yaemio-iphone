//
//  ParametrosGuardarPuntuacionCompra.h
//  Yaesmio
//
//  Created by freelance on 13/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"

@interface ParametrosGuardarPuntuacionCompra : SoapObject

/* elements */
@property (strong) NSString *IDPedido;
@property (strong) NSString *IDProducto;
@property (assign) int      Valor;
@property (strong) NSString *TextoValoracion;

@end
