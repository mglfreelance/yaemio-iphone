//
//  SRP_ObtenerDatosProducto.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 17/07/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_ObtenerDatosProductoPorQR.h"
#import "ArrayOfSoapObject.h"

@implementation SRP_ObtenerDatosProductoPorQR

- (id)init
{
    if ((self = [super init]))
    {
        self.UrlProducto = @"";
    }
    
    return self;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix =@"productos";
    self.ParametrosLLamada.nsPrefix =@"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.UrlProducto WithKey:@"Url"];
    
    return true;
}

+ (SRP_ObtenerDatosProductoPorQR *)deserializeNode:(xmlNodePtr)cur
{
    SRP_ObtenerDatosProductoPorQR *newObject = [SRP_ObtenerDatosProductoPorQR new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerPorQr"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IYaesmio/ObtenerPorQr";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_ObtenerDatosProductoPorQRResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ObtenerPorQrResponse";
}

@end



@implementation SRP_ObtenerDatosProductoPorQRResponse

- (id)init
{
    if ((self = [super init]))
    {
        self.ProductoPadreResponse = nil;
    }
    
    return self;
}

+ (SRP_ObtenerDatosProductoPorQRResponse *)deserializeNode:(xmlNodePtr)cur
{
    SRP_ObtenerDatosProductoPorQRResponse *newObject = [SRP_ObtenerDatosProductoPorQRResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ObtenerPorQrResult";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ListaProductosAgrupados"])
    {
    }
    else if ([nodeName isEqualToString:@"Producto"])
    {
        self.ProductoPadreResponse = [ProductoPadre deserializeNode:pobjCur];
        self.ProductoPadreResponse.TipoCanal = [TipoCanal TipoCanalBusqueda];
    }
}

@end
