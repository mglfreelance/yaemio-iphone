//
//  SNT_CrearGrupo.h
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"
#import "GrupoNotificacion.h"

@interface SNT_CrearGrupo : SoapGenericMethod

+ (SNT_CrearGrupo *)		deserializeNode:(xmlNodePtr)cur;

@property (strong) NSString *NombreGrupo;
@property (strong) NSArray  *ListaIdContactos;

@end


@interface SNT_CrearGrupoResponse : SoapGenericMethod

+ (SNT_CrearGrupoResponse *)deserializeNode:(xmlNodePtr)cur;

@property (strong) GrupoNotificacion *Grupo;

@end
