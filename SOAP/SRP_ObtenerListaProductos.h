//
//  SRP_ObtenerListaProductos.h
//  Yaesmio
//
//  Created by freelance on 08/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"
#import "FiltroBusquedaProductos.h"

@interface SRP_ObtenerListaProductos : SoapMethod

+ (SRP_ObtenerListaProductos *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) FiltroBusquedaProductos *filtro;

@end


@interface SRP_ObtenerListaProductosResponse : SoapMethod

+ (SRP_ObtenerListaProductosResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong)      NSArray     *ListaProductos;
@property (nonatomic)   int         NumeroElementosEncontrados;
@property (nonatomic)   int         CodigoRetorno;
@property (strong)      NSString    *MensajeSAP;

@end
