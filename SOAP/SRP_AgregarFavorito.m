//
//  SRP_AgregarFavorito.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 20/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_AgregarFavorito.h"

@implementation SRP_AgregarFavorito

- (id)init
{
    if ((self = [super init]))
    {
        self.IdUsuario      = @"";
        self.IdProducto     = @"";
        self.Medio          = @"";
        self.Latitud        = 0.0;
        self.Longitud       = 0.0;
    }
    
    return self;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"tnsParametros";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.IdProducto WithKey:@"IdProducto"];
    [self.ParametrosLLamada addFloat:self.Latitud WithKey:@"Latitud"];
    [self.ParametrosLLamada addFloat:self.Longitud WithKey:@"Longitud"];
    [self.ParametrosLLamada addObject:self.Medio WithKey:@"Medio"];
    [self.ParametrosLLamada addObject:self.IdUsuario WithKey:@"Usuario"];
    
    return true;
}

+ (SRP_AgregarFavorito *)deserializeNode:(xmlNodePtr)cur
{
    SRP_AgregarFavorito *newObject = [SRP_AgregarFavorito new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"AgregarFavorito"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IYaesmio/AgregarFavorito";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_AgregarFavoritoResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"AgregarFavoritoResponse";
}

@end


@implementation SRP_AgregarFavoritoResponse

+ (SRP_AgregarFavoritoResponse *)deserializeNode:(xmlNodePtr)cur
{
    SRP_AgregarFavoritoResponse *newObject = [SRP_AgregarFavoritoResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"AgregarFavoritoResult"])
    {
        NSArray *Valor = [NSArray deserializeNode:pobjCur toClass:[NSString class]];
        
        if (Valor.count >= 1)
        {
            self.RespuestaCodigoRetorno = ([Valor[0] isEqualToString:@"0"] ? SoapGenericMethodResponseOK : SoapGenericMethodResponseError);
        }
        
        if (Valor.count >= 2)
        {
            self.RespuestaMensajeSap = Valor[1];
        }
    }
}

@end
