//
//  SNT_ModificarEstadoEnviadoNotificacion.h
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"
#import "CabeceraNotificacion.h"

@interface SNT_ModificarEstadoEnviadoNotificacion : SoapGenericMethod

+ (SNT_ModificarEstadoEnviadoNotificacion *)			deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (assign) SNTipoNotificacion EstadoNotificacion;
@property (strong) NSString *IdNotificacion;

@end


@interface SNT_ModificarEstadoEnviadoNotificacionResponse : SoapGenericMethod

+ (SNT_ModificarEstadoEnviadoNotificacionResponse *)	deserializeNode:(xmlNodePtr)cur;

@end
