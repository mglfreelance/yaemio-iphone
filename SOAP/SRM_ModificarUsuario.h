//
//  ServicioRegistro_ModificarUsuario.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"
#import "Usuario.h"

@interface SRM_ModificarUsuario : SoapMethod
{
}

+ (SRM_ModificarUsuario *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) Usuario * usu;

@end






@interface SRM_ModificarUsuarioResponse : SoapMethod
{
}

+ (SRM_ModificarUsuarioResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSNumber * ModificarUsuarioResult;
@property (strong) NSString * MensajeSap;

@end

