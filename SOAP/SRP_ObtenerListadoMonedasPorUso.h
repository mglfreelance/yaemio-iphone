//
//  ObtenerListadoMonedasPorUso.h
//  Yaesmio
//
//  Created by freelance on 08/07/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SRP_ObtenerListadoMonedasPorUso : SoapGenericMethod

+ (SRP_ObtenerListadoMonedasPorUso *)		deserializeNode:(xmlNodePtr)cur;

@end




@interface SRP_ObtenerListadoMonedasPorUsoResponse : SoapGenericMethod

+ (SRP_ObtenerListadoMonedasPorUsoResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (readonly) NSArray *ListaMonedas;

@end
