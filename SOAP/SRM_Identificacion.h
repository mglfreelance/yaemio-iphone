//
//  ServicioRegistro_Identificacion.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 24/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"
#import "LoginUsuarioResponse.h"

@interface SRM_Identificacion : SoapMethod
{
}

+ (SRM_Identificacion *)		deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *email;
@property (strong) NSString *password;

@end


@interface SRM_IdentificacionResponse : SoapMethod
{
}

+ (SRM_IdentificacionResponse *)deserializeNode:(xmlNodePtr)cur AndHeader:(xmlNodePtr)header;

/* elements */
@property (strong) LoginUsuarioResponse *Response;
@property (assign) int                  CodeResult;


@end
