//
//  SRP_ObtenerDatosProducto.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 17/07/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ServicioSOAP.h"
#import "ProductoPadre.h"
#import "ParametrosObtenerPorCodigo.h"

@interface SRP_ObtenerDatosProductoPorID : SoapMethod

+ (SRP_ObtenerDatosProductoPorID *)			deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) ParametrosObtenerPorCodigo *Parametros;

@end

@interface SRP_ObtenerDatosProductoPorIDResponse : SoapMethod

+ (SRP_ObtenerDatosProductoPorIDResponse *)deserializeNode:(xmlNodePtr)cur Canal:(TipoCanalProducto)pTipo;

/* elements */
@property (nonatomic) int CodigoRetorno;
@property (strong)    NSString *MensajeSAP;
@property (strong)    ProductoPadre *ProductoPadreResponse;


@end
