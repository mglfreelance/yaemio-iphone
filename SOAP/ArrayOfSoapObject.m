#import "ArrayOfSoapObject.h"
#import <libxml/xmlstring.h>
#if TARGET_OS_IPHONE
#import <CFNetwork/CFNetwork.h>
#endif

@interface ArrayOfSoapObject ()

@property (readonly) NSMutableDictionary *List;
@property (readonly) NSMutableArray *KeyList;

@end

@implementation ArrayOfSoapObject

@synthesize List = _List;
@synthesize KeyList = _KeyList;

- (id)init
{
    if ((self = [super init]))
    {
        _List = [NSMutableDictionary new];
        _KeyList = [NSMutableArray new];
        self.Name = @"";
        self.ChilldnsPrefix = @"";
    }
    
    return self;
}

- (void)addObject:(id)pObject WithKey:(NSString *)pKey
{
    if (pObject == nil)
    {
        pObject = @"";
    }
    
    [self.List setObject:pObject forKey:pKey];
    [self.KeyList addObject:pKey];
}

- (void)addInt:(int)pNumero WithKey:(NSString *)pKey
{
    [self addObject:[NSString stringWithFormat:@"%d", pNumero] WithKey:pKey];
}

- (void)addBool:(BOOL)pNumero WithKey:(NSString *)pKey
{
    [self addObject:[NSString stringWithFormat:@"%d", pNumero] WithKey:pKey];
}

- (void)addFloat:(float)pNumero WithKey:(NSString *)pKey
{
    [self addObject:[NSString stringWithFormat:@"%f", pNumero] WithKey:pKey];
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    if (self.List != nil)
    {
        for (NSString *key in self.KeyList)
        {
            xmlAddChild(node, [self.List[key] xmlNodeForDoc:node->doc elementName:key elementNSPrefix:self.ChilldnsPrefix]);
        }
    }
}

+ (ArrayOfSoapObject *)deserializeNode:(xmlNodePtr)cur
{
    ArrayOfSoapObject *newObject = [ArrayOfSoapObject new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

@end
