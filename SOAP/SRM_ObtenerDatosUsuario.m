//
//  ServicioRegistro_ObtenerDatosUsuario.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_ObtenerDatosUsuario.h"

@implementation SRM_ObtenerDatosUsuario

- (id)init
{
	if((self = [super init]))
    {
		self.IdUsuario = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{	
	if(self.IdUsuario != 0)
    {
		xmlAddChild(node, [self.IdUsuario xmlNodeForDoc:node->doc elementName:@"sapId" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_ObtenerDatosUsuario *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerDatosUsuario *newObject = [SRM_ObtenerDatosUsuario new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"sapId"])
    {
        self.IdUsuario = [NSString deserializeNode:pobjCur];
    }
}

#pragma mark - BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerDatosUsuario"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/ObtenerDatosUsuario";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_ObtenerDatosUsuarioResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ObtenerDatosUsuarioResponse";
}

@end






@implementation SRM_ObtenerDatosUsuarioResponse

- (id)init
{
	if((self = [super init]))
    {
		self.ObtenerDatosUsuarioResult = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{	
	if(self.ObtenerDatosUsuarioResult != 0)
    {
		xmlAddChild(node, [self.ObtenerDatosUsuarioResult xmlNodeForDoc:node->doc elementName:@"ObtenerDatosUsuarioResult" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_ObtenerDatosUsuarioResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerDatosUsuarioResponse *newObject = [SRM_ObtenerDatosUsuarioResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ObtenerDatosUsuarioResult"])
    {
        self.ObtenerDatosUsuarioResult = [Usuario deserializeNode:pobjCur];
    }
}


@end
