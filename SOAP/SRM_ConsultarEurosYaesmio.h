//
//  ServicioRegistro_ConsultarEurosYaesmio.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRM_ConsultarEurosYaesmio : SoapMethod
{
}

+ (SRM_ConsultarEurosYaesmio *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * emailUsu;

@end





@interface SRM_ConsultarEurosYaesmioResponse : SoapMethod
{
}

+ (SRM_ConsultarEurosYaesmioResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSNumber * ConsultarEurosYaesmioResult;

@end

