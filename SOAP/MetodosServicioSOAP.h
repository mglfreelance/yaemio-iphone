//
//  MetodosServicioSOAP.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 28/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

// USUARIO //
#import "SRM_Identificacion.h"
#import "SRM_RecuperarContrasena.h"
#import "SRM_ComprobarExisteEmail.h"
#import "SRM_ConsultarEurosYaesmio.h"
#import "SRP_ObtenerListadoPaisesPorUso.h"
#import "SRM_DevolverListaProvincias.h"
#import "SRM_ObtenerDatosUsuario.h"
#import "SRM_CrearUsuarioMovil.h"
#import "SRM_ModificarUsuario.h"
#import "SRM_EditarPassword.h"
#import "SRM_ObtenerDatosDireccionPrincipalUsuario.h"
#import "SRM_ObtenerDatosDireccionAlternativaUsuario.h"
#import "SRM_ModificarDireccionAlternativa.h"
#import "SRM_ModificarDireccionPrincipal.h"
#import "SRM_ObtenerTextos.h"
#import "SRM_ObtenerSitiosYaesmio.h"
#import "SRM_DevolverListaGrupoArticulo.h"
#import "SRM_DevolverListaEmpresas.h"
#import "SRM_ObtenerTitulosInformacion.h"
#import "SRM_ObtenerProvinciaDeGeolocalizacion.h"
#import "SRM_ObtenerTarjetas.h"


// PRODUCTO //
#import "SRP_GuardarCompras.h"
#import "SRP_ConfirmarCompra.h"
#import "SRP_ObtenerFavoritos.h"
#import "SRP_AgregarFavorito.h"
#import "SRP_EliminarFavorito.h"
#import "SRP_ObtenerProductoHijo.h"
#import "SRP_HistorialComprasUsuario.h"
#import "SRP_ObtenerDatosProductoPorQR.h"
#import "SRP_ChequearCompra.h"
#import "SRP_CancelarCompra.h"
#import "SRP_ValorarProductoCompra.h"
#import "SRP_DevolverEmoticonos.h"
#import "SRP_ObtenerListaProductos.h"
#import "SRP_ObtenerDatosProductoPorID.h"
#import "SRP_RealizarCompraPrecioCero.h"
#import "SRP_ObtenerListadoMonedasPorUso.h"
#import "SRP_ObtenerListaDestacados.h"


// MEDIA //
#import "SRA_ObtenerConfiguracionApp.h"


// NOTIFICACIONES //
#import "SNT_ActivarNotificacionPush.h"
#import "SNT_ObtenerListaNotificacionesRecibidas.h"
#import "SNT_ModificarEstadoRecibidoNotificacion.h"
#import "SNT_ObtenerListaNotificacionesEmitidas.h"
#import "SNT_ModificarEstadoEnviadoNotificacion.h"
#import "SNT_ConsultarDestinatariosNotificacion.h"
#import "SNT_ObtenerListadoGruposContactos.h"
#import "SNT_EliminarGrupo.h"
#import "SNT_CrearGrupo.h"
#import "SNT_ModificarGrupo.h"
#import "SNT_CrearOEliminarContatoEnGrupo.h"
#import "SNT_ComprobarContactos.h"
#import "SNT_EnviarNotificacionesPush.h"
#import "SNT_DesactivarNotificacionesPush.h"
#import "SNT_ObtenerNumeroNotificacionesNoLeidas.h"
