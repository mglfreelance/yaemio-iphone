//
//  ServicioRegistro_DevolverListaPaises.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRP_ObtenerListadoPaisesPorUso : SoapGenericMethod
{
}

@property (strong) NSString *CodigoTipo;

+ (SRP_ObtenerListadoPaisesPorUso *)		deserializeNode:(xmlNodePtr)cur;

@end




@interface SRP_ObtenerListadoPaisesPorUsoResponse : SoapGenericMethod
{
}

+ (SRP_ObtenerListadoPaisesPorUsoResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (readonly) NSArray *ListaPaisesResult;

@end
