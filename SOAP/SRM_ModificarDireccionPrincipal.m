//
//  ServicioRegistro_ModificarDireccionPrincipal.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 28/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_ModificarDireccionPrincipal.h"

@implementation SRM_ModificarDireccionPrincipal

- (id)init
{
    if ((self = [super init]))
    {
        self.IdUsuario = nil;
        self.primeraDir = nil;
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    if (self.IdUsuario != 0)
    {
        xmlAddChild(node, [self.IdUsuario xmlNodeForDoc:node->doc elementName:@"sapId" elementNSPrefix:@"ServicioRegistroSvc"]);
    }
    
    if (self.primeraDir != 0)
    {
        xmlAddChild(node, [self.primeraDir xmlNodeForDoc:node->doc elementName:@"primeraDir" elementNSPrefix:@"ServicioRegistroSvc"]);
    }
}

+ (SRM_ModificarDireccionPrincipal *)deserializeNode:(xmlNodePtr)cur
{
    SRM_ModificarDireccionPrincipal *newObject = [SRM_ModificarDireccionPrincipal new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"sapId"])
    {
        self.IdUsuario = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"primeraDir"])
    {
        self.primeraDir = [Direccion deserializeNode:pobjCur];
    }
}

#pragma mark- BindingOperation

- (void)getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ModificarDireccionPrincipal"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/ModificarDireccionPrincipal";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_ModificarDireccionPrincipalResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ModificarDireccionPrincipalResponse";
}

@end






@implementation SRM_ModificarDireccionPrincipalResponse

- (id)init
{
    if ((self = [super init]))
    {
        self.ModificarDireccionPrincipalResult = nil;
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    if (self.ModificarDireccionPrincipalResult != 0)
    {
        xmlAddChild(node, [self.ModificarDireccionPrincipalResult xmlNodeForDoc:node->doc elementName:@"ModificarDireccionPrincipal" elementNSPrefix:@"ServicioRegistroSvc"]);
    }
}

+ (SRM_ModificarDireccionPrincipalResponse *)deserializeNode:(xmlNodePtr)cur
{
    SRM_ModificarDireccionPrincipalResponse *newObject = [SRM_ModificarDireccionPrincipalResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ModificarDireccionPrincipalResult"])
    {
        self.ModificarDireccionPrincipalResult = [NSString deserializeNode:pobjCur];
    }
}

@end
