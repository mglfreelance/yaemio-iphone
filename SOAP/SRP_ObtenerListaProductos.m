//
//  SRP_ObtenerListaProductos.m
//  Yaesmio
//
//  Created by freelance on 08/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SRP_ObtenerListaProductos.h"
#import "ProductoBuscado.h"

@implementation SRP_ObtenerListaProductos
- (id)init
{
	if((self = [super init]))
    {
		self.filtro = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.filtro xmlNodeForDoc:node->doc elementName:@"parametros" elementNSPrefix:@"ServicioRegistroSvc"]);
    
}

+ (SRP_ObtenerListaProductos *)deserializeNode:(xmlNodePtr)cur
{
	SRP_ObtenerListaProductos *newObject = [SRP_ObtenerListaProductos new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark - BindingOperation

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerListaProductos"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IYaesmio/ObtenerListaProductos";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_ObtenerListaProductosResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ObtenerListaProductosResponse";
}

@end


@implementation SRP_ObtenerListaProductosResponse

- (id)init
{
	if((self = [super init]))
    {
		self.ListaProductos             = nil;
        self.MensajeSAP                 = @"";
        self.CodigoRetorno              = 0;
        self.NumeroElementosEncontrados = 0;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
}

+ (SRP_ObtenerListaProductosResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRP_ObtenerListaProductosResponse *newObject = [SRP_ObtenerListaProductosResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ObtenerListaProductosResult"])
    {
        //vuelvo a desserializar el objeto
        [self deserializeElementsFromNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"ListaProductos"])
    {
        //vuelvo a deserializar los hijos
        self.ListaProductos = [NSArray deserializeNode:pobjCur toClass:[ProductoBuscado class]];
    }
    else if([nodeName isEqualToString:@"CodigoRetorno"])
    {
        //vuelvo a deserializar los hijos
        self.CodigoRetorno = [[NSString deserializeNode:pobjCur] intValue];
    }
    else if([nodeName isEqualToString:@"MensajeSap"])
    {
        //vuelvo a deserializar los hijos
        self.MensajeSAP = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"NumeroProductosEncontrado"])
    {
        //vuelvo a deserializar los hijos
        self.NumeroElementosEncontrados = [[NSString deserializeNode:pobjCur] intValue];
    }
}

@end
