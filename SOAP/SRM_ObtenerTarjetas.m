//
//  SRM_ObtenerTarjetas.m
//  Yaesmio
//
//  Created by freelance on 09/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SRM_ObtenerTarjetas.h"
#import "TarjetaUsuario.h"

@implementation SRM_ObtenerTarjetas

+ (SRM_ObtenerTarjetas *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerTarjetas *newObject = [SRM_ObtenerTarjetas new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark - BindingOperation


-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerTarjetas"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/ObtenerTarjetas";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_ObtenerTarjetasResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ObtenerTarjetasResponse";
}

@end






@implementation SRM_ObtenerTarjetasResponse

- (id)init
{
	if((self = [super init]))
    {
		self.ListaTarjetas = nil;
	}
	
	return self;
}

+ (SRM_ObtenerTarjetasResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerTarjetasResponse *newObject = [SRM_ObtenerTarjetasResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (NSString *)getNameResult
{
    return @"ObtenerTarjetasResult";
}


- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ListTarjetas"])
    {
        self.ListaTarjetas = [NSArray deserializeNode:pobjCur toClass:[TarjetaUsuario class]];
    }
}
@end
