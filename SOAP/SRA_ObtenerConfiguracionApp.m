//
//  SRA_ObtenerConfiguracionApp.m
//  Yaesmio
//
//  Created by freelance on 30/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SRA_ObtenerConfiguracionApp.h"
#import "ConfigurationParameter.h"

@implementation SRA_ObtenerConfiguracionApp

- (id)init
{
    if ((self = [super init]))
    {
        self.IdApp = @"";
    }
    
    return self;
}

+ (SRA_ObtenerConfiguracionApp *)deserializeNode:(xmlNodePtr)cur
{
    SRA_ObtenerConfiguracionApp *newObject = [SRA_ObtenerConfiguracionApp new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"yaesMedia";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.IdApp WithKey:@"App"];
    
    return true;
}


#pragma mark- BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerConfiguracionApp"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IMedia/ObtenerConfiguracionApp";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRA_ObtenerConfiguracionAppResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ObtenerConfiguracionAppResponse";
}

@end





@implementation SRA_ObtenerConfiguracionAppResponse

- (id)init
{
    if ((self = [super init]))
    {
        self.UrlSonido = @"";
        self.UrlImagenFondoApp = @"";
    }
    
    return self;
}

+ (SRA_ObtenerConfiguracionAppResponse *)deserializeNode:(xmlNodePtr)cur
{
    SRA_ObtenerConfiguracionAppResponse *newObject = [SRA_ObtenerConfiguracionAppResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ObtenerConfiguracionAppResult";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ImagenFondo"])
    {
        self.UrlImagenFondoApp = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Sonido"])
    {
        self.UrlSonido = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Parameters"])
    {
        self.Parametros = [NSArray deserializeNode:pobjCur toClass:[ConfigurationParameter class]];
    }
}

@end
