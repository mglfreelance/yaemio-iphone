//
//  SNT_ObtenerListadoGruposContactos.h
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SNT_ObtenerListadoGruposContactos : SoapGenericMethod

+ (SNT_ObtenerListadoGruposContactos *)			deserializeNode:(xmlNodePtr)cur;

@end


@interface SNT_ObtenerListadoGruposContactosResponse : SoapGenericMethod

+ (SNT_ObtenerListadoGruposContactosResponse *)	deserializeNode:(xmlNodePtr)cur;

@property (strong) NSArray *ListaGrupos;


@end
