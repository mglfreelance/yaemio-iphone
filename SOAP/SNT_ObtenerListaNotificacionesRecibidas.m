//
//  SNT_ObtenerListaNotificacionesRecibidas.m
//  Yaesmio
//
//  Created by freelance on 16/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_ObtenerListaNotificacionesRecibidas.h"
#import "CabeceraNotificacion.h"

@implementation SNT_ObtenerListaNotificacionesRecibidas

- (id)init
{
    if ((self = [super init]))
    {
        self.Visto           = false;
        self.IdUsuario       = @"";
        self.NumeroLineas    = 0;
        self.PosicionInicial = 0;
    }
    
    return self;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix =@"notificaciones";
    self.ParametrosLLamada.nsPrefix =@"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.IdUsuario WithKey:@"EmisorOReceptor"];
    [self.ParametrosLLamada addInt:self.NumeroLineas WithKey:@"NumeroLineas"];
    [self.ParametrosLLamada addInt:self.PosicionInicial WithKey:@"PosicionInicial"];
    [self.ParametrosLLamada addObject:self.Visto?@"V":@"" WithKey:@"Visto"];
    
    return true;
}

+ (SNT_ObtenerListaNotificacionesRecibidas *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ObtenerListaNotificacionesRecibidas *newObject = [SNT_ObtenerListaNotificacionesRecibidas new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerListaNotificacionesRecibidas"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/ObtenerListaNotificacionesRecibidas";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_ObtenerListaNotificacionesRecibidasResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ObtenerListaNotificacionesRecibidasResponse";
}

@end


@implementation SNT_ObtenerListaNotificacionesRecibidasResponse

- (id)init
{
    if ((self = [super init]))
    {
        self.ListaNotificaciones = nil;
        self.NumeroTotal   = 0;
    }
    
    return self;
}


+ (SNT_ObtenerListaNotificacionesRecibidasResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ObtenerListaNotificacionesRecibidasResponse *newObject = [SNT_ObtenerListaNotificacionesRecibidasResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ObtenerListaNotificacionesRecibidasResult";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ListaCabeceraNotificaciones"])
    {
        self.ListaNotificaciones = [NSArray deserializeNode:pobjCur toClass:[CabeceraNotificacion class]];
    }
    else if ([nodeName isEqualToString:@"NumeroMensajes"])
    {
        self.NumeroTotal = [NSString deserializeNode:pobjCur].intValue;
    }
}



@end
