//
//  ServicioRegistro_DevolverListaPaises.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_ObtenerListadoPaisesPorUso.h"

@implementation SRP_ObtenerListadoPaisesPorUso

- (id)init
{
    if ((self = [super init]))
    {
        self.CodigoTipo = @"";
    }
    
    return self;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"yaes";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.CodigoTipo WithKey:@"Uso"];
    
    return true;
}

+ (SRP_ObtenerListadoPaisesPorUso *)deserializeNode:(xmlNodePtr)cur
{
    SRP_ObtenerListadoPaisesPorUso *newObject = [SRP_ObtenerListadoPaisesPorUso new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark- BindingOperation

- (void)getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerListadoPaisesPorUso"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IYaesmio/ObtenerListadoPaisesPorUso";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_ObtenerListadoPaisesPorUsoResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ObtenerListadoPaisesPorUsoResponse";
}

@end




@implementation SRP_ObtenerListadoPaisesPorUsoResponse

- (id)init
{
    if ((self = [super init]))
    {
    }
    
    return self;
}

+ (SRP_ObtenerListadoPaisesPorUsoResponse *)deserializeNode:(xmlNodePtr)cur
{
    SRP_ObtenerListadoPaisesPorUsoResponse *newObject = [SRP_ObtenerListadoPaisesPorUsoResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ObtenerListadoPaisesPorUsoResult";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ListadoPaises"])
    {
        _ListaPaisesResult = [NSArray deserializeNode:pobjCur toClass:[Pais class]];
    }
}

@end
