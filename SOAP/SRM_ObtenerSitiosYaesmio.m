//
//  SRM_ObtenerSitiosYaesmio.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 12/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_ObtenerSitiosYaesmio.h"
#import "SitioYaesmio.h"

@implementation SRM_ObtenerSitiosYaesmio

- (id)init
{
	if((self = [super init]))
    {
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
}

+ (SRM_ObtenerSitiosYaesmio *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerSitiosYaesmio *newObject = [SRM_ObtenerSitiosYaesmio new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark - BindingOperation

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerSitiosYaesmio"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/ObtenerSitiosYaesmio";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_ObtenerSitiosYaesmioResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ObtenerSitiosYaesmioResponse";
}

@end


@implementation SRM_ObtenerSitiosYaesmioResponse

- (id)init
{
	if((self = [super init]))
    {
		self.ListaSitios = nil;
	}
	
	return self;
}

+ (SRM_ObtenerSitiosYaesmioResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerSitiosYaesmioResponse *newObject = [SRM_ObtenerSitiosYaesmioResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ObtenerSitiosYaesmioResult"])
    {
        self.ListaSitios = [NSArray deserializeNode:pobjCur toClass:[SitioYaesmio class]];
    }
    
}

@end
