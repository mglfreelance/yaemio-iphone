//
//  ServicioRegistro_EditarPassword.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRM_EditarPassword : SoapMethod

+ (SRM_EditarPassword *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * mail;
@property (strong) NSString * oldpass;
@property (strong) NSString * nuevapass;

@end



@interface SRM_EditarPasswordResponse : SoapMethod

+ (SRM_EditarPasswordResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSNumber * EditarPasswordResult;
@property (strong) NSString * SecurityToken;

@end

