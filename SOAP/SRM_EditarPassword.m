//
//  ServicioRegistro_EditarPassword.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_EditarPassword.h"

@implementation SRM_EditarPassword

- (id)init
{
	if((self = [super init]))
    {
		self.mail = nil;
		self.oldpass = nil;
		self.nuevapass = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
	if(self.mail != 0)
    {
		xmlAddChild(node, [self.mail xmlNodeForDoc:node->doc elementName:@"userEmail" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
	if(self.oldpass != 0)
    {
		xmlAddChild(node, [self.oldpass xmlNodeForDoc:node->doc elementName:@"password" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
	if(self.nuevapass != 0)
    {
		xmlAddChild(node, [self.nuevapass xmlNodeForDoc:node->doc elementName:@"nuevapassword" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_EditarPassword *)deserializeNode:(xmlNodePtr)cur
{
	SRM_EditarPassword *newObject = [SRM_EditarPassword new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark- BindingOperation


-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"EditarPasswordV2"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/EditarPasswordV2";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_EditarPasswordResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "EditarPasswordV2Response";
}

@end







@implementation SRM_EditarPasswordResponse

- (id)init
{
	if((self = [super init]))
    {
		self.EditarPasswordResult = nil;
	}
	
	return self;
}

+ (SRM_EditarPasswordResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_EditarPasswordResponse *newObject = [SRM_EditarPasswordResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"EditarPasswordV2Result"])
    {
        [self deserializeElementsFromNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"CodigoRetorno"])
    {
        self.EditarPasswordResult = [NSNumber deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"Token"])
    {
        self.SecurityToken = [NSString deserializeNode:pobjCur];
    }
}

@end
