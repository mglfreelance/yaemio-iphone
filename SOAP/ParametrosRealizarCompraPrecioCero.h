//
//  ParametrosRealizarCompraPrecioCero.h
//  Yaesmio
//
//  Created by freelance on 09/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"

@interface ParametrosRealizarCompraPrecioCero : SoapObject

- (id)initWithIdUsuario:(NSString *)pIdUsuario Producto:(ElementoCarrito *)pProducto;

/* elements */
@property (strong) NSString *IdUsuario;
@property (strong) ElementoCarrito *Producto;

@end
