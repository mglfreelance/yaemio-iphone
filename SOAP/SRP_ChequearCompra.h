//
//  SRP_ChequearCompra.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 23/07/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SRP_ChequearCompra : SoapGenericMethod

+ (SRP_ChequearCompra *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) Direccion *Direccion;
@property (strong) NSString  *IdUsuario;
@property (strong) NSArray   *ListaProductos;

@end



@interface SRP_ChequearCompraResponse : SoapGenericMethod

+ (SRP_ChequearCompraResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSDecimalNumber * GastosEnvioResponse;
@property (strong) NSString        * MailPaypalResponse;

@end
