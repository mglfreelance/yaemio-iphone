//
//  ServicioRegistro_ObtenerDatosDireccionPrincipalUsuario.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_ObtenerDatosDireccionPrincipalUsuario.h"

@implementation SRM_ObtenerDatosDireccionPrincipalUsuario

- (id)init
{
	if((self = [super init]))
    {
		self.IdUsuario = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
	if(self.IdUsuario != 0)
    {
		xmlAddChild(node, [self.IdUsuario xmlNodeForDoc:node->doc elementName:@"sapId" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_ObtenerDatosDireccionPrincipalUsuario *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerDatosDireccionPrincipalUsuario *newObject = [SRM_ObtenerDatosDireccionPrincipalUsuario new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"sapId"])
    {
        self.IdUsuario = [NSString deserializeNode:pobjCur];
    }
}

#pragma mark- BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
  [bodyElements setObject:self forKey:@"ObtenerDatosDireccionPrincipalUsuario"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/ObtenerDatosDireccionPrincipalUsuario";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_ObtenerDatosDireccionPrincipalUsuarioResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ObtenerDatosDireccionPrincipalUsuarioResponse";
}

@end






@implementation SRM_ObtenerDatosDireccionPrincipalUsuarioResponse
- (id)init
{
	if((self = [super init]))
    {
		self.Direccion = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
	if(self.Direccion != 0)
    {
		xmlAddChild(node, [self.Direccion xmlNodeForDoc:node->doc elementName:@"ObtenerDatosDireccionPrincipalUsuarioResponse" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_ObtenerDatosDireccionPrincipalUsuarioResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerDatosDireccionPrincipalUsuarioResponse *newObject = [SRM_ObtenerDatosDireccionPrincipalUsuarioResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ObtenerDatosDireccionPrincipalUsuarioResult"] && pobjCur->children != nil)
    {
        self.Direccion = [Direccion deserializeNode:pobjCur];
    }
}

@end