//
//  SRP_ActualizarCompras.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 17/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ParametrosConfirmarCompra.h"
#import "ServicioSOAP.h"

@interface SRP_ConfirmarCompra : SoapMethod
{
}

+ (SRP_ConfirmarCompra *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) ParametrosConfirmarCompra *Parametros;

@end



@interface SRP_ConfirmarCompraResponse : SoapMethod
{
}

+ (SRP_ConfirmarCompra *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *ActualizarComprasResult;

@end
