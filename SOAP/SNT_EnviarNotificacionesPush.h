//
//  SNT_EnviarNotificacionesPush.h
//  Yaesmio
//
//  Created by freelance on 22/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SNT_EnviarNotificacionesPush : SoapGenericMethod

+ (SNT_EnviarNotificacionesPush *)			deserializeNode:(xmlNodePtr)cur;

@property (strong) NSArray *ListaIdContactosSap;
@property (strong) NSArray *ListaMails;
@property (strong) NSArray *ListaIdGruposContactos;
@property (strong) NSString *IdUsuario;
@property (strong) NSString *IdProducto;
@property (strong) NSString *Medio;
@property (strong) NSString *TextoMensaje;

@end


@interface SNT_EnviarNotificacionesPushResponse : SoapGenericMethod

+ (SNT_EnviarNotificacionesPushResponse *)	deserializeNode:(xmlNodePtr)cur;

@end
