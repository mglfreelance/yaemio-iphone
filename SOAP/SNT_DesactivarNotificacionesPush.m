//
//  SNT_DesactivarNotificacionesPush.m
//  Yaesmio
//
//  Created by freelance on 22/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_DesactivarNotificacionesPush.h"

@implementation SNT_DesactivarNotificacionesPush

- (id)init
{
    if ((self = [super init]))
    {
        self.IdUsuario = @"";
        self.IdPush    = @"";
        self.MacDevice = @"";
        self.Telefono  = @"";
    }
    
    return self;
}

+ (SNT_DesactivarNotificacionesPush *)deserializeNode:(xmlNodePtr)cur
{
    SNT_DesactivarNotificacionesPush *newObject = [SNT_DesactivarNotificacionesPush new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"tnsParametros";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.IdPush WithKey:@"IdPush"];
    [self.ParametrosLLamada addObject:self.IdUsuario WithKey:@"IdUsuario"];
    [self.ParametrosLLamada addObject:self.MacDevice WithKey:@"MacDevice"];
    [self.ParametrosLLamada addObject:self.Telefono WithKey:@"Telefono"];
    
    return true;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"DesactivarNotificacionesPush"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/DesactivarNotificacionesPush";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_DesactivarNotificacionesPushResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"DesactivarNotificacionesPushResponse";
}

@end


@implementation SNT_DesactivarNotificacionesPushResponse

+ (SNT_DesactivarNotificacionesPushResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_DesactivarNotificacionesPushResponse *newObject = [SNT_DesactivarNotificacionesPushResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"DesactivarNotificacionesPushResult";
}


@end
