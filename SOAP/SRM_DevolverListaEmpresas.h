//
//  SRM_DevolverListaEmpresas.h
//  Yaesmio
//
//  Created by freelance on 22/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"
#import "ParametrosDevolverListaEmpresa.h"

@interface SRM_DevolverListaEmpresas : SoapMethod
{
}
@property (strong)    ParametrosDevolverListaEmpresa    *Parametros;

+ (SRM_DevolverListaEmpresas *)deserializeNode:(xmlNodePtr)cur;

@end



@interface SRM_DevolverListaEmpresasResponse : SoapMethod
{
}

+ (SRM_DevolverListaEmpresasResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (readonly)  NSArray     *ListaEmpresaResult;
@property (nonatomic) int         CodigoRetorno;
@property (strong)    NSString    *MensajeSAP;

@end

