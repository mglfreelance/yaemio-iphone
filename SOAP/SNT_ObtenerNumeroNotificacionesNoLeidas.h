//
//  SNT_ObtenerNumeroNotificacionesNoLeidas.h
//  Yaesmio
//
//  Created by freelance on 03/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SNT_ObtenerNumeroNotificacionesNoLeidas : SoapGenericMethod

+ (SNT_ObtenerNumeroNotificacionesNoLeidas *)			deserializeNode:(xmlNodePtr)cur;

@end


@interface SNT_ObtenerNumeroNotificacionesNoLeidasResponse : SoapGenericMethod

+ (SNT_ObtenerNumeroNotificacionesNoLeidasResponse *)	deserializeNode:(xmlNodePtr)cur;

@property (assign) NSInteger NumeroNotificacionesSinLeer;
@end
