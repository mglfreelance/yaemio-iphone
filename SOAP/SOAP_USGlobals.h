#import <Foundation/Foundation.h>

@interface SOAP_USGlobals : NSObject {
	NSMutableDictionary *wsdlStandardNamespaces;
}

@property (strong) NSMutableDictionary *wsdlStandardNamespaces;

+ (SOAP_USGlobals *)sharedInstance;

@end