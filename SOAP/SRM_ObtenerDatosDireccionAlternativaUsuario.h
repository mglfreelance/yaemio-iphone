//
//  SRM_ObtenerDatosDireccionAlternativaUsuario.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 28/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRM_ObtenerDatosDireccionAlternativaUsuario : SoapMethod
{
}

+ (SRM_ObtenerDatosDireccionAlternativaUsuario *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * IdUsuario;

@end

@interface SRM_ObtenerDatosDireccionAlternativaUsuarioResponse : SoapMethod
{
}

+ (SRM_ObtenerDatosDireccionAlternativaUsuarioResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) Direccion * Direccion;

@end
