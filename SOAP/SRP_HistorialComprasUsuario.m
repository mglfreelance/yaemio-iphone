//
//  SRP_HistorialComprasUsuario.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_HistorialComprasUsuario.h"
#import "Historico.h"

@implementation SRP_HistorialComprasUsuario

- (id)init
{
	if((self = [super init]))
    {
        self.PosicionInicio  = 0;
        self.NumeroRegistros = 0;
        self.TipoProducto    = @"";
	}
	
	return self;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"yaes";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addInt:self.NumeroRegistros WithKey:@"NumElem"];
    [self.ParametrosLLamada addInt:self.PosicionInicio WithKey:@"PosIni"];
    [self.ParametrosLLamada addObject:self.TipoProducto WithKey:@"Tipo"];
    
    return true;
}

+ (SRP_HistorialComprasUsuario *)deserializeNode:(xmlNodePtr)cur
{
	SRP_HistorialComprasUsuario *newObject = [SRP_HistorialComprasUsuario new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark - BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"DevolverHistoricoV2"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IYaesmio/DevolverHistoricoV2";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_HistorialComprasUsuarioResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "DevolverHistoricoV2Response";
}

@end


@implementation SRP_HistorialComprasUsuarioResponse

@synthesize ListaProductos = _ListaProductos;

- (id)init
{
	if((self = [super init]))
    {
        _NumeroElementos = 0;
        _ListaProductos  = nil;
	}
	
	return self;
}

+ (SRP_HistorialComprasUsuarioResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRP_HistorialComprasUsuarioResponse *newObject = [SRP_HistorialComprasUsuarioResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
    
	return newObject;
}

- (NSString *)getNameResult
{
    return @"DevolverHistoricoV2Result";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ListaHistorico"])
    {
        //vuelvo a deserializar los hijos
        _ListaProductos = [NSArray deserializeNode:pobjCur toClass:[Historico class]];
    }
    else if([nodeName isEqualToString:@"NumeroElementos"])
    {
        _NumeroElementos = [NSString deserializeNode:pobjCur].integerValue;
    }
}


@end
