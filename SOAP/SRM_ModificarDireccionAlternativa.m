//
//  ServicioRegistro_ModificarDireccionAlternativa.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_ModificarDireccionAlternativa.h"

@implementation SRM_ModificarDireccionAlternativa

- (id)init
{
	if((self = [super init]))
    {
		self.IdUsuario = nil;
		self.segundaDir = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
	if(self.IdUsuario != 0)
    {
		xmlAddChild(node, [self.IdUsuario xmlNodeForDoc:node->doc elementName:@"sapId" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
	if(self.segundaDir != 0)
    {
		xmlAddChild(node, [self.segundaDir xmlNodeForDoc:node->doc elementName:@"segundaDir" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_ModificarDireccionAlternativa *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ModificarDireccionAlternativa *newObject = [SRM_ModificarDireccionAlternativa new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"sapId"])
    {
        self.IdUsuario = [NSString deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"segundaDir"])
    {
        self.segundaDir = [Direccion deserializeNode:pobjCur];
    }
}

#pragma mark- BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ModificarDireccionAlternativa"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/ModificarDireccionAlternativa";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_ModificarDireccionAlternativaResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ModificarDireccionAlternativaResponse";
}


@end






@implementation SRM_ModificarDireccionAlternativaResponse

- (id)init
{
	if((self = [super init]))
    {
		self.ModificarDireccionAlternativaResult = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
	if(self.ModificarDireccionAlternativaResult != 0)
    {
		xmlAddChild(node, [self.ModificarDireccionAlternativaResult xmlNodeForDoc:node->doc elementName:@"ModificarDireccionAlternativaResult" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_ModificarDireccionAlternativaResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ModificarDireccionAlternativaResponse *newObject = [SRM_ModificarDireccionAlternativaResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ModificarDireccionAlternativaResult"])
    {
        self.ModificarDireccionAlternativaResult = [NSString deserializeNode:pobjCur];
    }
}

@end
