//
//  SNO_ActivarNotificacionPush.h
//  Yaesmio
//
//  Created by freelance on 14/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SNT_ActivarNotificacionPush : SoapGenericMethod

+ (SNT_ActivarNotificacionPush *)			deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *IdPush;
@property (strong) NSString *IdUsuario;
@property (strong) NSString *MacDevice;
@property (strong) NSString *Telefono;

@end


@interface SNT_ActivarNotificacionPushResponse : SoapGenericMethod

+ (SNT_ActivarNotificacionPushResponse *)	deserializeNode:(xmlNodePtr)cur;

@end
