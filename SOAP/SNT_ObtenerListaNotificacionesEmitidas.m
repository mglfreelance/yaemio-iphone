//
//  SNT_ObtenerListaNotificacionesEmitidas.m
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_ObtenerListaNotificacionesEmitidas.h"
#import "CabeceraNotificacion.h"

@implementation SNT_ObtenerListaNotificacionesEmitidas

- (id)init
{
    if ((self = [super init]))
    {
        self.Visto           = false;
        self.IdUsuario       = @"";
        self.NumeroLineas    = 0;
        self.PosicionInicial = 0;
    }
    
    return self;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"notificaciones";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.IdUsuario WithKey:@"EmisorOReceptor"];
    [self.ParametrosLLamada addInt:self.NumeroLineas WithKey:@"NumeroLineas"];
    [self.ParametrosLLamada addInt:self.PosicionInicial WithKey:@"PosicionInicial"];
    [self.ParametrosLLamada addObject:self.Visto ? @"V":@"" WithKey:@"Visto"];
    
    return true;
}

+ (SNT_ObtenerListaNotificacionesEmitidas *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ObtenerListaNotificacionesEmitidas *newObject = [SNT_ObtenerListaNotificacionesEmitidas new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerListaNotificacionesEmitidas"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/ObtenerListaNotificacionesEmitidas";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_ObtenerListaNotificacionesEmitidasResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ObtenerListaNotificacionesEmitidasResponse";
}

@end


@implementation SNT_ObtenerListaNotificacionesEmitidasResponse

- (id)init
{
    if ((self = [super init]))
    {
        self.ListaNotificaciones = nil;
        self.NumeroTotal   = 0;
    }
    
    return self;
}

+ (SNT_ObtenerListaNotificacionesEmitidasResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ObtenerListaNotificacionesEmitidasResponse *newObject = [SNT_ObtenerListaNotificacionesEmitidasResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ObtenerListaNotificacionesEmitidasResult";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ListaCabeceraNotificaciones"])
    {
        self.ListaNotificaciones = [NSArray deserializeNode:pobjCur toClass:[CabeceraNotificacion class]];
    }
    else if ([nodeName isEqualToString:@"NumeroMensajes"])
    {
        self.NumeroTotal = [NSString deserializeNode:pobjCur].intValue;
    }
}

@end
