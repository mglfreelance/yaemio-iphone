//
//  SRP_RealizarCompraPrecioCero.m
//  Yaesmio
//
//  Created by freelance on 09/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SRP_RealizarCompraPrecioCero.h"


@implementation SRP_RealizarCompraPrecioCero

- (id)init
{
    if ((self = [super init]))
    {
        self.Parametros = [ParametrosRealizarCompraPrecioCero new];
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Parametros xmlNodeForDoc:node->doc elementName:@"parametros" elementNSPrefix:@"ServicioRegistroSvc"]);
}

+ (SRP_RealizarCompraPrecioCero *)deserializeNode:(xmlNodePtr)cur
{
    SRP_RealizarCompraPrecioCero *newObject = [SRP_RealizarCompraPrecioCero new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation


- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"RealizarCompraPrecioCero"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IYaesmio/RealizarCompraPrecioCero";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_RealizarCompraPrecioCeroResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"RealizarCompraPrecioCeroResponse";
}

@end



@implementation SRP_RealizarCompraPrecioCeroResponse

- (id)init
{
    if ((self = [super init]))
    {
        self.CodigoRetorno = -1;
        self.MensajeSAP  = @"";
    }
    
    return self;
}

+ (SRP_RealizarCompraPrecioCeroResponse *)deserializeNode:(xmlNodePtr)cur
{
    SRP_RealizarCompraPrecioCeroResponse *newObject = [SRP_RealizarCompraPrecioCeroResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"RealizarCompraPrecioCeroResult"])
    {
        [self deserializeElementsFromNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"CodigoRetorno"])
    {
        self.CodigoRetorno = [[NSString deserializeNode:pobjCur] intValue];
    }
    else if ([nodeName isEqualToString:@"MensajeSap"])
    {
        self.MensajeSAP = [NSString deserializeNode:pobjCur];
    }
}

@end
