//
//  SRP_DevolverEmoticonos.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_DevolverEmoticonos.h"
#import "Emoticono.h"

@implementation SRP_DevolverEmoticonos

- (id)init
{
	if((self = [super init]))
    {
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
}

+ (SRP_DevolverEmoticonos *)deserializeNode:(xmlNodePtr)cur
{
	SRP_DevolverEmoticonos *newObject = [SRP_DevolverEmoticonos new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark - BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"DevolverEmoticonos"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IYaesmio/DevolverEmoticonos";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_DevolverEmoticonosResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "DevolverEmoticonosResponse";
}

@end


@implementation SRP_DevolverEmoticonosResponse

@synthesize ListaEmoticonos = _ListaEmoticonos;

- (id)init
{
	if((self = [super init]))
    {
	}
	
	return self;
}

+ (SRP_DevolverEmoticonosResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRP_DevolverEmoticonosResponse *newObject = [SRP_DevolverEmoticonosResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
    
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"DevolverEmoticonosResult"])
    {
        _ListaEmoticonos = [NSArray deserializeNode:pobjCur toClass:[Emoticono class]];
    }
}


@end