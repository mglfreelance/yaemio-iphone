//
//  ServicioRegistro_RecuperarContrasena.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 24/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_RecuperarContrasena.h"

@implementation SRM_RecuperarContrasena

- (id)init
{
	if((self = [super init]))
    {
		self.email = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{	
	if(self.email != 0)
    {
		xmlAddChild(node, [self.email xmlNodeForDoc:node->doc elementName:@"email" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_RecuperarContrasena *)deserializeNode:(xmlNodePtr)cur
{
	SRM_RecuperarContrasena *newObject = [SRM_RecuperarContrasena new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"email"])
    {
        self.email = [NSString deserializeNode:pobjCur];
    }
}

#pragma mark- BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"RecuperarContrasena"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/RecuperarContrasena";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_RecuperarContrasenaResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "RecuperarContrasenaResponse";
}

@end






@implementation SRM_RecuperarContrasenaResponse
- (id)init
{
	if((self = [super init]))
    {
		self.RecuperarContrasenaResult = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{	
	if(self.RecuperarContrasenaResult != 0)
    {
		xmlAddChild(node, [self.RecuperarContrasenaResult xmlNodeForDoc:node->doc elementName:@"RecuperarContrasenaResult" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_RecuperarContrasenaResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_RecuperarContrasenaResponse *newObject = [SRM_RecuperarContrasenaResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"RecuperarContrasenaResult"])
    {
        self.RecuperarContrasenaResult = [NSNumber deserializeNode:pobjCur];
    }
}

@end
