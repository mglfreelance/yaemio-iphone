//
//  ServicioRegistro_CrearUsurioWeb.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_CrearUsuarioMovil.h"

@implementation SRM_CrearUsuarioMovil

- (id)init
{
	if((self = [super init]))
    {
		self.usuario = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{	
	if(self.usuario != 0)
    {
		xmlAddChild(node, [self.usuario xmlNodeForDoc:node->doc elementName:@"usuario" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_CrearUsuarioMovil *)deserializeNode:(xmlNodePtr)cur
{
	SRM_CrearUsuarioMovil *newObject = [SRM_CrearUsuarioMovil new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"CrearUsuarioMovil"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/CrearUsuarioMovil";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_CrearUsuarioMovilResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "CrearUsuarioMovilResult";
}

@end







@implementation SRM_CrearUsuarioMovilResponse

- (id)init
{
	if((self = [super init]))
    {
		self.CrearUsurioWebResult = nil;
        self.MensajeSap = @"";
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{	
	if(self.CrearUsurioWebResult != 0)
    {
		xmlAddChild(node, [self.CrearUsurioWebResult xmlNodeForDoc:node->doc elementName:@"CrearUsuarioMovilResult" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_CrearUsuarioMovilResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_CrearUsuarioMovilResponse *newObject = [SRM_CrearUsuarioMovilResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"CrearUsuarioMovilResult"])
    {
        self.CrearUsurioWebResult = [NSNumber deserializeNode:pobjCur];
    }
}

@end
