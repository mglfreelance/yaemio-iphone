//
//  SRM_ObtenerTitulosInformacion.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SRM_ObtenerTitulosInformacion : SoapMethod

+ (SRM_ObtenerTitulosInformacion *)deserializeNode:(xmlNodePtr)cur;


@end


@interface SRM_ObtenerTitulosInformacionResponse : SoapMethod

+ (SRM_ObtenerTitulosInformacionResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSArray *ListaTitulos;

@end
