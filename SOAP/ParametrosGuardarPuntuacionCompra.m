//
//  ParametrosGuardarPuntuacionCompra.m
//  Yaesmio
//
//  Created by freelance on 13/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ParametrosGuardarPuntuacionCompra.h"

@implementation ParametrosGuardarPuntuacionCompra

- (id)init
{
    if ((self = [super init]))
    {
        self.IDPedido        = @"";
        self.IDProducto      = @"";
        self.TextoValoracion = 0;
        self.TextoValoracion = @"";
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.IDPedido xmlNodeForDoc:node->doc elementName:@"IdPedido" elementNSPrefix:@"yaes"]);
    
    xmlAddChild(node, [self.IDProducto xmlNodeForDoc:node->doc elementName:@"IdProducto" elementNSPrefix:@"yaes"]);
    
    xmlAddChild(node, [[NSString stringWithFormat:@"%d", self.Valor] xmlNodeForDoc:node->doc elementName:@"Puntuacion" elementNSPrefix:@"yaes"]);
    
    xmlAddChild(node, [self.TextoValoracion xmlNodeForDoc:node->doc elementName:@"TextoValoracion" elementNSPrefix:@"yaes"]);
}

@end
