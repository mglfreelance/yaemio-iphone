//
//  SRP_devolverDatosPaypal.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 17/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_devolverDatosPaypal.h"

@implementation SRP_devolverDatosPaypal

- (id)init
{
	if((self = [super init]))
    {
		self.ProductoID = @"";
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.ProductoID xmlNodeForDoc:node->doc elementName:@"productoId" elementNSPrefix:@"ServicioRegistroSvc"]);
}

+ (SRP_devolverDatosPaypal *)deserializeNode:(xmlNodePtr)cur
{
	SRP_devolverDatosPaypal *newObject = [SRP_devolverDatosPaypal new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark - BindingOperation

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"devolverDatosPaypal"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IYaesmio/devolverDatosPaypal";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_devolverDatosPaypalResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "devolverDatosPaypalResponse";
}

@end



@implementation SRP_devolverDatosPaypalResponse

- (id)init
{
	if((self = [super init]))
    {
		self.DatosPaypal = @"";
	}
	
	return self;
}

+ (SRP_devolverDatosPaypalResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRP_devolverDatosPaypalResponse *newObject = [SRP_devolverDatosPaypalResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(xmlNodePtr)pobjCur
{
    if(xmlStrEqual(pobjCur->name, (const xmlChar *) "devolverDatosPaypalResult"))
    {
        self.DatosPaypal = [NSString deserializeNode:pobjCur];
    }
}

@end

