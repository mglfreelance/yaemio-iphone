//
//  ServicioRegistro_ModificarDireccionPrincipal.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 28/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRM_ModificarDireccionPrincipal: SoapMethod
{
}

+ (SRM_ModificarDireccionPrincipal *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString  * IdUsuario;
@property (strong) Direccion * primeraDir;

@end




@interface SRM_ModificarDireccionPrincipalResponse : SoapMethod
{
}

+ (SRM_ModificarDireccionPrincipalResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * ModificarDireccionPrincipalResult;

@end

