//
//  SRM_ObtenerTarjetas.h
//  Yaesmio
//
//  Created by freelance on 09/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SRM_ObtenerTarjetas : SoapGenericMethod

+ (SRM_ObtenerTarjetas *)deserializeNode:(xmlNodePtr)cur;

@end

@interface SRM_ObtenerTarjetasResponse : SoapGenericMethod
{
}

+ (SRM_ObtenerTarjetasResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSArray * ListaTarjetas;

@end
