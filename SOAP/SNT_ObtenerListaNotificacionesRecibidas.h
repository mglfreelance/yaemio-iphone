//
//  SNT_ObtenerListaNotificacionesRecibidas.h
//  Yaesmio
//
//  Created by freelance on 16/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SNT_ObtenerListaNotificacionesRecibidas : SoapGenericMethod

+ (SNT_ObtenerListaNotificacionesRecibidas *)			deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *IdUsuario;
@property (assign) int NumeroLineas;
@property (assign) int PosicionInicial;
@property (assign) BOOL Visto;

@end


@interface SNT_ObtenerListaNotificacionesRecibidasResponse : SoapGenericMethod

+ (SNT_ObtenerListaNotificacionesRecibidasResponse *)	deserializeNode:(xmlNodePtr)cur;

@property (strong) NSArray *ListaNotificaciones;
@property (assign) int NumeroTotal;

@end
