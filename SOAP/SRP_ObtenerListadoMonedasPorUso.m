//
//  ObtenerListadoMonedasPorUso.m
//  Yaesmio
//
//  Created by freelance on 08/07/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SRP_ObtenerListadoMonedasPorUso.h"
#import "Moneda.h"

@implementation SRP_ObtenerListadoMonedasPorUso

- (id)init
{
    if ((self = [super init]))
    {
    }
    
    return self;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"yaes";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:@"2" WithKey:@"Uso"];
    
    return true;
}

+ (SRP_ObtenerListadoMonedasPorUso *)deserializeNode:(xmlNodePtr)cur
{
    SRP_ObtenerListadoMonedasPorUso *newObject = [SRP_ObtenerListadoMonedasPorUso new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark- BindingOperation

- (void)getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerListadoMonedasPorUso"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IYaesmio/ObtenerListadoMonedasPorUso";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_ObtenerListadoMonedasPorUsoResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ObtenerListadoMonedasPorUsoResponse";
}

@end




@implementation SRP_ObtenerListadoMonedasPorUsoResponse

- (id)init
{
    if ((self = [super init]))
    {
    }
    
    return self;
}

+ (SRP_ObtenerListadoMonedasPorUsoResponse *)deserializeNode:(xmlNodePtr)cur
{
    SRP_ObtenerListadoMonedasPorUsoResponse *newObject = [SRP_ObtenerListadoMonedasPorUsoResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ObtenerListadoMonedasPorUsoResult";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ListadoMonedas"])
    {
        _ListaMonedas = [NSArray deserializeNode:pobjCur toClass:[Moneda class]];
    }
}

@end