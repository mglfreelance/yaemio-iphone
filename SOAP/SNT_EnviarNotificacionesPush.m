//
//  SNT_EnviarNotificacionesPush.m
//  Yaesmio
//
//  Created by freelance on 22/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_EnviarNotificacionesPush.h"

@implementation SNT_EnviarNotificacionesPush

- (id)init
{
    if ((self = [super init]))
    {
        self.ListaIdContactosSap    = nil;
        self.ListaMails             = nil;
        self.ListaIdGruposContactos = nil;
        self.IdUsuario              = @"";
        self.IdProducto             = @"";
        self.Medio                  = @"";
        self.TextoMensaje           = @"";
    }
    
    return self;
}

+ (SNT_EnviarNotificacionesPush *)deserializeNode:(xmlNodePtr)cur
{
    SNT_EnviarNotificacionesPush *newObject = [SNT_EnviarNotificacionesPush new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"tnsParametros";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.IdProducto WithKey:@"IdProducto"];
    [self.ParametrosLLamada addObject:self.IdUsuario WithKey:@"IdUsuario"];
    [self.ParametrosLLamada addObject:[self getListaIds:self.ListaIdContactosSap] WithKey:@"ListContactosDestinatarios"];
    [self.ParametrosLLamada addObject:[self getListaIds:self.ListaMails] WithKey:@"ListEmailsDestinatarios"];
    [self.ParametrosLLamada addObject:[self getListaIds:self.ListaIdGruposContactos] WithKey:@"ListGruposDestinatarios"];
    [self.ParametrosLLamada addObject:self.Medio WithKey:@"Medio"];
    [self.ParametrosLLamada addObject:self.TextoMensaje WithKey:@"TextoEnvio"];
    
    return true;
}

- (ArrayOfSoapObject *)getListaIds:(NSArray*)pLista
{
    ArrayOfSoapObject *resul = [ArrayOfSoapObject new];
    
    resul.Name = @"";
    resul.ChilldnsPrefix = @"tns1";
    resul.nsPrefix = @"";
    
    for (NSString *item in pLista)
    {
        [resul addObject:item WithKey:@"string"];
    }
    
    return resul;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"EnviarNotificacionesPush"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/EnviarNotificacionesPush";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_EnviarNotificacionesPushResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"EnviarNotificacionesPushResponse";
}

@end


@implementation SNT_EnviarNotificacionesPushResponse

+ (SNT_EnviarNotificacionesPushResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_EnviarNotificacionesPushResponse *newObject = [SNT_EnviarNotificacionesPushResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"EnviarNotificacionesPushResult";
}

@end
