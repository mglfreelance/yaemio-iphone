//
//  SRM_ObtenerTitulosInformacion.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_ObtenerTitulosInformacion.h"
#import "TituloInformacion.h"

@implementation SRM_ObtenerTitulosInformacion

- (id)init
{
	if((self = [super init]))
    {
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
}

+ (SRM_ObtenerTitulosInformacion *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerTitulosInformacion *newObject = [SRM_ObtenerTitulosInformacion new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark - BindingOperation

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerTitulosInformacion"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/ObtenerTitulosInformacion";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_ObtenerTitulosInformacionResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ObtenerTitulosInformacionResponse";
}

@end


@implementation SRM_ObtenerTitulosInformacionResponse

- (id)init
{
	if((self = [super init]))
    {
		self.ListaTitulos = nil;
	}
	
	return self;
}

+ (SRM_ObtenerTitulosInformacionResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerTitulosInformacionResponse *newObject = [SRM_ObtenerTitulosInformacionResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ObtenerTitulosInformacionResult"])
    {
        self.ListaTitulos = [NSArray deserializeNode:pobjCur toClass:[TituloInformacion class]];
    }
    
}

@end
