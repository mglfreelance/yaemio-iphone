//
//  SRP_ObtenerDatosPorId.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 20/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"
#import "Producto.h"

@interface SRP_ObtenerProductoHijo : SoapMethod

+ (SRP_ObtenerProductoHijo *)			deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *IdProducto;

@end


@interface SRP_ObtenerProductoHijoResponse : SoapMethod

+ (SRP_ObtenerProductoHijoResponse *)	deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) Producto *ProductoResponse;

@end
