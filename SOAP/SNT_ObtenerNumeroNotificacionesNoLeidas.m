//
//  SNT_ObtenerNumeroNotificacionesNoLeidas.m
//  Yaesmio
//
//  Created by freelance on 03/06/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_ObtenerNumeroNotificacionesNoLeidas.h"

@implementation SNT_ObtenerNumeroNotificacionesNoLeidas

- (id)init
{
    if ((self = [super init]))
    {
    }
    
    return self;
}

+ (SNT_ObtenerNumeroNotificacionesNoLeidas *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ObtenerNumeroNotificacionesNoLeidas *newObject = [SNT_ObtenerNumeroNotificacionesNoLeidas new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerNumeroNotificacionesNoLeidas"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/ObtenerNumeroNotificacionesNoLeidas";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_ObtenerNumeroNotificacionesNoLeidasResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ObtenerNumeroNotificacionesNoLeidasResponse";
}

@end


@implementation SNT_ObtenerNumeroNotificacionesNoLeidasResponse

+ (SNT_ObtenerNumeroNotificacionesNoLeidasResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ObtenerNumeroNotificacionesNoLeidasResponse *newObject = [SNT_ObtenerNumeroNotificacionesNoLeidasResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ObtenerNumeroNotificacionesNoLeidasResult";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"NumeroNotificacionesNoLeidas"])
    {
        self.NumeroNotificacionesSinLeer = [NSString deserializeNode:pobjCur].integerValue;
    }
}

@end
