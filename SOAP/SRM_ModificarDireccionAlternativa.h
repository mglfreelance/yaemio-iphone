//
//  ServicioRegistro_ModificarDireccionAlternativa.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRM_ModificarDireccionAlternativa : SoapMethod
{
}

+ (SRM_ModificarDireccionAlternativa *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * IdUsuario;
@property (strong) Direccion * segundaDir;

@end




@interface SRM_ModificarDireccionAlternativaResponse : SoapMethod
{
}

+ (SRM_ModificarDireccionAlternativaResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * ModificarDireccionAlternativaResult;

@end

