//
//  SNO_ActivarNotificacionPush.m
//  Yaesmio
//
//  Created by freelance on 14/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_ActivarNotificacionPush.h"
#import "ArrayOfSoapObject.h"

@implementation SNT_ActivarNotificacionPush

- (id)init
{
    if ((self = [super init]))
    {
        self.IdUsuario                      = @"";
        self.IdPush                         = @"";
        self.MacDevice                      = @"";
        self.Telefono                       = @"";
    }
    
    return self;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix =@"tnsParametros";
    self.ParametrosLLamada.nsPrefix =@"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.IdPush WithKey:@"IdPush"];
    [self.ParametrosLLamada addObject:self.IdUsuario WithKey:@"IdUsuario"];
    [self.ParametrosLLamada addObject:self.MacDevice WithKey:@"MacDevice"];
    [self.ParametrosLLamada addObject:self.Telefono WithKey:@"Telefono"];
    
    return true;
}

+ (SNT_ActivarNotificacionPush *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ActivarNotificacionPush *newObject = [SNT_ActivarNotificacionPush new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ActivarNotificacionPush"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/ActivarNotificacionPush";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_ActivarNotificacionPushResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ActivarNotificacionPushResponse";
}

@end


@implementation SNT_ActivarNotificacionPushResponse

+ (SNT_ActivarNotificacionPushResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ActivarNotificacionPushResponse *newObject = [SNT_ActivarNotificacionPushResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ActivarNotificacionPushResult";
}

@end
