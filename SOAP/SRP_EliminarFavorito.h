//
//  SRP_EliminarFavorito.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 20/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRP_EliminarFavorito : SoapMethod

+ (SRP_EliminarFavorito *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *IdUsuario;
@property (strong) NSString *IdFavorito;

@end



@interface SRP_EliminarFavoritoResponse : SoapMethod

+ (SRP_EliminarFavoritoResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *MensajeResultado;
@property (assign) BOOL     esCorrecto;
@end
