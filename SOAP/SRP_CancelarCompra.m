//
//  SRP_CancelarCompra.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/08/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_CancelarCompra.h"

@implementation SRP_CancelarCompra

- (id)init
{
	if((self = [super init]))
    {
        self.IdCompra = @"";
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.IdCompra xmlNodeForDoc:node->doc elementName:@"identCompra" elementNSPrefix:@"ServicioRegistroSvc"]);
}

+ (SRP_CancelarCompra *)deserializeNode:(xmlNodePtr)cur
{
	SRP_CancelarCompra *newObject = [SRP_CancelarCompra new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark - BindingOperation

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"CancelarCompra"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IYaesmio/CancelarCompra";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_CancelarCompraResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "CancelarCompraResponse";
}

@end


@implementation SRP_CancelarCompraResponse


- (id)init
{
	if((self = [super init]))
    {
	}
	
	return self;
}

+ (SRP_CancelarCompraResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRP_CancelarCompraResponse *newObject = [SRP_CancelarCompraResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

@end

