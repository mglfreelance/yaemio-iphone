//
//  ServicioRegistro_ConsultarEurosYaesmio.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_ConsultarEurosYaesmio.h"

@implementation SRM_ConsultarEurosYaesmio

- (id)init
{
	if((self = [super init]))
    {
		self.emailUsu = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{	
	if(self.emailUsu != 0)
    {
		xmlAddChild(node, [self.emailUsu xmlNodeForDoc:node->doc elementName:@"emailUsu" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_ConsultarEurosYaesmio *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ConsultarEurosYaesmio *newObject = [SRM_ConsultarEurosYaesmio new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"emailUsu"])
    {
        self.emailUsu = [NSString deserializeNode:pobjCur];
    }
}

#pragma mark- BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ConsultarEurosYaesmio"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/ConsultarEurosYaesmio";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_ConsultarEurosYaesmioResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ConsultarEurosYaesmioResponse";
}

@end




@implementation SRM_ConsultarEurosYaesmioResponse

- (id)init
{
	if((self = [super init]))
    {
		self.ConsultarEurosYaesmioResult = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
	if(self.ConsultarEurosYaesmioResult != 0)
    {
		xmlAddChild(node, [self.ConsultarEurosYaesmioResult xmlNodeForDoc:node->doc elementName:@"ConsultarEurosYaesmioResult" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_ConsultarEurosYaesmioResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ConsultarEurosYaesmioResponse *newObject = [SRM_ConsultarEurosYaesmioResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ConsultarEurosYaesmioResult"])
    {
        self.ConsultarEurosYaesmioResult = [NSNumber deserializeNode:pobjCur];
    }
}

@end
