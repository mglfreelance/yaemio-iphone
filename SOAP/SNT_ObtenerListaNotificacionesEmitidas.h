//
//  SNT_ObtenerListaNotificacionesEmitidas.h
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SNT_ObtenerListaNotificacionesEmitidas : SoapGenericMethod

+ (SNT_ObtenerListaNotificacionesEmitidas *)			deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *IdUsuario;
@property (assign) int NumeroLineas;
@property (assign) int PosicionInicial;
@property (assign) BOOL Visto;

@end


@interface SNT_ObtenerListaNotificacionesEmitidasResponse : SoapGenericMethod

+ (SNT_ObtenerListaNotificacionesEmitidasResponse *)	deserializeNode:(xmlNodePtr)cur;

@property (strong) NSArray *ListaNotificaciones;
@property (assign) int NumeroTotal;


@end
