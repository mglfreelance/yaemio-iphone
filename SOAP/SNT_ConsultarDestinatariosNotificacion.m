//
//  SNT_ConsultarDestinatariosNotificacion.m
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_ConsultarDestinatariosNotificacion.h"
#import "DestinatarioNotificacion.h"

@implementation SNT_ConsultarDestinatariosNotificacion

- (id)init
{
    if ((self = [super init]))
    {
        self.IdNotificacion     = @"";
    }
    
    return self;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"notificaciones";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.IdNotificacion WithKey:@"IdEnvio"];
    
    return true;
}

+ (SNT_ConsultarDestinatariosNotificacion *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ConsultarDestinatariosNotificacion *newObject = [SNT_ConsultarDestinatariosNotificacion new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ConsultarDestinatariosNotificacion"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/ConsultarDestinatariosNotificacion";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_ConsultarDestinatariosNotificacionResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ConsultarDestinatariosNotificacionResponse";
}

@end


@implementation SNT_ConsultarDestinatariosNotificacionResponse

+ (SNT_ConsultarDestinatariosNotificacionResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ConsultarDestinatariosNotificacionResponse *newObject = [SNT_ConsultarDestinatariosNotificacionResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ConsultarDestinatariosNotificacionResult";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ListaDestinatarios"])
    {
        self.ListaDestinatarios = [NSArray deserializeNode:pobjCur toClass:[DestinatarioNotificacion class]];
    }
}
@end
