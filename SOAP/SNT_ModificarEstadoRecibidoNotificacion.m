//
//  SNT_ModificarEstadoRecibidoNotificacion.m
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_ModificarEstadoRecibidoNotificacion.h"

@implementation SNT_ModificarEstadoRecibidoNotificacion

- (id)init
{
    if ((self = [super init]))
    {
        self.IdNotificacion     = @"";
        self.IdUsuario          = @"";
        self.EstadoNotificacion = SNTipoNotificacionNinguna;
    }
    
    return self;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"tnsParametros";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addInt:self.EstadoNotificacion WithKey:@"EstadoNotificacion"];
    [self.ParametrosLLamada addObject:self.IdNotificacion WithKey:@"IdNotificacion"];
    [self.ParametrosLLamada addObject:self.IdUsuario WithKey:@"IdUsuarioDestinatario"];
    
    return true;
}

+ (SNT_ModificarEstadoRecibidoNotificacion *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ModificarEstadoRecibidoNotificacion *newObject = [SNT_ModificarEstadoRecibidoNotificacion new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ModificarEstadoRecibidoNotificacion"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/ModificarEstadoRecibidoNotificacion";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_ModificarEstadoRecibidoNotificacionResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ModificarEstadoRecibidoNotificacionResponse";
}

@end


@implementation SNT_ModificarEstadoRecibidoNotificacionResponse

+ (SNT_ModificarEstadoRecibidoNotificacionResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ModificarEstadoRecibidoNotificacionResponse *newObject = [SNT_ModificarEstadoRecibidoNotificacionResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ModificarEstadoRecibidoNotificacionResult";
}


@end
