//
//  SRM_ObtenerSitiosYaesmio.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 12/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SRM_ObtenerSitiosYaesmio : SoapMethod

+ (SRM_ObtenerSitiosYaesmio *)deserializeNode:(xmlNodePtr)cur;


@end


@interface SRM_ObtenerSitiosYaesmioResponse : SoapMethod

+ (SRM_ObtenerSitiosYaesmioResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSArray *ListaSitios;

@end
