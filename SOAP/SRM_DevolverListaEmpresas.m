//
//  SRM_DevolverListaEmpresas.m
//  Yaesmio
//
//  Created by freelance on 22/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SRM_DevolverListaEmpresas.h"

@implementation SRM_DevolverListaEmpresas

- (id)init
{
	if((self = [super init]))
    {
        self.Parametros = [ParametrosDevolverListaEmpresa new];
	}
	
	return self;
}

+ (SRM_DevolverListaEmpresas *)deserializeNode:(xmlNodePtr)cur
{
	SRM_DevolverListaEmpresas *newObject = [SRM_DevolverListaEmpresas new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark- BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Parametros xmlNodeForDoc:node->doc elementName:@"parametros" elementNSPrefix:@"ServicioRegistroSvc"]);
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerMarcas"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IYaesmio/ObtenerMarcas";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_DevolverListaEmpresasResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ObtenerMarcasResponse";
}

@end





@implementation SRM_DevolverListaEmpresasResponse

NSArray *mListaResultado;

- (id)init
{
	if((self = [super init]))
    {
        mListaResultado = [NSMutableArray new];
	}
	
	return self;
}

-(NSArray*) ListaEmpresaResult
{
    return mListaResultado;
}

+ (SRM_DevolverListaEmpresasResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_DevolverListaEmpresasResponse *newObject = [SRM_DevolverListaEmpresasResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ObtenerMarcasResult"])
    {
        [self deserializeElementsFromNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"CodigoRetorno"])
    {
        //vuelvo a deserializar los hijos
        self.CodigoRetorno = [[NSString deserializeNode:pobjCur] intValue];
    }
    else if([nodeName isEqualToString:@"MensajeSap"])
    {
        //vuelvo a deserializar los hijos
        self.MensajeSAP = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ListadoMarcas"])
    {
        if (pobjCur->children != nil)
        {
            mListaResultado =[NSArray deserializeNode:pobjCur toClass:[Empresa class]];
        }
    }
}

@end

