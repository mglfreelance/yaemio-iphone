//
//  SNT_CrearOEliminarContatoEnGrupo.m
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_CrearOEliminarContatoEnGrupo.h"
#import "ContactoGrupo.h"

@implementation SNT_CrearOEliminarContatoEnGrupo

- (id)init
{
    if ((self = [super init]))
    {
        self.EsCrear = @"";
        self.IdUsuario = @"";
        self.ListaIdUsuario = nil;
        self.IdGrupo = @"";
    }
    
    return self;
}

+ (SNT_CrearOEliminarContatoEnGrupo *)deserializeNode:(xmlNodePtr)cur
{
    SNT_CrearOEliminarContatoEnGrupo *newObject = [SNT_CrearOEliminarContatoEnGrupo new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"grupos";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.EsCrear WithKey:@"Crear"];
    [self.ParametrosLLamada addObject:[self getListaIds] WithKey:@"IdContactos"];
    [self.ParametrosLLamada addObject:self.IdGrupo WithKey:@"IdGrupo"];
    [self.ParametrosLLamada addObject:self.IdUsuario WithKey:@"IdUsuario"];
    
    return true;
}

- (ArrayOfSoapObject *)getListaIds
{
    ArrayOfSoapObject *resul = [ArrayOfSoapObject new];
    
    resul.Name = @"IdContactos";
    resul.ChilldnsPrefix = @"tns1";
    resul.nsPrefix = @"ServicioRegistroSvc";
    
    for (NSString *item in self.ListaIdUsuario)
    {
        [resul addObject:item WithKey:@"string"];
    }
    
    return resul;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"CrearOEliminarContatoEnGrupo"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/CrearOEliminarContatoEnGrupo";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_CrearOEliminarContatoEnGrupoResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"CrearOEliminarContatoEnGrupoResponse";
}

@end


@implementation SNT_CrearOEliminarContatoEnGrupoResponse

+ (SNT_CrearOEliminarContatoEnGrupoResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_CrearOEliminarContatoEnGrupoResponse *newObject = [SNT_CrearOEliminarContatoEnGrupoResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"CrearOEliminarContatoEnGrupoResult";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"Contactos"])
    {
        self.ListaContactos = [NSArray deserializeNode:pobjCur toClass:[ContactoGrupo class]];
    }
}

@end
