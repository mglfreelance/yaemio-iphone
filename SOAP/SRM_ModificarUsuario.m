//
//  ServicioRegistro_ModificarUsuario.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_ModificarUsuario.h"

@implementation SRM_ModificarUsuario

- (id)init
{
	if((self = [super init]))
    {
		self.usu = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.usu xmlNodeForDoc:node->doc elementName:@"usu" elementNSPrefix:@"ServicioRegistroSvc"]);
}

+ (SRM_ModificarUsuario *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ModificarUsuario *newObject = [SRM_ModificarUsuario new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark- BindingOperation

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ModificarUsuarioV2"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/ModificarUsuarioV2";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_ModificarUsuarioResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ModificarUsuarioV2Response";
}

@end







@implementation SRM_ModificarUsuarioResponse

- (id)init
{
	if((self = [super init]))
    {
		self.ModificarUsuarioResult = nil;
        self.MensajeSap = @"";
	}
	
	return self;
}

+ (SRM_ModificarUsuarioResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ModificarUsuarioResponse *newObject = [SRM_ModificarUsuarioResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ModificarUsuarioV2Result"])
    {
        //vuelvo a desserializar el objeto
        [self deserializeElementsFromNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"CodigoRetorno"])
    {
        self.ModificarUsuarioResult = [NSNumber deserializeNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"MensajeSap"])
    {
        self.MensajeSap = [NSString deserializeNode:pobjCur];
    }
}

@end
