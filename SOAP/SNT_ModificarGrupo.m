//
//  SNT_ModificarGrupo.m
//  Yaesmio
//
//  Created by freelance on 20/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_ModificarGrupo.h"

@implementation SNT_ModificarGrupo

- (id)init
{
    if ((self = [super init]))
    {
        self.Grupo = nil;
        self.IdUsuario = @"";
    }
    
    return self;
}

+ (SNT_ModificarGrupo *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ModificarGrupo *newObject = [SNT_ModificarGrupo new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"grupos";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.Grupo.NombreGrupo WithKey:@"Grupo"];
    [self.ParametrosLLamada addObject:self.Grupo.IdGrupo WithKey:@"IdGrupo"];
    [self.ParametrosLLamada addObject:self.IdUsuario WithKey:@"IdUsuario"];
    [self.ParametrosLLamada addObject:self.Grupo.ListaContactos WithKey:@"ListadoContactos"];
    
    return true;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ModificarGrupo"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/ModificarGrupo";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_ModificarGrupoResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ModificarGrupoResponse";
}

@end


@implementation SNT_ModificarGrupoResponse

+ (SNT_ModificarGrupoResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ModificarGrupoResponse *newObject = [SNT_ModificarGrupoResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ModificarGrupoResult";
}


@end
