//
//  SNT_ComprobarContactos.m
//  Yaesmio
//
//  Created by freelance on 21/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_ComprobarContactos.h"
#import "Contacto.h"

@implementation SNT_ComprobarContactos

- (id)init
{
    if ((self = [super init]))
    {
        self.IdUsuario      = @"";
        self.ListaContactos = nil;
    }
    
    return self;
}

+ (SNT_ComprobarContactos *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ComprobarContactos *newObject = [SNT_ComprobarContactos new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"tnsParametros";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addObject:self.IdUsuario WithKey:@"IdUsuario"];
    [self.ParametrosLLamada addObject:self.ListaContactos WithKey:@"ListaContactos"];
    
    return true;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ComprobarContactos"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/ComprobarContactos";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_ComprobarContactosResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ComprobarContactosResponse";
}

@end


@implementation SNT_ComprobarContactosResponse

+ (SNT_ComprobarContactosResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ComprobarContactosResponse *newObject = [SNT_ComprobarContactosResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ComprobarContactosResult";
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"ListaContactos"])
    {
        self.ListaContactos = [NSArray deserializeNode:pobjCur toClass:[Contacto class]];
    }
}

@end
