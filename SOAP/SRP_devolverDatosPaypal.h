//
//  SRP_devolverDatosPaypal.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 17/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SoapObject.h"

@interface SRP_devolverDatosPaypal : SoapObject
{
}

+ (SRP_devolverDatosPaypal *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * ProductoID;

@end



@interface SRP_devolverDatosPaypalResponse : SoapObject
{
}

+ (SRP_devolverDatosPaypalResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * DatosPaypal;

@end
