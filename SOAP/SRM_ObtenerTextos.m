//
//  SRM_ObtenerTextos.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 10/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_ObtenerTextos.h"

@implementation SRM_ObtenerTextos
- (id)init
{
	if((self = [super init]))
    {
		self.CodigoMenu = @"";
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.CodigoMenu xmlNodeForDoc:node->doc elementName:@"text" elementNSPrefix:@"ServicioRegistroSvc"]);
}

+ (SRM_ObtenerTextos *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerTextos *newObject = [SRM_ObtenerTextos new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark - BindingOperation

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerTextosInfYaesmio"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/ObtenerTextosInfYaesmio";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_ObtenerTextosResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ObtenerTextosInfYaesmioResponse";
}

@end


@implementation SRM_ObtenerTextosResponse

- (id)init
{
	if((self = [super init]))
    {
		self.Texto = @"";
	}
	
	return self;
}

+ (SRM_ObtenerTextosResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ObtenerTextosResponse *newObject = [SRM_ObtenerTextosResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ObtenerTextosInfYaesmioResult"])
    {
        self.Texto = [NSString deserializeNode:pobjCur];
    }
    
}

@end