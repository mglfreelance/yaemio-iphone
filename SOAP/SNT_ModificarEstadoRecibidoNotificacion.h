//
//  SNT_ModificarEstadoRecibidoNotificacion.h
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"
#import "CabeceraNotificacion.h"

@interface SNT_ModificarEstadoRecibidoNotificacion : SoapGenericMethod

+ (SNT_ModificarEstadoRecibidoNotificacion *)			deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *IdUsuario;
@property (assign) SNTipoNotificacion EstadoNotificacion;
@property (strong) NSString *IdNotificacion;

@end


@interface SNT_ModificarEstadoRecibidoNotificacionResponse : SoapGenericMethod

+ (SNT_ModificarEstadoRecibidoNotificacionResponse *)	deserializeNode:(xmlNodePtr)cur;


@end
