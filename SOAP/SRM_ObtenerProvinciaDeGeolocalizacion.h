//
//  SRM_ObtenerProvinciaDeGeolocalizacion.h
//  Yaesmio
//
//  Created by freelance on 27/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"
#import "Provincia.h"

@interface SRM_ObtenerProvinciaDeGeolocalizacion : SoapGenericMethod

+ (SRM_ObtenerProvinciaDeGeolocalizacion *)			deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *IdPais;
@property (strong) NSString *CodigoPostal;
@property (assign) double Latitud;
@property (assign) double Logintud;

@end


@interface SRM_ObtenerProvinciaDeGeolocalizacionResponse : SoapGenericMethod

+ (SRM_ObtenerProvinciaDeGeolocalizacionResponse *)	deserializeNode:(xmlNodePtr)cur;

@property (strong) NSString *IdProvincia;
@property (strong) NSString *IdPais;
@property (strong) NSString *NombreProvincia;
@property (strong) NSString *NombrePais;

@end
