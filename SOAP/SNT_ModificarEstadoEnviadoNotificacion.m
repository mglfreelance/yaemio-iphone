//
//  SNT_ModificarEstadoEnviadoNotificacion.m
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SNT_ModificarEstadoEnviadoNotificacion.h"

@implementation SNT_ModificarEstadoEnviadoNotificacion

- (id)init
{
    if ((self = [super init]))
    {
        self.IdNotificacion     = @"";
        self.EstadoNotificacion = SNTipoNotificacionNinguna;
    }
    
    return self;
}

- (BOOL)agregateChildParametrosLLamada
{
    //config
    self.ParametrosLLamada.Name = @"parametros";
    self.ParametrosLLamada.ChilldnsPrefix = @"tnsParametros";
    self.ParametrosLLamada.nsPrefix = @"ServicioRegistroSvc";
    
    //agregate child objects
    [self.ParametrosLLamada addInt:self.EstadoNotificacion WithKey:@"EstadoEnvioNotificacion"];
    [self.ParametrosLLamada addObject:self.IdNotificacion WithKey:@"IdNotificacion"];
    
    return true;
}

+ (SNT_ModificarEstadoEnviadoNotificacion *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ModificarEstadoEnviadoNotificacion *newObject = [SNT_ModificarEstadoEnviadoNotificacion new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ModificarEstadoEnviadoNotificacion"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/INotificacion/ModificarEstadoEnviadoNotificacion";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SNT_ModificarEstadoEnviadoNotificacionResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ModificarEstadoEnviadoNotificacionResponse";
}

@end


@implementation SNT_ModificarEstadoEnviadoNotificacionResponse

+ (SNT_ModificarEstadoEnviadoNotificacionResponse *)deserializeNode:(xmlNodePtr)cur
{
    SNT_ModificarEstadoEnviadoNotificacionResponse *newObject = [SNT_ModificarEstadoEnviadoNotificacionResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ModificarEstadoEnviadoNotificacionResult";
}

@end
