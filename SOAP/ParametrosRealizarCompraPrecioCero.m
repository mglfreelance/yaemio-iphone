//
//  ParametrosRealizarCompraPrecioCero.m
//  Yaesmio
//
//  Created by freelance on 09/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ParametrosRealizarCompraPrecioCero.h"

@implementation ParametrosRealizarCompraPrecioCero

- (id)init
{
	if((self = [super init]))
    {
		self.IdUsuario  = @"";
        self.Producto   = nil;
	}
	
	return self;
}

- (id)initWithIdUsuario:(NSString *)pIdUsuario Producto:(ElementoCarrito *)pProducto
{
	if((self = [self init]))
    {
		self.IdUsuario = pIdUsuario;
        self.Producto  = pProducto;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Producto xmlNodeForDoc:node->doc elementName:@"Producto" elementNSPrefix:@"tnsParametros"]);
    
    xmlAddChild(node, [self.IdUsuario xmlNodeForDoc:node->doc elementName:@"UsuarioId" elementNSPrefix:@"tnsParametros"]);
}


@end
