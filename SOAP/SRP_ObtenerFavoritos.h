//
//  SRP_ObtenerFavoritos.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 20/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRP_ObtenerFavoritos : SoapMethod

+ (SRP_ObtenerFavoritos *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *IdUsuario;
@property (assign) int       PosicionInicio;
@property (assign) int       NumeroRegistros;

@end



@interface SRP_ObtenerFavoritosResponse : SoapMethod

+ (SRP_ObtenerFavoritosResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (readonly) NSArray * ListaFavoritos;

@end
