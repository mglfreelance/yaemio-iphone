//
//  SNT_ConsultarDestinatariosNotificacion.h
//  Yaesmio
//
//  Created by freelance on 19/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"

@interface SNT_ConsultarDestinatariosNotificacion : SoapGenericMethod

+ (SNT_ConsultarDestinatariosNotificacion *)			deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *IdNotificacion;

@end


@interface SNT_ConsultarDestinatariosNotificacionResponse : SoapGenericMethod

+ (SNT_ConsultarDestinatariosNotificacionResponse *)	deserializeNode:(xmlNodePtr)cur;

@property (strong) NSArray *ListaDestinatarios;

@end
