//
//  ServicioRegistro_DevolverListaProvincias.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRM_DevolverListaProvincias : SoapMethod

+ (SRM_DevolverListaProvincias *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString * pais;

@end




@interface SRM_DevolverListaProvinciasResponse : SoapMethod

+ (SRM_DevolverListaProvinciasResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (readonly) NSArray * DevolverListaProvinciasResult;

@end
