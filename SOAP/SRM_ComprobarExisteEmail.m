//
//  ServicioRegistro_ComprobarExisteEmail.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_ComprobarExisteEmail.h"

@implementation SRM_ComprobarExisteEmail

- (id)init
{
	if((self = [super init]))
    {
		self.email = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
	if(self.email != 0)
    {
		xmlAddChild(node, [self.email xmlNodeForDoc:node->doc elementName:@"email" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_ComprobarExisteEmail *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ComprobarExisteEmail *newObject = [SRM_ComprobarExisteEmail new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"email"])
    {
        self.email = [NSString deserializeNode:pobjCur];
    }
}

#pragma mark - BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ComprobarExisteEmail"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/ComprobarExisteEmail";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_ComprobarExisteEmailResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ComprobarExisteEmailResponse";
}

@end



@implementation SRM_ComprobarExisteEmailResponse

- (id)init
{
	if((self = [super init]))
    {
		self.ComprobarExisteEmailResult = false;
	}
	
	return self;
}

+ (SRM_ComprobarExisteEmailResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_ComprobarExisteEmailResponse *newObject = [SRM_ComprobarExisteEmailResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ComprobarExisteEmailResult"])
    {
        self.ComprobarExisteEmailResult = [SOAP_Boolean deserializeNode:pobjCur].boolValue;
    }
}

@end
