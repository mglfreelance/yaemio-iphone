//
//  SRP_ObtenerDatosPorId.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 20/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_ObtenerProductoHijo.h"

@implementation SRP_ObtenerProductoHijo
- (id)init
{
    if ((self = [super init]))
    {
        self.IdProducto = @"";
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.IdProducto xmlNodeForDoc:node->doc elementName:@"codigo" elementNSPrefix:@"ServicioRegistroSvc"]);
}

+ (SRP_ObtenerProductoHijo *)deserializeNode:(xmlNodePtr)cur
{
    SRP_ObtenerProductoHijo *newObject = [SRP_ObtenerProductoHijo new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"DevolverDatosProductosSapHijo"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IYaesmio/DevolverDatosProductosSapHijo";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_ObtenerProductoHijoResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"DevolverDatosProductosSapHijoResponse";
}

@end


@implementation SRP_ObtenerProductoHijoResponse

- (id)init
{
    if ((self = [super init]))
    {
        self.ProductoResponse = nil;
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    if (self.ProductoResponse != nil)
    {
        xmlAddChild(node, [self.ProductoResponse xmlNodeForDoc:node->doc elementName:@"DevolverDatosProductosSapHijoResponse" elementNSPrefix:@"ServicioRegistroSvc"]);
    }
}

+ (SRP_ObtenerProductoHijoResponse *)deserializeNode:(xmlNodePtr)cur
{
    SRP_ObtenerProductoHijoResponse *newObject = [SRP_ObtenerProductoHijoResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if ([nodeName isEqualToString:@"DevolverDatosProductosSapHijoResult"])
    {
        //vuelvo a deserializar los hijos
        self.ProductoResponse = [Producto deserializeNode:pobjCur];
    }
}

@end
