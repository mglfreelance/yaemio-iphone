//
//  SRM_ObtenerProvinciaDeGeolocalizacion.m
//  Yaesmio
//
//  Created by freelance on 27/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SRM_ObtenerProvinciaDeGeolocalizacion.h"

@implementation SRM_ObtenerProvinciaDeGeolocalizacion

- (id)init
{
    if ((self = [super init]))
    {
        self.IdPais       = @"";
        self.CodigoPostal = @"";
        self.Latitud      = 0.0;
        self.Logintud     = 0.0;
    }
    
    return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.IdPais xmlNodeForDoc:node->doc elementName:@"pais" elementNSPrefix:@"ServicioRegistroSvc"]);
    xmlAddChild(node, [self.CodigoPostal xmlNodeForDoc:node->doc elementName:@"codpostal" elementNSPrefix:@"ServicioRegistroSvc"]);
    xmlAddChild(node, [[NSString stringWithFormat:@"%f", self.Logintud] xmlNodeForDoc:node->doc elementName:@"longitud" elementNSPrefix:@"ServicioRegistroSvc"]);
    xmlAddChild(node, [[NSString stringWithFormat:@"%f", self.Latitud] xmlNodeForDoc:node->doc elementName:@"latitud" elementNSPrefix:@"ServicioRegistroSvc"]);
}

+ (SRM_ObtenerProvinciaDeGeolocalizacion *)deserializeNode:(xmlNodePtr)cur
{
    SRM_ObtenerProvinciaDeGeolocalizacion *newObject = [SRM_ObtenerProvinciaDeGeolocalizacion new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

#pragma mark - BindingOperation

- (void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerProvinciaPorGeolocalizacion"];
}

- (NSString *)getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/ObtenerProvinciaPorGeolocalizacion";
}

- (SoapObject *)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_ObtenerProvinciaDeGeolocalizacionResponse deserializeNode:bodyNode];
}

- (const xmlChar *)getMethodResponse
{
    return (const xmlChar *)"ObtenerProvinciaPorGeolocalizacionResponse";
}

@end


@implementation SRM_ObtenerProvinciaDeGeolocalizacionResponse

- (id)init
{
    if ((self = [super init]))
    {
        self.IdPais          = @"";
        self.IdProvincia     = @"";
        self.NombrePais      = @"";
        self.NombreProvincia = @"";
    }
    
    return self;
}

+ (SRM_ObtenerProvinciaDeGeolocalizacionResponse *)deserializeNode:(xmlNodePtr)cur
{
    SRM_ObtenerProvinciaDeGeolocalizacionResponse *newObject = [SRM_ObtenerProvinciaDeGeolocalizacionResponse new];
    
    [newObject deserializeAttributesFromNode:cur];
    [newObject deserializeElementsFromNode:cur];
    
    return newObject;
}

- (NSString *)getNameResult
{
    return @"ObtenerProvinciaPorGeolocalizacionResult";
}

- (void)ElementForNode:(NSString *)pName Node:(xmlNodePtr)pobjCur
{
    if ([pName isEqualToString:@"IdPais"])
    {
        self.IdPais = [NSString deserializeNode:pobjCur];
    }
    else if ([pName isEqualToString:@"IdProvincia"])
    {
        self.IdProvincia = [NSString deserializeNode:pobjCur];
    }
    else if ([pName isEqualToString:@"NombrePais"])
    {
        self.NombrePais = [NSString deserializeNode:pobjCur];
    }
    else if ([pName isEqualToString:@"NombreProvincia"])
    {
        self.NombreProvincia = [NSString deserializeNode:pobjCur];
    }
}

@end
