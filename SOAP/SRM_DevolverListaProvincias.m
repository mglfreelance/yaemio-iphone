//
//  ServicioRegistro_DevolverListaProvincias.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRM_DevolverListaProvincias.h"
#import "Provincia.h"

@implementation SRM_DevolverListaProvincias

- (id)init
{
	if((self = [super init]))
    {
		self.pais = nil;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{	
	if(self.pais != 0)
    {
		xmlAddChild(node, [self.pais xmlNodeForDoc:node->doc elementName:@"pais" elementNSPrefix:@"ServicioRegistroSvc"]);
	}
}

+ (SRM_DevolverListaProvincias *)deserializeNode:(xmlNodePtr)cur
{
	SRM_DevolverListaProvincias *newObject = [SRM_DevolverListaProvincias new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"pais"])
    {
        self.pais = [NSString deserializeNode:pobjCur];
    }
}

#pragma mark- BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"DevolverListaProvincias"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IServicioRegistro/DevolverListaProvincias";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_DevolverListaProvinciasResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "DevolverListaProvinciasResponse";
}

@end






@implementation SRM_DevolverListaProvinciasResponse

NSMutableArray *mListaResultado;

- (id)init
{
	if((self = [super init]))
    {
		mListaResultado = [NSMutableArray new];
	}
	
	return self;
}

-(NSArray*) DevolverListaProvinciasResult
{
    return mListaResultado;
}

+ (SRM_DevolverListaProvinciasResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_DevolverListaProvinciasResponse *newObject = [SRM_DevolverListaProvinciasResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"DevolverListaProvinciasResult"])
    {
        [self deserializeElementsFromNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Provincia"])
    {
        if (pobjCur->children != nil)
        {
            [mListaResultado addObject:[Provincia deserializeNode:pobjCur]];
        }
    }
}

@end
