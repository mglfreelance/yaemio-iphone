//
//  SRM_DevolverListaCategoriasProducto.h
//  Yaesmio
//
//  Created by freelance on 22/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"


@interface SRM_DevolverListaGrupoArticulo: SoapMethod
{
}

+ (SRM_DevolverListaGrupoArticulo *)deserializeNode:(xmlNodePtr)cur;

@end



@interface SRM_DevolverListaGrupoArticuloResponse : SoapMethod
{
}

+ (SRM_DevolverListaGrupoArticuloResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (readonly)  NSArray     *ListaGrupoArticuloResult;
@property (nonatomic) int         CodigoRetorno;
@property (strong)    NSString    *MensajeSAP;

@end

