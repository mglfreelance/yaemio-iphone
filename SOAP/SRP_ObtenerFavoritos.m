//
//  SRP_ObtenerFavoritos.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 20/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_ObtenerFavoritos.h"
#import "Favorito.h"

@implementation SRP_ObtenerFavoritos

- (id)init
{
	if((self = [super init]))
    {
		self.IdUsuario     = @"";
        self.PosicionInicio  = 0;
        self.NumeroRegistros = 0;
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.IdUsuario xmlNodeForDoc:node->doc elementName:@"usu" elementNSPrefix:@"ServicioRegistroSvc"]);
    
     xmlAddChild(node, [[NSString stringWithFormat:@"%d", self.PosicionInicio] xmlNodeForDoc:node->doc elementName:@"posIni" elementNSPrefix:@"ServicioRegistroSvc"]);
    
     xmlAddChild(node, [[NSString stringWithFormat:@"%d", self.NumeroRegistros] xmlNodeForDoc:node->doc elementName:@"numElem" elementNSPrefix:@"ServicioRegistroSvc"]);
    
}

+ (SRP_ObtenerFavoritos *)deserializeNode:(xmlNodePtr)cur
{
	SRP_ObtenerFavoritos *newObject = [SRP_ObtenerFavoritos new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark - BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"DevolverFavoritos"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IYaesmio/DevolverFavoritos";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_ObtenerFavoritosResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "DevolverFavoritosResponse";
}

@end


@implementation SRP_ObtenerFavoritosResponse

NSMutableArray *mLista;

- (id)init
{
	if((self = [super init]))
    {
		mLista = [NSMutableArray new];
	}
	
	return self;
}

-(NSArray*) ListaFavoritos
{
    return mLista;
}

+ (SRP_ObtenerFavoritosResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRP_ObtenerFavoritosResponse *newObject = [SRP_ObtenerFavoritosResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
    
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"DevolverFavoritosResult"])
    {
        //vuelvo a deserializar los hijos
        [self deserializeElementsFromNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"Favorito"])
    {
        Favorito *lProducto=[Favorito deserializeNode:pobjCur];
        
        [mLista addObject:lProducto];
    }
}

@end
