//
//  ServicioRegistro_CrearUsurioWeb.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 27/05/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"
#import "MembershipDatosUsuario.h"

@interface SRM_CrearUsuarioMovil : SoapMethod
{
}

+ (SRM_CrearUsuarioMovil *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) MembershipDatosUsuario * usuario;

@end




@interface SRM_CrearUsuarioMovilResponse : SoapMethod
{

}

+ (SRM_CrearUsuarioMovilResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSNumber * CrearUsurioWebResult;
@property (strong) NSString * MensajeSap;

@end

