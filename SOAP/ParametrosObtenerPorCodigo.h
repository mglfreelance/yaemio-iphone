//
//  ParametrosObtenerPorCodigo.h
//  Yaesmio
//
//  Created by freelance on 23/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SoapObject.h"
#import "ProductoPadre.h"

@interface ParametrosObtenerPorCodigo : SoapObject

+ (ParametrosObtenerPorCodigo *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic, strong) NSString *Codigo;
@property (nonatomic, strong) NSString *Medio;
@property (nonatomic, assign) TipoCanalProducto Origen;

@end
