//
//  SRA_ObtenerConfiguracionApp.h
//  Yaesmio
//
//  Created by freelance on 30/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRA_ObtenerConfiguracionApp : SoapGenericMethod

+ (SRA_ObtenerConfiguracionApp *)			deserializeNode:(xmlNodePtr)cur;

@property(strong) NSString *IdApp;

@end



@interface SRA_ObtenerConfiguracionAppResponse : SoapGenericMethod

+ (SRA_ObtenerConfiguracionAppResponse *)	deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) NSString *UrlSonido;
@property (strong) NSString *UrlImagenFondoApp;
@property (strong) NSArray  *Parametros;

@end
