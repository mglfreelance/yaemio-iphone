//
//  SRP_ValorarProductoCompra.m
//  Yaesmio
//
//  Created by manuel garcia lopez on 11/09/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import "SRP_ValorarProductoCompra.h"

@implementation SRP_ValorarProductoCompra

- (id)init
{
	if((self = [super init]))
    {
		self.Parametros = [[ParametrosGuardarPuntuacionCompra alloc] init];
	}
	
	return self;
}

- (void)addElementsToNode:(xmlNodePtr)node
{
    xmlAddChild(node, [self.Parametros xmlNodeForDoc:node->doc elementName:@"parametros" elementNSPrefix:@"ServicioRegistroSvc"]);
}

+ (SRP_ValorarProductoCompra *)deserializeNode:(xmlNodePtr)cur
{
	SRP_ValorarProductoCompra *newObject = [SRP_ValorarProductoCompra new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark - BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"GuardarPuntuacionCompraV2"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IYaesmio/GuardarPuntuacionCompraV2";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRP_ValorarProductoCompraResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "GuardarPuntuacionCompraV2Response";
}

@end


@implementation SRP_ValorarProductoCompraResponse

@synthesize Mensaje = _Mensaje;
@synthesize Codigo = _Codigo;

- (id)init
{
	if((self = [super init]))
    {
        _Mensaje = @"";
        _Codigo  = -1;
	}
	
	return self;
}

+ (SRP_ValorarProductoCompraResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRP_ValorarProductoCompraResponse *newObject = [SRP_ValorarProductoCompraResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
    
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"GuardarPuntuacionCompraV2Result"])
    {
        //vuelvo a desserializar el objeto
        [self deserializeElementsFromNode:pobjCur];
    }
    else  if([nodeName isEqualToString:@"CodigoRetorno"])
    {
        _Codigo= [NSString deserializeNode:pobjCur].intValue;
    }
    else  if([nodeName isEqualToString:@"MensajeSap"])
    {
        _Mensaje = [NSString deserializeNode:pobjCur];
    }
}


@end
