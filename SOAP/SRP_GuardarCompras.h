//
//  SRP_GuardarCompras.h
//  Yaesmio
//
//  Created by manuel garcia lopez on 17/06/13.
//  Copyright (c) 2013 befree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ServicioSOAP.h"

@interface SRP_GuardarCompras : SoapGenericMethod
{
}

+ (SRP_GuardarCompras *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) Direccion *Direccion;
@property (strong) NSString  *IdUsuario;
@property (strong) NSArray   *ListaProductos;

@end



@interface SRP_GuardarComprasResponse : SoapGenericMethod
{
}

+ (SRP_GuardarComprasResponse *)deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic) NSString *IDCarrito;

@end
