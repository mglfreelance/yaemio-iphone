//
//  SRP_RealizarCompraPrecioCero.h
//  Yaesmio
//
//  Created by freelance on 09/05/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "ServicioSOAP.h"
#import "ParametrosRealizarCompraPrecioCero.h"

@interface SRP_RealizarCompraPrecioCero : SoapMethod

+ (SRP_RealizarCompraPrecioCero *)			deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (strong) ParametrosRealizarCompraPrecioCero *Parametros;

@end

@interface SRP_RealizarCompraPrecioCeroResponse : SoapMethod

+ (SRP_RealizarCompraPrecioCeroResponse *)	deserializeNode:(xmlNodePtr)cur;

/* elements */
@property (nonatomic) int CodigoRetorno;
@property (strong)    NSString *MensajeSAP;



@end
