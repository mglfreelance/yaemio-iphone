//
//  SRM_DevolverListaCategoriasProducto.m
//  Yaesmio
//
//  Created by freelance on 22/04/14.
//  Copyright (c) 2014 befree. All rights reserved.
//

#import "SRM_DevolverListaGrupoArticulo.h"
#import "GrupoArticulo.h"

@implementation SRM_DevolverListaGrupoArticulo

- (id)init
{
	if((self = [super init])) {
	}
	
	return self;
}

+ (SRM_DevolverListaGrupoArticulo *)deserializeNode:(xmlNodePtr)cur
{
	SRM_DevolverListaGrupoArticulo *newObject = [SRM_DevolverListaGrupoArticulo new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

#pragma mark- BindingOperation

-(void) getHeaderElements:(NSMutableDictionary *)headerElements
{
    //nothing
}

-(void)getBodyElements:(NSMutableDictionary *)bodyElements
{
    [bodyElements setObject:self forKey:@"ObtenerListadoProductosAgrupados"];
}

-(NSString*) getSoapAction
{
    return @"http://tempuri.org/IYaesmio/ObtenerListadoProductosAgrupados";
}

-(SoapObject*)CreateResponse:(xmlNodePtr)headerNode AndBody:(xmlNodePtr)bodyNode
{
    return [SRM_DevolverListaGrupoArticuloResponse deserializeNode:bodyNode];
}

-(const xmlChar *) getMethodResponse
{
    return (const xmlChar *) "ObtenerListadoProductosAgrupadosResponse";
}

@end





@implementation SRM_DevolverListaGrupoArticuloResponse

NSArray *mListaResultado;

- (id)init
{
	if((self = [super init]))
    {
        mListaResultado = [NSMutableArray new];
	}
	
	return self;
}

-(NSArray*) ListaGrupoArticuloResult
{
    return mListaResultado;
}

+ (SRM_DevolverListaGrupoArticuloResponse *)deserializeNode:(xmlNodePtr)cur
{
	SRM_DevolverListaGrupoArticuloResponse *newObject = [SRM_DevolverListaGrupoArticuloResponse new];
	
	[newObject deserializeAttributesFromNode:cur];
	[newObject deserializeElementsFromNode:cur];
	
	return newObject;
}

- (void)ElementForNode:(NSString *)nodeName Node:(xmlNodePtr)pobjCur
{
    if([nodeName isEqualToString:@"ObtenerListadoProductosAgrupadosResult"])
    {
        [self deserializeElementsFromNode:pobjCur];
    }
    else if([nodeName isEqualToString:@"CodigoRetorno"])
    {
        //vuelvo a deserializar los hijos
        self.CodigoRetorno = [[NSString deserializeNode:pobjCur] intValue];
    }
    else if([nodeName isEqualToString:@"MensajeSap"])
    {
        //vuelvo a deserializar los hijos
        self.MensajeSAP = [NSString deserializeNode:pobjCur];
    }
    else if ([nodeName isEqualToString:@"ListadoProductos"])
    {
        if (pobjCur->children != nil)
        {
            mListaResultado =[NSArray deserializeNode:pobjCur toClass:[GrupoArticulo class]];
        }
    }
}

@end
