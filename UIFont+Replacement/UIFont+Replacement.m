//
//  UIFont+Replacement.m
//  FontReplacer
//
//  Created by Cédric Luthi on 2011-08-08.
//  Copyright (c) 2011 Cédric Luthi. All rights reserved.
//

#import "UIFont+Replacement.h"
#import <objc/runtime.h>

@implementation UIFont (Replacement)

static NSDictionary *replacementDictionary = nil;

static void initializeReplacementFonts()
{
	static BOOL initialized = NO;
	if (initialized)
		return;
	initialized = YES;
	
	NSDictionary *replacementDictionary = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"ReplacementFonts"];
	[UIFont setReplacementDictionary:replacementDictionary];
}

+ (void) load
{
	Method fontWithName_size_ = class_getClassMethod([UIFont class], @selector(fontWithName:size:));
	Method fontWithName_size_traits_ = class_getClassMethod([UIFont class], @selector(fontWithName:size:));
	Method replacementFontWithName_size_ = class_getClassMethod([UIFont class], @selector(replacement_fontWithName:size:));
	Method replacementFontWithName_size_traits_ = class_getClassMethod([UIFont class], @selector(replacement_fontWithName:size:));
	
	if (fontWithName_size_ && replacementFontWithName_size_ && strcmp(method_getTypeEncoding(fontWithName_size_), method_getTypeEncoding(replacementFontWithName_size_)) == 0)
		method_exchangeImplementations(fontWithName_size_, replacementFontWithName_size_);
	if (fontWithName_size_traits_ && replacementFontWithName_size_traits_ && strcmp(method_getTypeEncoding(fontWithName_size_traits_), method_getTypeEncoding(replacementFontWithName_size_traits_)) == 0)
		method_exchangeImplementations(fontWithName_size_traits_, replacementFontWithName_size_traits_);
}

+ (UIFont *) replacement_fontWithName:(NSString *)fontName size:(CGFloat)fontSize
{
	initializeReplacementFonts();
	NSString *replacementFontName = [replacementDictionary objectForKey:fontName];
	UIFont *result =[self replacement_fontWithName:replacementFontName ?: fontName size:fontSize];
    [self logFuenteUtilizada:fontName NameResult:replacementFontName ResultFon:result];
    return result;
}

+ (UIFont *) replacement_fontWithName:(NSString *)fontName size:(CGFloat)fontSize traits:(int)traits
{
	initializeReplacementFonts();
	NSString *replacementFontName = [replacementDictionary objectForKey:fontName];
	UIFont *result = [self replacement_fontWithName:replacementFontName ?: fontName size:fontSize];
    [self logFuenteUtilizada:fontName NameResult:replacementFontName ResultFon:result];
    return result;
}

+ (NSDictionary *) replacementDictionary
{
	return replacementDictionary;
}

+ (void) setReplacementDictionary:(NSDictionary *)aReplacementDictionary
{
	if (aReplacementDictionary == replacementDictionary)
		return;
	
	for (id key in [aReplacementDictionary allKeys])
	{
		if (![key isKindOfClass:[NSString class]])
		{
			NSLog(@"ERROR: Replacement font key must be a string.");
			return;
		}
		
		id value = [aReplacementDictionary valueForKey:key];
		if (![value isKindOfClass:[NSString class]])
		{
			NSLog(@"ERROR: Replacement font value must be a string.");
			return;
		}
	}
	;
	replacementDictionary = aReplacementDictionary;
	
	for (id key in [replacementDictionary allKeys])
	{
		NSString *fontName = [replacementDictionary objectForKey:key];
		UIFont *font = [UIFont fontWithName:fontName size:10];
		if (!font)
			NSLog(@"WARNING: replacement font '%@' is not available.", fontName);
	}
}

+(void) logFuenteUtilizada:(NSString*)pNameFuente NameResult:(NSString*)pNameResultFont ResultFon:(UIFont*)pFuente
{
    if (![self EsFuenteReemplazada:pNameFuente] && [pNameFuente isEqualToString:pNameResultFont]==false && [pFuente.fontName isEqualToString:pNameFuente])
    {
        NSLog(@"Fuente no encontrada ->%@",pNameFuente);
    }
}

+(BOOL) EsFuenteReemplazada:(NSString*)pNameFuente
{
    BOOL lbolresultado = false;
    
    for (id key in [replacementDictionary allKeys])
	{
		if( [[replacementDictionary objectForKey:key] isEqualToString:pNameFuente])
        {
            lbolresultado=true;
            break; // salida rapida.
        }
	}
    return lbolresultado;
}

@end
