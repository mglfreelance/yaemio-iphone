<?php

// Put your device token here (without spaces):
//$deviceToken = '4f6fd1cfb6e2557f567b71b563da36683d6a4d002161fb2361866323c7cc5620'; // desarrollo
$deviceToken = '05d1289a52a28e3ce5cfd6149082e79bc5b619c50f74c398350492fe0b9e5ec1'; // desarrollo-produccion

// Put your private key's passphrase here:
$passphrase = 'nevaditos';

// Put your alert message here:
$message = 'My first push notification!';

////////////////////////////////////////////////////////////////////////////////


$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'cafile', 'entrust_2048_ca.cer');
stream_context_set_option($ctx, 'ssl', 'local_cert', 'ck.pem');
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
$fp = stream_socket_client(
	'ssl://gateway.sandbox.push.apple.com:2195', $err,
	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
$body['aps'] = array(
	'alert' => $message,
	'sound' => 'default'
	);

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result)
	echo 'Message not delivered' . PHP_EOL;
else
	echo 'Message successfully delivered' . PHP_EOL;

// Close the connection to the server
fclose($fp);
